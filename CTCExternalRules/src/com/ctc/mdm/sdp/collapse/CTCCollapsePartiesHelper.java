package com.ctc.mdm.sdp.collapse;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.ctc.mdm.addition.component.CTCDataSourceBObj;
import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.constants.CTCErrorConstants;
import com.ctc.mdm.common.util.CTCClassFactory;
import com.ctc.mdm.common.util.CTCCommonUtil;
import com.ctc.mdm.extension.component.CTCPartyAddressBObjExt;
import com.ctc.mdm.extension.component.CTCPartyContactMethodBObjExt;
import com.ctc.mdm.extension.component.CTCPersonNameBObjExt;
import com.dwl.base.DWLCommon;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMOrganizationBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyLobRelationshipBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCoreComponentID;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.coreParty.interfaces.IPartyBusinessServices;
import com.dwl.tcrm.coreParty.interfaces.IPerson;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.tcrm.utilities.TCRMExceptionUtils;

public class CTCCollapsePartiesHelper {

	private IParty partyComponent = null;
	private IPartyBusinessServices partyBusinessServicesComponent = null;
	private Vector<TCRMPartyContactMethodBObj> allInactivePartyContactMethodsList = null;
	
	
	public CTCCollapsePartiesHelper() throws Exception {
		super();
		partyComponent = (IParty) TCRMClassFactory
			.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
		partyBusinessServicesComponent = (IPartyBusinessServices) TCRMClassFactory
			.getTCRMComponent(TCRMCorePropertyKeys.PARTY_BUSINESS_SERVICES_COMPONENT);
		allInactivePartyContactMethodsList = new Vector<TCRMPartyContactMethodBObj>();
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public TCRMPartyBObj collapsePartiesUsingCTCSurvivorshipRules(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		
		String partyType = null;
		if(collapsedParty == null){
			TCRMPartyBObj partyBObj = vecParties.elementAt(0);
			Class bObjClass = null;
			if(partyBObj instanceof TCRMPersonBObj){
				bObjClass = TCRMPersonBObj.class;
				partyType = "P";
			}
			else
			{
				bObjClass = TCRMOrganizationBObj.class;
				partyType = "O";
			}
			
			collapsedParty = (TCRMPartyBObj) CTCClassFactory.createBObj(bObjClass, partyBObj.getControl());
			collapsedParty.setPartyType(partyType);
			
			//Set the CollapsedParty.CreatedDate = CURRENT DATE
			collapsedParty.setCreatedDate(DWLDateTimeUtilities.getCurrentSystemTime());
			
			// Set the SourceIdentifierType = MDM
			collapsedParty.setSourceIdentifierType(CTCConstants.SOURCE_IDENTIFIER_MDM);
		}
		
		if(collapsedParty instanceof TCRMPersonBObj){
			collapseBirthDate((TCRMPersonBObj)collapsedParty, vecParties);
			collapseGender((TCRMPersonBObj)collapsedParty, vecParties);
		}
		collapseLanguagePreference(collapsedParty, vecParties);
		collapseCustomerIdentifiers(collapsedParty, vecParties);
		collapseLOB(collapsedParty, vecParties);
		
		if(collapsedParty instanceof TCRMPersonBObj){
			collapseActivePersonNames((TCRMPersonBObj)collapsedParty, vecParties);
			collapseInactivePersonNames((TCRMPersonBObj)collapsedParty, vecParties);
		}
		collapseActiveAddresses(collapsedParty, vecParties);
		collapseInactiveAddresses(collapsedParty, vecParties);
		collapseActivePhoneNumbers(collapsedParty, vecParties);
		collapseInactivePhoneNumbers(collapsedParty, vecParties);
		collapseActiveEmails(collapsedParty, vecParties);
		collapseInactiveEmails(collapsedParty, vecParties);
		collapseActiveFaxNumbers(collapsedParty, vecParties);
		collapseInactiveFaxNumbers(collapsedParty, vecParties);
		return collapsedParty;
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseInactiveFaxNumbers(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		/*
		 * Store all the inactive Party ContactMethods from the input parties into allInactivePartyContactMethodsList
		 */
		
		if(!(isVectorNonEmpty(allInactivePartyContactMethodsList))){
			for (TCRMPartyBObj partyBObj : vecParties) {
				Vector<TCRMPartyContactMethodBObj> inactivePartyContactMethodsList = partyComponent
					.getAllPartyContactMethods(partyBObj.getPartyId(), TCRMRecordFilter.INACTIVE, partyBObj.getControl());
				if(isVectorNonEmpty(inactivePartyContactMethodsList)){
					allInactivePartyContactMethodsList.addAll(inactivePartyContactMethodsList);
				}
			}
		}
		
		/*
		 * Store all the inactive Fax Numbers into allInactiveFaxNumbersList
		 */
		Vector<TCRMPartyContactMethodBObj> allInactiveFaxNumbersList = new Vector<TCRMPartyContactMethodBObj>();
		if(isVectorNonEmpty(allInactivePartyContactMethodsList)){
			for (TCRMPartyContactMethodBObj partyContactMethodBObj : allInactivePartyContactMethodsList) {
				String contactMethodType = partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType();
				if(StringUtils.isNonBlank(contactMethodType) 
						&& StringUtils.compareIgnoreCaseWithTrim(contactMethodType, CTCConstants.CONT_METH_TP_FAX)){
					allInactiveFaxNumbersList.add(partyContactMethodBObj);
				}
			}
		}
		
		Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>> inactiveFaxNumbersTable = 
			new Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>>();
		
		boolean found = false;
		for (TCRMPartyContactMethodBObj partyContactMethodBObj : allInactiveFaxNumbersList) {
			found = false;
			
			TCRMContactMethodBObj contactMethodBObj = partyContactMethodBObj.getTCRMContactMethodBObj();
			Enumeration<TCRMPartyContactMethodBObj> e = inactiveFaxNumbersTable.keys();
			while (e.hasMoreElements() && !found) {
				TCRMPartyContactMethodBObj key = e.nextElement();
				TCRMContactMethodBObj keyContactMethod = key.getTCRMContactMethodBObj();
				
				//Compare the attributes ReferenceNumber, StartDate & EndDate between key & PartyContactMethod.
				if(!(StringUtils.compareIgnoreCaseWithTrim(partyContactMethodBObj.getStartDate(), key.getStartDate())
						&& StringUtils.compareIgnoreCaseWithTrim(partyContactMethodBObj.getEndDate(), key.getEndDate())
						&& StringUtils.compareIgnoreCaseWithTrim(contactMethodBObj.getReferenceNumber(), keyContactMethod.getReferenceNumber()))){
					// Attribute did not match. Continue to check with other keys.
					continue;
				}
				
				// All attributes matched.
				Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = inactiveFaxNumbersTable.get(key);
				
				vecPartyContactMethod.add(partyContactMethodBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}
			
			/*
			 * PartyContactMethod did not match any key. Add to the inactiveFaxNumbersTable table
			 */
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = new Vector<TCRMPartyContactMethodBObj>();
			vecPartyContactMethod.add(partyContactMethodBObj);
			
			inactiveFaxNumbersTable.put(partyContactMethodBObj, vecPartyContactMethod);
			
		} // End of FOR loop
		
		Vector< ? extends DWLCommon> survivingInactiveFaxNumbersList = collectKeys(inactiveFaxNumbersTable);
		
		/*
		 * Set the surviving inactive fax numbers into the collapsed party.
		 */
		collapsedParty.getItemsTCRMPartyContactMethodBObj().addAll(survivingInactiveFaxNumbersList);
		
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseActiveFaxNumbers(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		/*
		 * Store all the active Fax Numbers into allActiveFaxNumbersList.
		 */
		Vector<TCRMPartyContactMethodBObj> allActiveFaxNumbersList = new Vector<TCRMPartyContactMethodBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			Vector<TCRMPartyContactMethodBObj> activePartyContactMethodsList = 
				partyBObj.getItemsTCRMPartyContactMethodBObj();
			if(isVectorNonEmpty(activePartyContactMethodsList)){
				//Check if the ContactMethodType is 'Fax Number'
				for (TCRMPartyContactMethodBObj partyContactMethodBObj : activePartyContactMethodsList) {
					String contactMethodType = partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType();
					if(StringUtils.isNonBlank(contactMethodType) 
							&& StringUtils.compareIgnoreCaseWithTrim(contactMethodType, CTCConstants.CONT_METH_TP_FAX)){ 
						allActiveFaxNumbersList.add(partyContactMethodBObj);
					}
				}
			}
			
		}
		
		// Create a list to store the active fax numbers which may get end dated.
		Vector<TCRMPartyContactMethodBObj> endDatedFaxNumbersList = new  Vector<TCRMPartyContactMethodBObj>();
		
		Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>> faxNumbersTable = 
			new Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>>();
		
		boolean found = false;
		for (TCRMPartyContactMethodBObj partyContactMethodBObj : allActiveFaxNumbersList) {
			found = false;
			
			TCRMContactMethodBObj contactMethodBObj = partyContactMethodBObj.getTCRMContactMethodBObj();
			Enumeration<TCRMPartyContactMethodBObj> e = faxNumbersTable.keys();
			while (e.hasMoreElements() && !found) {
				TCRMPartyContactMethodBObj key = e.nextElement();
				TCRMContactMethodBObj keyContactMethod = key.getTCRMContactMethodBObj();
				
				Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = faxNumbersTable.get(key);
				
				//Compare the actual fax number value
				if(StringUtils.compareIgnoreCaseWithTrim(
						contactMethodBObj.getReferenceNumber(), keyContactMethod.getReferenceNumber())){
					
					//Compare the StartDate attribute between the key & input PartyAddress
					int comparisonOutput = key.getStartDate().compareTo(partyContactMethodBObj.getStartDate());
					
					if(comparisonOutput > 0){//implies key.Startdate is greater than PartyContactMethod.StartDate
						faxNumbersTable.remove(key);
						faxNumbersTable.put(partyContactMethodBObj, vecPartyContactMethod);
					}
				}
				else // Fax Numbers are different.
				{
					//Compare the LastUpdateDate between the key & PartyContactMethod
					int comparisonOutput = keyContactMethod.getContactMethodLastUpdateDate()
						.compareTo(contactMethodBObj.getContactMethodLastUpdateDate());
					
					if(comparisonOutput > 0){ //implies key is the latest.
						partyContactMethodBObj.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
						endDatedFaxNumbersList.add(partyContactMethodBObj);
					}
					else
					{
						key.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
						endDatedFaxNumbersList.add(key);
						faxNumbersTable.remove(key);
						faxNumbersTable.put(partyContactMethodBObj, vecPartyContactMethod);
					}
					
				}
				
				vecPartyContactMethod.add(partyContactMethodBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}

			/*
			 * If PartyContactMethod did not match any key, add to the faxNumbersTable
			 */
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = new Vector<TCRMPartyContactMethodBObj>();
			vecPartyContactMethod.add(partyContactMethodBObj);
			
			faxNumbersTable.put(partyContactMethodBObj, vecPartyContactMethod);
			
		} // End of FOR loop
		
		Vector< ? extends DWLCommon> survivingActiveFaxNumbersList = collectKeys(faxNumbersTable);
		
		/*
		 * Add the surviving active Fax Number + End Dated Fax Numbers to the collapsed party
		 */
		collapsedParty.getItemsTCRMPartyContactMethodBObj().addAll(survivingActiveFaxNumbersList);
		collapsedParty.getItemsTCRMPartyContactMethodBObj().addAll(endDatedFaxNumbersList);
		
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseInactiveEmails(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		/*
		 * Store all the inactive Party ContactMethods from the input parties into allInactivePartyContactMethodsList
		 */
		
		if(!(isVectorNonEmpty(allInactivePartyContactMethodsList))){
			for (TCRMPartyBObj partyBObj : vecParties) {
				Vector<TCRMPartyContactMethodBObj> inactivePartyContactMethodsList = partyComponent
					.getAllPartyContactMethods(partyBObj.getPartyId(), TCRMRecordFilter.INACTIVE, partyBObj.getControl());
				if(isVectorNonEmpty(inactivePartyContactMethodsList)){
					allInactivePartyContactMethodsList.addAll(inactivePartyContactMethodsList);
				}
			}
		}
		
		/*
		 * Store all the inactive Emails into allInactiveEmailsList
		 */
		Vector<TCRMPartyContactMethodBObj> allInactiveEmailsList = new Vector<TCRMPartyContactMethodBObj>();
		if(isVectorNonEmpty(allInactivePartyContactMethodsList)){
			for (TCRMPartyContactMethodBObj partyContactMethodBObj : allInactivePartyContactMethodsList) {
				String contactMethodType = partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType();
				if(StringUtils.isNonBlank(contactMethodType) 
						&& StringUtils.compareIgnoreCaseWithTrim(contactMethodType, CTCConstants.CONT_METH_TP_EMAIL)){ 
					allInactiveEmailsList.add(partyContactMethodBObj);
				}
			}
		}
		
		Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>> inactiveEmailsTable = 
			new Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>>();
		
		boolean found = false;
		for (TCRMPartyContactMethodBObj partyContactMethodBObj : allInactiveEmailsList) {
			found = false;
			
			TCRMContactMethodBObj contactMethodBObj = partyContactMethodBObj.getTCRMContactMethodBObj();
			Enumeration<TCRMPartyContactMethodBObj> e = inactiveEmailsTable.keys();
			while (e.hasMoreElements() && !found) {
				TCRMPartyContactMethodBObj key = e.nextElement();
				TCRMContactMethodBObj keyContactMethod = key.getTCRMContactMethodBObj();
				
				//Compare the attributes ReferenceNumber, StartDate & EndDate between key & PartyContactMethod.
				if(!(StringUtils.compareIgnoreCaseWithTrim(partyContactMethodBObj.getStartDate(), key.getStartDate())
						&& StringUtils.compareIgnoreCaseWithTrim(partyContactMethodBObj.getEndDate(), key.getEndDate())
						&& StringUtils.compareIgnoreCaseWithTrim(contactMethodBObj.getReferenceNumber(), keyContactMethod.getReferenceNumber()))){
					// Attribute did not match. Continue to check with other keys.
					continue;
				}
				
				// All attributes matched.
				Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = inactiveEmailsTable.get(key);
				
				vecPartyContactMethod.add(partyContactMethodBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}
			
			/*
			 * PartyContactMethod did not match any key. Add to the inactiveEmails table
			 */
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = new Vector<TCRMPartyContactMethodBObj>();
			vecPartyContactMethod.add(partyContactMethodBObj);
			
			inactiveEmailsTable.put(partyContactMethodBObj, vecPartyContactMethod);
			
		} // End of FOR loop
		
		Vector< ? extends DWLCommon> survivingInactiveEmailsList = collectKeys(inactiveEmailsTable);
		
		/*
		 * Set the surviving inactive emails into the collapsed party.
		 */
		collapsedParty.getItemsTCRMPartyContactMethodBObj().addAll(survivingInactiveEmailsList);
		
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseActiveEmails(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		/*
		 * Store all the active Emails into allActiveEmailsList.
		 */
		Vector<TCRMPartyContactMethodBObj> allActiveEmailsList = new Vector<TCRMPartyContactMethodBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			Vector<TCRMPartyContactMethodBObj> activePartyContactMethodsList = 
				partyBObj.getItemsTCRMPartyContactMethodBObj();
			if(isVectorNonEmpty(activePartyContactMethodsList)){
				//Check if the ContactMethodType is 'Email'
				for (TCRMPartyContactMethodBObj partyContactMethodBObj : activePartyContactMethodsList) {
					String contactMethodType = partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType();
					if(StringUtils.isNonBlank(contactMethodType) 
							&& StringUtils.compareIgnoreCaseWithTrim(contactMethodType, CTCConstants.CONT_METH_TP_EMAIL)){ 
						allActiveEmailsList.add(partyContactMethodBObj);
					}
				}
			}
		}
		
		/*
		 * Get the CTCDataSource information for each active Email
		 */
		
		for (TCRMPartyContactMethodBObj partyContactMethodBObj : allActiveEmailsList) {
			if(partyContactMethodBObj instanceof CTCPartyContactMethodBObjExt){
				((CTCPartyContactMethodBObjExt)partyContactMethodBObj).getRecord();
			}
		}

		Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>> emailsTable = 
			new Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>>();
		
		boolean found = false;
		for (TCRMPartyContactMethodBObj partyContactMethodBObj : allActiveEmailsList) {
			found = false;
			
			TCRMContactMethodBObj contactMethodBObj = partyContactMethodBObj.getTCRMContactMethodBObj();
			Enumeration<TCRMPartyContactMethodBObj> e = emailsTable.keys();
			while((e.hasMoreElements()) && (!found)){
				TCRMPartyContactMethodBObj key = e.nextElement();
				TCRMContactMethodBObj keyContactMethod = key.getTCRMContactMethodBObj();

				//Compare the attribute ReferenceNumber between key & PartyContactMethod.
				if(!(StringUtils.compareIgnoreCaseWithTrim(
						contactMethodBObj.getReferenceNumber(), keyContactMethod.getReferenceNumber()))){
					// Attribute did not match. Continue to check with other keys.
					continue;
				}
				
				// Attributes Matched.
				Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = emailsTable.get(key);
				
				//Compare the StartDate attribute between the key & input PartyContactMethod
				int comparisonOutput = key.getStartDate().compareTo(partyContactMethodBObj.getStartDate());
				
				if(comparisonOutput > 0){ //implies key.Startdate is greater than PartyContactMethod.StartDate
					
					emailsTable.remove(key);
					emailsTable.put(partyContactMethodBObj, vecPartyContactMethod);
				}
				
				vecPartyContactMethod.add(partyContactMethodBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}
			
			/*
			 * If PartyContactMethod did not match any key, add to the emailsTable
			 */
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = new Vector<TCRMPartyContactMethodBObj>();
			vecPartyContactMethod.add(partyContactMethodBObj);
			
			emailsTable.put(partyContactMethodBObj, vecPartyContactMethod);
			
		} // End of For loop
		
		//Determine the latest Email on the LastUpdateDate
		TCRMPartyContactMethodBObj latestPartyContactMethod = null;
		String latestLastUpdateDate = null;
		
		Enumeration<TCRMPartyContactMethodBObj> e = emailsTable.keys();
		while (e.hasMoreElements()) {
			TCRMPartyContactMethodBObj key = e.nextElement();
			String lastUpdateDate = key.getTCRMContactMethodBObj().getContactMethodLastUpdateDate();
			
			String emailUsageType = key.getContactMethodUsageType();
			
			if(!emailUsageType.equalsIgnoreCase(CTCCommonUtil.getProperty(CTCConstants.EMAIL_PRIMARY_USAGE_TYPE))){
				continue;
			}
			if(latestLastUpdateDate == null || ((lastUpdateDate.compareTo(latestLastUpdateDate)) > 0)){
				latestPartyContactMethod = key;
				latestLastUpdateDate = lastUpdateDate;
			}
		}
		
		/*
		 * (1) Set the latestPartyContactMethod.ContactMethodUsage = Primary Email & rest of them as Alternate Email.
		 * (2) Collapse the CTC Data Source information for each key
		 */
		e = emailsTable.keys();
		while (e.hasMoreElements()) {
			Vector<CTCDataSourceBObj> allCTCDataSourcesList = new Vector<CTCDataSourceBObj>();
			TCRMPartyContactMethodBObj key = e.nextElement();
			if(latestPartyContactMethod != null && latestPartyContactMethod.equals(key)){
				key.setContactMethodUsageType(CTCCommonUtil.getProperty(CTCConstants.EMAIL_PRIMARY_USAGE_TYPE));
			}
			else
			{
				key.setContactMethodUsageType(CTCCommonUtil.getProperty(CTCConstants.EMAIL_ALTERNATE_USAGE_TYPE));
			}
			
			//Get the PartyContactMethodList for the key
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = emailsTable.get(key);
			
			for (TCRMPartyContactMethodBObj partyContactMethodBObj : vecPartyContactMethod) {
				Vector<CTCDataSourceBObj> dataSourceList = ((CTCPartyContactMethodBObjExt)partyContactMethodBObj).getItemsCTCDataSourceBObj();
				if(isVectorNonEmpty(dataSourceList)){
					allCTCDataSourcesList.addAll(dataSourceList);
				}
			}
			
			//Get the surviving CTCDataSources
			Vector<CTCDataSourceBObj> survivingDataSourcesList = collapseActiveCTCDataSources(allCTCDataSourcesList);
			
			//Store the surviving CTCDataSources in the key
			((CTCPartyContactMethodBObjExt)key).getItemsCTCDataSourceBObj().clear();
			((CTCPartyContactMethodBObjExt)key).getItemsCTCDataSourceBObj().addAll(survivingDataSourcesList);
			
		}
		
		
		Vector<TCRMPartyContactMethodBObj> survivingActiveEmailsList = (Vector<TCRMPartyContactMethodBObj>) collectKeys(emailsTable);
	
		/*
		 * Set the surviving active emails into the collapsed party
		 */
		collapsedParty.getItemsTCRMPartyContactMethodBObj().addAll(survivingActiveEmailsList);
		
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseInactivePhoneNumbers(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		/*
		 * Store all the inactive Party ContactMethods from the input parties into allInactivePartyContactMethodsList
		 */
		
		if(!(isVectorNonEmpty(allInactivePartyContactMethodsList))){
			for (TCRMPartyBObj partyBObj : vecParties) {
				Vector<TCRMPartyContactMethodBObj> inactivePartyContactMethodsList = partyComponent
					.getAllPartyContactMethods(partyBObj.getPartyId(), TCRMRecordFilter.INACTIVE, partyBObj.getControl());
				if(isVectorNonEmpty(inactivePartyContactMethodsList)){
					allInactivePartyContactMethodsList.addAll(inactivePartyContactMethodsList);
				}
			}
		}
		
		/*
		 * Store all the inactive Phone Numbers into allInactivePhoneNumbersList
		 */
		Vector<TCRMPartyContactMethodBObj> allInactivePhoneNumbersList = new Vector<TCRMPartyContactMethodBObj>();
		if(isVectorNonEmpty(allInactivePartyContactMethodsList)){
			for (TCRMPartyContactMethodBObj partyContactMethodBObj : allInactivePartyContactMethodsList) {
				String contactMethodType = partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType();
				if(StringUtils.isNonBlank(contactMethodType) 
						&& StringUtils.compareIgnoreCaseWithTrim(contactMethodType, CTCConstants.CONT_METH_TP_PHONE)){ 
					allInactivePhoneNumbersList.add(partyContactMethodBObj);
				}
			}
		}
		
		Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>> inactivePhoneNumbersTable = 
			new Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>>();
		
		boolean found = false;
		for (TCRMPartyContactMethodBObj partyContactMethodBObj : allInactivePhoneNumbersList) {
			found = false;
			
			TCRMContactMethodBObj contactMethodBObj = partyContactMethodBObj.getTCRMContactMethodBObj();
			Enumeration<TCRMPartyContactMethodBObj> e = inactivePhoneNumbersTable.keys();
			while (e.hasMoreElements() && !found) {
				TCRMPartyContactMethodBObj key = e.nextElement();
				TCRMContactMethodBObj keyContactMethod = key.getTCRMContactMethodBObj();
				
				//Compare the attribute ContactMethodUsageType, ReferenceNumber, StartDate & EndDate between key & PartyContactMethod.
				if(!(StringUtils.compareIgnoreCaseWithTrim(
						partyContactMethodBObj.getContactMethodUsageType(), key.getContactMethodUsageType())
						&& StringUtils.compareIgnoreCaseWithTrim(partyContactMethodBObj.getStartDate(), key.getStartDate())
						&& StringUtils.compareIgnoreCaseWithTrim(partyContactMethodBObj.getEndDate(), key.getEndDate())
						&& StringUtils.compareIgnoreCaseWithTrim(contactMethodBObj.getReferenceNumber(), keyContactMethod.getReferenceNumber()))){
					// Attribute did not match. Continue to check with other keys.
					continue;
				}
				
				// All attributes matched.
				Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = inactivePhoneNumbersTable.get(key);
				
				vecPartyContactMethod.add(partyContactMethodBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}
			
			/*
			 * PartyContactMethod did not match any key. Add to the inactivePhoneNumers table
			 */
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = new Vector<TCRMPartyContactMethodBObj>();
			vecPartyContactMethod.add(partyContactMethodBObj);
			
			inactivePhoneNumbersTable.put(partyContactMethodBObj, vecPartyContactMethod);
			
		} // End of FOR loop
		
		Vector< ? extends DWLCommon> survivingInactivePhoneNumbersList = collectKeys(inactivePhoneNumbersTable);
		
		/*
		 * Set the surviving inactive phone numbers into the collapsed party.
		 */
		collapsedParty.getItemsTCRMPartyContactMethodBObj().addAll(survivingInactivePhoneNumbersList);
		
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseActivePhoneNumbers(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		/*
		 * Store all the active Phone Numbers into allActivePhoneNumbersList.
		 */
		Vector<TCRMPartyContactMethodBObj> allActivePhoneNumbersList = new Vector<TCRMPartyContactMethodBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			Vector<TCRMPartyContactMethodBObj> activePartyContactMethodsList = 
				partyBObj.getItemsTCRMPartyContactMethodBObj();
			if(isVectorNonEmpty(activePartyContactMethodsList)){
				//Check if the ContactMethodType is 'Phone Number'
				for (TCRMPartyContactMethodBObj partyContactMethodBObj : activePartyContactMethodsList) {
					String contactMethodType = partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType();
					if(StringUtils.isNonBlank(contactMethodType) 
							&& StringUtils.compareIgnoreCaseWithTrim(contactMethodType, CTCConstants.CONT_METH_TP_PHONE)){ 
						allActivePhoneNumbersList.add(partyContactMethodBObj);
					}
				}
			}
			
		}
		
		// Create a list to store the active phone numbers which may get end dated.
		Vector<TCRMPartyContactMethodBObj> endDatedPhoneNumbersList = new  Vector<TCRMPartyContactMethodBObj>();
		
		Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>> phoneNumbersTable = 
			new Hashtable<TCRMPartyContactMethodBObj, Vector<TCRMPartyContactMethodBObj>>();
		
		boolean found = false;
		for (TCRMPartyContactMethodBObj partyContactMethodBObj : allActivePhoneNumbersList) {
			found = false;
			
			TCRMContactMethodBObj contactMethodBObj = partyContactMethodBObj.getTCRMContactMethodBObj();
			Enumeration<TCRMPartyContactMethodBObj> e = phoneNumbersTable.keys();
			while (e.hasMoreElements() && !found) {
				TCRMPartyContactMethodBObj key = e.nextElement();
				TCRMContactMethodBObj keyContactMethod = key.getTCRMContactMethodBObj();
				
				//Compare the attribute ContactMethodUsageType between key & PartyContactMethod.
				if(!(StringUtils.compareIgnoreCaseWithTrim(
						partyContactMethodBObj.getContactMethodUsageType(), key.getContactMethodUsageType()))){
					// Attribute did not match. Continue to check with other keys.
					continue;
				}
				
				//ContactMethodUsageType matched.
				Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = phoneNumbersTable.get(key);
				
				//Compare the actual phone number value
				if(StringUtils.compareIgnoreCaseWithTrim(
						contactMethodBObj.getReferenceNumber(), keyContactMethod.getReferenceNumber())){
					
					//Compare the StartDate attribute between the key & input PartyAddress
					int comparisonOutput = key.getStartDate().compareTo(partyContactMethodBObj.getStartDate());
					
					if(comparisonOutput > 0){//implies key.Startdate is greater than PartyContactMethod.StartDate
						phoneNumbersTable.remove(key);
						phoneNumbersTable.put(partyContactMethodBObj, vecPartyContactMethod);
					}
				}
				else // Phone Numbers are different.
				{
					//Compare the LastUpdateDate between the key & PartyContactMethod
					int comparisonOutput = keyContactMethod.getContactMethodLastUpdateDate()
						.compareTo(contactMethodBObj.getContactMethodLastUpdateDate());
					
					if(comparisonOutput > 0){ //implies key is the latest.
						partyContactMethodBObj.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
						endDatedPhoneNumbersList.add(partyContactMethodBObj);
					}
					else
					{
						key.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
						endDatedPhoneNumbersList.add(key);
						phoneNumbersTable.remove(key);
						phoneNumbersTable.put(partyContactMethodBObj, vecPartyContactMethod);
					}
					
				}
				
				vecPartyContactMethod.add(partyContactMethodBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}

			/*
			 * If PartyContactMethod did not match any key, add to the phoneNumbersTable
			 */
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = new Vector<TCRMPartyContactMethodBObj>();
			vecPartyContactMethod.add(partyContactMethodBObj);
			
			phoneNumbersTable.put(partyContactMethodBObj, vecPartyContactMethod);
			
		} // End of FOR loop
		
		Vector< ? extends DWLCommon> survivingActivePhoneNumbersList = collectKeys(phoneNumbersTable);
		
		/*
		 * Add the surviving active Phone Numbers + End Dated Phone Numbers to the collapsed party
		 */
		collapsedParty.getItemsTCRMPartyContactMethodBObj().addAll(survivingActivePhoneNumbersList);
		collapsedParty.getItemsTCRMPartyContactMethodBObj().addAll(endDatedPhoneNumbersList);
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseInactiveAddresses(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		/*
		 * Store all the inactive Party Addresses from the input parties into allInactivePartyAddressesList
		 */
		Vector<TCRMPartyAddressBObj> allInactivePartyAddressesList = new Vector<TCRMPartyAddressBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			Vector<TCRMPartyAddressBObj> inactivePartyAddressesList = partyComponent
				.getAllPartyAddresses(partyBObj.getPartyId(), TCRMRecordFilter.INACTIVE, partyBObj.getControl());
			if(isVectorNonEmpty(inactivePartyAddressesList)){
				allInactivePartyAddressesList.addAll(inactivePartyAddressesList);
			}
		}
		
		Hashtable<TCRMPartyAddressBObj, Vector<TCRMPartyAddressBObj>> inactivePartyAddressesTable = 
			new Hashtable<TCRMPartyAddressBObj, Vector<TCRMPartyAddressBObj>>();
		
		boolean found = false;
		for (TCRMPartyAddressBObj partyAddressBObj : allInactivePartyAddressesList) {
			found = false;
			
			TCRMAddressBObj addressBObj = partyAddressBObj.getTCRMAddressBObj();
			Enumeration<TCRMPartyAddressBObj> e = inactivePartyAddressesTable.keys();
			while((e.hasMoreElements()) && (!found)){
				TCRMPartyAddressBObj key = e.nextElement();
				
				TCRMAddressBObj keyAddress = key.getTCRMAddressBObj();
				/*
				 * Compare the attributes - AddressLineOne, AddressLineTwo, City, ProvinceState, PostalCode, StartDate & EndDate 
				 * between the key & input PartyAddress
				 */
				if(!(StringUtils.compareIgnoreCaseWithTrim(addressBObj.getAddressLineOne(), keyAddress.getAddressLineOne())
						&& StringUtils.compareIgnoreCaseWithTrim(addressBObj.getAddressLineTwo(), keyAddress.getAddressLineTwo())
						&& StringUtils.compareIgnoreCaseWithTrim(addressBObj.getCity(), keyAddress.getCity())
						&& StringUtils.compareIgnoreCaseWithTrim(addressBObj.getProvinceStateType(), keyAddress.getProvinceStateType())
						&& StringUtils.compareIgnoreCaseWithTrim(addressBObj.getZipPostalCode(), keyAddress.getZipPostalCode())
						&& StringUtils.compareIgnoreCaseWithTrim(partyAddressBObj.getStartDate(), key.getStartDate())
						&& StringUtils.compareIgnoreCaseWithTrim(partyAddressBObj.getEndDate(), key.getEndDate()))){
					// Attributes did not match. Continue to check with other keys.
					continue;
				}
				
				// Attributes matched
				Vector<TCRMPartyAddressBObj> vecPartyAddress = inactivePartyAddressesTable.get(key);
				
				vecPartyAddress.add(partyAddressBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}
			
			/*
			 * If PartyAddress did not match any key, add to the inactivePartyAddressesTable
			 */
			Vector<TCRMPartyAddressBObj> vecPartyAddress = new Vector<TCRMPartyAddressBObj>();
			vecPartyAddress.add(partyAddressBObj);
			
			inactivePartyAddressesTable.put(partyAddressBObj, vecPartyAddress);
			
		} // End of For loop
		
		Vector< ? extends DWLCommon> survivingList = collectKeys(inactivePartyAddressesTable);
		
		/*
		 * Set the surviving inactive party addresses into the collapsed party.
		 */
		collapsedParty.getItemsTCRMPartyAddressBObj().addAll(survivingList);
		
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseActiveAddresses(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		/*
		 * Store all the active party addresses from the input parties into allActivePartyAddressesList
		 */
		Vector<TCRMPartyAddressBObj> allActivePartyAddressesList = new Vector<TCRMPartyAddressBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			Vector<TCRMPartyAddressBObj> partyAddressesList = partyBObj.getItemsTCRMPartyAddressBObj();
			if(isVectorNonEmpty(partyAddressesList)){
				allActivePartyAddressesList.addAll(partyAddressesList);
			}
		}
		
		/*
		 * Get the CTCDataSource information for each active party address
		 */
		for (TCRMPartyAddressBObj partyAddressBObj : allActivePartyAddressesList) {
			if(partyAddressBObj instanceof CTCPartyAddressBObjExt){
				((CTCPartyAddressBObjExt)partyAddressBObj).getRecord();
			}
		}
		
		Hashtable<TCRMPartyAddressBObj, Vector<TCRMPartyAddressBObj>> partyAddressTable = 
			new Hashtable<TCRMPartyAddressBObj, Vector<TCRMPartyAddressBObj>>();
		
		boolean found = false;
		for (TCRMPartyAddressBObj partyAddressBObj : allActivePartyAddressesList) {
			found = false;
			
			TCRMAddressBObj addressBObj = partyAddressBObj.getTCRMAddressBObj();
			Enumeration<TCRMPartyAddressBObj> e = partyAddressTable.keys();
			while((e.hasMoreElements()) && (!found)){
				TCRMPartyAddressBObj key = e.nextElement();
				TCRMAddressBObj keyAddress = key.getTCRMAddressBObj();
				/*
				 * Compare the attributes - AddressLineOne, AddressLineTwo, City, ProvinceState, PostalCode
				 * between the key & input PartyAddress
				 */
				if(!(StringUtils.compareIgnoreCaseWithTrim(addressBObj.getAddressLineOne(), keyAddress.getAddressLineOne())
						&& StringUtils.compareIgnoreCaseWithTrim(addressBObj.getAddressLineTwo(), keyAddress.getAddressLineTwo())
						&& StringUtils.compareIgnoreCaseWithTrim(addressBObj.getCity(), keyAddress.getCity())
						&& StringUtils.compareIgnoreCaseWithTrim(addressBObj.getProvinceStateType(), keyAddress.getProvinceStateType())
						&& StringUtils.compareIgnoreCaseWithTrim(addressBObj.getZipPostalCode(), keyAddress.getZipPostalCode()))){
					// Attributes did not match. Continue to check with other keys.
					continue;
				}
				
				// Attributes Matched.
				Vector<TCRMPartyAddressBObj> vecPartyAddress = partyAddressTable.get(key);
				
				//Compare the StartDate attribute between the key & input PartyAddress
				int comparisonOutput = key.getStartDate().compareTo(partyAddressBObj.getStartDate());
				
				if(comparisonOutput > 0){ //implies key.Startdate is greater than PartyAddress.StartDate
					
					partyAddressTable.remove(key);
					partyAddressTable.put(partyAddressBObj, vecPartyAddress);
				}
				
				vecPartyAddress.add(partyAddressBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}
			
			/*
			 * If PartyAddress did not match any key, add to the partyAddressTable
			 */
			Vector<TCRMPartyAddressBObj> vecPartyAddress = new Vector<TCRMPartyAddressBObj>();
			vecPartyAddress.add(partyAddressBObj);
			
			partyAddressTable.put(partyAddressBObj, vecPartyAddress);
			
		} // End of For loop
		
		//Determine the latest Party Address based on the LastUpdateDate
		TCRMPartyAddressBObj latestPartyAddress = null;
		String latestLastUpdateDate = null;
		
		Enumeration<TCRMPartyAddressBObj> e = partyAddressTable.keys();
		while (e.hasMoreElements()) {
			TCRMPartyAddressBObj key = e.nextElement();
			String lastUpdateDate = key.getAddressGroupLastUpdateDate();
			
			String addressUsageType = key.getAddressUsageType();
			
			if(!addressUsageType.equalsIgnoreCase(CTCCommonUtil.getProperty(CTCConstants.ADDRESS_PRIMARY_USAGE_TYPE))){
				continue;
			}
			if(latestLastUpdateDate == null || ((lastUpdateDate.compareTo(latestLastUpdateDate)) > 0)){
				latestPartyAddress = key;
				latestLastUpdateDate = lastUpdateDate;
			}
		}
		
		/*
		 * (1) Set the latestPartyAddress.AddressUsage = Primary & rest of them as Alternate.
		 *  (2) Collapse the CTC Data Source information for each key
		 */
		e = partyAddressTable.keys();
		while (e.hasMoreElements()) {
			Vector<CTCDataSourceBObj> allCTCDataSourcesList = new Vector<CTCDataSourceBObj>();
			TCRMPartyAddressBObj key = e.nextElement();
			if(latestPartyAddress != null && latestPartyAddress.equals(key)){
				key.setAddressUsageType(CTCCommonUtil.getProperty(CTCConstants.ADDRESS_PRIMARY_USAGE_TYPE));
			}
			else
			{
				key.setAddressUsageType(CTCCommonUtil.getProperty(CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE));
			}
			
			//Get the PartyAddressList for the key
			Vector<TCRMPartyAddressBObj> vecPartyAddress = partyAddressTable.get(key);
			
			for (TCRMPartyAddressBObj partyAddressBObj : vecPartyAddress) {
				Vector<CTCDataSourceBObj> dataSourceList = ((CTCPartyAddressBObjExt)partyAddressBObj).getItemsCTCDataSourceBObj();
				if(isVectorNonEmpty(dataSourceList)){
					allCTCDataSourcesList.addAll(dataSourceList);
				}
			}
			
			//Get the surviving CTCDataSources
			Vector<CTCDataSourceBObj> survivingDataSourcesList = collapseActiveCTCDataSources(allCTCDataSourcesList);
			
			//Store the surviving CTCDataSources in the key
			((CTCPartyAddressBObjExt)key).getItemsCTCDataSourceBObj().clear();
			((CTCPartyAddressBObjExt)key).getItemsCTCDataSourceBObj().addAll(survivingDataSourcesList);
			
		}
		
		
		Vector<TCRMPartyAddressBObj> survivingList = (Vector<TCRMPartyAddressBObj>) collectKeys(partyAddressTable);
	
		/*
		 * Set the surviving active party addresses into the collapsed party
		 */
		collapsedParty.getItemsTCRMPartyAddressBObj().addAll(survivingList);
	}

	/**
	 * 
	 * @param allActiveCTCDataSourcesList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Vector<CTCDataSourceBObj> collapseActiveCTCDataSources(
			Vector<CTCDataSourceBObj> allActiveCTCDataSourcesList) {
		Hashtable<CTCDataSourceBObj, Vector<CTCDataSourceBObj>> ctcDataSourceTable = 
			new Hashtable<CTCDataSourceBObj, Vector<CTCDataSourceBObj>>();
		
		boolean found = false;
		for (CTCDataSourceBObj dataSourceBObj : allActiveCTCDataSourcesList) {
			found = false;
			
			Enumeration<CTCDataSourceBObj> e = ctcDataSourceTable.keys();
			while (e.hasMoreElements() && !found) {
				CTCDataSourceBObj key = e.nextElement();
				
				/*
				 * Compare the attributes - AdminSystemType, EntityName & AttributeName
				 */
				if(!(StringUtils.compareIgnoreCaseWithTrim(key.getAdminSystemType(), dataSourceBObj.getAdminSystemType())
						&& StringUtils.compareIgnoreCaseWithTrim(key.getEntityName(), dataSourceBObj.getEntityName())
						&& StringUtils.compareIgnoreCaseWithTrim(key.getAttributeName(), dataSourceBObj.getAttributeName()))){
					continue;
				}
				
				//Attributes matched.
				
				Vector<CTCDataSourceBObj> vecDataSource = ctcDataSourceTable.get(key);
				
				//Compare the start date between the key & datasourceBObj
				int comparisonOutput = key.getStartDate().compareTo(dataSourceBObj.getStartDate());
				
				if(comparisonOutput > 0){ //implies key.StartDate is greater than datasource.StartDate
					ctcDataSourceTable.remove(key);
					ctcDataSourceTable.put(dataSourceBObj, vecDataSource);
				}
				
				vecDataSource.add(dataSourceBObj);
				
				found = true;
				
			}
			
			if(found){
				continue;
			}
			
			/*
			 * If dataSourceBObj did not match any key, add to the ctcDataSourceTable
			 */
			Vector<CTCDataSourceBObj> vecDataSource = new Vector<CTCDataSourceBObj>();
			vecDataSource.add(dataSourceBObj);
			
			ctcDataSourceTable.put(dataSourceBObj, vecDataSource);
		} // end of FOR loop
		
		Vector<CTCDataSourceBObj> survivingList = (Vector<CTCDataSourceBObj>) collectKeys(ctcDataSourceTable);
		
		return survivingList;
	}

	/**
	 * 
	 * @param collapsedPerson
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseInactivePersonNames(TCRMPersonBObj collapsedPerson, Vector<TCRMPartyBObj> vecParties) throws Exception{
		
		/*
		 * Store all the inactive PersonNames from the input parties into allInactivePersonNamesList
		 */
		Vector<TCRMPersonNameBObj> allInactivePersonNamesList = new Vector<TCRMPersonNameBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			Vector<TCRMPersonNameBObj> inactivePersonNameList = ((IPerson)partyComponent)
				.getAllPersonNames(partyBObj.getPartyId(), TCRMRecordFilter.INACTIVE, partyBObj.getControl());
			if(isVectorNonEmpty(inactivePersonNameList)){
				allInactivePersonNamesList.addAll(inactivePersonNameList);
			}
		}
		
		Hashtable<TCRMPersonNameBObj, Vector<TCRMPersonNameBObj>> inactivePersonNamesTable = 
			new Hashtable<TCRMPersonNameBObj, Vector<TCRMPersonNameBObj>>();
		
		boolean found = false;
		for (TCRMPersonNameBObj personNameBObj : allInactivePersonNamesList) {
			found = false;
			Enumeration<TCRMPersonNameBObj> e = inactivePersonNamesTable.keys();
			while((e.hasMoreElements()) && (!found)){
				TCRMPersonNameBObj key = e.nextElement();
				
				/*
				 * Compare the attributes - GivenNameOne, LastName, GenerationType, StartDate & EndDate
				 * between the key & input personName
				 */
				if(!(StringUtils.compareIgnoreCaseWithTrim(personNameBObj.getGivenNameOne(), key.getGivenNameOne())
						&& StringUtils.compareIgnoreCaseWithTrim(personNameBObj.getLastName(), key.getLastName())
						&& StringUtils.compareIgnoreCaseWithTrim(personNameBObj.getGenerationType(), key.getGenerationType())
						&& StringUtils.compareIgnoreCaseWithTrim(personNameBObj.getStartDate(), key.getStartDate())
						&& StringUtils.compareIgnoreCaseWithTrim(personNameBObj.getEndDate(), key.getEndDate()))){
					// Attributes did not match. Continue to check with other keys.
					continue;
				}
				
				// Attributes matched
				Vector<TCRMPersonNameBObj> vecPersonName = inactivePersonNamesTable.get(key);
				
				vecPersonName.add(personNameBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}
			
			/*
			 * If PersonName did not match any key, add to the inactivePersonNamesTable
			 */
			Vector<TCRMPersonNameBObj> vecPersonName = new Vector<TCRMPersonNameBObj>();
			vecPersonName.add(personNameBObj);
			
			inactivePersonNamesTable.put(personNameBObj, vecPersonName);
			
		} // End of For loop
		
		Vector< ? extends DWLCommon> survivingList = collectKeys(inactivePersonNamesTable);
		
		/*
		 * Set the surviving inactive names into the collapsed party.
		 */
		collapsedPerson.getItemsTCRMPersonNameBObj().addAll(survivingList);
		
	}

	/**
	 * 
	 * @param collapsedPerson
	 * @param vecParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void collapseActivePersonNames(TCRMPersonBObj collapsedPerson, Vector<TCRMPartyBObj> vecParties) throws Exception{

		/*
		 * Store all the active PersonNames from the input parties into allActivePersonNamesList.
		 */
		Vector<TCRMPersonNameBObj> allActivePersonNamesList = new Vector<TCRMPersonNameBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			TCRMPersonBObj personBObj = (TCRMPersonBObj)partyBObj;
			Vector<TCRMPersonNameBObj> personNameList = personBObj.getItemsTCRMPersonNameBObj();
			if(isVectorNonEmpty(personNameList)){
				allActivePersonNamesList.addAll(personNameList);
			}
		}
		
		//Get the CTCDataSource information for each PersonName
		for (TCRMPersonNameBObj personNameBObj : allActivePersonNamesList) {
			if(personNameBObj instanceof CTCPersonNameBObjExt){
				((CTCPersonNameBObjExt)personNameBObj).getRecord();
			}
		}
		
		Hashtable<TCRMPersonNameBObj, Vector<TCRMPersonNameBObj>> activePersonNameTable = 
			new Hashtable<TCRMPersonNameBObj, Vector<TCRMPersonNameBObj>>();
		
		boolean found = false;
		for (TCRMPersonNameBObj personNameBObj : allActivePersonNamesList) {
			found = false;
			
			Enumeration<TCRMPersonNameBObj> e = activePersonNameTable.keys();
			while (e.hasMoreElements() && !found) {
				TCRMPersonNameBObj key = e.nextElement();
				
				/*
				 * Compare the attributes - GivenNameOne, LastName, GenerationType
				 * between the key & input personName
				 */
				if(!(StringUtils.compareIgnoreCaseWithTrim(personNameBObj.getGivenNameOne(), key.getGivenNameOne())
						&& StringUtils.compareIgnoreCaseWithTrim(personNameBObj.getLastName(), key.getLastName())
						&& StringUtils.compareIgnoreCaseWithTrim(personNameBObj.getGenerationType(), key.getGenerationType()))){
					// Attributes did not match. Continue to check with other keys.
					continue;
				}
				
				// Attributes matched
				Vector<TCRMPersonNameBObj> vecPersonName = activePersonNameTable.get(key);
				
				//Compare the StartDate attribute between the key & input PersonName
				int comparisonOutput = key.getStartDate().compareTo(personNameBObj.getStartDate());
				
				if(comparisonOutput > 0){ //implies key.Startdate is greater than PartyLOBRelationship.StartDate
					
					activePersonNameTable.remove(key);
					activePersonNameTable.put(personNameBObj, vecPersonName);
				}
				
				vecPersonName.add(personNameBObj);
				
				found = true;
				
			}
			
			if(found){
				continue;
			}
			
			/*
			 * If PersonName did not match any key, add the PersonName to the activePersonNameTable
			 */
			Vector<TCRMPersonNameBObj> vecPersonName = new Vector<TCRMPersonNameBObj>();
			vecPersonName.add(personNameBObj);
			
			activePersonNameTable.put(personNameBObj, vecPersonName);
			
		} // End of FOR loop

		/*
		 * (a) Collapse GivenNameTwo, GivenNameThree, GivenNameFour, PrefixType, PrefixDescription, Suffix
		 * (b) Determine the latest Name based on the latest LastUpdate. 
		 * 
		 */
		TCRMPersonNameBObj latestPersonName = null;
		String latestLastUpdateDate = null;

		Enumeration<TCRMPersonNameBObj> e = activePersonNameTable.keys();
		
		while (e.hasMoreElements()) {
			TCRMPersonNameBObj key = e.nextElement();
			Vector<TCRMPersonNameBObj> vecPersonName = activePersonNameTable.get(key);
			
			vecPersonName.remove(key);
			
			if(StringUtils.isBlank(key.getGivenNameTwo())){
				collapseGivenNameTwo(key, vecPersonName);
			}
			
			if(StringUtils.isBlank(key.getGivenNameThree())){
				collapseGivenNameThree(key, vecPersonName);
			}

			if(StringUtils.isBlank(key.getGivenNameFour())){
				collapseGivenNameFour(key, vecPersonName);
			}

			if(StringUtils.isBlank(key.getPrefixType())){
				collapsePrefixType(key, vecPersonName);
			}

			if(StringUtils.isBlank(key.getPrefixDescription())){
				collapsePrefixDescription(key, vecPersonName);
			}

			if(StringUtils.isBlank(key.getSuffix())){
				collapseSuffix(key, vecPersonName);
			}
			
			String lastUpdateDate = key.getPersonNameLastUpdateDate();
			
			String nameUsageType = key.getNameUsageType();
			
			if(!nameUsageType.equalsIgnoreCase(CTCCommonUtil.getProperty(CTCConstants.PERSONNAME_PRIMARY_USAGE_TYPE))){
				continue;
			}
			if(latestLastUpdateDate == null || ((lastUpdateDate.compareTo(latestLastUpdateDate)) > 0)){
				latestPersonName = key;
				latestLastUpdateDate = lastUpdateDate;
			}
		}
		
		/*
		 * (a)Set the latestPersonName.NameUsage = Primary Name. Set the rest PersonNames to Alternate
		 * (b) Collapse the CTC Data Source information for each key
		 */
		e = activePersonNameTable.keys();

		while (e.hasMoreElements()) {
			Vector<CTCDataSourceBObj> allCTCDataSourcesList = new Vector<CTCDataSourceBObj>();
			TCRMPersonNameBObj key = e.nextElement();
			if(key.equals(latestPersonName)){
				key.setNameUsageType(CTCCommonUtil.getProperty(CTCConstants.PERSONNAME_PRIMARY_USAGE_TYPE));
			}
			else{
				key.setNameUsageType(CTCCommonUtil.getProperty(CTCConstants.PERSONNAME_ALTERNATE_USAGE_TYPE));
			}
			
			//Get the PersonNameList for the key
			Vector<TCRMPersonNameBObj> vecPersonName = activePersonNameTable.get(key);
			
			for (TCRMPersonNameBObj personNameBObj : vecPersonName) {
				Vector<CTCDataSourceBObj> dataSourceList = ((CTCPersonNameBObjExt)personNameBObj).getItemsCTCDataSourceBObj();
				if(isVectorNonEmpty(dataSourceList)){
					allCTCDataSourcesList.addAll(dataSourceList);
				}
			}
			
			Vector<CTCDataSourceBObj> dataSourceListFromKey = ((CTCPersonNameBObjExt)key).getItemsCTCDataSourceBObj();
			if(isVectorNonEmpty(dataSourceListFromKey)){
				allCTCDataSourcesList.addAll(dataSourceListFromKey);
			}
			
			
			//Get the surviving CTCDataSources
			Vector<CTCDataSourceBObj> survivingDataSourcesList = collapseActiveCTCDataSources(allCTCDataSourcesList);
			
			//Store the surviving CTCDataSources in the key
			((CTCPersonNameBObjExt)key).getItemsCTCDataSourceBObj().clear();
			((CTCPersonNameBObjExt)key).getItemsCTCDataSourceBObj().addAll(survivingDataSourcesList);
			
		}
		
		Vector< ? extends DWLCommon> survivingActivePersonNamesList = collectKeys(activePersonNameTable);
		
		/*
		 * Set the surviving active person names into the collapsed party.
		 */
		collapsedPerson.getItemsTCRMPersonNameBObj().addAll(survivingActivePersonNamesList);
		
	}

	@SuppressWarnings("unchecked")
	private void collapseLOB(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		
		/*
		 * Store all the active LOB from the input parties into allActiveLOBRelList
		 */
		Vector<TCRMPartyLobRelationshipBObj> allActiveLOBRelList = new Vector<TCRMPartyLobRelationshipBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			Vector<TCRMPartyLobRelationshipBObj> partyLOBList = partyBusinessServicesComponent
				.getAllPartyLobRelationships(partyBObj.getPartyId(), TCRMRecordFilter.ACTIVE, partyBObj.getControl());
			if(isVectorNonEmpty(partyLOBList)){
				allActiveLOBRelList.addAll(partyLOBList);
			}
		}
		
		Hashtable<TCRMPartyLobRelationshipBObj, Vector<TCRMPartyLobRelationshipBObj>> partyLOBTable = 
			new Hashtable<TCRMPartyLobRelationshipBObj, Vector<TCRMPartyLobRelationshipBObj>>();
		
		boolean found = false;
		for (TCRMPartyLobRelationshipBObj partyLobRelationshipBObj : allActiveLOBRelList) {
			found = false;
			
			Enumeration<TCRMPartyLobRelationshipBObj> e = partyLOBTable.keys();
			while((e.hasMoreElements()) && (!found)){
				TCRMPartyLobRelationshipBObj key = e.nextElement();
				
				/*
				 * Compare the RelatedLOBType between the key & input PartyLOBRelationship
				 */
				if(!StringUtils.compareIgnoreCaseWithTrim(
						partyLobRelationshipBObj.getRelatedLobType(), key.getRelatedLobType())){
					// Attributes did not match. Continue to check with other keys.
					continue;
				}
				
				// Attributes Matched.
				Vector<TCRMPartyLobRelationshipBObj> vecLOB = partyLOBTable.get(key);
				
				//Compare the StartDate attribute between the key & input PartyLOBRelationship
				int comparisonOutput = key.getStartDate().compareTo(partyLobRelationshipBObj.getStartDate());
				
				if(comparisonOutput > 0){ //implies key.Startdate is greater than PartyLOBRelationship.StartDate
					
					partyLOBTable.remove(key);
					partyLOBTable.put(partyLobRelationshipBObj, vecLOB);
				}
				
				vecLOB.add(partyLobRelationshipBObj);
				
				found = true;
			}
			
			if(found){
				continue;
			}
			
			/*
			 * If PartyLOBRelationship did not match any key, add to the partyLOBTable
			 */
			Vector<TCRMPartyLobRelationshipBObj> vecLOB = new Vector<TCRMPartyLobRelationshipBObj>();
			vecLOB.add(partyLobRelationshipBObj);
			
			partyLOBTable.put(partyLobRelationshipBObj, vecLOB);
			
		} // End of For loop
		
		Vector< ? extends DWLCommon> survivingList = collectKeys(partyLOBTable);
		
		/*
		 * Set the surviving LOBs into the collapsed party.
		 */
		collapsedParty.getItemsTCRMPartyLobRelationshipBObj().addAll(survivingList);
	}

	@SuppressWarnings("unchecked")
	private void collapseCustomerIdentifiers(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception{
		Vector<TCRMAdminContEquivBObj> survivingCustomerIdentifiersList = new Vector<TCRMAdminContEquivBObj>();
		for (TCRMPartyBObj partyBObj : vecParties) {
			Vector<TCRMAdminContEquivBObj> customerIdentifierList = partyBObj.getItemsTCRMAdminContEquivBObj();
			if(isVectorNonEmpty(customerIdentifierList)){
				survivingCustomerIdentifiersList.addAll(customerIdentifierList);
			}
		}
		
		/*
		 * Set the surviving customer identifiers to the collapsed party.
		 */
		collapsedParty.getItemsTCRMAdminContEquivBObj().addAll(survivingCustomerIdentifiersList);
		
	}

	/**
	 * 
	 * @param collapsedParty
	 * @param vecParties
	 * @throws Exception
	 */
	private void collapseLanguagePreference(TCRMPartyBObj collapsedParty, Vector<TCRMPartyBObj> vecParties) throws Exception {
		String survivingPreferredLanguage = null;
		String latestLastUpdateDate = null;
		
		for (TCRMPartyBObj partyBObj : vecParties) {
			String preferredLanguage = partyBObj.getPreferredLanguageType();
			String lastUpdateDate = partyBObj.getPartyLastUpdateDate();
			
			if(StringUtils.isNonBlank(preferredLanguage)){
				if(StringUtils.isBlank(survivingPreferredLanguage)){
					survivingPreferredLanguage = preferredLanguage;
					latestLastUpdateDate = lastUpdateDate;
				}
				else
				{
					//Compare the LastUpdateDate with LatestLastUpdateDate
					int comparisonOutput = lastUpdateDate.compareTo(latestLastUpdateDate);
					if(comparisonOutput > 0){
						survivingPreferredLanguage = preferredLanguage;
						latestLastUpdateDate = lastUpdateDate;
					}
				}
			}
		}
		
		/*
		 * Set the surviving preferred language to the collapsed party
		 */
		collapsedParty.setPreferredLanguageType(survivingPreferredLanguage);
	
	}

	/**
	 * 
	 * @param key
	 * @param vecPersonName
	 */
	private void collapseGivenNameTwo(TCRMPersonNameBObj key, Vector<TCRMPersonNameBObj> vecPersonName){
		String survivingGivenNameTwo = null;
		String latestLastUpdateDate = null;
		
		for (TCRMPersonNameBObj personNameBObj : vecPersonName) {
			String givenNameTwo = personNameBObj.getGivenNameTwo();
			String lastUpdateDate = personNameBObj.getPersonNameLastUpdateDate();
			
			if(StringUtils.isNonBlank(givenNameTwo)){
				if(StringUtils.isBlank(survivingGivenNameTwo)){
					survivingGivenNameTwo = givenNameTwo;
					latestLastUpdateDate = lastUpdateDate;
				}
				else
				{
					//Compare the LastUpdateDate with LatestLastUpdateDate
					int comparisonOutput = lastUpdateDate.compareTo(latestLastUpdateDate);
					if(comparisonOutput > 0){
						survivingGivenNameTwo = givenNameTwo;
						latestLastUpdateDate = lastUpdateDate;
					}
				}
			}
		}
		
		key.setGivenNameTwo(survivingGivenNameTwo);
	}

	/**
	 * 
	 * @param key
	 * @param vecPersonName
	 */
	private void collapseGivenNameThree(TCRMPersonNameBObj key, Vector<TCRMPersonNameBObj> vecPersonName){
		String survivingGivenNameThree = null;
		String latestLastUpdateDate = null;
		
		for (TCRMPersonNameBObj personNameBObj : vecPersonName) {
			String givenNameThree = personNameBObj.getGivenNameThree();
			String lastUpdateDate = personNameBObj.getPersonNameLastUpdateDate();
			
			if(StringUtils.isNonBlank(givenNameThree)){
				if(StringUtils.isBlank(survivingGivenNameThree)){
					survivingGivenNameThree = givenNameThree;
					latestLastUpdateDate = lastUpdateDate;
				}
				else
				{
					//Compare the LastUpdateDate with LatestLastUpdateDate
					int comparisonOutput = lastUpdateDate.compareTo(latestLastUpdateDate);
					if(comparisonOutput > 0){
						survivingGivenNameThree = givenNameThree;
						latestLastUpdateDate = lastUpdateDate;
					}
				}
			}
		}
		
		key.setGivenNameThree(survivingGivenNameThree);
	}

	/**
	 * 
	 * @param key
	 * @param vecPersonName
	 */
	private void collapseGivenNameFour(TCRMPersonNameBObj key, Vector<TCRMPersonNameBObj> vecPersonName){
		String survivingGivenNameFour = null;
		String latestLastUpdateDate = null;
		
		for (TCRMPersonNameBObj personNameBObj : vecPersonName) {
			String givenNameFour = personNameBObj.getGivenNameFour();
			String lastUpdateDate = personNameBObj.getPersonNameLastUpdateDate();
			
			if(StringUtils.isNonBlank(givenNameFour)){
				if(StringUtils.isBlank(survivingGivenNameFour)){
					survivingGivenNameFour = givenNameFour;
					latestLastUpdateDate = lastUpdateDate;
				}
				else
				{
					//Compare the LastUpdateDate with LatestLastUpdateDate
					int comparisonOutput = lastUpdateDate.compareTo(latestLastUpdateDate);
					if(comparisonOutput > 0){
						survivingGivenNameFour = givenNameFour;
						latestLastUpdateDate = lastUpdateDate;
					}
				}
			}
		}
		
		key.setGivenNameFour(survivingGivenNameFour);
	}

	
	/**
	 * 
	 * @param key
	 * @param vecPersonName
	 */
	private void collapsePrefixType(TCRMPersonNameBObj key, Vector<TCRMPersonNameBObj> vecPersonName){
		String survivingPrefixType = null;
		String latestLastUpdateDate = null;
		
		for (TCRMPersonNameBObj personNameBObj : vecPersonName) {
			String prefixType = personNameBObj.getPrefixType();
			String lastUpdateDate = personNameBObj.getPersonNameLastUpdateDate();
			
			if(StringUtils.isNonBlank(prefixType)){
				if(StringUtils.isBlank(survivingPrefixType)){
					survivingPrefixType = prefixType;
					latestLastUpdateDate = lastUpdateDate;
				}
				else
				{
					//Compare the LastUpdateDate with LatestLastUpdateDate
					int comparisonOutput = lastUpdateDate.compareTo(latestLastUpdateDate);
					if(comparisonOutput > 0){
						survivingPrefixType = prefixType;
						latestLastUpdateDate = lastUpdateDate;
					}
				}
			}
		}
		
		key.setPrefixType(survivingPrefixType);
	}

	
	/**
	 * 
	 * @param key
	 * @param vecPersonName
	 */
	private void collapsePrefixDescription(TCRMPersonNameBObj key, Vector<TCRMPersonNameBObj> vecPersonName){
		String survivingPrefixDescription = null;
		String latestLastUpdateDate = null;
		
		for (TCRMPersonNameBObj personNameBObj : vecPersonName) {
			String prefixDescription = personNameBObj.getPrefixDescription();
			String lastUpdateDate = personNameBObj.getPersonNameLastUpdateDate();
			
			if(StringUtils.isNonBlank(prefixDescription)){
				if(StringUtils.isBlank(survivingPrefixDescription)){
					survivingPrefixDescription = prefixDescription;
					latestLastUpdateDate = lastUpdateDate;
				}
				else
				{
					//Compare the LastUpdateDate with LatestLastUpdateDate
					int comparisonOutput = lastUpdateDate.compareTo(latestLastUpdateDate);
					if(comparisonOutput > 0){
						survivingPrefixDescription = prefixDescription;
						latestLastUpdateDate = lastUpdateDate;
					}
				}
			}
		}
		
		key.setPrefixDescription(survivingPrefixDescription);
	}

	
	/**
	 * 
	 * @param key
	 * @param vecPersonName
	 */
	private void collapseSuffix(TCRMPersonNameBObj key, Vector<TCRMPersonNameBObj> vecPersonName){
		String survivingSuffix = null;
		String latestLastUpdateDate = null;
		
		for (TCRMPersonNameBObj personNameBObj : vecPersonName) {
			String suffix = personNameBObj.getSuffix();
			String lastUpdateDate = personNameBObj.getPersonNameLastUpdateDate();
			
			if(StringUtils.isNonBlank(suffix)){
				if(StringUtils.isBlank(survivingSuffix)){
					survivingSuffix = suffix;
					latestLastUpdateDate = lastUpdateDate;
				}
				else
				{
					//Compare the LastUpdateDate with LatestLastUpdateDate
					int comparisonOutput = lastUpdateDate.compareTo(latestLastUpdateDate);
					if(comparisonOutput > 0){
						survivingSuffix = suffix;
						latestLastUpdateDate = lastUpdateDate;
					}
				}
			}
		}
		
		key.setSuffix(survivingSuffix);
	}
	
	/**
	 * 
	 * @param collapsedPerson
	 * @param vecParties
	 * @throws Exception
	 */
	private void collapseGender(TCRMPersonBObj collapsedPerson, Vector<TCRMPartyBObj> vecParties) throws Exception{
		String survivingGender = null;
		String latestLastUpdateDate = null;
		
		for (TCRMPartyBObj partyBObj : vecParties) {
			TCRMPersonBObj inputPerson = (TCRMPersonBObj)partyBObj;
			String gender = inputPerson.getGenderType();
			String lastUpdateDate = inputPerson.getPersonLastUpdateDate();
			
			if(StringUtils.isNonBlank(gender)){
				if(StringUtils.isBlank(survivingGender)){
					survivingGender = gender;
					latestLastUpdateDate = lastUpdateDate;
				}
				else
				{
					//Compare the LastUpdateDate with LatestLastUpdateDate
					int comparisonOutput = lastUpdateDate.compareTo(latestLastUpdateDate);
					if(comparisonOutput > 0){
						survivingGender = gender;
						latestLastUpdateDate = lastUpdateDate;
					}
				}
			}
		}
		
		/*
		 * Set the surviving gender to the collapsed person
		 */
		collapsedPerson.setGenderType(survivingGender);
	}

	/**
	 * 
	 * @param collapsedPerson
	 * @param vecParties
	 * @throws Exception
	 */
	private void collapseBirthDate(TCRMPersonBObj collapsedPerson, Vector<TCRMPartyBObj> vecParties) throws Exception {
		String survivingBirthDate = null;
		for (TCRMPartyBObj partyBObj : vecParties) {
			TCRMPersonBObj inputPerson = (TCRMPersonBObj)partyBObj;
			String birthDate = inputPerson.getBirthDate();
			if(StringUtils.isNonBlank(birthDate)){
				if(StringUtils.isBlank(survivingBirthDate)){
					survivingBirthDate = birthDate;
				}
				else
				{
					if(StringUtils.compareIgnoreCaseWithTrim(birthDate, survivingBirthDate)){
						// Both the birthDate are same. 
					}
					else // BirthDate are different. This is not allowed.
					{
		                TCRMExceptionUtils.throwTCRMException(null, new TCRMException(), null,
		                        DWLStatus.FATAL, TCRMCoreComponentID.PARTY_COMPONENT, TCRMErrorCode.DATA_INVALID_ERROR,
		                        CTCErrorConstants.COLLAPSE_NOT_ALLOWED_DUE_TO_BIRTHDATE_MISMATCH, collapsedPerson.getControl(), null);
					}
				}
			}
		}
		
		/*
		 * Set the surviving birth date to the collapsed person
		 */
		collapsedPerson.setBirthDate(survivingBirthDate);
	}
	
	/**
	 * 
	 * @param vector
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean isVectorNonEmpty(Vector vector){
		if(vector != null & vector.size() > 0){
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * @param hashtable
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Vector<? extends DWLCommon> collectKeys(Hashtable hashtable){
		Vector<DWLCommon> vecKeys = new Vector<DWLCommon>();
		Enumeration<? extends DWLCommon> e = hashtable.keys();
		while (e.hasMoreElements()) {
			DWLCommon common = (DWLCommon) e.nextElement();
			
			/*
			 * Set the SourceIdentifierType = MDM for PersonName, PartyAddress, PartyContactMethod 
			 */
			if(common instanceof TCRMPersonNameBObj){
				((TCRMPersonNameBObj)common).setSourceIdentifierType(CTCConstants.SOURCE_IDENTIFIER_MDM); 
			}
			else if(common instanceof TCRMPartyAddressBObj){
				((TCRMPartyAddressBObj)common).setSourceIdentifierType(CTCConstants.SOURCE_IDENTIFIER_MDM);
			}
			else if(common instanceof TCRMPartyContactMethodBObj){
				((TCRMPartyContactMethodBObj)common).setSourceIdentifierType(CTCConstants.SOURCE_IDENTIFIER_MDM); 
			}
			vecKeys.add(common);
		}
		
		return vecKeys;
	}
}
