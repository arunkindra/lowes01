package com.ctc.mdm.sdp.collapse;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.coreParty.component.TCRMPartyAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyContactMethodBObj;
import com.dwl.tcrm.financial.component.TCRMContractPartyRoleBObj;
import com.dwl.tcrm.financial.component.TCRMContractRoleLocationBObj;
import com.dwl.tcrm.financial.constant.TCRMFinancialPropertyKeys;
import com.dwl.tcrm.financial.interfaces.IContract;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;

public class CTCFSCollapsePartiesHelper {

	/**
	 * 
	 * @param vecParties
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public TCRMPartyBObj collapsePartiesUsingCTCSurvivorshipRules(Vector<TCRMPartyBObj> vecParties) throws Exception
	{

		// The last party is the collapsedParty
		 TCRMPartyBObj newParty = vecParties.elementAt((vecParties.size() - 1));
		DWLControl theControl = newParty.getControl();

		IContract contractComp = (IContract) TCRMClassFactory
				.getTCRMComponent(TCRMFinancialPropertyKeys.CONTRACT_COMPONENT);

		Hashtable<String, String> addressMap = new Hashtable<String, String>();
		Vector<TCRMPartyAddressBObj> vecAddress = newParty.getItemsTCRMPartyAddressBObj();
		
		if(vecAddress != null && vecAddress.size() > 0){
			for (TCRMPartyAddressBObj partyAddressBObj : vecAddress) {
				String addressBusinessKey = getBusinessKey(partyAddressBObj);
				addressMap.put(addressBusinessKey, partyAddressBObj.getPartyAddressIdPK());
			}
		}
		
		HashMap<String, String> phoneNumberMap = new HashMap<String, String>();
		HashMap<String, String> emailMap = new HashMap<String, String>();
		HashMap<String, String> faxMap = new HashMap<String, String>();

		
		Vector<TCRMPartyContactMethodBObj> vecContactMethod = newParty.getItemsTCRMPartyContactMethodBObj();
		
		if(vecContactMethod != null && vecContactMethod.size() > 0){
			for (TCRMPartyContactMethodBObj partyContactMethodBObj : vecContactMethod) {
				String contactMethodType = partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType();
				if(StringUtils.compareIgnoreCaseWithTrim(contactMethodType, "1")){// Type = Phone Number TODO Remove hard coded value
					String phoneBusinessKey = getBusinessKeyForPhoneNumber(partyContactMethodBObj);
					phoneNumberMap.put(phoneBusinessKey, partyContactMethodBObj.getPartyContactMethodIdPK());
				}
				else if(StringUtils.compareIgnoreCaseWithTrim(contactMethodType, "2")){ // Type = Email  TODO Remove hard coded value
					String emailBusinessKey = getBusinessKeyForEmail(partyContactMethodBObj);
					emailMap.put(emailBusinessKey, partyContactMethodBObj.getPartyContactMethodIdPK());
				}
				else if(StringUtils.compareIgnoreCaseWithTrim(contactMethodType, "3")){ // Type = Fax TODO Remove hard coded value
					String faxBusinessKey = getBusinessKeyForFax(partyContactMethodBObj);
					faxMap.put(faxBusinessKey, partyContactMethodBObj.getPartyContactMethodIdPK());
				}
			}
		}



		// Get all contractPartyRoles of parties that participating in
		// collapsing, and put them into vecContractPartyRoles
		Vector<TCRMContractPartyRoleBObj> vecContractPartyRoles = new Vector<TCRMContractPartyRoleBObj>();
		
		for (TCRMPartyBObj partyBObj : vecParties) { //TODO Remove hard coded values
			Vector<TCRMContractPartyRoleBObj> vecTmpRoles = contractComp.getAllContractPartyRolesByParty(
					partyBObj.getPartyId(), "1", "0",
					TCRMRecordFilter.ACTIVE, theControl);

			if (vecTmpRoles != null && vecTmpRoles.size() > 0) {
				vecContractPartyRoles.addAll(vecTmpRoles);
			}
		}


		for (TCRMContractPartyRoleBObj contractPartyRoleBObj : vecContractPartyRoles) {
			contractPartyRoleBObj.setControl(theControl);
			
			contractPartyRoleBObj.setPartyId(newParty.getPartyId());
			
			contractPartyRoleBObj = contractComp.updateContractPartyRole(contractPartyRoleBObj);
			
			Vector<TCRMContractRoleLocationBObj> vecRoleLocation = contractComp.getAllContractRoleLocations(
					contractPartyRoleBObj.getContractRoleIdPK(),
					TCRMRecordFilter.ACTIVE, theControl);
			
			if (vecRoleLocation != null && vecRoleLocation.size() > 0) {
				
				// Link all RoleLocations of active ContractPartyRoles to
				// the corresponding new LocationGroup
				for (TCRMContractRoleLocationBObj roleLocationBObj : vecRoleLocation) {
					TCRMPartyAddressBObj oldPartyAddress = roleLocationBObj.getTCRMPartyAddressBObj();
					TCRMPartyContactMethodBObj oldPartyContactMethod = roleLocationBObj.getTCRMPartyContactMethodBObj();
	
					// For each role location, there can be only one and
					// must be one location group (which is either an
					// address or a contact method).
					if (oldPartyAddress != null) {
						String addressBusinessKey = getBusinessKey(oldPartyAddress);
						String newLocationGroupId = addressMap.get(addressBusinessKey);
	
						if (newLocationGroupId != null) {
							roleLocationBObj.setLocationGroupId(newLocationGroupId);
							roleLocationBObj.setControl(newParty.getControl());
							roleLocationBObj = contractComp.updateContractRoleLocation(roleLocationBObj);
						}
						else 
						{ 								
							roleLocationBObj.setControl(newParty.getControl());
							// endDt is time zone Sensitive		
							roleLocationBObj.setEndDate(Timestamp.valueOf(DWLDateTimeUtilities.getCurrentSystemTime()).toString());
							roleLocationBObj = contractComp
									.updateContractRoleLocation(roleLocationBObj);
						}
					} else 
					{
						// if the PartyAddress is null then the
						// PartyContactMethod should never be null
						// since there can be only one and must be one
						// location group in each RoleLocation
						String contactMethodType = oldPartyContactMethod.getTCRMContactMethodBObj().getContactMethodType();
						String newLocationGroupId = null;
						if(StringUtils.compareIgnoreCaseWithTrim(contactMethodType, "1")){// Type = Phone Number TODO Remove hard coded value
							String phoneBusinessKey = getBusinessKeyForPhoneNumber(oldPartyContactMethod);
							newLocationGroupId = phoneNumberMap.get(phoneBusinessKey);
						}
						else if(StringUtils.compareIgnoreCaseWithTrim(contactMethodType, "2")){ // Type = Email  TODO Remove hard coded value
							String emailBusinessKey = getBusinessKeyForEmail(oldPartyContactMethod);
							newLocationGroupId = emailMap.get(emailBusinessKey);
						}
						else if(StringUtils.compareIgnoreCaseWithTrim(contactMethodType, "3")){ // Type = Fax TODO Remove hard coded value
							String faxBusinessKey = getBusinessKeyForFax(oldPartyContactMethod);
							newLocationGroupId = faxMap.get(faxBusinessKey);
						}

						if (newLocationGroupId != null) {
							roleLocationBObj.setLocationGroupId(newLocationGroupId);
							roleLocationBObj.setControl(newParty.getControl());
							roleLocationBObj = contractComp.updateContractRoleLocation(roleLocationBObj);
						} else {
							roleLocationBObj.setControl(newParty.getControl());
							//  endDt is time zone Sensitive	
							roleLocationBObj.setEndDate(Timestamp.valueOf(DWLDateTimeUtilities.getCurrentSystemTime()).toString());
							roleLocationBObj = contractComp.updateContractRoleLocation(roleLocationBObj);
						}
					}
				}
			}
		}

		return newParty;

	}
	
	/**
	 * Gets the business key of TCRMPartyAddressBObj.
	 * 
	 */
	private String getBusinessKey(TCRMPartyAddressBObj partyAddress) {
		return partyAddress.getAddressId();
	}

	/**
	 * 
	 * @param partyContactMethodBObj
	 * @return
	 */
	private String getBusinessKeyForPhoneNumber(TCRMPartyContactMethodBObj partyContactMethodBObj) {
		return partyContactMethodBObj.getContactMethodUsageType() 
			+ ";"
			+ partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType()
			+ ";"
			+ partyContactMethodBObj.getTCRMContactMethodBObj().getReferenceNumber();
	}

	/**
	 * 
	 * @param partyContactMethodBObj
	 * @return
	 */
	private String getBusinessKeyForEmail(TCRMPartyContactMethodBObj partyContactMethodBObj) {
		return partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType()
			+ ";"
			+ partyContactMethodBObj.getTCRMContactMethodBObj().getReferenceNumber();
	}

	/**
	 * 
	 * @param partyContactMethodBObj
	 * @return
	 */
	private String getBusinessKeyForFax(TCRMPartyContactMethodBObj partyContactMethodBObj) {
		return partyContactMethodBObj.getContactMethodUsageType() 
			+ ";"
			+ partyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType()
			+ ";"
			+ partyContactMethodBObj.getTCRMContactMethodBObj().getReferenceNumber();
	}
}
