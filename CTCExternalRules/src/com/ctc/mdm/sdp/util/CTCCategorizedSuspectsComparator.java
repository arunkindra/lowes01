// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCPartyMatchCategoryExtRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.sdp.util;

import java.util.Comparator;

import com.dwl.tcrm.coreParty.component.CategorizedSuspects;


/**
 * This class Sort the Suspects based on the CdSuspectTp 
 * It will order the suspect like A1,A2,B,L 
 *
 */
public class CTCCategorizedSuspectsComparator implements Comparator<Object>{

/* 
 * (non-Javadoc)
 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
 */
public  int compare(Object obj1, Object obj2) {

	int intSuspectTpObj1 = Integer.parseInt(((CategorizedSuspects)obj1).getCdSuspectTp());
	int intSuspectTpObj2 = Integer.parseInt(((CategorizedSuspects)obj2).getCdSuspectTp());

	if(intSuspectTpObj1 > intSuspectTpObj2) 
		return 1;
	else if(intSuspectTpObj1 < intSuspectTpObj2) 
		return -1;
	else 
		return 0;
}
}
