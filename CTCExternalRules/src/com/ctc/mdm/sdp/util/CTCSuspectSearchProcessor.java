// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCSuspectSearchProcessor.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************
package com.ctc.mdm.sdp.util;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.constants.CTCSQLConstants;
import com.ctc.mdm.common.helper.CTCCheckInputDataHelper;
import com.ctc.mdm.common.util.CTCCommonUtil;
import com.dwl.base.DWLControl;
import com.dwl.base.db.DataManager;
import com.dwl.base.db.QueryConnection;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonSearchBObj;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMProperties;



/**
 * This class contains implementation for Suspect 
 * Searches based on various criteria
 *
 */
public class CTCSuspectSearchProcessor {

	private static final String AND = " AND ";

	private static final String CHAR_END = ")";

	private static final String COMMA_Q = ",?";

	private static final String CHAR_Q = "?";

	private static final String IN = " IN ( ";

	private static final String CHAR_COMMA_Q = " =? ";

	private static final String WHERE = " WHERE ";

	private static final String PROP_MAX_SUSPECT_POOL_SIZE = "max_suspect_pool_size";

	private static final String CONTACT_METHOD_TYPE_EMAIL = "2";

	private static final String CONTACT_METHOD_TYPE_PHONE = "1";

	private static final String STRING_Y = "Y";

	private static final String LAST_NAME = "LAST_NAME";
	
	private static final String FIRST_NAME = "GIVEN_NAME_ONE";

	private static final String IS_NULL = " IS NULL ";
	
	private final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCSuspectSearchProcessor.class);

	private final static String className = "CTCSuspectSearchProcessor";

	private static final int DEFAULT_MAX_SUSPECT_POOL_SIZE = 100;
	private static int intMaxSuspectPoolSize = 0;
	
	private long requestId;
	private DWLControl dwlControl;
	private int defaultTimeout = 180;
	private DWLStatus status;
	
	public CTCSuspectSearchProcessor() {
		super();
		
		if(intMaxSuspectPoolSize == 0){
			
		try {
			String strMaxSuspectPoolSize =  TCRMProperties.getProperty(PROP_MAX_SUSPECT_POOL_SIZE);
			intMaxSuspectPoolSize = Integer.parseInt(strMaxSuspectPoolSize);
		} catch (Exception e) {
			intMaxSuspectPoolSize = DEFAULT_MAX_SUSPECT_POOL_SIZE;
			logger.error("Property max_suspect_pool_size not specified defaulting to " + intMaxSuspectPoolSize);
		}
		}
	}
	
	/**
	 * Search suspects for a given party.
	 * @param tcrmPartyBObj
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Vector<TCRMPartyBObj> searchSuspects(TCRMPartyBObj tcrmPartyBObj) throws Exception {
		String methodName = "searchSuspects";
		logger.info("Entering " + className + " :: " + methodName);
		long startTime = System.currentTimeMillis();
		
		Vector<TCRMPersonNameBObj> vecPersonNames = null;
		Vector<TCRMPartyBObj> vecSuspectParty = new Vector<TCRMPartyBObj>();
		List<String> lstPartyId = new ArrayList<String>();
		String strLastNameQry = null;
		String strFirstNameQry = null;
		
		TCRMPersonBObj personbobj = null;
		
		Set<String> setLastName = new HashSet<String>();
		Set<String> setOfFirstNames = new HashSet<String>();
		requestId = tcrmPartyBObj.getControl().getRequestID();
		dwlControl = tcrmPartyBObj.getControl();
		status = tcrmPartyBObj.getStatus();
		
		if (StringUtils.compareIgnoreCaseWithTrim(tcrmPartyBObj.getPartyType(),CTCConstants.PARTY_TYPE_PERSON)) {
			// Party is a Person

			personbobj = (TCRMPersonBObj) tcrmPartyBObj;

			// Standardize PersonName and Collect Standardized Last Names in a List

			vecPersonNames = personbobj.getItemsTCRMPersonNameBObj();

			if (CTCCommonUtil.isEmpty(vecPersonNames)) {
				return vecSuspectParty;
			}

			for (TCRMPersonNameBObj tcrmPersonNameBObj : vecPersonNames) {
				tcrmPersonNameBObj = CTCCommonUtil.standardizePersonName(tcrmPersonNameBObj);
				TCRMPersonSearchBObj objPersonSearch = new TCRMPersonSearchBObj();
				objPersonSearch.setGivenNameOne(tcrmPersonNameBObj.getStdGivenNameOne());
				objPersonSearch.setGivenNameTwo(tcrmPersonNameBObj.getStdGivenNameTwo());
				objPersonSearch.setGivenNameThree(tcrmPersonNameBObj.getGivenNameThree());
				objPersonSearch.setGivenNameFour(tcrmPersonNameBObj.getGivenNameFour());
				objPersonSearch.setLastName(tcrmPersonNameBObj.getStdLastName());
				
				if (CTCCheckInputDataHelper.getInstance(dwlControl).isValidName(objPersonSearch.getLastName())) {
			 	    setLastName.add(objPersonSearch.getLastName());
				}
				
				if (CTCCheckInputDataHelper.getInstance(dwlControl).isValidName(objPersonSearch.getGivenNameOne())) {
				    setOfFirstNames.add(objPersonSearch.getGivenNameOne());
				}
			}

			// Compute LastName Where Clause
						
			if(CTCCommonUtil.isEmpty(setLastName) && CTCCommonUtil.isEmpty(setOfFirstNames)) {
				return vecSuspectParty;
			}
			
			strLastNameQry = constructSQLWithoutWhereClause(LAST_NAME, setLastName);
			strFirstNameQry =  constructSQLWithoutWhereClause(FIRST_NAME, setOfFirstNames);

			// Execute Suspect Search SQLs
			
			// Search with Last Name and Address
			lstPartyId = searchSuspectByNameAndAddress(setLastName,
					tcrmPartyBObj.getItemsTCRMPartyAddressBObj(),
					strLastNameQry);
			
			addSearchResultToSuspectList(vecSuspectParty, lstPartyId,
					tcrmPartyBObj);
			
			if(vecSuspectParty.size() >= intMaxSuspectPoolSize){
	
				return adjustPoolSize(vecSuspectParty);
				
			}

			// Search with First Name and Address
			lstPartyId = searchSuspectByNameAndAddress(setOfFirstNames,
					tcrmPartyBObj.getItemsTCRMPartyAddressBObj(),
					strFirstNameQry);
			
			addSearchResultToSuspectList(vecSuspectParty, lstPartyId,
					tcrmPartyBObj);
			
			if(vecSuspectParty.size() >= intMaxSuspectPoolSize){
	
				return adjustPoolSize(vecSuspectParty);
				
			}

			// Search with Last Name and Date of Birth
			lstPartyId = searchSuspectByNameAndDOB(setLastName, tcrmPartyBObj, strLastNameQry);
			
			addSearchResultToSuspectList(vecSuspectParty, lstPartyId, tcrmPartyBObj);
			
			if(vecSuspectParty.size() >= intMaxSuspectPoolSize){
				return adjustPoolSize(vecSuspectParty);
			}
			
			// Search with First Name and Date of Birth
		    lstPartyId = searchSuspectByNameAndDOB(setOfFirstNames, tcrmPartyBObj, strFirstNameQry);
			
			addSearchResultToSuspectList(vecSuspectParty, lstPartyId, tcrmPartyBObj);
			
			if(vecSuspectParty.size() >= intMaxSuspectPoolSize){
				return adjustPoolSize(vecSuspectParty);
			}
			
			// Search with Last Name and Phone
			lstPartyId = searchSuspectByNameAndPhone(setLastName,
					tcrmPartyBObj.getItemsTCRMPartyContactMethodBObj(),
					strLastNameQry);

			addSearchResultToSuspectList(vecSuspectParty, lstPartyId,
					tcrmPartyBObj);

			if(vecSuspectParty.size() >= intMaxSuspectPoolSize){
				return adjustPoolSize(vecSuspectParty);
			}

			// Search with First Name and Phone
			lstPartyId = searchSuspectByNameAndPhone(setOfFirstNames,
					tcrmPartyBObj.getItemsTCRMPartyContactMethodBObj(),
					strFirstNameQry);

			addSearchResultToSuspectList(vecSuspectParty, lstPartyId,
					tcrmPartyBObj);

			if(vecSuspectParty.size() >= intMaxSuspectPoolSize){
				
				return adjustPoolSize(vecSuspectParty);
				
			}

			// Search with Last Name and EMail
			lstPartyId = searchSuspectByNameAndEMail(setLastName,
					tcrmPartyBObj.getItemsTCRMPartyContactMethodBObj(),
					strLastNameQry);

			addSearchResultToSuspectList(vecSuspectParty, lstPartyId,
					tcrmPartyBObj);
			
			if(vecSuspectParty.size() >= intMaxSuspectPoolSize){
				
				return adjustPoolSize(vecSuspectParty);
				
			}

			// Search with First Name and EMail
			lstPartyId = searchSuspectByNameAndEMail(setOfFirstNames,
					tcrmPartyBObj.getItemsTCRMPartyContactMethodBObj(),
					strFirstNameQry);

			addSearchResultToSuspectList(vecSuspectParty, lstPartyId,
					tcrmPartyBObj);
			
			if(vecSuspectParty.size() >= intMaxSuspectPoolSize){
				
				return adjustPoolSize(vecSuspectParty);
				
			}

		}
		
		logger.info("Exiting " + className + " :: " + methodName);
		
		long endTime = System.currentTimeMillis();
		CTCCommonUtil.printResults(this.getClass().getName(), "searchSuspects() - RequestId: " + requestId, startTime, endTime, dwlControl);
		
		return vecSuspectParty;

	}
	
	
	/**
	 * Search suspect by Name And Address
	 * @param vecName
	 * @param vecPartyAddress
	 * @param strNameQry
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<String> searchSuspectByNameAndAddress(Set<String> vecName,
			Vector<TCRMPartyAddressBObj> vecPartyAddress, String strNameQry) 
			throws Exception {
		long startTime = 0;
		String methodName = "searchSuspectByNameAndAddress";
		
		logger.info("Entering " + className + " :: " + methodName);

		List<String> lstSearchResults = new ArrayList<String>();
		QueryConnection connection = null;
		// PreparedStatement preparedStatement = null;
		ResultSet objResultSet = null;
		
		List params = null;
		
			// Standardize address

			for (TCRMPartyAddressBObj tcrmPartyAddress : vecPartyAddress) {

				TCRMAddressBObj tcrmAddressBObj = tcrmPartyAddress
						.getTCRMAddressBObj();

				CTCCommonUtil.standardizeAddress(tcrmAddressBObj);

				// Is Address Standardized
				params = new ArrayList();

				if ( StringUtils.compareIgnoreCaseWithTrim(tcrmAddressBObj.getStandardFormatingIndicator(),STRING_Y)) {//equals
					if (CTCCheckInputDataHelper.getInstance(dwlControl).isValidAddress(tcrmAddressBObj.getAddressLineOne())) {
						if(StringUtils.isNonBlank(tcrmAddressBObj.getAddressLineOne()) 
								&& StringUtils.isNonBlank(tcrmAddressBObj.getCity())
								&& StringUtils.isNonBlank(tcrmAddressBObj.getProvinceStateType())
								&& StringUtils.isNonBlank(tcrmAddressBObj.getZipPostalCode())) {
							
							StringBuffer strBufQuery = new StringBuffer(CTCSQLConstants.SQLSEARCHSTDADDRESS);
							
							// Address is standardized by Quality Stage
							if (strNameQry != null) {
							    strBufQuery.append(strNameQry);
							}
							
							//	add extra check
							params.add(tcrmAddressBObj
									.getAddressLineOne());
							params.add(tcrmAddressBObj.getCity());
							params.add(new Long (tcrmAddressBObj
									.getProvinceStateType()));
							params.add(tcrmAddressBObj
									.getZipPostalCode());
	
							int numberOfNames = vecName.size();
							for (String strName : vecName) {
								if(strName == null && numberOfNames == 1){
									break;
								}
								params.add(strName);
							}
							
							try
							{
								connection = DataManager.getInstance().getQueryConnection();
								startTime = System.currentTimeMillis();
								
								StringBuffer strBuff = new StringBuffer(1000);
								strBuff.append("Search SQL Query with Name and Standardized Address :: - before executed: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray()));
								CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, System.currentTimeMillis(), dwlControl);
								
								// To execute the SQLQuery
								objResultSet = connection.queryResults(strBufQuery.toString(),params.toArray());
								
							    /*preparedStatement = connection.getConnection().prepareStatement(strBufQuery.toString());
								preparedStatement.setQueryTimeout(defaultTimeout);
								
								for (int i = 1; i <= params.size(); i++) {
									if (params.get(i-1) instanceof String) {
								        preparedStatement.setString(i, (String)params.get(i-1));
									} else if (params.get(i-1) instanceof Long) {
								        preparedStatement.setLong(i, (Long)params.get(i-1));
									}
								}
					             
								// execute select SQL stetement
								objResultSet = preparedStatement.executeQuery();*/
		
								while (objResultSet.next()) {
									lstSearchResults.add(objResultSet.getString(CTCConstants.CONT_ID));
								}
							}catch(Exception objException){
								// throw objException;
								// DWLExceptionUtils.throwDWLBaseException(objException, new DWLUpdateException(objException.getLocalizedMessage()), 
								//		status, DWLStatus.FATAL, "11028", TCRMErrorCode.READ_RECORD_ERROR , CTCErrorConstants.SQL_TIMEOUT, dwlControl);
							}
							finally {
								if (objResultSet != null) {
								    objResultSet.close();
								}
								
								/*if (preparedStatement != null) {
							        preparedStatement.close();
								}*/
								
							    connection.close();
							}
							
							long endTime = System.currentTimeMillis();
							StringBuffer strBuff = new StringBuffer(1000);
							strBuff.append("searchSuspectByNameAndAddress - Standardized Address: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray())).append(", Total returned search results: ").append(lstSearchResults.size());
							
							CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, endTime, dwlControl);
						}
					}
				} else {
					StringBuffer strBufQuery = null;
					objResultSet = null;
					
					if (CTCCheckInputDataHelper.getInstance(dwlControl).isValidAddress(tcrmAddressBObj.getAddressLineOne())) {
						if(StringUtils.isNonBlank(tcrmAddressBObj.getAddressLineOne())){
							strBufQuery = new StringBuffer(CTCSQLConstants.SQLSEARCHADDRLINEONE);
	//						 Address is standardized by Quality Stage
							
							if (strNameQry != null) {
							    strBufQuery.append(strNameQry);
							}
							
							params.add(tcrmAddressBObj.getAddressLineOne());
							
							int numberOfNames = vecName.size();
							for (String strName : vecName) {
								if(strName == null && numberOfNames == 1){
									break;
								}
								params.add(strName);
							}
							
							startTime = System.currentTimeMillis();
							
							StringBuffer strBuff = new StringBuffer(1000);
							strBuff.append("Search SQL Query with Name and Unstandardized Address Line One :: Unstandardized Address - before executed:  ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray()));
							CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, System.currentTimeMillis(), dwlControl);
							
							try
							{
							    connection = DataManager.getInstance().getQueryConnection();
								// To execute the SQLQuery
								objResultSet = connection.queryResults(strBufQuery.toString(),params.toArray());
								
								/*preparedStatement = connection.getConnection().prepareStatement(strBufQuery.toString());
								preparedStatement.setQueryTimeout(defaultTimeout);
								
								for (int i = 1; i <= params.size(); i++) {
									if (params.get(i-1) instanceof String) {
								        preparedStatement.setString(i, (String)params.get(i-1));
									} else if (params.get(i-1) instanceof Long) {
								        preparedStatement.setLong(i, (Long)params.get(i-1));
									}
								}
					            
								// execute select SQL stetement
								objResultSet = preparedStatement.executeQuery(); */
								
								while (objResultSet.next()) {
									lstSearchResults.add(objResultSet.getString(CTCConstants.CONT_ID));
								}
							}catch(Exception objException){
							    throw objException;
								/*DWLExceptionUtils.throwDWLBaseException(objException, new DWLUpdateException(objException.getLocalizedMessage()), 
										status, DWLStatus.FATAL, "11028", TCRMErrorCode.READ_RECORD_ERROR , CTCErrorConstants.SQL_TIMEOUT, dwlControl);*/
							} finally {
								if (objResultSet != null) {
								    objResultSet.close();
								}
								
								/*if (preparedStatement != null) {
							        preparedStatement.close();
								}*/
								
							    connection.close();
							}
							
							strBuff = new StringBuffer(1000);
							strBuff.append("searchSuspectByNameAndAddress - unstandardized Address: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray())).append(", Total returned search results: ").append(lstSearchResults.size());
							CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, System.currentTimeMillis(), dwlControl);
						}
					}
					
					if(StringUtils.isNonBlank(tcrmAddressBObj.getZipPostalCode()) ){
						if (CTCCheckInputDataHelper.getInstance(dwlControl).isValidPostCode(tcrmAddressBObj.getZipPostalCode())) {
							strBufQuery = new StringBuffer(CTCSQLConstants.SQLSEARCHSPOSTALCODE);
							lstSearchResults =  new ArrayList<String>();
							
							// Address is standardized by Quality Stage
							if (strNameQry != null) {
							    strBufQuery.append(strNameQry);
							} else {
								return lstSearchResults;
							}
							
							params.clear();
							params.add(tcrmAddressBObj.getZipPostalCode());
	
							int numberOfNames = vecName.size();
							for (String strName : vecName) {
								if(strName == null && numberOfNames == 1){
									break;
								}
								params.add(strName);
							}
							
							startTime = System.currentTimeMillis();
							
							StringBuffer strBuff = new StringBuffer(1000);
							strBuff.append("Search SQL Query of searchSuspectByNameAndAddress: Unstandardized PostalCode - before executed: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray()));
							CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, System.currentTimeMillis(), dwlControl);
							
							try
							{
							    connection = DataManager.getInstance().getQueryConnection();
							    
							   // To execute the SQLQuery
							   objResultSet = connection.queryResults(strBufQuery.toString(),params.toArray());
								
								/*preparedStatement = connection.getConnection().prepareStatement(strBufQuery.toString());
								preparedStatement.setQueryTimeout(defaultTimeout);
								
								for (int i = 1; i <= params.size(); i++) {
									if (params.get(i-1) instanceof String) {
								        preparedStatement.setString(i, (String)params.get(i-1));
									} else if (params.get(i-1) instanceof Long) {
								        preparedStatement.setLong(i, (Long)params.get(i-1));
									}
								}
					 
								// execute select SQL stetement
								objResultSet = preparedStatement.executeQuery(); */
								
								while (objResultSet.next()) {
									lstSearchResults.add(objResultSet.getString(CTCConstants.CONT_ID));
								}
							}catch(Exception objException){
								throw objException;
								/*DWLExceptionUtils.throwDWLBaseException(objException, new DWLUpdateException(objException.getLocalizedMessage()), 
										status, DWLStatus.FATAL, "11028", TCRMErrorCode.READ_RECORD_ERROR , CTCErrorConstants.SQL_TIMEOUT, dwlControl);*/
							}
							finally {
								if (objResultSet != null) {
								    objResultSet.close();
								}
								
								/*if (preparedStatement != null) {
							        preparedStatement.close();
								}*/
								
							    connection.close();
							}
							
							strBuff = new StringBuffer(1000);
							strBuff.append("searchSuspectByNameAndAddress Unstandardized PostalCode: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray())).append(", Total returned search results: ").append(lstSearchResults.size());
							CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, System.currentTimeMillis(), dwlControl);
						}
					}					
					
				}

			}
		
		logger.info("Exiting " + className + " :: " + methodName);

		return lstSearchResults;

	}

	/**
	 * Search suspect by Name And Date of birth
	 * 
	 * @param vecName
	 * @param tcrmPartyBObj
	 * @param strNameQry
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private List<String> searchSuspectByNameAndDOB(Set<String> vecName,
			TCRMPartyBObj tcrmPartyBObj, String strNameQry)
			throws Exception {
		QueryConnection connection = null;
		String methodName = "searchSuspectByNameAndDOB";

		logger.info("Entering " + className + " :: " + methodName);

		List<String> lstSearchResults = new ArrayList<String>();
		
		if(StringUtils.isBlank(((TCRMPersonBObj) tcrmPartyBObj).getBirthDate())){
			return lstSearchResults;
			
		}
		
		List params = new ArrayList();

		StringBuffer strBufQuery = new StringBuffer(CTCSQLConstants.SQLSEARCHBIRTHDATE);
		// Address is standardized by Quality Stage

		params.add(((TCRMPersonBObj) tcrmPartyBObj).getEObjPerson().getBirthDt());
		
		if (strNameQry != null) {
		    strBufQuery.append(strNameQry);
		} else {
			return lstSearchResults;
		}

		int numberOfNames = vecName.size();
		for (String strName : vecName) {
			if(strName == null && numberOfNames == 1){
				break;
			}
			params.add(strName);
		}

		long startTime = System.currentTimeMillis();
		
		StringBuffer strBuff = new StringBuffer(1000);
		strBuff.append("Search SQL Query of searchSuspectByNameAndDOB: - before executed: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray()));
		CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, System.currentTimeMillis(), dwlControl);
		
		try
		{
			connection = DataManager.getInstance().getQueryConnection();
		    // To execute the SQLQuery
		    
			ResultSet objResultSet = connection.queryResults(strBufQuery.toString(),params.toArray());
		while (objResultSet.next()) {
			lstSearchResults.add(objResultSet.getString(CTCConstants.CONT_ID));
		}
		}catch(Exception objException){
			//log exception
			throw objException;
		}
		finally {
		   connection.close();
		}
		
		logger.info("Exiting " + className + " :: " + methodName);
		
		long endTime = System.currentTimeMillis();
		strBuff = new StringBuffer(1000);
		strBuff.append("searchSuspectByNameAndDOB: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray())).append(", Total returned search results: ").append(lstSearchResults.size());
		CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, endTime, dwlControl);
		 
		return lstSearchResults;
	}
    
	
	/**
	 * Search suspect by Name And Phone
	 * 
	 * @param setNames
	 * @param vecPartyContactMethod
	 * @param strNameQry
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private List<String> searchSuspectByNameAndPhone(Set<String> setNames,
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod, 
			String strNameQry) throws Exception {
		long startTime = 0;
		
		QueryConnection connection = null;
		String methodName = "searchSuspectByNameAndPhone";

		logger.info("Entering " + className + " :: " + methodName);

		List<String> lstSearchResults = new ArrayList<String>();

		for (TCRMPartyContactMethodBObj tcrmPartyContactMethodObj : vecPartyContactMethod) {

			TCRMContactMethodBObj objContactMethod = tcrmPartyContactMethodObj
					.getTCRMContactMethodBObj();

			if (StringUtils.isNonBlank(objContactMethod.getReferenceNumber())) {
				if (StringUtils.compareIgnoreCaseWithTrim(objContactMethod.getContactMethodType(), CONTACT_METHOD_TYPE_PHONE)) {
					if (CTCCheckInputDataHelper.getInstance(dwlControl).isValidPhoneNumber(objContactMethod.getReferenceNumber())) {
						CTCCommonUtil.standardizeContactMethod(objContactMethod);
	
						List params = new ArrayList();
	
						StringBuffer strBufQuery = new StringBuffer(CTCSQLConstants.SQLSEARCH_CONTACTMETHOD_PHONE);
						
						if (strNameQry != null) {
						    strBufQuery.append(strNameQry);
						}
						
						params.add((tcrmPartyContactMethodObj.getTCRMContactMethodBObj().getReferenceNumber()));
						params.add(CONTACT_METHOD_TYPE_PHONE);
	
					    logger.fine("Search SQL Query of searchSuspectByNameAndPhone: - before executed: " + strBufQuery.toString() + ", params: " + returnSring(params));
					    
						int numberOfNames = setNames.size();
						for (String strName : setNames) {
							if(strName == null && numberOfNames == 1){
								break;
							}
							params.add(strName);
						}
	
						try {
							connection = DataManager.getInstance().getQueryConnection();
							// To execute the SQLQuery
							startTime = System.currentTimeMillis();
							
							ResultSet objResultSet = connection.queryResults(strBufQuery.toString(), params.toArray());
							
							while (objResultSet.next()) {
								lstSearchResults.add(objResultSet.getString(CTCConstants.CONT_ID));
							}
						} catch (Exception objException) {
							// log exception
							throw objException;
						} finally {
							connection.close();
						}
						
						long endTime = System.currentTimeMillis();
						StringBuffer strBuff = new StringBuffer(1000);
						strBuff.append("searchSuspectByNameAndPhone: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray())).append(", Total returned search results: ").append(lstSearchResults.size());
						CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, endTime, dwlControl);
					}
				}
			}
		}
		
		logger.info("Exiting " + className + " :: " + methodName);

		return lstSearchResults;
	}

	/**
	 * Search suspect by Name And Email
	 * 
	 * @param setNames
	 * @param vecPartyContactMethod
	 * @param strNameQry
	 * @return
	 */
	@SuppressWarnings("unchecked")

	private List<String> searchSuspectByNameAndEMail(Set<String> setNames,
			Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod, 
			String strNameQry) throws Exception {
		long startTime = 0;
		
		QueryConnection connection = null;
		String methodName = "searchSuspectByNameAndEMail";

		logger.info("Entering " + className + " :: " + methodName);

		List<String> lstSearchResults = new ArrayList<String>();

		for (TCRMPartyContactMethodBObj tcrmPartyContactMethodObj : vecPartyContactMethod) {

			TCRMContactMethodBObj objContactMethod = tcrmPartyContactMethodObj
					.getTCRMContactMethodBObj();
			if (StringUtils.isNonBlank(objContactMethod.getReferenceNumber())) {
				
				if (StringUtils.compareIgnoreCaseWithTrim(objContactMethod
						.getContactMethodType(), CONTACT_METHOD_TYPE_EMAIL)) {

					List params = new ArrayList();

					StringBuffer strBufQuery = new StringBuffer(
							CTCSQLConstants.SQLSEARCH_CONTACTMETHOD_EMAIL);

					if (strNameQry != null) {
					    strBufQuery.append(strNameQry);
					}

					params.add(tcrmPartyContactMethodObj
							.getTCRMContactMethodBObj().getReferenceNumber().toUpperCase());

					params.add(CONTACT_METHOD_TYPE_EMAIL);
					
					int numberOfNames = setNames.size();
					for (String strName : setNames) {
						if(strName == null && numberOfNames == 1){
							break;
						}
						params.add(strName);
					}
					
					startTime = System.currentTimeMillis();
				    StringBuffer strBuff = new StringBuffer(1000);
					strBuff.append("Search SQL Query of searchSuspectByNameAndEMail:: - before executed: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray()));
					CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, System.currentTimeMillis(), dwlControl);
				    
					try {
						connection = DataManager.getInstance()
								.getQueryConnection();
						
						// To execute the SQLQuery
						ResultSet objResultSet = connection.queryResults(strBufQuery.toString(), params.toArray());
						
						while (objResultSet.next()) {
							lstSearchResults.add(objResultSet
									.getString(CTCConstants.CONT_ID));
						}
					} catch (Exception objException) {
						// log exception
						throw objException;
					} finally {
						connection.close();
					}
					
					long endTime = System.currentTimeMillis();
					strBuff = new StringBuffer(1000);
					strBuff.append("searchSuspectByNameAndEMail: ").append(strBufQuery.toString()).append(convertPramsArrayToString(params.toArray())).append(", Total returned search results: ").append(lstSearchResults.size());
					CTCCommonUtil.printResults(this.getClass().getName(), strBuff.toString(), startTime, endTime, dwlControl);
				}
			}
		}
		logger.info("Exiting " + className + " :: " + methodName);

		return lstSearchResults;
	}
	
	
	/**
	 * Add the specific party to the list of suspects.
	 * 
	 * @param suspectList
	 * @param searchResultList
	 * @param party
	 */
	private void addSearchResultToSuspectList(
			Vector<TCRMPartyBObj> suspectList, List<String> searchResult,
			TCRMPartyBObj party) {
		String partyId = party.getPartyId();

		if (searchResult != null) {
			for (String suspectPartyId : searchResult) {
				if (!partyInList(suspectList, suspectPartyId)
						&& !suspectPartyId.equals(partyId)) {
					TCRMPartyBObj pty = new TCRMPartyBObj();
					pty.setPartyId(suspectPartyId);
					pty.setControl(party.getControl());
					suspectList.addElement(pty);
				}

			}
		}
	}

	/**
	 * Check if the party id in the list of suspects.
	 * 
	 * @param vecSuspect
	 * @param partyId
	 * @return true if party id is in the list of suspects; otherwise, return
	 *         false.
	 */
	private boolean partyInList(Vector<TCRMPartyBObj> vecSuspect,
			String partyId) {

		for (int i = 0; i < vecSuspect.size(); i++) {
			if (((TCRMPartyBObj) vecSuspect.elementAt(i)).getPartyId().equals(
					partyId)) {
				return true;
			}
		}

		return false;
	}
	
	
	/**
	 * @param strColumnName
	 * @param setValues
	 * @param blnWhere
	 * @return
	 */
	public static String constructSQLWithWhereClause(String strColumnName, Set<String> setValues) {
		
		StringBuffer strBufWhereClause = new StringBuffer();
		if(CTCCommonUtil.isEmpty(setValues)) {
			return null;
		}
		else {
			if(setValues.size() == 1) {
				
					strBufWhereClause.append(WHERE);//string constant
					strBufWhereClause.append(strColumnName);
					strBufWhereClause.append(CHAR_COMMA_Q);

			} else {
				
					strBufWhereClause.append(WHERE);
					strBufWhereClause.append(strColumnName);
					strBufWhereClause.append(IN);

				boolean firstExecution = true;
				
				for (String strValue : setValues) {

					if(firstExecution) {
						strBufWhereClause.append(CHAR_Q);
						firstExecution = false;
					}
					else
						strBufWhereClause.append(COMMA_Q);
				}
				strBufWhereClause.append(CHAR_END);
			}
		
			return strBufWhereClause.toString();	
		}
		

	}

	/**
	 * @param strColumnName
	 * @param setValues
	 * @return
	 */
	public static String constructSQLWithoutWhereClause(String strColumnName, Set<String> setValues) {
		
		StringBuffer strBufWhereClause = new StringBuffer();
		if(CTCCommonUtil.isEmpty(setValues)) {
			return null;
		}
		else {
			if(setValues.size() == 1) {

					String firstValue = null;
					for (String strValue : setValues) {
						firstValue = strValue;
					}
					
					if(firstValue == null){
						strBufWhereClause.append(AND);
						strBufWhereClause.append(strColumnName);
						strBufWhereClause.append(IS_NULL);
					}
					else
					{
						strBufWhereClause.append(AND);
						strBufWhereClause.append(strColumnName);
						strBufWhereClause.append(CHAR_COMMA_Q);
					}
		
			} else {
				
					strBufWhereClause.append(AND);
					strBufWhereClause.append(strColumnName);
					strBufWhereClause.append(IN);
			
				boolean firstExecution = true;
				
				for (String strValue : setValues) {

					if(firstExecution) {
						strBufWhereClause.append(CHAR_Q);
						firstExecution = false;
					}
					else
						strBufWhereClause.append(COMMA_Q);
				}
				strBufWhereClause.append(CHAR_END);
			}
		
			return strBufWhereClause.toString();	
		}

	}
	
	/**
	 * This method adjusts the pool size depending on the max_suspect_pool_size configuration
	 * @param vecSuspectParty
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Vector adjustPoolSize(Vector<TCRMPartyBObj> vecSuspectParty) {
		
		if(intMaxSuspectPoolSize == vecSuspectParty.size()){
			
			return vecSuspectParty;
		}
		
		Vector vtrSuspectParty = new Vector();
		
		int i=1;
		for (Iterator iter = vecSuspectParty.iterator(); iter.hasNext();) {
			
			if(i>intMaxSuspectPoolSize){
				break;
			}
			vtrSuspectParty.add(iter.next());
			i++;
		}
		
		return vtrSuspectParty;
	}
	
	public String convertPramsArrayToString(Object[] obj) {
		StringBuffer retString = new StringBuffer(100);
		retString.append("PARAMS:");
		
		for (int i = 0; i < obj.length; i++) {
			retString.append(obj[i]).append(", ");
		}
		
		return retString.toString();
	}
	
	private String returnSring(List params) {
		String retString = "";
		
		for (int i = 0; i< params.size(); i++) {
			retString += params.get(i) + ", ";
		}
		
		return retString;
	}
	
}
