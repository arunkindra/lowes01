package com.ctc.mdm.externalrule;

import java.util.Vector;

import com.ctc.mdm.sdp.collapse.CTCCollapsePartiesHelper;
import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCoreComponentID;
import com.dwl.tcrm.coreParty.constant.TCRMCoreErrorReasonCode;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.externalrule.CollapsePartiesWithRules;
import com.dwl.tcrm.utilities.TCRMClassFactory;

public class CTCCollapsePartiesWithRules extends CollapsePartiesWithRules {

	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2006, 2009\nUS Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";

	protected final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCCollapsePartiesWithRules.class);

	protected String ruleName = "CTCCollapsePartiesWithRules";

	protected String debugStr = "External Java Rule 38 " + ruleName + ": ";

	private boolean bCollapseAdminContEquiv = true;

	private boolean bCollapseAlerts = false;

	/**
	 * @param input
	 *            A vector of <code>TCRMPartyBObj</code>s passed in by the caller.
	 * @param componentObject
	 *            Not used currently
	 * @return A vector containing the <code>DWLStatus</code> in the first element
	 * 		   (index = 0) and the new <code>TCRMPartyBObj</code> in the second
	 * 		   element (index = 1).
	 */
	public Object execute(Object input, Object componentObject)
			throws Exception {
		debugOut(debugStr + "Entering Rule");

		Vector vecParties = (Vector) input;

		IParty partyComp = (IParty) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);

		DWLStatus status = new DWLStatus();
		status.setStatus(DWLStatus.SUCCESS);
		TCRMPartyBObj outputParty = null;
		Vector outputList = new Vector();
		TCRMPartyBObj bObjParty1 = null;
		TCRMPartyBObj bObjParty2 = null;
		DWLControl theControl = null;

		try {
			bObjParty1 = (TCRMPartyBObj) vecParties.elementAt(0);
			bObjParty2 = (TCRMPartyBObj) vecParties.elementAt(1);
			theControl = ((TCRMPartyBObj) bObjParty1).getControl();

			if (isCollapseAlerts()) {
				debugOut(debugStr + "Retrieving Alerts");

				Vector vec = ((IParty) partyComp).getAllPartyAlerts(
						(((TCRMPartyBObj) bObjParty1).getPartyId()),
						TCRMRecordFilter.ACTIVE, theControl);

	      	    if (vec != null){
	      	        (((TCRMPartyBObj)bObjParty1).getItemsTCRMAlertBObj()).addAll(vec);
	      	    }

				vec = ((IParty) partyComp).getAllPartyAlerts(
						(((TCRMPartyBObj) bObjParty2).getPartyId()),
						TCRMRecordFilter.ACTIVE, theControl);

	      	    if (vec != null){
	      	    	((TCRMPartyBObj)bObjParty2).getItemsTCRMAlertBObj().addAll(vec);
	      	    }
			}

			if (isCollapseAdminContEquiv()) {
				debugOut(debugStr + "Retrieving AdminContEquiv");

				Vector vec = ((IParty) partyComp)
						.getAllPartyAdminSysKeys((((TCRMPartyBObj) bObjParty1)
								.getPartyId()), theControl);
				(((TCRMPartyBObj) bObjParty1).getItemsTCRMAdminContEquivBObj())
						.addAll(vec);

				vec = ((IParty) partyComp)
						.getAllPartyAdminSysKeys((((TCRMPartyBObj) bObjParty2)
								.getPartyId()), theControl);
				(((TCRMPartyBObj) bObjParty2).getItemsTCRMAdminContEquivBObj())
						.addAll(vec);
			}

			outputParty = new CTCCollapsePartiesHelper().collapsePartiesUsingCTCSurvivorshipRules(null, vecParties);
			
		} catch (DWLBaseException ex) {
			status = ex.getStatus();
		} catch (Exception ex) {
			status = addError(
					theControl,
					TCRMErrorCode.READ_RECORD_ERROR,
					TCRMCoreErrorReasonCode.COLLAPSE_PARTIES_SURVIVING_RULES_FAILED,
					ex.getMessage());
		}

		outputList.addElement(status);
		outputList.addElement(outputParty);
		debugOut(debugStr + "Finished");

		return outputList;
	}

	/**
	 * @return Holding the error messages if there are any
	 */
	protected DWLStatus addError(DWLControl theControl, String sErrorCode,
			String sReasonCode, String sDetail) {
		IDWLErrorMessage errHandler = TCRMClassFactory.getErrorHandler();

		DWLError error = errHandler.getErrorMessage(
				TCRMCoreComponentID.PARTY_COMPONENT, sErrorCode, sReasonCode,
				theControl, new String[0]);

		error.setDetail(sDetail);
		DWLStatus status = new DWLStatus();
		status.addError(error);
		status.setStatus(DWLStatus.FATAL);
		return status;
	}

	/**
	 * @param tmpCollapseAlerts
	 *            Set bCollapseAlerts to "true" if collapse partyAlerts is
	 *            desired otherwsie "false"
	 */
	public void setCollapseAlerts(boolean tmpCollapseAlerts) {
		bCollapseAlerts = tmpCollapseAlerts;
	}

	public boolean isCollapseAlerts() {
		return bCollapseAlerts;
	}

	/**
	 * Set bCollapseAdminContEquiv to "true" if collapse AdminContEquiv is
	 * desired otherwise "false"
	 */
	public void setCollapseAdminContEquiv(boolean tmpCollapseAdminContEquiv) {
		bCollapseAdminContEquiv = tmpCollapseAdminContEquiv;
	}

	protected boolean isCollapseAdminContEquiv() {
		return bCollapseAdminContEquiv;
	}

	protected void debugOut(String s) {
		if (logger.isFineEnabled()) {
			logger.fine(s);
		}
	}
}
