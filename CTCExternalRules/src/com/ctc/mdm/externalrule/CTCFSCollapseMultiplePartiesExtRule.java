package com.ctc.mdm.externalrule;

import java.util.Vector;

import com.ctc.mdm.sdp.collapse.CTCFSCollapsePartiesHelper;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.DWLExceptionUtils;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCoreComponentID;
import com.dwl.tcrm.coreParty.constant.TCRMCoreErrorReasonCode;
import com.dwl.tcrm.exception.TCRMReadException;
import com.dwl.tcrm.externalrule.FSCollapseMultiplePartiesExtRule;
import com.dwl.tcrm.utilities.TCRMClassFactory;

public class CTCFSCollapseMultiplePartiesExtRule extends
		FSCollapseMultiplePartiesExtRule {

	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2002, 2009\nUS Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";

	protected final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCFSCollapseMultiplePartiesExtRule.class);

	protected String ruleName = "CTCFSCollapseMultiplePartiesExtRule";

	protected String debugStr = "External Java Rule 120 " + ruleName + ": ";

	private IDWLErrorMessage errHandler;

	/**
	 * Default constructor.
	 */
	public CTCFSCollapseMultiplePartiesExtRule() {
		super();
		errHandler = TCRMClassFactory.getErrorHandler();
	}


	/**
	 * @param input
	 *            A vector of three instances of TCRMPartyBObj (collapsePartyA,
	 *            collapsePartyB, newPartyBObj
	 * @param componentObject
	 *            Not used.
	 * @return A new partyBObj which holds the most recent information of those
	 *         input parties.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {
		debugOut(debugStr + "Entering Rule");

		Vector vecParties = null;
		TCRMPartyBObj newParty = null;
		DWLStatus status = new DWLStatus();

		if (input instanceof Vector) {
			vecParties = (Vector) input;
		}

		if ((vecParties == null)
				|| (vecParties.size() < 3)) {
			DWLExceptionUtils.addErrorToStatus(status, DWLStatus.FATAL,
					TCRMCoreComponentID.GENERAL,
					TCRMErrorCode.READ_RECORD_ERROR,
					TCRMCoreErrorReasonCode.REQUIRED_PARAMETERS_MISSING,
					((TCRMPartyBObj) vecParties.elementAt(0)).getControl(), errHandler);
			// instantiate the readException object.
			TCRMReadException readEx = new TCRMReadException();
			readEx.setStatus(status);
			throw readEx;
		}
		
		newParty = new CTCFSCollapsePartiesHelper().collapsePartiesUsingCTCSurvivorshipRules(vecParties);

		return newParty;
	}

	/**
	 * @param s
	 */
	protected void debugOut(String s) {
		if (logger.isFineEnabled()) {
			logger.fine(s);
		}
	}

}
