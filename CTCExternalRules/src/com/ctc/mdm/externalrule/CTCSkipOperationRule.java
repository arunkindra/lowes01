package com.ctc.mdm.externalrule;

import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.tcrm.utilities.StringUtils;

public class CTCSkipOperationRule extends ClientJavaExtensionSet {

	@Override
	public void execute(ExtensionParameters params) {

		if(StringUtils.isNonBlank(params.getActionType())
				&& StringUtils.isNonBlank(params.getTransactionType())
				&&(!StringUtils.compareIgnoreCaseWithTrim(params.getActionType(), params.getTransactionType()))){
			params.setSkipExecutionFlag(true);
		}
	}

}
