package com.ctc.mdm.externalrule;

import java.util.Vector;

import com.ctc.mdm.sdp.collapse.CTCFSCollapsePartiesHelper;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.externalrule.FSCollapsePartiesExtRule;

public class CTCFSCollapsePartiesExtRule extends FSCollapsePartiesExtRule {

	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2002, 2009\nUS Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";

	protected final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCFSCollapsePartiesExtRule.class);

	protected String ruleName = "CTCFSCollapsePartiesExtRule";

	protected String debugStr = "External Java Rule 12 " + ruleName + ": ";


	/**
	 * @param input
	 *            A vector of three instances of TCRMPartyBObj (collapsePartyA,
	 *            collapsePartyB, newPartyBObj
	 * @param componentObject
	 *            Not used.
	 * @return A new partyBObj which holds the most recent information of those
	 *         input parties.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {
		debugOut(debugStr + "Entering Rule");

		Vector vecParties = null;
		TCRMPartyBObj newParty = null;

		if (input instanceof Vector) {
			vecParties = (Vector) input;
		}

		newParty = new CTCFSCollapsePartiesHelper().collapsePartiesUsingCTCSurvivorshipRules(vecParties);

		return newParty;
	}

	/**
	 * @param s
	 */
	protected void debugOut(String s) {
		if (logger.isFineEnabled()) {
			logger.fine(s);
		}
	}
}
