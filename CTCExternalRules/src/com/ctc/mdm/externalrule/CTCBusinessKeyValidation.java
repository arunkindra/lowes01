package com.ctc.mdm.externalrule;

import java.util.Iterator;
import java.util.Vector;

import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.tcrm.externalrule.BusinessKeyValidation;


public class CTCBusinessKeyValidation extends BusinessKeyValidation{
	
	public Object execute(Object input, Object componentObject) throws Exception {
		if(super.getRuleId().equals("18")){
		    Object retObject = null;
		    Vector statusVector=new Vector();
		    long modifiedStatus = 0L;
		    retObject =super.partyBusinessKeyValidation(input, componentObject);
		    DWLStatus status = (DWLStatus)retObject;
		    
		    if(status.getStatus() != 0) {
		    	Vector vecStatus = status.getDwlErrorGroup();
		    	Iterator itrVecStatus=vecStatus.iterator();
			    while(itrVecStatus.hasNext()) {
			    	DWLError dwlError=(DWLError)itrVecStatus.next();
		    		dwlError.getReasonCode();
		    		if(dwlError.getReasonCode()==1640 /*Error code for PersonName*/
		    				|| dwlError.getReasonCode()==1631/*Error code for Address*/
		    				|| dwlError.getReasonCode()==1634 /*Error code for ContactMethod*/
		    			){
		    			//do nothing
		    		}
		    		else {
		    			statusVector.add(dwlError);
		    			modifiedStatus = 9L;
		    		}
			    }
			    status.getDwlErrorGroup().clear();
			    status.setStatus(modifiedStatus);
			    if(statusVector != null){
			    	status.getDwlErrorGroup().addAll(statusVector);
			    }
		    }
		    return status;
		}else{
			return super.execute(input, componentObject);
		}
	}
}