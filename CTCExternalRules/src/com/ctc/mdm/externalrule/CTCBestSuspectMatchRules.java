// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCBestSuspectMatchRules.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.externalrule.DWLControlFinder;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.management.config.client.Configuration;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCoreComponentID;
import com.dwl.tcrm.coreParty.constant.TCRMCoreErrorReasonCode;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.coreParty.interfaces.ISuspectProcessor;
import com.dwl.tcrm.externalrule.BestSuspectMatchRules;
import com.dwl.tcrm.utilities.TCRMClassFactory;

/**
 * External Rule 37: BestSuspectMatchRules. This rule identifies the best match
 * suspect candidate in a list of suspect objects for a party. It is called by
 * the method <code>findBestSuspectMatch()</code> of the
 * <code>TCRMSuspectComponent</code>.
 * @since 7.0
 */
public class CTCBestSuspectMatchRules extends BestSuspectMatchRules {
	protected final static IDWLLogger logger = DWLLoggerManager
		.getLogger(CTCBestSuspectMatchRules.class);

	private static final String Y = "Y";

	private static final String SUSPECT_TYPE_A1 = "1";

	private static final String _100 = "100";

	private static final String IBM_DWLCOMMON_SERVICES_LOCALE_LANGUAGE_ID = "/IBM/DWLCommonServices/Locale/languageId";

	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright " +
			"IBM Corp. 2006, 2009\nUS Government Users Restricted Rights - Use, duplication or disclosure " +
				"restricted by GSA ADP Schedule Contract with IBM Corp.";

	private static final String WCC_ENGINE_CODE = SUSPECT_TYPE_A1;

	private static final String QUALITY_STAGE_ENGINE_CODE = "2";

	private static final String ABILITEC_ENGINE_CODE = "3";

	/**
	 * @param input
	 *            A <code>Vector</code> of <code>TCRMSuspectBObj</code>s passed
	 *            by the caller.
	 * @param componentObject
	 *            ISuspectProcessor
	 * 
	 * @return A <code>Vector</code> containing a <code>TCRMSuspectBObj</code>
	 *         in the first element (index = 0) and a <code>DWLStatus</code> in
	 *         the second element (index = 1).
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {
		logger.info("Entering CTCBestSuspectMatchRules.execute()");
		Vector vecSuspects = (Vector) input;
		ISuspectProcessor compISuspect = (ISuspectProcessor) componentObject;
		TCRMSuspectBObj candidateSuspect = null;
		DWLStatus status = new DWLStatus();
		status.setStatus(DWLStatus.SUCCESS);
		DWLControl theControl = null;
		String debugStr = "External Java Rule 37 - CTCBestSuspectMatchRules fired";

		try {
			// Do not count A1 suspects if they have pending
			// CDC (critical data changes)
			if (vecSuspects == null || vecSuspects.isEmpty()) {
				theControl = new DWLControlFinder()
						.findInCollection(vecSuspects);

				try {
					theControl.put(DWLControl.LANG_ID, Configuration
							.getConfiguration().getConfigItem(
									IBM_DWLCOMMON_SERVICES_LOCALE_LANGUAGE_ID)
							.getValue());
				} catch (Exception ex) {
					theControl.put(DWLControl.LANG_ID, _100);
				}
			} else {
				theControl = ((TCRMSuspectBObj) (vecSuspects.elementAt(0)))
						.getControl();
			}

			IParty partyComp = (IParty) TCRMClassFactory
					.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
			String partyId = null;
			TCRMPartyBObj partyBObj = null;
			int numOfSuspect = vecSuspects.size() - 1;
			for (int i = numOfSuspect; i >= 0; i--) {
				TCRMSuspectBObj suspect = (TCRMSuspectBObj) vecSuspects.get(i);
				if (suspect.getCurrentSuspectCategoryType() != null
						&& suspect.getCurrentSuspectCategoryType().equals(
								SUSPECT_TYPE_A1)) { // A1
					// type
					partyId = suspect.getSuspectPartyId();
					partyBObj = partyComp.getPartyBasic(partyId, theControl);
					if (partyBObj.getPendingCDCIndicator() != null
							&& partyBObj.getPendingCDCIndicator()
									.equalsIgnoreCase(Y)) {
						vecSuspects.remove(i);
					}
				}
			}

			/*
			 * By default, the sequence of determining best match is based on
			 * this order: AbiliTEC Adjusted A1 (if present) precedes QS
			 * Adjusted A1 (if present) precedes WCC Adjusted or Not Adjusted A1
			 * (if present) precedes AbiliTEC Adjusted A2 (if present) precedes
			 * QS Adjusted A2 (if present) precedes WCC Adjusted or Not Adjusted
			 * A2 (if present)
			 */
			Vector vecSuspectTypePriority = new Vector();
			vecSuspectTypePriority.addElement("A1");


			Vector vecMatchEngineTypeCodePriority = new Vector();
			vecMatchEngineTypeCodePriority.addElement(ABILITEC_ENGINE_CODE);
			vecMatchEngineTypeCodePriority
					.addElement(QUALITY_STAGE_ENGINE_CODE);
			vecMatchEngineTypeCodePriority.addElement(WCC_ENGINE_CODE);

			candidateSuspect = compISuspect.chooseBestMatch(vecSuspects,
					vecSuspectTypePriority, vecMatchEngineTypeCodePriority,
					true);

			if (candidateSuspect != null) {
				if (logger.isErrorEnabled())
					logger.fine(debugStr
							+ "getHighestMatchRelecvancyScoreParty succeed");
			} else {
				if (logger.isErrorEnabled())
					logger.fine(debugStr
							+ "getHighestMatchRelecvancyScoreParty failed");
				status = addError(theControl, TCRMErrorCode.READ_RECORD_ERROR,
						TCRMCoreErrorReasonCode.BEST_SUSPECT_MATCH_RULE_FAILED,
						"getHighestMatchRelecvancyScoreParty failed");
			}
		} catch (DWLBaseException ex) {
			status = ex.getStatus();
		} catch (Exception ex) {
			status = addError(theControl, TCRMErrorCode.READ_RECORD_ERROR,
					TCRMCoreErrorReasonCode.BEST_SUSPECT_MATCH_RULE_FAILED, ex
							.getMessage());
		}

		Vector vecRet = new Vector();
		vecRet.addElement(status);
		vecRet.addElement(candidateSuspect);
		
		logger.info("Exiting CTCBestSuspectMatchRules.execute()");
		return vecRet;
	}

	/**
	 * Add an instance of DWLError to DWLStatus
	 * 
	 * @param theControl
	 * @param sErrorCode
	 * @param sReasonCode
	 * @param sDetail
	 * @return DWLStatus
	 */
	protected DWLStatus addError(DWLControl theControl, String sErrorCode,
			String sReasonCode, String sDetail) {
		logger.info("Entering CTCBestSuspectMatchRules.addError()");
		IDWLErrorMessage errHandler = TCRMClassFactory.getErrorHandler();
		DWLError error = errHandler.getErrorMessage(
				TCRMCoreComponentID.PARTY_COMPONENT, sErrorCode, sReasonCode,
				theControl, new String[0]);
		error.setDetail(sDetail);
		DWLStatus status = new DWLStatus();
		status.addError(error);
		status.setStatus(DWLStatus.FATAL);
		
		logger.info("Exiting CTCBestSuspectMatchRules.addError()");
		return status;
	}

}
