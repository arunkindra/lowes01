// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCLSuspectsActionRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.util.Vector;
import com.dwl.base.DWLControl;
import com.dwl.base.externalrule.Rule;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.management.config.client.Configuration;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.ISuspectProcessor;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.common.codetype.component.CodeTypeTableNames;
import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;
import com.ibm.mdm.domains.codetype.obj.SuspectStatusTypeBObj;

/**
 *  
 * Handles the processing of L suspects. 
 * 
 * An L suspect entry will be created.
 * 
 */

public class CTCLSuspectsActionRule extends Rule {

	private static final String IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_REASON_TYPE_SYSTEM_MARKED 
		= "/IBM/Party/SuspectProcessing/SuspectReasonType/systemMarked";
	
	private static final String IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECTDUPLICATE = "/IBM/Party/SuspectProcessing/SuspectStatusType/suspectDuplicate";
	
	private final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCPartyUpdateExtRule.class);

	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {
		logger.info("Entering into CTCLSuspectsActionRule.execute()");

		Vector vecInput = (Vector) input;
		TCRMPartyBObj sourceParty = (TCRMPartyBObj) vecInput.elementAt(0); // source
		// party
		Vector<TCRMSuspectBObj> vecLSuspects = (Vector) vecInput.elementAt(1);// LSuspect
		// Party
		SuspectStatusTypeBObj objSuspectStatusTypeBobj = null;

		ISuspectProcessor suspComp = (ISuspectProcessor) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.SUSPECT_COMPONENT);
		CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory
				.getCodeTypeComponentHelper();

		DWLControl theDWLControl = sourceParty.getControl();
		String langId = theDWLControl.getRequesterLanguage();

		int sizeVecLSuspects = vecLSuspects.size();

		for (int i = 0; i < sizeVecLSuspects; i++) {

			TCRMSuspectBObj objTCRMSuspectBObj = vecLSuspects.elementAt(i);
			objTCRMSuspectBObj.setPartyId(sourceParty.getPartyId());
			objTCRMSuspectBObj.setControl(theDWLControl);
			objTCRMSuspectBObj.setSuspectStatusType(getSuspectStatus());
			objSuspectStatusTypeBobj = (SuspectStatusTypeBObj) codeTypeCompHelper
					.getCodeTypeByCode(CodeTypeTableNames.SUSPECT_STATUS_TYPE,
							langId, objTCRMSuspectBObj.getSuspectStatusType(),
							theDWLControl);
			objTCRMSuspectBObj.setSuspectStatusValue(objSuspectStatusTypeBobj
					.getname());
			objTCRMSuspectBObj
					.setSourceType(Configuration
							.getConfiguration()
							.getConfigItem(
									IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_REASON_TYPE_SYSTEM_MARKED)
							.getValue());

			suspComp.createSuspect(objTCRMSuspectBObj);
		}
		sourceParty.vecTCRMSuspectBObj.removeAllElements();
		logger.info("Exiting from CTCLSuspectsActionRule.execute()");
		return null;
	}

	/**
	 * Get the Suspect Status
	 * @return
	 * @throws Exception
	 */
	protected String getSuspectStatus() throws Exception {
		return Configuration.getConfiguration().getConfigItem(
				IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECTDUPLICATE)
				.getValue();
	}
}
