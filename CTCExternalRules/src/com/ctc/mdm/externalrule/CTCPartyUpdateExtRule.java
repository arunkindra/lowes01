// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCPartyUpdateExtRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.helper.CTCMaintainPartyHelper;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.externalrule.PartyUpdateExtRule;


public class CTCPartyUpdateExtRule extends PartyUpdateExtRule {
	protected final static IDWLLogger logger = DWLLoggerManager
	.getLogger(CTCPartyUpdateExtRule.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.dwl.tcrm.externalrule.PartyUpdateExtRule#execute(java.lang.Object,
	 * java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
	throws Exception {
		logger.info("Entering CTCPartyUpdateExtRule.execute() ");
		TCRMPartyBObj resultTCRMPartyBObj = null;
		Vector v = (Vector) input;
		// partyA is source party
		TCRMPartyBObj partyA = (TCRMPartyBObj) v.elementAt(0); 
		// partyB is A1Suspect party
		TCRMPartyBObj partyB = (TCRMPartyBObj) v.elementAt(1); 

		if (partyA.getPartyType() != null) {
			if (partyA.getPartyType().equalsIgnoreCase(
					CTCConstants.PARTY_TYPE_ORG)) {
				resultTCRMPartyBObj = (TCRMPartyBObj) super.execute(input,
						componentObject);
			} else {
				CTCMaintainPartyHelper objCTCMaintainPartyHelper = new CTCMaintainPartyHelper();
				partyA = objCTCMaintainPartyHelper.handleUpdateCustomer(partyA,
						partyB);
				resultTCRMPartyBObj = (TCRMPartyBObj) super
						.updateMergedParty(partyA);

			}
		}
		logger.info("Exiting CTCPartyUpdateExtRule.execute() ");
		return resultTCRMPartyBObj;
	}

}
