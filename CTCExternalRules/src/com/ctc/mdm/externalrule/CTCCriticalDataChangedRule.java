// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCCriticalDataChangedRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.util.CTCCommonUtil;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.common.TCRMCommon;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMOrganizationBObj;
import com.dwl.tcrm.coreParty.component.TCRMOrganizationNameBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyIdentificationBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCoreInquiryLevel;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IAddress;
import com.dwl.tcrm.coreParty.interfaces.IContactMethod;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.coreParty.interfaces.IPerson;
import com.dwl.tcrm.coreParty.notification.CriticalDataChangedElement;
import com.dwl.tcrm.externalrule.PartyMatch;
import com.dwl.tcrm.utilities.TCRMClassFactory;

/**
 * Rule 8 - critical data changed rule for TCRMPartyMacher - check if any
 * critical data is changed for the given party based on the defined rules
 * (Compare the descendant of TcrmCommon passed in with the corresponding data
 * in database.)
 * 
 * @param input
 *            An instance of TCRMPartyBObj/TCRMPersonBObj/
 *            TCRMOrganizationBObj/TCRMPersonNameBObj
 *            /TCRMOrganizationNameBObj/TCRMPartyAddressBObj
 *            /TCRMPartyContactMethodBObj/TCRMPartyIdentificationBObj and
 *            partyId
 * @param componentObject
 *            Not used currently
 * @return A vector of instances of CriticalDataChangedElement, which holds the
 *         data changes - all the two BObjs compared that are different
 * 
 */
public class CTCCriticalDataChangedRule extends PartyMatch {

	private static final String TCRMPERSON_BOBJ = "TCRMPersonBObj";

	private static final String TCRMPARTY_CONTACT_METHOD_BOBJ = "TCRMPartyContactMethodBObj";

	private static final String TCRMPARTY_ADDRESS_BOBJ = "TCRMPartyAddressBObj";

	private static final String TCRMPERSON_NAME_BOBJ = "TCRMPersonNameBObj";

	protected final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCCriticalDataChangedRule.class);

	protected String ruleName = "CTCCriticalDataChangedRule";

	protected String debugStr = "External Java Rule 8 " + ruleName + ": ";

	/**
	 * Search suspects of a given party.
	 * 
	 * @param input
	 *            parameters that passed by the caller
	 * @param componentObject
	 *            Not used currently
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {

		if (logger.isFineEnabled()) {
			logger.fine(debugStr + "Entering Rule");
		}

		debugOut(debugStr + "Entering Rule");

		Vector vecInput = (Vector) input;
		TCRMCommon tcrmCommon = (TCRMCommon) vecInput.elementAt(0);

		boolean isPerson = false;
		boolean isPersonName = false;
		boolean isPartyAddress = false;
		boolean isPartyContactMethod = false;
		boolean isOrganization = false;
		boolean isOrganizationName = false;
		boolean isPartyIdentification = false;

		Vector vecCriticalData = new Vector();

		// instantiate the party component.
		IParty partyComp = (IParty) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);

		if ((tcrmCommon instanceof TCRMPartyBObj)
				&& (StringUtils.compareIgnoreCaseWithTrim(
						((TCRMPartyBObj) tcrmCommon).getPartyType(),
						CTCConstants.PARTY_TYPE_PERSON))) {
			isPerson = true;
		} else if ((tcrmCommon instanceof TCRMPartyBObj)
				&& (StringUtils.compareIgnoreCaseWithTrim(
						((TCRMPartyBObj) tcrmCommon).getPartyType(),
						CTCConstants.PARTY_TYPE_ORG))) {
			isOrganization = true;
		} else if (tcrmCommon instanceof TCRMPersonBObj) {
			isPerson = true;
		} else if (tcrmCommon instanceof TCRMPersonNameBObj) {
			isPersonName = true;
		} else if (tcrmCommon instanceof TCRMPartyAddressBObj) {
			isPartyAddress = true;
		} else if (tcrmCommon instanceof TCRMPartyContactMethodBObj) {
			isPartyContactMethod = true;
		} else if (tcrmCommon instanceof TCRMOrganizationBObj) {
			isOrganization = true;
		} else if (tcrmCommon instanceof TCRMOrganizationNameBObj) {
			isOrganizationName = true;
		} else if (tcrmCommon instanceof TCRMPartyIdentificationBObj) {
			isPartyIdentification = true;
		}

		// If Organization or OrganizationName invoke super.execute() method
		if (isOrganization || isOrganizationName || isPartyIdentification) {
			vecCriticalData = (Vector) super.execute(input, componentObject);
			return vecCriticalData;
		}

		if (isPersonName) {

			TCRMPersonNameBObj personNameInput = (TCRMPersonNameBObj) tcrmCommon;

			if (StringUtils.isNonBlank(personNameInput.getPersonNameIdPK())) {

				TCRMPersonNameBObj personNameDB = ((IPerson) partyComp)
						.getPersonNameByIdPK(personNameInput
								.getPersonNameIdPK(), personNameInput
								.getControl());

				if (personNameDB != null) {
					// Existing personName

					// Standardize Input Object
					CTCCommonUtil.standardizePersonName(personNameInput);

					// Compare LastName, FirstName, Generation Fields
					CriticalDataChangedElement criticalDataChanged = personNameIsSame(
							personNameDB, personNameInput);

					if (criticalDataChanged != null) {
						vecCriticalData.addElement(criticalDataChanged);
					}

				} else {
					CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
					criticalDataChanged.setBeforeUpdate(null);
					criticalDataChanged.setAfterUpdate(personNameInput);
					criticalDataChanged.setElementName(TCRMPERSON_NAME_BOBJ);
					vecCriticalData.addElement(criticalDataChanged);

				}

			} else { // if it is new data
				CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
				criticalDataChanged.setBeforeUpdate(null);
				criticalDataChanged.setAfterUpdate(personNameInput);
				criticalDataChanged.setElementName(TCRMPERSON_NAME_BOBJ);
				vecCriticalData.addElement(criticalDataChanged);
			}

		} else if (isPartyAddress) { // ************ handle party address
			// changes *********************

			TCRMPartyAddressBObj partyAddressInput = (TCRMPartyAddressBObj) tcrmCommon;

			TCRMAddressBObj addressInput = (TCRMAddressBObj) ((TCRMPartyAddressBObj) tcrmCommon)
					.getTCRMAddressBObj();

			if (StringUtils.isNonBlank(partyAddressInput.getPartyAddressIdPK())) {

				TCRMPartyAddressBObj partyAddressDB = partyComp
						.getPartyAddressByIdPK(partyAddressInput
								.getPartyAddressIdPK(), partyAddressInput
								.getControl());

				if (partyAddressDB != null) {
					// Existing Address
					IAddress addressComp = (IAddress) TCRMClassFactory
							.getTCRMComponent(TCRMCorePropertyKeys.ADDRESS_COMPONENT);
					TCRMAddressBObj addressDB = addressComp.getAddress(
							partyAddressDB.getAddressId(), partyAddressInput
									.getControl());
					partyAddressDB.setTCRMAddressBObj(addressDB);

					CTCCommonUtil.standardizeAddress(addressInput);

					if (addressDB != null
							&& !isAddressSame(addressDB, addressInput)) {
						CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
						criticalDataChanged.setBeforeUpdate(partyAddressDB);
						criticalDataChanged.setAfterUpdate(partyAddressInput);
						criticalDataChanged
								.setElementName(TCRMPARTY_ADDRESS_BOBJ);
						vecCriticalData.addElement(criticalDataChanged);
					}
				} else {
					CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
					criticalDataChanged.setBeforeUpdate(null);
					criticalDataChanged.setAfterUpdate(partyAddressInput);
					criticalDataChanged.setElementName(TCRMPARTY_ADDRESS_BOBJ);
					vecCriticalData.addElement(criticalDataChanged);
				}
			} else { // else it is a new party address
				CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
				criticalDataChanged.setBeforeUpdate(null);
				criticalDataChanged.setAfterUpdate(partyAddressInput);
				criticalDataChanged.setElementName(TCRMPARTY_ADDRESS_BOBJ);
				vecCriticalData.addElement(criticalDataChanged);
			}

		} else if (isPartyContactMethod) {

			TCRMPartyContactMethodBObj partyContactMethodInput = (TCRMPartyContactMethodBObj) tcrmCommon;

			TCRMContactMethodBObj contactMethodInput = partyContactMethodInput
					.getTCRMContactMethodBObj();

			if (StringUtils.isNonBlank(partyContactMethodInput
					.getPartyContactMethodIdPK())) {
				TCRMPartyContactMethodBObj partyContactMethodDB = partyComp
						.getPartyContactMethodByIdPK(partyContactMethodInput
								.getPartyContactMethodIdPK(),
								partyContactMethodInput.getControl());

				if (partyContactMethodDB != null) {
					IContactMethod contactMethodComp = (IContactMethod) TCRMClassFactory
							.getTCRMComponent(TCRMCorePropertyKeys.CONTACTMETHOD_COMPONENT);
					TCRMContactMethodBObj contactMethodDB = contactMethodComp
							.getContactMethod(partyContactMethodDB
									.getContactMethodId(), partyContactMethodDB
									.getControl());

					partyContactMethodDB
							.setTCRMContactMethodBObj(contactMethodDB);

					if (StringUtils.compareWithTrim(contactMethodInput
							.getContactMethodType(),
							CTCConstants.CONT_METH_TP_PHONE)) {
						CTCCommonUtil
								.standardizeContactMethod(contactMethodInput);
					}
					if (!isContactMethodSame(contactMethodDB,
							contactMethodInput)) {
						CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
						criticalDataChanged
								.setBeforeUpdate(partyContactMethodDB);
						criticalDataChanged
								.setAfterUpdate(partyContactMethodInput);
						criticalDataChanged
								.setElementName(TCRMPARTY_CONTACT_METHOD_BOBJ);
						vecCriticalData.addElement(criticalDataChanged);
					}
				} else {
					CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
					criticalDataChanged.setBeforeUpdate(null);
					criticalDataChanged.setAfterUpdate(partyContactMethodDB);
					criticalDataChanged
							.setElementName(TCRMPARTY_CONTACT_METHOD_BOBJ);
					vecCriticalData.addElement(criticalDataChanged);
				}
			} else { // else it is a new party address
				CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
				criticalDataChanged.setBeforeUpdate(null);
				criticalDataChanged
						.setAfterUpdate((TCRMPartyContactMethodBObj) tcrmCommon);
				criticalDataChanged
						.setElementName(TCRMPARTY_CONTACT_METHOD_BOBJ);
				vecCriticalData.addElement(criticalDataChanged);

			}

		}
		if (isPerson) {
			TCRMPersonBObj p2 = (TCRMPersonBObj) tcrmCommon;

			if (StringUtils.isNonBlank(p2.getPartyId())) {
				TCRMPersonBObj p1 = (TCRMPersonBObj) partyComp.getParty(p2
						.getPartyId(), TCRMCoreInquiryLevel.INQUIRY_LEVEL_1,
						((TCRMPersonBObj) tcrmCommon).getControl());

				// ***************** for new party ***************************
				if (p1 == null) {
					// party address
					Vector<TCRMPartyAddressBObj> vecPartyAddr = p2
							.getItemsTCRMPartyAddressBObj();

					if (CTCCommonUtil.isNotEmpty(vecPartyAddr)) {
						for (TCRMPartyAddressBObj objPartyAddress : vecPartyAddr) {
							CriticalDataChangedElement criticalData = new CriticalDataChangedElement();
							criticalData.setBeforeUpdate(null);
							criticalData.setAfterUpdate(objPartyAddress);
							criticalData.setElementName(TCRMPARTY_ADDRESS_BOBJ);
							vecCriticalData.addElement(criticalData);
						}
					}

					// person name
					Vector<TCRMPersonNameBObj> vecPersonName = p2
							.getItemsTCRMPersonNameBObj();
					if (CTCCommonUtil.isNotEmpty(vecPersonName)) {
						for (TCRMPersonNameBObj objPersonName : vecPersonName) {
							CriticalDataChangedElement criticalData = new CriticalDataChangedElement();
							criticalData.setBeforeUpdate(null);
							criticalData.setAfterUpdate(objPersonName);
							criticalData.setElementName(TCRMPERSON_NAME_BOBJ);
							vecCriticalData.addElement(criticalData);
						}
					}

					// contact method
					Vector<TCRMPartyContactMethodBObj> vecPartyContactMethod = p2
							.getItemsTCRMPartyContactMethodBObj();
					if (CTCCommonUtil.isNotEmpty(vecPartyContactMethod)) {
						for (TCRMPartyContactMethodBObj objPartyContactMethod : vecPartyContactMethod) {
							CriticalDataChangedElement criticalData = new CriticalDataChangedElement();

							criticalData.setBeforeUpdate(null);
							criticalData.setAfterUpdate(objPartyContactMethod);
							criticalData
									.setElementName(TCRMPARTY_CONTACT_METHOD_BOBJ);
							vecCriticalData.addElement(criticalData);
						}
					}

					debugOut(debugStr
							+ " ********************this is a new person. ");
				} else {
					// Existing Party
					// handle person business object itself.
					CriticalDataChangedElement criticalData = personIsSame(p1,
							p2);

					if (criticalData != null) {
						vecCriticalData.addElement(criticalData);
					}

					// handle person name changes
					for (TCRMPersonNameBObj personNameInput : (Vector<TCRMPersonNameBObj>) p2
							.getItemsTCRMPersonNameBObj()) {

						// for newly added data
						if (StringUtils.isBlank(personNameInput
								.getPersonNameIdPK())) {

							criticalData = new CriticalDataChangedElement();
							criticalData.setBeforeUpdate(null);
							criticalData.setAfterUpdate(personNameInput);
							criticalData.setElementName(TCRMPERSON_NAME_BOBJ);
							vecCriticalData.addElement(criticalData);

						} else { // for update data
							for (TCRMPersonNameBObj personNameDB : (Vector<TCRMPersonNameBObj>) p1
									.getItemsTCRMPersonNameBObj()) {

								if (StringUtils.compareWithTrim(personNameDB
										.getPersonNameIdPK(), personNameInput
										.getPersonNameIdPK())) {
									// type is different, critical data must
									// have been changed.
									if (!StringUtils.compareWithTrim(
											personNameDB.getNameUsageType(),
											personNameInput.getNameUsageType())) {
										criticalData = new CriticalDataChangedElement();
										criticalData
												.setAfterUpdate(personNameInput);
										criticalData
												.setBeforeUpdate(personNameDB);
										criticalData
												.setElementName(TCRMPERSON_NAME_BOBJ);
										vecCriticalData
												.addElement(criticalData);

									} else {
										// Check Person Name Data

										CTCCommonUtil
												.standardizePersonName(personNameInput);

										CriticalDataChangedElement cdata = personNameIsSame(
												personNameDB, personNameInput);

										if (cdata != null) {
											vecCriticalData.addElement(cdata);

										}
									}

									break;
								}
							}
						}
					}

					// handles party address
					for (TCRMPartyAddressBObj partyAddressInput : (Vector<TCRMPartyAddressBObj>) p2
							.getItemsTCRMPartyAddressBObj()) {

						if (StringUtils.isBlank(partyAddressInput
								.getPartyAddressIdPK())) {
							CriticalDataChangedElement cdata = new CriticalDataChangedElement();
							cdata.setBeforeUpdate(null);
							cdata.setAfterUpdate(partyAddressInput);
							cdata.setElementName(TCRMPARTY_ADDRESS_BOBJ);
							vecCriticalData.addElement(cdata);
						} else {
							for (TCRMPartyAddressBObj partyAddressDB : (Vector<TCRMPartyAddressBObj>) p1
									.getItemsTCRMPartyAddressBObj()) {

								if (StringUtils
										.compareWithTrim(partyAddressDB
												.getPartyAddressIdPK(),
												partyAddressInput
														.getPartyAddressIdPK())) {
									TCRMAddressBObj addressDB = (TCRMAddressBObj) partyAddressDB
											.getTCRMAddressBObj();
									TCRMAddressBObj addressInput = (TCRMAddressBObj) partyAddressInput
											.getTCRMAddressBObj();

									if (!StringUtils.compareWithTrim(
											partyAddressDB
													.getAddressUsageType(),
											partyAddressInput
													.getAddressUsageType())) {
										CriticalDataChangedElement cdata = new CriticalDataChangedElement();
										cdata.setBeforeUpdate(partyAddressDB);
										cdata.setAfterUpdate(partyAddressInput);
										cdata
												.setElementName(TCRMPARTY_ADDRESS_BOBJ);
									} else {
										CTCCommonUtil
												.standardizeAddress(addressInput);
										if (!isAddressSame(addressDB,
												addressInput)) {
											CriticalDataChangedElement criticalDataChanged 
												= new CriticalDataChangedElement();
											criticalDataChanged
													.setBeforeUpdate(partyAddressDB);
											criticalDataChanged
													.setAfterUpdate(partyAddressInput);
											criticalDataChanged
													.setElementName(TCRMPARTY_ADDRESS_BOBJ);
											vecCriticalData
													.addElement(criticalDataChanged);
										}
									}

									break;
								}
							}
						}
					}

					// handles party contact method
					for (TCRMPartyContactMethodBObj partyContactMethodInput 
							: (Vector<TCRMPartyContactMethodBObj>) p2
								.getItemsTCRMPartyContactMethodBObj()) {

						if (StringUtils.isBlank((partyContactMethodInput
								.getPartyContactMethodIdPK()))) {
							CriticalDataChangedElement cdata = new CriticalDataChangedElement();
							cdata.setBeforeUpdate(null);
							cdata.setAfterUpdate(partyContactMethodInput);
							cdata.setElementName(TCRMPARTY_CONTACT_METHOD_BOBJ);
							vecCriticalData.addElement(cdata);
						} else {
							for (TCRMPartyContactMethodBObj partyContactMethodDB
							 	: (Vector<TCRMPartyContactMethodBObj>) p1
									.getItemsTCRMPartyContactMethodBObj()) {
								if (StringUtils.compareWithTrim(
										partyContactMethodDB
												.getPartyContactMethodIdPK(),
										partyContactMethodInput
												.getPartyContactMethodIdPK())) {
									TCRMContactMethodBObj contactMethodDB 
										= (TCRMContactMethodBObj) partyContactMethodDB
											.getTCRMContactMethodBObj();
									TCRMContactMethodBObj contactMethodInput 
										= (TCRMContactMethodBObj) partyContactMethodInput
											.getTCRMContactMethodBObj();

									if (!StringUtils
											.compareWithTrim(
													partyContactMethodDB
															.getContactMethodUsageType(),
													partyContactMethodInput
															.getContactMethodUsageType())) {
										CriticalDataChangedElement cdata = new CriticalDataChangedElement();
										cdata
												.setBeforeUpdate(partyContactMethodDB);
										cdata
												.setAfterUpdate(partyContactMethodInput);
										cdata
												.setElementName(TCRMPARTY_CONTACT_METHOD_BOBJ);
									} else {
										// if contactmethod is phone

										if (StringUtils
												.compareWithTrim(
														contactMethodInput
																.getContactMethodType(),
														CTCConstants.CONT_METH_TP_PHONE)) {
											CTCCommonUtil
													.standardizeContactMethod(contactMethodInput);
										}
										if (!isContactMethodSame(
												contactMethodDB,
												contactMethodInput)) {
											CriticalDataChangedElement criticalDataChanged = new CriticalDataChangedElement();
											criticalDataChanged
													.setBeforeUpdate(partyContactMethodDB);
											criticalDataChanged
													.setAfterUpdate(partyContactMethodInput);
											criticalDataChanged
													.setElementName(TCRMPARTY_CONTACT_METHOD_BOBJ);
											vecCriticalData
													.addElement(criticalDataChanged);
										}
									}

									break;
								}
							}
						}
					}

				}
			}
		}

		debugOut("criticalDateVec size " + vecCriticalData.size());

		return vecCriticalData;

	}

	/**
	 * Compare two person's birthdate, if they are same, return null; otherwise,
	 * return an instance of CriticalDataChangedElement with personBObj1 and
	 * personBObj1.
	 * 
	 * @param personBObj1
	 * @param personBObj2
	 * @return An instance of CriticalDataChangedElement with personBObj1 and
	 *         personBObj2
	 * @throws Exception
	 */
	protected CriticalDataChangedElement personIsSame(
			TCRMPersonBObj personBObj1, TCRMPersonBObj personBObj2)
			throws Exception {

		boolean birthDateSame = true;
		CriticalDataChangedElement criticalData = null;

		// set the clone p1 and p2 to add to the criticalDataElement.
		TCRMPersonBObj cloneP1 = new TCRMPersonBObj();
		cloneP1.setPartyId(personBObj1.getPartyId());

		TCRMPersonBObj cloneP2 = new TCRMPersonBObj();
		cloneP2.setPartyId(personBObj2.getPartyId());

		if (!StringUtils.compareIgnoreCaseWithTrim(personBObj1.getBirthDate(),
				personBObj2.getBirthDate())) {
			if (StringUtils.isNonBlank(personBObj1.getBirthDate())) {
				cloneP1.setBirthDate(personBObj1.getBirthDate());
				cloneP1.setPersonPartyId(personBObj1.getPersonPartyId());
			}

			if (StringUtils.isNonBlank(personBObj2.getBirthDate())) {
				cloneP2.setBirthDate(personBObj2.getBirthDate());
				cloneP2.setPersonPartyId(personBObj2.getPersonPartyId());
			}

			birthDateSame = false;
		}

		if (birthDateSame) {
			return null;
		} else {
			criticalData = new CriticalDataChangedElement();
			criticalData.setBeforeUpdate(cloneP1);
			criticalData.setAfterUpdate(cloneP2);
			criticalData.setElementName(TCRMPERSON_BOBJ);

			if (logger.isFineEnabled()) {
				logger.fine("birthdate are not same");
			}
		}

		return criticalData;
	}

	/**
	 * @param personNameBObj1
	 *            the person name needed to compare
	 * @param personNameBObj2
	 *            the person name needed to compare
	 * @return null if two names are the same; otherwise, return instance of
	 *         CriticalDataChangedElement holding personNameBObj1 and
	 *         personNameBObj2
	 */
	protected CriticalDataChangedElement personNameIsSame(
			TCRMPersonNameBObj personNameBObj1,
			TCRMPersonNameBObj personNameBObj2) {
		boolean firstNameSame = false;
		boolean lastNameSame = false;
		boolean generationSame = false;

		CriticalDataChangedElement criticalData = null;

		if (personNameBObj1 == null || personNameBObj2 == null)
			return null;

		if (StringUtils.compareWithTrim(personNameBObj1.getStdLastName(),
				personNameBObj2.getStdLastName())) {
			lastNameSame = true;
		}

		if (StringUtils.compareWithTrim(personNameBObj1.getStdGivenNameOne(),
				personNameBObj2.getStdGivenNameOne())) {
			firstNameSame = true;
		}

		if (StringUtils.compareWithTrim(personNameBObj1.getGenerationType(),
				personNameBObj2.getGenerationType())) {
			generationSame = true;
		}

		if (firstNameSame && lastNameSame && generationSame) {
			return null;
		} else {
			criticalData = new CriticalDataChangedElement();
			criticalData.setBeforeUpdate(personNameBObj1);
			criticalData.setAfterUpdate(personNameBObj2);
			criticalData.setElementName(TCRMPERSON_NAME_BOBJ);
		}

		return criticalData;
	}

	/**
	 * Compare two addressBObj (address line 1, city, zipPostalCode,
	 * provinceStateType to see if they are the same or not.
	 * 
	 * @param addressBObj1
	 * @param addressBObj2
	 * 
	 */
	protected boolean isAddressSame(TCRMAddressBObj addressBObj1,
			TCRMAddressBObj addressBObj2) {
		boolean addressSame = false;

		if (StringUtils.compareWithTrim(addressBObj1.getAddressLineOne(),
				addressBObj2.getAddressLineOne())
				&& StringUtils.compareWithTrim(addressBObj1.getCity(),
						addressBObj2.getCity())
				&& StringUtils.compareWithTrim(addressBObj1.getZipPostalCode(),
						addressBObj2.getZipPostalCode())
				&& StringUtils.compareWithTrim(addressBObj1
						.getProvinceStateType(), addressBObj2
						.getProvinceStateType())) {
			addressSame = true;

		}
		return addressSame;
	}

	/**
	 * Compare two contactMethodBObj to see if they are the same or not.
	 * 
	 * @param contactMethodBObj1
	 * @param contactMethodBObj2
	 * 
	 */
	protected boolean isContactMethodSame(
			TCRMContactMethodBObj contactMethodBObj1,
			TCRMContactMethodBObj contactMethodBObj2) {
		boolean contactMethodSame = false;

		if (StringUtils.compareWithTrim(
				contactMethodBObj1.getReferenceNumber(), contactMethodBObj2
						.getReferenceNumber())) {
			contactMethodSame = true;

		}
		return contactMethodSame;
	}

	/**
	 * @param s
	 *            Output the debug information to log file if the log level is
	 *            set to fine.
	 */
	protected void debugOut(String s) {
		if (logger.isFineEnabled()) {
			logger.fine(s);
		}
	}

}
