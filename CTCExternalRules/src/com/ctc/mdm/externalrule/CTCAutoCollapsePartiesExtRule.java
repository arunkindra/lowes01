// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCA2SuspectsActionRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// *******************************************************************************

package com.ctc.mdm.externalrule;

import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import com.ctc.mdm.common.util.CTCCommonUtil;
import com.dwl.base.externalrule.Rule;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.StringUtils;
import com.dwl.tcrm.coreParty.component.TCRMConsolidatedPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyListBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.utilities.TCRMClassFactory;

/**
 * This rule Automatically collapse A1 Suspects when the suspects are
 * encountered after an update.
 * 
 */
public class CTCAutoCollapsePartiesExtRule extends Rule {

	public static final String copyright = "Licensed Materials -- Property "
			+ "of IBM\n(c) Copyright IBM Corp. 2002, 2009\nUS Government Users "
			+ "Restricted Rights - Use, duplication or disclosure restricted by GSA ADP "
			+ "Schedule Contract with IBM Corp.";

	protected final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCAutoCollapsePartiesExtRule.class);

	protected String ruleName = "CTCAutoCollapsePartiesExtRule";

	protected String debugStr = "External Java Rule - " + ruleName + ": ";

	/**
	 * 
	 * 
	 * @return java.lang.Object
	 * @param input
	 *            java.lang.Object
	 * @param componentObject
	 *            java.lang.Object
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {
		if (logger.isFineEnabled()) {
			logger.fine(debugStr + "Entering Rule");
		}

		Vector<TCRMPartyBObj> vecParties = null;
		TCRMPartyBObj newParty = null;
		TCRMPartyBObj theTCRMPartyBObj = null;
		TCRMPartyBObj theCollapsePartyBObj = null;

		try {

			if (input instanceof Vector) {
				vecParties = (Vector) input;
			}

			if (vecParties != null) {
				theTCRMPartyBObj = (TCRMPartyBObj) vecParties.elementAt(0);
				theCollapsePartyBObj = (TCRMPartyBObj) vecParties.elementAt(1);
			}

			Vector<TCRMSuspectBObj> vecSuspect = theTCRMPartyBObj
					.getItemsTCRMSuspectBObj();

			TCRMPartyListBObj a1SuspectList = new TCRMPartyListBObj();

			if ((vecSuspect != null) && (vecSuspect.size() > 0)) {

				Set<TCRMSuspectBObj> uniqueSuspectsSet = new HashSet<TCRMSuspectBObj>();
				for (TCRMSuspectBObj suspect : vecSuspect) {
					
					if(uniqueSuspectsSet.contains(suspect)){
						continue;
					}
					else
					{
						uniqueSuspectsSet.add(suspect);
					}
					TCRMPartyBObj partyBObj = null;
					boolean suspectTypeA1 = false;
					if(StringUtils.isNonBlank(suspect.getAdjustedMatchCategoryCode())){
						if(StringUtils.compareIgnoreCaseWithTrim(suspect.getAdjustedMatchCategoryCode(), "A1")){
							suspectTypeA1 = true;
						}
					}
					else if(StringUtils.isNonBlank(suspect.getMatchCategoryCode())){
						if(StringUtils.compareIgnoreCaseWithTrim(suspect.getMatchCategoryCode(), "A1")){
							suspectTypeA1 = true;
						}
					}
					
					if(suspectTypeA1){
						if (theTCRMPartyBObj.getPartyId().equalsIgnoreCase(
								suspect.getPartyId())) {

							partyBObj = new TCRMPartyBObj();
							partyBObj.setPartyId(suspect.getSuspectPartyId());
							partyBObj.setControl(theTCRMPartyBObj.getControl());

						} else {

							partyBObj = new TCRMPartyBObj();
							partyBObj.setPartyId(suspect.getPartyId());
							partyBObj.setControl(theTCRMPartyBObj.getControl());

						}

						a1SuspectList.setTCRMPartyBObj(partyBObj);
					}

				}

				a1SuspectList.setTCRMPartyBObj(theTCRMPartyBObj);
				a1SuspectList.setControl(theTCRMPartyBObj.getControl());

				if (CTCCommonUtil.isNotEmpty(a1SuspectList
						.getItemsTCRMPartyBObj())) {
					if (a1SuspectList.getItemsTCRMPartyBObj().size() > 1) {
						TCRMConsolidatedPartyBObj objTCRMConsolidatedPartyBObj = new TCRMConsolidatedPartyBObj();
						objTCRMConsolidatedPartyBObj
								.setTCRMPartyListBObj(a1SuspectList);
						objTCRMConsolidatedPartyBObj
								.setControl(theTCRMPartyBObj.getControl());

						IParty partyComp = (IParty) TCRMClassFactory
								.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);

						TCRMConsolidatedPartyBObj objResTCRMConsolidatedPartyBObj = partyComp
								.collapseMultipleParties(objTCRMConsolidatedPartyBObj);

						if (objResTCRMConsolidatedPartyBObj != null
								&& objResTCRMConsolidatedPartyBObj
										.getTCRMPartyBObj() != null) {

							newParty = objResTCRMConsolidatedPartyBObj
									.getTCRMPartyBObj();
							newParty.setControl(objResTCRMConsolidatedPartyBObj
									.getControl());
						}

					}
				}

			}

		} catch (Exception ex) {
			throw ex;
		}

		return newParty;
	}

}
