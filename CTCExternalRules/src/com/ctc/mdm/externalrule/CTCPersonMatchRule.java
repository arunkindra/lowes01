// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCPersonMatchRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.util.CTCCommonUtil;
import com.dwl.base.externalrule.Rule;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.StringUtils;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectBObj;
import com.dwl.tcrm.exception.TCRMException;

/**
 * @author Infotrellis 
 * This class extends the class CTCPartyMatchRule . It has
 * one execute() method which returns an Object type. The purpose of
 * this rule is to generate the suspect object based on match and non
 * match relevance score. The match and non match relevance score is
 * calculated based on person name, birth date, address, phone number,
 * and email.
 */
public class CTCPersonMatchRule extends Rule {

	private int matchScore = 0;
	private int nonMatchScore = 0;

	protected final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCPersonMatchRule.class);

	/**
	 * This method invokes matchPersonName(),matchBirthDate(),
	 * matchStandardizedAddress(), matchPhoneNumber()and matchEmail() methods.
	 * It returns an suspect object.
	 * 
	 * @param input
	 *            - Object
	 * @param componentObject
	 *            - Object
	 * 
	 **/
	@SuppressWarnings("unchecked")
	@Override
	public Object execute(Object input, Object componentObject)
			throws Exception {

		logger.info("Entering into CTCPersonMatchRule.execute()");

		TCRMSuspectBObj suspect = new TCRMSuspectBObj();
		Vector v = (Vector) input;

		TCRMPersonBObj partyA = ((TCRMPersonBObj) v.elementAt(0));

		TCRMPersonBObj partyB = ((TCRMPersonBObj) v.elementAt(1));

		matchPersonName(partyA, partyB);

		matchBirthDate(partyA, partyB);

		matchStandardizedAddress(partyA, partyB);

		matchPhoneNumber(partyA, partyB);

		matchEmail(partyA, partyB);

		Integer intMatchScore = new Integer(matchScore);
		Integer intNonMatchScore = new Integer(nonMatchScore);
		suspect.setMatchRelevencyScore(intMatchScore.toString());
		suspect.setNonMatchRelevencyScore(intNonMatchScore.toString());

		logger.info("Exiting from CTCPersonMatchRule.execute()");

		return suspect;

	}

	/**
	 * This method matches birth Date and generates the matchScore and
	 * nonMatchScore accordingly.
	 * 
	 * @param partyA
	 *            - TCRMPersonBObj
	 * @param partyB
	 *            - TCRMPersonBObj
	 */

	private void matchBirthDate(TCRMPersonBObj partyA, TCRMPersonBObj partyB) {
		logger.info("Entering into CTCPersonMatchRule.matchBirthDate()");

		int birthDateMatchScore = 512;
		int birthDateNonMatchScore = 512;
		int birthDateNeutralScore = 80000;

		if (StringUtils.isNonBlank(partyA.getBirthDate())
				&& StringUtils.isNonBlank(partyB.getBirthDate())) {
			if (partyA.getBirthDate().equalsIgnoreCase(partyB.getBirthDate())) {
				matchScore = matchScore + birthDateMatchScore;

			} else {
				nonMatchScore = nonMatchScore + birthDateNonMatchScore;

			}
		} else {
			nonMatchScore = nonMatchScore + birthDateNeutralScore;
		}
		logger.info("Exiting from CTCPersonMatchRule.matchBirthDate()");
	}

	/**
	 * 
	 * This method matches person name and generates the matchScore and
	 * nonMatchScore accordingly.
	 * 
	 * @param partyA
	 *            - TCRMPersonBObj
	 * @param partyB
	 *            - TCRMPersonBObj
	 * @throws TCRMException
	 */

	private void matchPersonName(TCRMPersonBObj partyA, TCRMPersonBObj partyB)
			throws TCRMException {

		logger.info("Entering into CTCPersonMatchRule.matchPersonName()");

		int firstNameMatchScore = 1;
		int firstNameNonMatchScore = 1;
		int lastNameMatchScore = 2;
		int lastNameNonMatchScore = 2;
		int generationMatchScore = 4;
		int generationNonMatchScore = 4;
		int generationNeutralScore = 10000;

		boolean matchingLastNameFound = false;
		boolean matchingFirstNameFound = false;
		boolean matchingGenerationFound = false;
		boolean partyALastNameProvided = false;
		boolean partyBLastNameProvided = false;
		boolean partyAGivenNameOneProvided = false;
		boolean partyBGivenNameOneProvided = false;
		boolean partyAGenerationProvided = false;
		boolean partyBGenerationProvided = false;
		boolean allMatched = false;
		boolean firstNameAndGenerationMatched = false;
		int i, j, sizePersonNameA, sizePersonNameB;

		/**
		 * 1st loop starting.. This loop is used to process each name of a
		 * person provided as partyA.
		 **/
		sizePersonNameA = partyA.getItemsTCRMPersonNameBObj().size();
		sizePersonNameB = partyB.getItemsTCRMPersonNameBObj().size();

		for (i = 0; i < sizePersonNameA; i++) {
			TCRMPersonNameBObj partyAName = ((TCRMPersonNameBObj) partyA
					.getItemsTCRMPersonNameBObj().elementAt(i));
			partyAName = CTCCommonUtil.standardizePersonName(partyAName);
			if (StringUtils.isNonBlank(partyAName.getStdLastName())) {
				partyALastNameProvided = true;
			}

			/**
			 * 2nd loop starting.. This loop is used to process each name of a
			 * person provided as partyB.
			 */

			for (j = 0; j < sizePersonNameB; j++) {
				TCRMPersonNameBObj partyBName = ((TCRMPersonNameBObj) partyB
						.getItemsTCRMPersonNameBObj().elementAt(j));
				partyBName = CTCCommonUtil.standardizePersonName(partyBName);

				if (StringUtils.isNonBlank(partyBName.getStdLastName())) {
					partyBLastNameProvided = true;
				}

				/** Matching the last name of partyA and partyB. */

				if (partyALastNameProvided
						&& partyBLastNameProvided
						&& StringUtils.compareWithTrim(partyAName
								.getStdLastName(), partyBName.getStdLastName())) {

					matchingLastNameFound = true;

					if (StringUtils.isNonBlank(partyAName.getStdGivenNameOne())) {
						partyAGivenNameOneProvided = true;
					}

					if (StringUtils.isNonBlank(partyBName.getStdGivenNameOne())) {
						partyBGivenNameOneProvided = true;
					}

					if (partyAGivenNameOneProvided
							&& partyAGivenNameOneProvided
							&& StringUtils.compareWithTrim(partyAName
									.getStdGivenNameOne(), partyBName
									.getStdGivenNameOne())) {
						matchingFirstNameFound = true;
						partyAGenerationProvided = false;
						partyBGenerationProvided = false;
						matchingGenerationFound = false;

						if (StringUtils.isNonBlank(partyAName
								.getGenerationType())) {
							partyAGenerationProvided = true;
						}

						if (StringUtils.isNonBlank(partyBName
								.getGenerationType())) {
							partyBGenerationProvided = true;
						}

						if (partyAGenerationProvided
								&& partyBGenerationProvided) {
							if (StringUtils.compareWithTrim(partyAName
									.getGenerationType(), partyBName
									.getGenerationType())) {
								matchingGenerationFound = true;
								allMatched = true;
								break;
							}
						}
					} else {
						if (!matchingFirstNameFound) {
							if (StringUtils.isNonBlank(partyAName
									.getGenerationType())) {
								partyAGenerationProvided = true;
							}

							if (StringUtils.isNonBlank(partyBName
									.getGenerationType())) {
								partyBGenerationProvided = true;
							}

							if (partyAGenerationProvided
									&& partyBGenerationProvided) {
								if (StringUtils.compareWithTrim(partyAName
										.getGenerationType(), partyBName
										.getGenerationType())) {
									matchingGenerationFound = true;

								}
							}
						}

					}
				}
				
			}
			if (allMatched == true) {
				break;

			}
		}
		if (matchingLastNameFound) {

			matchScore = matchScore + lastNameMatchScore;

			if (partyAGivenNameOneProvided && partyBGivenNameOneProvided) {

				if (matchingFirstNameFound) {
					matchScore = matchScore + firstNameMatchScore;

				} else {
					nonMatchScore = nonMatchScore + firstNameNonMatchScore;
				}
			}

			if (partyAGenerationProvided && partyBGenerationProvided) {

				if (matchingGenerationFound) {
					matchScore = matchScore + generationMatchScore;
				} else {
					nonMatchScore = nonMatchScore + generationNonMatchScore;
				}
			} else {
				nonMatchScore = nonMatchScore + generationNeutralScore;
			}
		}

		else {

			if (partyALastNameProvided && partyBLastNameProvided) {

				nonMatchScore = nonMatchScore + lastNameNonMatchScore;

			}

			for (i = 0; i < sizePersonNameA; i++) {
				TCRMPersonNameBObj partyAName = ((TCRMPersonNameBObj) partyA
						.getItemsTCRMPersonNameBObj().elementAt(i));
				
				if (StringUtils.isNonBlank(partyAName.getStdGivenNameOne())) {
					partyAGivenNameOneProvided = true;
				}
				for (j = 0; j < sizePersonNameB; j++) {
					TCRMPersonNameBObj partyBName = ((TCRMPersonNameBObj) partyB
							.getItemsTCRMPersonNameBObj().elementAt(j));


					if (StringUtils.isNonBlank(partyBName.getStdGivenNameOne())) {
						partyBGivenNameOneProvided = true;
					}

					if (partyAGivenNameOneProvided
							&& partyAGivenNameOneProvided
							&& StringUtils.compareWithTrim(partyAName
									.getStdGivenNameOne(), partyBName
									.getStdGivenNameOne())) {
						matchingFirstNameFound = true;
						matchingGenerationFound = false;
						partyAGenerationProvided = false;
						partyBGenerationProvided = false;

						if (StringUtils.isNonBlank(partyAName
								.getGenerationType())) {
							partyAGenerationProvided = true;
						}

						if (StringUtils.isNonBlank(partyBName
								.getGenerationType())) {
							partyBGenerationProvided = true;
						}

						if (partyAGenerationProvided
								&& partyBGenerationProvided) {
							if (StringUtils.compareWithTrim(partyAName
									.getGenerationType(), partyBName
									.getGenerationType())) {
								matchingGenerationFound = true;
								firstNameAndGenerationMatched = true;
								break;
							}
						}
					} else {
						if (!matchingFirstNameFound) {
							if (StringUtils.isNonBlank(partyAName
									.getGenerationType())) {
								partyAGenerationProvided = true;
							}

							if (StringUtils.isNonBlank(partyBName
									.getGenerationType())) {
								partyBGenerationProvided = true;
							}

							if (partyAGenerationProvided
									&& partyBGenerationProvided) {
								if (StringUtils.compareWithTrim(partyAName
										.getGenerationType(), partyBName
										.getGenerationType())) {
									matchingGenerationFound = true;

								}
							}
						}
					}
				}
				if (firstNameAndGenerationMatched) {
					break;
				}

			}

			if (partyAGivenNameOneProvided && partyBGivenNameOneProvided) {
				if (matchingFirstNameFound) {
					matchScore = matchScore + firstNameMatchScore;
				} else {
					matchScore = matchScore + firstNameNonMatchScore;
				}
			}
			if (partyAGenerationProvided && partyBGenerationProvided) {

				if (matchingGenerationFound) {
					matchScore = matchScore + generationMatchScore;
				} else {
					nonMatchScore = nonMatchScore + generationNonMatchScore;
				}
			} else {
				nonMatchScore = nonMatchScore + generationNeutralScore;
			}

		}

		logger.info("Exiting from CTCPersonMatchRule.matchPersonName()");
	}

	/**
	 * 
	 * This method matches the standardized address and generates the matchScore
	 * and nonMatchScore accordingly.
	 * 
	 * @param partyA
	 * @param partyB
	 * @throws TCRMException
	 */

	private void matchStandardizedAddress(TCRMPersonBObj partyA,
			TCRMPersonBObj partyB) throws TCRMException {

		logger
				.info("Entering Into CTCPersonMatchRule.matchStandardizedAddress()");

		int addressLineOneNonMatchScore = 8;
		int addressLineOneMatchScore = 8;
		int cityMatchScore = 16;
		int cityNonMatchScore = 16;
		int postalCodeMatchScore = 32;
		int postalCodeNonMatchScore = 32;
		int postalCodeNeutralScore = 20000;
		int provinceStateMatchScore = 64;
		int provinceStateNonMatchScore = 64;

		int highestMatchScore = 0;
		int lowestNonMatchScore = 0;
		int neutralScore = 0;

		boolean allMatched = false;

		int i, j, sizePartyAAddress, sizePartyBAddress;
	
		boolean addressLineOneProvided = false;
		boolean addressLineTwoProvided = false;
		boolean cityProvided = false;
		boolean postalCodeProvided = false;
		boolean provinceStateProvided = false;
		boolean addressLineOneMatched = false;
		boolean addressLineTwoMatched = false;
		boolean cityMatched = false;
		boolean provinceStateMatched = false;
		boolean postalCodeMatched = false;
		
		boolean atLeastOneAddressPairProvided = false;

		sizePartyAAddress = partyA.getItemsTCRMPartyAddressBObj().size();
		sizePartyBAddress = partyB.getItemsTCRMPartyAddressBObj().size();

		for (i = 0; i < sizePartyAAddress; i++) {

			// resetting fields
			addressLineOneProvided = false;
			addressLineTwoProvided = false;
			cityProvided = false;
			postalCodeProvided = false;
			provinceStateProvided = false;
			addressLineOneMatched = false;
			addressLineTwoMatched = false;
			cityMatched = false;
			provinceStateMatched = false;
			postalCodeMatched = false;

			TCRMPartyAddressBObj objTCRMPartyAAddressBobj = ((TCRMPartyAddressBObj) partyA
					.getItemsTCRMPartyAddressBObj().elementAt(i));
			TCRMAddressBObj partyAAddress = (TCRMAddressBObj) objTCRMPartyAAddressBobj
					.getTCRMAddressBObj();

			partyAAddress = CTCCommonUtil.standardizeAddress(partyAAddress);

			for (j = 0; j < sizePartyBAddress; j++) {

				atLeastOneAddressPairProvided = true;
				int addressMatchScore = 0;
				int addressNonMatchScore = 0;
				int addressNeutralScore = 0;

				TCRMPartyAddressBObj objTCRMPartyBAddress = ((TCRMPartyAddressBObj) partyB
						.getItemsTCRMPartyAddressBObj().elementAt(j));
				TCRMAddressBObj objBAddressBobj = (TCRMAddressBObj) objTCRMPartyBAddress
						.getTCRMAddressBObj();

				/** Condition to match the address line one. */

				if (StringUtils.isNonBlank(partyAAddress.getAddressLineOne())
						&& StringUtils.isNonBlank(objBAddressBobj
								.getAddressLineOne())) {
					addressLineOneProvided = true;
					if (partyAAddress.getAddressLineOne().equalsIgnoreCase(
							objBAddressBobj.getAddressLineOne())) {
						addressLineOneMatched = true;
					}
				}
				/** Condition to match the address line two. */

				if ((StringUtils.isNonBlank(partyAAddress.getAddressLineTwo()))
						&& (StringUtils.isNonBlank(objBAddressBobj
								.getAddressLineTwo()))) {
					addressLineTwoProvided = true;
					if (partyAAddress.getAddressLineTwo().equalsIgnoreCase(
							objBAddressBobj.getAddressLineTwo())) {
						addressLineTwoMatched = true;
					}
				}

				/** Condition to match the city. */

				if (StringUtils.isNonBlank(partyAAddress.getCity())
						&& StringUtils.isNonBlank(objBAddressBobj.getCity())) {
					cityProvided = true;
					if (partyAAddress.getCity().equalsIgnoreCase(
							objBAddressBobj.getCity())) {
						cityMatched = true;
					}
				}

				/** Condition to match the province State. */
				if (StringUtils
						.isNonBlank(partyAAddress.getProvinceStateType())
						&& StringUtils.isNonBlank(objBAddressBobj
								.getProvinceStateType())) {
					provinceStateProvided = true;
					if (partyAAddress.getProvinceStateType().equalsIgnoreCase(
							objBAddressBobj.getProvinceStateType())) {
						provinceStateMatched = true;
					}
				}

				/** Condition to the postal code. */

				if (StringUtils.isNonBlank(partyAAddress.getZipPostalCode())
						&& StringUtils.isNonBlank(objBAddressBobj
								.getZipPostalCode())) {
					postalCodeProvided = true;
					if (partyAAddress.getZipPostalCode().equalsIgnoreCase(
							objBAddressBobj.getZipPostalCode())) {
						postalCodeMatched = true;
					}
				}

				if (addressLineOneMatched && (addressLineTwoMatched || !addressLineTwoProvided)
						&& cityMatched && postalCodeMatched
						&& provinceStateMatched) {
					
						allMatched  = true;
						break;
					 
				}

				else {
					if (postalCodeMatched) {
						addressMatchScore = addressMatchScore
								+ postalCodeMatchScore;
					} else if (postalCodeProvided) {
						addressNonMatchScore = addressNonMatchScore
								+ postalCodeNonMatchScore;
					} else {
						addressNeutralScore = addressNeutralScore
								+ postalCodeNeutralScore;
					}

					if (cityMatched) {
						addressMatchScore = addressMatchScore + cityMatchScore;
					} else if (cityProvided) {
						addressNonMatchScore = addressNonMatchScore
								+ cityNonMatchScore;
					}

					if (provinceStateMatched) {
						addressMatchScore = addressMatchScore
								+ provinceStateMatchScore;
					} else if (provinceStateProvided) {
						addressNonMatchScore = addressNonMatchScore
								+ provinceStateNonMatchScore;
					}

					if (addressLineOneMatched) {
						if (addressLineTwoProvided) {
							if (addressLineTwoMatched) {
								addressMatchScore = addressMatchScore
										+ addressLineOneMatchScore;
							} else if (addressLineTwoProvided) {
								addressNonMatchScore = addressNonMatchScore
										+ addressLineOneNonMatchScore;
							}

						} else {
							addressMatchScore = addressMatchScore
									+ addressLineOneMatchScore;
						}
					}

					else if (addressLineOneProvided) {
						addressNonMatchScore = addressNonMatchScore
								+ addressLineOneNonMatchScore;
					}

				}

				if (j == 0 && i == 0) {
					highestMatchScore = addressMatchScore;
					lowestNonMatchScore = addressNonMatchScore;
					neutralScore = addressNeutralScore;
					continue;
				}

				if (addressMatchScore > highestMatchScore) {
					highestMatchScore = addressMatchScore;
					lowestNonMatchScore = addressNonMatchScore;
					neutralScore = addressNeutralScore;
				} else if (addressMatchScore == highestMatchScore) {

					if (addressNonMatchScore < lowestNonMatchScore) {

						highestMatchScore = addressMatchScore;
						lowestNonMatchScore = addressNonMatchScore;
						neutralScore = addressNeutralScore;
					} else if (addressNonMatchScore == lowestNonMatchScore) {

						if (addressNeutralScore < neutralScore) {
							highestMatchScore = addressMatchScore;
							lowestNonMatchScore = addressNonMatchScore;
							neutralScore = addressNeutralScore;
						}

					}

				}
			}

			if (allMatched) {
				highestMatchScore = addressLineOneMatchScore + cityMatchScore
						+ provinceStateMatchScore + postalCodeMatchScore;
				lowestNonMatchScore = 0;
				neutralScore = 0; 
				break;
			}
		}
		
		/*
		 * 2011-11-05, VijayB
		 * For those critical data without neutral scoring, the rule is if the data is provided 
		 * for both Parties and they are matching, then it is considered a match, 
		 * otherwise it is considered a mismatch.  
		 * Therefore, it is a mismatch if one of the following is true:
		 * �	The data for both Parties is provided and they don�t match
		 * �	The data is provided for one Party but not for the other
		 * �	The data is not provided for both Parties
		 * If a mismatch, the Non-Match Relevancy Score for that critical data will be added to the Non-Match Score.
		 * This applies to First Name, Address, and Phone Number.
		 */
		if(atLeastOneAddressPairProvided){
			matchScore = matchScore + highestMatchScore;
			nonMatchScore = nonMatchScore + lowestNonMatchScore + neutralScore;
		}
		else
		{
			nonMatchScore = nonMatchScore + addressLineOneNonMatchScore + cityNonMatchScore + postalCodeNonMatchScore + provinceStateNonMatchScore;
		}

		logger.info("Exiting from CTCPersonMatchRule.matchStandardizedAddress()");
	}

	/**
	 * This method matches the Email id and generates the matchScore and
	 * nonMatchScore accordingly.
	 * 
	 * @param partyA
	 * @param partyB
	 */
	private void matchEmail(TCRMPersonBObj partyA, TCRMPersonBObj partyB)
			throws Exception {
		logger.info("Entering into CTCPersonMatchRule.matchEmail()");

		int emailMatchScore = 128;
		int emailNonMatchScore = 128;
		int emailNeutralScore = 40000;

		int i, j, sizePartyAContMethod, sizePartyBContMethod;
		String emailA = null;
		String emailB = null;
		boolean matchFound = false;
		boolean partyAEmailProvided = false;
		boolean partyBEmailProvided = false;

		sizePartyAContMethod = partyA.getItemsTCRMPartyContactMethodBObj()
				.size();
		sizePartyBContMethod = partyB.getItemsTCRMPartyContactMethodBObj()
				.size();

		for (i = 0; i < sizePartyAContMethod; i++) {
			TCRMPartyContactMethodBObj objTCRMPartyAContactMethodBObj = (TCRMPartyContactMethodBObj) partyA
					.getItemsTCRMPartyContactMethodBObj().elementAt(i);
			TCRMContactMethodBObj objAContactMethodBobj = objTCRMPartyAContactMethodBObj
					.getTCRMContactMethodBObj();
			
			if (StringUtils.compareWithTrim(objAContactMethodBobj
					.getContactMethodType(), CTCConstants.CONT_METH_TP_EMAIL)) {
				emailA = objAContactMethodBobj.getReferenceNumber();
				partyAEmailProvided = true;
			}
			else
			{
				continue;
			}

			for (j = 0; j < sizePartyBContMethod; j++) {
				TCRMPartyContactMethodBObj tcrmPartyBContactMethodObj = (TCRMPartyContactMethodBObj) partyB
						.getItemsTCRMPartyContactMethodBObj().elementAt(j);
				TCRMContactMethodBObj objBContactMethod = tcrmPartyBContactMethodObj
						.getTCRMContactMethodBObj();

				if (StringUtils.compareWithTrim(objBContactMethod
						.getContactMethodType(), CTCConstants.CONT_METH_TP_EMAIL)) {
					emailB = objBContactMethod.getReferenceNumber();
					partyBEmailProvided = true;
				}
				else
				{
					continue;
				}

				if (partyAEmailProvided && partyBEmailProvided) {

					if (StringUtils.compareIgnoreCaseWithTrim(emailA, emailB)) {
						matchFound = true;
						break;
					}

				}

			}

			if (matchFound) {
				break;
			}
		}

		if (partyAEmailProvided && partyBEmailProvided) {
			if (matchFound) {
				matchScore = matchScore + emailMatchScore;

			} else {
				nonMatchScore = nonMatchScore + emailNonMatchScore;
			}
		} else {
			nonMatchScore = nonMatchScore + emailNeutralScore;
		}
		logger.info("Exiting from CTCPersonMatchRule.matchEmail()");

	}

	/**
	 * This method matches the phone number and generates the matchScore and
	 * nonMatchScore accordingly.
	 * 
	 * @param partyA
	 * @param partyB
	 * @throws TCRMException
	 */
	private void matchPhoneNumber(TCRMPersonBObj partyA, TCRMPersonBObj partyB)
			throws TCRMException {

		logger.info("Entering into CTCPersonMatchRule.matchPhoneNumber()");

		int phoneNumberMatchScore = 256;
		int phoneNumberNonMatchScore = 256;

		int i, j, sizePartyAContMethod, sizePartyBContMethod;

		boolean matchFound = false;

		boolean objAPhoneNumberFound = false;
		boolean objBPhoneNumberFound = false;
		String phoneNumberA = null;
		String phoneNumberB = null;
		boolean phoneNumberProvided = false;

		sizePartyAContMethod = partyA.getItemsTCRMPartyContactMethodBObj()
				.size();
		sizePartyBContMethod = partyB.getItemsTCRMPartyContactMethodBObj()
				.size();

		for (i = 0; i < sizePartyAContMethod; i++) {
			TCRMPartyContactMethodBObj objTCRMPartyAContactMethodBObj = (TCRMPartyContactMethodBObj) partyA
					.getItemsTCRMPartyContactMethodBObj().elementAt(i);
			TCRMContactMethodBObj objAContactMethodBobj = objTCRMPartyAContactMethodBObj
					.getTCRMContactMethodBObj();
			

			if (objAContactMethodBobj.getContactMethodType().equals(CTCConstants.CONT_METH_TP_PHONE)) {
				objAContactMethodBobj = CTCCommonUtil
					.standardizeContactMethod(objAContactMethodBobj);
				phoneNumberA = objAContactMethodBobj.getReferenceNumber();
				objAPhoneNumberFound = true;
			}
			else
			{
				continue;
			}

			for (j = 0; j < sizePartyBContMethod; j++) {
				matchFound = false;
				TCRMPartyContactMethodBObj objTCRMPartyBContactMethodBObj = (TCRMPartyContactMethodBObj) partyB
						.getItemsTCRMPartyContactMethodBObj().elementAt(j);
				TCRMContactMethodBObj objBContactMethodBobj = objTCRMPartyBContactMethodBObj
						.getTCRMContactMethodBObj();

				if (objBContactMethodBobj.getContactMethodType().equals(CTCConstants.CONT_METH_TP_PHONE)) {
					phoneNumberB = objBContactMethodBobj.getReferenceNumber();
					objBPhoneNumberFound = true;
				}
				else
				{
					continue;
				}

				if (objAPhoneNumberFound && objBPhoneNumberFound) {
					phoneNumberProvided = true;
					if (phoneNumberA.equals(phoneNumberB)) {
						matchFound = true;
						break;
					}

				}

			}

			if (matchFound) {
				break;
			}

		}
		
		/*
		 * 2011-11-05, VijayB
		 * For those critical data without neutral scoring, the rule is if the data is provided 
		 * for both Parties and they are matching, then it is considered a match, 
		 * otherwise it is considered a mismatch.  
		 * Therefore, it is a mismatch if one of the following is true:
		 * �	The data for both Parties is provided and they don�t match
		 * �	The data is provided for one Party but not for the other
		 * �	The data is not provided for both Parties
		 * If a mismatch, the Non-Match Relevancy Score for that critical data will be added to the Non-Match Score.
		 * This applies to First Name, Address, and Phone Number.
		 */
		if (phoneNumberProvided) {
			if (matchFound) {
				matchScore = matchScore + phoneNumberMatchScore;
			}

			else {
				nonMatchScore = nonMatchScore + phoneNumberNonMatchScore;
			}
		}
		else
		{
			nonMatchScore = nonMatchScore + phoneNumberNonMatchScore;
		}
		logger.info("Exiting from CTCPersonMatchRule.matchPhoneNumber()");
	}

}
