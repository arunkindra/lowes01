// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCA1SuspectsActionRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.sql.Timestamp;
import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.dwl.base.DWLControl;
import com.dwl.base.accessToken.AccessTokenCollection;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.management.config.client.Configuration;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyMatcherManager;
import com.dwl.tcrm.coreParty.component.TCRMSuspectBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectComponent;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.coreParty.interfaces.IPartyMatcher;
import com.dwl.tcrm.coreParty.interfaces.IPartyMatcherManager;
import com.dwl.tcrm.coreParty.interfaces.ISuspectProcessor;
import com.dwl.tcrm.coreParty.notification.A1PartySelectedNotification;
import com.dwl.tcrm.coreParty.notification.CustomerNotification;
import com.dwl.tcrm.coreParty.notification.SuspectIdentificationNotification;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.externalrule.A1SuspectsActionRule;
//import com.dwl.tcrm.util.SuspectProcessingUtil;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;
import com.ibm.mdm.domains.codetype.obj.SuspectStatusTypeBObj;

/**
 * An external rule which handles the A1 suspects and performs the appropriate
 * action.
 * 
 * It is called by <code>processSuspects()</code> of
 * <code>TCRMSuspectComponent</code>.
 * <p>
 * This default implementation does the following:
 * <ul>
 * <li>If persistDuplicateParties is turned off:
 * <ol>
 * <li>If more than one A1-matched party is found for the source party, mark
 * those that are not the best A1 match as suspects for the highest A1 match
 * suspect party.
 * 
 * If notification is ON, notifications are sent for the best matched A1 suspect
 * (A1PartySelectedNotification), and then other notifications are sent for the
 * suspects that are not the best matching A1
 * (SuspectIdentificationNotification).
 * <li>If only one A1-matched party is found, a notification is sent
 * (A1PartySelectedNotification). No suspect entries are created for this party.
 * </ol>
 * 
 * <li>If persistDuplicateParties is turned on:
 * <ol>
 * <li>If the source party has the A1 match (more than one or only one A1
 * match), the source party type is 10, then it will create a suspect record for
 * the best A1 matched party and set its suspect status type to 6
 * </ol>
 * </ul>
 * 
 * @since 5.0.1
 * @modelguid {DF5662D4-2D79-42D3-B28F-7A1B9A00208F}
 */
public class CTCA1SuspectsActionRule extends A1SuspectsActionRule {

	private static final String LANG_ID = "langId";

	private static final String CD_SUSPECT_STATUS_TP = "CdSuspectStatusTp";

	private static final String IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_REASON_TYPE_SYSTEM_MARKED = "/IBM/Party/SuspectProcessing/SuspectReasonType/systemMarked";

	private static final String INQUIRY_LEVEL_2 = "2";

	private static final String NT1 = "nt1";

	private static final String IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECT_DUPLICATE = "/IBM/Party/SuspectProcessing/SuspectStatusType/suspectDuplicate";

	private static final String USER_NAME = "userName";

	private static final String REQUEST_ID = "request_id";

	private static final String REQUEST_NAME = "request_name";

	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2004, 2011\nUS Go"
			+ "vernment Users Restricted Rights - Use, duplication or disclosure restricted by "
			+ "GSA ADP Schedule Contract with IBM Corp.";

	private static final IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCA1SuspectsActionRule.class);
	private static final String NOTIFICATION_ENABLED = "/IBM/DWLCommonServices/Notifications/enabled";
	private static final String PERSIST_DUPLICATE_PARTIES = "/IBM/Party/SuspectProcessing/PersistDuplicateParties/enabled";
	private static final String BEST_A1_PERSISTED = "10";
	private static final String BEST_FILITERED_SUSPECT_RULE_ID = "111";
	String rulesetName;
	String rulesetVersion;
	protected Configuration config;
	boolean coReturnMandatorySearchResults;
	boolean coDoNotfication;
	boolean debugOn;
	protected static final String ERROR_MESSAGE = "Error_Shared_Execute";
	protected String exceedA1Matches;
	protected ISuspectProcessor suspComp;
	private static final String A1_NOT_USED_BECAUSE_OF_ACCESS_TOKEN = "11";

	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {
		if (logger.isInfoEnabled()) {
			logger
					.info(ResourceBundleHelper
							.resolve(
									"com.dwl.tcrm.defaultExternalRules.nl.DefaultExternalRulesStrings",
									"Info_A1SuspectsActionRule_Fired"));
		}
		Vector vecInputElements = new Vector();
		TCRMPartyBObj theSourceTCRMPartyBObj = null;
		DWLControl theDWLControl = null;
		Vector<TCRMSuspectBObj> vecA1Suspects = new Vector();
		CustomerNotification theCustomerNotification = new CustomerNotification();
		try {
			if (input instanceof Vector) {
				vecInputElements = (Vector) input;
			}

			// 1. Initialise incoming arguments and components needed for this
			// rule

			suspComp = (ISuspectProcessor) TCRMClassFactory
					.getTCRMComponent(TCRMCorePropertyKeys.SUSPECT_COMPONENT);

			theSourceTCRMPartyBObj = (TCRMPartyBObj) vecInputElements
					.elementAt(0);

			vecA1Suspects = (Vector) vecInputElements.elementAt(1);

			theDWLControl = theSourceTCRMPartyBObj.getControl();
			coDoNotfication = Configuration.getConfiguration()
					.getConfigItem(NOTIFICATION_ENABLED,
							theDWLControl.retrieveConfigContext())
					.getBooleanValue();
			boolean coPersistDuplicateParties = Configuration
					.getConfiguration().getConfigItem(
							PERSIST_DUPLICATE_PARTIES,
							theDWLControl.retrieveConfigContext())
					.getBooleanValue();

			// Step 2.
			if (StringUtils.compareIgnoreCaseWithTrim(theSourceTCRMPartyBObj
					.getPartyType(), CTCConstants.PARTY_TYPE_ORG)) {
				return super.execute(input, componentObject);

			}
			// Step 3.
			
			/*
			 * Suspects needs to be re-identified for the A1 merged parties.
			 *   
			 */
			suspComp.reidentifySuspects(theSourceTCRMPartyBObj.getPartyId(), theSourceTCRMPartyBObj, "CriticalDataChanged", "Y", new Vector(), true);
			
//			if (vecA1Suspects.size() == 1) {
//				TCRMSuspectBObj a1MatchSuspectBObj = null;
//				TCRMPartyBObj theA1MatchedSuspectParty = null;
//				if (StringUtils.isNonBlank(theSourceTCRMPartyBObj
//						.getAddPartyStatus())
//						&& (coPersistDuplicateParties
//								&& theSourceTCRMPartyBObj.getAddPartyStatus()
//										.equals(BEST_A1_PERSISTED) || theSourceTCRMPartyBObj
//								.getAddPartyStatus().equals(
//										A1_NOT_USED_BECAUSE_OF_ACCESS_TOKEN))) {
//					// Create Suspect Record
//					a1MatchSuspectBObj = (TCRMSuspectBObj) vecA1Suspects
//							.elementAt(0);
//					if (!StringUtils.isNonBlank(a1MatchSuspectBObj
//							.getSuspectStatusType())) {
//						a1MatchSuspectBObj
//								.setSuspectStatusType(Configuration
//										.getConfiguration()
//										.getConfigItem(
//												IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECT_DUPLICATE)
//										.getValue());
//					}
//					createSuspectRecords(a1MatchSuspectBObj,
//							theSourceTCRMPartyBObj.getPartyId(),
//							theSourceTCRMPartyBObj.getControl(), suspComp);
//				}
//				// Notification
//				
//				a1MatchSuspectBObj = (TCRMSuspectBObj) vecA1Suspects
//						.elementAt(0);
//				theA1MatchedSuspectParty = a1MatchSuspectBObj
//						.getTCRMSuspectPersonBObj();
//				theA1MatchedSuspectParty
//						.setObjectReferenceId(theSourceTCRMPartyBObj
//								.getObjectReferenceId());
//				theA1MatchedSuspectParty.setControl(theDWLControl);
//				if (coDoNotfication) {
//					A1PartySelectedNotification theA1PartySelectedNotification = (A1PartySelectedNotification) (A1PartySelectedNotification) TCRMClassFactory
//							.getTCRMCommonNotification(NT1);
//					theA1PartySelectedNotification
//							.setTxnType((String) (String) theDWLControl
//									.get(REQUEST_NAME));
//					theA1PartySelectedNotification.setTxnTime(new Timestamp(
//							System.currentTimeMillis()));
//					theA1PartySelectedNotification.setRequestId(theDWLControl
//							.get(REQUEST_ID).toString());
//					theA1PartySelectedNotification
//							.setUserId((String) (String) theDWLControl
//									.get(USER_NAME));
//					theA1PartySelectedNotification
//							.setSourceParty(theSourceTCRMPartyBObj);
//					theA1PartySelectedNotification
//							.setA1PartyFound(theA1MatchedSuspectParty);
//					theA1PartySelectedNotification.setControl(theDWLControl);
//					theCustomerNotification
//							.sendNotification(theA1PartySelectedNotification);
//				}
//
//			}
//
//			// Step 4.
//
//			else if (vecA1Suspects.size() > 1) {
//				TCRMSuspectBObj theBestA1MatchedSuspect = null;
//				if (StringUtils.isNonBlank(theSourceTCRMPartyBObj
//						.getAddPartyStatus())
//						&& (coPersistDuplicateParties
//								&& theSourceTCRMPartyBObj.getAddPartyStatus()
//										.equals(BEST_A1_PERSISTED) || theSourceTCRMPartyBObj
//								.getAddPartyStatus().equals(
//										A1_NOT_USED_BECAUSE_OF_ACCESS_TOKEN))) {
//					TCRMSuspectBObj a1MatchSuspectBObj = null;
//					for (int i = 0; i < vecA1Suspects.size(); i++) {
//						a1MatchSuspectBObj = (TCRMSuspectBObj) vecA1Suspects
//								.elementAt(i);
//						if (!StringUtils.isNonBlank(a1MatchSuspectBObj
//								.getSuspectStatusType())) {
//							a1MatchSuspectBObj
//									.setSuspectStatusType(Configuration
//											.getConfiguration()
//											.getConfigItem(
//													IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECT_DUPLICATE)
//											.getValue());
//						}
//						createSuspectRecords(a1MatchSuspectBObj,
//								theSourceTCRMPartyBObj.getPartyId(),
//								theSourceTCRMPartyBObj.getControl(), suspComp);
//					}
//
//				} else {
//					int col = 0;
//					Vector vecNonCDCA1Suspect = filterAccessTokenA1Suspects(
//							vecA1Suspects, theSourceTCRMPartyBObj
//									.getPartyType(), theDWLControl);
//					if (coPersistDuplicateParties) {
//						Vector input2 = new Vector();
//						input2.add(theSourceTCRMPartyBObj);
//						input2.add(vecNonCDCA1Suspect);
//						theBestA1MatchedSuspect = (TCRMSuspectBObj) SuspectProcessingUtil
//								.callExternalRule(input2,
//										BEST_FILITERED_SUSPECT_RULE_ID,
//										theSourceTCRMPartyBObj.getStatus(),
//										theSourceTCRMPartyBObj.getControl());
//						int i = 0;
//						do {
//							if (i >= vecA1Suspects.size()) {
//								break;
//							}
//							if (vecA1Suspects.get(i) == theBestA1MatchedSuspect) {
//								col = i;
//								break;
//							}
//							i++;
//						} while (true);
//					} else {
// 					}
//					if (theBestA1MatchedSuspect != null) {
//						createSuspectsForBestA1Match(theSourceTCRMPartyBObj,
//								theBestA1MatchedSuspect, vecA1Suspects, col,
//								coPersistDuplicateParties);
//					}
//				}
//			}
			theSourceTCRMPartyBObj.vecTCRMSuspectBObj.removeAllElements();
		} catch (Exception ex) {
			if (logger.isErrorEnabled()) {
				logger
						.error(ResourceBundleHelper
								.resolve(
										"com.dwl.tcrm.defaultExternalRules.nl.DefaultExternalRulesStrings",
										"Error_Shared_Execute", new Object[] {
												getClass().getName(),
												ex.getLocalizedMessage() }));
			}
			if (ex instanceof TCRMException) {
				TCRMException tcrmEx = (TCRMException) ex;
				throw tcrmEx;
			} else {
				throw ex;
			}
		}
		return null;
	}

	/**
	 * Create Suspects for A1 Determined as Best Match
	 * 
	 * @param theSourceTCRMPartyBObj
	 * @param theBestA1MatchedSuspect
	 * @param vecA1Suspects
	 * @param col
	 * @param coPersistDuplicateParties
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void createSuspectsForBestA1Match(
			TCRMPartyBObj theSourceTCRMPartyBObj,
			TCRMSuspectBObj theBestA1MatchedSuspect, Vector vecA1Suspects,
			int col, boolean coPersistDuplicateParties) throws Exception {
		TCRMPartyBObj theBestA1MatchedSuspectParty = null;
		CustomerNotification theCustomerNotification = new CustomerNotification();
		DWLControl theDWLControl = theSourceTCRMPartyBObj.getControl();
		suspComp = (ISuspectProcessor) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.SUSPECT_COMPONENT);
		IParty partyComp = (IParty) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
		String partyId = null;

		if (StringUtils.compareIgnoreCaseWithTrim(theSourceTCRMPartyBObj
				.getPartyType(), CTCConstants.PARTY_TYPE_ORG)) {
			partyId = theBestA1MatchedSuspect.getTCRMSuspectOrganizationBObj()
					.getPartyId();
		} else {
			partyId = theBestA1MatchedSuspect.getTCRMSuspectPersonBObj()
					.getPartyId();
		}
		boolean tempAccessTokenFlag = theSourceTCRMPartyBObj.getControl()
				.isAccessTokenEnforced();
		theSourceTCRMPartyBObj.getControl().enforceAccessToken(false);
		theBestA1MatchedSuspectParty = partyComp.getParty(partyId,
				INQUIRY_LEVEL_2, theSourceTCRMPartyBObj.getControl());
		theSourceTCRMPartyBObj.getControl().enforceAccessToken(
				tempAccessTokenFlag);
		theBestA1MatchedSuspectParty
				.setObjectReferenceId(theSourceTCRMPartyBObj
						.getObjectReferenceId());
		theBestA1MatchedSuspectParty.setControl(theDWLControl);

		if (coDoNotfication) {
			A1PartySelectedNotification theA1PartySelectedNotification = (A1PartySelectedNotification) (A1PartySelectedNotification) TCRMClassFactory
					.getTCRMCommonNotification(NT1);
			theA1PartySelectedNotification
					.setTxnType((String) (String) theDWLControl
							.get(REQUEST_NAME));
			theA1PartySelectedNotification.setTxnTime(new Timestamp(System
					.currentTimeMillis()));
			theA1PartySelectedNotification.setRequestId(theDWLControl.get(
					REQUEST_ID).toString());
			theA1PartySelectedNotification
					.setUserId((String) (String) theDWLControl.get(USER_NAME));
			theA1PartySelectedNotification
					.setSourceParty(theSourceTCRMPartyBObj);
			theA1PartySelectedNotification
					.setA1PartyFound(theBestA1MatchedSuspectParty);
			theA1PartySelectedNotification.setControl(theDWLControl);
			theCustomerNotification
					.sendNotification(theA1PartySelectedNotification);
		}
		Vector vecTCRMPartyBObj_Susp = new Vector();

		for (int l = 0; l < vecA1Suspects.size(); l++) {
			if (l == col) {
				continue;
			}
			TCRMPartyBObj theOtherA1MatchedSuspectParty = null;
			if (StringUtils.compareIgnoreCaseWithTrim(theSourceTCRMPartyBObj
					.getPartyType(), CTCConstants.PARTY_TYPE_ORG)) {
				theOtherA1MatchedSuspectParty = ((TCRMSuspectBObj) vecA1Suspects
						.elementAt(l)).getTCRMSuspectOrganizationBObj();
			} else {
				theOtherA1MatchedSuspectParty = ((TCRMSuspectBObj) vecA1Suspects
						.elementAt(l)).getTCRMSuspectPersonBObj();
			}
			vecTCRMPartyBObj_Susp.addElement(theOtherA1MatchedSuspectParty);
		}

		IPartyMatcherManager matcherManager = new TCRMPartyMatcherManager();
		IPartyMatcher partyMatcher = matcherManager
				.getPartyMatcher(theDWLControl);
		Vector vecPerSuspects = new Vector();

		if (coPersistDuplicateParties) {
			vecPerSuspects.addAll(vecA1Suspects);
		}
		vecA1Suspects = partyMatcher.matchParties(theBestA1MatchedSuspectParty,
				vecTCRMPartyBObj_Susp);
		TCRMSuspectComponent susComp = (TCRMSuspectComponent) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.SUSPECT_COMPONENT);
		vecA1Suspects = susComp.assignSuspectProcessingAction(
				theBestA1MatchedSuspectParty, vecA1Suspects);
		Vector vecA1SuspectsTemp = new Vector();
		TCRMSuspectBObj suspectA1 = null;
		TCRMSuspectBObj tempSuspectBObj = null;

		if (coPersistDuplicateParties) {
			restoreOrgSuspectStatusTp(vecPerSuspects, vecA1Suspects);
		}
		String strBestPartyId = theBestA1MatchedSuspect.getSuspectPartyId();

		for (int i = 0; i < vecA1Suspects.size(); i++) {
			suspectA1 = (TCRMSuspectBObj) vecA1Suspects.elementAt(i);
			Timestamp ts = Timestamp.valueOf(DWLDateTimeUtilities
					.getCurrentSystemTime());
			if (coPersistDuplicateParties) {
				if (!StringUtils.isNonBlank(suspectA1.getSuspectStatusType())) {
					suspectA1
							.setSuspectStatusType(Configuration
									.getConfiguration()
									.getConfigItem(
											IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECT_DUPLICATE)
									.getValue());
				}
			} else {
				suspectA1
						.setSuspectStatusType(Configuration
								.getConfiguration()
								.getConfigItem(
										IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECT_DUPLICATE)
								.getValue());
			}
			tempSuspectBObj = createSuspectRecords(suspectA1, strBestPartyId,
					theDWLControl, suspComp);
			if (tempSuspectBObj != null
					&& tempSuspectBObj.getEObjSuspect().getLastUpdateDt()
							.after(ts)) {
				vecA1SuspectsTemp.addElement(tempSuspectBObj);
			}
		}

		if (coDoNotfication && vecA1SuspectsTemp != null
				&& vecA1SuspectsTemp.size() != 0) {
			SuspectIdentificationNotification theSuspectIdentificationNotification = (SuspectIdentificationNotification) (SuspectIdentificationNotification) TCRMClassFactory
					.getTCRMCommonNotification("nt2");
			theSuspectIdentificationNotification
					.setTxnType((String) (String) theDWLControl
							.get(REQUEST_NAME));
			theSuspectIdentificationNotification.setTxnTime(new Timestamp(
					System.currentTimeMillis()));
			theSuspectIdentificationNotification.setRequestId(theDWLControl
					.get(REQUEST_ID).toString());
			theSuspectIdentificationNotification
					.setUserId((String) (String) theDWLControl.get(USER_NAME));
			theSuspectIdentificationNotification
					.setSourceParty(theBestA1MatchedSuspectParty);
			theSuspectIdentificationNotification
					.setSuspectList(vecA1SuspectsTemp);
			theSuspectIdentificationNotification.setControl(theDWLControl);
			theCustomerNotification
					.sendNotification(theSuspectIdentificationNotification);
		}
	}

	/**
	 * Create suspect for a given party
	 * 
	 * @param aSuspectBObj
	 *            TCRMSuspectBObj
	 * @param strSourcePartyId
	 *            String
	 * @param theDWLControl
	 *            DWLControl
	 * @param suspComp
	 *            ISuspectProcessor
	 * @throws Exception
	 */
	private TCRMSuspectBObj createSuspectRecords(TCRMSuspectBObj aSuspectBObj,
			String strSourcePartyId, DWLControl theDWLControl,
			ISuspectProcessor suspComp) throws Exception {
		aSuspectBObj.setControl(theDWLControl);
		aSuspectBObj.setPartyId(strSourcePartyId);
		CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory
				.getCodeTypeComponentHelper();
		String langId = (String) theDWLControl.get(LANG_ID);
		SuspectStatusTypeBObj ct_suspectstatus = (SuspectStatusTypeBObj) codeTypeCompHelper
				.getCodeTypeByCode(CD_SUSPECT_STATUS_TP, langId, aSuspectBObj
						.getSuspectStatusType(), theDWLControl);
		aSuspectBObj.setSuspectStatusValue(ct_suspectstatus.getname());
		aSuspectBObj
				.setSourceType(Configuration
						.getConfiguration()
						.getConfigItem(
								IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_REASON_TYPE_SYSTEM_MARKED)
						.getValue());
		return suspComp.createSuspect(aSuspectBObj);
	}

	/**
	 * Restore the pre-set suspect status type (by rule
	 * BestFilteredSuspectsRule)
	 * 
	 * @param vecOrgSuspects
	 * @param vecA1Suspects
	 */
	@SuppressWarnings("unchecked")
	private void restoreOrgSuspectStatusTp(Vector vecOrgSuspects,
			Vector vecA1Suspects) {
		TCRMSuspectBObj suspectBObjA = null;
		TCRMSuspectBObj suspectBObjB = null;
		for (int i = 0; i < vecOrgSuspects.size(); i++) {
			suspectBObjA = (TCRMSuspectBObj) vecOrgSuspects.elementAt(i);
			for (int j = 0; j < vecA1Suspects.size(); j++) {
				suspectBObjB = (TCRMSuspectBObj) vecA1Suspects.elementAt(j);
				if (suspectBObjA.getSuspectPartyId().equals(
						suspectBObjB.getSuspectPartyId())) {
					suspectBObjB.setSuspectStatusType(suspectBObjA
							.getSuspectStatusType());
				}
			}

		}

	}

	/**
	 * @param vecA1Suspects
	 * @param strPartyType
	 * @param objControl
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private Vector filterAccessTokenA1Suspects(Vector vecA1Suspects,
			String strPartyType, DWLControl objControl) throws Exception {
		// return, if the AccessToken check is off
		if (!objControl.isAccessTokenEnforced()) {
			return vecA1Suspects;
		}
		Vector vecAccessTokenA1Suspects = new Vector();
		if (vecA1Suspects != null) {
			for (int i = 0; i < vecA1Suspects.size(); i++) {
				TCRMSuspectBObj objSuspect = (TCRMSuspectBObj) vecA1Suspects
						.get(i);
				TCRMPartyBObj objA1SuspectParty = null;
				if (StringUtils.compareIgnoreCaseWithTrim(strPartyType,
						CTCConstants.PARTY_TYPE_ORG)) {
					objA1SuspectParty = objSuspect
							.getTCRMSuspectOrganizationBObj();
				} else {
					objA1SuspectParty = objSuspect.getTCRMSuspectPersonBObj();
				}
				String strAccessTokenValue = objA1SuspectParty
						.getAccessTokenValue();
				AccessTokenCollection objAccessTokenCollection = objControl
						.getAccessTokenCollection();
				if (objAccessTokenCollection == null
						|| !StringUtils.isNonBlank(strAccessTokenValue)
						|| objAccessTokenCollection
								.contains(strAccessTokenValue)) {
					vecAccessTokenA1Suspects.add(objSuspect);
				}
			}

		}
		return vecAccessTokenA1Suspects;
	}

}
