/* _______________________________________________________ {COPYRIGHT-TOP} _____
 * Licensed Materials - Property of IBM
"Restricted Materials of IBM"
 *
 * 5724-S78
 *
 * (C) Copyright IBM Corp. 2004, 2009  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication, or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 * ________________________________________________________ {COPYRIGHT-END} _____*/
/* ______________________________________________________________________
 *
 * Copyright (c) 2004 DWL Inc. All Rights Reserved.
 * http://www.dwl.com
 *
 * The source code for this software is confidential and proprietary.
 * You shall not disclose such confidential information and shall use
 * it only in accordance with the terms of the license agreement you
 * have with DWL.
 * ______________________________________________________________________
 */

/***************************************************
 * RCSID -- $RCSfile: SuspectAddPartyRule.java,v $
 *          $Revision: 1.6.8.6 $
 *          $Date: 2009/11/26 20:25:01 $
 *          $Author: cgheorgh $
 *          DWL Inc.
 ***************************************************/

package com.ctc.mdm.externalrule;

import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.sdp.util.CTCCategorizedSuspectsComparator;
import com.dwl.base.DWLControl;
import com.dwl.base.accessToken.AccessTokenCollection;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.management.config.client.Configuration;
import com.dwl.tcrm.coreParty.component.CategorizedSuspects;
import com.dwl.tcrm.coreParty.component.TCRMConsolidatedPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyListBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectOrganizationBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectPersonBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.defaultExternalRules.constant.ResourceBundleNames;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.externalrule.SuspectAddPartyRule;
//import com.dwl.tcrm.util.SuspectProcessingUtil;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;

/**
 * External Rule 35.
 * <p>
 * An external rule for adding a party based on the suspects found.
 * 
 * 
 * @since 5.0
 * @modelguid {48EEC273-03AB-411E-B2A2-30146C67204A}
 */
public class CTCSuspectAddPartyRule extends SuspectAddPartyRule {

	private static final String SUSPECT_TYPE_101 = "101";
	private static final String CHAR_Y = "Y";
	private static final String ADD_PARTY_STATUS_7 = "7";
	private static final String ADD_PARTY_STATUS_6 = "6";
	private static final String SUSPECT_TYPE_2 = "2";
	private static final String ADD_PARTY_STATUS_3 = "3";
	private static final String ADD_PARTY_STATUS_8 = "8";
	private static final String SUSPECT_TYPE_1 = "1";
	private static final String SUSPECT_TYPE_3 = "3";
	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2004, 2009\nUS Government Users "
			+ "Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";
	private static final String BEST_A1_PERSISTED = "10";
	private static final String A1_NOT_USED_BECAUSE_OF_ACCESSTOKEN = "11";
	private static final String BEST_FILITERED_SUSPECT_RULE_ID = "111";

	private final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCSuspectAddPartyRule.class);

	private static final String PERSIST_DUPLICATE_PARTIES = "/IBM/Party/SuspectProcessing/PersistDuplicateParties/enabled";

	private static final String RETURN_SUSPECT_ENABLED = "/IBM/Party/SuspectProcessing/AddParty/returnSuspect";

	String rulesetName = "SuspectAddParty";

	String rulesetVersion = " 5.0 ";

	boolean debugOn = true;
	protected boolean coReturnMandatorySearchResults = true;
	protected boolean existingPartyProvided = false;

	private final static String ERROR_MESSAGE = "Error_Shared_Execute";

	/**
	 * Constructor for SuspectAddPartyRule.
	 * 
	 */
	public CTCSuspectAddPartyRule() {
		super();
	}

	/**
	 * @param input
	 *            A <code>Vector</code> passed by the caller. The first element
	 *            is an instance of a party object. The second element is itself
	 *            a <code>Vector</code> of <code>CategorizedSuspect</code>
	 *            objects.
	 * @param componentObject
	 *            Not used currently
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {
		if (logger.isFineEnabled()) {
			logger.fine("External Java Rule 35 - SuspectAddPartyRule fired");
		}

		long startTime = System.currentTimeMillis();

		Vector vecInputElements = new Vector();
		TCRMPartyBObj theSourcePartyBObj = null;
		boolean partyAdded = false;

		try {
			if (input instanceof Vector) {
				vecInputElements = (Vector) input;
			}

			theSourcePartyBObj = (TCRMPartyBObj) vecInputElements.elementAt(0);
			DWLControl theDWLControl = theSourcePartyBObj.getControl();

			if (StringUtils.compareIgnoreCaseWithTrim(theSourcePartyBObj
					.getPartyType(), CTCConstants.PARTY_TYPE_ORG)) {

				return super.execute(input, componentObject);
			}

			// USE CONFIGURATION CLIENT
			coReturnMandatorySearchResults = Configuration.getConfiguration().getConfigItem(RETURN_SUSPECT_ENABLED,
							theDWLControl.retrieveConfigContext()).getBooleanValue();
			
			boolean coPersistDuplicateParties = Configuration
					.getConfiguration().getConfigItem(
							PERSIST_DUPLICATE_PARTIES,
							theDWLControl.retrieveConfigContext())
					.getBooleanValue();

			boolean a1PartyUpdateOccurred = false;
			
			if (StringUtils.isNonBlank(theSourcePartyBObj.getPartyId())) {
				// an existing party identified by the partyId was provided.
				existingPartyProvided = true;
			}

			if ((vecInputElements != null) && (vecInputElements.size() != 0)) {
				IParty partyComp = (IParty) TCRMClassFactory
						.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);

				// Note: in vecCategorizedSuspects, you can have category A1, or
				// categories (A2/B)
				// but not categories(A1/A2/B)

				Vector vecCategorizedSuspects = (Vector) vecInputElements
						.elementAt(1);

				Collections.sort(vecCategorizedSuspects,
						new CTCCategorizedSuspectsComparator());

				if ((vecCategorizedSuspects != null)
						&& (vecCategorizedSuspects.size() != 0)) {

					boolean blnPartyNotUpdated = true;

					int vecSize = vecCategorizedSuspects.size();

					boolean satisfiesACase = false;
					for (int i = 0; i < vecSize; i++) {

						CategorizedSuspects aCategoryOfSuspects = (CategorizedSuspects) vecCategorizedSuspects
								.elementAt(i);

						// 1. get the suspect category type (i.e., 1 - A1, 2 -
						// A2, 3 - B, 4 - C)
						String theSuspectCategoryType = aCategoryOfSuspects
								.getCdSuspectTp();

						// 2. Determine what action to take with the party -
						// either add or call a rule to update it.

						// 3. a) Deal with the case if it is an A1 match
						// (suspect type 1)
						

						if (theSuspectCategoryType.trim().equalsIgnoreCase(
								SUSPECT_TYPE_1)) {
							Vector vecA1Suspects = aCategoryOfSuspects
									.getTCRMSuspectBObjs();
							TCRMPartyBObj a1Suspect = null;
							TCRMSuspectBObj theBestA1MatchedSuspect = null;
							Vector vecNonCDCA1Suspect = filterAccessTokenA1Suspects(
									vecA1Suspects, theSourcePartyBObj
											.getPartyType(), theDWLControl);
							// 1. Case: There is more than one A1 match found

							if (vecNonCDCA1Suspect.size() > 1) {

								// 1. Get the A1 Matched Suspect that most
								// closely matches the source party.
								// Ensure that this one does not have a suspect
								// record added for it.

								if (coPersistDuplicateParties) {
									// Call external rule
									// BestFilteredSuspectsRule to compare the
									// LOBRelatedType
									Vector input2 = new Vector();
									input2.add(theSourcePartyBObj);
									input2.add(vecNonCDCA1Suspect);

									/*theBestA1MatchedSuspect = (TCRMSuspectBObj) SuspectProcessingUtil
											.callExternalRule(
													input2,
													BEST_FILITERED_SUSPECT_RULE_ID,
													theSourcePartyBObj
															.getStatus(),
													theSourcePartyBObj
															.getControl());*/
								} else {

									TCRMPersonBObj objNewTCRMPersonBObj = (TCRMPersonBObj) collapseMultilpleParties(
											vecNonCDCA1Suspect, theDWLControl);

									theSourcePartyBObj = partyComp
											.updatePartyDetails(
													theSourcePartyBObj,
													objNewTCRMPersonBObj);

									blnPartyNotUpdated = false;

									a1PartyUpdateOccurred = true;
								}

								if (theBestA1MatchedSuspect != null) {
									if (StringUtils.compareIgnoreCaseWithTrim(
											theSourcePartyBObj.getPartyType(),
											CTCConstants.PARTY_TYPE_ORG)) {
										a1Suspect = (TCRMSuspectOrganizationBObj) theBestA1MatchedSuspect
												.getTCRMSuspectOrganizationBObj();
									} else {
										a1Suspect = (TCRMSuspectPersonBObj) theBestA1MatchedSuspect
												.getTCRMSuspectPersonBObj();
									}
								}

								theSourcePartyBObj
										.setAddPartyStatus(ADD_PARTY_STATUS_8);
							} else if (vecNonCDCA1Suspect.size() == 1) {
								// 2. Case: Only one A1 Match

								if (coPersistDuplicateParties) {
									// Call external rule
									// BestFilteredSuspectsRule to compare the
									// LOBRelatedType
									Vector input2 = new Vector();
									input2.add(theSourcePartyBObj);
									input2.add(vecNonCDCA1Suspect);

									/*theBestA1MatchedSuspect = (TCRMSuspectBObj) SuspectProcessingUtil
											.callExternalRule(
													input2,
													BEST_FILITERED_SUSPECT_RULE_ID,
													theSourcePartyBObj
															.getStatus(),
													theSourcePartyBObj
															.getControl());*/
								} else {
									theBestA1MatchedSuspect = (TCRMSuspectBObj) vecNonCDCA1Suspect
											.elementAt(0);
								}

								if (theBestA1MatchedSuspect != null) {
									if (StringUtils.compareIgnoreCaseWithTrim(
											theSourcePartyBObj.getPartyType(),
											CTCConstants.PARTY_TYPE_ORG)) {
										a1Suspect = (TCRMSuspectOrganizationBObj) theBestA1MatchedSuspect
												.getTCRMSuspectOrganizationBObj();
									} else {
										a1Suspect = (TCRMSuspectPersonBObj) theBestA1MatchedSuspect
												.getTCRMSuspectPersonBObj();
									}
								}

								theSourcePartyBObj
										.setAddPartyStatus(ADD_PARTY_STATUS_3);
							}

							satisfiesACase = true;
							if (vecNonCDCA1Suspect.size() >= 1) {
								// Now that you know which A1 match you're going
								// to use to update/add; if configured to do
								// conditional persistence of duplicates:
								// see if any suspect party (all the A1 matches)
								// has the same or different LOB as the source
								// party
								// If there is a different LOB, add the A1 match
								// party; if it is the same, update the A1 match
								// party.
								//
								// when a1Suspect == null, which is returned by
								// rule BEST_FILITERED_SUSPECT_RULE_ID, means
								// there is different LOBs; if a1Suspect !=
								// null, means there is same LOB between source
								// party and
								// the best A1 matches (vector of suspects)
								if (coPersistDuplicateParties
										&& a1Suspect == null) { // there is
									// different LOB
									// between
									// source party
									// and the best
									// A1 match
									// suspect party
									// (vector of
									// suspects)
									// BEST_A1_PERSISTED "10" is a new status,
									// indicating best A1 match suspect was
									// persisted.
									theSourcePartyBObj
											.setAddPartyStatus(BEST_A1_PERSISTED);
									partyComp
											.addPartySimple(theSourcePartyBObj);
								} else {

									if (blnPartyNotUpdated) {
										theSourcePartyBObj = partyComp
												.updatePartyDetails(
														theSourcePartyBObj,
														a1Suspect);
										a1PartyUpdateOccurred = true;
									}

								}
							} else {
								// Because we filter out non CDC A1, it is
								// possible that
								// we had 1 or many A1 coming in. But because of
								// the filter we could end up having no A1.
								// In this case, simply add
								theSourcePartyBObj
										.setAddPartyStatus(A1_NOT_USED_BECAUSE_OF_ACCESSTOKEN);
								partyComp.addPartySimple(theSourcePartyBObj);
							}

						}

						// 3. b) Deal with the case if it is an A2 match
						// (suspect type 2)
						if (theSuspectCategoryType.trim().equalsIgnoreCase(
								SUSPECT_TYPE_2)) {
							if (existingPartyProvided) {
								if (!partyAdded) {
									partyComp
											.addPartySimple(theSourcePartyBObj);
									partyAdded = true;
								}

								theSourcePartyBObj
										.setAddPartyStatus(ADD_PARTY_STATUS_6);
							} else {
								if (((theSourcePartyBObj
										.getMandatorySearchDone() == null) || (!(theSourcePartyBObj
										.getMandatorySearchDone()
										.equalsIgnoreCase(CHAR_Y))))
										&& (coReturnMandatorySearchResults)) {
									theSourcePartyBObj
											.setAddPartyStatus(ADD_PARTY_STATUS_7);
								} else {
									if (!partyAdded) {
										partyComp
												.addPartySimple(theSourcePartyBObj);
										partyAdded = true;
									}

									if (coReturnMandatorySearchResults) {
										theSourcePartyBObj
												.setMandatorySearchDone(CHAR_Y);
									}

									theSourcePartyBObj
											.setAddPartyStatus(ADD_PARTY_STATUS_6);
								}
							}

							satisfiesACase = true;
						}

						// 3. c) Deal with the case if it is an B match (suspect
						// type 3)
						if (theSuspectCategoryType.trim().equalsIgnoreCase(
								SUSPECT_TYPE_3)) {
							if (((theSourcePartyBObj.getMandatorySearchDone() == null) || (!(theSourcePartyBObj
									.getMandatorySearchDone()
									.equalsIgnoreCase(CHAR_Y))))
									&& (coReturnMandatorySearchResults)
									&& (vecCategorizedSuspects.size() > 1)) {

								// If we found multiple categories of suspects
								// and mandatory suspect flag is not Y,
								// let other categories determine how to handle
								// this party.
								continue;
							}

							// 1. There is at one B-level suspect so add the
							// source party
							if (!partyAdded) {
								partyComp.addPartySimple(theSourcePartyBObj);
								partyAdded = true;
							}

							theSourcePartyBObj
									.setAddPartyStatus(SUSPECT_TYPE_2);

							if (coReturnMandatorySearchResults) {
								theSourcePartyBObj
										.setMandatorySearchDone(CHAR_Y);
							}

							satisfiesACase = true;
						}

						// 3. c) Deal with the case if it is an L match (suspect
						// type 3)
						if (theSuspectCategoryType.trim().equalsIgnoreCase(
								SUSPECT_TYPE_101)) {
							if (((theSourcePartyBObj.getMandatorySearchDone() == null) || (!(theSourcePartyBObj
									.getMandatorySearchDone()
									.equalsIgnoreCase(CHAR_Y))))
									&& (coReturnMandatorySearchResults)
									&& (vecCategorizedSuspects.size() > 1)) {

								// If we found multiple categories of suspects
								// and mandatory suspect flag is not Y,
								// let other categories determine how to handle
								// this party.
								continue;
							}

							// 1. There is at one L-level suspect so add the
							// source party
							if (!partyAdded) {
								partyComp.addPartySimple(theSourcePartyBObj);
								partyAdded = true;
							}

							theSourcePartyBObj
									.setAddPartyStatus(SUSPECT_TYPE_2);

							if (coReturnMandatorySearchResults) {
								theSourcePartyBObj
										.setMandatorySearchDone(CHAR_Y);
							}

							satisfiesACase = true;
						}
						if(satisfiesACase)
						{
							break;
						}
					}

					// 3. d) None of the above cases are satisfied, so no
					// suspects were found.
					// Add the party
					if (!(satisfiesACase)) {
						// no suspects found at all, so simply add the party
						if (!partyAdded) {
							partyComp.addPartySimple(theSourcePartyBObj);
							partyAdded = true;
						}

						if (coReturnMandatorySearchResults) {
							theSourcePartyBObj
									.setMandatorySearchDone(CHAR_Y);
						}

						theSourcePartyBObj
								.setAddPartyStatus(SUSPECT_TYPE_1);

						theSourcePartyBObj.vecTCRMSuspectBObj
								.removeAllElements();
					}
						
					/*
					 * Remove all the other suspects if an A1 Party was updated.
					 * These results are no more valid and suspects need to be re-identified.
					 * This will happen in the A1 Suspects Action Rule.
					 */
					if(a1PartyUpdateOccurred){
						
						for (Iterator iterator = vecCategorizedSuspects
								.iterator(); iterator.hasNext();) {
							CategorizedSuspects categorizedSuspects = (CategorizedSuspects) iterator.next();
							if(!StringUtils.compareIgnoreCaseWithTrim(SUSPECT_TYPE_1, categorizedSuspects.getCdSuspectTp())){
								iterator.remove();
							}
						}
					}
					
				}
			}
		} catch (Exception ex) {
			if (logger.isErrorEnabled())
				logger.error(ResourceBundleHelper.resolve(
						ResourceBundleNames.DEFAULT_EXTERNAL_RULES_STRINGS,
						ERROR_MESSAGE, new Object[] {
								this.getClass().getName(),
								ex.getLocalizedMessage() }));

			if (ex instanceof TCRMException) {
				TCRMException tcrmEx = (TCRMException) ex;
				throw tcrmEx;
			} else {
				throw ex;
			}
		}

		if (logger.isFineEnabled()) {
			logger.info("CTCSuspectAddPartyRule : Execution Time (in MilliSeconds) :: "+(System.currentTimeMillis() - startTime)); 
		}

		return theSourcePartyBObj;
	}

	/**
	 * @param vecA1Suspects
	 * @param strPartyType
	 * @param objControl
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private Vector filterAccessTokenA1Suspects(Vector vecA1Suspects,
			String strPartyType, DWLControl objControl) throws Exception {
		// return, if the AccessToken check is off
		if (!objControl.isAccessTokenEnforced()) {
			return vecA1Suspects;
		}

		Vector vecAccessTokenA1Suspects = new Vector();

		if (vecA1Suspects != null) {
			for (int i = 0; i < vecA1Suspects.size(); i++) {
				TCRMSuspectBObj objSuspect = (TCRMSuspectBObj) vecA1Suspects
						.get(i);
				TCRMPartyBObj objA1SuspectParty = null;

				if (StringUtils.compareIgnoreCaseWithTrim(strPartyType,
						CTCConstants.PARTY_TYPE_ORG)) {
					objA1SuspectParty = (TCRMSuspectOrganizationBObj) objSuspect
							.getTCRMSuspectOrganizationBObj();
				} else {
					objA1SuspectParty = (TCRMSuspectPersonBObj) objSuspect
							.getTCRMSuspectPersonBObj();
				}

				String strAccessTokenValue = objA1SuspectParty
						.getAccessTokenValue();
				AccessTokenCollection objAccessTokenCollection = objControl
						.getAccessTokenCollection();
				if (objAccessTokenCollection != null
						&& StringUtils.isNonBlank(strAccessTokenValue)
						&& !objAccessTokenCollection
								.contains(strAccessTokenValue)) {
					continue;
				} else {
					vecAccessTokenA1Suspects.add(objSuspect);
				}
			}
		}
		return vecAccessTokenA1Suspects;
	}

	/**
	 * @param vecNonCDCA1Suspect
	 * @param theDWLControl
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private TCRMPartyBObj collapseMultilpleParties(Vector vecNonCDCA1Suspect,
			DWLControl theDWLControl) throws Exception {

		TCRMConsolidatedPartyBObj objTCRMConsolidatedPartyBObj = new TCRMConsolidatedPartyBObj();
		objTCRMConsolidatedPartyBObj.setControl(theDWLControl);

		TCRMPartyListBObj objTCRMPartyListBObj = new TCRMPartyListBObj();
		objTCRMPartyListBObj.setControl(theDWLControl);

		for (Iterator iter = vecNonCDCA1Suspect.iterator(); iter.hasNext();) {

			TCRMSuspectBObj objTCRMSuspectBObj = (TCRMSuspectBObj) iter.next();
			objTCRMPartyListBObj.setTCRMPersonBObj(objTCRMSuspectBObj
					.getTCRMSuspectPersonBObj());

		}

		objTCRMConsolidatedPartyBObj.setTCRMPartyListBObj(objTCRMPartyListBObj);

		IParty partyComp = (IParty) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);

		TCRMConsolidatedPartyBObj objResTCRMConsolidatedPartyBObj = partyComp
				.collapseMultipleParties(objTCRMConsolidatedPartyBObj);

		TCRMPartyBObj collapsedNewParty = null;

		if (objResTCRMConsolidatedPartyBObj != null
				&& objResTCRMConsolidatedPartyBObj.getTCRMPartyBObj() != null) {

			collapsedNewParty = objResTCRMConsolidatedPartyBObj
					.getTCRMPartyBObj();
			collapsedNewParty.setControl(objResTCRMConsolidatedPartyBObj
					.getControl());
		}

		return collapsedNewParty;
	}

}
