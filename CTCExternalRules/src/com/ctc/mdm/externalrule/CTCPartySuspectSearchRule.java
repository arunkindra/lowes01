// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCPartySuspectSearchRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.sdp.util.CTCSuspectSearchProcessor;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.StringUtils;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.externalrule.PartySuspectSearchRule;

/**
 * Rule 3: Search suspects of a given party.
 */
public class CTCPartySuspectSearchRule extends PartySuspectSearchRule {

	protected final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCPartySuspectSearchRule.class);

	protected String ruleName = "CTCPartySuspectSearchRule";

	protected String debugStr = "External Java Rule 3 " + ruleName + ": ";

	/**
	 * Search suspects of a given party.
	 * 
	 * @param input
	 *            parameters that passed by the caller
	 * @param componentObject
	 *            Not used currently
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {
		if (logger.isFineEnabled()) {
			logger.fine(debugStr + "Entering Rule");
		}
		long startTime = System.currentTimeMillis();
		
		TCRMPartyBObj party = (TCRMPartyBObj) input;
		Vector<TCRMPartyBObj> vecSuspectParties = null;

		//If partyType is Organization delegate call to default rules
		if (StringUtils.compareIgnoreCaseWithTrim(party.getPartyType(),CTCConstants.PARTY_TYPE_ORG)) {
			
			vecSuspectParties = (Vector) super.execute(input, componentObject);
		}
		else {
			if (logger.isFineEnabled()) {
				logger.fine(debugStr + "Party is a Person");
			}
			
			CTCSuspectSearchProcessor suspectSearchProcessor = new CTCSuspectSearchProcessor();
			vecSuspectParties = (Vector<TCRMPartyBObj>)suspectSearchProcessor.searchSuspects(party);
			
		}	

		if (logger.isFineEnabled()) {
			logger.fine(debugStr + "Exiting Rule");
			if(vecSuspectParties != null){
				int size = vecSuspectParties.size();
				logger.info("CTCPartySuspectSearchRule : Number of Suspects :: "+ size);
			}
			logger.info("CTCPartySuspectSearchRule : Execution Time (in MilliSeconds) :: "+(System.currentTimeMillis() - startTime)); 
		}
		
		return vecSuspectParties;
		
	}


}
