// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCPartyMatchCategoryExtRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.sql.ResultSet;
import java.util.List;
import java.util.Vector;
import com.ctc.mdm.codetable.obj.*;
import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.constants.CTCErrorConstants;
import com.ctc.mdm.common.constants.CTCSQLConstants;
//import com.ctc.mdm.services.exception.CTCCompositeException;
import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.coreParty.interfaces.IPerson;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.externalrule.PartyMatchCategoryExtRule;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.base.db.DataManager;
import com.dwl.base.db.QueryConnection;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.exception.DWLBaseException;
import com.ibm.mdm.common.codetype.component.CodeTypeComponentHelperImpl;
import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;
import com.ibm.mdm.common.codetype.obj.CodeTypeBObj;

/**
 * @author Infotrellis 
 * This Class Adjust the Match Category Depends on
 * CTCActionType Table. When suspect party adminSystemtype is equal to 
 * Source party AdminSystemType and if it is L suspect then Suspect will be converted to A1
 * suspect. All C suspects are removed from Suspect List
 */
public class CTCPartyMatchCategoryExtRule extends PartyMatchCategoryExtRule {

	private static final String ACTIONCODE1 = "ACTIONCODE1";

	private static final IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCPartyMatchCategoryExtRule.class);

	ResultSet objResultSet = null;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.dwl.tcrm.externalrule.PartyMatchCategoryExtRule#execute(java.lang
	 * .Object, java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {

		logger.info("Entering CTCPartyMatchCategoryExtRule().execute()");
		String strMatchRelevTpCd = null;
		String strSuspReasonTpCd = null;
		String strPersonOrgCode = null;
		Vector vecSuspect = null;
		String sourcePartyAdminSystemType = null;
		String lowerPriorityAdminSystemType = null;
		String suspectAdminSystemType = null;
		String lowPriorityAdminSysTp = null;

		if (input instanceof Vector) {
			vecSuspect = (Vector) input;
		}
		TCRMPartyBObj tcrmPartyBObj = (TCRMPartyBObj) vecSuspect.elementAt(0);
		DWLControl dwlControl = tcrmPartyBObj.getControl();

		vecSuspect.remove(0);

		if (tcrmPartyBObj != null) {
			if (!tcrmPartyBObj.getPartyType().equalsIgnoreCase(
					CTCConstants.PARTY_TYPE_ORG)) {
				Vector<TCRMAdminContEquivBObj> vecTCRMAdminContEquivBObj = tcrmPartyBObj
						.getItemsTCRMAdminContEquivBObj();
				
				if(vecTCRMAdminContEquivBObj.size()== 0)					
				{
					IParty  partyComp = (IParty) TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
					
					vecTCRMAdminContEquivBObj = partyComp.getAllPartyAdminSysKeys(tcrmPartyBObj.getPartyId(), dwlControl);
				}
					
				
				if (vecTCRMAdminContEquivBObj.size() > 1) {
						TCRMAdminContEquivBObj higherPriorityAdminSystem = getHigherPriorityAdminSystem(
								vecTCRMAdminContEquivBObj, dwlControl);
						sourcePartyAdminSystemType = higherPriorityAdminSystem
								.getAdminSystemType();
					} else 
					{
						TCRMAdminContEquivBObj tcrmAdminContEquiObj = (TCRMAdminContEquivBObj) vecTCRMAdminContEquivBObj
								.get(0);
						sourcePartyAdminSystemType = tcrmAdminContEquiObj
								.getAdminSystemType();
						
					}
				

				int vecSuspectSize = vecSuspect.size();
				for (int i = 0; i < vecSuspectSize; i++) {
					// I.Determine the AdminSystem to which the existing suspect
					// party belongs.
					TCRMSuspectBObj suspect = (TCRMSuspectBObj) vecSuspect
							.elementAt(i);

					TCRMPersonBObj suspectPerson = suspect
							.getTCRMSuspectPersonBObj();

					if (suspectPerson != null) {

						IParty partyComp = (IParty) TCRMClassFactory
								.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);

						Vector<TCRMAdminContEquivBObj> vecAdminContEqui = partyComp
								.getAllPartyAdminSysKeys(suspectPerson
										.getPartyId(), dwlControl);

						if (vecAdminContEqui != null
								&& vecAdminContEqui.size() != 0) {
							if (vecAdminContEqui.size() == 1) {
								suspectAdminSystemType = vecAdminContEqui
										.get(0).getAdminSystemType();
							} else {
								// Pick the TCRMAdminContEquivbObj with the
								// higher priority defined in the
								// CDCTCADMINSYSPRIORITYTP code table

								TCRMAdminContEquivBObj higherPriorityAdminSystem = getHigherPriorityAdminSystem(
										vecAdminContEqui, dwlControl);
								suspectAdminSystemType = higherPriorityAdminSystem
										.getAdminSystemType();
							}
						} else {
							logger
									.error("No Admin contiquivelent Value in the suspect");
						}
						// II. Determine the lower priority AdminSystem between
						// sourcePartyAdminSystemType & suspectAdminSystemType
						lowerPriorityAdminSystemType = getLowerPriorityAdminSystem(
								sourcePartyAdminSystemType,
								suspectAdminSystemType, dwlControl);

						// III. Retrieve CTC Processing Action record from the
						// CTCACTIONTYPE
						// table using the Match Relevancy Type, Non-Match
						// Relevancy Type, Party Type and
						// lowerPriorityAdminSystemType
						strMatchRelevTpCd = suspect.getMatchRelevencyType();
						strSuspReasonTpCd = suspect.getNonMatchRelevencyType();
						strPersonOrgCode = "P";
						lowPriorityAdminSysTp = lowerPriorityAdminSystemType;

					
						QueryConnection connection = null;
						try {

							connection = DataManager.getInstance()
									.getQueryConnection();
							List sqlParam = new java.util.ArrayList(4);
							sqlParam.add(new Long(strMatchRelevTpCd));
							sqlParam.add(new Long(strSuspReasonTpCd));
							sqlParam.add(new String(strPersonOrgCode));
							sqlParam.add(new Long(lowPriorityAdminSysTp));

							// To execute the SQLQuery
							ResultSet objResultSet = connection
									.queryResults(
											CTCSQLConstants.STR_SQL_SEARCH_ADD_ACTION_CODE_FROM_CTCACTIONTYPE,
											sqlParam.toArray());

							if (objResultSet.next()) {
								String strActionCode = (objResultSet
										.getString(ACTIONCODE1));
								if (strActionCode.equalsIgnoreCase(
										CTCConstants.SUSPECT_TYPE_L)
										&& sourcePartyAdminSystemType
												.equalsIgnoreCase(suspectAdminSystemType))
									strActionCode = CTCConstants.SUSPECT_TYPE_A1;

								suspect
										.setAdjustedMatchCategoryCode(strActionCode);
							}

						} catch (Exception objException) {

							throw objException;
						} finally {
							connection.close();
						}

					}// if
				}// For loop
			}
			// Remove C Suspects form the Suspect list

			for (int i = 0; i < vecSuspect.size(); i++) {
				TCRMSuspectBObj objTCRMSuspectBObj = (TCRMSuspectBObj) vecSuspect
						.elementAt(i);
				if (objTCRMSuspectBObj.getAdjustedMatchCategoryCode() != null) {
					if (objTCRMSuspectBObj.getAdjustedMatchCategoryCode()
							.equalsIgnoreCase(CTCConstants.SUSPECT_TYPE_C)) {
						vecSuspect.remove(i);
						i--;
					}
				} else if (objTCRMSuspectBObj.getMatchCategoryCode() != null
						&& objTCRMSuspectBObj.getMatchCategoryCode()
								.equalsIgnoreCase(CTCConstants.SUSPECT_TYPE_C)) {
					
					vecSuspect.remove(i);
					i--;
				}

			}
		} else {
			logger.error("Input Object is null");
		}
		
		if(logger.isFineEnabled()){
			if(vecSuspect != null && vecSuspect.size() > 0){
				for (Object suspectObject : vecSuspect) {
					TCRMSuspectBObj suspect = (TCRMSuspectBObj)suspectObject;
					String adjustMatchCategoryCode = suspect.getAdjustedMatchCategoryCode();
					String matchCategoryCode = suspect.getMatchCategoryCode();
					
					logger.info("AdjustedMatchCategoryCode --- " + adjustMatchCategoryCode + " : MatchCategoryCode --- " + matchCategoryCode);
					
				}
			}
		}
		logger.info("Exiting CTCPartyMatchCategoryExtRule().execute()");

		return vecSuspect;
	}

	/**
	 * Compare the Two input AdminSystems and return the LowerPriority Admin
	 * System type
	 * 
	 * @param sourcePartyAdminSysTp
	 * @param strSuspectAdminSysTp
	 * @param dwlControl
	 * @return
	 * @throws DWLBaseException
	 * @throws CTCCompositeException
	 */
	private String getLowerPriorityAdminSystem(String sourcePartyAdminSysTp,
			String strSuspectAdminSysTp, DWLControl dwlControl)
			throws DWLBaseException {
		logger
				.info("Entering CTCPartyMatchCategoryExtRule.getLowerPriorityLOB()");
		String strSourcePriority = null;
		String strSuspectPriority = null;
		CodeTypeComponentHelper objCodeTypeComponent = new CodeTypeComponentHelperImpl();

		CodeTypeBObj SourceAdminSysPriorityTp = objCodeTypeComponent
				.getCodeTypeByCode(CTCConstants.CODETABLE_CTCADMINSYSPRIORITY,
						CTCConstants.LANGUAGE_EN, sourcePartyAdminSysTp,
						dwlControl);
		CodeTypeBObj SuspectAdminSysPriorityTp = objCodeTypeComponent
				.getCodeTypeByCode(CTCConstants.CODETABLE_CTCADMINSYSPRIORITY,
						CTCConstants.LANGUAGE_EN, strSuspectAdminSysTp,
						dwlControl);
		if (SourceAdminSysPriorityTp == null
				|| SuspectAdminSysPriorityTp == null) {
			logger
					.error("Can't find the AdminsSysType or Invalid AdminSysType   ");
			IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
			DWLError error = errHandler.getErrorMessage(
					CTCConstants.ADMIN_SYSTEM_KEY_COMPONENT,
					DWLErrorCode.DATA_INVALID_ERROR,
					CTCErrorConstants.ERR_MSG_ADMIN_SYSTEM_NOT_FOUND,
					dwlControl);
			throw new TCRMException(error.getErrorMessage());

		}
		CTCAdminSysPriorityTypeBObj objCTCLobSourcePriority = (CTCAdminSysPriorityTypeBObj) SourceAdminSysPriorityTp;
		CTCAdminSysPriorityTypeBObj objCTCLobsuspectPriority = (CTCAdminSysPriorityTypeBObj) SuspectAdminSysPriorityTp;
		strSourcePriority = objCTCLobSourcePriority.getprioritytpcd();
		strSuspectPriority = objCTCLobsuspectPriority.getprioritytpcd();

		if (Integer.parseInt(strSourcePriority) > Integer
				.parseInt(strSuspectPriority)) {
			return sourcePartyAdminSysTp;
		}

		logger
				.info("Exiting CTCPartyMatchCategoryExtRule.getLowerPriorityLOB()");
		return strSuspectAdminSysTp;
	}

	/**
	 * To Get Higher Priority Admin System from the input Vector
	 * 
	 * @param vecLobRel
	 * @param dwlControl
	 * @return
	 * @throws DWLBaseException
	 */
	private TCRMAdminContEquivBObj getHigherPriorityAdminSystem(
			Vector<TCRMAdminContEquivBObj> vecLobRel, DWLControl dwlControl)
			throws DWLBaseException {
		logger
				.info("Entering CTCPartyMatchCategoryExtRule.getHigherPriorityLOB()");
		String strAdminSystp = null;
		String strAdminSysPrioritytp = null;
		TCRMAdminContEquivBObj objHigherPriorityAdminSys = null;
		int highPriority = 0;

		CodeTypeComponentHelper objCodeTypeComponent = new CodeTypeComponentHelperImpl();

		for (TCRMAdminContEquivBObj objTCRMAdminContEquiv : vecLobRel) {

			strAdminSystp = objTCRMAdminContEquiv.getAdminSystemType();
			CodeTypeBObj AdminSysPriorityTp = objCodeTypeComponent
					.getCodeTypeByCode(
							CTCConstants.CODETABLE_CTCADMINSYSPRIORITY,
							CTCConstants.LANGUAGE_EN, strAdminSystp, dwlControl);
			CTCAdminSysPriorityTypeBObj objCTCAdminPriority = (CTCAdminSysPriorityTypeBObj) AdminSysPriorityTp;
			strAdminSysPrioritytp = objCTCAdminPriority.getprioritytpcd();

			if (highPriority == 0) {
				highPriority = Integer.parseInt(strAdminSysPrioritytp);
				objHigherPriorityAdminSys = objTCRMAdminContEquiv;
			}

			if (highPriority > Integer.parseInt(strAdminSysPrioritytp)) {
				highPriority = Integer.parseInt(strAdminSysPrioritytp);
				objHigherPriorityAdminSys = objTCRMAdminContEquiv;
			}

		}

		logger
				.info("Exiting CTCPartyMatchCategoryExtRule.getHigherPriorityLOB()");
		return objHigherPriorityAdminSys;
	}
}
