// ******************************************************************************
// * Copyright (c) 2011 Canadian Tire Corporation. All Rights Reserved.
// *
// * $Workfile: CTCA2SuspectsActionRule.java
// * $Revision: 1.0
// * $Author: Infotrellis
// * $Date: 19-Aug-2011
// *
// ******************************************************************************

package com.ctc.mdm.externalrule;

import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Vector;

import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.externalrule.Rule;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.management.config.client.Configuration;
import com.dwl.tcrm.common.TCRMControlKeys;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMSuspectBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.ISuspectProcessor;
import com.dwl.tcrm.coreParty.notification.CustomerNotification;
import com.dwl.tcrm.coreParty.notification.SuspectIdentificationNotification;
import com.dwl.tcrm.customerNotification.NotificationTypes;
import com.dwl.tcrm.defaultExternalRules.constant.ResourceBundleNames;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.common.codetype.component.CodeTypeTableNames;
import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;
import com.ibm.mdm.domains.codetype.obj.SuspectStatusTypeBObj;

/**
 * External Rule 32.
 * 
 * Handles the processing of A2 suspects. This default
 * implementation does the following:
 * 
 * If the mandatory search has not been done and it is configured to be
 * done, a suspect record is not created.
 * If the mandatory search has been done or it is not configured to be
 * done, a suspect entry will be created.
 * 
 */
public class CTCA2SuspectsActionRule extends Rule {

	private static final String IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECTDUPLICATE = "/IBM/Party/SuspectProcessing/SuspectStatusType/suspectDuplicate";

	private static final String IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_REASON_TYPE_SYSTEM_MARKED = "/IBM/Party/SuspectProcessing/SuspectReasonType/systemMarked";

	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2004, 2009\nUS Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";

	private static final String NOTIFICATION_ENABLED = "/IBM/DWLCommonServices/Notifications/enabled";

	private static final String RETURN_SUSPECT_ENABLED = "/IBM/Party/SuspectProcessing/AddParty/returnSuspect";


	private final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCA2SuspectsActionRule.class);

	
	IDWLErrorMessage errHandler;
	String rulesetName = "A2SuspectsActionRule";
	String rulesetVersion = " 5.0.1";
	boolean coReturnMandatorySearchResults = true;
	boolean coDoNotfication = true;

	
	boolean debugOn = true;

	private final static String ERROR_MESSAGE = "Error_Shared_Execute";

	public CTCA2SuspectsActionRule() {
		super();
		errHandler = TCRMClassFactory.getErrorHandler();

	}

	/**
	 * See description above.
	 * 
	 * @return This rule returns nothing (null). Only suspect entries are
	 *         created and notifications are sent.
	 * @param input
	 *            Element at 0 holds the source party we are trying to add
	 *            (TCRMPartyBObj). Element at 1 holds a Vector of
	 *            TCRMSuspectBObj - in this case they are all of type A2.
	 * @param componentObject
	 *            Not used.
	 * @see com.dwl.base.externalrule.IExternalJavaRule#execute(Object, Object)
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Object execute(Object input, Object componentObject)
			throws Exception {

		if (logger.isFineEnabled()) {
			logger.fine("External Java Rule 32 - A2SuspectsActionRule fired");
		}

		Vector vecInputElements = new Vector();
		TCRMPartyBObj theSourceTCRMPartyBObj = null;
		DWLControl theDWLControl = null;
		Vector vecSuspects = new Vector();
		CustomerNotification theCustomerNotification = new CustomerNotification();

		try {

			if (input instanceof Vector) {
				vecInputElements = (Vector) input;
			}

			// Initialize incoming arguments and components needed for this rule

			ISuspectProcessor suspComp = (ISuspectProcessor) TCRMClassFactory
					.getTCRMComponent(TCRMCorePropertyKeys.SUSPECT_COMPONENT);

			theSourceTCRMPartyBObj = (TCRMPartyBObj) vecInputElements
					.elementAt(0);

			vecSuspects = (Vector) vecInputElements.elementAt(1);

			theDWLControl = theSourceTCRMPartyBObj.getControl();

			coReturnMandatorySearchResults = Configuration.getConfiguration()
					.getConfigItem(RETURN_SUSPECT_ENABLED,
							theDWLControl.retrieveConfigContext())
					.getBooleanValue();

			coDoNotfication = Configuration.getConfiguration()
					.getConfigItem(NOTIFICATION_ENABLED,
							theDWLControl.retrieveConfigContext())
					.getBooleanValue();

			// 1. If the mandatory search has not been done and it is configured
			// to be done
			// do not create a suspect record
			if (((theSourceTCRMPartyBObj.getMandatorySearchDone() == null) || (!(theSourceTCRMPartyBObj
					.getMandatorySearchDone().equalsIgnoreCase("Y"))))
					&& (coReturnMandatorySearchResults == true)) {
				// do nothing - do not create suspect record
			} else {
				// 2. The mandatory search has been done (or is not a config
				// option set)

				// 3. Find the item in the vector that holds the best matched A2
				// party

				// suspect duplicate entry is created linking the source party
				// to target A2 party with a status of pending

				String langId = theDWLControl.getRequesterLanguage();
	
				for (Iterator iterator = vecSuspects.iterator(); iterator
						.hasNext();) {
					
					TCRMSuspectBObj objTCRMSuspectBObj = (TCRMSuspectBObj) iterator.next();
										
					CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
					SuspectStatusTypeBObj ct_suspectstatus = null;
					// Create the Vector of suspects holding only the best A2
					// match to send to notification.
					Vector bestA2Suspect = new Vector();
					bestA2Suspect.add(objTCRMSuspectBObj);

					objTCRMSuspectBObj.setControl(theDWLControl);
					objTCRMSuspectBObj.setPartyId(theSourceTCRMPartyBObj.getPartyId());
					objTCRMSuspectBObj.setSuspectStatusType(getSuspectStatus());
					ct_suspectstatus = (SuspectStatusTypeBObj) codeTypeCompHelper.getCodeTypeByCode(CodeTypeTableNames.SUSPECT_STATUS_TYPE, langId,
							objTCRMSuspectBObj.getSuspectStatusType(), theDWLControl);
					objTCRMSuspectBObj.setSuspectStatusValue(ct_suspectstatus.getname());
					objTCRMSuspectBObj.setSourceType(Configuration.getConfiguration().getConfigItem(
							IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_REASON_TYPE_SYSTEM_MARKED).getValue());
					suspComp.createSuspect(objTCRMSuspectBObj);

					// ******NOTIFICATION TO BE SENT FOR Notification_Type = 2
					// A2 Pending critical data change Suspect Marked
					if (coDoNotfication) {

						SuspectIdentificationNotification theSuspectIdentificationNotification = (SuspectIdentificationNotification) (TCRMClassFactory
								.getTCRMCommonNotification(NotificationTypes.SuspectIdentification));
								
						theSuspectIdentificationNotification
								.setTxnType((String) (theDWLControl
										.get(DWLControl.REQUEST_NAME)));
						theSuspectIdentificationNotification
								.setTxnTime(new Timestamp(System
										.currentTimeMillis()));
						theSuspectIdentificationNotification
								.setRequestId(theDWLControl.get(
										DWLControl.REQUEST_ID).toString());
						theSuspectIdentificationNotification
								.setUserId((String) (theDWLControl
										.get(TCRMControlKeys.USER_NAME)));
						theSuspectIdentificationNotification
								.setSourceParty(theSourceTCRMPartyBObj);
						theSuspectIdentificationNotification
								.setSuspectList(bestA2Suspect);
						theSuspectIdentificationNotification.setControl(theDWLControl);
						theCustomerNotification
								.sendNotification(theSuspectIdentificationNotification);
					}

					// ******NOTIFICATION TO BE SENT FOR Notification_Type = 2

				}
				// 4. Remove Suspect Vector attached to the source party
				theSourceTCRMPartyBObj.vecTCRMSuspectBObj.removeAllElements();
			}

		} catch (Exception ex) {
			if (logger.isErrorEnabled())
				logger.error(ResourceBundleHelper.resolve(
					ResourceBundleNames.DEFAULT_EXTERNAL_RULES_STRINGS,
					ERROR_MESSAGE, new Object[] { this.getClass().getName(),
							ex.getLocalizedMessage() }));

			if (ex instanceof TCRMException) {

				TCRMException tcrmEx = (TCRMException) ex;
				throw tcrmEx;
			} else {

				throw ex;
			}
		}

		// This rule returns nothing.
		return null;
	}

	/**
	 * Return suspect status as pending.
	 * This is default implementation, can be changed in extended class.
	 * 
	 * @throws Exception
	 * 
	 */
	protected String getSuspectStatus() throws Exception {
		return Configuration.getConfiguration().getConfigItem(IBM_PARTY_SUSPECT_PROCESSING_SUSPECT_STATUS_TYPE_SUSPECTDUPLICATE).getValue();
	}

}
