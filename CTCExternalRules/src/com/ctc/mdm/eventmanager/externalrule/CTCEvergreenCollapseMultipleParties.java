package com.ctc.mdm.eventmanager.externalrule;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Vector;

import com.dwl.base.error.DWLError;
import com.dwl.base.exception.DWLResponseException;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.requestHandler.beans.DWLServiceControllerLocal;
import com.dwl.base.requestHandler.beans.DWLServiceControllerLocalHome;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.commoncomponents.eventmanager.EMException;
import com.dwl.commoncomponents.eventmanager.EventManagerConstants;
import com.dwl.commoncomponents.eventmanager.EventManagerProperties;
import com.dwl.commoncomponents.eventmanager.EventObj;
import com.dwl.commoncomponents.eventmanager.EventTaskObject;
import com.dwl.commoncomponents.eventmanager.tcrm.constant.ResourceBundleNames;
import com.dwl.commoncomponents.eventmanager.test.BaseRule;
import com.dwl.tcrm.coreParty.constant.TCRMCoreErrorReasonCode;
import com.dwl.unifi.services.ServiceLocator;


public class CTCEvergreenCollapseMultipleParties extends BaseRule {

	private final static IDWLLogger logger = DWLLoggerManager.getLogger(CTCEvergreenCollapseMultipleParties.class);

	/** The first portion of the constructed XML */
	private static final String XML_BEGIN = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE TCRMService SYSTEM \"myTCRM.dtd\"><TCRMService><RequestControl><requestID>500088</requestID><DWLControl><requesterName>cusadmin</requesterName><requesterLanguage>100</requesterLanguage></DWLControl></RequestControl><TCRMTx><TCRMTxType>collapseMultipleParties</TCRMTxType><TCRMTxObject>TCRMConsolidatedPartyBObj</TCRMTxObject><TCRMObject><TCRMConsolidatedPartyBObj><TCRMPartyListBObj><TCRMPartyBObj><PartyId>";
	
	/** The last portion of the constructed XML */
	private static final String XML_END = "</PartyId></TCRMPartyBObj></TCRMPartyListBObj></TCRMConsolidatedPartyBObj></TCRMObject></TCRMTx></TCRMService>";
	
	private final static String EXCEPTION_RULE_FAILED = "Exception_Shared_RuleFailed";
	
	private final static String EXCEPTION_TRANSACTION_FAILED = "Exception_Shared_TransactionFailed";
	
	private final static long inactivatedErrorReason = new Long(TCRMCoreErrorReasonCode.PARTY_A_INACTIVE).longValue();
	
	/**
	* Default constructor.
	*/
	public CTCEvergreenCollapseMultipleParties() {
		super();
	}
	
	/**
	* Invokes the <code>collapseMultipleParties</code> transaction for a party.
	* This method creates the following transaction, using the business object key in the task
	* object passed in, as the party ID.
	* <p>
	* <xmp>
	* <?xml version="1.0" encoding="UTF-8"?>
	* <!DOCTYPE TCRMService SYSTEM "myTCRM.dtd">
	* <TCRMService>
	*   <RequestControl>
	*     <requestID>500088</requestID>
	*     <DWLControl>
	*       <requesterName>cusadmin</requesterName>
	*       <requesterLanguage>100</requesterLanguage>
	*     </DWLControl>
	*   </RequestControl>
	*   <TCRMTx>
	*     <TCRMTxType>collapseMultipleParties</TCRMTxType>
	*     <TCRMTxObject>TCRMConsolidatedPartyBObj</TCRMTxObject>
	*     <TCRMObject>
	*     	<TCRMConsolidatedPartyBObj>
	*       	<TCRMPartyListBObj>
	*         		<TCRMPartyBObj>
	*           		<PartyId>...party ID from task object...</PartyId>
	*         		</TCRMPartyBObj>
	*       	</TCRMPartyListBObj>
	*       </TCRMConsolidatedPartyBObj>
	*     </TCRMObject>
	*   </TCRMTx>
	* </TCRMService>
	* </xmp>
	* <p>
	* It then calls the DWLServiceController to execute the transaction.
	*
	* @param task
	*            Task object containing the party ID to perform the collapse party on.
	*
	* @see com.dwl.commoncomponents.eventmanager.test.BaseRule#executeRules(EventTaskObject)
	*/
	public void executeRules(EventTaskObject task) throws EMException {
	
	if (logger.isFineEnabled()) {
		logger
				.fine(this.getClass().getName()
						+ " executeRules(); Running CTCEvergreenCollapseMultipleParties Rule");
	}
	
	// use the cached properties
	EventManagerProperties prop = (EventManagerProperties) ServiceLocator
			.getInstance()
			.getService(
					"com.dwl.commoncomponents.eventmanager.EventManagerProperties");
	String jndiName = (String) prop.getProperty("EvergreenRule.JNDINAME");
	
	try {
	
		com.dwl.base.util.ServiceLocator wccServiceLocator = com.dwl.base.util.ServiceLocator
				.getInstance();
	
		DWLServiceControllerLocalHome homeInterface = (DWLServiceControllerLocalHome) wccServiceLocator.getLocalHome(jndiName);
	
		DWLServiceControllerLocal serviceController = homeInterface.create();
	
		Vector stringParameters = new Vector();
	
		HashMap hash = new HashMap();
		hash.put("RequestType", "standard");
		hash.put("TargetApplication", "tcrm");
		hash.put("ResponseType", "standard");
	
		// Remove leading space and change to use DTD
		StringBuffer strBuff = new StringBuffer(600);
	
		// append beginning of XML
		strBuff.append(XML_BEGIN);
	
		// Party ID comes from task object
		strBuff.append(task.getBusinessObjKey());
	
		// append end of XML
		strBuff.append(XML_END);
	
		Serializable response;
	
		if (logger.isFineEnabled()) {
			logger.fine(this.getClass().getName()
					+ " executeRules(); Request=" + strBuff.toString());
		}
	
		response = serviceController.processRequest(hash, strBuff
				.toString());
	
		if (logger.isFineEnabled()) {
			logger.fine(this.getClass().getName()
					+ " executeRules(); Response=" + response);
		}
	
		// add event to list of pending events in order to have a record
		// inserted to Event table
		task.addPendingEventObject(new EventObj("Suspect Processing",
				new Long(9), new Boolean(false),
				EventManagerConstants.TRIGGER_EVENT_TRIGGERED));
	    // the current party instance is inactivated after collpasing
	    //set the eventStatus to be excluded
	    task.setEventStatus(EventManagerConstants.EVENTSTATUS_EXCLUDED);
	    if (logger.isFineEnabled()) {
			logger.fine(task.getBusinessEntity() + " Object ID: " + task.getBusinessObjKey()
					+ " event status is set to excluded.");
		}
	} catch (DWLResponseException ce) {
		//set enventStatus to 5 if the party is inactivated.
		if (isPartyInactivated(ce)) {
			task.setEventStatus(EventManagerConstants.EVENTSTATUS_EXCLUDED);
	        if (logger.isFineEnabled()) {
				logger.fine(task.getBusinessEntity() + " Object ID: " + task.getBusinessObjKey()
						+ " event status is set to excluded by exception handling.");
			}
		}
	
		throw new EMException(ResourceBundleHelper.resolve(
				ResourceBundleNames.CUSTOMER_EM_EXTERNAL_RULES_STRINGS,
				EXCEPTION_TRANSACTION_FAILED,
				new Object[] { this.getClass().getName(),
						ce.getLocalizedMessage() }), ce);
	} catch (Exception e) {
		throw new EMException(ResourceBundleHelper.resolve(
				ResourceBundleNames.CUSTOMER_EM_EXTERNAL_RULES_STRINGS,
				EXCEPTION_RULE_FAILED,
				new Object[] { this.getClass().getName(),
						e.getLocalizedMessage() }), e);
	}
	}
	/**
	* @param ce
	* @return
	*/
	
	private boolean isPartyInactivated(DWLResponseException ce) {
	boolean partyInactivatedInd = false;
	
	if (ce.getStatus() !=null && ce.getStatus().getDwlErrorGroup() != null ) {
		Vector dwlErrorGroup = ce.getStatus().getDwlErrorGroup();
		DWLError error = null;
		for (int i = 0; i < dwlErrorGroup.size(); i++) {
			error = (DWLError) dwlErrorGroup.get(i);
			if (error.getReasonCode() == inactivatedErrorReason) {
				partyInactivatedInd = true;
			}
		}
	}
	
	return partyInactivatedInd;
	}
	
	
}
