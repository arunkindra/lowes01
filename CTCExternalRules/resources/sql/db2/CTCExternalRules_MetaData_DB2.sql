
-- @SqlSnippetPriority 400
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- MDM_TODO: CDKWB0044I Review CDDWLCOLUMNTP LOCALE_SENSITIVE settings
-- MDM_TODO: CDKWB0043I You may need to add entries to the INTERNALTXNKEY table for any new transactions


-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCExternalRules_SETUP_DB2.sql
-- 			db2 -vf CTCExternalRules_TRIGGERS_DB2.sql
-- 			db2 -vf CTCExternalRules_CONSTRAINTS_DB2.sql
--			db2 -vf CTCExternalRules_ERRORS_100_DB2.sql
-- 			db2 -vf CTCExternalRules_MetaData_DB2.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_DB2.sql
-- 			db2 -vf CTCExternalRules_CODETABLES_DB2.sql
	
	--#SET TERMINATOR ;
	


----------------------------------------------
-- Component type
----------------------------------------------

UPDATE CONFIGELEMENT SET VALUE ='true', LAST_UPDATE_DT = CURRENT_TIMESTAMP WHERE NAME = '/IBM/Party/SuspectProcessing/enabled';
UPDATE CONFIGELEMENT SET VALUE ='false', LAST_UPDATE_DT = CURRENT_TIMESTAMP WHERE NAME = '/IBM/Party/SuspectProcessing/Asynchronous/AutoCollapse/enabled';
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCA1SuspectsActionRule',last_update_dt = current timestamp where ext_rule_impl_id = 1036;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCA2SuspectsActionRule',last_update_dt = current timestamp where ext_rule_impl_id = 1033;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCAutoCollapsePartiesExtRule',last_update_dt = current timestamp where ext_rule_impl_id = 1011;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCBestSuspectMatchRules',last_update_dt = current timestamp where ext_rule_impl_id = 1037;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCBusinessKeyValidation',last_update_dt = current timestamp where ext_rule_impl_id = 1018;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCCollapseMultiplePartiesRule',last_update_dt = current timestamp where ext_rule_impl_id = 10119;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCCollapsePartiesWithRules',last_update_dt = current timestamp where ext_rule_impl_id = 1038;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCCriticalDataChangedRule',last_update_dt = current timestamp where ext_rule_impl_id = 1007;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCFSCollapseMultiplePartiesExtRule',last_update_dt = current timestamp where ext_rule_impl_id = 10120;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCPartyMatchCategoryExtRule',last_update_dt = current timestamp where ext_rule_impl_id = 1009;
--update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCPartyMatchRule',last_update_dt = current timestamp where ext_rule_impl_id = 1032;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCPartySuspectSearchRule',last_update_dt = current timestamp where ext_rule_impl_id = 1002;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCPartyUpdateExtRule',last_update_dt = current timestamp where ext_rule_impl_id = 1005;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCPersonMatchRule',last_update_dt = current timestamp where ext_rule_impl_id = 1000;
--update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCSkipOperationRule',last_update_dt = current timestamp where ext_rule_impl_id = 1032;
update javaimpl set java_classname = 'com.ctc.mdm.externalrule.CTCSuspectAddPartyRule',last_update_dt = current timestamp where ext_rule_impl_id = 1032;



----------------------------------------------
-- Error messages
----------------------------------------------
--CTCLSuspectsActionRule
INSERT INTO EXTRULE (RULE_ID, RULE_DESCRIPTION, CREATED_DT, LAST_UPDATE_DT, RULE_USAGE_TP_CD) VALUES (1000001, 'RULE FOR L SUSPECT PROCESSING ACTION', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 14);
INSERT INTO EXTRULEIMPLEM (EXT_RULE_IMPL_ID, RULE_ID, EXT_RULE_TP_CODE, RULE_IN_FORCE_IND, IMPL_ORDER, LAST_UPDATE_DT) VALUES (1000001, 1000001, 'J', 'Y', 1, CURRENT_TIMESTAMP);
INSERT INTO JAVAIMPL (EXT_RULE_IMPL_ID, JAVA_CLASSNAME, LAST_UPDATE_DT) VALUES (1000001, 'com.ctc.mdm.externalrule.CTCLSuspectsActionRule', CURRENT_TIMESTAMP);

--CTCEvergreenCollapseParties EventManager Rule
INSERT INTO EXTRULE (RULE_ID, RULE_DESCRIPTION, CREATED_DT, LAST_UPDATE_DT, RULE_USAGE_TP_CD) VALUES (1000002, 'Rule for CTCEvergreenCollapseMultipleParties', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 49);
INSERT INTO EXTRULEIMPLEM (EXT_RULE_IMPL_ID, RULE_ID, EXT_RULE_TP_CODE, RULE_IN_FORCE_IND, IMPL_ORDER, LAST_UPDATE_DT) VALUES (1000002, 1000002, 'J', 'Y', 1, CURRENT_TIMESTAMP);
INSERT INTO JAVAIMPL (EXT_RULE_IMPL_ID, JAVA_CLASSNAME, LAST_UPDATE_DT) VALUES (1000002, 'com.ctc.mdm.eventmanager.externalrule.CTCEvergreenCollapseMultipleParties', CURRENT_TIMESTAMP);



---------------------------------------------
-- Metadata setup
---------------------------------------------


----------------------------------------------
-- V_GROUP
----------------------------------------------


CREATE SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ AS BIGINT START WITH 1070000 INCREMENT BY 1 MINVALUE 1070000 MAXVALUE 1079999 CACHE 10;


DROP SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ RESTRICT;

----------------------------------------------
-- V_ELEMENTATTRIBUTE
----------------------------------------------


CREATE SEQUENCE DB2admin.V_ELATTR_ID_SEQ AS BIGINT START WITH 1070000 INCREMENT BY 1 MINVALUE 1070000 MAXVALUE 1079999 CACHE 10;

DROP SEQUENCE DB2admin.V_ELATTR_ID_SEQ RESTRICT;

----------------------------------------------
-- Transactions
----------------------------------------------


----------------------------------------------
-- CDBUSINESSTXTP
----------------------------------------------


----------------------------------------------
-- CDINTERNALTXNTP
----------------------------------------------




----------------------------------------------
-- BUSINTERNALTXN
----------------------------------------------



----------------------------------------------
-- BUSINESSTXREQRESP
----------------------------------------------



----------------------------------------------
-- INTERNALTXREQRESP
----------------------------------------------


----------------------------------------------
-- GROUPTXMAP
----------------------------------------------



----------------------------------------------
-- TAIL setup
----------------------------------------------



