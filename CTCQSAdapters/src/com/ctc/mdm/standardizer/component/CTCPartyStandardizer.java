package com.ctc.mdm.standardizer.component;

import com.dwl.tcrm.coreParty.component.TCRMPartyStandardizer;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.exception.TCRMException;

public class CTCPartyStandardizer extends TCRMPartyStandardizer {

	@Override
	public TCRMPersonNameBObj standardizePersonName(TCRMPersonNameBObj personNameBObj)
			throws TCRMException 
	{
		personNameBObj.setStdGivenNameOne(personNameBObj.getGivenNameOne());

		personNameBObj.setStdGivenNameTwo(personNameBObj.getGivenNameTwo());

		personNameBObj.setStdGivenNameThree(personNameBObj.getGivenNameThree());

		personNameBObj.setStdGivenNameFour(personNameBObj.getGivenNameFour());

		personNameBObj.setStdLastName(personNameBObj.getLastName());

		if ("N".equalsIgnoreCase(personNameBObj.getStandardFormattingIndicator())) {
			personNameBObj.setStandardFormattingIndicator(null);
	    }
		
		return personNameBObj;
	}
}
