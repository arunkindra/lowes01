package com.ctc.mdm.standardizer.component;

import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMAddressStandardizer;
import com.dwl.tcrm.utilities.StringUtils;

public class CTCAddressStandardizer extends TCRMAddressStandardizer {

	@Override
	public TCRMAddressBObj standardizeAddress(TCRMAddressBObj theTCRMAddressBObj) {
		
		/*
		 * Convert all fields to Upper Case
		 */
		String addressLineOne = theTCRMAddressBObj.getAddressLineOne();
		if(StringUtils.isNonBlank(addressLineOne)){
			addressLineOne = addressLineOne.toUpperCase();
			theTCRMAddressBObj.setAddressLineOne(addressLineOne);
		}

		String addressLineTwo = theTCRMAddressBObj.getAddressLineTwo();
		if(StringUtils.isNonBlank(addressLineTwo)){
			addressLineTwo = addressLineTwo.toUpperCase();
			theTCRMAddressBObj.setAddressLineTwo(addressLineTwo);
		}

		String addressLineThree = theTCRMAddressBObj.getAddressLineThree();
		if(StringUtils.isNonBlank(addressLineThree)){
			addressLineThree = addressLineThree.toUpperCase();
			theTCRMAddressBObj.setAddressLineThree(addressLineThree);
		}

		String city = theTCRMAddressBObj.getCity();
		if(StringUtils.isNonBlank(city)){
			city = city.toUpperCase();
			theTCRMAddressBObj.setCity(city);
		}

		String postalCode = theTCRMAddressBObj.getZipPostalCode();
		if(StringUtils.isNonBlank(postalCode)){
			postalCode = postalCode.toUpperCase();
			theTCRMAddressBObj.setZipPostalCode(postalCode);
		}
		
		return theTCRMAddressBObj;
	}
}
