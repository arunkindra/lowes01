package com.ctc.mdm.thirdparty.integration.iis8.adapter;

import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.management.config.client.Configuration;
import com.dwl.tcrm.common.TCRMControlKeys;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.utilities.StringUtils;
import com.ibm.mdm.common.codetype.component.CodeTypeTableNames;
import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;
import com.ibm.mdm.thirdparty.integration.iis8.adapter.InfoServerStandardizerAdapter;

public class CTCInfoServerStandardizerAdapter extends
		InfoServerStandardizerAdapter {

	@Override
	public TCRMAddressBObj standardizeAddress(TCRMAddressBObj inputAddressBObj)
			throws TCRMException {
		TCRMAddressBObj addressBObj =  super.standardizeAddress(inputAddressBObj);
		
		/*
		 * 2011-10-27, VijayB - If QS-SERP successfully standardized the address, set the CountryValue = Canada
		 * since only Canadian Addresses would be flagged as standardized.
		 * 
		 * 2011-11-01, VijayB - ProvinceState could be changed after the QS standardization.
		 * Therefore, Reset the ProvinceStateType to the correct value.
		 */
		String standardFormatingIndicator = addressBObj.getStandardFormatingIndicator();
		if(StringUtils.isNonBlank(standardFormatingIndicator) 
				&& StringUtils.compareIgnoreCaseWithTrim(standardFormatingIndicator, "Y")){
			
			CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
			String langType = addressBObj.getControl().get(TCRMControlKeys.LANG_ID).toString();
			String provinceStateValue = addressBObj.getProvinceStateValue();
			String provinceStateType = null;
			try {
				String countryType = Configuration.getConfiguration().getConfigItem("/IBM/ThirdPartyAdapters/IIS/defaultCountry").getValue();
				String countryValue = codeTypeCompHelper.getCodeTypeByCode(CodeTypeTableNames.COUNTRY_TYPE, langType,
						countryType, addressBObj.getControl()).getvalue();
				provinceStateType = codeTypeCompHelper.getCodeTypeByValue(CodeTypeTableNames.PROVINCE_STATE_TYPE, langType,
						provinceStateValue, addressBObj.getControl()).gettp_cd();
				addressBObj.setProvinceStateType(provinceStateType);
				addressBObj.setCountryValue(countryValue);
				addressBObj.setCountryType(countryType);
			} catch (DWLBaseException e) {
				throw new TCRMException(e.getMessage());
			} catch (Exception e) {
				throw new TCRMException(e.getMessage());
			}
			
		}
		return addressBObj;
	}
}
