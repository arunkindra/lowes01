package com.ctc.mdm.thirdparty.integration.iis8.converter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;

import com.ctc.mdm.standardizer.component.CTCAddressStandardizer;
import com.ctc.mdm.standardizer.component.CTCPartyStandardizer;
import com.dwl.base.DWLCommon;
import com.dwl.base.DWLControl;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.StringUtils;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.management.ManagementException;
import com.dwl.management.config.client.Configuration;
import com.dwl.management.config.repository.ConfigurationRepositoryException;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMOrganizationNameBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.thirdparty.integration.constant.ResourceBundleNames;
import com.ibm.mdm.thirdparty.integration.iis8.InfoServerConfigurationConstants;
import com.ibm.mdm.thirdparty.integration.iis8.converter.InfoServerConverter;

public class CTCDefaultInfoServerConverter extends InfoServerConverter {

    public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2007, 2011\nUS Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";

	
	private final static IDWLLogger logger = DWLLoggerManager
		.getLogger(CTCDefaultInfoServerConverter.class);

	private static HashMap<String, HashMap<String, Vector<String>>> allCachedMappings = 
			new HashMap<String, HashMap<String, Vector<String>>>();
	
	private HashMap<String,String> skipStandardizationMap = new HashMap<String, String>();
	
	private final static String WARN_IIS_CONVERTER_UNHANDLED_ADDRESS = "Warn_IIS_DefaultInfoServerConverter_UnhandledAddress";

	private final static String WARN_IIS_CONVERTER_UNHANDLED_PERSON_NAME = "Warn_IIS_DefaultInfoServerConverter_UnhandledPersonName";

	private final static String WARN_IIS_CONVERTER_UNHANDLED_ORG_NAME = "Warn_IIS_DefaultInfoServerConverter_UnhandledOrgName";

	private final static String WARN_IIS_CONVERTER_UNHANDLED_PHONE_NUMBER = "Warn_IIS_DefaultInfoServerConverter_UnhandledPhoneNumber";
	
	private final static String LOCATION_NORMALIZATION_ENABLED = "/IBM/Party/LocationNormalization/enabled";
	private final static String EXCEPTION_IIS_INVOKE_SERVICE ="Exception_IIS_InvokeService";
	private final static String EXCEPTION_IIS_CONVERTER_CONFIG = "Exception_IIS_Config";
	
	//PMR#11397,057,649 - BMO - Andrew Wang 2010-08-13
	//
	//Introduce "post formatter" so the client may disable the post format process by setting its
	//to value "None". In such way, the un-normalized fields (e.g. AddressLineOne/Two/Three) 
	//will keep the value returned by the standardizer (i.e. QS, Trillium, etc.)
	//
	//Attention: Linda Park, Wei Zheng, Lee McCallum
	private static final String ADDRESS_FORMATTER_MAPPINGS_NAME = "/IBM/ThirdPartyAdapters/Address/PostFormatter";
	private final static String ADDRESS_SEPARATOR = " ";
	private final static String ADDRESS_STANDARD_FORMATTING_IND_ENABLED = "/IBM/ThirdPartyAdapters/IIS/StandardizeAddress/StandardFormattingIndicator/enabled";
	private final static String CONTACTMETHOD_STANDARD_FORMATTING_IND_ENABLED = "/IBM/ThirdPartyAdapters/IIS/StandardizePhoneNumber/StandardFormattingIndicator/enabled";

	public CTCDefaultInfoServerConverter(String configurationKey) {
		super(configurationKey);
	}

	/* (non-Javadoc)
	 * @see com.ibm.mdm.thirdparty.integration.iis8.converter.IInformationServerConverter#convertToBusinessObject(com.dwl.base.DWLCommon, java.lang.Object)
	 */
	public DWLCommon convertToBusinessObject(DWLCommon targetBObj, Object data, String delimiter)
			throws DWLBaseException {
		
		try {
			// Check if names/address were sucessfully standardized. For
			// multinational address standardization,
			// unhandled text field should be checked. For name standardization,
			// unhandled and exceptional data fields
			// should be checked.
			StringTokenizer strTokens = new StringTokenizer(failureIndicators,";");
			String failureInd;
			String columnValue = null;
			StringBuffer unhandledData = new StringBuffer();
			while (strTokens.hasMoreTokens()) {
				failureInd = strTokens.nextToken();
				
				columnValue = getObject(data, failureInd);
				if (columnValue!=null) 
					unhandledData.append(columnValue.trim());
				unhandledData.append(delimiter);
			}

			if (unhandledData.toString() != null
					&& unhandledData.toString().trim().length() > 0) {
				if (targetBObj instanceof TCRMAddressBObj) {
					if (logger.isWarnEnabled())
						logger
							.warn(ResourceBundleHelper
									.resolve(
											ResourceBundleNames.THIRD_PARTY_INTEGRATION_STRINGS,
											WARN_IIS_CONVERTER_UNHANDLED_ADDRESS,
											new Object[] { unhandledData
													.toString() }));
		
					// Use the CTC Address Standardizer. This converts all the attribute values into upper case.
					CTCAddressStandardizer addressStandardizer = new CTCAddressStandardizer();
					TCRMAddressBObj address = (TCRMAddressBObj) targetBObj;
					address = addressStandardizer.standardizeAddress(address);
					// set formating indicator to 'N' for unstandardized address
					address.setStandardFormatingIndicator("N");					
					return address;
				} else if (targetBObj instanceof TCRMPersonNameBObj) {
					if (logger.isWarnEnabled())
						logger
							.warn(ResourceBundleHelper
									.resolve(
											ResourceBundleNames.THIRD_PARTY_INTEGRATION_STRINGS,
											WARN_IIS_CONVERTER_UNHANDLED_PERSON_NAME,
											new Object[] { unhandledData
													.toString() }));
		
					CTCPartyStandardizer partyStandardizer = new CTCPartyStandardizer();
					TCRMPersonNameBObj personName = (TCRMPersonNameBObj) targetBObj;
					personName = partyStandardizer
							.standardizePersonName(personName);
					personName.setStandardFormattingIndicator("N");
					return personName;
				} else if (targetBObj instanceof TCRMOrganizationNameBObj) {
					if (logger.isWarnEnabled())
						logger
							.warn(ResourceBundleHelper
									.resolve(
											ResourceBundleNames.THIRD_PARTY_INTEGRATION_STRINGS,
											WARN_IIS_CONVERTER_UNHANDLED_ORG_NAME,
											new Object[] { unhandledData
													.toString() }));
		
					CTCPartyStandardizer partyStandardizer = new CTCPartyStandardizer();
					TCRMOrganizationNameBObj orgName = (TCRMOrganizationNameBObj) targetBObj;
					orgName = partyStandardizer
							.standardizeOrganizationName(orgName);
					orgName.setStandardFormattingIndicator("N");
					return orgName;
				} else if (targetBObj instanceof TCRMContactMethodBObj) {
					if(logger.isWarnEnabled())
						logger.warn(ResourceBundleHelper.resolve(
									ResourceBundleNames.THIRD_PARTY_INTEGRATION_STRINGS,
									WARN_IIS_CONVERTER_UNHANDLED_PHONE_NUMBER,
									new Object[] { unhandledData.toString() }));
		
					CTCAddressStandardizer addressStandardizer = new CTCAddressStandardizer();
					TCRMContactMethodBObj contactMethodBObj = (TCRMContactMethodBObj) targetBObj;
					contactMethodBObj = addressStandardizer.standardizeContactMethod(contactMethodBObj);
					// set formating indicator to 'N' for unstandardized address
					contactMethodBObj.setStandardFormatingIndicator("N");
					return contactMethodBObj;
				} 
			} else {
				if (targetBObj instanceof TCRMAddressBObj) {
					if(Configuration.getConfiguration().getConfigItem(ADDRESS_STANDARD_FORMATTING_IND_ENABLED, targetBObj.getControl().retrieveConfigContext()).getBooleanValue()) {
						((TCRMAddressBObj) targetBObj).setStandardFormatingIndicator("Y");	
					}
				} else if (targetBObj instanceof TCRMContactMethodBObj) {
					if(Configuration.getConfiguration().getConfigItem(CONTACTMETHOD_STANDARD_FORMATTING_IND_ENABLED, targetBObj.getControl().retrieveConfigContext()).getBooleanValue()) {
						((TCRMContactMethodBObj) targetBObj).setStandardFormatingIndicator("Y");
					}
				} else if (targetBObj instanceof TCRMPersonNameBObj) {
					((TCRMPersonNameBObj) targetBObj).setNameStandardizedFlag(true);
					((TCRMPersonNameBObj) targetBObj).setStandardFormattingIndicator("Y");
				} else if (targetBObj instanceof TCRMOrganizationNameBObj) {
					((TCRMOrganizationNameBObj) targetBObj).setNameStandardizedFlag(true);
					((TCRMOrganizationNameBObj) targetBObj).setStandardFormattingIndicator("Y");
				}
			}

			
			String configKey = configurationKey + InfoServerConfigurationConstants.SUFFIX_OUT_MAPPING;
			HashMap<String, Vector<String>> cachedOutputMappings = getCachedMappings(configKey,false);
			
			Iterator<String> it = cachedOutputMappings.keySet().iterator();
			while (it.hasNext()) {
				
				StringBuffer rightHandSideValues = new StringBuffer();
				String leftHandSideField = it.next();
				
				// if the field contains '%' or '?', it was not standardized by QS
				// and was only converted to upper case
				// get the value from the skipStandarizationMap
				if (skipStandardizationMap.get(leftHandSideField) != null) {
					rightHandSideValues.append((String) skipStandardizationMap
							.get(leftHandSideField));
				} else {
				//get the right hand side value
				
					Vector<String> rightHandSideFields = cachedOutputMappings.get(leftHandSideField);
					
					for (int i=0; i<rightHandSideFields.size(); i++) {
						String rightHandSideField = (String)rightHandSideFields.get(i);
						
						String rightHandSideValue = getObject(data, rightHandSideField); 

						if (rightHandSideValue ==null) {
							rightHandSideValue = "";
						}
						
						rightHandSideValue = rightHandSideValue.trim();
						
						if (StringUtils.isNonBlank(rightHandSideValue)) {
							rightHandSideValues.append(rightHandSideValue);

							//if it's not the last item then add the delimiter character
							if (i<rightHandSideFields.size() - 1) {
								rightHandSideValues.append(delimiter);
							}
						}
					}
				}
				//set the value to the right hand side field
				setObject(targetBObj, leftHandSideField, rightHandSideValues.toString().trim());
			}
			
		    //PMR65282,057,649 -- Moved address post-format address normalization here
			//from InforServerStandardizerAdapter#standardizeAddress
			
			//PMR# 65282.057.649 fix 
			if (targetBObj instanceof TCRMAddressBObj && getLocationNormalizationEnabled(targetBObj.getControl())) {
				//If isLocationNormalizationEnabled is ON,  post-format addressLineOne, two, etc.
				
				/*
				 * 2011-09-28, VijayB - AddressLines need not be derived.
				 * MDM will use the output AddressLines from QS.
				 */
//				ThirdPartyStandardizerUtil.deriveAddressLines(targetBObj, ADDRESS_FORMATTER_MAPPINGS_NAME, ADDRESS_SEPARATOR);
			}
			
			//PMR 67651,057,649 
			//if request addressLineOne is about mail box, QS standardization moved it to addressLineTwo. We need to move it back
			if (targetBObj instanceof TCRMAddressBObj) {
				TCRMAddressBObj address = (TCRMAddressBObj)targetBObj;
				
				//move addressLine3 to addressLine2 if addressLine 2 is empty and addressLine3 is not.
				if(!StringUtils.isNonBlank(address.getAddressLineTwo()) && 
						StringUtils.isNonBlank(address.getAddressLineThree())) {
					address.setAddressLineTwo(address.getAddressLineThree());
					address.setAddressLineThree(null);
				}
				//move addressLine2 to addressLine1 if addressLine 1 is empty and addressLine2 is not.
				if(!StringUtils.isNonBlank(address.getAddressLineOne()) && 											
						StringUtils.isNonBlank(address.getAddressLineTwo())) {
					
					address.setAddressLineOne(address.getAddressLineTwo());
					address.setAddressLineTwo(null);
					//for scenario addressline1 is empty, addressLine2 and addressLine3 are not empty.
					if (StringUtils.isNonBlank(address.getAddressLineThree())) {						
						address.setAddressLineTwo(address.getAddressLineThree());
						address.setAddressLineThree(null);						
					}
				}
				
				//
			}
			//end of PMR 67651,057,649
			

			
		} catch (Exception e) {
			if(logger.isErrorEnabled())
				logger.error(e.getLocalizedMessage());
			throw new DWLBaseException(e.getLocalizedMessage());
		}
		
		return targetBObj;

	}

	/* (non-Javadoc)
	 * @see com.ibm.mdm.thirdparty.integration.iis8.converter.IInformationServerConverter#convertToIISDataFormat(com.dwl.base.DWLCommon)
	 */
	public Object convertToIISDataFormat(Object inputData, DWLCommon bObj, String delimiter)
			throws DWLBaseException {
		//compose the complete configuration key
		String configKey = configurationKey + InfoServerConfigurationConstants.SUFFIX_IN_MAPPING;
		
		HashMap<String, Vector<String>> cachedInputMappings = getCachedMappings(configKey,true);
		try {
			
			Iterator<String> it = cachedInputMappings.keySet().iterator();
			while (it.hasNext()) {
				String leftHandSideField = it.next();
				Vector<String> rightHandSideFields = cachedInputMappings.get(leftHandSideField);
				
				//get the right hand side value
				StringBuffer rightHandSideValues = new StringBuffer();
				for (int i=0; i< rightHandSideFields.size(); i++) {
					String rightHandSideField = rightHandSideFields.get(i);
					String fieldName = rightHandSideField.substring(0,1).toUpperCase() + rightHandSideField.substring(1);
					//String getterName = "get" + fieldName;
					//Method getter = bObj.getClass().getMethod(getterName);
					//String rightHandSideValue = (String)getter.invoke(bObj);
					String rightHandSideValue = getObject(bObj, rightHandSideField);
					
					if (rightHandSideValue!=null) {
						
						//PMR#01135,999,706
						if (skipIISStandardization(bObj, rightHandSideValue)) {
						// if '%' or '?' exist, put the string in the Map for later use
						//if (rightHandSideValue.indexOf("%") != -1 || rightHandSideValue.indexOf("?") != -1) {
							if (fieldName.equals("GivenNameOne")
									|| fieldName.equals("GivenNameTwo")
									|| fieldName.equals("GivenNameThree")
									|| fieldName.equals("GivenNameFour")
									|| fieldName.equals("LastName"))
								fieldName = "Std" + fieldName;
							if (fieldName.equals("OrganizationName"))
								fieldName = "SOrganizationName";
							skipStandardizationMap.put(fieldName, rightHandSideValue.toUpperCase());
						} else {
							//if it doesn't contain wildcard char, append it.
							rightHandSideValues.append(rightHandSideValue);
						}
						
					}
					
					//if it's not the last field then append space between fields that are concattenated together
					if (i < rightHandSideFields.size() -1) {
						rightHandSideValues.append(delimiter);
					}
				}
				
				//set the value to the left hand side field
				setObject(inputData, leftHandSideField, rightHandSideValues.toString());
				
			}
			
		} catch (Exception e) {
			if(logger.isErrorEnabled())
				logger.error(e.getLocalizedMessage());
			throw new DWLBaseException(e.getLocalizedMessage());
		}
		
		return inputData;

	}

	
	/**
	 * This methods returns the mappings that have been cached. The assumption
	 * is that all the adapter names in the QS projects are unique.
	 */
	protected HashMap<String, Vector<String>> getCachedMappings(String configKey, boolean isInput) {

		
		if (allCachedMappings.containsKey(configKey)) {
			return allCachedMappings.get(configKey);
		}

		synchronized (CTCDefaultInfoServerConverter.class) {
			// just in case
			if (allCachedMappings.containsKey(configKey)) {
				return allCachedMappings.get(configKey);
			}

			// find out how many input columns are defined for this converter
			// Use the isInput to determine whether we need to get
			// the input or output map
			StringTokenizer strTokens = null;
			if (isInput) {
				strTokens = new StringTokenizer(inputMappings, ";");
			} else {
				strTokens = new StringTokenizer(outputMappings, ";");
			}
			
			HashMap<String, Vector<String>> columns = new HashMap<String, Vector<String>>(strTokens.countTokens());
			String token;
			String leftHandSideColumn;
			String rightHandSideFields;
			while (strTokens.hasMoreTokens()) {
				token = strTokens.nextToken();
				leftHandSideColumn = token.substring(0, token.indexOf('='));
				rightHandSideFields = token.substring(token.indexOf('=') + 1);
				StringTokenizer fieldTokens = new StringTokenizer(
						rightHandSideFields, "+");
				Vector<String> fields = new Vector<String>(fieldTokens.countTokens());
				while (fieldTokens.hasMoreTokens()) {
					fields.add(fieldTokens.nextToken());
				}
				columns.put(leftHandSideColumn, fields);
			}

			// add to the all cache, use configKey as key
			allCachedMappings.put(configKey, columns);
		}

		return allCachedMappings.get(configKey);
	}
	
	
	private boolean getLocationNormalizationEnabled(DWLControl dwlControl) throws TCRMException {
		//get the location normalization setting	
		boolean isLocationNormalizationEnabled = false;
		
		try {
			isLocationNormalizationEnabled = Configuration.getConfiguration().getConfigItem(LOCATION_NORMALIZATION_ENABLED, dwlControl.retrieveConfigContext()).getBooleanValue();
		} catch (ConfigurationRepositoryException e) {
			String errMsg = ResourceBundleHelper.resolve(
					ResourceBundleNames.THIRD_PARTY_INTEGRATION_STRINGS,
					EXCEPTION_IIS_CONVERTER_CONFIG, new Object[] { e
							.getLocalizedMessage() });
			if(logger.isErrorEnabled())
				logger.error(errMsg);
			throw new TCRMException(errMsg);
		} catch (ManagementException e) {
			String errMsg = ResourceBundleHelper.resolve(
					ResourceBundleNames.THIRD_PARTY_INTEGRATION_STRINGS,
					EXCEPTION_IIS_CONVERTER_CONFIG, new Object[] { e
							.getLocalizedMessage() });
			if(logger.isErrorEnabled())
				logger.error(errMsg);
			throw new TCRMException(errMsg);
		}
		
		return isLocationNormalizationEnabled;
	}
	
	/**
	 * skipIISStanddardization verifies the transaction type and object value and decide whether 
	 * the value is sent to IIS server for standardization.
	 * 
	 * The behavior is that IIS adaptor does not send the value to IIS server if and only if the 
	 * transaction is search transaction and the value includes % or ?. 
	 * 
	 * Transaction type is acquired from DWLControl in the pass-in business object. 
	 * 
	 * The current behavior can be overridden if required.
	 * 
	 * @param bObj MDM business object including value which may need to be sent to 
	 *             IIS server for standardization
	 * @param value the value which may need to be sent to IIS server for standardization
	 * @return boolean value. If true, the value will not be sent to IIS.
	 *                        If false, the value will be sent to IIS.
	 */
	protected boolean skipIISStandardization(DWLCommon bObj, String value) 
												throws Exception{
		
		boolean isSkip = false;
		final String searchTx = "search";
		DWLControl control = bObj.getControl();
		
		//If control is null, this must be caused by other defects. 
		//we could generate and throw TCRMException or skip IIS or send value to IIS server
		//Considering we only skip IIS when transaction is definitely search and value include % or ?,
		//choose to send value to IIS server as  default behavior for scenario of null control.
		//If required, this behavior can easily be overriden by overriding this method
		
		if (control  == null) {
			logger.warn("DWLControl is null during verificaiton of skipping IIS standardization.");
			return isSkip;
		} 
		
		String txName = control.getRequestName();
		//if transaction is search transaction and value includes % or ?, MDM does not send 
		//value to IIS server and just change the value to upper case later.
		if (txName !=null && txName.startsWith(searchTx) && 
				(value.indexOf("%") >= 0 || value.indexOf("?") >= 0)) {
			isSkip = true;
			logger.info(txName + " skips standardizing " + value + " with IIS!");
		}

		return isSkip;
	}


	
}
