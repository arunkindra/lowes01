
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[177a72ce2810653867d9fbd1b76b3522]
 */

package com.ctc.mdm.codetable.obj;

import com.dwl.base.exception.DWLBaseException;

import com.ibm.mdm.common.codetype.obj.NLSCodeTypeAdminBObj;


import com.ibm.mdm.common.codetype.obj.CodeTypeColumnMetadataBObj;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
@SuppressWarnings("serial")
public class CTCAdminSysPriorityTypeBObj extends  NLSCodeTypeAdminBObj  {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public CTCAdminSysPriorityTypeBObj() throws DWLBaseException{
	      super();
	      init();
	 }
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	 @Override
	 protected String getCodeTypeName() {
	      return "Cdctcadminsysprioritytp";
	 }
	 
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	 private void init() {
	 	metaDataMap.put("adminsystpcd", null);	  
	 	metaDataMap.put("name", null);	  
	 	metaDataMap.put("prioritytpcd", null);	  
	 }	 
	 
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	 public void refreshMap() {
	      if(this.bRequireMapRefresh) {
	           super.refreshMap();
			 	metaDataMap.put("adminsystpcd", getadminsystpcd());
			 	metaDataMap.put("name", getname());
			 	metaDataMap.put("prioritytpcd", getprioritytpcd());
	            bRequireMapRefresh = false;
	      }
	 }
	   
	 
	    	       	 	
	    	/**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public String getadminsystpcd (){
	    		return gettp_cd();
	    	}
	    	       	 	
		    /**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public void setadminsystpcd (String adminsystpcd){	    	   
	    		metaDataMap.put("adminsystpcd", adminsystpcd);
		    	if (adminsystpcd != null && "".equals(adminsystpcd.trim())) adminsystpcd = null;	
	
	    		super.settp_cd(adminsystpcd);
	    	}
	    	/**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public String getname (){
	    		return getvalue();
	    	}
	    	       	 	
	        /**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public void setname (String name){	
	    		metaDataMap.put("name", name);
		    	if (name != null && "".equals(name.trim())) name = null;		
	
	    		super.setvalue(name);
	    	}
	    	/**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public String getprioritytpcd (){
	    		return codeTypeEObj.getColumnStringValue(codeTypeEObj.getCodeTypeMetadataBaseBObj().getColumnNameByDesignation(CodeTypeColumnMetadataBObj.MISCELLANEOUS));
	    	}
	    	       	 	
	        /**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public void setprioritytpcd (String prioritytpcd){	
	       		metaDataMap.put("prioritytpcd", prioritytpcd);
		    	if (prioritytpcd != null && "".equals(prioritytpcd.trim())) prioritytpcd = null;		
	
	    		codeTypeEObj.setColumnValue(codeTypeEObj.getCodeTypeMetadataBaseBObj().getColumnNameByDesignation(CodeTypeColumnMetadataBObj.MISCELLANEOUS),prioritytpcd);	 
	    	}	
	    	       	 	
	    	       	 	
	    	       	 	
	    	       	 	
}



