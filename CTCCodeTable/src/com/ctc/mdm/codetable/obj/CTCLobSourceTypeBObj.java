
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[ea1b780118eee6e4764437a513a28172]
 */

package com.ctc.mdm.codetable.obj;

import com.dwl.base.exception.DWLBaseException;

import com.ibm.mdm.common.codetype.obj.NLSCodeTypeAdminBObj;


import com.ibm.mdm.common.codetype.obj.CodeTypeColumnMetadataBObj;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
@SuppressWarnings("serial")
public class CTCLobSourceTypeBObj extends  NLSCodeTypeAdminBObj  {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	public CTCLobSourceTypeBObj() throws DWLBaseException{
	      super();
	      init();
	 }
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	 @Override
	 protected String getCodeTypeName() {
	      return "Cdctclobsourcetp";
	 }
	 
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	 private void init() {
	 	metaDataMap.put("lobsourcetpcd", null);	  
	 	metaDataMap.put("name", null);	  
	 	metaDataMap.put("adminsystemtpcd", null);	  
		metaDataMap.put("adminsystemtpvalue", null);	
	 	metaDataMap.put("lobtpcd", null);	  
		metaDataMap.put("lobtpvalue", null);	
	 }	 
	 
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
	 * @generated
	 */
	 public void refreshMap() {
	      if(this.bRequireMapRefresh) {
	           super.refreshMap();
			 	metaDataMap.put("lobsourcetpcd", getlobsourcetpcd());
			 	metaDataMap.put("name", getname());
			 	metaDataMap.put("adminsystemtpcd", getadminsystemtpcd());
	  			metaDataMap.put("adminsystemtpvalue", getadminsystemtpvalue ());
			 	metaDataMap.put("lobtpcd", getlobtpcd());
	  			metaDataMap.put("lobtpvalue", getlobtpvalue ());
	            bRequireMapRefresh = false;
	      }
	 }
	   
		
		/**
    	 * <!-- begin-user-doc -->
	     * <!-- end-user-doc -->
	     *
	     * @generated
    	 */
    	public String getadminsystemtpvalue (){
			return getcategory_tp_value();
    	}
    	
    	/**
    	 * <!-- begin-user-doc -->
	     * <!-- end-user-doc -->
	     *
	     * @generated
    	 */
    	public void setadminsystemtpvalue(String typeValue) {
			metaDataMap.put("adminsystemtpvalue", typeValue);
			if (typeValue != null && "".equals(typeValue.trim())) typeValue = null;
			
			setcategory_tp_value(typeValue); 
		}
			private String lobtpvalue;
		
		/**
    	 * <!-- begin-user-doc -->
	     * <!-- end-user-doc -->
	     *
	     * @generated
    	 */
    	public String getlobtpvalue (){
    		return lobtpvalue;
    	}
    	
    	/**
    	 * <!-- begin-user-doc -->
	     * <!-- end-user-doc -->
	     *
	     * @generated
    	 */
    	public void setlobtpvalue(String typeValue) {
			metaDataMap.put("lobtpvalue", typeValue);
			if (typeValue != null && "".equals(typeValue.trim())) typeValue = null;
			
			this.lobtpvalue = typeValue;
		}
	 
	    	       	 	
	    	/**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public String getlobsourcetpcd (){
	    		return gettp_cd();
	    	}
	    	       	 	
		    /**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public void setlobsourcetpcd (String lobsourcetpcd){	    	   
	    		metaDataMap.put("lobsourcetpcd", lobsourcetpcd);
		    	if (lobsourcetpcd != null && "".equals(lobsourcetpcd.trim())) lobsourcetpcd = null;	
	
	    		super.settp_cd(lobsourcetpcd);
	    	}
	    	/**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public String getname (){
	    		return getvalue();
	    	}
	    	       	 	
	        /**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public void setname (String name){	
	    		metaDataMap.put("name", name);
		    	if (name != null && "".equals(name.trim())) name = null;		
	
	    		super.setvalue(name);
	    	}
	    	/**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public String getadminsystemtpcd (){
	       		return getcategory_tp_cd();
	       	}
	    	       	 	
	        /**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */
	    	public void setadminsystemtpcd (String adminsystemtpcd){	
	       		metaDataMap.put("adminsystemtpcd", adminsystemtpcd);
		    	if (adminsystemtpcd != null && "".equals(adminsystemtpcd.trim())) adminsystemtpcd = null;		
	
	    		super.setcategory_tp_cd(adminsystemtpcd);
	    	}
	    	/**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */			
	    	public String getlobtpcd (){
	       		return codeTypeEObj.getColumnStringValue(codeTypeEObj.getCodeTypeMetadataBaseBObj().getColumnNameByDesignation(
	    			CodeTypeColumnMetadataBObj.CATEGORY_CODE + 1)	
	    		);
	       	}
	    	       	 	
	        /**
	    	 * <!-- begin-user-doc -->
		     * <!-- end-user-doc -->
		     *
		     * @generated
	    	 */   
	    	public void setlobtpcd (String lobtpcd){			
	       		metaDataMap.put("lobtpcd", lobtpcd);
		    	if (lobtpcd != null && "".equals(lobtpcd.trim())) lobtpcd = null;		
	
	    		codeTypeEObj.setColumnValue(codeTypeEObj.getCodeTypeMetadataBaseBObj().getColumnNameByDesignation(
	    			CodeTypeColumnMetadataBObj.CATEGORY_CODE + 1)	
	    			,lobtpcd);
	    	}
	    	       	 	
	    	       	 	
	    	       	 	
	    	       	 	
}



