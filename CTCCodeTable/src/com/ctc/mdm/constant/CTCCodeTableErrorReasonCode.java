/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[0e30ce6f6daebfd72d33a10db2fc4115]
 */
package com.ctc.mdm.constant;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Defines the error message codes used in this module.
 *
 * @generated
 */
public class CTCCodeTableErrorReasonCode {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: LobSourceTpCd
     *
     * @generated
     */
    public final static String CTCLOBSOURCE_LOBSOURCETPCD_NULL = "1020004";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: Name
     *
     * @generated
     */
    public final static String CTCLOBSOURCE_NAME_NULL = "1020006";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * AdminSystemTpCd is not correct
     *
     * @generated
     */
    public final static String INVALID_CTCLOBSOURCE_ADMINSYSTEMTPCD = "1020008";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: AdminSystemTpCd
     *
     * @generated
     */
    public final static String CTCLOBSOURCE_ADMINSYSTEMTPCD_NULL = "1020009";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * LobTpCd is not correct
     *
     * @generated
     */
    public final static String INVALID_CTCLOBSOURCE_LOBTPCD = "1020011";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: LobTpCd
     *
     * @generated
     */
    public final static String CTCLOBSOURCE_LOBTPCD_NULL = "1020012";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: AdminSysTpCd
     *
     * @generated
     */
    public final static String CTCADMINSYSPRIORITY_ADMINSYSTPCD_NULL = "1020015";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: name
     *
     * @generated
     */
    public final static String CTCADMINSYSPRIORITY_NAME_NULL = "1020017";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: PriorityTpCd
     *
     * @generated
     */
  //  public final static String CTCADMINSYSPRIORITY_PRIORITYTPCD_NULL = "1020149";

}