
-- @SqlSnippetPriority 600

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.




-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_ZOS.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



INSERT INTO CTCMDM.CDCTCLOBSOURCETP ( lang_tp_cd, Lob_Source_Tp_Cd, Name, Admin_Sys_Tp_Cd, Lob_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 1, 'name1', 1, 1 , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCLOBSOURCETP ( lang_tp_cd, Lob_Source_Tp_Cd, Name, Admin_Sys_Tp_Cd, Lob_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 2, 'name2', 1, 1 , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCLOBSOURCETP ( lang_tp_cd, Lob_Source_Tp_Cd, Name, Admin_Sys_Tp_Cd, Lob_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 3, 'name3', 1, 1 , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCLOBSOURCETP ( lang_tp_cd, Lob_Source_Tp_Cd, Name, Admin_Sys_Tp_Cd, Lob_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 4, 'name4', 1, 1 , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCADMINSYSPRIORITYTP ( lang_tp_cd, Admin_Sys_Tp_Cd, name, Priority_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 1, 'name1', NULL , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCADMINSYSPRIORITYTP ( lang_tp_cd, Admin_Sys_Tp_Cd, name, Priority_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 2, 'name2', NULL , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCADMINSYSPRIORITYTP ( lang_tp_cd, Admin_Sys_Tp_Cd, name, Priority_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 3, 'name3', NULL , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCADMINSYSPRIORITYTP ( lang_tp_cd, Admin_Sys_Tp_Cd, name, Priority_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 4, 'name4', NULL , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

