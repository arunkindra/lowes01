
-- @SqlSnippetPriority 100
-- @SqlModuleOrdering 2

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_DB2.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_DB2.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_DB2.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_DB2.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_DB2.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_DB2.sql

--#SET TERMINATOR ;

  
CREATE TABLE CTCMDM.CDCTCLOBSOURCETP (
	lang_tp_cd BIGINT  NOT NULL  , 
	Lob_Source_Tp_Cd BIGINT  NOT NULL  , 
	Name VARCHAR(120)  NOT NULL  , 
	Admin_Sys_Tp_Cd BIGINT  NOT NULL  , 
	Lob_Tp_Cd BIGINT  NOT NULL  , 
	description VARCHAR(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  NOT NULL   WITH DEFAULT CURRENT TIMESTAMP, 
	last_update_user VARCHAR(20)   
  );

ALTER TABLE CTCMDM.CDCTCLOBSOURCETP
  ADD PRIMARY KEY (
	lang_tp_cd, 
	Lob_Source_Tp_Cd
  );

  
CREATE TABLE CTCMDM.CDCTCADMINSYSPRIORITYTP (
	lang_tp_cd BIGINT  NOT NULL  , 
	Admin_Sys_Tp_Cd BIGINT  NOT NULL  , 
	name VARCHAR(120)  NOT NULL  , 
	Priority_Tp_Cd BIGINT   , 
	description VARCHAR(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  NOT NULL   WITH DEFAULT CURRENT TIMESTAMP, 
	last_update_user VARCHAR(20)   
  );

ALTER TABLE CTCMDM.CDCTCADMINSYSPRIORITYTP
  ADD PRIMARY KEY (
	lang_tp_cd, 
	Admin_Sys_Tp_Cd
  );

  
CREATE TABLE CTCMDM.H_CDCTCLOBSOURCETP (
	h_lang_tp_cd BIGINT  NOT NULL  ,
	h_Lob_Source_Tp_Cd BIGINT  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	lang_tp_cd BIGINT  NOT NULL  , 
	Lob_Source_Tp_Cd BIGINT  NOT NULL  , 
	Name VARCHAR(120)  NOT NULL  , 
	Admin_Sys_Tp_Cd BIGINT  NOT NULL  , 
	Lob_Tp_Cd BIGINT  NOT NULL  , 
	description VARCHAR(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  NOT NULL   WITH DEFAULT CURRENT TIMESTAMP, 
	last_update_user VARCHAR(20)   
  );

ALTER TABLE CTCMDM.H_CDCTCLOBSOURCETP

  ADD PRIMARY KEY (
	h_lang_tp_cd,
	h_Lob_Source_Tp_Cd,
   	h_create_dt    
 );

  
CREATE TABLE CTCMDM.H_CDCTCADMINSYSPRIORITYTP (
	h_lang_tp_cd BIGINT  NOT NULL  ,
	h_Admin_Sys_Tp_Cd BIGINT  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	lang_tp_cd BIGINT  NOT NULL  , 
	Admin_Sys_Tp_Cd BIGINT  NOT NULL  , 
	name VARCHAR(120)  NOT NULL  , 
	Priority_Tp_Cd BIGINT   , 
	description VARCHAR(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  NOT NULL   WITH DEFAULT CURRENT TIMESTAMP, 
	last_update_user VARCHAR(20)   
  );

ALTER TABLE CTCMDM.H_CDCTCADMINSYSPRIORITYTP

  ADD PRIMARY KEY (
	h_lang_tp_cd,
	h_Admin_Sys_Tp_Cd,
   	h_create_dt    
 );

