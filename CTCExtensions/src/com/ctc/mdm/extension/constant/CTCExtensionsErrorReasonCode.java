/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.ctc.mdm.extension.constant;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Defines the error message codes used in this module.
 *
 * @generated
 */
public class CTCExtensionsErrorReasonCode {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: CTCPersonNamepkId
     *
     * @generated
     */
    public final static String CTCPERSONNAME_CTCPERSONNAMEPKID_NULL = "1010004";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPersonName insert failed.
     *
     * @generated
     */
    public final static String INSERT_EXTENSION_CTCPERSONNAME_FAILED = "1010005";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPersonName delete failed.
     *
     * @generated
     */
    public final static String DELETE_EXTENSION_CTCPERSONNAME_FAILED = "1010006";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPersonName read failed.
     *
     * @generated
     */
    public final static String READ_EXTENSION_CTCPERSONNAME_FAILED = "1010007";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPersonName update failed.
     *
     * @generated
     */
    public final static String UPDATE_EXTENSION_CTCPERSONNAME_FAILED = "1010008";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: CTCPartyAddresspkId
     *
     * @generated
     */
    public final static String CTCPARTYADDRESS_CTCPARTYADDRESSPKID_NULL = "1010011";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyAddress insert failed.
     *
     * @generated
     */
    public final static String INSERT_EXTENSION_CTCPARTYADDRESS_FAILED = "1010012";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyAddress delete failed.
     *
     * @generated
     */
    public final static String DELETE_EXTENSION_CTCPARTYADDRESS_FAILED = "1010013";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyAddress read failed.
     *
     * @generated
     */
    public final static String READ_EXTENSION_CTCPARTYADDRESS_FAILED = "1010014";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyAddress update failed.
     *
     * @generated
     */
    public final static String UPDATE_EXTENSION_CTCPARTYADDRESS_FAILED = "1010015";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: CTCPartyContactMethodpkId
     *
     * @generated
     */
    public final static String CTCPARTYCONTACTMETHOD_CTCPARTYCONTACTMETHODPKID_NULL = "1010018";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyContactMethod insert failed.
     *
     * @generated
     */
    public final static String INSERT_EXTENSION_CTCPARTYCONTACTMETHOD_FAILED = "1010019";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyContactMethod delete failed.
     *
     * @generated
     */
    public final static String DELETE_EXTENSION_CTCPARTYCONTACTMETHOD_FAILED = "1010020";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyContactMethod read failed.
     *
     * @generated
     */
    public final static String READ_EXTENSION_CTCPARTYCONTACTMETHOD_FAILED = "1010021";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyContactMethod update failed.
     *
     * @generated
     */
    public final static String UPDATE_EXTENSION_CTCPARTYCONTACTMETHOD_FAILED = "1010022";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyAddress insert failed.
     *
     * @generated
     */
    public final static String INSERT_EXTENSION_CTCCONTRACTROLE_FAILED = "1010023";
    
  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyAddress delete failed.
     *
     * @generated
     */
    public final static String DELETE_EXTENSION_CTCCONTRACTROLE_FAILED = "1010024";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyAddress read failed.
     *
     * @generated
     */
    public final static String READ_EXTENSION_CTCCONTRACTROLE_FAILED = "1010025";

  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartyAddress update failed.
     *
     * @generated
     */
    public final static String UPDATE_EXTENSION_CTCCONTRACTROLE_FAILED = "1010026";

}

