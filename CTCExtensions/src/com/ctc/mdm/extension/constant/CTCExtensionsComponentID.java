/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.ctc.mdm.extension.constant;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Defines the component IDs used in this module.
 *
 * @generated
 */
public class CTCExtensionsComponentID {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCPersonNameBObjExt.
     *
     * @generated
     */
    public final static String CTCPERSON_NAME_BOBJ_EXT = "1010023";
  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCPartyAddressBObjExt.
     *
     * @generated
     */
    public final static String CTCPARTY_ADDRESS_BOBJ_EXT = "1010030";
  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCPartyContactMethodBObjExt.
     *
     * @generated
     */
    public final static String CTCPARTY_CONTACT_METHOD_BOBJ_EXT = "1010037";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCContractRoleBObjExt.
     *
     * @generated
     */
   
    public final static String CTCCONTRACT_ROLE_BOBJ_EXT = "1010044";
}

