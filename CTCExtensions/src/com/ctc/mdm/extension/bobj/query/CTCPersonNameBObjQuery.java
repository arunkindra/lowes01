package com.ctc.mdm.extension.bobj.query;

import com.dwl.base.DWLCommon;
import com.dwl.tcrm.coreParty.bobj.query.PersonNameBObjQuery;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonSearchBObj;

public class CTCPersonNameBObjQuery extends PersonNameBObjQuery {

	public CTCPersonNameBObjQuery(String persistenceStrategyName,
			DWLCommon objectToPersist) {
		super(persistenceStrategyName, objectToPersist);
	}

	@Override
	protected TCRMPersonSearchBObj convertPersonNameToPersonSearch(
			TCRMPersonNameBObj theTCRMPersonNameBObj) {
		TCRMPersonSearchBObj personSearchBObj = super.convertPersonNameToPersonSearch(theTCRMPersonNameBObj);
		theTCRMPersonNameBObj.setStdGivenNameOne(personSearchBObj.getGivenNameOne());
		theTCRMPersonNameBObj.setStdGivenNameTwo(personSearchBObj.getGivenNameTwo());
		theTCRMPersonNameBObj.setStdGivenNameThree(personSearchBObj.getGivenNameThree());
		theTCRMPersonNameBObj.setStdGivenNameFour(personSearchBObj.getGivenNameFour());
		theTCRMPersonNameBObj.setStdLastName(personSearchBObj.getLastName());
		return personSearchBObj;
	}
}
