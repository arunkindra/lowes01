package com.ctc.mdm.extension.bobj.query;

import com.dwl.base.DWLCommon;
import com.dwl.bobj.query.Persistence;
import com.dwl.tcrm.coreParty.bobj.query.PartyModuleBObjQueryFactoryImpl;

public class CTCPartyModuleBObjQueryFactoryImpl extends
		PartyModuleBObjQueryFactoryImpl {

    public CTCPartyModuleBObjQueryFactoryImpl() {
        super();
    }
    
    
    @Override
    public Persistence createPartyBObjPersistence(
    		String persistenceStrategyName, DWLCommon objectToPersist) {
    	return new CTCPartyBObjQuery(persistenceStrategyName,
    			objectToPersist);
    }
	
    @Override
    public Persistence createPersonNameBObjPersistence(
    		String persistenceStrategyName, DWLCommon objectToPersist) {
    	return new CTCPersonNameBObjQuery(persistenceStrategyName,
    			objectToPersist);
    }
}
