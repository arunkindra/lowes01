package com.ctc.mdm.extension.bobj.query;

import com.dwl.base.DWLControl;
import com.dwl.bobj.query.BObjQuery;
import com.dwl.tcrm.financial.bobj.query.FinancialServicesModuleBObjQueryFactoryImpl;

public class CTCFinancialServicesModuleBObjQueryFactoryImpl extends
		FinancialServicesModuleBObjQueryFactoryImpl {

	@Override
	public BObjQuery createContractComponentBObjQuery(String queryName,
			DWLControl control) {
		return new CTCContractComponentBObjQuery(queryName, control);
	}
}
