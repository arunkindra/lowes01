package com.ctc.mdm.extension.bobj.query;

import com.dwl.base.DWLCommon;
import com.dwl.tcrm.coreParty.bobj.query.PartyBObjQuery;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;

public class CTCPartyBObjQuery extends PartyBObjQuery {


	public CTCPartyBObjQuery(String persistenceStrategyName,
			DWLCommon objectToPersist) {
		super(persistenceStrategyName, objectToPersist);
	}

	@Override
	protected void addContact() throws Exception {
		// Set the DisplayName to null
		((TCRMPartyBObj)this.objectToPersist).setDisplayName(null);
		super.addContact();
	}
}
