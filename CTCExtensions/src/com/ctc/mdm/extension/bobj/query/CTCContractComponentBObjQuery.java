package com.ctc.mdm.extension.bobj.query;

import com.ctc.mdm.extension.constant.CTCExtensionsConstants;
import com.dwl.base.DWLControl;
import com.dwl.base.constant.DWLConstantDef;
import com.dwl.bobj.query.BObjQueryException;
import com.dwl.tcrm.financial.bobj.query.ContractComponentBObjQuery;
import com.dwl.tcrm.utilities.StringUtils;
import com.ibm.mdm.common.util.PropertyManager;

public class CTCContractComponentBObjQuery extends ContractComponentBObjQuery {

	private static final String CTC_CONTRACT_COMPONENTS_ALL_QUERY_SQL = "SELECT CONTRACTCOMPONENT.CONTR_COMPONENT_ID CONTRCOMPONENTID26, CONTRACTCOMPONENT.PROD_TP_CD PRODTPCD26, CONTRACTCOMPONENT.CONTRACT_ST_TP_CD CONTRACTSTTPCD26, CONTRACTCOMPONENT.CONTRACT_ID CONTRACTID26, CONTRACTCOMPONENT.CURR_CASH_VAL_AMT CURRCASHVALAMT26,CONTRACTCOMPONENT.CASHVAL_CUR_TP CURRCASHVALAMTCT26, CONTRACTCOMPONENT.PREMIUM_AMT PREMIUMAMT26, CONTRACTCOMPONENT.PREMAMT_CUR_TP PREMIUMAMTCT26,CONTRACTCOMPONENT.ISSUE_DT CONTCOMP_ISSUE_DT, CONTRACTCOMPONENT.VIATICAL_IND VIATICALIND26, CONTRACTCOMPONENT.BASE_IND CONTCOMP_BASE_IND, CONTRACTCOMPONENT.LAST_UPDATE_DT LASTUPDATEDT26, CONTRACTCOMPONENT.LAST_UPDATE_USER LASTUPDATEUSER26, CONTRACTCOMPONENT.CONTR_COMP_TP_CD CONTRCOMPTPCD26, CONTRACTCOMPONENT.LAST_UPDATE_TX_ID LASTUPDATETXID26, CONTRACTCOMPONENT.SERV_ARRANGE_TP_CD SERVARRANTPCD26, CONTRACTCOMPONENT.HOLDING_ID HOLDINGID, CONTRACTCOMPONENT.EXPIRY_DT EXPIRY_DT, CONTRACTCOMPONENT.CLUSTER_KEY CLUSTER_KEY  FROM CONTRACTCOMPONENT, NATIVEKEY WHERE CONTRACTCOMPONENT.CONTR_COMPONENT_ID = NATIVEKEY.CONTRACT_ID AND CONTRACTCOMPONENT.CONTRACT_ID = ? AND NATIVEKEY.ADMIN_CONTRACT_ID = ?";

	public CTCContractComponentBObjQuery(String queryName, DWLControl control) {
		super(queryName, control);
	}

	@Override
	protected String modifySQLStatement(String statement)
			throws BObjQueryException {
		String modifiedSQLStatement = null;
		try {
			if(queryName != null && queryName.equalsIgnoreCase(CONTRACT_COMPONENTS_ALL_QUERY)){
				String clientSystemName = this.control.getClientSystemName();
				if(this.control.containsKey(CTCExtensionsConstants.ADMIN_CONTRACT_ID_TYPE_CUSTOMER_ID)){
					String adminContractId = (String)this.control.get(CTCExtensionsConstants.ADMIN_CONTRACT_ID_TYPE_CUSTOMER_ID);
					if (StringUtils.compareIgnoreCaseWithTrim(clientSystemName,
							PropertyManager.getProperty(DWLConstantDef.TCRM_APP_NAME,CTCExtensionsConstants.TSYS_CLIENT_SYSTEM_NAME))
							&& StringUtils.isNonBlank(adminContractId))
					{
						modifiedSQLStatement = CTC_CONTRACT_COMPONENTS_ALL_QUERY_SQL;
						setParameter(this.positionalParams.size(), adminContractId);
						this.control.remove(CTCExtensionsConstants.ADMIN_CONTRACT_ID_TYPE_CUSTOMER_ID);
					}
				}
			}
		} catch (Exception e) {
			throw new BObjQueryException(e);
		}

		if(modifiedSQLStatement != null){
			return modifiedSQLStatement;
		}
		return super.modifySQLStatement(statement);
	}
}
