package com.ctc.mdm.extension.behavior;

import com.dwl.base.DWLControl;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.utilities.StringUtils;

public class CTCUpdateAddressPostBehaviorExt extends ClientJavaExtensionSet {

	@Override
	public void execute(ExtensionParameters parameters) {

		Object addressObject = parameters.getWorkingObjectHierarchy();
		DWLControl control = parameters.getControl();
		if(addressObject instanceof TCRMAddressBObj){
			TCRMAddressBObj addressBObj = (TCRMAddressBObj)addressObject;
			String addressIdPK = addressBObj.getAddressIdPK();
			if(control != null && StringUtils.isNonBlank(addressIdPK)){
				control.put(addressIdPK + "|" + "ADDRESS_ID", addressBObj);
			}
		}
	}

}
