package com.ctc.mdm.extension.behavior;

import com.dwl.base.DWLControl;
import com.dwl.base.extensionFramework.ClientJavaExtensionSet;
import com.dwl.base.extensionFramework.ExtensionParameters;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;

public class CTCUpdateAddressPreBehaviorExt extends ClientJavaExtensionSet {

	@Override
	public void execute(ExtensionParameters parameters) {
		Object addressObject = parameters.getWorkingObjectHierarchy();
		DWLControl control = parameters.getControl();
		try {
			if(addressObject instanceof TCRMAddressBObj){
				TCRMAddressBObj addressBObj = (TCRMAddressBObj)addressObject;
				String addressIdPK = addressBObj.getAddressIdPK();
				if(control != null && control.containsKey(addressIdPK + "|" + "ADDRESS_ID")){
					TCRMAddressBObj latestAddressBObj = (TCRMAddressBObj)control.get(addressIdPK + "|" + "ADDRESS_ID");
					addressBObj.setAddressLastUpdateDate(latestAddressBObj.getAddressLastUpdateDate());
				}
			}
		} catch (Exception e) {
			//ignore
		}

	}

}
