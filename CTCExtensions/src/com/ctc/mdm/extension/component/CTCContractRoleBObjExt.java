
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.ctc.mdm.extension.component;

import java.util.Iterator;
import java.util.Vector;

import com.ctc.mdm.addition.component.CTCContractRolePrivPrefBObj;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;
import com.ctc.mdm.addition.interfaces.CTCContractRolePrivPref;
import com.ctc.mdm.extension.constant.CTCExtensionsComponentID;
import com.ctc.mdm.extension.constant.CTCExtensionsErrorReasonCode;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.tcrm.common.IExtension;
import com.dwl.tcrm.common.ITCRMValidation;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.coreParty.constant.TCRMCoreInquiryLevel;
import com.dwl.tcrm.exception.TCRMInsertException;
import com.dwl.tcrm.exception.TCRMReadException;
import com.dwl.tcrm.exception.TCRMUpdateException;
import com.dwl.tcrm.financial.component.TCRMContractPartyRoleBObj;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This class provides the implementation of the business object
 * <code>CTCContractRoleBObjExt</code>.
 * 
 * @see com.dwl.tcrm.common.TCRMCommon
 * @generated NOT
 */
 
public class CTCContractRoleBObjExt extends TCRMContractPartyRoleBObj implements IExtension {

     protected Vector<CTCContractRolePrivPrefBObj> vecCTCContractRolePrivPrefBObj = null;

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      * @generated NOT
      */
     private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCContractRoleBObjExt.class);

        
     /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public CTCContractRoleBObjExt() {
        super();
        vecCTCContractRolePrivPrefBObj = new Vector<CTCContractRolePrivPrefBObj>();
        setComponentID(CTCExtensionsComponentID.CTCCONTRACT_ROLE_BOBJ_EXT);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the control object on this business object.
     *
     * @see com.dwl.base.DWLCommon#setControl(DWLControl)
     * @generated NOT
     */
    public void setControl(DWLControl newDWLControl) {
        super.setControl(newDWLControl);

        if (vecCTCContractRolePrivPrefBObj != null && vecCTCContractRolePrivPrefBObj.size() > 0) {
        	Iterator<CTCContractRolePrivPrefBObj> iterator = vecCTCContractRolePrivPrefBObj.iterator();
        	while (iterator.hasNext()) {
				CTCContractRolePrivPrefBObj contractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj) iterator
						.next();
				contractRolePrivPrefBObj.setControl(newDWLControl);
			}
        }
    }

    /**
     * 
     * @return
     */
	public Vector<CTCContractRolePrivPrefBObj> getItemsCTCContractRolePrivPrefBObj() {
		return this.vecCTCContractRolePrivPrefBObj;
	}
	
	/**
	 * 
	 * @param ctcContractRolePrivPrefBObj
	 */
	public void setCTCContractRolePrivPrefBObj(CTCContractRolePrivPrefBObj contractRolePrivPrefBObj){
		vecCTCContractRolePrivPrefBObj.addElement(contractRolePrivPrefBObj);
	}

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an add transaction.
     *
     * @generated NOT
     */
    public DWLStatus validateAdd(int level, DWLStatus status) throws Exception {

        status = super.validateAdd(level, status);
        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {

        	/*
        	 * Execute Controller Level Validation for CTCContractRolePrivPrefBObj
        	 */
        	Vector<CTCContractRolePrivPrefBObj> vector = getItemsCTCContractRolePrivPrefBObj();
        	if (vector != null && vector.size() > 0) {
				for (CTCContractRolePrivPrefBObj contractRolePrivPrefBObj : vector) {
					status = contractRolePrivPrefBObj.validateAdd(level, status);
				}
			}
        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
        }
        status = getValidationStatus(level, status);
        return status;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an update transaction.
     *
     * @generated NOT
     */
    public DWLStatus validateUpdate(int level, DWLStatus status) throws Exception {
        logger.finest("ENTER validateUpdate(int level, DWLStatus status)");


        status = super.validateUpdate(level, status);
        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {

        	/*
        	 * Execute Controller Level Validation for CTCContractRolePrivPrefBObj
        	 */
        	Vector<CTCContractRolePrivPrefBObj> vector = getItemsCTCContractRolePrivPrefBObj();
        	if (vector != null && vector.size() > 0) {
				for (CTCContractRolePrivPrefBObj contractRolePrivPrefBObj : vector) {
					if(StringUtils.isBlank(contractRolePrivPrefBObj.getContractRolePrivPrefIdPK())){
						status = contractRolePrivPrefBObj.validateAdd(level, status);
					}
					else
					{
						status = contractRolePrivPrefBObj.validateUpdate(level, status);
					}
				}
			}
        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
        }
        status = getValidationStatus(level, status);
        if (logger.isFinestEnabled()) {
            String returnValue = status.toString();
            logger.finest("RETURN validateUpdate(int level, DWLStatus status) " + returnValue);

        }
        return status;
    }



    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation common to both add and update transactions.
     *
     * @generated NOT
     */
     
    private DWLStatus getValidationStatus(int level, DWLStatus status) throws Exception {
        logger.finest("ENTER getValidationStatus(int level, DWLStatus status)");


        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO: Add any common controller-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions

        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
            // MDM_TODO: Add any common component-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions

        }
        
        if (logger.isFinestEnabled()) {
            String returnValue = status.toString();
            logger.finest("RETURN getValidationStatus(int level, DWLStatus status) " + returnValue);

        }
        
        return status;
    }
    


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Adds a record to the extension table.
     *
     * @throws DWLBaseException
     * @generated NOT
     */
    public void addRecord() throws DWLBaseException {
        logger.finest("ENTER addRecord()");


        try {
            if(vecCTCContractRolePrivPrefBObj != null && vecCTCContractRolePrivPrefBObj.size() > 0){
            	Vector<CTCContractRolePrivPrefBObj> vecAddedCTCContractRolePrivPrefBObj = new Vector<CTCContractRolePrivPrefBObj>();
                CTCContractRolePrivPref contractRolePrivPrefComponent = (CTCContractRolePrivPref) TCRMClassFactory
    	        	.getTCRMComponent(CTCAdditionsPropertyKeys.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT);
            	Iterator<CTCContractRolePrivPrefBObj> iterator = vecCTCContractRolePrivPrefBObj.iterator();
            	while (iterator.hasNext()) {
    				CTCContractRolePrivPrefBObj contractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj) iterator
    						.next();
    				contractRolePrivPrefBObj.setContractRoleId(this.getContractRoleIdPK());
    				DWLResponse response = contractRolePrivPrefComponent.addCTCContractRolePrivacyPreference(contractRolePrivPrefBObj);
    				CTCContractRolePrivPrefBObj addedContractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj)response.getData();
    				vecAddedCTCContractRolePrivPrefBObj.add(addedContractRolePrivPrefBObj);
    			}
        		this.vecCTCContractRolePrivPrefBObj.clear();
        		this.vecCTCContractRolePrivPrefBObj.addAll(vecAddedCTCContractRolePrivPrefBObj);
            }
        } catch (Exception e) {
            //DWLExceptionUtils.log(e);                
            if (logger.isFinestEnabled()) {
                String infoForLogging="Error: Insert record error " + e.getMessage(); 
            logger.finest("addRecord() " + infoForLogging);

            }
            status = new DWLStatus();
            
            TCRMInsertException insertEx = new TCRMInsertException();
            IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
            DWLError          error = errHandler.getErrorMessage(CTCExtensionsComponentID.CTCCONTRACT_ROLE_BOBJ_EXT,
                                                                 TCRMErrorCode.INSERT_RECORD_ERROR,
                                                                 CTCExtensionsErrorReasonCode.INSERT_EXTENSION_CTCCONTRACTROLE_FAILED,
                                                                 getControl(), new String[0]);
            error.setThrowable(e);
            status.addError(error);
            status.setStatus(DWLStatus.FATAL);
            insertEx.setStatus(status);
            throw insertEx;
        }
        logger.finest("RETURN addRecord()");

    }

	/*
	 * deleteRecord() commented for now as there is no component method to 
	 * delete the data source information.
	 */

//    /**
//     * <!-- begin-user-doc -->
//     * <!-- end-user-doc -->
//     *
//     * Deletes a record from the extension table.
//     *
//     * @see com.dwl.base.DWLCommon#deleteRecord()
//     * @generated 
//     */
//    public void deleteRecord() throws DWLBaseException {
//        logger.finest("ENTER deleteRecord()");
//
//        try {
//
//        } catch (Exception e) {
//            //DWLExceptionUtils.log(e);                
//            if (logger.isFinestEnabled()) {
//                String infoForLogging="Error: Error deleting record " + e.getMessage(); 
//            logger.finest("deleteRecord() " + infoForLogging);
//
//            }
//            status = new DWLStatus();
//
//            TCRMDeleteException deleteEx = new TCRMDeleteException();
//            IDWLErrorMessage errHandler = TCRMClassFactory.getErrorHandler();
//            DWLError error = errHandler.getErrorMessage(
//                    CTCExtensionsComponentID.CTCPARTY_ADDRESS_BOBJ_EXT,
//                    TCRMErrorCode.DELETE_RECORD_ERROR,
//                    CTCExtensionsErrorReasonCode.DELETE_EXTENSION_CTCPARTYADDRESS_FAILED,
//                    getControl(), new String[0]);
//            error.setThrowable(e);
//            status.addError(error);
//            status.setStatus(DWLStatus.FATAL);
//            deleteEx.setStatus(status);
//            throw deleteEx;
//        }
//        logger.finest("RETURN deleteRecord()");
//
//    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Updates a record in the extension table.
     *
     * @throws DWLBaseException
     * @generated NOT
     */
    public void updateRecord() throws DWLBaseException {
        logger.finest("ENTER updateRecord()");

        try {
    		if(vecCTCContractRolePrivPrefBObj != null && vecCTCContractRolePrivPrefBObj.size() > 0){
        		Vector<CTCContractRolePrivPrefBObj> vecProcessedContractRolePrivPref = new Vector<CTCContractRolePrivPrefBObj>();
        		CTCContractRolePrivPref contractRolePrivPrefComponent = (CTCContractRolePrivPref) TCRMClassFactory
	        		.getTCRMComponent(CTCAdditionsPropertyKeys.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT);
                Iterator<CTCContractRolePrivPrefBObj> iterator = vecCTCContractRolePrivPrefBObj.iterator();
	        	while (iterator.hasNext()) {
					CTCContractRolePrivPrefBObj contractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj) iterator
							.next();
					CTCContractRolePrivPrefBObj tempContractRolePrivPrefBObj = null;
					DWLResponse response = null;
					if(StringUtils.isNonBlank(contractRolePrivPrefBObj.getContractRolePrivPrefIdPK())){
						response = contractRolePrivPrefComponent.updateCTCContractRolePrivacyPreference(contractRolePrivPrefBObj);
					}
					else // prepare for add
					{
						contractRolePrivPrefBObj.setContractRoleId(this.getContractRoleIdPK());
						response = contractRolePrivPrefComponent.addCTCContractRolePrivacyPreference(contractRolePrivPrefBObj);
					}
					tempContractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj)response.getData();
					vecProcessedContractRolePrivPref.add(tempContractRolePrivPrefBObj);
				}
        		this.getItemsCTCContractRolePrivPrefBObj().clear();
        		this.getItemsCTCContractRolePrivPrefBObj().addAll(vecProcessedContractRolePrivPref);
    		}
        } catch (Exception e) {
            //DWLExceptionUtils.log(e);                
            status = new DWLStatus();
            if (logger.isFinestEnabled()) {
                String infoForLogging="Error: Error updating record " + e.getMessage(); 
                logger.finest("updateRecord() " + infoForLogging);

            }
            TCRMUpdateException updateEx = new TCRMUpdateException();
            IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
            DWLError          error = errHandler.getErrorMessage(CTCExtensionsComponentID.CTCCONTRACT_ROLE_BOBJ_EXT,
                                                             TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                             CTCExtensionsErrorReasonCode.UPDATE_EXTENSION_CTCCONTRACTROLE_FAILED,
                                                             getControl(), new String[0]);
            error.setThrowable(e);
            status.addError(error);
            status.setStatus(DWLStatus.FATAL);
            updateEx.setStatus(status);
            throw updateEx;
        }
        logger.finest("RETURN updateRecord()");

    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a record from the extension table.
     *
     * @throws DWLBaseException
     * @generated NOT
     */
    public void getRecord() throws DWLBaseException {
        logger.finest("ENTER getRecord()");

        try {
            CTCContractRolePrivPref contractRolePrivPrefComponent = (CTCContractRolePrivPref) TCRMClassFactory
        		.getTCRMComponent(CTCAdditionsPropertyKeys.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT);
    
			DWLResponse response = contractRolePrivPrefComponent.getAllCTCContractRolePrivacyPreferences(this.getContractRoleIdPK(), TCRMCoreInquiryLevel.INQUIRY_LEVEL_1, TCRMRecordFilter.ACTIVE, this.getControl());
			Vector<CTCContractRolePrivPrefBObj> vecExistingContractRolePrivPrefs = (Vector<CTCContractRolePrivPrefBObj>)response.getData();
			if(vecExistingContractRolePrivPrefs != null && vecExistingContractRolePrivPrefs.size() > 0){
			
				this.getItemsCTCContractRolePrivPrefBObj().clear();
				this.getItemsCTCContractRolePrivPrefBObj().addAll(vecExistingContractRolePrivPrefs);
			}
        } catch (Exception e) {
            //DWLExceptionUtils.log(e);                
            if (logger.isFinestEnabled()) {
                String infoForLogging="Error: Error reading record " + e.getMessage(); 
            logger.finest("getRecord() " + infoForLogging);

            }
            status = new DWLStatus();

            TCRMReadException readEx = new TCRMReadException();
            IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
            DWLError          error = errHandler.getErrorMessage(CTCExtensionsComponentID.CTCCONTRACT_ROLE_BOBJ_EXT,
                                                                 TCRMErrorCode.READ_RECORD_ERROR,
                                                                 CTCExtensionsErrorReasonCode.READ_EXTENSION_CTCCONTRACTROLE_FAILED,
                                                                 getControl(), new String[0]);
            error.setThrowable(e);
            status.addError(error);
            status.setStatus(DWLStatus.FATAL);
            readEx.setStatus(status);
            throw readEx;
        }       
        logger.finest("RETURN getRecord()");

    }

}

