/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.ctc.mdm.extension.component;

import java.util.Iterator;
import java.util.Vector;

import com.ctc.mdm.addition.component.CTCDataSourceBObj;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;
import com.ctc.mdm.addition.interfaces.CTCDataSource;
import com.ctc.mdm.extension.constant.CTCExtensionsComponentID;
import com.ctc.mdm.extension.constant.CTCExtensionsErrorReasonCode;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.tcrm.common.IExtension;
import com.dwl.tcrm.common.ITCRMValidation;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.exception.TCRMDeleteException;
import com.dwl.tcrm.exception.TCRMInsertException;
import com.dwl.tcrm.exception.TCRMReadException;
import com.dwl.tcrm.exception.TCRMUpdateException;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This class provides the implementation of the business object
 * <code>CTCPersonNameBObjExt</code>.
 * 
 * @see com.dwl.tcrm.common.TCRMCommon
 * @generated NOT
 */
 
public class CTCPersonNameBObjExt extends TCRMPersonNameBObj implements IExtension {

    protected Vector<CTCDataSourceBObj> vecCTCDataSourceBObj = null;

    /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      * @generated 
      */
     private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCPersonNameBObjExt.class);

        
     /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public CTCPersonNameBObjExt() {
        super();
        vecCTCDataSourceBObj = new Vector<CTCDataSourceBObj>();
        setComponentID(CTCExtensionsComponentID.CTCPERSON_NAME_BOBJ_EXT);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the control object on this business object.
     *
     * @see com.dwl.base.DWLCommon#setControl(DWLControl)
     * @generated NOT
     */
    public void setControl(DWLControl newDWLControl) {
        super.setControl(newDWLControl);

        if (vecCTCDataSourceBObj != null && vecCTCDataSourceBObj.size() > 0) {
        	Iterator<CTCDataSourceBObj> iterator = vecCTCDataSourceBObj.iterator();
        	while (iterator.hasNext()) {
				CTCDataSourceBObj dataSourceBObj = (CTCDataSourceBObj) iterator
						.next();
				dataSourceBObj.setControl(newDWLControl);
			}
        }
    }

    /**
     * 
     * @return
     */
	public Vector<CTCDataSourceBObj> getItemsCTCDataSourceBObj() {
		return this.vecCTCDataSourceBObj;
	}
	
	/**
	 * 
	 * @param ctcDataSourceBObj
	 */
	public void setCTCDataSourceBObj(CTCDataSourceBObj ctcDataSourceBObj){
		vecCTCDataSourceBObj.addElement(ctcDataSourceBObj);
	}

	@Override
	public void setStandardFormattingIndicator(
			String newStandardFormattingIndicator) {
		super.setStandardFormattingIndicator(newStandardFormattingIndicator);
		if(StringUtils.isNonBlank(newStandardFormattingIndicator)){
			if(StringUtils.compareIgnoreCaseWithTrim(newStandardFormattingIndicator, "Y")){
				this.setNameStandardizedFlag(true);
			}
		}
	}
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an add transaction.
     *
     * @generated
     */
    public DWLStatus validateAdd(int level, DWLStatus status) throws Exception {

        status = super.validateAdd(level, status);
        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO: Add any controller-level custom validation logic to be
            // executed for this object during an "add" transaction

        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
            // MDM_TODO: Add any component-level custom validation logic to be
            // executed for this object during an "add" transaction
        	
        }
        status = getValidationStatus(level, status);
        return status;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an update transaction.
     *
     * @generated
     */
    public DWLStatus validateUpdate(int level, DWLStatus status) throws Exception {
        logger.finest("ENTER validateUpdate(int level, DWLStatus status)");


        status = super.validateUpdate(level, status);
        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO: Add any controller-level custom validation logic to be
            // executed for this object during an "update" transaction

        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
            
            // MDM_TODO: Add any component-level custom validation logic to be
            // executed for this object during an "update" transaction
        }
        status = getValidationStatus(level, status);
        if (logger.isFinestEnabled()) {
            String returnValue = status.toString();
            logger.finest("RETURN validateUpdate(int level, DWLStatus status) " + returnValue);

        }
        return status;
    }



    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation common to both add and update transactions.
     *
     * @generated
     */
     
    private DWLStatus getValidationStatus(int level, DWLStatus status) throws Exception {
        logger.finest("ENTER getValidationStatus(int level, DWLStatus status)");


        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO: Add any common controller-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions

        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
            // MDM_TODO: Add any common component-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions

        }
        
        if (logger.isFinestEnabled()) {
            String returnValue = status.toString();
            logger.finest("RETURN getValidationStatus(int level, DWLStatus status) " + returnValue);

        }
        
        return status;
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Adds a record to the extension table.
     *
     * @throws DWLBaseException
     * @generated
     */
    public void addRecord() throws DWLBaseException {
        logger.finest("ENTER addRecord()");

        try {
            if(vecCTCDataSourceBObj != null && vecCTCDataSourceBObj.size() > 0){
            	Vector<CTCDataSourceBObj> vecAddedCTCDataSourceBObj = new Vector<CTCDataSourceBObj>();
                CTCDataSource dataSourceComponent = (CTCDataSource) TCRMClassFactory
    	        	.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
            	Iterator<CTCDataSourceBObj> iterator = vecCTCDataSourceBObj.iterator();
            	while (iterator.hasNext()) {
    				CTCDataSourceBObj dataSourceBObj = (CTCDataSourceBObj) iterator
    						.next();
    				dataSourceBObj.setInstancePK(this.getPersonNameIdPK());
    				dataSourceBObj.setEntityName("PERSONNAME");
    				DWLResponse response = dataSourceComponent.addCTCDataSource(dataSourceBObj);
    				CTCDataSourceBObj addedDataSourceBObj = (CTCDataSourceBObj)response.getData();
    				vecAddedCTCDataSourceBObj.add(addedDataSourceBObj);
    			}
        		this.vecCTCDataSourceBObj.clear();
        		this.vecCTCDataSourceBObj.addAll(vecAddedCTCDataSourceBObj);
            }
			
		} catch (Exception e) {
            if (logger.isFinestEnabled()) {
                String infoForLogging="Error: Insert record error " + e.getMessage(); 
            logger.finest("addRecord() " + infoForLogging);

            }
            status = new DWLStatus();
            
            TCRMInsertException insertEx = new TCRMInsertException();
            IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
            DWLError          error = errHandler.getErrorMessage(CTCExtensionsComponentID.CTCPERSON_NAME_BOBJ_EXT,
                                                                 TCRMErrorCode.INSERT_RECORD_ERROR,
                                                                 CTCExtensionsErrorReasonCode.INSERT_EXTENSION_CTCPERSONNAME_FAILED,
                                                                 getControl(), new String[0]);
            error.setThrowable(e);
            status.addError(error);
            status.setStatus(DWLStatus.FATAL);
            insertEx.setStatus(status);
            throw insertEx;

		}
		
        logger.finest("RETURN addRecord()");

    }

    	/*
    	 * deleteRecord() commented for now as there is no component method to 
    	 * delete the data source information.
    	 */
//    /**
//     * <!-- begin-user-doc -->
//     * <!-- end-user-doc -->
//     *
//     * Deletes a record from the extension table.
//     *
//     * @see com.dwl.base.DWLCommon#deleteRecord()
//     * @generated
//     */
//    public void deleteRecord() throws DWLBaseException {
//        logger.finest("ENTER deleteRecord()");
//
//        try {
//        } catch (Exception e) {
//            //DWLExceptionUtils.log(e);                
//            if (logger.isFinestEnabled()) {
//                String infoForLogging="Error: Error deleting record " + e.getMessage(); 
//            logger.finest("deleteRecord() " + infoForLogging);
//
//            }
//            status = new DWLStatus();
//
//            TCRMDeleteException deleteEx = new TCRMDeleteException();
//            IDWLErrorMessage errHandler = TCRMClassFactory.getErrorHandler();
//            DWLError error = errHandler.getErrorMessage(
//                    CTCExtensionsComponentID.CTCPERSON_NAME_BOBJ_EXT,
//                    TCRMErrorCode.DELETE_RECORD_ERROR,
//                    CTCExtensionsErrorReasonCode.DELETE_EXTENSION_CTCPERSONNAME_FAILED,
//                    getControl(), new String[0]);
//            error.setThrowable(e);
//            status.addError(error);
//            status.setStatus(DWLStatus.FATAL);
//            deleteEx.setStatus(status);
//            throw deleteEx;
//        }
//        logger.finest("RETURN deleteRecord()");
//
//    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Updates a record in the extension table.
     *
     * @throws DWLBaseException
     * @generated
     */
    public void updateRecord() throws DWLBaseException {
        logger.finest("ENTER updateRecord()");

        try {
        	if(vecCTCDataSourceBObj != null && vecCTCDataSourceBObj.size() > 0){
        		Vector<CTCDataSourceBObj> vecProcessedDataSource = new Vector<CTCDataSourceBObj>();
                CTCDataSource dataSourceComponent = (CTCDataSource) TCRMClassFactory
	        		.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
                Iterator<CTCDataSourceBObj> iterator = vecCTCDataSourceBObj.iterator();
	        	while (iterator.hasNext()) {
					CTCDataSourceBObj dataSourceBObj = (CTCDataSourceBObj) iterator
							.next();
					CTCDataSourceBObj tempDataSourceBObj = null;
					DWLResponse response = null;
					if(StringUtils.isNonBlank(dataSourceBObj.getDataSourceID())){
						response = dataSourceComponent.updateCTCDataSource(dataSourceBObj);
					}
					else // prepare for add
					{
						dataSourceBObj.setInstancePK(this.getPersonNameIdPK());
						dataSourceBObj.setEntityName("PERSONNAME");
						response = dataSourceComponent.addCTCDataSource(dataSourceBObj);
					}
					tempDataSourceBObj = (CTCDataSourceBObj)response.getData();
					vecProcessedDataSource.add(tempDataSourceBObj);
				}
        		this.vecCTCDataSourceBObj.clear();
        		this.getItemsCTCDataSourceBObj().addAll(vecProcessedDataSource);
        	}
        } catch (Exception e) {
            //DWLExceptionUtils.log(e);                
            status = new DWLStatus();
            if (logger.isFinestEnabled()) {
                String infoForLogging="Error: Error updating record " + e.getMessage(); 
        logger.finest("updateRecord() " + infoForLogging);

            }
            TCRMUpdateException updateEx = new TCRMUpdateException();
            IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
            DWLError          error = errHandler.getErrorMessage(CTCExtensionsComponentID.CTCPERSON_NAME_BOBJ_EXT,
                                                             TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                             CTCExtensionsErrorReasonCode.UPDATE_EXTENSION_CTCPERSONNAME_FAILED,
                                                             getControl(), new String[0]);
            error.setThrowable(e);
            status.addError(error);
            status.setStatus(DWLStatus.FATAL);
            updateEx.setStatus(status);
            throw updateEx;
        }
        logger.finest("RETURN updateRecord()");

    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a record from the extension table.
     *
     * @throws DWLBaseException
     * @generated
     */
    public void getRecord() throws DWLBaseException {
        logger.finest("ENTER getRecord()");

        try {
            CTCDataSource dataSourceComponent = (CTCDataSource) TCRMClassFactory
            	.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
			DWLResponse response = dataSourceComponent.getAllCTCDataSourcesByEntity("PERSONNAME", this.getPersonNameIdPK(), TCRMRecordFilter.ACTIVE, this.getControl());
			Vector<CTCDataSourceBObj> vecExistingDataSources = (Vector<CTCDataSourceBObj>)response.getData();
			if(vecExistingDataSources != null && vecExistingDataSources.size() > 0){
				this.getItemsCTCDataSourceBObj().clear();
				this.getItemsCTCDataSourceBObj().addAll(vecExistingDataSources);
			}
        } catch (Exception e) {
            //DWLExceptionUtils.log(e);                
            if (logger.isFinestEnabled()) {
                String infoForLogging="Error: Error reading record " + e.getMessage(); 
            logger.finest("getRecord() " + infoForLogging);

            }
            status = new DWLStatus();

            TCRMReadException readEx = new TCRMReadException();
            IDWLErrorMessage  errHandler = DWLClassFactory.getErrorHandler();
            DWLError          error = errHandler.getErrorMessage(CTCExtensionsComponentID.CTCPERSON_NAME_BOBJ_EXT,
                                                                 TCRMErrorCode.READ_RECORD_ERROR,
                                                                 CTCExtensionsErrorReasonCode.READ_EXTENSION_CTCPERSONNAME_FAILED,
                                                                 getControl(), new String[0]);
            error.setThrowable(e);
            status.addError(error);
            status.setStatus(DWLStatus.FATAL);
            readEx.setStatus(status);
            throw readEx;
        }       
        logger.finest("RETURN getRecord()");

    }
}

