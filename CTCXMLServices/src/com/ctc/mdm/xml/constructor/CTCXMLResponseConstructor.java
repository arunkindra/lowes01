package com.ctc.mdm.xml.constructor;

import java.util.HashMap;

import com.dwl.base.DWLResponse;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.base.requestHandler.exception.ResponseConstructorException;
import com.dwl.tcrm.coreParty.xmlHandler.XMLResponseConstructor;

public class CTCXMLResponseConstructor extends XMLResponseConstructor {

	@Override
	public Object constructResponse(HashMap theContext, DWLResponse responseObject, DWLTransaction transaction, Object theRequest)
    	throws ResponseConstructorException
    {
		if(!(transaction instanceof DWLTransactionInquiry)){
			if(responseObject != null){
				responseObject.setData(null);	
			}
		}
		return super.constructResponse(theContext, responseObject, transaction, theRequest);
	}
}
