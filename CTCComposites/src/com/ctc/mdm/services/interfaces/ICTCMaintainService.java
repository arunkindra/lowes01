package com.ctc.mdm.services.interfaces;

import com.ctc.mdm.services.exception.CTCCompositeException;
import com.dwl.base.DWLCommon;
import com.dwl.unifi.tx.exception.BusinessProxyException;

public interface ICTCMaintainService {

	/**
	 * This method resolves identity of request business object.
	 * @param requestBObj, the business object from request
	 * @param foundBObj, the business object that found from database
	 * @param isFineGrainTxn, boolean to identify transaction from local business proxy or from other business proxy
	 * @throws BusinessProxyException
	 */
	public void resolveIdentity(DWLCommon requestBObj, DWLCommon foundBObj, boolean isFineGrainTxn) throws BusinessProxyException, CTCCompositeException;
	
}
