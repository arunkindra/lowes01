/* _______________________________________________________ {COPYRIGHT-TOP} _____
* Licensed Materials - Property of IBM
*
* 5724-S78
*
* (C) Copyright IBM Corp. 2008, 2009  All Rights Reserved.
*
* US Government Users Restricted Rights - Use, duplication, or
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
* ________________________________________________________ {COPYRIGHT-END} _____*/
package com.ctc.mdm.services.constants;

public class CTCMaintainServiceConstants {
	public static final String UPDATE_CONTRACT = "updateContract";
	public static final String ADD_CONTRACT = "addContract";
	public static final String CTCMAINTAINCONTRACT = "ctcMaintainContract";
	public static final String BUSINESSPROXY_CTCMAINTAINPARTY = "BusinessProxy.tcrm.ctcMaintainParty";
	public static final String ADD_CONTRACT_COMPONENT = "addContractComponent";
	public static final String UPDATE_CONTRACT_COMPONENT = "updateContractComponent";
}
