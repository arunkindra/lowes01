/* _______________________________________________________ {COPYRIGHT-TOP} _____
 * Licensed Materials - Property of IBM
 *
 * 5724-S78
 *
 * (C) Copyright IBM Corp. 2008, 2009  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication, or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 * ________________________________________________________ {COPYRIGHT-END} _____*/
package com.ctc.mdm.services.proxy;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.ctc.mdm.addition.component.CTCContractRolePrivPrefBObj;
import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.constants.CTCErrorConstants;
import com.ctc.mdm.common.constants.CTCSQLConstants;
import com.ctc.mdm.common.util.CTCCommonUtil;
import com.ctc.mdm.extension.component.CTCContractRoleBObjExt;
import com.ctc.mdm.search.component.CTCContractSearchResultProcessor;
import com.ctc.mdm.services.constants.CTCMaintainServiceConstants;
import com.ctc.mdm.services.exception.CTCCompositeException;
import com.ctc.mdm.services.util.CTCCompositeUtil;
import com.dwl.base.DWLCommon;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.db.DataManager;
import com.dwl.base.db.QueryConnection;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLPropertyNotFoundException;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.base.requestHandler.DWLTransactionPersistent;
import com.dwl.base.util.DWLCommonProperties;
import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMInactivatedPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyLinkBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCoreInquiryLevel;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.financial.component.TCRMAdminNativeKeyBObj;
import com.dwl.tcrm.financial.component.TCRMContractBObj;
import com.dwl.tcrm.financial.component.TCRMContractComponentBObj;
import com.dwl.tcrm.financial.component.TCRMContractPartyRoleBObj;
import com.dwl.tcrm.financial.component.TCRMContractRoleLocationBObj;
import com.dwl.tcrm.financial.component.TCRMContractRoleLocationPurposeBObj;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.tcrm.utilities.TCRMObjectCloner;
import com.dwl.unifi.tx.exception.BusinessProxyException;


public class CTCMaintainContractBP extends CTCMaintainBaseBP {

	public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2008, 2009\nUS Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";

	private final static IDWLLogger logger = DWLLoggerManager
			.getLogger(CTCMaintainContractBP.class);

	private  static String strCoStar = null;
	
	private  static String strTSYS = null;
	
	private static String strEpsilon = null;
	
	Hashtable<String, Object> htPersonBObj = new Hashtable<String, Object>();

	List<String> lstReqLocationGroupIds = new ArrayList<String>();
	
	static {
		try {
			strCoStar = CTCCommonUtil
				.getProperty(CTCConstants.COSTAR_CLIENT_SYSTEM_NAME);
			strTSYS = CTCCommonUtil
				.getProperty(CTCConstants.TSYS_CLIENT_SYSTEM_NAME);
			strEpsilon = CTCCommonUtil.getProperty(CTCConstants.EPSILON_CLIENT_SYSTEM_NAME);

		} catch (Exception e) {
			strCoStar = CTCConstants.COSTAR;
			strTSYS = CTCConstants.TSYS;
			strEpsilon = CTCConstants.EPSILON;
		}

	}
	
	/**
     * The request object has following contract structure
     * 
     * TCRMContractBObj 
     * 			TCRMContractComponentBObj	 							
     * 					TCRMContractPartyRoleBObj	 
     *      		 			TCRMExtension 
     * 								CTCContractRoleBObjExt
     * 									CTCContractRolePrivPrefBObj 
     * 							TCRMPartyBObj (TCRMPersonBObj/TCRMOrganizationBObj) 
     * 									TCRMPartyAddressBObj/TCRMPartyContactMethodBObj 
     * 									TCRMAdminContEquivBObj	 
     * 							TCRMContractRoleLocationBObj 
     * 									TCRMContractRoleLocationPurposeBObj
     * 			TCRMAdminNativeKeyBObj 
	 * @throws CTCCompositeException 
     * 
     * 
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ctc.mdm.services.proxy.CTCCompositeBaseBP#executeImpl(com.dwl.base
	 * .requestHandler.DWLTransaction)
	 */
	public Object executeImpl(DWLTransaction dwlTransaction)
			throws BusinessProxyException, CTCCompositeException {
		logger.info("Entering CTCMaintainContractBP.executeImpl()");
		long beginTime = System.currentTimeMillis();

		DWLTransactionPersistent theDWLTxnObj = (DWLTransactionPersistent) dwlTransaction;
		
		TCRMContractBObj foundContract = null;
		Object returnObject = null;

		try {

			// Validate input Request
			validateInput(
					theDWLTxnObj.getTxnTopLevelObject(),
					TCRMContractBObj.class,
					CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
					CTCErrorConstants.CTCMAINTAINCONTRACT_NOT_OF_TYPE_TCRMCONTRACTBOBJ_ERR_CODE,
					theDWLTxnObj.getTxnControl());

			TCRMContractBObj inputContract = (TCRMContractBObj) theDWLTxnObj
					.getTxnTopLevelObject();
			// 2.search existing contract
			foundContract = searchContract(inputContract);

			// 3.identify party child business objects and modify transaction
			// object
			resolveIdentity(inputContract, foundContract, true);

			// 4.fire transaction to backend

			returnObject = fireTransaction(theDWLTxnObj);

			// postExecute(returnObject);

		}catch(Exception exception){
				CTCCompositeUtil.handleException(exception);
			}
		finally {
			if (logger.isInfoEnabled()) {
				long endTime = System.currentTimeMillis();
				logger
						.info("MaintainContractBP : execute : total time in milliseconds "
								+ (endTime - beginTime));
			}
		}
		logger.info("Exiting CTCMaintainContractBP.executeImpl()");
		return returnObject;
	}

	/**
	 * This method Validate the request for necessary fields
	 * 
	 * @param businessObject
	 * @param componentId
	 * @param status
	 * @throws CTCCompositeException
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ctc.mdm.services.proxy.CTCMaintainBaseBP#validateFields(com.dwl.base
	 * .DWLCommon, java.lang.String, com.dwl.base.error.DWLStatus)
	 */
	@SuppressWarnings("unchecked")
	protected void validateFields(DWLCommon businessObject, String componentId,
			DWLStatus status) throws CTCCompositeException {

		TCRMContractBObj inputContract = (TCRMContractBObj) businessObject;
		Vector<TCRMAdminNativeKeyBObj> vecAdminNativeKeysBObj = (Vector<TCRMAdminNativeKeyBObj>) inputContract
				.getItemsTCRMAdminNativeKeyBObj();
		boolean fieldNameTypeFound = false;
		Vector<DWLError> vecDWLError = new Vector<DWLError>();
		String clientSystemName = inputContract.getControl()
				.getClientSystemName();
		String adminFieldNameType = null;
		Vector<TCRMContractComponentBObj> vecTCRMContractComponentBObj = inputContract.getItemsTCRMContractComponentBObj();
		
			if (StringUtils.isNonBlank(clientSystemName)) {
				if (StringUtils.compareIgnoreCaseWithTrim(clientSystemName,
						strCoStar)) {
					for (TCRMAdminNativeKeyBObj tcrmAdminNativeKeyBObj : vecAdminNativeKeysBObj) {
						if (StringUtils.isBlank(tcrmAdminNativeKeyBObj
								.getAdminFieldNameType())) {
							if (StringUtils.isNonBlank(tcrmAdminNativeKeyBObj
									.getAdminFieldNameValue())) {
								
								try {
									adminFieldNameType = CTCCommonUtil
											.getCodeType(
													tcrmAdminNativeKeyBObj
															.getAdminFieldNameValue(),
													CTCConstants.ADMIN_FIELD_NAME_TYPE_CODE_TABLE,
													inputContract.getControl());
								} catch (Exception e) {
									DWLError dwlError = CTCCompositeUtil
									.getDWLErrorObject(
											CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
											DWLErrorCode.READ_RECORD_ERROR,
											CTCErrorConstants.ERROR_IN_GETTING_CODE_TYPE,
											inputContract.getControl());

							vecDWLError.add(dwlError);
							
							status.setStatus(DWLStatus.FATAL);
							status.setDwlErrorGroup(vecDWLError);
							throw new CTCCompositeException(CTCCompositeUtil
									.createResponseByStatus(status));
									
								}
								

								if (StringUtils.isBlank(adminFieldNameType)) {

									DWLError dwlError = CTCCompositeUtil
											.getDWLErrorObject(
													CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
													DWLErrorCode.DATA_INVALID_ERROR,
													CTCErrorConstants.ADMINFEILDNAMETYPE_OR_ADMINFIELDNAMEVALUE_IS_NOT_CORRECT,
													inputContract.getControl());

									vecDWLError.add(dwlError);

								} else {
									tcrmAdminNativeKeyBObj
											.setAdminFieldNameType(adminFieldNameType);

								}

							} else {

								DWLError dwlError = CTCCompositeUtil
										.getDWLErrorObject(
												CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
												DWLErrorCode.FIELD_VALIDATION_ERROR,
												CTCErrorConstants.ADMINFIELDNAMETYPE_OR_ADMINFIELDNAMEVALUE_MISSING_ERR_CODE,
												inputContract.getControl());

								vecDWLError.add(dwlError);
							}

						}

						if (StringUtils.isNonBlank(tcrmAdminNativeKeyBObj
								.getAdminFieldNameType())) {
							if (StringUtils.compareIgnoreCaseWithTrim(
									tcrmAdminNativeKeyBObj
											.getAdminFieldNameType(),
									CTCConstants.ADMIN_FIELD_NAME_TYPE_COSTAR)) {
								fieldNameTypeFound = true;

								if (StringUtils.isBlank(tcrmAdminNativeKeyBObj
										.getAdminContractId())) {

									DWLError dwlError = CTCCompositeUtil
											.getDWLErrorObject(
													CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
													DWLErrorCode.FIELD_VALIDATION_ERROR,
													CTCErrorConstants.ADMINCONTRACTID_MISSING_ERR_CODE,
													inputContract.getControl());

									vecDWLError.add(dwlError);
								}
								break;
							}
						}
					}
					if (!fieldNameTypeFound) {
						DWLError dwlError = CTCCompositeUtil
								.getDWLErrorObject(
										CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
										DWLErrorCode.DATA_INVALID_ERROR,
										CTCErrorConstants.NO_ADMINNATIVEKEY_FOR_COSTAR_ERR_CODE,
										inputContract.getControl());

						vecDWLError.add(dwlError);
					}
				} else {
					TCRMAdminNativeKeyBObj tcrmAdminNativeKeyBObj = (TCRMAdminNativeKeyBObj) vecAdminNativeKeysBObj
							.get(0);
					if (StringUtils.isBlank(tcrmAdminNativeKeyBObj
							.getAdminFieldNameType())) {
						if (StringUtils.isNonBlank(tcrmAdminNativeKeyBObj
								.getAdminFieldNameValue())) {
							 try {
								adminFieldNameType = CTCCommonUtil
										.getCodeType(
												tcrmAdminNativeKeyBObj
														.getAdminFieldNameValue(),
												CTCConstants.ADMIN_FIELD_NAME_TYPE_CODE_TABLE,
												inputContract.getControl());
							} catch (Exception e) {
								DWLError dwlError = CTCCompositeUtil
								.getDWLErrorObject(
										CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
										DWLErrorCode.READ_RECORD_ERROR,
										CTCErrorConstants.ERROR_IN_GETTING_CODE_TYPE,
										inputContract.getControl());
									vecDWLError.add(dwlError);
									status.setStatus(DWLStatus.FATAL);
									status.setDwlErrorGroup(vecDWLError);
									throw new CTCCompositeException(CTCCompositeUtil
											.createResponseByStatus(status));
											}

							if (StringUtils.isBlank(adminFieldNameType)) {
								DWLError dwlError = CTCCompositeUtil
										.getDWLErrorObject(
												CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
												DWLErrorCode.DATA_INVALID_ERROR,
												CTCErrorConstants.ADMINFEILDNAMETYPE_OR_ADMINFIELDNAMEVALUE_IS_NOT_CORRECT,
												inputContract.getControl());

								vecDWLError.add(dwlError);

							} else {
								tcrmAdminNativeKeyBObj
										.setAdminFieldNameType(adminFieldNameType);
							}
						} else {

							DWLError dwlError = CTCCompositeUtil
									.getDWLErrorObject(
											CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
											DWLErrorCode.FIELD_VALIDATION_ERROR,
											CTCErrorConstants.ADMINFIELDNAMETYPE_OR_ADMINFIELDNAMEVALUE_MISSING_ERR_CODE,
											inputContract.getControl());

							vecDWLError.add(dwlError);
						}
					}

					if (StringUtils.isBlank(tcrmAdminNativeKeyBObj
							.getAdminContractId())) {
						DWLError dwlError = CTCCompositeUtil
								.getDWLErrorObject(
										CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
										DWLErrorCode.FIELD_VALIDATION_ERROR,
										CTCErrorConstants.ADMINCONTRACTID_MISSING_ERR_CODE,
										inputContract.getControl());

						vecDWLError.add(dwlError);

					}

				}
			} else {
				DWLError dwlError = CTCCompositeUtil.getDWLErrorObject(
						CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
						DWLErrorCode.FIELD_VALIDATION_ERROR,
						CTCErrorConstants.CLLIENTSYSTEMNAME_NOT_FOUND_ERR_CODE,
						inputContract.getControl());

				vecDWLError.add(dwlError);
			}

			if(StringUtils.compareIgnoreCaseWithTrim(strEpsilon, clientSystemName)){
				int size = 0;
				int activeCards = 0;
				for (TCRMContractComponentBObj contractComponentBObj : vecTCRMContractComponentBObj) {

					Vector<TCRMContractPartyRoleBObj> vecRoles = contractComponentBObj.getItemsTCRMContractPartyRoleBObj();

					String contractStatusType = contractComponentBObj.getContractStatusType();
					
					if(StringUtils.isBlank(contractStatusType)){
						
						// ContractStatusType must be provided in the input ContractComponent.
						DWLError dwlError = CTCCompositeUtil.getDWLErrorObject(
								CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
								DWLErrorCode.FIELD_VALIDATION_ERROR,
								CTCErrorConstants.CONTRACT_STATUS_TYPE_MISSING,
								inputContract.getControl());

						vecDWLError.add(dwlError);

					}
					
					if(StringUtils.isNonBlank(contractStatusType) 
							&& StringUtils.compareIgnoreCaseWithTrim(contractStatusType, CTCConstants.CONTRACT_STATUS_TYPE_ACTIVE)){
						activeCards = activeCards + 1;
						
						if(CTCCommonUtil.isEmpty(vecRoles)){
							
							// ContractPartyRole must be provided in the Active ContractComponent
							DWLError dwlError = CTCCompositeUtil.getDWLErrorObject(
									CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
									DWLErrorCode.FIELD_VALIDATION_ERROR,
									CTCErrorConstants.CONTRACT_PARTY_ROLE_MISSING_IN_THE_ACTIVE_CONTRCOMP,
									inputContract.getControl());

							vecDWLError.add(dwlError);

						}
					}
					
					if(CTCCommonUtil.isNotEmpty(vecRoles))
					{
						if(vecRoles.size() > 1){
							
							// Only one ContractPartyRole must be provided in the input ContractComponent.
							DWLError dwlError = CTCCompositeUtil.getDWLErrorObject(
									CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
									DWLErrorCode.FIELD_VALIDATION_ERROR,
									CTCErrorConstants.ONLY_ONE_CONTRACT_PARTY_ROLE_REQUIRED_IN_CONTRCOMP,
									inputContract.getControl());

							vecDWLError.add(dwlError);

						}
						size = size + 1;
					}
				}
				
				if(size > 1){
					// Only one ContractPartyRole must be provided in the input Contract.
					DWLError dwlError = CTCCompositeUtil.getDWLErrorObject(
							CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
							DWLErrorCode.FIELD_VALIDATION_ERROR,
							CTCErrorConstants.ONLY_ONE_CONTRACT_PARTY_ROLE_REQUIRED_IN_CONTRACT,
							inputContract.getControl());

					vecDWLError.add(dwlError);

				}
				
				if(activeCards > 1){
					
					// Only one Active ContractComponent must be provided in the input Contract.
					DWLError dwlError = CTCCompositeUtil.getDWLErrorObject(
							CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
							DWLErrorCode.FIELD_VALIDATION_ERROR,
							CTCErrorConstants.ONLY_ONE_ACTIVE_CONTRACT_COMPONENT_REQUIRED_IN_CONTRACT,
							inputContract.getControl());

					vecDWLError.add(dwlError);

				}
			}
			
			if (vecDWLError.size() > 0) {

				status.setStatus(DWLStatus.FATAL);
				status.setDwlErrorGroup(vecDWLError);
				throw new CTCCompositeException(CTCCompositeUtil
						.createResponseByStatus(status));
			}

	}

	/**
	 * 
	 * @param businessObject
	 * @param componentId
	 * @param status
	 * @throws CTCCompositeException
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.ctc.mdm.services.proxy.CTCMaintainBaseBP#validateStructure(com.dwl
	 * .base.DWLCommon, java.lang.String, com.dwl.base.error.DWLStatus)
	 */
	@SuppressWarnings("unchecked")
	protected void validateStructure(DWLCommon businessObject,
			String componentId, DWLStatus status) throws CTCCompositeException {

		TCRMContractBObj inputContract = (TCRMContractBObj) businessObject;
		Vector<TCRMAdminNativeKeyBObj> vecTCRMAdminNativeKeysBObj = (Vector<TCRMAdminNativeKeyBObj>) inputContract
				.getItemsTCRMAdminNativeKeyBObj();

		Vector<TCRMContractComponentBObj> vecTCRMContractComponentBObj = inputContract
				.getItemsTCRMContractComponentBObj();

		String clientSystemName = inputContract.getControl().getClientSystemName();

		Vector<DWLError> vecDWLError = new Vector<DWLError>();
		if (CTCCommonUtil.isEmpty(vecTCRMAdminNativeKeysBObj)) {
			DWLError dwlError = CTCCompositeUtil.getDWLErrorObject(
					CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
					DWLErrorCode.FIELD_VALIDATION_ERROR,
					CTCErrorConstants.ADMINNATIVEKEY_IS_REQUIRED_ERR_CODE,
					inputContract.getControl());

			vecDWLError.add(dwlError);
		}

		if (CTCCommonUtil.isEmpty(vecTCRMContractComponentBObj)) {
			DWLError dwlError = CTCCompositeUtil.getDWLErrorObject(
					CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
					DWLErrorCode.FIELD_VALIDATION_ERROR,
					CTCErrorConstants.CONTRACT_COMPONENT_NOT_FOUND_ERR_CODE,
					inputContract.getControl());

			vecDWLError.add(dwlError);
		} else {
			for (TCRMContractComponentBObj tcrmContractComponentBObj : vecTCRMContractComponentBObj) {

				Vector<TCRMContractPartyRoleBObj> vecTCRMContractPartyRoleBObj = tcrmContractComponentBObj
						.getItemsTCRMContractPartyRoleBObj();

				if (CTCCommonUtil.isEmpty(vecTCRMContractPartyRoleBObj)) {
					
					if(!StringUtils.compareIgnoreCaseWithTrim(strEpsilon, clientSystemName)){
						DWLError dwlError = CTCCompositeUtil
								.getDWLErrorObject(
								CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
								DWLErrorCode.FIELD_VALIDATION_ERROR,
								CTCErrorConstants.CONTRACT_PARTY_ROLE_NOT_FOUND_ERR_CODE,
								inputContract.getControl());

						vecDWLError.add(dwlError);
					}
				}

				else {
					for (TCRMContractPartyRoleBObj tcrmContractPartyRoleBObj : vecTCRMContractPartyRoleBObj) {
						TCRMPartyBObj tcrmPartyBObj = tcrmContractPartyRoleBObj
								.getTCRMPartyBObj();
						if (tcrmPartyBObj == null) {
							DWLError dwlError = CTCCompositeUtil
									.getDWLErrorObject(
											CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
											DWLErrorCode.FIELD_VALIDATION_ERROR,
											CTCErrorConstants.PARTY_NOT_FOUND_ERR_CODE,
											inputContract.getControl());

							vecDWLError.add(dwlError);
						} else {
							Vector<TCRMAdminContEquivBObj> vecTCRMAdminContEquivBObj = tcrmPartyBObj
									.getItemsTCRMAdminContEquivBObj();

							if (CTCCommonUtil
									.isEmpty(vecTCRMAdminContEquivBObj)) {
								DWLError dwlError = CTCCompositeUtil
										.getDWLErrorObject(
												CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
												DWLErrorCode.FIELD_VALIDATION_ERROR,
												CTCErrorConstants.ADMINCONTEQUIVOBJ_MISSING_ERROR_CODE,
												inputContract.getControl());

								vecDWLError.add(dwlError);

							}
						}

					}
				}

			}
		}

		if (vecDWLError.size() > 0) {

			status.setStatus(DWLStatus.FATAL);
			status.setDwlErrorGroup(vecDWLError);
			throw new CTCCompositeException(CTCCompositeUtil
					.createResponseByStatus(status));
		}

	}

	/**
	 * This method search for a TCRMContractBObj based on its AdminContractId
	 * and AdminNativeKey The returned fields are ContractIdPK, EndDate and
	 * ContractLastUpdateDate.
	 * 
	 * @param contract
	 * @return TCRMContractBObj
	 * @throws BusinessProxyException
	 */

	@SuppressWarnings("unchecked")
	public TCRMContractBObj searchContract(TCRMContractBObj contract)
			throws Exception {

		TCRMContractBObj contractBObj = null;

		String sql = null;
		List<String> sqlParam = new ArrayList<String>(2);
		String adminFldNameType = null;
		String adminContractId = null;
		QueryConnection connection = null;
		DWLStatus status = new DWLStatus();
		String clientSystemName = contract.getControl().getClientSystemName();
		Vector<TCRMAdminNativeKeyBObj> vecAdminNativeKeysBObj = (Vector<TCRMAdminNativeKeyBObj>) contract
				.getItemsTCRMAdminNativeKeyBObj();

		try {
			vecAdminNativeKeysBObj = contract.getItemsTCRMAdminNativeKeyBObj();
			if (StringUtils
					.compareIgnoreCaseWithTrim(
							clientSystemName,
							CTCCommonUtil
									.getProperty(CTCConstants.COSTAR_CLIENT_SYSTEM_NAME))) {
				for (TCRMAdminNativeKeyBObj tcrmAdminNativeKeyBObj : vecAdminNativeKeysBObj) {
					if (StringUtils.compareIgnoreCaseWithTrim(
							tcrmAdminNativeKeyBObj.getAdminFieldNameType(),
							CTCConstants.ADMIN_FIELD_NAME_TYPE_COSTAR)) {
						adminFldNameType = tcrmAdminNativeKeyBObj
								.getAdminFieldNameType();
						adminContractId = tcrmAdminNativeKeyBObj
								.getAdminContractId();
						break;
					}
				}
			} else {
				adminFldNameType = ((TCRMAdminNativeKeyBObj) vecAdminNativeKeysBObj
						.firstElement()).getAdminFieldNameType();
				adminContractId = ((TCRMAdminNativeKeyBObj) vecAdminNativeKeysBObj
						.firstElement()).getAdminContractId();
			}
			sql = CTCSQLConstants.SEARCH_CONTRACT_BY_NATIVE_ADMIN_FLD_TP_CD_AND_ADMIN_CONTRACT_ID_QUERY_SQL;

			sqlParam.add(new String(adminContractId));
			sqlParam.add(new String(adminFldNameType));

			ResultSet rs = null;

			connection = DataManager.getInstance().getQueryConnection();

			if (connection == null) {
				throw new Exception(
						CTCErrorConstants.ERROR_GETTING_DATABASE_CONNECTION);
			}

			rs = connection.queryResults(sql, sqlParam.toArray());
			CTCContractSearchResultProcessor rsprocessor = new CTCContractSearchResultProcessor();
			Vector<TCRMContractBObj> vecContract = rsprocessor
					.getObjectFromResultSet(rs);

			Iterator itrContract = vecContract.iterator();

			while (itrContract.hasNext()) {

				TCRMContractBObj dbContract = (TCRMContractBObj) itrContract
						.next();
				if (dbContract.getTerminationDate() != null) {
					itrContract.remove();
				} else if (dbContract.getEndDate() != null) {
					if (DWLDateTimeUtilities.getCurrentSystemTime().compareTo(
							dbContract.getEndDate()) >= 0)
						itrContract.remove();

				}

			}

			if (CTCCommonUtil.isNotEmpty(vecContract) && vecContract.size() > 1) {

				DWLError dwlError = CTCCompositeUtil
						.getDWLErrorObject(
								CTCConstants.CTCMAINTAINCONTRACT_COMPONENTID,
								DWLErrorCode.DUPLICATE_RECORD_ERROR,
								CTCErrorConstants.MULTIPLE_ACTIVE_CONTRACTS_FOUND_ERR_CODE,
								contract.getControl());

				status.addError(dwlError);

				status.setStatus(DWLStatus.FATAL);

				throw new CTCCompositeException(CTCCompositeUtil
						.createResponseByStatus(status));

			} else if (vecContract.size() == 1) {
				contractBObj = (TCRMContractBObj) vecContract.firstElement();

				if (StringUtils.compareIgnoreCaseWithTrim(strTSYS, clientSystemName)) {
					
					TCRMContractComponentBObj contractComponentBObj = (TCRMContractComponentBObj)
						contract.getItemsTCRMContractComponentBObj().firstElement();
					
					TCRMContractPartyRoleBObj contractPartyRoleBObj = (TCRMContractPartyRoleBObj)
						contractComponentBObj.getItemsTCRMContractPartyRoleBObj().firstElement();
					
					TCRMAdminContEquivBObj adminContEquivBObj = (TCRMAdminContEquivBObj)
						contractPartyRoleBObj.getTCRMPartyBObj().getItemsTCRMAdminContEquivBObj().firstElement();
					
					contract.getControl().put(CTCConstants.ADMIN_CONTRACT_ID_TYPE_CUSTOMER_ID, adminContEquivBObj.getAdminPartyId());  
				}
				contractBObj = getContractInfo(contractBObj.getContractIdPK(),
						CTCConstants.INQUIRY_LEVEL_1000,
						CTCConstants.PARTY_INQUIRY_LEVEL_1, contract.getControl());
			}

		} catch (Exception e) {
			throw e;

		} finally {
			if (connection != null) {
				connection.close();
			}

		}
		if (contractBObj == null)
			logger.info("foundContract: is null");
		return contractBObj;
	}

	/**
	 * This method get the Contract information
	 * 
	 * @param contractId
	 * @param contractInquiryLevel
	 * @param partyInquiryLevel
	 * @param theControl
	 * @return
	 * @throws Exception
	 */
	private TCRMContractBObj getContractInfo(String contractId,
			String contractInquiryLevel, String partyInquiryLevel,
			DWLControl theControl) throws Exception {
		TCRMContractBObj retContract = null;
		Vector<String> queryParams = new Vector<String>();
		queryParams.add(contractId);
		queryParams.add(contractInquiryLevel);
		queryParams.add(partyInquiryLevel);
		DWLTransactionInquiry objTxnInquiry = new DWLTransactionInquiry(
				CTCConstants.GET_CONTRACT, queryParams, theControl);
		DWLResponse dwlResponse = null;

		dwlResponse = super.processInquiryObject(objTxnInquiry);

		if ((CTCCompositeUtil.checkError(dwlResponse))
				|| (dwlResponse.getData() == null)) {
			DWLStatus status = dwlResponse.getStatus();

			throw new CTCCompositeException(CTCCompositeUtil
					.createResponseByStatus(status));

		} else {
			retContract = (TCRMContractBObj) dwlResponse.getData();
		}

		return retContract;
	}

	/**
	 * @throws CTCCompositeException
	 *             , BusinessProxyException, Exception This method resolves
	 *             Contract identity. It compares request ContractBObj business
	 *             key with the a vector of existing ContractBObj business key.
	 * 
	 * @param requestContractBObj
	 * @param foundContractBObj
	 * @param isFineGrainTx
	 * @throws BusinessProxyException
	 * @throws TCRMException
	 * @throws CTCCompositeException
	 * @throws
	 */

	public void resolveIdentity(DWLCommon requestContractBObj,
			DWLCommon foundContractBObj, boolean isFineGrainTx)
			throws BusinessProxyException, CTCCompositeException {

		logger.info("Entering CTCMaintainContractBP.resolveIdentity()");

		try {
			handleContract((TCRMContractBObj) requestContractBObj,
					(TCRMContractBObj) foundContractBObj);

		} catch (Exception e) {

			logger.info(e.getMessage());
			if (e instanceof CTCCompositeException) {
				throw (CTCCompositeException) e;
			}

			throw new BusinessProxyException(e);
		}
		logger.info("Exiting CTCMaintainContractBP.resolveIdentity()");
	}

	/**
	 * This method modifies transaction topLevelObject and transaction type. If
	 * it finds error in topLevelObject, it returns with empty response plus
	 * error message. Otherwise it fires transaction to MDM application.
	 * 
	 * @param txnObj
	 * @return Object
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Object fireTransaction(DWLTransactionPersistent txnObj)
			throws Exception {
		logger.info("Entering CTCMaintainContractBP.fireTransaction()");
		DWLResponse response = null;

		TCRMContractBObj requestContract = (TCRMContractBObj) txnObj
				.getTxnTopLevelObject();
		String txnType = null;
		DWLControl control = txnObj.getTxnControl();
		String clientSystemName = control.getClientSystemName();
		if (StringUtils.isNonBlank(requestContract.getContractIdPK())) {
			
			if (StringUtils.compareIgnoreCaseWithTrim(strTSYS, clientSystemName)) {
				
				TCRMContractComponentBObj contractComponentBObj = 
					(TCRMContractComponentBObj)requestContract.getItemsTCRMContractComponentBObj().firstElement();
				String contractComponentIdPK = contractComponentBObj.getContractComponentIdPK();
				String contractComponentLastUpdateDate = contractComponentBObj.getContractComponentLastUpdateDate();
				if(StringUtils.isNonBlank(contractComponentIdPK)
						&& StringUtils.isNonBlank(contractComponentLastUpdateDate)){
					txnType = CTCMaintainServiceConstants.UPDATE_CONTRACT_COMPONENT;
				}
				else
				{
					txnType = CTCMaintainServiceConstants.ADD_CONTRACT_COMPONENT;
				}
				txnObj.setTxnType(txnType);
				txnObj.setTxnTopLevelObject(contractComponentBObj);
			}
			else
			{
				txnType = CTCMaintainServiceConstants.UPDATE_CONTRACT;
				txnObj.setTxnType(CTCMaintainServiceConstants.UPDATE_CONTRACT);
			}
			requestContract.getControl().setRequestName(
					CTCMaintainServiceConstants.CTCMAINTAINCONTRACT);
			
			
		} else {
			txnType = CTCMaintainServiceConstants.ADD_CONTRACT;
			txnObj.setTxnType(CTCMaintainServiceConstants.ADD_CONTRACT);
			requestContract.getControl().setRequestName(
					CTCMaintainServiceConstants.CTCMAINTAINCONTRACT);

		}

		response = (DWLResponse) super.fireTransaction(txnObj);

		if (CTCCompositeUtil.checkError(response.getStatus())) {
			return response;
		} else {
			
			TCRMContractComponentBObj objContractComponent = null;
			if(StringUtils.compareIgnoreCaseWithTrim(txnType, CTCMaintainServiceConstants.UPDATE_CONTRACT_COMPONENT)
					|| StringUtils.compareIgnoreCaseWithTrim(txnType, CTCMaintainServiceConstants.ADD_CONTRACT_COMPONENT)){
				objContractComponent = (TCRMContractComponentBObj)response.getData();
				
				/*
				 * Return the TCRMContractBObj in the response along with the TCRMContractComponentBObj.
				 */
				requestContract.getItemsTCRMContractComponentBObj().clear();
				requestContract.setTCRMContractComponentBObj(objContractComponent);
				response.setData(requestContract);
			}
			else
			{
				objContractComponent = (TCRMContractComponentBObj) ((TCRMContractBObj) response
						.getData()).getItemsTCRMContractComponentBObj()
						.firstElement();
			}
			Vector<TCRMContractPartyRoleBObj> vecRespContractPartyRole = objContractComponent
					.getItemsTCRMContractPartyRoleBObj();

			for (TCRMContractPartyRoleBObj objContractPartyRole : vecRespContractPartyRole) {

				TCRMPersonBObj tcrmPersonBObj = (TCRMPersonBObj) htPersonBObj
						.get(objContractPartyRole.getRoleType());

				// Fix BTM defect
				if (tcrmPersonBObj.getItemsTCRMAdminContEquivBObj().isEmpty()) {
				    IParty partyComp = (IParty) TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
				    Vector<TCRMAdminContEquivBObj> vecAdminCont = partyComp.getAllPartyAdminSysKeys(tcrmPersonBObj.getPartyId(), control);
				    
				    if (vecAdminCont != null && !vecAdminCont.isEmpty()) {
				        tcrmPersonBObj.getItemsTCRMAdminContEquivBObj().addAll(vecAdminCont);
				    }
			    }
				
				TCRMAdminContEquivBObj adminContEquivObj = null;
				if (!tcrmPersonBObj.getItemsTCRMAdminContEquivBObj().isEmpty()) {
				    adminContEquivObj = (TCRMAdminContEquivBObj)tcrmPersonBObj.getItemsTCRMAdminContEquivBObj().firstElement();
				}
				
				// Perform getparty to get the latest image of the party
				
				tcrmPersonBObj = (TCRMPersonBObj) getPartyInfo(
						tcrmPersonBObj.getPartyId(),
						CTCConstants.PARTY_TYPE_PERSON,
						TCRMCoreInquiryLevel.INQUIRY_LEVEL_1, tcrmPersonBObj
								.getControl()); 
				
				if (adminContEquivObj != null) {
				    tcrmPersonBObj.getItemsTCRMAdminContEquivBObj().add(adminContEquivObj);
				}
				
				objContractPartyRole.setTCRMPersonBObj(tcrmPersonBObj);
			}
			requestContract.getControl().setRequestName(
					CTCMaintainServiceConstants.CTCMAINTAINCONTRACT);

		}

		
		logger.info("Exiting CTCMaintainContractBP.fireTransaction()");
		return response;
	}

	/**
	 * This method invokes handleContract Component and handle Native Key . if
	 * existing contract is found, set its id and last update date to the
	 * request contract.
	 * 
	 * @param inputContract
	 * @param foundContract
	 * @throws Exception
	 */
	private void handleContract(TCRMContractBObj inputContract,
			TCRMContractBObj foundContract) throws Exception {
		logger.info("Entering CTCMaintainContractBP.handleContract()");
		// Handle Contract
		if (foundContract != null) {

			inputContract.setContractIdPK(((TCRMContractBObj) foundContract)
					.getContractIdPK());

			inputContract
					.setContractLastUpdateDate(((TCRMContractBObj) foundContract)
							.getContractLastUpdateDate());

		}

		handleContractAdminNativekey(inputContract, foundContract);
		handleContractComponent(inputContract, foundContract);

		logger.info("Exiting CTCMaintainContractBP.handleContract()");

	}

	/**
	 * This method handles AdminNativeKey Objects. If existing AdminNativekey,
	 * set primary key and lastupdatedate in the request else set contract id
	 * 
	 * @param inputContract
	 * @param foundContract
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void handleContractAdminNativekey(TCRMContractBObj inputContract,
			TCRMContractBObj foundContract) throws Exception {
		Vector<TCRMAdminNativeKeyBObj> vecInputAdminNativeKey = inputContract
				.getItemsTCRMAdminNativeKeyBObj();
		Vector<TCRMAdminNativeKeyBObj> vecFoundAdminNativeKey = null;

		if (foundContract == null) {
			vecFoundAdminNativeKey = new Vector();
		} else {
			vecFoundAdminNativeKey = foundContract
					.getItemsTCRMAdminNativeKeyBObj();
		}
		for (TCRMAdminNativeKeyBObj objInputAdminNativeKey : vecInputAdminNativeKey) {
			boolean foundSameNativeKey = false;
			for (TCRMAdminNativeKeyBObj objFoundAdminNativeKey : vecFoundAdminNativeKey) {
				if (isIdentical(objInputAdminNativeKey, objFoundAdminNativeKey)) {
					foundSameNativeKey = true;
					objInputAdminNativeKey
							.setAdminNativeKeyIdPK(objFoundAdminNativeKey
									.getAdminNativeKeyIdPK());
					objInputAdminNativeKey
							.setNativeKeyLastUpdateDate(objFoundAdminNativeKey
									.getNativeKeyLastUpdateDate());
				}

				if (foundSameNativeKey)
					break;

			}

			if (!foundSameNativeKey) {
				if (foundContract != null) {
					objInputAdminNativeKey.setContractId(foundContract
							.getContractIdPK());
				}
			}
		}

	}

	/**
	 * This method resolves ContractComponent objects by comparing it with
	 * corresponding database Objects. if existing ContractComponent, set its
	 * contractId, componentId and lastupdatedate in the request else set
	 * ContractId. Then invokes handleContractPartyRole() method.
	 * 
	 * @param inputContract
	 * @param foundContract
	 * @throws CTCCompositeException
	 * @throws TCRMException
	 */

	@SuppressWarnings("unchecked")
	private void handleContractComponent(TCRMContractBObj inputContract,
			TCRMContractBObj foundContract) throws Exception {

		logger.info("Entering CTCMaintainContractBP.handleContractComponent()");

		Vector<TCRMContractComponentBObj> vecFoundContractComponents = null;
		boolean foundSameContractComponent = false;
		String clientSystemName = inputContract.getControl()
				.getClientSystemName();

		Vector<TCRMContractComponentBObj> vecReqContractComponents = inputContract
				.getItemsTCRMContractComponentBObj();

		if (foundContract == null) {
			vecFoundContractComponents = new Vector<TCRMContractComponentBObj>();
		} else {
			vecFoundContractComponents = foundContract
					.getItemsTCRMContractComponentBObj();
		}

		if (StringUtils.compareIgnoreCaseWithTrim(strTSYS, clientSystemName)) {

			for (TCRMContractComponentBObj reqContractComponentBObj : vecReqContractComponents) {

				TCRMContractPartyRoleBObj reqTCRMContractPartyRoleBObj = (TCRMContractPartyRoleBObj) reqContractComponentBObj
						.getItemsTCRMContractPartyRoleBObj().firstElement();

				TCRMAdminContEquivBObj reqTCRMAdminContEquivBObj = (TCRMAdminContEquivBObj) reqTCRMContractPartyRoleBObj
						.getTCRMPartyBObj().getItemsTCRMAdminContEquivBObj()
						.firstElement();

				TCRMAdminNativeKeyBObj reqAdminNativeKeyBObj = CTCCompositeUtil
						.constructAdminNativeKey(reqTCRMAdminContEquivBObj
								.getAdminPartyId(),
								CTCConstants.ADMIN_FIELD_NAME_TYPE_CUSTOMER_ID,
								reqContractComponentBObj.getControl());
				foundSameContractComponent = false;

				for (TCRMContractComponentBObj foundContractComponentBObj : vecFoundContractComponents) {

					Vector<TCRMAdminNativeKeyBObj> vecFoundTCRMAdminNativeKeyBObj = foundContractComponentBObj
							.getItemsTCRMAdminNativeKeyBObj();

					if (CTCCommonUtil
							.isNotEmpty(vecFoundTCRMAdminNativeKeyBObj)) {

						TCRMAdminNativeKeyBObj foundAdminNativeKeyBObj = (TCRMAdminNativeKeyBObj) vecFoundTCRMAdminNativeKeyBObj
								.firstElement();

						if (isIdentical(reqAdminNativeKeyBObj,
								foundAdminNativeKeyBObj)) {
							foundSameContractComponent = true;

							reqContractComponentBObj
									.setContractId(foundContractComponentBObj
											.getContractId());
							reqContractComponentBObj
									.setContractComponentIdPK(foundContractComponentBObj
											.getContractComponentIdPK());
							reqContractComponentBObj
									.setContractComponentLastUpdateDate(foundContractComponentBObj
											.getContractComponentLastUpdateDate());
							handleContractPartyRole(reqContractComponentBObj,
									foundContractComponentBObj);
							break;

						}

					}

				}

				if (!foundSameContractComponent) {

					reqContractComponentBObj
							.setTCRMAdminNativeKeyBObj(reqAdminNativeKeyBObj);

					if (foundContract != null) {
						reqContractComponentBObj.setContractId(foundContract
								.getContractIdPK());
					}
					handleContractPartyRole(reqContractComponentBObj, null);
				}
			}
		}
		else if(StringUtils.compareIgnoreCaseWithTrim(strEpsilon, clientSystemName)){
			
			TCRMContractPartyRoleBObj inputRoleBObj = null;

			//Find the ContractPartyRole from the input. There will always be only one ContractPartyRole provided in the input.
			for (TCRMContractComponentBObj reqContractComponentBObj : vecReqContractComponents) {

				if(CTCCommonUtil.isNotEmpty(reqContractComponentBObj.getItemsTCRMContractPartyRoleBObj())){
					inputRoleBObj = (TCRMContractPartyRoleBObj) reqContractComponentBObj
						.getItemsTCRMContractPartyRoleBObj().firstElement();
				}
			}
			
			Vector<TCRMContractComponentBObj> vecTemp = new Vector<TCRMContractComponentBObj>();
			for (TCRMContractComponentBObj reqContractComponentBObj : vecReqContractComponents) {

				TCRMContractPartyRoleBObj reqTCRMContractPartyRoleBObj = null;
				if(CTCCommonUtil.isNotEmpty(reqContractComponentBObj.getItemsTCRMContractPartyRoleBObj())){
					reqTCRMContractPartyRoleBObj = (TCRMContractPartyRoleBObj) reqContractComponentBObj
						.getItemsTCRMContractPartyRoleBObj().firstElement();
				}
				
				TCRMAdminNativeKeyBObj reqAdminNativeKeyBObj = (TCRMAdminNativeKeyBObj) reqContractComponentBObj
					.getItemsTCRMAdminNativeKeyBObj().firstElement(); 

				foundSameContractComponent = false;

				for (TCRMContractComponentBObj foundContractComponentBObj : vecFoundContractComponents) {

					Vector<TCRMAdminNativeKeyBObj> vecFoundTCRMAdminNativeKeyBObj = foundContractComponentBObj
							.getItemsTCRMAdminNativeKeyBObj();

					Vector<TCRMContractPartyRoleBObj> vecFoundContractRoles = foundContractComponentBObj.getItemsTCRMContractPartyRoleBObj();

					TCRMContractPartyRoleBObj foundContractPartyRoleBObj = null;
					
					if(CTCCommonUtil.isNotEmpty(vecFoundContractRoles)){
						foundContractPartyRoleBObj = (TCRMContractPartyRoleBObj) vecFoundContractRoles.firstElement();
					}
					
					if (CTCCommonUtil.isNotEmpty(vecFoundTCRMAdminNativeKeyBObj)) {

						TCRMAdminNativeKeyBObj foundAdminNativeKeyBObj = (TCRMAdminNativeKeyBObj) vecFoundTCRMAdminNativeKeyBObj
								.firstElement();

						if (isIdentical(reqAdminNativeKeyBObj,
								foundAdminNativeKeyBObj)) {
							foundSameContractComponent = true;

							reqAdminNativeKeyBObj.setAdminNativeKeyIdPK(foundAdminNativeKeyBObj.getAdminNativeKeyIdPK());
							reqAdminNativeKeyBObj.setNativeKeyLastUpdateDate(foundAdminNativeKeyBObj.getNativeKeyLastUpdateDate());
							reqAdminNativeKeyBObj.setContractId(foundAdminNativeKeyBObj.getContractId());
							reqContractComponentBObj
									.setContractId(foundContractComponentBObj
											.getContractId());
							reqContractComponentBObj
									.setContractComponentIdPK(foundContractComponentBObj
											.getContractComponentIdPK());
							reqContractComponentBObj
									.setContractComponentLastUpdateDate(foundContractComponentBObj
											.getContractComponentLastUpdateDate());
							
							if(reqTCRMContractPartyRoleBObj != null && foundContractPartyRoleBObj != null){
								handleContractPartyRole(reqContractComponentBObj,
										foundContractComponentBObj);
								
								//EndDate the existing Role, RoleLocations & Privacy Preferences if the Card is not 'ACTIVE'
								if(!(StringUtils.compareIgnoreCaseWithTrim(
										reqContractComponentBObj.getContractStatusType(), 
										CTCConstants.CONTRACT_STATUS_TYPE_ACTIVE))){
									endDateContractRole(reqTCRMContractPartyRoleBObj);	
								}
								
							}
							else if(reqTCRMContractPartyRoleBObj != null && foundContractPartyRoleBObj == null){
								
								// This means that Card is already inactive.No need to process the input ContractRole.
								// Remove the ContractRole from the request
								// This scenario should not occur.
								
								reqContractComponentBObj.getItemsTCRMContractPartyRoleBObj().clear();
							}
							else if(reqTCRMContractPartyRoleBObj == null && foundContractPartyRoleBObj != null){
								
								// EndDate the existing Role, RoleLocations & Privacy Preferences
								endDateContractRole(foundContractPartyRoleBObj);
								
								foundContractPartyRoleBObj.setTCRMPartyBObj(null);
								
								// Include the existing Role in the Input so that it gets updated.
								reqContractComponentBObj.getItemsTCRMContractPartyRoleBObj().add(foundContractPartyRoleBObj);
							}
							else if(reqTCRMContractPartyRoleBObj == null && foundContractPartyRoleBObj == null){
								// Do Nothing
							}
							
							break;

						}

					}

				}

				if (!foundSameContractComponent) {

					if (foundContract != null) {
						reqContractComponentBObj.setContractId(foundContract
								.getContractIdPK());
					}
					
					if(reqTCRMContractPartyRoleBObj != null){
						handleContractPartyRole(reqContractComponentBObj, null);
					}
					else
					{
						// Add to the temp Vector<TCRMContractComponentBObj>.
						// This is done because ContractRole will need to be created for the input ContractComponent 
						// along with the RoleLocation & Privacy Preferences.
						// And must be end-dated as well.
						vecTemp.add(reqContractComponentBObj);
					}
				}
			}

			for (TCRMContractComponentBObj reqContractComponentBObj : vecReqContractComponents) {

				if(CTCCommonUtil.isEmpty(reqContractComponentBObj.getItemsTCRMContractPartyRoleBObj())){

					//process the temp Vector<TCRMContractComponentBObj>
					
					if(vecTemp.contains(reqContractComponentBObj)){
						//1.Clone the Input Role, RoleLocations & Privacy Preferences
						//2. EndDate the Role, RoleLocations & Privacy Preferences
						//3. Store the cloned Role object into the ContractComponent.

						TCRMContractPartyRoleBObj clonedRoleBObj = (TCRMContractPartyRoleBObj) TCRMObjectCloner.deepCopy(inputRoleBObj);
						endDateContractRole(clonedRoleBObj);
						
						reqContractComponentBObj.setTCRMContractPartyRoleBObj(clonedRoleBObj);
					}
				}
				else
				{
					//EndDate the Role, RoleLocation & Privacy Preferences if the ContractStatus != ACTIVE
					if(!(StringUtils.compareIgnoreCaseWithTrim(
							reqContractComponentBObj.getContractStatusType(), CTCConstants.CONTRACT_STATUS_TYPE_ACTIVE))){
						Vector<TCRMContractPartyRoleBObj> vecContractRoles = reqContractComponentBObj.getItemsTCRMContractPartyRoleBObj();
						TCRMContractPartyRoleBObj contractRole = vecContractRoles.firstElement();
						endDateContractRole(contractRole);
					}
				}
				
			}
		
		}
		else {

			TCRMContractComponentBObj reqContractComponentBObj = vecReqContractComponents
					.firstElement();
			TCRMContractComponentBObj foundContractComponentBObj = null;
			if (CTCCommonUtil.isNotEmpty(vecFoundContractComponents)) {
				foundContractComponentBObj = vecFoundContractComponents
						.firstElement();

				reqContractComponentBObj
						.setContractId(foundContractComponentBObj
								.getContractId());
				reqContractComponentBObj
						.setContractComponentIdPK(foundContractComponentBObj
								.getContractComponentIdPK());
				reqContractComponentBObj
						.setContractComponentLastUpdateDate(foundContractComponentBObj
								.getContractComponentLastUpdateDate());

			} else {
				if (foundContract != null) {
					reqContractComponentBObj.setContractId(foundContract
							.getContractIdPK());
				}
			}
			handleContractPartyRole(reqContractComponentBObj,
					foundContractComponentBObj);

		}

		logger.info("Exiting CTCMaintainContractBP.handleContractComponent()");
	}

	/**
	 * 
	 * @param contractPartyRoleBObj
	 */
	private void endDateContractRole(TCRMContractPartyRoleBObj contractPartyRoleBObj) throws Exception {
		contractPartyRoleBObj.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
		
		Vector<TCRMContractRoleLocationBObj> vecRoleLocation = contractPartyRoleBObj.getItemsTCRMContractRoleLocationBObj();
		if(CTCCommonUtil.isNotEmpty(vecRoleLocation)){
			for (TCRMContractRoleLocationBObj contractRoleLocationBObj : vecRoleLocation) {
				contractRoleLocationBObj.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
			}
		}
		
		if(contractPartyRoleBObj instanceof CTCContractRoleBObjExt){
			CTCContractRoleBObjExt contractRoleBObjExt = (CTCContractRoleBObjExt)contractPartyRoleBObj;
			
			Vector<CTCContractRolePrivPrefBObj> vecPrivPref = contractRoleBObjExt.getItemsCTCContractRolePrivPrefBObj();
			if(CTCCommonUtil.isNotEmpty(vecPrivPref)){
				for (CTCContractRolePrivPrefBObj contractRolePrivPrefBObj : vecPrivPref) {
					contractRolePrivPrefBObj.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
				}
			}
		}
	}

	/**
	 * This method invokes resolvIdentity() method of ContractComponent child
	 * BObj business proxy.
	 * 
	 * @param contractComponent
	 * @param foundContractComp
	 * @throws CTCCompositeException
	 * @throws TCRMException
	 */
	@SuppressWarnings("unchecked")
	private void handleContractPartyRole(
			TCRMContractComponentBObj reqContractComponent,
			TCRMContractComponentBObj foundContractComp) throws Exception {

		logger.info("Entering CTCMaintainContractBP.handleContractPartyRole()");

		Vector<TCRMContractPartyRoleBObj> vecFoundContractPartyRole = null;

		Vector<TCRMContractPartyRoleBObj> vecReqContractPartyRole = reqContractComponent
				.getItemsTCRMContractPartyRoleBObj();
		boolean isPartyCollapsed = false;

		if (foundContractComp != null) {
			vecFoundContractPartyRole = ((TCRMContractComponentBObj) foundContractComp)
					.getItemsTCRMContractPartyRoleBObj();
		} else {
			vecFoundContractPartyRole = new Vector();
		}

		if (CTCCommonUtil.isNotEmpty(vecReqContractPartyRole)) {

			for (TCRMContractPartyRoleBObj requestContractPartyRoleObj : vecReqContractPartyRole) {
				// Step 1: Link input Party Addresses and Contact Method
				Vector<TCRMContractRoleLocationBObj> vecReqContractRoleLocation = requestContractPartyRoleObj
						.getItemsTCRMContractRoleLocationBObj();

				TCRMPartyBObj reqParty = requestContractPartyRoleObj
						.getTCRMPartyBObj();
				if (reqParty != null) {
					Vector<TCRMPartyAddressBObj> vecReqPartyAddress = reqParty
							.getItemsTCRMPartyAddressBObj();
					if (CTCCommonUtil.isNotEmpty(vecReqPartyAddress)) {
						for (TCRMPartyAddressBObj reqPartyAddress : vecReqPartyAddress) {

							for (TCRMContractRoleLocationBObj reqContractRoleLocation : vecReqContractRoleLocation) {

								if (reqContractRoleLocation
										.getObjectReferenceId()
										.equals(
												reqPartyAddress
														.getObjectReferenceId())) {
									CTCCommonUtil
											.standardizeAddress(reqPartyAddress
													.getTCRMAddressBObj());
									reqContractRoleLocation
											.setTCRMPartyAddressBObj(reqPartyAddress);
								}

							}
						}
					}
					Vector<TCRMPartyContactMethodBObj> vecPartyContact = reqParty
							.getItemsTCRMPartyContactMethodBObj();

					if (CTCCommonUtil.isNotEmpty(vecPartyContact)) {

						for (TCRMPartyContactMethodBObj partyContact : vecPartyContact) {
							TCRMContactMethodBObj contMethodObj = partyContact
									.getTCRMContactMethodBObj();
							if (StringUtils.compareWithTrim(contMethodObj
									.getContactMethodType(),
									CTCConstants.CONT_METH_TP_EMAIL)) {
								for (TCRMContractRoleLocationBObj contractRoleLocation : vecReqContractRoleLocation) {
									if (StringUtils
											.compareIgnoreCaseWithTrim(
													contractRoleLocation
															.getObjectReferenceId(),
													partyContact
															.getObjectReferenceId())) {
										contractRoleLocation
												.setTCRMPartyContactMethodBObj(partyContact);

									}
								}
							}

						}
					}
					// Step 2 : Process Party Object

					TCRMPartyBObj inputParty = requestContractPartyRoleObj
							.getTCRMPartyBObj();

					DWLTransactionPersistent txnPersistent = new DWLTransactionPersistent();
					txnPersistent.setTxnTopLevelObject(inputParty);
					txnPersistent.setTxnControl(inputParty.getControl());
					CTCMaintainPartyBP ctcMaintainPartyBP = new CTCMaintainPartyBP();
					DWLResponse responsePartyObj = (DWLResponse) ctcMaintainPartyBP.executeImpl(txnPersistent);

					if (CTCCompositeUtil.checkError(responsePartyObj)) {
						logger.error("Error in Person Add or update");
						throw new CTCCompositeException(responsePartyObj);
					}

					TCRMPersonBObj dbPersonObj = (TCRMPersonBObj) responsePartyObj
							.getData();

					if (dbPersonObj.getInactivatedDate() != null
							&& StringUtils.compareWithTrim(dbPersonObj
									.getPartyActiveIndicator(), "N")) {
						TCRMInactivatedPartyBObj dbInactivatedPartyObj = dbPersonObj
								.getTCRMInactivatedPartyBObj();
						if (dbInactivatedPartyObj != null) {
							Vector<TCRMPartyLinkBObj> vecPartyLink = dbInactivatedPartyObj
									.getItemsTCRMPartyLinkBObj();
							String collapsePartyId = ((TCRMPartyLinkBObj) vecPartyLink
									.elementAt(0)).getTargetPartyId();
							requestContractPartyRoleObj
									.setPartyId(collapsePartyId);

							TCRMPartyBObj dbCollapsedPartyObj = getPartyInfo(
									collapsePartyId,
									CTCConstants.PARTY_TYPE_PERSON,
									TCRMCoreInquiryLevel.INQUIRY_LEVEL_1,
									dbPersonObj.getControl());
							
							if (dbPersonObj.getItemsTCRMAdminContEquivBObj().isEmpty()) {  // Fix BTM defect
								IParty partyComp = (IParty) TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
								Vector<TCRMAdminContEquivBObj> vecAdminContEquiv = partyComp.getAllPartyAdminSysKeys(dbPersonObj.getPartyId(), inputParty.getControl());
								
								if (vecAdminContEquiv != null && !vecAdminContEquiv.isEmpty()) {
								    dbPersonObj.getItemsTCRMAdminContEquivBObj().addAll(vecAdminContEquiv);
								}
							} 
							
							if (!dbPersonObj.getItemsTCRMAdminContEquivBObj().isEmpty()) {
							    TCRMAdminContEquivBObj adminContEquivObj = (TCRMAdminContEquivBObj)dbPersonObj.getItemsTCRMAdminContEquivBObj().firstElement();
						        dbCollapsedPartyObj.getItemsTCRMAdminContEquivBObj().add(adminContEquivObj);
							}
						    
							dbPersonObj = (TCRMPersonBObj) dbCollapsedPartyObj;

							requestContractPartyRoleObj
									.setTCRMPartyBObj(dbCollapsedPartyObj);
							isPartyCollapsed = true;

						}

					} else {
						requestContractPartyRoleObj.setPartyId(dbPersonObj
								.getPartyId());
					}

					// Step 4 :

					Vector<TCRMPartyAddressBObj> vecDbPersonAddress = dbPersonObj
							.getItemsTCRMPartyAddressBObj();

					for (TCRMContractRoleLocationBObj reqContractRoleLocation : vecReqContractRoleLocation) {
						boolean foundContractLocation = false;

						TCRMPartyAddressBObj reqContractAddress = reqContractRoleLocation
								.getTCRMPartyAddressBObj();

						for (TCRMPartyAddressBObj dbPersonAddress : vecDbPersonAddress) {

							if (reqContractAddress != null) {
								if (CTCCommonUtil
										.isAddressMatch(dbPersonAddress
												.getTCRMAddressBObj(),
												reqContractAddress
														.getTCRMAddressBObj())) {
									reqContractRoleLocation
											.setTCRMPartyAddressBObj(dbPersonAddress);
									reqContractRoleLocation
											.setLocationGroupId(dbPersonAddress
													.getPartyAddressIdPK());
									lstReqLocationGroupIds.add(reqContractRoleLocation.getLocationGroupId());
									
									
									foundContractLocation = true;
									break;
								}
							}

						}
						if (foundContractLocation) {
							continue;
						}
						Vector<TCRMPartyContactMethodBObj> vecDbPersonContact = dbPersonObj
								.getItemsTCRMPartyContactMethodBObj();
						TCRMPartyContactMethodBObj reqContactMethodObj = reqContractRoleLocation
								.getTCRMPartyContactMethodBObj();

						for (TCRMPartyContactMethodBObj dbPersonContactMethodObj : vecDbPersonContact) {

							TCRMContactMethodBObj dbContactMethodObj = dbPersonContactMethodObj
									.getTCRMContactMethodBObj();
							if (StringUtils.compareWithTrim(dbContactMethodObj
									.getContactMethodType(),
									CTCConstants.CONT_METH_TP_EMAIL)) {
								String strDbEmailId = dbContactMethodObj
										.getReferenceNumber();

								if (reqContactMethodObj != null) {
									String strReqEmailId = reqContactMethodObj
											.getTCRMContactMethodBObj()
											.getReferenceNumber();

									if (strReqEmailId.equals(strDbEmailId)) {
										reqContractRoleLocation
												.setTCRMPartyContactMethodBObj(dbPersonContactMethodObj);
										reqContractRoleLocation
												.setLocationGroupId(dbPersonContactMethodObj
														.getPartyContactMethodIdPK()); 
										lstReqLocationGroupIds.add(reqContractRoleLocation.getLocationGroupId());
											
										foundContractLocation = true;
										break;
									}
								}
							}
						}

					}

					htPersonBObj.put(requestContractPartyRoleObj.getRoleType(),
							requestContractPartyRoleObj.getTCRMPartyBObj());
					requestContractPartyRoleObj.setTCRMPartyBObj(null);

				}// Party null

				// Step 5: Get Corresponding Database Object

				boolean foundSameContractRole = false;
				if (vecFoundContractPartyRole.size() > 0) {
					for (TCRMContractPartyRoleBObj foundContractPartyRoleObj : vecFoundContractPartyRole) {

						if (foundContractPartyRoleObj.getRoleType().equals(
								requestContractPartyRoleObj.getRoleType())) {
							foundSameContractRole = true;

							if (isPartyCollapsed) {
								foundContractPartyRoleObj = refreshContractPartyRole(
										foundContractPartyRoleObj
												.getContractRoleIdPK(),
										requestContractPartyRoleObj
												.getControl());
								isPartyCollapsed = false;
							}

							requestContractPartyRoleObj
									.setContractRoleIdPK(foundContractPartyRoleObj
											.getContractRoleIdPK());
							requestContractPartyRoleObj
									.setContractComponentId(foundContractPartyRoleObj
											.getContractComponentId());

							requestContractPartyRoleObj
									.setContractPartyRoleLastUpdateDate(foundContractPartyRoleObj
											.getContractPartyRoleLastUpdateDate());

							handleContractRolePrivPref(
									requestContractPartyRoleObj,
									foundContractPartyRoleObj);
							handleContractRoleLocation(
									requestContractPartyRoleObj,
									foundContractPartyRoleObj);
						}
						if (foundSameContractRole) {
							break;
						}
					}
					if (!foundSameContractRole) {
						if (foundContractComp != null) {
							requestContractPartyRoleObj
									.setContractComponentId(foundContractComp
											.getContractComponentIdPK());

						}
						handleContractRolePrivPref(requestContractPartyRoleObj,
								null);
						handleContractRoleLocation(requestContractPartyRoleObj,
								null);
					}
				}

			}
		}

		logger.info("Exiting CTCMaintainContractBP.handleContractPartyRole()");
	}

	/**
	 * This method resolve contractRoleLocationBObj identity.
	 * 
	 * @param reqContractPartyRole
	 * @param foundContractPartyRole
	 * @throws Exception
	 * @throws DWLPropertyNotFoundException
	 * @throws ctccompositeexception
	 */
	@SuppressWarnings("unchecked")
	private void handleContractRoleLocation(
			TCRMContractPartyRoleBObj reqContractPartyRole,
			TCRMContractPartyRoleBObj foundContractPartyRole) throws Exception {

		logger
				.info("Entering CTCMaintainContractBP.handleContractRoleLocation()");

		TCRMContractRoleLocationBObj foundContractRoleLocationObj = null;

		boolean foundSameContractRoleLocation = false;

		Vector<TCRMContractRoleLocationBObj> vecReqContractRoleLocation = reqContractPartyRole
				.getItemsTCRMContractRoleLocationBObj();
		Vector<TCRMContractRoleLocationBObj> vecFoundContractRoleLocation = null;

		if (foundContractPartyRole == null) {
			vecFoundContractRoleLocation = new Vector();
		} else {
			vecFoundContractRoleLocation = foundContractPartyRole
					.getItemsTCRMContractRoleLocationBObj();
		}

		if (CTCCommonUtil.isNotEmpty(vecReqContractRoleLocation)) {

			for (TCRMContractRoleLocationBObj requestContractRoleLocationObj : vecReqContractRoleLocation) {

				Iterator it = vecFoundContractRoleLocation.iterator();
				foundSameContractRoleLocation = false;

				while (it.hasNext()) {
					foundContractRoleLocationObj = (TCRMContractRoleLocationBObj) it
							.next();

					// compare business keys
					if (isRoleLocTypeSame(requestContractRoleLocationObj,
							foundContractRoleLocationObj)) {
						foundSameContractRoleLocation = true;

						requestContractRoleLocationObj
								.setContractRoleId(foundContractRoleLocationObj
										.getContractRoleId());
						requestContractRoleLocationObj
								.setContractRoleLocationIdPK(foundContractRoleLocationObj
										.getContractRoleLocationIdPK());
						requestContractRoleLocationObj
								.setContractRoleLocationLastUpdateDate(foundContractRoleLocationObj
										.getContractRoleLocationLastUpdateDate());

						Vector<TCRMContractRoleLocationPurposeBObj> reqRoleLocationPurpose = requestContractRoleLocationObj
								.getItemsTCRMContractRoleLocationPurposeBObj();
						Vector<TCRMContractRoleLocationPurposeBObj> foundRoleLocationPurpose = foundContractRoleLocationObj
								.getItemsTCRMContractRoleLocationPurposeBObj();

						if (CTCCommonUtil.isNotEmpty(reqRoleLocationPurpose)
								&& CTCCommonUtil
										.isNotEmpty(foundRoleLocationPurpose)) {
							TCRMContractRoleLocationPurposeBObj inputRoleLocationPurpose = reqRoleLocationPurpose
									.firstElement();
							TCRMContractRoleLocationPurposeBObj foundRoleLocPurpose = foundRoleLocationPurpose
									.firstElement();

							if (inputRoleLocationPurpose != null
									&& foundRoleLocPurpose != null) {
								inputRoleLocationPurpose
										.setContractRoleLocationId(foundRoleLocPurpose
												.getContractRoleLocationId());
								inputRoleLocationPurpose
										.setContractRoleLocationPurposeIdPK(foundRoleLocPurpose
												.getContractRoleLocationPurposeIdPK());
								inputRoleLocationPurpose
										.setContractRoleLocationPurposeLastUpdateDate(foundRoleLocPurpose
												.getContractRoleLocationPurposeLastUpdateDate());

							}
						}
						if (!StringUtils.compareWithTrim(
								requestContractRoleLocationObj
										.getLocationGroupId(),
								foundContractRoleLocationObj
										.getLocationGroupId()) && !(lstReqLocationGroupIds.contains(foundContractRoleLocationObj.getLocationGroupId())) ) {
							handleLocationGroup(foundContractRoleLocationObj);

						}

					}

					if (foundSameContractRoleLocation)

						break;

				}

				if (!foundSameContractRoleLocation) {
					if (foundContractPartyRole != null) {
						requestContractRoleLocationObj
								.setContractRoleId(foundContractPartyRole
										.getContractRoleIdPK());
					}

				}
			}
		}

		logger
				.info("Exiting CTCMaintainContractBP.handleContractRoleLocation()");

	}

	/**
	 * This method resolves LocationGroup Identity
	 * 
	 * @param foundContractRoleLocationBObj
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void handleLocationGroup(
			TCRMContractRoleLocationBObj foundContractRoleLocationBObj)
			throws Exception {

		TCRMPartyAddressBObj objPartyAddress = foundContractRoleLocationBObj
				.getTCRMPartyAddressBObj();
		TCRMPartyContactMethodBObj objPartyContactMethod = foundContractRoleLocationBObj
				.getTCRMPartyContactMethodBObj();
		String locationGroup = foundContractRoleLocationBObj
				.getLocationGroupId();

		IParty partyComp = (IParty) TCRMClassFactory
				.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);

		if (objPartyContactMethod != null) {
			objPartyContactMethod = partyComp.getPartyContactMethodByIdPK(
					locationGroup, foundContractRoleLocationBObj.getControl());

			if (objPartyContactMethod.getContactMethodUsageType().equals(
					CTCConstants.ALTERNATE_EMAIL_USAGE_TYPE)) {
				QueryConnection connection = null;
				try {

					connection = DataManager.getInstance().getQueryConnection();
					List sqlParam = new java.util.ArrayList(1);
					sqlParam.add(new Long(locationGroup));

					ResultSet objResultSet = connection.queryResults(
							CTCSQLConstants.SEARCH_ACTIVE_ROLELOCATION,
							sqlParam.toArray());
					boolean foundOtherActiveAssoc = false;
					while (objResultSet.next()) {
						String strContractRoleId = objResultSet
								.getString("CONTRACT_ROLE_ID");
						String strEndDate = objResultSet.getString("END_DT");
						if (strEndDate == null
								&& !(StringUtils.compareIgnoreCaseWithTrim(
										strContractRoleId,
										foundContractRoleLocationBObj
												.getContractRoleId()))) {
							foundOtherActiveAssoc = true;
							break;
						}
					}

					if (!foundOtherActiveAssoc) {
						objPartyContactMethod.setEndDate(DWLDateTimeUtilities
								.getCurrentSystemTime());

						// Invoke updatePartyContactMethod
						partyComp
								.updatePartyContactMethod(objPartyContactMethod);

					}

				} catch (Exception objException) {

					throw objException;
				} finally {
					connection.close();
				}
			}
		}
		if (objPartyAddress != null) {

			objPartyAddress = partyComp.getPartyAddressByIdPK(locationGroup,
					foundContractRoleLocationBObj.getControl());

			if (objPartyAddress.getAddressUsageType().equals(
					CTCConstants.ALTERNATE_ADDRESS_USAGE_TYPE)) {

				QueryConnection connection = null;
				try {

					connection = DataManager.getInstance().getQueryConnection();
					List sqlParam = new java.util.ArrayList(1);
					sqlParam.add(new Long(locationGroup));

					ResultSet objResultSet = connection.queryResults(
							CTCSQLConstants.SEARCH_ACTIVE_ROLELOCATION,
							sqlParam.toArray());

					boolean foundOtherActiveAssoc = false;
					while (objResultSet.next()) {
						String strContractRoleId = objResultSet
								.getString("CONTRACT_ROLE_ID");
						String strEndDate = objResultSet.getString("END_DT");
						if (strEndDate == null
								&& !(StringUtils.compareIgnoreCaseWithTrim(
										strContractRoleId,
										foundContractRoleLocationBObj
												.getContractRoleId()))) {
							foundOtherActiveAssoc = true;
							break;
						}
					}
					if (!foundOtherActiveAssoc) {
						objPartyAddress.setEndDate(DWLDateTimeUtilities
								.getCurrentSystemTime());
						partyComp.updatePartyAddress(objPartyAddress);
					}

				} catch (Exception objException) {

					throw objException;
				} finally {
					connection.close();
				}
			} else {
				return;
			}
		} else {
			return;
		}

	}

	/**
	 * This method checks the similarity between the input RoleLocation and
	 * \ound RoleLocation
	 * 
	 * @param inputRoleLocation
	 * @param foundRoleLocation
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private boolean isRoleLocTypeSame(
			TCRMContractRoleLocationBObj inputRoleLocation,
			TCRMContractRoleLocationBObj foundRoleLocation) throws Exception {

		logger.info("Entering CTCMaintainContractBP.isRoleLocTypeSame()");
		if (inputRoleLocation.getTCRMPartyAddressBObj() != null
				&& foundRoleLocation.getTCRMPartyAddressBObj() != null) {
			Vector<TCRMContractRoleLocationPurposeBObj> vecInputRoleLocationPurpose = inputRoleLocation
					.getItemsTCRMContractRoleLocationPurposeBObj();
			Vector<TCRMContractRoleLocationPurposeBObj> vecFoundRoleLocationPurpose = foundRoleLocation
					.getItemsTCRMContractRoleLocationPurposeBObj();
			if (CTCCommonUtil.isNotEmpty(vecFoundRoleLocationPurpose)
					&& CTCCommonUtil.isNotEmpty(vecInputRoleLocationPurpose)) {
				if (vecInputRoleLocationPurpose.elementAt(0).getPurposeType()
						.equals(
								vecFoundRoleLocationPurpose.elementAt(0)
										.getPurposeType())) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		} else if (inputRoleLocation.getTCRMPartyContactMethodBObj() != null
				&& foundRoleLocation.getTCRMPartyContactMethodBObj() != null
				&& StringUtils.compareWithTrim(inputRoleLocation
						.getTCRMPartyContactMethodBObj()
						.getTCRMContactMethodBObj().getContactMethodType(),
						foundRoleLocation.getTCRMPartyContactMethodBObj()
								.getTCRMContactMethodBObj()
								.getContactMethodType())) {
			Vector<TCRMContractRoleLocationPurposeBObj> vecInputRoleLocationPurpose = inputRoleLocation
					.getItemsTCRMContractRoleLocationPurposeBObj();
			Vector<TCRMContractRoleLocationPurposeBObj> vecFoundRoleLocationPurpose = foundRoleLocation
					.getItemsTCRMContractRoleLocationPurposeBObj();
			if (CTCCommonUtil.isNotEmpty(vecFoundRoleLocationPurpose)
					&& CTCCommonUtil.isNotEmpty(vecInputRoleLocationPurpose)) {
				if (vecInputRoleLocationPurpose.elementAt(0).getPurposeType()
						.equals(
								vecFoundRoleLocationPurpose.elementAt(0)
										.getPurposeType())) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}

	}

	/**
	 * This method resolves ContractRolePrivPref identity
	 * 
	 * @param inputContractRole
	 * @param foundContractRole
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public void handleContractRolePrivPref(
			TCRMContractPartyRoleBObj inputContractRole,
			TCRMContractPartyRoleBObj foundContractRole) throws Exception {

		logger
				.info("Entering CTCMaintainContractBP.handleContractRolePrivPref()");

		if (!(inputContractRole instanceof CTCContractRoleBObjExt)) {
			return;
		}

		Vector<CTCContractRolePrivPrefBObj> vecInputCTCContractRolePrivPref = ((CTCContractRoleBObjExt) inputContractRole)
				.getItemsCTCContractRolePrivPrefBObj();
		Vector<CTCContractRolePrivPrefBObj> vecFoundCTCContractRolePrivPref = null;
		if (foundContractRole == null) {
			vecFoundCTCContractRolePrivPref = new Vector();
		} else {
			vecFoundCTCContractRolePrivPref = ((CTCContractRoleBObjExt) foundContractRole)
					.getItemsCTCContractRolePrivPrefBObj();
		}

		for (CTCContractRolePrivPrefBObj inputCTCContractRolePrivPref : vecInputCTCContractRolePrivPref) {
			boolean foundSameContractRolePrivPref = false;
			for (CTCContractRolePrivPrefBObj foundCTCContractRolePrivPref : vecFoundCTCContractRolePrivPref) {
				if (isIdentical(inputCTCContractRolePrivPref,
						foundCTCContractRolePrivPref)) {
					foundSameContractRolePrivPref = true;
					inputCTCContractRolePrivPref
							.setContractRoleId(foundCTCContractRolePrivPref
									.getContractRoleId());
					inputCTCContractRolePrivPref
							.setContractRolePrivPrefIdPK(foundCTCContractRolePrivPref
									.getContractRolePrivPrefIdPK());
					inputCTCContractRolePrivPref
							.setPrivPrefLastUpdateDate(foundCTCContractRolePrivPref
									.getPrivPrefLastUpdateDate());
					inputCTCContractRolePrivPref
							.setEntityPrivPrefLastUpdateDate(foundCTCContractRolePrivPref
									.getEntityPrivPrefLastUpdateDate());
				}
				if (foundSameContractRolePrivPref)
					break;
			}
			if (!foundSameContractRolePrivPref) {
				if (foundContractRole != null) {
					inputContractRole.setContractRoleIdPK(foundContractRole
							.getContractRoleIdPK());
				}
			}

		}

		logger
				.info("Exiting CTCMaintainContractBP.handleContractRolePrivPref()");
	}

	/**
	 * This method refresh the ContractPartyRole Object
	 * 
	 * @param contractRoleId
	 * @param dwlControl
	 * @return
	 * @throws Exception
	 */
	public TCRMContractPartyRoleBObj refreshContractPartyRole(
			String contractRoleId, DWLControl dwlControl) throws Exception {

		TCRMContractPartyRoleBObj retContractPartyRoleBObj = null;

		Vector<String> queryParams = new Vector<String>();
		queryParams.add(contractRoleId);
		DWLTransactionInquiry objTxnInquiry = new DWLTransactionInquiry(
				CTCConstants.TXN_GET_CONTRACT_PARTY_ROLE, queryParams,
				dwlControl);
		DWLResponse dwlResponse = super.processInquiryObject(objTxnInquiry);

		if ((CTCCompositeUtil.checkError(dwlResponse))
				|| (dwlResponse.getData() == null)) {
			DWLStatus status = dwlResponse.getStatus();

			throw new CTCCompositeException(CTCCompositeUtil
					.createResponseByStatus(status));

		} else {
			retContractPartyRoleBObj = (TCRMContractPartyRoleBObj) dwlResponse
					.getData();
			queryParams.add(CTCConstants.FILTER_ACTIVE);
			DWLTransactionInquiry objTxnInquiryRoleLocation = new DWLTransactionInquiry(
					CTCConstants.TXN_GET_ALL_CONTRACT_ROLE_LOCATION,
					queryParams, dwlControl);
			dwlResponse = super.processInquiryObject(objTxnInquiryRoleLocation);

			if ((CTCCompositeUtil.checkError(dwlResponse))
					|| (dwlResponse.getData() == null)) {
				DWLStatus status = dwlResponse.getStatus();

				throw new CTCCompositeException(CTCCompositeUtil
						.createResponseByStatus(status));

			} else {
				Vector<TCRMContractRoleLocationBObj> vecContractRoleLocationBObj = (Vector) dwlResponse
						.getData();

				for (TCRMContractRoleLocationBObj retContractRoleLocationBObj : vecContractRoleLocationBObj) {
					retContractPartyRoleBObj
							.setTCRMContractRoleLocationBObj(retContractRoleLocationBObj);
				}

			}
		}
		return retContractPartyRoleBObj;

	}

}
