package com.ctc.mdm.services.proxy;

import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.constants.CTCErrorConstants;
import com.ctc.mdm.common.util.CTCCommonUtil;
import com.ctc.mdm.services.exception.CTCCompositeException;
import com.ctc.mdm.services.util.CTCCompositeUtil;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.tcrm.coreParty.component.TCRMInactivatedPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.financial.component.TCRMFSPartyBObj;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.unifi.tx.exception.BusinessProxyException;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;

import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;

public class CTCGetFSPartyBP extends CTCCompositeBaseBP {
	
	private final static IDWLLogger logger = DWLLoggerManager.getLogger(CTCGetFSPartyBP.class);
	
	/**
	 * This method is invoked by the execute().
	 * It processes passing transaction object and executes the transaction.
	 *  and get the TCRMPartyLinkBObj from both in-activated PartyBObj and activated PartyBObj.
	 *  Construct response based on TCRMPartyLinkBObj and return the response.
	 * @param transaction
	 * @return
	 * @throws BusinessProxyException
	 * @throws CTCCompositeException
	 */
	
	@Override
	@SuppressWarnings("unchecked")
	public Object executeImpl(DWLTransaction transaction)
			throws BusinessProxyException, CTCCompositeException {
		
		long startTime = System.currentTimeMillis();
		logger.info("CTCGetFSPartyBP:executeImpl-" + CTCConstants.LOG_ENTRY_OF_METHOD);
		
		DWLTransactionInquiry transactionInquiry = (DWLTransactionInquiry)transaction;
		
		DWLControl control  = transactionInquiry.getTxnControl();
		
		Vector inquiryParameterList = transactionInquiry.getStringParameters();
		
		validateInput(inquiryParameterList, null, null, control);
		
		try {
			DWLResponse response = invokeGetPartyWithContractsService(transactionInquiry);
			if(CTCCommonUtil.isSuccessResponse(response)) {
				TCRMFSPartyBObj fsParty = (TCRMFSPartyBObj)response.getData();
				TCRMPartyBObj party = fsParty.getTCRMPartyBObj();
				String partyActiveIndicator = party.getPartyActiveIndicator();
				String partyId = party.getPartyId();
				
				IParty partyComponent = (IParty)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
				//TO add all AdminContEquivBObj in the Response.
				Vector vecAdminContEquivBObj = partyComponent.getAllPartyAdminSysKeys(partyId, party.getControl());
				party.getItemsTCRMAdminContEquivBObj().addAll(vecAdminContEquivBObj);
				
				if(CTCConstants.INACTIVE_PARTY.equalsIgnoreCase(partyActiveIndicator)) {
					TCRMInactivatedPartyBObj inactivatedParty = (TCRMInactivatedPartyBObj)partyComponent.getInactivatedPartyDetail(partyId,control);
					Vector vecPartyLinks = partyComponent.getAllPartyLinks(partyId, control);
					inactivatedParty.getItemsTCRMPartyLinkBObj().addAll(vecPartyLinks);
					party.setTCRMInactivatedPartyBObj(inactivatedParty);
				}
				else {
					Vector vecPartyLinks = partyComponent.getAllPartyLinks(partyId, control);
					party.getItemsTCRMPartyLinkBObj().addAll(vecPartyLinks);
				}
				logger.info("CTCGetFSPartyBP:executeImpl-" + CTCConstants.LOG_EXIT_OF_METHOD);
				long endTime = System.currentTimeMillis();
				logger.info(CTCConstants.TIME_TAKEN + "CTCGetFSPartyBP:executeImpl-" + (endTime-startTime));
				
				return response;
			}
			
			return response;
		} catch(Exception ex) {
			throw new CTCCompositeException(ex.getMessage(),ex);
		}
	}
	
	/**
	 * 
	 * This method modifies transaction object and invokes super business proxy to fire transaction to MDM applicaton
	 * @param transactionInquiry
	 * @return
	 * @throws Exception
	 */
	
	private DWLResponse invokeGetPartyWithContractsService(
			DWLTransactionInquiry transactionInquiry) throws Exception{
		try {
			String originalTxnType = transactionInquiry.getTxnType();
			transactionInquiry.setTxnType(CTCConstants.TXN_GET_PARTY_WITH_CONTRACTS);
			transactionInquiry.getTxnControl().setRequestName(CTCConstants.TXN_GET_PARTY_WITH_CONTRACTS);
			DWLResponse response = (DWLResponse)super.fireTransaction(transactionInquiry);
			transactionInquiry.setTxnType(originalTxnType);
			transactionInquiry.getTxnControl().setRequestName(originalTxnType);
			return response;
		} catch(Exception ex) {
			throw ex;
		}
	}
	
	/**
     * 
     * This method used to Validate the inquiry parameter list provided in the input.
     *  If inquiry parameter list is not valid, construct DWLError Object and throws exception.
     * @param inquiryParameterList
     * @param componentId
     * @param status
     * @throws CTCCompositeException
     */
	@Override
	@SuppressWarnings("unchecked")
	protected void validateInquiryParameters(Vector inquiryParameterList, String componentId, DWLStatus status, DWLControl control) throws CTCCompositeException {
		DWLError error;
		Vector vecDWLError = new Vector();
		if(inquiryParameterList.size() != 3) {
			error = CTCCompositeUtil.getDWLErrorObject(CTCConstants.CTCGETFSPARTY_COMPONENTID, DWLErrorCode.FIELD_VALIDATION_ERROR, CTCErrorConstants.REQUIRED_PARAMETERS_NOT_SUPLIED,control);
			error.setSeverity(DWLStatus.FATAL);
			vecDWLError.add(error);
		}
		if(vecDWLError.size() > 0) {
			status.setStatus(DWLStatus.FATAL);
    		status.setDwlErrorGroup(vecDWLError);
    		throw new CTCCompositeException(CTCCompositeUtil.createResponseByStatus(status));
		}
	}
}
