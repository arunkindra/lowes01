package com.ctc.mdm.services.proxy;


import java.util.Vector;

import com.ctc.mdm.addition.component.CTCContractRolePrivPrefBObj;
import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.services.interfaces.ICTCMaintainService;
import com.dwl.base.DWLCommon;
import com.dwl.base.DWLControl;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.base.util.StringUtils;
import com.dwl.tcrm.common.TCRMResponse;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.financial.component.TCRMAdminNativeKeyBObj;
import com.dwl.tcrm.financial.component.TCRMContractRoleLocationBObj;
import com.dwl.unifi.tx.exception.BusinessProxyException;
import com.dwl.unifi.tx.exception.ITxRxException;


public abstract class CTCMaintainBaseBP extends CTCCompositeBaseBP implements ICTCMaintainService {
	private final static IDWLLogger logger = DWLLoggerManager.getLogger(CTCMaintainBaseBP.class);
	

	/**
	 * This method identifies business keys in two bobj are same or not. It invokes DWLCommon.isBusinessKeySame() to do the comparation.
	 * This method is the place for any customizing code to identify business objects
	 * 
	 * @param incomingBObj
	 * @param existingBObj
	 * @return
	 * @throws DWLBaseException
	 */

	
	public static boolean isIdentical(DWLCommon incomingBObj, DWLCommon existingBObj) throws DWLBaseException{
		boolean isSame = false;
		
		try {
			if(incomingBObj instanceof TCRMAdminNativeKeyBObj)
			{
				if(StringUtils.compareIgnoreCaseWithTrim(((TCRMAdminNativeKeyBObj) incomingBObj).getAdminContractId(),
						((TCRMAdminNativeKeyBObj)existingBObj).getAdminContractId()) && StringUtils.compareIgnoreCaseWithTrim((
								(TCRMAdminNativeKeyBObj) incomingBObj).getAdminFieldNameType(), ((TCRMAdminNativeKeyBObj) existingBObj).getAdminFieldNameType()))
				{
					isSame = true;
				}
			}
			else if(incomingBObj instanceof TCRMContractRoleLocationBObj)
			{
				if(StringUtils.compareIgnoreCaseWithTrim(
						((TCRMContractRoleLocationBObj) incomingBObj).getLocationGroupId(), ((TCRMContractRoleLocationBObj) existingBObj).getLocationGroupId()))
				{
					isSame = true;
				}
				
			}else if (incomingBObj instanceof CTCContractRolePrivPrefBObj)
			{
				if(StringUtils.compareWithTrim(((CTCContractRolePrivPrefBObj) incomingBObj).getPrivPrefType(),((CTCContractRolePrivPrefBObj) existingBObj).getPrivPrefType()))
						{
							isSame = true;
						}
			}
			else
			{
			isSame = incomingBObj.isBusinessKeySame(existingBObj);
			}
		} catch (DWLBaseException e) {	
			throw e;
		} catch (Exception e) {
			throw new DWLBaseException(e.getMessage());
		}
		return isSame;
	}
	
	
	
    


    /**
     * Gets  the party
     * @param partyId
     * @param partyType
     * @param inquiryLevel
     * @param control
     * @return retParty 
     * @throws BusinessProxyException
     */
    public TCRMPartyBObj getPartyInfo(String partyId, String partyType, String inquiryLevel, DWLControl control)
    throws BusinessProxyException {
    	

    	logger.info("CTCMaintainPartyBP:getPartyInfo-" + CTCConstants.LOG_ENTRY_OF_METHOD);
    	
    	
    	TCRMPartyBObj retParty = null;

    	Vector<String> queryParams = new Vector<String>(); // to construct the query parameters

    	queryParams.add(partyId);
    	queryParams.add(partyType);
    	queryParams.add(inquiryLevel);

    	DWLTransactionInquiry objTxnInquiry = new DWLTransactionInquiry(CTCConstants.TXN_GET_PARTY, queryParams, control);

    	TCRMResponse response = (TCRMResponse)super.processInquiryObject(objTxnInquiry);

    	if (response == null || response.getData() == null) {

    		logger.error("No Party object returned from the database" + " for the Party ID: "+partyId+ " with Party type: "+partyType);
    		throw new BusinessProxyException(CTCConstants.TXN_GET_PARTY + " transaction did not retrieve any data");
    	}
    	DWLStatus dwlStatus = response.getStatus();

    	if (dwlStatus.getStatus() != DWLStatus.SUCCESS) {

    		logger.error("Get Party txn returned with Failure Status" + " for the Party ID: "+partyId+ " with Party type: "+partyType);
    		DWLError error = (DWLError) dwlStatus.getDwlErrorGroup().firstElement();
    		throw new BusinessProxyException(error.getErrorMessage());
    	}

    	retParty = (TCRMPartyBObj) response.getData();
    	
    	
    	logger.info("CTCMaintainPartyBP:getPartyInfo-" + CTCConstants.LOG_EXIT_OF_METHOD);
    	return retParty;
    }
 
    
}

