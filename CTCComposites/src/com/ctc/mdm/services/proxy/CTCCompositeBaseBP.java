package com.ctc.mdm.services.proxy;

import java.util.Vector;

import com.ctc.mdm.common.constants.CTCErrorConstants;
import com.ctc.mdm.services.exception.CTCCompositeException;
import com.ctc.mdm.services.util.CTCCompositeUtil;
import com.dwl.base.DWLCommon;
import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTxnBP;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.unifi.tx.exception.BusinessProxyException;
import com.dwl.unifi.tx.exception.ITxRxException;

public abstract class CTCCompositeBaseBP extends DWLTxnBP {

	private final static IDWLLogger logger = DWLLoggerManager.getLogger(CTCCompositeBaseBP.class);


	protected IDWLErrorMessage errHandler;	
	
	/**
	 * 
	 */
	@Override
	public Object execute(Object object) throws BusinessProxyException{
		
		long beginTime = System.currentTimeMillis();
		
		DWLTransaction transaction = (DWLTransaction)object;
        Object responseObj = null;
		try {
			
			initialize();

			preExecute(transaction);
			
			responseObj = executeImpl(transaction);
			
			postExecute(responseObj);
			
		} catch (BusinessProxyException ex) {
			logger.fatal(ex.getLocalizedMessage());
			throw ex;
		} catch (CTCCompositeException ex) {
			logger.fatal(ex.getLocalizedMessage());
			responseObj = ex.getResponseObject();
		} catch (Exception ex) {
			logger.fatal(ex.getLocalizedMessage());
			throw new BusinessProxyException(ex.getMessage());
			
		} finally {
			if (logger.isInfoEnabled()) {
				long endTime = System.currentTimeMillis();
				logger.info("DWLTxnBP : execute : total time in milliseconds "
						+ (endTime - beginTime));
			}
		}

		return responseObj;
	}

	/**
	 * 
	 */
	private void initialize() {
		errHandler = DWLClassFactory.getErrorHandler();
	}
	
	/**
	 * 
	 * @param topLevelObject
	 * @param bObjClass
	 * @param componentId
	 * @param reasonCode
	 * @param control
	 * @throws CTCCompositeException
	 */
    protected void validateInput(Object topLevelObject, Class bObjClass, String componentId, String reasonCode, DWLControl control) throws CTCCompositeException {

    	Vector dwlErrorGroup = null;
        DWLStatus status = null;
        DWLError error = null;
        DWLCommon businessObject = null;

        status = new DWLStatus();
        dwlErrorGroup = status.getDwlErrorGroup();

        if (topLevelObject == null) {
        	error=CTCCompositeUtil.getDWLErrorObject(componentId, DWLErrorCode.DATA_INVALID_ERROR, reasonCode, control);
        	dwlErrorGroup.add(error);

        } else 
        {
            if (!bObjClass.isInstance(topLevelObject)) {
            	error=CTCCompositeUtil.getDWLErrorObject(componentId, DWLErrorCode.DATA_INVALID_ERROR, reasonCode, control);
            	dwlErrorGroup.add(error);

            }
        }

		String clientSystemName = control.getClientSystemName();
		
		if(StringUtils.isBlank(clientSystemName)){
			error = CTCCompositeUtil.getDWLErrorObject(
					componentId,
					DWLErrorCode.DATA_INVALID_ERROR,
					CTCErrorConstants.CLLIENTSYSTEMNAME_NOT_FOUND_ERR_CODE,
					control);

			dwlErrorGroup.add(error);

		}

        if (dwlErrorGroup.size() > 0) {
            status.setStatus(DWLStatus.FATAL);
            throw new CTCCompositeException(CTCCompositeUtil
                    .createResponseByStatus(status));
        }

        businessObject = (DWLCommon)topLevelObject;
        validateStructure(businessObject, componentId, status);

        validateFields(businessObject, componentId, status);

    }

    /**
     * 
     * @param inquiryParameterList
     * @param componentId
     * @param reasonCode
     * @param control
     * @throws CTCCompositeException
     */
    protected void validateInput(Vector inquiryParameterList, String componentId, String reasonCode, DWLControl control) throws CTCCompositeException {

    	Vector dwlErrorGroup = null;
        DWLStatus status = null;
        DWLError error = null;
        DWLCommon businessObject = null;

        status = new DWLStatus();
        dwlErrorGroup = status.getDwlErrorGroup();

        if (inquiryParameterList == null || inquiryParameterList.size() == 0) {
        	error=CTCCompositeUtil.getDWLErrorObject(componentId, DWLErrorCode.DATA_INVALID_ERROR, reasonCode, control);
        	dwlErrorGroup.add(error);

        }
        
        if (dwlErrorGroup.size() > 0) {
            status.setStatus(DWLStatus.FATAL);
            throw new CTCCompositeException(CTCCompositeUtil
                    .createResponseByStatus(status));
        }

        validateInquiryParameters(inquiryParameterList, componentId, status, control);
    }

    /**
     * 
     * @param inquiryParameterList
     * @param componentId
     * @param status
     * @throws CTCCompositeException
     */
    protected void validateInquiryParameters(Vector inquiryParameterList, String componentId, DWLStatus status, DWLControl control) throws CTCCompositeException{
    	
    }
    
	/**
	 * 
	 * @param businessObject
	 * @param componentId
	 * @param status
	 * @throws CTCCompositeException
	 */
    protected void validateFields(DWLCommon businessObject, String componentId, DWLStatus status) throws CTCCompositeException {
		
	}

    /**
     * 
     * @param businessObject
     * @param componentId
     * @param status
     * @throws CTCCompositeException
     */
	protected void validateStructure(DWLCommon businessObject, String componentId, DWLStatus status)  throws CTCCompositeException {
		
	}

	/**
	 * 
	 * @param transaction
	 * @return
	 * @throws BusinessProxyException
	 * @throws CTCCompositeException
	 */
	public abstract Object executeImpl(DWLTransaction transaction) throws BusinessProxyException, CTCCompositeException;

    
    /**
     * This method invokes super business proxy execute method to send transaction to MDM application.
     * 
     * @param transaction
     * @return
     * @throws BusinessProxyException
     */
	protected Object fireTransaction(DWLTransaction transaction) throws BusinessProxyException, CTCCompositeException {		
		return super.execute(transaction);
	}
	
	
	/**
	 * This method would be invoked by MaintainService business proxy and its extension. 
	 * it hosts customized code after business proxy execution.
	 *  
	 * @param object
	 * @throws BusinessProxyException
	 */
	protected void postExecute(Object object) throws BusinessProxyException, CTCCompositeException{
		//add customized code here
	}

	/**
	 * This method would be invoked by MaintainService business proxy and its extension. 
	 * it hosts customized code before business proxy execution.
	 * @param object
	 * @throws BusinessProxyException
	 */
	protected void preExecute(DWLTransaction txnObj) throws BusinessProxyException, CTCCompositeException {
		//add customized code here
	}
	/**
     * This method constructs and return an instance of business proxy that extends IMaintainService interface.
     * 
     * @param bpName
     * @return
     * @throws Exception
     */
    public static CTCMaintainBaseBP getBusProxy(String bpName) throws Exception {

    	CTCMaintainBaseBP aBuxProxy = null;

		try {

			Class finderClass = Class.forName(bpName.trim());
			aBuxProxy = (CTCMaintainBaseBP) finderClass.newInstance();
		} catch (Exception e) {
			String errorMsg = e.getLocalizedMessage();
			ITxRxException ex = new ITxRxException(errorMsg, e);
			throw ex;
		}

		return aBuxProxy;
	}

}
