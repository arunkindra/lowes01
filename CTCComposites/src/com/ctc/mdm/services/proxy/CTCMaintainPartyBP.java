/* _______________________________________________________ {COPYRIGHT-TOP} _____
* Licensed Materials - Property of IBM
*
* 5724-S78
*
* (C) Copyright IBM Corp. 2008, 2009  All Rights Reserved.
*
* US Government Users Restricted Rights - Use, duplication, or
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
* ________________________________________________________ {COPYRIGHT-END} _____*/
package com.ctc.mdm.services.proxy;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.constants.CTCErrorConstants;
import com.ctc.mdm.common.constants.CTCSQLConstants;
import com.ctc.mdm.common.helper.CTCMaintainPartyHelper;
import com.ctc.mdm.common.util.CTCCommonUtil;
import com.ctc.mdm.services.exception.CTCCompositeException;
import com.ctc.mdm.services.util.CTCCompositeUtil;
import com.dwl.base.DWLCommon;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.db.SQLParam;
import com.dwl.base.db.SQLQuery;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionPersistent;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMInactivatedPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyLinkBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyLobRelationshipBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCoreInquiryLevel;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.unifi.tx.exception.BusinessProxyException;

public class CTCMaintainPartyBP extends CTCMaintainBaseBP {

    public static final String copyright = "Licensed Materials -- Property of IBM\n(c) Copyright IBM Corp. 2008, 2009" +
    		"US Government Users Restricted Rights - Use, duplication or disclosure restricted by GSA ADP Schedule Contract with IBM Corp.";

	
	private final static IDWLLogger logger = DWLLoggerManager.getLogger(CTCMaintainPartyBP.class);


	/**
     * The request party object has following structure
     * 					TCRMPartyBObj
     * 	TCRMPersonBObj				  TCRMOrganizationBObj
     * 		TCRMPersonNameBObj			  TCRMOrganizationNameBOBj
     * 		...							  ...
     * 		TCRMAdminContEquivBObj		  TCRMAdminContEquivBObj
     * 
     * <p>
     * 
	 * This method is invoked by the execute().
	 * It processes passing transaction object and executes the transaction.
	 *
     */
	
	
	public Object executeImpl(DWLTransaction transaction) throws BusinessProxyException, CTCCompositeException {
		
		long startTime = System.currentTimeMillis();
		logger.info("CTCMaintainPartyBP:executeImpl-" + CTCConstants.LOG_ENTRY_OF_METHOD);
		
        DWLTransactionPersistent theDWLTxnObj = (DWLTransactionPersistent) transaction;
        Object responseObj = null;
        TCRMPartyBObj foundParty = null;
        
		try {
			
			
			TCRMPartyBObj inputParty =(TCRMPartyBObj) theDWLTxnObj.getTxnTopLevelObject();
				
			/*
			 * 1. Validate the incoming object.
			 * validateInput() invokes the validateStructure() & validateFields() methods.
			 */
			validateInput(inputParty, TCRMPartyBObj.class,
					CTCConstants.CTCMAINTAINPARTY_COMPONENTID,
					CTCErrorConstants.CTCMAINTAINPARTY_NOT_OF_TYPE_TCRMPARTYBOBJ, theDWLTxnObj.getTxnControl());
			
			/*
			 * 2. Find if the input Party already exists based on the 
			 * AdminSystemType & AdminPartyId from the TCRMContEquivBObj
			 */
			foundParty = searchParty((TCRMPartyBObj) theDWLTxnObj.getTxnTopLevelObject());
			
			
			/*
			 * 3. Identify party and its child business objects and modify transaction object.
			 * Prepare the Party object either for add or update transaction
			 */
			resolveIdentity(inputParty, foundParty,true);
			
			/*
			 * 4. Fire transaction to backend
			 * Invoke either the addParty or updateParty transaction.
			 */
			responseObj = fireTransaction(theDWLTxnObj);	
			
			
		}
		catch(Exception exception){
			CTCCompositeUtil.handleException(exception);
		}

		logger.info("CTCMaintainPartyBP:executeImpl-" + CTCConstants.LOG_EXIT_OF_METHOD);
		long endTime = System.currentTimeMillis();
		logger.info(CTCConstants.TIME_TAKEN + "CTCMaintainPartyHelper:executeImpl-" + (endTime-startTime));
		return responseObj;
	}
	
	/**
	 * This method resolves party identity. If passed in party object is not null, It creates update party transaction 
	 * object and maintains its child bobj context. Else it is an add new party transaction and return transaction 
	 * object without doing anything.
	 * 
	 * 
	 *  @param requestPartyBObj
	 *  @param foundPartyBObj
	 *  @param isFineGrainxn
	 *  @throws BusinessProxyException
	 */
	public void resolveIdentity(DWLCommon requestPartyBObj,
			DWLCommon foundPartyBObj, boolean isFineGrainTxn)
			throws BusinessProxyException, CTCCompositeException {
		
		logger.info("CTCMaintainPartyBP:resolveIdentity-" + CTCConstants.LOG_ENTRY_OF_METHOD);
		
		
		TCRMPartyBObj inputParty = (TCRMPartyBObj) requestPartyBObj;
		TCRMPartyBObj dbTCRMPartyBObj = (TCRMPartyBObj) foundPartyBObj;
		try {
			if (foundPartyBObj != null) {
				// UpdateParty
				new CTCMaintainPartyHelper().handleUpdateCustomer(inputParty, dbTCRMPartyBObj, true);
			} else {
				// AddParty
				new CTCMaintainPartyHelper().handleAddCustomer(inputParty);

			}
		}
		catch(Exception exception){
			CTCCompositeUtil.handleException(exception);			
		}
		finally{
			logger.info("CTCMaintainPartyBP:resolveIdentity-" + CTCConstants.LOG_EXIT_OF_METHOD);	
		}
		
		
	}
	
	/**
	 * This method modifies transaction object and invokes super business proxy to fire transaction to MDM applicaton
	 *  @param txnObj
	 *  @throws BusinessProxyException
	 *  @throws CTCCompositeException
	 */
	public Object fireTransaction(DWLTransactionPersistent txnObj) throws BusinessProxyException, CTCCompositeException {
		
		long startTime = System.currentTimeMillis();
		logger.info("CTCMaintainPartyBP:fireTransaction-" + CTCConstants.LOG_ENTRY_OF_METHOD);
		 
		DWLResponse response = null;
		try{
			DWLControl control = txnObj.getTxnControl();
			String strRequestName = control.getRequestName();
			TCRMAdminContEquivBObj objDBContEquiv = null;
			TCRMPartyBObj inputParty = (TCRMPartyBObj)txnObj.getTxnTopLevelObject();
			String inputPartyId = null;
			if (inputParty.getPartyId() != null && inputParty.getPartyLastUpdateDate() != null){
				inputPartyId = inputParty.getPartyId();
				//Get matched DB AdminContEquivBObj for inputParty  
				TCRMAdminContEquivBObj objRequestContEquiv = (TCRMAdminContEquivBObj)inputParty.getItemsTCRMAdminContEquivBObj().firstElement();
				objDBContEquiv = CTCCommonUtil.getMatchedDBAdminContEquiv(inputPartyId,
						objRequestContEquiv,inputParty.getControl());
				//clearing AdminContEquivBObj before Update to avoid Duplicate
				inputParty.getItemsTCRMAdminContEquivBObj().clear();
				txnObj.setTxnType(CTCConstants.TXN_UPDATE_PARTY);
				txnObj.getTxnControl().setRequestName(CTCConstants.TXN_UPDATE_PARTY);
			}
			else{
				txnObj.setTxnType(CTCConstants.TXN_ADD_PARTY);
				txnObj.getTxnControl().setRequestName(CTCConstants.TXN_ADD_PARTY);
			}
			response = (DWLResponse)super.fireTransaction(txnObj);
			IParty partyComponent = (IParty) TCRMClassFactory.getTCRMComponent(
					TCRMCorePropertyKeys.PARTY_COMPONENT);
			
			
			if(CTCCommonUtil.isNotFatalResponse(response)){
				TCRMPartyBObj processedParty = (TCRMPartyBObj)response.getData();
				TCRMPartyBObj finalParty = null;
				TCRMPartyBObj party = partyComponent.getParty(processedParty.getPartyId(), CTCConstants.INQUIRY_LEVEL_1000, control);
				String partyActiveIndicator = party.getPartyActiveIndicator(); 
				
				// To add InactivatedPartyBObj in the Response, if partyActiveIndicator is inactive
				
				if(CTCConstants.INACTIVE_PARTY.equalsIgnoreCase(partyActiveIndicator)) {					
					TCRMInactivatedPartyBObj inactivatedParty = partyComponent.getInactivatedPartyDetail(processedParty.getPartyId(), control);
					Vector<TCRMPartyLinkBObj> vecPartyLinks = partyComponent.getAllPartyLinks(processedParty.getPartyId(), control);
					if(CTCCommonUtil.isNotEmpty(vecPartyLinks)){
						inactivatedParty.getItemsTCRMPartyLinkBObj().addAll(vecPartyLinks);
					}
					// copy all the other party objects from updatePartyResponse to Party
					party.setTCRMInactivatedPartyBObj(inactivatedParty);
	
					party.getItemsTCRMPartyAddressBObj().addAll(processedParty.getItemsTCRMPartyAddressBObj());
					party.getItemsTCRMPartyContactMethodBObj().addAll(processedParty.getItemsTCRMPartyContactMethodBObj());
					party.getItemsTCRMPartyLobRelationshipBObj().addAll(processedParty.getItemsTCRMPartyLobRelationshipBObj());
					
					if(processedParty instanceof TCRMPersonBObj) {
						Vector vecResp = ((TCRMPersonBObj) processedParty).getItemsTCRMPersonNameBObj();
						((TCRMPersonBObj) party).getItemsTCRMPersonNameBObj().addAll(vecResp);
					}
					
					// Assign party to Final Response Party
					finalParty = party;
				}
				else {
					finalParty = processedParty;
				}
				
				if(StringUtils.isNonBlank(inputPartyId)){
					//Set the DB Matched AdminContEquivBObj in the updatePartyResponse
					finalParty.getItemsTCRMAdminContEquivBObj().clear();
					finalParty.getItemsTCRMAdminContEquivBObj().add(objDBContEquiv);
					
					if(CTCCommonUtil.isEmpty(finalParty.getItemsTCRMPartyLobRelationshipBObj())) {
						String strAdminSysType = finalParty.getSourceIdentifierType();
						//Set the DB Matched PartyLobRelationshipBObj in the updatePartyResponse
						TCRMPartyLobRelationshipBObj dbMatchedPartyLobRelationship = CTCCommonUtil.getMatchedDBPartyLobRelationship(inputPartyId, strAdminSysType, control);
						finalParty.getItemsTCRMPartyLobRelationshipBObj().add(dbMatchedPartyLobRelationship);
					}
				}
				
				response.setData(finalParty);
			}
			
			txnObj.getTxnControl().setRequestName(strRequestName);
		}
		catch(Exception ex){
			CTCCompositeUtil.handleException(ex);
		}	
		
		logger.info("CTCMaintainPartyBP:fireTransaction-" + CTCConstants.LOG_EXIT_OF_METHOD);
		long endTime = System.currentTimeMillis();
		logger.info(CTCConstants.TIME_TAKEN + "CTCMaintainPartyHelper:fireTransaction-" + (endTime-startTime));
		
		return response;
		
	}
	
	/**
     * Search the party and ensure detailed information is returned
     * pertaining to that party
     * @param party
     * @return partyBObj
     * @throws BusinessProxyException
     * @throws CTCCompositeException
     */
    private TCRMPartyBObj searchParty(TCRMPartyBObj party) throws BusinessProxyException, CTCCompositeException {
    	
    	long startTime = System.currentTimeMillis();
		logger.info("CTCMaintainPartyBP:searchParty-" + CTCConstants.LOG_ENTRY_OF_METHOD);
    	
		TCRMPartyBObj partyBObj = null;
		DWLControl reqDWLControl=party.getControl();
		
		/*
		 * Get the first TCRMAdminContEquivBObj from the input TCRMPartyBObj
		 */
		TCRMAdminContEquivBObj contEquiv =(TCRMAdminContEquivBObj)party.getItemsTCRMAdminContEquivBObj().firstElement();
		String strAdminSysType = contEquiv.getAdminSystemType();
		String strAdminPartyId = contEquiv.getAdminPartyId();
		String strAdminSysValue;
		String strContIDActiveParty = null;
		int activePartyCount = 0;
        if(StringUtils.isBlank(strAdminSysType)){
        	strAdminSysValue = contEquiv.getAdminSystemValue();
        	try{
        		strAdminSysType = CTCCommonUtil.getCodeType(strAdminSysValue, CTCConstants.CODETABLE_ADMINSYSTEM, reqDWLControl);	
        	}
        	catch(Exception E){
        		throw new CTCCompositeException(E.getMessage(),E);
        	}
        }
		
		boolean multiActiveParty=false;
		try{
			  SQLQuery sqlQuery= new SQLQuery();

              List<SQLParam> params= new ArrayList<SQLParam>();

              params.add(0,new SQLParam(strAdminPartyId));
              params.add(1,new SQLParam(strAdminSysType));
             

              //To execute the SQLQuery to find out the active parties using AdminPartyId,AdminSysType and a null INACTIVATED_DT 
              ResultSet objResultSet = sqlQuery
					.executeQuery(
							CTCSQLConstants.SQL_QUERY_TO_FIND_ACTIVE_PARTY_WTTH_ADMIN_CLIENT_ID_AND_ADMIN_SYS_TP_CD,
							params);
              
              while (objResultSet.next()) {
            	  //more than ONE active parties returned than break from while Loop and set flag "multiActiveParty" to true
            	  if(activePartyCount >= 1){
            		  multiActiveParty=true;
            		  break;
            	  }
            	  strContIDActiveParty = objResultSet.getString(CTCConstants.CONT_ID);
            	  activePartyCount++;
            	  
              }
              
              if(multiActiveParty){
            	
            	  DWLError error = CTCCompositeUtil
						.getDWLErrorObject(
								CTCConstants.CTCMAINTAINPARTY_COMPONENTID,
								DWLErrorCode.DATA_INVALID_ERROR,
								CTCErrorConstants.MULTIPLE_ACTIVE_PARTIES_FOUND_ERROR_CODE,
								reqDWLControl);
            	  DWLStatus status= new DWLStatus();
            	  status.getDwlErrorGroup().add(error);
            	  throw new CTCCompositeException(CTCCompositeUtil
                          .createResponseByStatus(status));
      		  }
              else if(activePartyCount==0){
            	  partyBObj= null;
              }
              else if(strContIDActiveParty!= null){
            	partyBObj = getPartyInfo(strContIDActiveParty, CTCConstants.PARTY_TYPE_PERSON, TCRMCoreInquiryLevel.INQUIRY_LEVEL_1, reqDWLControl);
      			logger.info(CTCConstants.SEARCH_COMPLETED_MSSG);
      		}
		}
		catch(Exception exception){
			CTCCompositeUtil.handleException(exception);
		}
		
		logger.info("CTCMaintainPartyBP:searchParty-" + CTCConstants.LOG_EXIT_OF_METHOD);
		long endTime = System.currentTimeMillis();
		logger.info(CTCConstants.TIME_TAKEN + "CTCMaintainPartyHelper:searchParty-" + (endTime-startTime));
		return partyBObj;
    }


    /**
     * Validates fields AdminPartyID and AdminSystemType
     * @param businessObject
     * @param componentId
     * @param status
     * @throws CTCCompositeException
     */
    protected void validateFields(DWLCommon businessObject, String componentId, DWLStatus status) throws CTCCompositeException{
    	
    	logger.info("CTCMaintainPartyBP:validateFields-" + CTCConstants.LOG_ENTRY_OF_METHOD);
    	
    	TCRMPartyBObj inputParty = (TCRMPartyBObj)businessObject;
    	Vector vecDWLError = new Vector();
    
    	Vector vecAdminContEquiv = inputParty.getItemsTCRMAdminContEquivBObj();

    	/*
    	 * To validate the first tcrAdminContEquivBObj of the vecAdminContEquiv Vector
    	 */
    	TCRMAdminContEquivBObj tcrAdminContEquivBObj = (TCRMAdminContEquivBObj)vecAdminContEquiv.firstElement();
    	/*
    	 * If AdminPartyId in input is null/empty	
    	 */
    	if(StringUtils.isBlank(tcrAdminContEquivBObj.getAdminPartyId())){
    			DWLError dwlError =CTCCompositeUtil
				.getDWLErrorObject(
						CTCConstants.CTCMAINTAINPARTY_COMPONENTID,
						DWLErrorCode.DATA_INVALID_ERROR,
						CTCErrorConstants.ADMINPARTYID_MISSING_ERROR_CODE,
						inputParty.getControl()); 
    				
    			dwlError.setSeverity(DWLStatus.FATAL);
    			vecDWLError.add(dwlError);
    		}
    	/*
    	 * If AdminSystemType in input is null/empty	
    	 */
    	if(StringUtils.isBlank(tcrAdminContEquivBObj.getAdminSystemType()) && 
    			StringUtils.isBlank(tcrAdminContEquivBObj.getAdminSystemValue())){
    			DWLError dwlError = CTCCompositeUtil
				.getDWLErrorObject(
						CTCConstants.CTCMAINTAINPARTY_COMPONENTID,
						DWLErrorCode.DATA_INVALID_ERROR,
						CTCErrorConstants.ADMINSYSTEMTYPE_AND_ADMINSYSTEMVALUE_MISSING_ERROR_CODE,
						inputParty.getControl()); 
    				
    			dwlError.setSeverity(DWLStatus.FATAL); 
    			vecDWLError.add(dwlError);
    		}
    	else{
    		try {
    			if(StringUtils.isNonBlank(tcrAdminContEquivBObj.getAdminSystemValue())) {
    				tcrAdminContEquivBObj.setAdminSystemType(CTCCommonUtil.getCodeType(
    						tcrAdminContEquivBObj.getAdminSystemValue(), CTCConstants.CD_ADMIN_SYS_TP, inputParty.getControl()));
    				if(StringUtils.isBlank(tcrAdminContEquivBObj.getAdminSystemType())) {
    					DWLError dwlError = CTCCompositeUtil
    					.getDWLErrorObject(
    							CTCConstants.CTCMAINTAINPARTY_COMPONENTID,
    							DWLErrorCode.DATA_INVALID_ERROR,
    							CTCErrorConstants.ADMINSYSTEMTYPE_AND_ADMINSYSTEMVALUE_IS_NOT_VALID_ERROR_CODE,
    							inputParty.getControl()); 
    	    				
    	    			dwlError.setSeverity(DWLStatus.FATAL); 
    	    			vecDWLError.add(dwlError);
    				}
    			}else {
    				tcrAdminContEquivBObj.setAdminSystemValue(CTCCommonUtil.getCodeValue(
    						tcrAdminContEquivBObj.getAdminSystemType(), CTCConstants.CD_ADMIN_SYS_TP, inputParty.getControl()));
    				if(StringUtils.isBlank(tcrAdminContEquivBObj.getAdminSystemValue())) {
    					DWLError dwlError = CTCCompositeUtil
    					.getDWLErrorObject(
    							CTCConstants.CTCMAINTAINPARTY_COMPONENTID,
    							DWLErrorCode.DATA_INVALID_ERROR,
    							CTCErrorConstants.ADMINSYSTEMTYPE_AND_ADMINSYSTEMVALUE_IS_NOT_VALID_ERROR_CODE,
    							inputParty.getControl()); 
    	    				
    	    			dwlError.setSeverity(DWLStatus.FATAL); 
    	    			vecDWLError.add(dwlError);
    				}
    			}
    		}
    		catch(CTCCompositeException e){
    			throw e;
    		}
    		catch(Exception e) {
    			throw new CTCCompositeException(e.getMessage(),e);
    		}
    	}
    	if(vecDWLError.size()>0){
    		
    		status.setStatus(DWLStatus.FATAL);
    		status.setDwlErrorGroup(vecDWLError);
    		throw new CTCCompositeException(CTCCompositeUtil.createResponseByStatus(status));
    	}
    	
    	logger.info("CTCMaintainPartyBP:validateFields-" + CTCConstants.LOG_EXIT_OF_METHOD);
    	
    	}
    	
    
    /**
     * Validates Structure 
     * @param businessObject
     * @param componentId
     * @param status
     * @throws CTCCompositeException
     */
    protected void validateStructure(DWLCommon businessObject, String componentId, DWLStatus status)  throws CTCCompositeException{
    	
    	logger.info("CTCMaintainPartyBP:validateStructure-" + CTCConstants.LOG_ENTRY_OF_METHOD);

    	
    	TCRMPartyBObj inputParty = (TCRMPartyBObj)businessObject;
    	Vector vecDWLError = new Vector();
    	Vector vecAdminContEquiv = inputParty.getItemsTCRMAdminContEquivBObj();
    /*
     * if vecAdminContEquiv is null or empty, Construct DWL ERROR
     */
    	if(CTCCommonUtil.isEmpty(vecAdminContEquiv)){
    		
    		DWLError dwlError = CTCCompositeUtil
			.getDWLErrorObject(
					CTCConstants.CTCMAINTAINPARTY_COMPONENTID,
					DWLErrorCode.DATA_INVALID_ERROR,
					CTCErrorConstants.ADMINCONTEQUIVOBJ_MISSING_ERROR_CODE	,
					inputParty.getControl()); 
				
			dwlError.setSeverity(DWLStatus.FATAL); 
    		vecDWLError.add(dwlError);
    		
    	}
    	if(vecDWLError.size()>0){
    		status.setStatus(DWLStatus.FATAL);
    		status.setDwlErrorGroup(vecDWLError);
    		throw new CTCCompositeException(CTCCompositeUtil.createResponseByStatus(status));
    	}
    	
    	logger.info("CTCMaintainPartyBP:validateStructure-" + CTCConstants.LOG_EXIT_OF_METHOD);
 
    	
    }
   
  
}
