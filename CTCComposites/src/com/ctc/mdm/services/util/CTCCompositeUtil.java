package com.ctc.mdm.services.util;

import com.ctc.mdm.services.exception.CTCCompositeException;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.tcrm.financial.component.TCRMAdminNativeKeyBObj;
import com.dwl.unifi.tx.exception.BusinessProxyException;

public class CTCCompositeUtil {

	/**
	 * <p>createFatalResponse</p>
	 *
	 * @param componentId a {@link java.lang.String} object.
	 * @param errorCode a {@link java.lang.String} object.
	 * @param errorReasonCode a {@link java.lang.String} object.
	 * @param control a {@link com.dwl.base.DWLControl} object.
	 * @return DWLResponse
	 */
	public static DWLResponse createFatalResponse(String componentId, String errorCode, String errorReasonCode, DWLControl control) {
		return createFatalResponse(componentId, errorCode, errorReasonCode, control,null);
	}

    /**
     * <p>createFatalResponse</p>
     *
     * @param componentId a {@link java.lang.String} object.
     * @param errorCode a {@link java.lang.String} object.
     * @param errorReasonCode a {@link java.lang.String} object.
     * @param control a {@link com.dwl.base.DWLControl} object.
     * @return DWLResponse
     * @param params an array of {@link java.lang.String} objects.
     */
    public static DWLResponse createFatalResponse(String componentId,String errorCode, String errorReasonCode, DWLControl control, String[] params) {
        DWLStatus status = new DWLStatus();

        IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
        DWLError error = errHandler.getErrorMessage(componentId, errorCode,
                errorReasonCode, control, params);
        status.addError(error);
        status.setStatus(DWLStatus.FATAL);

        DWLResponse fatalResponse = new DWLResponse();
        fatalResponse.setStatus(status);

        return fatalResponse;
    }
	
	/**
	 * This method retrieves the DWLError Object and returns it.
	 *
	 * @param	componentId		The Component Id of the Class
	 * @param	errType			The Error Type Code
	 * @param	errReasonCode	The Error Reason Code
	 * @param	control			The DWLControl object
	 * @return	DWLError		The DWLError object retrieved from the DataBase.
	 */
	public static DWLError getDWLErrorObject(String componentId, String errType,String errReasonCode, DWLControl control){
		
        IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
        DWLError error = errHandler.getErrorMessage(componentId, errType,   errReasonCode, control, new String[0]);
        return error;
    }

    public static DWLResponse createResponseByStatus(DWLStatus status) {
        DWLResponse response = new DWLResponse();
        response.setStatus(status);

        return response;
    }

    /**
    * This method checks DWLError in DWLStatus.
    *
    * @param status
    * @return
    */
    public static boolean checkError (DWLStatus status) {
    boolean hasError = false;
	
	    if(status == null) {
	    	return hasError;
	    }
	
	    if(status.getStatus() == DWLStatus.FATAL)
	    {
	    	hasError = true;
	    }
	    return hasError;
    }


    /**
    * This method checks DWLError in Response Object.
    *
    * @param status
    * @return
    */
    public static boolean checkError (DWLResponse response) {


	boolean hasError = false;
	
	    if(response == null) {
	    return hasError;
	    }
	
	    DWLStatus status = response.getStatus();
	
	    if(status == null) {
	    return hasError;
	    }
	
	    if(status.getStatus() == DWLStatus.FATAL)
	    {
	    hasError = true;
	    }
	    return hasError;
    }

  

	/**
	 * 
	 * @param exception
	 * @throws BusinessProxyException
	 * @throws CTCCompositeException
	 */
	public static void handleException(Exception exception) throws BusinessProxyException, CTCCompositeException{
		if(exception instanceof BusinessProxyException){
			throw (BusinessProxyException)exception;
		}
		else if(exception instanceof CTCCompositeException){
			throw (CTCCompositeException)exception;
		}
		else if(exception instanceof DWLBaseException) {
			//TODO Get the throwable
			DWLBaseException dwlBaseException = (DWLBaseException)exception;
			DWLStatus fatalStatus = dwlBaseException.getStatus();
			CTCCompositeException ctcCompositeException = new CTCCompositeException(CTCCompositeUtil.createResponseByStatus(fatalStatus));
			throw ctcCompositeException;
		}
		else
		{
			throw new BusinessProxyException(exception);
		}

	}
	
	/**
	 * @param inputContract
	 * @return TCRMAdminNativeKeyBObj
	 */
	
	@SuppressWarnings("unchecked")
	public static TCRMAdminNativeKeyBObj constructAdminNativeKey(String adminContractId, String adminNativeKeyType, DWLControl control)
	{
		TCRMAdminNativeKeyBObj tcrmAdminNativeKeyBobj = new TCRMAdminNativeKeyBObj();

		tcrmAdminNativeKeyBobj.setAdminContractId(adminContractId);
		tcrmAdminNativeKeyBobj.setAdminFieldNameType(adminNativeKeyType);
		tcrmAdminNativeKeyBobj.setControl(control);
	 	return tcrmAdminNativeKeyBobj;
	}

    
}
