package com.ctc.mdm.services.exception;

import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLResponseException;

public class CTCCompositeException extends DWLResponseException {

    private DWLResponse responseObject = null;
    
    
    /**
     * <p>Constructor for CBCompositeException.</p>
     */
    public CTCCompositeException() {
        super();
    }
    /**
     * <p>Constructor for CTCCompositeException.</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public CTCCompositeException(String message) {
        super(message);
    }
    /**
     * <p>Constructor for CTCCompositeException.</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param cause a {@link java.lang.Exception} object.
     */
    public CTCCompositeException(String message, Exception cause) {
        super(message);
        
    }
    /**
     * <p>Constructor for CTCCompositeException.</p>
     *
     * @param cause a {@link java.lang.Exception} object.
     */
    public CTCCompositeException(Exception cause) {
        super("Exception occurred",cause);
    }
    
    /**
     * Constructor for CTCCompositeException.
     *
     * @param response DWLResponse
     */
    public CTCCompositeException(DWLResponse response) {
        super();
        this.responseObject = response;
    }
    
    /**
     * <p>Getter for the field <code>responseObject</code>.</p>
     *
     * @return Returns the responseObject.
     */
    public DWLResponse getResponseObject() {
        return responseObject;
    }
    /**
     * <p>Setter for the field <code>responseObject</code>.</p>
     *
     * @param responseObject The responseObject to set.
     */
    public void setResponseObject(DWLResponse responseObject) {
        this.responseObject = responseObject;
    }
	
}
