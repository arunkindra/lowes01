
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[22f43bc079b2d1633c745322ce869b49]
 */

package com.ctc.mdm.addition.component;

import com.dwl.tcrm.common.TCRMCommon;
import com.dwl.tcrm.common.TCRMErrorCode;



import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;

import com.ctc.mdm.addition.entityObject.EObjCTCPartySegment;

import com.ctc.mdm.addition.interfaces.CTCPartySegment;
import com.ctc.mdm.addition.util.CTCAdditionUtil;
import com.dwl.base.DWLCommon;
import com.dwl.base.DWLControl;

import com.dwl.base.constant.DWLUtilErrorReasonCode;

import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;

import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.exception.DWLUpdateException;

import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.base.util.DWLExceptionUtils;
import com.dwl.base.util.DWLFunctionUtils;

import com.dwl.management.config.client.Configuration;

import com.dwl.tcrm.common.ITCRMValidation;

import com.dwl.tcrm.utilities.DateFormatter;
import com.dwl.tcrm.utilities.DateValidator;
import com.dwl.tcrm.utilities.TCRMClassFactory;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This class provides the implementation of the business object
 * <code>CTCPartySegmentBObj</code>.
 * 
 * @see com.dwl.tcrm.common.TCRMCommon
 * @generated
 */
 

@SuppressWarnings("serial")
public class CTCPartySegmentBObj extends TCRMCommon  {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    protected EObjCTCPartySegment eObjCTCPartySegment;
	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCPartySegmentBObj.class);
		
 
	protected boolean isValidEffectiveStartDt = true;
	
	protected boolean isValidEffectiveEndDt = true;
	


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */     
    public CTCPartySegmentBObj() {
        super();
        init();
        eObjCTCPartySegment = new EObjCTCPartySegment();
        setComponentID(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ);
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Initializes the fields required to populate the metaDataMap. Each key is
     * an element-level field of the business object.
     *
     * @generated
     */
    private void init() {
        metaDataMap.put("PartySegmentId", null);
        metaDataMap.put("ContId", null);
        metaDataMap.put("SegmentProvider", null);
        metaDataMap.put("BusinessGroupName", null);
        metaDataMap.put("SegmentTypeName", null);
        metaDataMap.put("SegmentValue", null);
        metaDataMap.put("SegmentCategoryType", null);
        metaDataMap.put("EffectiveStartDt", null);
        metaDataMap.put("EffectiveEndDt", null);
        metaDataMap.put("CTCPartySegmentHistActionCode", null);
        metaDataMap.put("CTCPartySegmentHistCreateDate", null);
        metaDataMap.put("CTCPartySegmentHistCreatedBy", null);
        metaDataMap.put("CTCPartySegmentHistEndDate", null);
        metaDataMap.put("CTCPartySegmentHistoryIdPK", null);
        metaDataMap.put("CTCPartySegmentLastUpdateDate", null);
        metaDataMap.put("CTCPartySegmentLastUpdateTxId", null);
        metaDataMap.put("CTCPartySegmentLastUpdateUser", null);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Refreshes all the attributes this business object supports.
     *
     * @see com.dwl.base.DWLCommon#refreshMap()
     * @generated
     */
    public void refreshMap() {

        if (bRequireMapRefresh) {
            super.refreshMap();
            metaDataMap.put("PartySegmentId", getPartySegmentId());
            metaDataMap.put("ContId", getContId());
            metaDataMap.put("SegmentProvider", getSegmentProvider());
            metaDataMap.put("BusinessGroupName", getBusinessGroupName());
            metaDataMap.put("SegmentTypeName", getSegmentTypeName());
            metaDataMap.put("SegmentValue", getSegmentValue());
            metaDataMap.put("SegmentCategoryType", getSegmentCategoryType());
            metaDataMap.put("EffectiveStartDt", getEffectiveStartDt());
            metaDataMap.put("EffectiveEndDt", getEffectiveEndDt());
            metaDataMap.put("CTCPartySegmentHistActionCode", getCTCPartySegmentHistActionCode());
            metaDataMap.put("CTCPartySegmentHistCreateDate", getCTCPartySegmentHistCreateDate());
            metaDataMap.put("CTCPartySegmentHistCreatedBy", getCTCPartySegmentHistCreatedBy());
            metaDataMap.put("CTCPartySegmentHistEndDate", getCTCPartySegmentHistEndDate());
            metaDataMap.put("CTCPartySegmentHistoryIdPK", getCTCPartySegmentHistoryIdPK());
            metaDataMap.put("CTCPartySegmentLastUpdateDate", getCTCPartySegmentLastUpdateDate());
            metaDataMap.put("CTCPartySegmentLastUpdateTxId", getCTCPartySegmentLastUpdateTxId());
            metaDataMap.put("CTCPartySegmentLastUpdateUser", getCTCPartySegmentLastUpdateUser());
            bRequireMapRefresh = false;
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the control object on this business object.
     *
     * @see com.dwl.base.DWLCommon#setControl(DWLControl)
     * @generated
     */
    public void setControl(DWLControl newDWLControl) {
        super.setControl(newDWLControl);

        if (eObjCTCPartySegment != null) {
            eObjCTCPartySegment.setControl(newDWLControl);
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the entity object associated with this business object.
     *
     * @generated
     */
    public EObjCTCPartySegment getEObjCTCPartySegment() {
        bRequireMapRefresh = true;
        return eObjCTCPartySegment;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the entity object associated with this business object.
     *
     * @param eObjCTCPartySegment
     *            The eObjCTCPartySegment to set.
     * @generated
     */
    public void setEObjCTCPartySegment(EObjCTCPartySegment eObjCTCPartySegment) {
        bRequireMapRefresh = true;
        this.eObjCTCPartySegment = eObjCTCPartySegment;
        if (this.eObjCTCPartySegment != null && this.eObjCTCPartySegment.getControl() == null) {
            DWLControl control = this.getControl();
            if (control != null) {
                this.eObjCTCPartySegment.setControl(control);
            }
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the partySegmentId attribute.
     * 
     * @generated
     */
    public String getPartySegmentId (){
   
        return DWLFunctionUtils.getStringFromLong(eObjCTCPartySegment.getPartySegmentId());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the partySegmentId attribute.
     * 
     * @param newPartySegmentId
     *     The new value of partySegmentId.
     * @generated
     */
    public void setPartySegmentId( String newPartySegmentId ) throws Exception {
        metaDataMap.put("PartySegmentId", newPartySegmentId);

        if (newPartySegmentId == null || newPartySegmentId.equals("")) {
            newPartySegmentId = null;


        }
        eObjCTCPartySegment.setPartySegmentId( DWLFunctionUtils.getLongFromString(newPartySegmentId) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the contId attribute.
     * 
     * @generated
     */
    public String getContId (){
   
        return DWLFunctionUtils.getStringFromLong(eObjCTCPartySegment.getContId());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the contId attribute.
     * 
     * @param newContId
     *     The new value of contId.
     * @generated
     */
    public void setContId( String newContId ) throws Exception {
        metaDataMap.put("ContId", newContId);

        if (newContId == null || newContId.equals("")) {
            newContId = null;


        }
        eObjCTCPartySegment.setContId( DWLFunctionUtils.getLongFromString(newContId) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the segmentProvider attribute.
     * 
     * @generated
     */
    public String getSegmentProvider (){
   
        return eObjCTCPartySegment.getSegmentProvider();
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the segmentProvider attribute.
     * 
     * @param newSegmentProvider
     *     The new value of segmentProvider.
     * @generated
     */
    public void setSegmentProvider( String newSegmentProvider ) throws Exception {
        metaDataMap.put("SegmentProvider", newSegmentProvider);

        if (newSegmentProvider == null || newSegmentProvider.equals("")) {
            newSegmentProvider = null;


        }
        eObjCTCPartySegment.setSegmentProvider( newSegmentProvider );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the businessGroupName attribute.
     * 
     * @generated
     */
    public String getBusinessGroupName (){
   
        return eObjCTCPartySegment.getBusinessGroupName();
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the businessGroupName attribute.
     * 
     * @param newBusinessGroupName
     *     The new value of businessGroupName.
     * @generated
     */
    public void setBusinessGroupName( String newBusinessGroupName ) throws Exception {
        metaDataMap.put("BusinessGroupName", newBusinessGroupName);

        if (newBusinessGroupName == null || newBusinessGroupName.equals("")) {
            newBusinessGroupName = null;


        }
        eObjCTCPartySegment.setBusinessGroupName( newBusinessGroupName );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the segmentTypeName attribute.
     * 
     * @generated
     */
    public String getSegmentTypeName (){
   
        return eObjCTCPartySegment.getSegmentTypeName();
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the segmentTypeName attribute.
     * 
     * @param newSegmentTypeName
     *     The new value of segmentTypeName.
     * @generated
     */
    public void setSegmentTypeName( String newSegmentTypeName ) throws Exception {
        metaDataMap.put("SegmentTypeName", newSegmentTypeName);

        if (newSegmentTypeName == null || newSegmentTypeName.equals("")) {
            newSegmentTypeName = null;


        }
        eObjCTCPartySegment.setSegmentTypeName( newSegmentTypeName );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the segmentValue attribute.
     * 
     * @generated
     */
    public String getSegmentValue (){
   
        return eObjCTCPartySegment.getSegmentValue();
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the segmentValue attribute.
     * 
     * @param newSegmentValue
     *     The new value of segmentValue.
     * @generated
     */
    public void setSegmentValue( String newSegmentValue ) throws Exception {
        metaDataMap.put("SegmentValue", newSegmentValue);

        if (newSegmentValue == null || newSegmentValue.equals("")) {
            newSegmentValue = null;


        }
        eObjCTCPartySegment.setSegmentValue( newSegmentValue );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the segmentCategoryType attribute.
     * 
     * @generated
     */
    public String getSegmentCategoryType (){
   
        return eObjCTCPartySegment.getSegmentCategoryType();
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the segmentCategoryType attribute.
     * 
     * @param newSegmentCategoryType
     *     The new value of segmentCategoryType.
     * @generated
     */
    public void setSegmentCategoryType( String newSegmentCategoryType ) throws Exception {
        metaDataMap.put("SegmentCategoryType", newSegmentCategoryType);

        if (newSegmentCategoryType == null || newSegmentCategoryType.equals("")) {
            newSegmentCategoryType = null;


        }
        eObjCTCPartySegment.setSegmentCategoryType( newSegmentCategoryType );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the effectiveStartDt attribute.
     * 
     * @generated
     */
    public String getEffectiveStartDt (){
   
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCPartySegment.getEffectiveStartDt());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the effectiveStartDt attribute.
     * 
     * @param newEffectiveStartDt
     *     The new value of effectiveStartDt.
     * @generated
     */
    public void setEffectiveStartDt( String newEffectiveStartDt ) throws Exception {
        metaDataMap.put("EffectiveStartDt", newEffectiveStartDt);
       	isValidEffectiveStartDt = true;

        if (newEffectiveStartDt == null || newEffectiveStartDt.equals("")) {
            newEffectiveStartDt = null;
            eObjCTCPartySegment.setEffectiveStartDt(null);


        }
		else {
        	if (DateValidator.validates(newEffectiveStartDt)) {
           		eObjCTCPartySegment.setEffectiveStartDt(DateFormatter.getStartDateTimestamp(newEffectiveStartDt));
            	metaDataMap.put("EffectiveStartDt", getEffectiveStartDt());
        	} else {
            	if (Configuration.getConfiguration().getConfigItem(
							"/IBM/DWLCommonServices/InternalValidation/enabled").getBooleanValue()) {
                	if (metaDataMap.get("EffectiveStartDt") != null) {
                    	metaDataMap.put("EffectiveStartDt", "");
                	}
                	isValidEffectiveStartDt = false;
                	eObjCTCPartySegment.setEffectiveStartDt(null);
            	}
        	}
        }
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the effectiveEndDt attribute.
     * 
     * @generated
     */
    public String getEffectiveEndDt (){
   
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCPartySegment.getEffectiveEndDt());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the effectiveEndDt attribute.
     * 
     * @param newEffectiveEndDt
     *     The new value of effectiveEndDt.
     * @generated
     */
    public void setEffectiveEndDt( String newEffectiveEndDt ) throws Exception {
        metaDataMap.put("EffectiveEndDt", newEffectiveEndDt);
       	isValidEffectiveEndDt = true;

        if (newEffectiveEndDt == null || newEffectiveEndDt.equals("")) {
            newEffectiveEndDt = null;
            eObjCTCPartySegment.setEffectiveEndDt(null);


        }
		else {
        	if (DateValidator.validates(newEffectiveEndDt)) {
           		eObjCTCPartySegment.setEffectiveEndDt(DateFormatter.getStartDateTimestamp(newEffectiveEndDt));
            	metaDataMap.put("EffectiveEndDt", getEffectiveEndDt());
        	} else {
            	if (Configuration.getConfiguration().getConfigItem(
							"/IBM/DWLCommonServices/InternalValidation/enabled").getBooleanValue()) {
                	if (metaDataMap.get("EffectiveEndDt") != null) {
                    	metaDataMap.put("EffectiveEndDt", "");
                	}
                	isValidEffectiveEndDt = false;
                	eObjCTCPartySegment.setEffectiveEndDt(null);
            	}
        	}
        }
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateTxId attribute.
     *
     * @generated
     */
    public String getCTCPartySegmentLastUpdateTxId() {
        return DWLFunctionUtils.getStringFromLong(eObjCTCPartySegment.getLastUpdateTxId());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateUser attribute.
     *
     * @generated
     */
    public String getCTCPartySegmentLastUpdateUser() {
        return eObjCTCPartySegment.getLastUpdateUser();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateDt attribute.
     *
     * @generated
     */
    public String getCTCPartySegmentLastUpdateDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCPartySegment.getLastUpdateDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateTxId attribute.
     *
     * @param newLastUpdateTxId
     *     The new value of LastUpdateTxId.
     * @generated
     */
    public void setCTCPartySegmentLastUpdateTxId(String newLastUpdateTxId) {
        metaDataMap.put("CTCPartySegmentLastUpdateTxId", newLastUpdateTxId);

        if ((newLastUpdateTxId == null) || newLastUpdateTxId.equals("")) {
            newLastUpdateTxId = null;
        }
        eObjCTCPartySegment.setLastUpdateTxId(DWLFunctionUtils.getLongFromString(newLastUpdateTxId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateUser attribute.
     *
     * @param newLastUpdateUser
     *     The new value of LastUpdateUser.
     * @generated
     */
    public void setCTCPartySegmentLastUpdateUser(String newLastUpdateUser) {
        metaDataMap.put("CTCPartySegmentLastUpdateUser", newLastUpdateUser);

        if ((newLastUpdateUser == null) || newLastUpdateUser.equals("")) {
            newLastUpdateUser = null;
        }
        eObjCTCPartySegment.setLastUpdateUser(newLastUpdateUser);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateDt attribute.
     *
     * @param newLastUpdateDt
     *     The new value of LastUpdateDt.
     * @throws Exception
     * @generated
     */
    public void setCTCPartySegmentLastUpdateDate(String newLastUpdateDt) throws Exception {
        metaDataMap.put("CTCPartySegmentLastUpdateDate", newLastUpdateDt);

        if ((newLastUpdateDt == null) || newLastUpdateDt.equals("")) {
            newLastUpdateDt = null;
        }

        eObjCTCPartySegment.setLastUpdateDt(DWLFunctionUtils.getTimestampFromTimestampString(newLastUpdateDt));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCPartySegmentHistActionCode history attribute.
     *
     * @generated
     */
    public String getCTCPartySegmentHistActionCode() {
        return eObjCTCPartySegment.getHistActionCode();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCPartySegmentHistActionCode history attribute.
     *
     * @param aCTCPartySegmentHistActionCode
     *     The new value of CTCPartySegmentHistActionCode.
     * @generated
     */
    public void setCTCPartySegmentHistActionCode(String aCTCPartySegmentHistActionCode) {
        metaDataMap.put("CTCPartySegmentHistActionCode", aCTCPartySegmentHistActionCode);

        if ((aCTCPartySegmentHistActionCode == null) || aCTCPartySegmentHistActionCode.equals("")) {
            aCTCPartySegmentHistActionCode = null;
        }
        eObjCTCPartySegment.setHistActionCode(aCTCPartySegmentHistActionCode);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCPartySegmentHistCreateDate history attribute.
     *
     * @generated
     */
    public String getCTCPartySegmentHistCreateDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCPartySegment.getHistCreateDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCPartySegmentHistCreateDate history attribute.
     *
     * @param aCTCPartySegmentHistCreateDate
     *     The new value of CTCPartySegmentHistCreateDate.
     * @generated
     */
    public void setCTCPartySegmentHistCreateDate(String aCTCPartySegmentHistCreateDate) throws Exception{
        metaDataMap.put("CTCPartySegmentHistCreateDate", aCTCPartySegmentHistCreateDate);

        if ((aCTCPartySegmentHistCreateDate == null) || aCTCPartySegmentHistCreateDate.equals("")) {
            aCTCPartySegmentHistCreateDate = null;
        }

        eObjCTCPartySegment.setHistCreateDt(DWLFunctionUtils.getTimestampFromTimestampString(aCTCPartySegmentHistCreateDate));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCPartySegmentHistCreatedBy history attribute.
     *
     * @generated
     */
    public String getCTCPartySegmentHistCreatedBy() {
        return eObjCTCPartySegment.getHistCreatedBy();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCPartySegmentHistCreatedBy history attribute.
     *
     * @param aCTCPartySegmentHistCreatedBy
     *     The new value of CTCPartySegmentHistCreatedBy.
     * @generated
     */
    public void setCTCPartySegmentHistCreatedBy(String aCTCPartySegmentHistCreatedBy) {
        metaDataMap.put("CTCPartySegmentHistCreatedBy", aCTCPartySegmentHistCreatedBy);

        if ((aCTCPartySegmentHistCreatedBy == null) || aCTCPartySegmentHistCreatedBy.equals("")) {
            aCTCPartySegmentHistCreatedBy = null;
        }

        eObjCTCPartySegment.setHistCreatedBy(aCTCPartySegmentHistCreatedBy);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCPartySegmentHistEndDate history attribute.
     *
     * @generated
     */
    public String getCTCPartySegmentHistEndDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCPartySegment.getHistEndDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCPartySegmentHistEndDate history attribute.
     *
     * @param aCTCPartySegmentHistEndDate
     *     The new value of CTCPartySegmentHistEndDate.
     * @generated
     */
    public void setCTCPartySegmentHistEndDate(String aCTCPartySegmentHistEndDate) throws Exception{
        metaDataMap.put("CTCPartySegmentHistEndDate", aCTCPartySegmentHistEndDate);

        if ((aCTCPartySegmentHistEndDate == null) || aCTCPartySegmentHistEndDate.equals("")) {
            aCTCPartySegmentHistEndDate = null;
        }
        eObjCTCPartySegment.setHistEndDt(DWLFunctionUtils.getTimestampFromTimestampString(aCTCPartySegmentHistEndDate));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCPartySegmentHistoryIdPK history attribute.
     *
     * @generated
     */
    public String getCTCPartySegmentHistoryIdPK() {
        return DWLFunctionUtils.getStringFromLong(eObjCTCPartySegment.getHistoryIdPK());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCPartySegmentHistoryIdPK history attribute.
     *
     * @param aCTCPartySegmentHistoryIdPK
     *     The new value of CTCPartySegmentHistoryIdPK.
     * @generated
     */
    public void setCTCPartySegmentHistoryIdPK(String aCTCPartySegmentHistoryIdPK) {
        metaDataMap.put("CTCPartySegmentHistoryIdPK", aCTCPartySegmentHistoryIdPK);

        if ((aCTCPartySegmentHistoryIdPK == null) || aCTCPartySegmentHistoryIdPK.equals("")) {
            aCTCPartySegmentHistoryIdPK = null;
        }
        eObjCTCPartySegment.setHistoryIdPK(DWLFunctionUtils.getLongFromString(aCTCPartySegmentHistoryIdPK));
    }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an add transaction.
     *
     * @generated NOT
     */
    public DWLStatus validateAdd(int level, DWLStatus status) throws Exception {

        status = super.validateAdd(level, status);
        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO0: CDKWB0038I Add any controller-level custom validation logic to be
            // executed for this object during an "add" transaction

        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){

            // MDM_TODO: Add any common component-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions
             boolean isSegmentProviderNull = false;
            if (eObjCTCPartySegment.getSegmentProvider() == null || eObjCTCPartySegment.getSegmentProvider().trim().equals("")) {
                isSegmentProviderNull = true;
            }
            if( isSegmentProviderNull){
                DWLError err = new DWLError();
                err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_SEGMENTPROVIDER_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured. Property segmentProvider is null, in entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
                    logger.finest("validateAdd(int level, DWLStatus status) " + infoForLogging);

                }
                status.addError(err);
            }
            boolean isBusinessGroupNameNull = false;
            if (eObjCTCPartySegment.getBusinessGroupName() == null || eObjCTCPartySegment.getBusinessGroupName().trim().equals("")) {
                isBusinessGroupNameNull = true;
            }
            if( isBusinessGroupNameNull){
                DWLError err = new DWLError();
                err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_BUSINESSGROUPNAME_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured. Property businessGroupName is null, in entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("validateAdd(int level, DWLStatus status) " + infoForLogging);

                }
                status.addError(err);
            }
            boolean isSegmentTypeNameNull = false;
            if (eObjCTCPartySegment.getSegmentTypeName() == null || eObjCTCPartySegment.getSegmentTypeName().trim().equals("")) {
                isSegmentTypeNameNull = true;
            }
            if( isSegmentTypeNameNull){
                DWLError err = new DWLError();
                err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_SEGMENTTYPENAME_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured. Property segmentTypeName is null, in entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("validateAdd(int level, DWLStatus status) " + infoForLogging);

                }
                status.addError(err);
            }
            //If start date is null or invalid set the current time stamp.
            boolean isEffectiveStartDtNull = (eObjCTCPartySegment.getEffectiveStartDt() == null);
            if( isEffectiveStartDtNull){
                eObjCTCPartySegment.setEffectiveStartDt(DWLDateTimeUtilities.getCurrentSystemTimeAsTimestamp());

                }
            if (!isValidEffectiveStartDt) {
                DWLError err = new DWLError();
                err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.INVALID_CTCPARTYSEGMENT_EFFECTIVESTARTDT).longValue());
                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                if (logger.isFinestEnabled()) {
                	String infoForLogging="Error: Validation error. Invalid time specified on property effectiveStartDt in entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                	logger.finest("validateAdd(int level, DWLStatus status) " + infoForLogging);
                }
                status.addError(err);
            } 
            }
     
       
        status = getValidationStatus(level, status);
        return status;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an update transaction.
     *
     * @generated NOT
     */
    public DWLStatus validateUpdate(int level, DWLStatus status) throws Exception {
		logger.finest("ENTER validateUpdate(int level, DWLStatus status)");

        status = super.validateUpdate(level, status);
        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
            // MDM_TODO0: CDKWB0040I Add any controller-level custom validation logic to be
            // executed for this object during an "update" transaction
			        }

        	if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
				assignBeforeImageValues(metaDataMap);
			         // MDM_TODO: Add any component-level custom validation logic to be
			         // executed for this object during an "update" transaction
        			if (eObjCTCPartySegment.getPartySegmentId() == null) {
        					DWLError err = new DWLError();
        					err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
        					err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_PARTYSEGMENTID_NULL).longValue());
        					err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
        						if (logger.isFinestEnabled()) {
        							String infoForLogging="Error: Validation error occured for update. No primary key for entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
						            logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);
						
						           }
					                status.addError(err);
					  }
        			if (eObjCTCPartySegment.getLastUpdateDt() == null) {
        					DWLError err = new DWLError();
        						err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
					            err.setReasonCode(new Long(DWLUtilErrorReasonCode.LAST_UPDATED_DATE_NULL).longValue());
					            err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
					            	if (logger.isFinestEnabled()) {
						                    String infoForLogging="Error: Validation error occured for update. No last update date for entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
						            logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);
						
						            }
						                status.addError(err);
					             
					 }
        			// Validation for Business Key, if business key is not same 
        	
        			if(!(this.isBusinessKeySame((DWLCommon)this.BeforeImage(), false))){
        					DWLError err = new DWLError();
        						err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
					            err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_BUSINESS_KEY_VALIDATION).longValue());
					            err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
					            	if (logger.isFinestEnabled()) {
						                	String infoForLogging="Error: Validation error occured for update. Business key not same, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
						            logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);
						
						                }
					                status.addError(err);
						
					 }

			            
			        }
        status = getValidationStatus(level, status);
		if (logger.isFinestEnabled()) {
        	String returnValue = status.toString();
			logger.finest("RETURN validateUpdate(int level, DWLStatus status) " + returnValue);
		}
        return status;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Populate the before image of this business object.
     *
     * @see com.dwl.base.DWLCommon#populateBeforeImage()
     * @generated
     */
    public void populateBeforeImage() throws DWLBaseException {
		logger.finest("ENTER populateBeforeImage()");

        CTCPartySegment comp = null;
        try {
        
			comp = (CTCPartySegment)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCPARTY_SEGMENT_COMPONENT);
        	
        } catch (Exception e) {
			if (logger.isFinestEnabled()) {
				String infoForLogging="Error: Fatal error while updating record " + e.getMessage();
			logger.finest("populateBeforeImage() " + infoForLogging);
			}
            DWLExceptionUtils.throwDWLBaseException(e, 
                                                new DWLUpdateException(e.getMessage()), 
                                                this.getStatus(), DWLStatus.FATAL,
                                                CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ, 
                                                TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_BEFORE_IMAGE_NOT_POPULATED, 
                                                this.getControl());
        }
        
        comp.loadBeforeImage(this);
		logger.finest("RETURN populateBeforeImage()");
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation common to both add and update transactions.
     *
     * @generated NOT
     */
     
    private DWLStatus getValidationStatus(int level, DWLStatus status) throws Exception {
		logger.finest("ENTER getValidationStatus(int level, DWLStatus status)");

        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
        	
            // MDM_TODO: Add any common controller-level custom validation logic
            // to be executed for this object during either "add" or "update"
            // transactions
        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
        	boolean isContIdNull = false;
            if (eObjCTCPartySegment.getContId() == null) {
                isContIdNull = true;
            }
            if( isContIdNull){
                DWLError err = new DWLError();
                err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_CONTID_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured. Property contId is null, in entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
                    logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                }
              status.addError(err);
            }else {
            	//Check if the party is exist and active else throw error.
            	boolean blnPartyActive = CTCAdditionUtil.isPartyExist(DWLFunctionUtils.getStringFromLong(eObjCTCPartySegment.getContId()), eObjCTCPartySegment.getControl());
            	if(!blnPartyActive){
	        		DWLError err = new DWLError();
	                err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
	                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTC_PARTY_NOT_EXIST_OR_INACTIVE).longValue());
	                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
	                if (logger.isFinestEnabled()) {
	                    String infoForLogging="Error: Validation error occured. Property contId is not exist / active, in entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
	                    logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);
	                }
	        		status.addError(err);  		
	        	
	        	}
            }
            if (!isValidEffectiveEndDt) {
                DWLError err = new DWLError();
                err.setComponentType(new Long(CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ).longValue());
                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.INVALID_CTCPARTYSEGMENT_EFFECTIVEENDDT).longValue());
                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                if (logger.isFinestEnabled()) {
                	String infoForLogging="Error: Validation error. Invalid time specified on property effectiveEndDt in entity CTCPartySegment, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                	logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                }
                status.addError(err);
            } 
        	
        }
        
        if (logger.isFinestEnabled()) {
            String returnValue = status.toString();
            logger.finest("RETURN getValidationStatus(int level, DWLStatus status) " + returnValue);

        }
        
        return status;
    }
    



}


