package com.ctc.mdm.addition.component;


import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;
import com.ctc.mdm.addition.interfaces.CTCContractRolePrivPref;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.exception.DWLUpdateException;
import com.dwl.base.util.DWLExceptionUtils;
import com.dwl.tcrm.businessServices.component.TCRMEntityPrivPrefBObj;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.financial.component.TCRMContractPartyRoleBObj;
import com.dwl.tcrm.financial.constant.TCRMFinancialPropertyKeys;
import com.dwl.tcrm.financial.interfaces.IContract;
import com.dwl.tcrm.utilities.FunctionUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.common.validator.BusinessKeyValidationContext;
import com.ibm.mdm.common.validator.BusinessKeyValidationType;

public class CTCContractRolePrivPrefBObj extends TCRMEntityPrivPrefBObj {

	protected String contractRoleId;

	/**
	 * 
	 */
	public CTCContractRolePrivPrefBObj()
	{
	   init();
	   setPrivPrefEntity("CONTRACTROLE");
	}

	private void init()
	{
	  this.metaDataMap.put("ContractRoleId", null);
	}

	public String getContractRoleId()
	{
	  return FunctionUtils.getStringFromLong(this.eObjEntityPrivPref.getPprefInstancePK());
	}

	public void setContractRoleId(String aContractRoleId)
	{
	  this.metaDataMap.put("ContractRoleId", aContractRoleId);
	  this.eObjEntityPrivPref.setPprefInstancePK(FunctionUtils.getLongFromString(aContractRoleId));
	  this.contractRoleId = aContractRoleId;
	}

	public void setContractRolePrivPrefIdPK(String idPk)
	{
	  if ((idPk == null) || (idPk.equals(""))) {
	    idPk = null;
	  }

	  if (idPk != null) {
	    this.eObjPrivPref.setPprefIdPK(FunctionUtils.getLongFromString(idPk));
	  }

	  this.eObjEntityPrivPref.setPprefIdPK(FunctionUtils.getLongFromString(idPk));
	}

	public String getContractRolePrivPrefIdPK()
	{
	  return FunctionUtils.getStringFromLong(this.eObjPrivPref.getPprefIdPK());
	}

	private DWLStatus getValidationStatus(int level, DWLStatus status)
	  throws Exception
	{
		  if ((level != 1) || 
		    (level == 2))
		  {
		    IContract contractComp = (IContract)TCRMClassFactory.getTCRMComponent(TCRMFinancialPropertyKeys.CONTRACT_COMPONENT);
	
		    if ((this.contractRoleId == null) || (this.contractRoleId.trim().equalsIgnoreCase("")))
		    {
		    	//TODO change error code
		      DWLError error10 = new DWLError();
		      error10.setComponentType(new Long(CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_BOBJ).longValue());
		      error10.setReasonCode(new Long(CTCAdditionsErrorReasonCode.MISSING_CONTRACTROLEID_CTCCONTRACTROLEPRIVACYPREFERENCE).longValue());
		      error10.setErrorType(TCRMErrorCode.DATA_INVALID_ERROR);
		      status.addError(error10);
		    }
		    else {
		      
		    	TCRMContractPartyRoleBObj contractRole = null;
		    	if(this.getControl() != null && this.getControl().containsKey(this.contractRoleId + "|PRIV_PREF")){
		    		contractRole = (TCRMContractPartyRoleBObj)this.getControl().get(this.contractRoleId + "|PRIV_PREF");
		    	}
		    	else
		    	{
			    	contractRole = contractComp.getContractPartyRole(this.contractRoleId, getControl());
		    		if(contractRole != null){
		    			this.getControl().put(this.contractRoleId + "|PRIV_PREF", contractRole);
		    		}
		    	}
		      
		    	if (contractRole == null)
		    	{
		    		DWLError error11 = new DWLError();
		    		error11.setComponentType(new Long(CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_BOBJ).longValue());
		    		error11.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CONTRACTPARTYROLE_NOT_FOUND_CTCCONTRACTROLEPRIVACYPREFERENCE).longValue());
		    		error11.setErrorType(TCRMErrorCode.DATA_INVALID_ERROR);
		    		status.addError(error11);
		          
		    	} else {
		        	
		    		this.eObjEntityPrivPref.setPprefInstancePK(FunctionUtils.getLongFromString(this.contractRoleId));
		          
		    	}
		    }
	    }
		return status;
	}

	  public DWLStatus validateAdd(int level, DWLStatus status)
	    throws Exception
	  {
	    if (level == 2) {
	      setPrivPrefEntity("CONTRACTROLE");
	
	      status = validateBusinessKey(new BusinessKeyValidationContext(this, status, BusinessKeyValidationType.ADD));
	    }
	
	    status = super.validateAdd(level, status);
	
	    if (this.useNullEndDateValidation) {
	      this.isValidEndDate = true;
	    }
	
	    if (this.useNullStartDateValidation) {
	      this.isValidStartDate = true;
	    }
	
	    status = getValidationStatus(level, status);
	
	    return status;
	  }

	  public DWLStatus validateUpdate(int level, DWLStatus status)
	    throws Exception
	  {
	    if (level == 2) {
	      setPrivPrefEntity("CONTRACTROLE");
	    }

	    status = super.validateUpdate(level, status);

	    if (level == 2)
	    {
	    	CTCContractRolePrivPrefBObj beforeObj = (CTCContractRolePrivPrefBObj)this.beforeImage;

		    if ((beforeObj != null) && (!this.contractRoleId.equals(beforeObj.getContractRoleId())))
		    {
		    	DWLError error11 = new DWLError();
		        error11.setComponentType(new Long(CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_BOBJ).longValue());
	
		        error11.setReasonCode(new Long(CTCAdditionsErrorReasonCode.UPDATE_CONTRACTROLEID_CTCCONTRACTROLEPRIVACYPREFERENCE_FAILED).longValue());
	
		        error11.setErrorType(TCRMErrorCode.UPDATE_RECORD_ERROR);
		        status.addError(error11);
		        
		     }

	      status = validateBusinessKey(new BusinessKeyValidationContext(this, status, BusinessKeyValidationType.UPDATE));
	    }

	    return status;
	  }

	  /**
	   * 
	   */
	  public void populateBeforeImage() throws DWLBaseException
	  {	
		  CTCContractRolePrivPref contractRolePrivPrefComp = null;
		  
		  try
		  {
			  contractRolePrivPrefComp = (CTCContractRolePrivPref)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT);
			  
			  contractRolePrivPrefComp.loadBeforeImage(this);
		  }
		  catch (Exception e)
		  {
			  //TODO correct the error constant
			  DWLExceptionUtils.throwDWLBaseException(e, new DWLUpdateException(e.getLocalizedMessage()), getStatus(), 9L, "1028", TCRMErrorCode.UPDATE_RECORD_ERROR , CTCAdditionsErrorReasonCode.BEFORE_IMAGE_EMPTY_CTCCONTRACTROLEPRIVACYPREFERENCE, getControl());
		  }

		  
	  }	
}
