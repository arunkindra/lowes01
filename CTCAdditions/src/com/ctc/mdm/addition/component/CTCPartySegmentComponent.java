
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[d66ba77e58654fafadca699aecf9ba80]
 */

package com.ctc.mdm.addition.component;

import com.dwl.base.DWLControl;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.base.DWLResponse;
import com.dwl.base.constant.DWLConstantDef;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.util.DWLFunctionUtils;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.annotations.Component;
import com.ibm.mdm.annotations.TxMetadata;


import com.ctc.mdm.addition.bobj.query.CTCAdditionsModuleBObjPersistenceFactory;
import com.ctc.mdm.addition.bobj.query.CTCAdditionsModuleBObjQueryFactory;
import com.ctc.mdm.addition.bobj.query.CTCPartySegmentBObjQuery;

import com.ctc.mdm.addition.component.CTCPartySegmentBObj;

import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.ResourceBundleNames;

import com.ctc.mdm.addition.interfaces.CTCPartySegment;
import com.ctc.mdm.addition.util.CTCAdditionUtil;

import com.dwl.base.IDWLErrorMessage;

import com.dwl.base.exception.DWLUpdateException;

import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.base.requestHandler.DWLTransactionPersistent;

import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.base.util.DWLExceptionUtils;
import com.dwl.base.util.PaginationUtils;

import com.dwl.bobj.query.BObjQuery;
import com.dwl.bobj.query.BObjQueryException;
import com.dwl.bobj.query.Persistence;

import com.dwl.tcrm.common.TCRMCommonComponent;

import com.dwl.tcrm.exception.TCRMInsertException;
import com.dwl.tcrm.exception.TCRMReadException;

import com.dwl.tcrm.utilities.FunctionUtils;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMExceptionUtils;

import com.ibm.mdm.common.brokers.BObjPersistenceFactoryBroker;
import com.ibm.mdm.common.brokers.BObjQueryFactoryBroker;
import com.ibm.mdm.common.util.PropertyManager;

import java.sql.Timestamp;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Business component class for handling CTCPartySegment related transactions
 * and inquiries.
 * @generated
 */
 @Component(errorComponentID = CTCAdditionsComponentID.CTCPARTY_SEGMENT_COMPONENT)
public class CTCPartySegmentComponent extends TCRMCommonComponent implements CTCPartySegment {
	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
	private final static String EXCEPTION_DUPLICATE_KEY = "Exception_Shared_DuplicateKey";

	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCPartySegmentComponent.class);
			
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private IDWLErrorMessage errHandler;
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static CTCAdditionsModuleBObjQueryFactory bObjQueryFactory = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static CTCAdditionsModuleBObjPersistenceFactory bObjPersistenceFactory = null;
  
   

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @generated
     */
    public CTCPartySegmentComponent() {
        super();
        errHandler = TCRMClassFactory.getErrorHandler();
    }




    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCPartySegment.
     *
     * @param PartySegmentId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetCTCPartySegment
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETCTCPARTYSEGMENT_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetCTCPartySegment"
		)
     public DWLResponse getCTCPartySegment(String PartySegmentId,  DWLControl control) throws DWLBaseException {
		logger.finest("ENTER getCTCPartySegment(String PartySegmentId,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(PartySegmentId);
        DWLTransaction txObj = new  DWLTransactionInquiry("getCTCPartySegment", params, control);
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction getCTCPartySegment.";
			logger.finest("getCTCPartySegment(String PartySegmentId,  DWLControl control) " + infoForLogging);
		}	
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction getCTCPartySegment.";
			logger.finest("getCTCPartySegment(String PartySegmentId,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN getCTCPartySegment(String PartySegmentId,  DWLControl control) " + returnValue);
		}
        return retObj;
    }
    
    
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a CTCPartySegment from the database.
     * 
     * @generated
     */
    public DWLResponse handleGetCTCPartySegment(String PartySegmentId,  DWLControl control) throws Exception {
        DWLStatus status = new DWLStatus();
        DWLResponse response = createDWLResponse();
        
        BObjQuery bObjQuery = null;
           
        String asOfDate = (String) control.get(DWLControl.INQUIRE_AS_OF_DATE);

        // History data inquiry: if inquireAsOfDate field has value in request xml 
        if (StringUtils.isNonBlank(asOfDate)) {
            Timestamp tsAsOfDate = getPITHistoryDate(asOfDate, CTCAdditionsComponentID.CTCPARTY_SEGMENT_COMPONENT,
                                                     CTCAdditionsErrorReasonCode.GETCTCPARTYSEGMENT_INVALID_INQUIRE_AS_OF_DATE_FORMAT,
                                                     status, control);

            bObjQuery = getBObjQueryFactory().createCTCPartySegmentBObjQuery(CTCPartySegmentBObjQuery.CTCPARTY_SEGMENT_HISTORY_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(PartySegmentId));
            bObjQuery.setParameter(1, tsAsOfDate);
            bObjQuery.setParameter(2, tsAsOfDate);
        } else {
            bObjQuery = getBObjQueryFactory().createCTCPartySegmentBObjQuery(CTCPartySegmentBObjQuery.CTCPARTY_SEGMENT_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(PartySegmentId));
        }


        CTCPartySegmentBObj o = (CTCPartySegmentBObj) bObjQuery.getSingleResult();
        if( o == null ){
        	return null;
        } 
          
        if (o.getStatus()==null) {
            o.setStatus(status);
        }
        response.addStatus(o.getStatus());
        response.setData(o);

	    return response;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void beforePreExecuteGetCTCPartySegment(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
        String pk = null;
        if (arguments!=null && arguments.length>0) {
            pk = (String)arguments[0];
        }
        // Check if the parameter passed in exists.
        if ((pk == null) || (pk.trim().length() == 0)) {
            TCRMExceptionUtils.throwTCRMException(null, new TCRMReadException(), transaction.getStatus(), DWLStatus.FATAL,
                CTCAdditionsComponentID.CTCPARTY_SEGMENT_COMPONENT,
                TCRMErrorCode.READ_RECORD_ERROR,
                CTCAdditionsErrorReasonCode.GETCTCPARTYSEGMENT_ID_NULL,
                transaction.getTxnControl(), errHandler);
        }
    }

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllCTCPartySegment.
     *
     * @param ContId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetAllCTCPartySegment
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETALLCTCPARTYSEGMENT_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetAllCTCPartySegment"
		)
     public DWLResponse getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) throws DWLBaseException {
		logger.finest("ENTER getAllCTCPartySegment(String ContId,  String filter,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(ContId);
        params.add(filter);
        DWLTransaction txObj = new  DWLTransactionInquiry("getAllCTCPartySegment", params, control);
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction getAllCTCPartySegment.";
			logger.finest("getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) " + infoForLogging);
		}	
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction getAllCTCPartySegment.";
			logger.finest("getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) " + returnValue);
		}
        return retObj;
    }
    
    
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a CTCPartySegment from the database.
     * 
     * @generated
     */
    public DWLResponse handleGetAllCTCPartySegment(String ContId,  String filter,  DWLControl control) throws Exception {
        DWLStatus status = new DWLStatus();
        DWLResponse response = createDWLResponse();
        
        BObjQuery bObjQuery = null;
           
        String asOfDate = (String) control.get(DWLControl.INQUIRE_AS_OF_DATE);

        // History data inquiry: if inquireAsOfDate field has value in request xml 
        if (StringUtils.isNonBlank(asOfDate)) {
            Timestamp tsAsOfDate = getPITHistoryDate(asOfDate, CTCAdditionsComponentID.CTCPARTY_SEGMENT_COMPONENT,
                                                     CTCAdditionsErrorReasonCode.GETALLCTCPARTYSEGMENT_INVALID_INQUIRE_AS_OF_DATE_FORMAT,
                                                     status, control);

            bObjQuery = getBObjQueryFactory().createCTCPartySegmentBObjQuery(CTCPartySegmentBObjQuery.ALL_CTCPARTY_SEGMENT_HISTORY_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(ContId));
            bObjQuery.setParameter(1, tsAsOfDate);
            bObjQuery.setParameter(2, tsAsOfDate);
        } else if(filter.equals(TCRMRecordFilter.ACTIVE)){
        	bObjQuery = getBObjQueryFactory().createCTCPartySegmentBObjQuery(CTCPartySegmentBObjQuery.ACTIVE_CTCPARTY_SEGMENT_QUERY,
                control);
        bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(ContId));
        bObjQuery.setParameter(1, DWLDateTimeUtilities.getCurrentSystemTimeAsTimestamp());
        }
        else if(filter.equals(TCRMRecordFilter.INACTIVE)){
        	bObjQuery = getBObjQueryFactory().createCTCPartySegmentBObjQuery(CTCPartySegmentBObjQuery.INACTIVE_CTCPARTY_SEGMENT_QUERY,
                    control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(ContId));
            bObjQuery.setParameter(1, DWLDateTimeUtilities.getCurrentSystemTimeAsTimestamp());
        }
        else{
        	bObjQuery = getBObjQueryFactory().createCTCPartySegmentBObjQuery(CTCPartySegmentBObjQuery.ALL_CTCPARTY_SEGMENT_QUERY,
                    control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(ContId));
        }

        boolean considerForPagination = PaginationUtils
														.considerForPagintion(CTCPartySegmentBObj.class.getName(), control);
        control.setConsiderForPagintionFlag(considerForPagination);

        // set returned object
        List<?> list = bObjQuery.getResults();

        if (list.size() == 0) {
            return null;
        }
		Vector<CTCPartySegmentBObj> vector = new Vector<CTCPartySegmentBObj>();
        for (Iterator<?> it = list.iterator(); it.hasNext();) {
            CTCPartySegmentBObj o = (CTCPartySegmentBObj) it.next();
            vector.add(o);
          
            if (o.getStatus()==null) {
                o.setStatus(status);
            }
            response.addStatus(o.getStatus());
        }
        response.setData(vector);

	    return response;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void beforePreExecuteGetAllCTCPartySegment(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
		if (arguments!=null && arguments.length>0) {
            // MDM_TODO0: CDKWB0003I Add argument validation logic
		}
    }

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction addCTCPartySegment.
     *
     * @param theBObj
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleAddCTCPartySegment
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.ADD_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.INSERT_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.ADDCTCPARTYSEGMENT_FAILED
		)
     public DWLResponse addCTCPartySegment(CTCPartySegmentBObj theBObj) throws DWLBaseException {
		logger.finest("ENTER addCTCPartySegment(CTCPartySegmentBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("addCTCPartySegment", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction addCTCPartySegment.";
			logger.finest("addCTCPartySegment(CTCPartySegmentBObj theBObj) " + infoForLogging);
		}	
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction addCTCPartySegment.";
			logger.finest("addCTCPartySegment(CTCPartySegmentBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN addCTCPartySegment(CTCPartySegmentBObj theBObj) " + returnValue);
		}
        return retObj;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Adds a CTCPartySegment to the database.
     *
     * @param theCTCPartySegmentBObj
     *     The object that contains CTCPartySegment attribute values.
     * @return
     *     DWLResponse containing a CTCPartySegmentBObj object.
     * @exception Exception
     * @generated NOT
     */
    public DWLResponse handleAddCTCPartySegment(CTCPartySegmentBObj theCTCPartySegmentBObj) throws Exception {
        DWLResponse response = null;
        DWLStatus status = theCTCPartySegmentBObj.getStatus();
        if (status == null) {
            status = new DWLStatus();
            theCTCPartySegmentBObj.setStatus(status);
        }
    	try {
    		//get all the CTC party segments based on the input party ID
    		response = getAllCTCPartySegment(theCTCPartySegmentBObj.getContId(), TCRMRecordFilter.ACTIVE, theCTCPartySegmentBObj.getControl() );
    		if(CTCAdditionUtil.isSuccessResponse(response)){
    			//Get the matched business key object from the request
    			Vector<CTCPartySegmentBObj> vecSegments= (Vector)response.getData();
    			CTCPartySegmentBObj objSegment = null;
    			if(CTCAdditionUtil.isNotEmpty(vecSegments)){
    				objSegment = getMatchingPartySegment(vecSegments, theCTCPartySegmentBObj);
    			}
				if(objSegment != null){ // if the matched object found updating the details.
					//set the id PKs and last update date for update.
					theCTCPartySegmentBObj.setPartySegmentId(objSegment.getPartySegmentId());
					theCTCPartySegmentBObj.setCTCPartySegmentLastUpdateDate(objSegment.getCTCPartySegmentLastUpdateDate());
					
					// set lastupdatetxid with txnid from dwlcontrol
			        theCTCPartySegmentBObj.getEObjCTCPartySegment().setLastUpdateTxId(new Long(theCTCPartySegmentBObj.getControl().getTxnId()));
			        Persistence theCTCPartySegmentBObjPersistence = getBObjPersistenceFactory().createCTCPartySegmentBObjPersistence(CTCPartySegmentBObjQuery.CTCPARTY_SEGMENT_UPDATE, theCTCPartySegmentBObj);
			        theCTCPartySegmentBObjPersistence.persistUpdate();

			        response = createDWLResponse();
			        response.setData(theCTCPartySegmentBObj);
			        response.setStatus(theCTCPartySegmentBObj.getStatus());
				}else{//If no match found add the record.
        
					String strPluggableID = null;
		
		            // Pluggable Key Structure implementation
		            strPluggableID = getSuppliedIdPKFromBObj(theCTCPartySegmentBObj);
		
		            if ((strPluggableID != null) && (strPluggableID.length() > 0)) {
		                theCTCPartySegmentBObj.getEObjCTCPartySegment().setPartySegmentId(FunctionUtils.getLongFromString(strPluggableID));
		            } else {
		                strPluggableID = null;
		                theCTCPartySegmentBObj.getEObjCTCPartySegment().setPartySegmentId(null);
		            }
		            Persistence theCTCPartySegmentBObjPersistence = getBObjPersistenceFactory().createCTCPartySegmentBObjPersistence(CTCPartySegmentBObjQuery.CTCPARTY_SEGMENT_ADD, theCTCPartySegmentBObj);
		            theCTCPartySegmentBObjPersistence.persistAdd();
		         	response = new DWLResponse();
					response.setData(theCTCPartySegmentBObj);
					response.setStatus(theCTCPartySegmentBObj.getStatus());
		        }
				
    		}
        } catch (Exception ex) {
                TCRMExceptionUtils.throwTCRMException(ex, new TCRMInsertException(ex.getMessage()), status,
                    DWLStatus.FATAL, CTCAdditionsComponentID.CTCPARTY_SEGMENT_COMPONENT, TCRMErrorCode.INSERT_RECORD_ERROR,
                    CTCAdditionsErrorReasonCode.ADDCTCPARTYSEGMENT_FAILED, theCTCPartySegmentBObj.getControl(), errHandler);
        }
        
        return response;
    }
    

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction updateCTCPartySegment.
     *
     * @param theBObj
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleUpdateCTCPartySegment
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.UPDATE_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.UPDATE_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.UPDATECTCPARTYSEGMENT_FAILED
		)
     public DWLResponse updateCTCPartySegment(CTCPartySegmentBObj theBObj) throws DWLBaseException {
		logger.finest("ENTER updateCTCPartySegment(CTCPartySegmentBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("updateCTCPartySegment", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction updateCTCPartySegment.";
			logger.finest("updateCTCPartySegment(CTCPartySegmentBObj theBObj) " + infoForLogging);
		}	
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction updateCTCPartySegment.";
			logger.finest("updateCTCPartySegment(CTCPartySegmentBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN updateCTCPartySegment(CTCPartySegmentBObj theBObj) " + returnValue);
		}
        return retObj;
    }
    
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Updates the specified CTCPartySegment with new attribute values.
     *
     * @param theCTCPartySegmentBObj
     *     The object that contains CTCPartySegment attribute values to be
     *     updated
     * @return
     *     DWLResponse containing a CTCPartySegmentBObj of the updated object.
     * @exception Exception
     * @generated
     */
    public DWLResponse handleUpdateCTCPartySegment(CTCPartySegmentBObj theCTCPartySegmentBObj) throws Exception {

        DWLStatus status = theCTCPartySegmentBObj.getStatus();

        if (status == null) {
            status = new DWLStatus();
            theCTCPartySegmentBObj.setStatus(status);
        }
        
            // set lastupdatetxid with txnid from dwlcontrol
            theCTCPartySegmentBObj.getEObjCTCPartySegment().setLastUpdateTxId(new Long(theCTCPartySegmentBObj.getControl().getTxnId()));
         Persistence theCTCPartySegmentBObjPersistence = getBObjPersistenceFactory().createCTCPartySegmentBObjPersistence(CTCPartySegmentBObjQuery.CTCPARTY_SEGMENT_UPDATE, theCTCPartySegmentBObj);
         theCTCPartySegmentBObjPersistence.persistUpdate();

        DWLResponse response = createDWLResponse();
        response.setData(theCTCPartySegmentBObj);
        response.setStatus(theCTCPartySegmentBObj.getStatus());

        return response;
    }



    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     **/
    public void loadBeforeImage(CTCPartySegmentBObj bObj) throws DWLBaseException {
		logger.finest("ENTER loadBeforeImage(CTCPartySegmentBObj bObj)");
    	if( bObj.BeforeImage() == null ){
    	
    		CTCPartySegmentBObj beforeImage = null;
    		DWLResponse response = null;
    		
    		try {
    			response = getCTCPartySegment( bObj.getPartySegmentId(), bObj.getControl());
    			beforeImage = (CTCPartySegmentBObj)response.getData();
    			
    		} catch( Exception e){
				if (logger.isFinestEnabled()) {
    				String infoForLogging="Error: Exception " + e.getMessage() + " while updating a record ";
			logger.finest("loadBeforeImage(CTCPartySegmentBObj bObj) " + infoForLogging);
				}
	            DWLExceptionUtils.throwDWLBaseException(e, 
            									new DWLUpdateException(e.getMessage()), 
            									bObj.getStatus(), DWLStatus.FATAL,
							                    CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ, 
                                                TCRMErrorCode.UPDATE_RECORD_ERROR,
							                    CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_BEFORE_IMAGE_NOT_POPULATED, 
							                    bObj.getControl(), errHandler);
    		}
    		
    		if( beforeImage == null ){
				if (logger.isFinestEnabled()) {
    		    	String infoForLogging="Error: Before image for updating a record is null ";
			logger.finest("loadBeforeImage(CTCPartySegmentBObj bObj) " + infoForLogging);
				}
	            DWLExceptionUtils.throwDWLBaseException( new DWLUpdateException(), 
            									bObj.getStatus(), DWLStatus.FATAL,
							                    CTCAdditionsComponentID.CTCPARTY_SEGMENT_BOBJ, 
                                                TCRMErrorCode.UPDATE_RECORD_ERROR,
							                    CTCAdditionsErrorReasonCode.CTCPARTYSEGMENT_BEFORE_IMAGE_NOT_POPULATED, 
							                    bObj.getControl(), errHandler);
    		}
    		
    		bObj.setBeforeImage(beforeImage);
    		
    	}
		logger.finest("RETURN loadBeforeImage(CTCPartySegmentBObj bObj)");
    }
	 


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the BObjQuery factory class from the configuration.
     * @generated
    **/
    private CTCAdditionsModuleBObjQueryFactory getBObjQueryFactory() throws BObjQueryException{
        logger.finest("ENTER getBObjQueryFactory()");

        if (bObjQueryFactory == null) {
            synchronized (CTCPartySegmentComponent.class) {
                if (bObjQueryFactory == null) {
                    try {
                        String className = PropertyManager.getProperty(DWLConstantDef.TCRM_APP_NAME,CTCAdditionsModuleBObjQueryFactory.BOBJ_QUERY_FACTORY);
                        Class bObjQueryFactoryClass = null;
                        bObjQueryFactoryClass = Class.forName(className);
                        bObjQueryFactory = (CTCAdditionsModuleBObjQueryFactory) bObjQueryFactoryClass.newInstance();
                        if (logger.isFinestEnabled()) {
                            String infoForLogging="Created an instance of BObj query factory " + bObjQueryFactoryClass;
            logger.finest("getBObjQueryFactory() " + infoForLogging);

                        }
                    } catch (Exception e) {
                        //DWLExceptionUtils.log(e);                
                        if (logger.isFinestEnabled()) {
                            String infoForLogging="Error: Exception " + e.getMessage() + " creating a BObj query factory.";
            logger.finest("getBObjQueryFactory() " + infoForLogging);

                        }
                        throw new BObjQueryException(e);
                    }
                }
            }
        }
        logger.finest("RETURN getBObjQueryFactory()");

        return bObjQueryFactory;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the BObjPersistenceFactory class from the configuration.
     * @generated
    **/
    private CTCAdditionsModuleBObjPersistenceFactory getBObjPersistenceFactory() throws BObjQueryException {
        if (bObjPersistenceFactory == null) {
            synchronized (CTCPartySegmentComponent.class) {
                if (bObjPersistenceFactory == null) {
                    try {
                        String className = PropertyManager.getProperty(DWLConstantDef.TCRM_APP_NAME,CTCAdditionsModuleBObjPersistenceFactory.BOBJ_PERSISTENCE_FACTORY);
                        Class bObjPersistenceFactoryClass = null;
                        bObjPersistenceFactoryClass = Class.forName(className);
                        bObjPersistenceFactory = (CTCAdditionsModuleBObjPersistenceFactory) bObjPersistenceFactoryClass.newInstance();
                    } catch (Exception e) {
                        //DWLExceptionUtils.log(e);                
                        if (logger.isFinestEnabled()) {
                            String infoForLogging="Error: Exception " + e.getMessage() + " accessing the Persistence factory.";
            logger.finest("Unknown method " + infoForLogging);

                        }
                        throw new BObjQueryException(e);
                    }
                }
            }
        }
        return bObjPersistenceFactory;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Builds duplicated key throwable message. There are only two elements in
     * the vector, one is the primary key, and the other is the class name.
     * @generated
     **/
    @SuppressWarnings("unused")
    private String buildDupThrowableMessage(String[] errParams) {
		return ResourceBundleHelper.resolve(
				ResourceBundleNames.COMMON_SERVICES_STRINGS,
				EXCEPTION_DUPLICATE_KEY, errParams);

    }
    /** 
     * To get the matched CTC Party segment from the list.
     * 
     * @param segmentList Segments Vector
     * @param theBobj - request CTC Party segment object
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	private CTCPartySegmentBObj getMatchingPartySegment(Vector<CTCPartySegmentBObj> vecSegments, CTCPartySegmentBObj theBobj)throws Exception{
    	
		if(CTCAdditionUtil.isNotEmpty(vecSegments)){
			for( CTCPartySegmentBObj objSegement  : vecSegments) {
				if(theBobj.isBusinessKeySame(objSegement)){
					return objSegement;
				}    	
			}
		}
		return null;
    }
}


