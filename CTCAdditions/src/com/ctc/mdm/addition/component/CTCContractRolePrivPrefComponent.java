
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.ctc.mdm.addition.component;

import java.util.Vector;

import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.ResourceBundleNames;
import com.ctc.mdm.addition.interfaces.CTCContractRolePrivPref;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.exception.DWLUpdateException;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.base.requestHandler.DWLTransactionPersistent;
import com.dwl.base.util.DWLExceptionUtils;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.tcrm.businessServices.component.TCRMEntityInstancePrivPrefBObj;
import com.dwl.tcrm.businessServices.component.TCRMEntityPrivPrefBObj;
import com.dwl.tcrm.businessServices.interfaces.IPrivacyPreference;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.exception.TCRMReadException;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.tcrm.utilities.TCRMExceptionUtils;
import com.ibm.mdm.annotations.Component;
import com.ibm.mdm.annotations.TxMetadata;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Business component class for handling CTCContractRolePrivPref related
 * transactions and inquiries.
 * @generated NOT
 */
 @Component(errorComponentID = CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT)
public class CTCContractRolePrivPrefComponent extends TCRMCommonComponent implements CTCContractRolePrivPref {
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private final static String EXCEPTION_DUPLICATE_KEY = "Exception_Shared_DuplicateKey";

    /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      * @generated 
      */
     private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCContractRolePrivPrefComponent.class);

            
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private IDWLErrorMessage errHandler;
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @generated
     */
    public CTCContractRolePrivPrefComponent() {
        super();
        errHandler = TCRMClassFactory.getErrorHandler();
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCContractRolePrivacyPreference.
     *
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetCTCContractRolePrivacyPreference
     * @generated NOT
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETCTCCONTRACTROLEPRIVACYPREFERENCE_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetCTCContractRolePrivacyPreference"
        )
     public DWLResponse getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) throws DWLBaseException {
        logger.finest("ENTER getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control)");

        Vector params = new Vector();
        params.add(privPrefType);
        params.add(contractRoleId);
        DWLTransaction txObj = new  DWLTransactionInquiry("getCTCContractRolePrivacyPreference", params, control);
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before execution of transaction getCTCContractRolePrivacyPreference.";
            logger.finest("getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After execution of transaction getCTCContractRolePrivacyPreference.";
            logger.finest("getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) " + returnValue);

        }
        return retObj;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a CTCContractRolePrivPref from the database.
     * 
     * @generated NOT
     */
    public DWLResponse handleGetCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) throws Exception {
        DWLStatus status = new DWLStatus();
        DWLResponse response = createDWLResponse();

        IPrivacyPreference privPrefComp = (IPrivacyPreference)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PRIVACY_PREFERENCE_COMPONENT);

        Vector vec = privPrefComp.getEntityPrivacyPreferenceByPrefType("CONTRACTROLE", privPrefType, contractRoleId, control);
        Vector resultVec = new Vector();
        if ((vec != null) && (vec.size() > 0))
        {
        	
        	for (int i = 0; i < vec.size(); i++)
        	{
        		TCRMEntityPrivPrefBObj entityPrivPref = (TCRMEntityPrivPrefBObj)vec.elementAt(i);

        		CTCContractRolePrivPrefBObj contractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj)TCRMClassFactory.createBObj(CTCContractRolePrivPrefBObj.class);
        		
        		contractRolePrivPrefBObj.setControl(control);
        		
        		contractRolePrivPrefBObj.setEObjEntityPrivPref(entityPrivPref.getEObjEntityPrivPref());

        		contractRolePrivPrefBObj.setEObjPrivPref(entityPrivPref.getEObjPrivPref());

        		contractRolePrivPrefBObj.setItemsTCRMEntityInstancePrivPrefBObj(entityPrivPref.getItemsTCRMEntityInstancePrivPrefBObj());

        		contractRolePrivPrefBObj.setContractRoleId(entityPrivPref.getEObjEntityPrivPref().getPprefInstancePK().toString());

        		contractRolePrivPrefBObj.setPrivPrefValue(entityPrivPref.getPrivPrefValue());

        		contractRolePrivPrefBObj.setPrivPrefReasonValue(entityPrivPref.getPrivPrefReasonValue());

        		contractRolePrivPrefBObj.setSourceIdentValue(entityPrivPref.getSourceIdentValue());

        		contractRolePrivPrefBObj.setPrivPrefCatType(entityPrivPref.getPrivPrefCatType());

        		contractRolePrivPrefBObj.setPrivPrefCatValue(entityPrivPref.getPrivPrefCatValue());

        		contractRolePrivPrefBObj.setPrivPrefActionType(entityPrivPref.getPrivPrefActionType());

        		contractRolePrivPrefBObj.setPrivPrefActionValue(entityPrivPref.getPrivPrefActionValue());

        		resultVec.addElement(contractRolePrivPrefBObj);
        	}

        }
        
        status.setStatus(DWLStatus.SUCCESS);
        response.setStatus(status);
        response.setData(resultVec);
        
        return response;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void beforePreExecuteGetCTCContractRolePrivacyPreference(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
        
        // MDM_TODO Add argument validation logic
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllCTCContractRolePrivacyPreferences.
     *
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetAllCTCContractRolePrivacyPreferences
     * @generated NOT
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETALLCTCCONTRACTROLEPRIVACYPREFERENCES_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetAllCTCContractRolePrivacyPreferences"
        )
     public DWLResponse getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) throws DWLBaseException {
        logger.finest("ENTER getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control)");

        Vector params = new Vector();
        params.add(contractRoleId);
        params.add(inquiryLevel);
        params.add(filter);
        DWLTransaction txObj = new  DWLTransactionInquiry("getAllCTCContractRolePrivacyPreferences", params, control);
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before execution of transaction getAllCTCContractRolePrivacyPreferences.";
            logger.finest("getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After execution of transaction getAllCTCContractRolePrivacyPreferences.";
            logger.finest("getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) " + returnValue);

        }
        return retObj;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a CTCContractRolePrivPref from the database.
     * 
     * @generated NOT
     */
    public DWLResponse handleGetAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) throws Exception {
        DWLStatus status = new DWLStatus();
        DWLResponse response = createDWLResponse();

        IPrivacyPreference privPrefComp = (IPrivacyPreference)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PRIVACY_PREFERENCE_COMPONENT);

        Vector vec = privPrefComp.getAllPrivacyPreferences("CONTRACTROLE", contractRoleId, inquiryLevel, filter, control);
        Vector resultVec = new Vector();
        if ((vec != null) && (vec.size() > 0))
        {
        	
        	for (int i = 0; i < vec.size(); i++)
        	{
        		if((vec.elementAt(i) instanceof TCRMEntityPrivPrefBObj)){
            		TCRMEntityPrivPrefBObj entityPrivPref = (TCRMEntityPrivPrefBObj)vec.elementAt(i);

            		CTCContractRolePrivPrefBObj contractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj)TCRMClassFactory.createBObj(CTCContractRolePrivPrefBObj.class);
            		
            		contractRolePrivPrefBObj.setControl(control);
            		
            		contractRolePrivPrefBObj.setEObjEntityPrivPref(entityPrivPref.getEObjEntityPrivPref());

            		contractRolePrivPrefBObj.setEObjPrivPref(entityPrivPref.getEObjPrivPref());

            		contractRolePrivPrefBObj.setItemsTCRMEntityInstancePrivPrefBObj(entityPrivPref.getItemsTCRMEntityInstancePrivPrefBObj());

            		contractRolePrivPrefBObj.setContractRoleId(entityPrivPref.getEObjEntityPrivPref().getPprefInstancePK().toString());

            		contractRolePrivPrefBObj.setPrivPrefValue(entityPrivPref.getPrivPrefValue());

            		contractRolePrivPrefBObj.setPrivPrefReasonValue(entityPrivPref.getPrivPrefReasonValue());

            		contractRolePrivPrefBObj.setSourceIdentValue(entityPrivPref.getSourceIdentValue());

            		contractRolePrivPrefBObj.setPrivPrefCatType(entityPrivPref.getPrivPrefCatType());

            		contractRolePrivPrefBObj.setPrivPrefCatValue(entityPrivPref.getPrivPrefCatValue());

            		contractRolePrivPrefBObj.setPrivPrefActionType(entityPrivPref.getPrivPrefActionType());

            		contractRolePrivPrefBObj.setPrivPrefActionValue(entityPrivPref.getPrivPrefActionValue());

            		resultVec.addElement(contractRolePrivPrefBObj);
        			
        		}
        		else
        		{
        			resultVec.addElement(vec.elementAt(i));
        		}
        	}

        }
        
        status.setStatus(DWLStatus.SUCCESS);
        response.setStatus(status);
        response.setData(resultVec);
        return response;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     */
    public void beforePreExecuteGetAllCTCContractRolePrivacyPreferences(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
        
        // MDM_TODO Add argument validation logic
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCContractRolePrivacyPreferenceByIdPK.
     *
     * @param theCTCContractRolePrivPrefpkId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetCTCContractRolePrivacyPreferenceByIdPK
     * @generated NOT
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETCTCCONTRACTROLEPRIVACYPREFERENCEBYIDPK_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetCTCContractRolePrivacyPreferenceByIdPK"
        )
     public DWLResponse getCTCContractRolePrivacyPreferenceByIdPK(String idPK,  DWLControl control) throws DWLBaseException {
        logger.finest("ENTER getCTCContractRolePrivacyPreferenceByIdPK(String idPK,  DWLControl control)");

        Vector params = new Vector();
        params.add(idPK);
        DWLTransaction txObj = new  DWLTransactionInquiry("getCTCContractRolePrivacyPreferenceByIdPK", params, control);
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before execution of transaction getCTCContractRolePrivacyPreferenceByIdPK.";
            logger.finest("getCTCContractRolePrivacyPreferenceByIdPK(String idPK,  DWLControl control) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After execution of transaction getCTCContractRolePrivacyPreferenceByIdPK.";
            logger.finest("getCTCContractRolePrivacyPreferenceByIdPK(String idPK,  DWLControl control) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN getCTCContractRolePrivacyPreferenceByIdPK(String idPK,  DWLControl control) " + returnValue);

        }
        return retObj;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a CTCContractRolePrivPref from the database.
     * 
     * @generated NOT
     */
    public DWLResponse handleGetCTCContractRolePrivacyPreferenceByIdPK(String idPK,  DWLControl control) throws Exception {
        DWLStatus status = new DWLStatus();
        DWLResponse response = createDWLResponse();
          
        IPrivacyPreference privPrefComp = (IPrivacyPreference)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PRIVACY_PREFERENCE_COMPONENT);

        TCRMEntityPrivPrefBObj entityPrivPref = privPrefComp.getEntityPrivacyPreferenceByIdPK(idPK, control);
        
        CTCContractRolePrivPrefBObj contractRolePrivPrefBObj = null;
        
        if (entityPrivPref != null)
        {
        	
    		contractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj)TCRMClassFactory.createBObj(CTCContractRolePrivPrefBObj.class);
    		
    		contractRolePrivPrefBObj.setControl(control);
    		
    		contractRolePrivPrefBObj.setEObjEntityPrivPref(entityPrivPref.getEObjEntityPrivPref());

    		contractRolePrivPrefBObj.setEObjPrivPref(entityPrivPref.getEObjPrivPref());

    		contractRolePrivPrefBObj.setItemsTCRMEntityInstancePrivPrefBObj(entityPrivPref.getItemsTCRMEntityInstancePrivPrefBObj());

    		contractRolePrivPrefBObj.setContractRoleId(entityPrivPref.getEObjEntityPrivPref().getPprefInstancePK().toString());

    		contractRolePrivPrefBObj.setPrivPrefValue(entityPrivPref.getPrivPrefValue());

    		contractRolePrivPrefBObj.setPrivPrefReasonValue(entityPrivPref.getPrivPrefReasonValue());

    		contractRolePrivPrefBObj.setSourceIdentValue(entityPrivPref.getSourceIdentValue());

    		contractRolePrivPrefBObj.setPrivPrefCatType(entityPrivPref.getPrivPrefCatType());

    		contractRolePrivPrefBObj.setPrivPrefCatValue(entityPrivPref.getPrivPrefCatValue());

    		contractRolePrivPrefBObj.setPrivPrefActionType(entityPrivPref.getPrivPrefActionType());

    		contractRolePrivPrefBObj.setPrivPrefActionValue(entityPrivPref.getPrivPrefActionValue());

        }
        
        response.setStatus(status);
        response.setData(contractRolePrivPrefBObj);

        return response;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void beforePreExecuteGetCTCContractRolePrivacyPreferenceByIdPK(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
        String pk = null;
        if (arguments!=null && arguments.length>0) {
            pk = (String)arguments[0];
        }
        // Check if the parameter passed in exists.
        if ((pk == null) || (pk.trim().length() == 0)) {
            TCRMExceptionUtils.throwTCRMException(null, new TCRMReadException(), transaction.getStatus(), DWLStatus.FATAL,
                CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT,
                TCRMErrorCode.READ_RECORD_ERROR,
                CTCAdditionsErrorReasonCode.GETCTCCONTRACTROLEPRIVACYPREFERENCEBYIDPK_ID_NULL,
                transaction.getTxnControl(), errHandler);
        }
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction addCTCContractRolePrivacyPreference.
     *
     * @param theBObj
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleAddCTCContractRolePrivacyPreference
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.ADD_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.INSERT_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.ADDCTCCONTRACTROLEPRIVACYPREFERENCE_FAILED
        )
     public DWLResponse addCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj) throws DWLBaseException {
        logger.finest("ENTER addCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj)");

        DWLTransaction txObj = new DWLTransactionPersistent("addCTCContractRolePrivacyPreference", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before execution of transaction addCTCContractRolePrivacyPreference.";
            logger.finest("addCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After execution of transaction addCTCContractRolePrivacyPreference.";
            logger.finest("addCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN addCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj) " + returnValue);

        }
        return retObj;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Adds a CTCContractRolePrivPref to the database.
     *
     * @param theCTCContractRolePrivPrefBObj
     *     The object that contains CTCContractRolePrivPref attribute values.
     * @return
     *     DWLResponse containing a CTCContractRolePrivPrefBObj object.
     * @exception Exception
     * @generated NOT
     */
    public DWLResponse handleAddCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theCTCContractRolePrivPrefBObj) throws Exception {
        DWLResponse response = null;

        IPrivacyPreference privPrefComp = (IPrivacyPreference)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PRIVACY_PREFERENCE_COMPONENT);

        CTCContractRolePrivPrefBObj contractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj)privPrefComp.addEntityPrivacyPreference(theCTCContractRolePrivPrefBObj);
        
        response = new DWLResponse();
        response.setData(theCTCContractRolePrivPrefBObj);
        response.setStatus(theCTCContractRolePrivPrefBObj.getStatus());
        
        return response;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction updateCTCContractRolePrivacyPreference.
     *
     * @param theBObj
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleUpdateCTCContractRolePrivacyPreference
     * @generated NOT
     */
     @TxMetadata(actionCategory=DWLControlKeys.UPDATE_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.UPDATE_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.UPDATECTCCONTRACTROLEPRIVACYPREFERENCE_FAILED
        )
     public DWLResponse updateCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj) throws DWLBaseException {
        logger.finest("ENTER updateCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj)");

        DWLTransaction txObj = new DWLTransactionPersistent("updateCTCContractRolePrivacyPreference", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before execution of transaction updateCTCContractRolePrivacyPreference.";
            logger.finest("updateCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After execution of transaction updateCTCContractRolePrivacyPreference.";
            logger.finest("updateCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN updateCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theBObj) " + returnValue);

        }
        return retObj;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Updates the specified CTCContractRolePrivPref with new attribute values.
     *
     * @param theCTCContractRolePrivPrefBObj
     *     The object that contains CTCContractRolePrivPref attribute values to
     *     be updated
     * @return
     *     DWLResponse containing a CTCContractRolePrivPrefBObj of the updated
     *     object.
     * @exception Exception
     * @generated NOT
     */
    public DWLResponse handleUpdateCTCContractRolePrivacyPreference(CTCContractRolePrivPrefBObj theCTCContractRolePrivPrefBObj) throws Exception {

        DWLResponse response = null;

        IPrivacyPreference privPrefComp = (IPrivacyPreference)TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PRIVACY_PREFERENCE_COMPONENT);

        CTCContractRolePrivPrefBObj contractRolePrivPrefBObj = (CTCContractRolePrivPrefBObj)privPrefComp.updateEntityPrivacyPreference(theCTCContractRolePrivPrefBObj);
        
        response = new DWLResponse();
        response.setData(theCTCContractRolePrivPrefBObj);
        response.setStatus(theCTCContractRolePrivPrefBObj.getStatus());
        
        return response;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated NOT
     **/
    public void loadBeforeImage(CTCContractRolePrivPrefBObj bObj) throws DWLBaseException {
        logger.finest("ENTER loadBeforeImage(CTCContractRolePrivPrefBObj bObj)");

        CTCContractRolePrivPrefBObj beforeImage = null;
        DWLResponse response = null;

        if( bObj.BeforeImage() == null ){
            
            try {
                response = getCTCContractRolePrivacyPreferenceByIdPK( bObj.getContractRolePrivPrefIdPK(), bObj.getControl());
                beforeImage = (CTCContractRolePrivPrefBObj)response.getData();
                
            } catch( Exception e){
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Exception " + e.getMessage() + " while updating a record ";
            logger.finest("loadBeforeImage(CTCContractRolePrivPrefBObj bObj) " + infoForLogging);

                }
                DWLExceptionUtils.throwDWLBaseException(e, 
                                                new DWLUpdateException(e.getMessage()), 
                                                bObj.getStatus(), DWLStatus.FATAL,
                                                CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_BOBJ, 
                                                TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                CTCAdditionsErrorReasonCode.CTCCONTRACTROLEPRIVPREF_BEFORE_IMAGE_NOT_POPULATED, 
                                                bObj.getControl(), errHandler);
            }
            
            if( beforeImage == null ){
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Before image for updating a record is null ";
            logger.finest("loadBeforeImage(CTCContractRolePrivPrefBObj bObj) " + infoForLogging);

                }
                DWLExceptionUtils.throwDWLBaseException( new DWLUpdateException(), 
                                                bObj.getStatus(), DWLStatus.FATAL,
                                                CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_BOBJ, 
                                                TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                CTCAdditionsErrorReasonCode.CTCCONTRACTROLEPRIVPREF_BEFORE_IMAGE_NOT_POPULATED, 
                                                bObj.getControl(), errHandler);
            }
            
            bObj.setBeforeImage(beforeImage);
            
        }
        
        Vector listUpdatingChildren = new Vector();

        Vector vecUpdate = bObj.getItemsTCRMEntityInstancePrivPrefBObj();

        Vector vecBeforeImage = beforeImage.getItemsTCRMEntityInstancePrivPrefBObj();

        if ((vecUpdate != null) && (vecBeforeImage != null))
        {
          for (int i = 0; i < vecUpdate.size(); i++)
          {
            TCRMEntityInstancePrivPrefBObj updBobj = (TCRMEntityInstancePrivPrefBObj)vecUpdate.elementAt(i);

            String updId = updBobj.getPrivPrefInstanceIdPK();

            if (!StringUtils.isNonBlank(updId))
              continue;
            for (int j = 0; j < vecBeforeImage.size(); j++)
            {
              TCRMEntityInstancePrivPrefBObj beforeBobj = (TCRMEntityInstancePrivPrefBObj)vecBeforeImage.elementAt(j);

              String beforeImageId = beforeBobj.getPrivPrefInstanceIdPK();

              if (updId.equalsIgnoreCase(beforeImageId)) {
                updBobj.setBeforeImage(beforeBobj);

                break;
              }
            }
          }
        }        
        logger.finest("RETURN loadBeforeImage(CTCContractRolePrivPrefBObj bObj)");

    }
     


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Builds duplicated key throwable message. There are only two elements in
     * the vector, one is the primary key, and the other is the class name.
     * @generated
     **/
    private String buildDupThrowableMessage(String[] errParams) {
        return ResourceBundleHelper.resolve(
                ResourceBundleNames.COMMON_SERVICES_STRINGS,
                EXCEPTION_DUPLICATE_KEY, errParams);

    }
  

}

