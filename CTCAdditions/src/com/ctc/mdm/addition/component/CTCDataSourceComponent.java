
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[dc8312d74702b0eb5ebe1c65e0777f99]
 */

package com.ctc.mdm.addition.component;

import com.dwl.base.DWLControl;
import com.dwl.common.globalization.util.ResourceBundleHelper;
import com.dwl.base.DWLResponse;
import com.dwl.base.constant.DWLConstantDef;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.util.DWLFunctionUtils;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.annotations.Component;
import com.ibm.mdm.annotations.TxMetadata;


import com.ctc.mdm.addition.bobj.query.CTCAdditionsModuleBObjPersistenceFactory;
import com.ctc.mdm.addition.bobj.query.CTCAdditionsModuleBObjQueryFactory;
import com.ctc.mdm.addition.bobj.query.CTCDataSourceBObjQuery;

import com.ctc.mdm.addition.component.CTCDataSourceBObj;

import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.ResourceBundleNames;

import com.ctc.mdm.addition.interfaces.CTCDataSource;

import com.dwl.base.IDWLErrorMessage;

import com.dwl.base.exception.DWLUpdateException;

import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.base.requestHandler.DWLTransactionPersistent;

import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.base.util.DWLExceptionUtils;

import com.dwl.bobj.query.BObjQuery;
import com.dwl.bobj.query.BObjQueryException;
import com.dwl.bobj.query.Persistence;

import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMControlKeys;
import com.dwl.tcrm.common.TCRMRecordFilter;

import com.dwl.tcrm.exception.TCRMInsertException;
import com.dwl.tcrm.exception.TCRMReadException;

import com.dwl.tcrm.utilities.FunctionUtils;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMExceptionUtils;

import com.ibm.mdm.common.brokers.BObjPersistenceFactoryBroker;
import com.ibm.mdm.common.brokers.BObjQueryFactoryBroker;

import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;

import com.ibm.mdm.common.codetype.obj.CodeTypeBObj;
import com.ibm.mdm.common.util.PropertyManager;

import java.sql.Timestamp;

import java.util.Vector;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Business component class for handling CTCDataSource related transactions and
 * inquiries.
 * @generated
 */
 @Component(errorComponentID = CTCAdditionsComponentID.CTCDATA_SOURCE_COMPONENT)
public class CTCDataSourceComponent extends TCRMCommonComponent implements CTCDataSource {
	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
	private final static String EXCEPTION_DUPLICATE_KEY = "Exception_Shared_DuplicateKey";

	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCDataSourceComponent.class);
			
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private IDWLErrorMessage errHandler;
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static CTCAdditionsModuleBObjQueryFactory bObjQueryFactory = null;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static CTCAdditionsModuleBObjPersistenceFactory bObjPersistenceFactory = null;
  
   

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @generated
     */
    public CTCDataSourceComponent() {
        super();
        errHandler = TCRMClassFactory.getErrorHandler();
    }






    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllCTCDataSourcesByEntity.
     *
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetAllCTCDataSourcesByEntity
     * @generated NOT
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETALLCTCDATASOURCESBYENTITY_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetAllCTCDataSourcesByEntity"
        )
     public DWLResponse getAllCTCDataSourcesByEntity(String entityName,String instancePK, String filter, DWLControl control) throws DWLBaseException {
        logger.finest("ENTER getAllCTCDataSourcesByEntity( DWLControl control)");

        Vector params = new Vector();
        params.add(entityName);
        params.add(instancePK);
        params.add(filter);
        DWLTransaction txObj = new  DWLTransactionInquiry("getAllCTCDataSourcesByEntity", params, control);
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before execution of transaction getAllCTCDataSourcesByEntity.";
            logger.finest("getAllCTCDataSourcesByEntity( DWLControl control) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After execution of transaction getAllCTCDataSourcesByEntity.";
            logger.finest("getAllCTCDataSourcesByEntity( DWLControl control) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN getAllCTCDataSourcesByEntity( DWLControl control) " + returnValue);

        }
        return retObj;
    }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a CTCDataSource from the database.
     * 
     * @generated NOT
     */
     public DWLResponse handleGetAllCTCDataSourcesByEntity(String entityName,String instancePK,String filter, DWLControl control) throws Exception {
         DWLStatus status = new DWLStatus();
         DWLResponse response = createDWLResponse();

         // MDM_TODO Add required business logic to complete the inquiry
         
         BObjQuery bObjQuery = null;
         
         String asOfDate = (String) control.get(DWLControl.INQUIRE_AS_OF_DATE);

         // History data inquiry: if inquireAsOfDate field has value in request xml 
         if (StringUtils.isNonBlank(asOfDate)) {
             Timestamp tsAsOfDate = getPITHistoryDate(asOfDate, CTCAdditionsComponentID.CTCDATA_SOURCE_COMPONENT,
                                                      CTCAdditionsErrorReasonCode.GETCTCDATASOURCE_INVALID_INQUIRE_AS_OF_DATE_FORMAT,
                                                      status, control);

             bObjQuery = getBObjQueryFactory().createCTCDataSourceBObjQuery(CTCDataSourceBObjQuery.CTCDATA_SOURCE_BY_ENTITY_HISTORY_QUERY,
                         control);
             bObjQuery.setParameter(0, entityName);
             bObjQuery.setParameter(1, DWLFunctionUtils.getLongFromString(instancePK));
             bObjQuery.setParameter(2, tsAsOfDate);
             bObjQuery.setParameter(3, tsAsOfDate);
             
         } else if (filter.equals(TCRMRecordFilter.ACTIVE)){
        	 bObjQuery = getBObjQueryFactory().createCTCDataSourceBObjQuery(CTCDataSourceBObjQuery.CTCDATA_SOURCE_BY_ENTITY_ACTIVE_QUERY,
                     control);
        	 bObjQuery.setParameter(0, entityName);
             bObjQuery.setParameter(1, DWLFunctionUtils.getLongFromString(instancePK));
             bObjQuery.setParameter(2, DWLDateTimeUtilities.getCurrentSystemTimeAsTimestamp());
      
         } else if (filter.equals(TCRMRecordFilter.INACTIVE)){
        	 bObjQuery = getBObjQueryFactory().createCTCDataSourceBObjQuery(CTCDataSourceBObjQuery.CTCDATA_SOURCE_BY_ENTITY_INACTIVE_QUERY,
                     control);
        	 bObjQuery.setParameter(0, entityName);
             bObjQuery.setParameter(1, DWLFunctionUtils.getLongFromString(instancePK));
             bObjQuery.setParameter(2, DWLDateTimeUtilities.getCurrentSystemTimeAsTimestamp());
         }
         else {
             bObjQuery = getBObjQueryFactory().createCTCDataSourceBObjQuery(CTCDataSourceBObjQuery.CTCDATA_SOURCE_BY_ENTITY_ALL_QUERY,
                         control);
             bObjQuery.setParameter(0, entityName);
             bObjQuery.setParameter(1, DWLFunctionUtils.getLongFromString(instancePK));
         }

         Vector <CTCDataSourceBObj> o = (Vector<CTCDataSourceBObj>) bObjQuery.getResults();
         if( o == null ){
             return null;
         } 
         
         for (int i=0;i<o.size();i++){
         	 if (o.get(i).getStatus()==null) {
                  o.get(i).setStatus(status);
              }
              postRetrieveCTCDataSourceBObj(o.get(i), "0", "ALL", control); 
 	
         }

           
        
         response.addStatus(status);
         response.setData(o);

         
         
         return response;
     }





    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void beforePreExecuteGetAllCTCDataSourcesByEntity(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
        
        // MDM_TODO Add argument validation logic
    }
    

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCDataSource.
     *
     * @param DataSourceID
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetCTCDataSource
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETCTCDATASOURCE_FAILED,
       beforePreExecuteMethod = "beforePreExecuteGetCTCDataSource"
		)
     public DWLResponse getCTCDataSource(String DataSourceID,  DWLControl control) throws DWLBaseException {
		logger.finest("ENTER getCTCDataSource(String DataSourceID,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(DataSourceID);
        DWLTransaction txObj = new  DWLTransactionInquiry("getCTCDataSource", params, control);
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction getCTCDataSource.";
			logger.finest("getCTCDataSource(String DataSourceID,  DWLControl control) " + infoForLogging);
		}	
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction getCTCDataSource.";
			logger.finest("getCTCDataSource(String DataSourceID,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN getCTCDataSource(String DataSourceID,  DWLControl control) " + returnValue);
		}
        return retObj;
    }
    
    
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets a CTCDataSource from the database.
     * 
     * @generated
     */
    public DWLResponse handleGetCTCDataSource(String DataSourceID,  DWLControl control) throws Exception {
        DWLStatus status = new DWLStatus();
        DWLResponse response = createDWLResponse();
        
        BObjQuery bObjQuery = null;
           
        String asOfDate = (String) control.get(DWLControl.INQUIRE_AS_OF_DATE);

        // History data inquiry: if inquireAsOfDate field has value in request xml 
        if (StringUtils.isNonBlank(asOfDate)) {
            Timestamp tsAsOfDate = getPITHistoryDate(asOfDate, CTCAdditionsComponentID.CTCDATA_SOURCE_COMPONENT,
                                                     CTCAdditionsErrorReasonCode.GETCTCDATASOURCE_INVALID_INQUIRE_AS_OF_DATE_FORMAT,
                                                     status, control);

            bObjQuery = getBObjQueryFactory().createCTCDataSourceBObjQuery(CTCDataSourceBObjQuery.CTCDATA_SOURCE_HISTORY_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(DataSourceID));
            bObjQuery.setParameter(1, tsAsOfDate);
            bObjQuery.setParameter(2, tsAsOfDate);
        } else {
            bObjQuery = getBObjQueryFactory().createCTCDataSourceBObjQuery(CTCDataSourceBObjQuery.CTCDATA_SOURCE_QUERY,
                		control);
            bObjQuery.setParameter(0, DWLFunctionUtils.getLongFromString(DataSourceID));
        }


        CTCDataSourceBObj o = (CTCDataSourceBObj) bObjQuery.getSingleResult();
        if( o == null ){
        	return null;
        } 
        postRetrieveCTCDataSourceBObj(o, "0", "ALL", control); 
          
        if (o.getStatus()==null) {
            o.setStatus(status);
        }
        response.addStatus(o.getStatus());
        response.setData(o);

	    return response;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public void beforePreExecuteGetCTCDataSource(DWLTransaction transaction) throws DWLBaseException{
        // Check if the parameters are valid.
        Object[] arguments = getInquiryArgumentType((DWLTransactionInquiry)transaction);
        String pk = null;
        if (arguments!=null && arguments.length>0) {
            pk = (String)arguments[0];
        }
        // Check if the parameter passed in exists.
        if ((pk == null) || (pk.trim().length() == 0)) {
            TCRMExceptionUtils.throwTCRMException(null, new TCRMReadException(), transaction.getStatus(), DWLStatus.FATAL,
                CTCAdditionsComponentID.CTCDATA_SOURCE_COMPONENT,
                TCRMErrorCode.READ_RECORD_ERROR,
                CTCAdditionsErrorReasonCode.GETCTCDATASOURCE_ID_NULL,
                transaction.getTxnControl(), errHandler);
        }
    }

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction addCTCDataSource.
     *
     * @param theBObj
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleAddCTCDataSource
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.ADD_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.INSERT_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.ADDCTCDATASOURCE_FAILED
		)
     public DWLResponse addCTCDataSource(CTCDataSourceBObj theBObj) throws DWLBaseException {
		logger.finest("ENTER addCTCDataSource(CTCDataSourceBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("addCTCDataSource", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction addCTCDataSource.";
			logger.finest("addCTCDataSource(CTCDataSourceBObj theBObj) " + infoForLogging);
		}	
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction addCTCDataSource.";
			logger.finest("addCTCDataSource(CTCDataSourceBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN addCTCDataSource(CTCDataSourceBObj theBObj) " + returnValue);
		}
        return retObj;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Adds a CTCDataSource to the database.
     *
     * @param theCTCDataSourceBObj
     *     The object that contains CTCDataSource attribute values.
     * @return
     *     DWLResponse containing a CTCDataSourceBObj object.
     * @exception Exception
     * @generated
     */
    public DWLResponse handleAddCTCDataSource(CTCDataSourceBObj theCTCDataSourceBObj) throws Exception {
        DWLResponse response = null;
        DWLStatus status = theCTCDataSourceBObj.getStatus();
        if (status == null) {
            status = new DWLStatus();
            theCTCDataSourceBObj.setStatus(status);
        }

        String strPluggableID = null;

        try {
            // Pluggable Key Structure implementation
            strPluggableID = getSuppliedIdPKFromBObj(theCTCDataSourceBObj);

            if ((strPluggableID != null) && (strPluggableID.length() > 0)) {
                theCTCDataSourceBObj.getEObjCTCDataSource().setDataSourceID(FunctionUtils.getLongFromString(strPluggableID));
            } else {
                strPluggableID = null;
                theCTCDataSourceBObj.getEObjCTCDataSource().setDataSourceID(null);
            }
         Persistence theCTCDataSourceBObjPersistence = getBObjPersistenceFactory().createCTCDataSourceBObjPersistence(CTCDataSourceBObjQuery.CTCDATA_SOURCE_ADD, theCTCDataSourceBObj);
         theCTCDataSourceBObjPersistence.persistAdd();

            response = new DWLResponse();
            response.setData(theCTCDataSourceBObj);
            response.setStatus(theCTCDataSourceBObj.getStatus());
		} catch (Exception ex) {
 				TCRMExceptionUtils.throwTCRMException(ex, new TCRMInsertException(ex.getMessage()), status,
                    DWLStatus.FATAL, CTCAdditionsComponentID.CTCDATA_SOURCE_COMPONENT, TCRMErrorCode.INSERT_RECORD_ERROR,
                    CTCAdditionsErrorReasonCode.ADDCTCDATASOURCE_FAILED, theCTCDataSourceBObj.getControl(), errHandler);
        }
        
        return response;
    }
    

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction updateCTCDataSource.
     *
     * @param theBObj
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleUpdateCTCDataSource
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.UPDATE_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.UPDATE_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.UPDATECTCDATASOURCE_FAILED
		)
     public DWLResponse updateCTCDataSource(CTCDataSourceBObj theBObj) throws DWLBaseException {
		logger.finest("ENTER updateCTCDataSource(CTCDataSourceBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("updateCTCDataSource", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before execution of transaction updateCTCDataSource.";
			logger.finest("updateCTCDataSource(CTCDataSourceBObj theBObj) " + infoForLogging);
		}	
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After execution of transaction updateCTCDataSource.";
			logger.finest("updateCTCDataSource(CTCDataSourceBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN updateCTCDataSource(CTCDataSourceBObj theBObj) " + returnValue);
		}
        return retObj;
    }
    
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Updates the specified CTCDataSource with new attribute values.
     *
     * @param theCTCDataSourceBObj
     *     The object that contains CTCDataSource attribute values to be updated
     * @return
     *     DWLResponse containing a CTCDataSourceBObj of the updated object.
     * @exception Exception
     * @generated
     */
    public DWLResponse handleUpdateCTCDataSource(CTCDataSourceBObj theCTCDataSourceBObj) throws Exception {

        DWLStatus status = theCTCDataSourceBObj.getStatus();

        if (status == null) {
            status = new DWLStatus();
            theCTCDataSourceBObj.setStatus(status);
        }
        
            // set lastupdatetxid with txnid from dwlcontrol
            theCTCDataSourceBObj.getEObjCTCDataSource().setLastUpdateTxId(new Long(theCTCDataSourceBObj.getControl().getTxnId()));
         Persistence theCTCDataSourceBObjPersistence = getBObjPersistenceFactory().createCTCDataSourceBObjPersistence(CTCDataSourceBObjQuery.CTCDATA_SOURCE_UPDATE, theCTCDataSourceBObj);
         theCTCDataSourceBObjPersistence.persistUpdate();

        DWLResponse response = createDWLResponse();
        response.setData(theCTCDataSourceBObj);
        response.setStatus(theCTCDataSourceBObj.getStatus());

        return response;
    }



    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     **/
    public void loadBeforeImage(CTCDataSourceBObj bObj) throws DWLBaseException {
		logger.finest("ENTER loadBeforeImage(CTCDataSourceBObj bObj)");
    	if( bObj.BeforeImage() == null ){
    	
    		CTCDataSourceBObj beforeImage = null;
    		DWLResponse response = null;
    		
    		try {
    			response = getCTCDataSource( bObj.getDataSourceID(), bObj.getControl());
    			beforeImage = (CTCDataSourceBObj)response.getData();
    			
    		} catch( Exception e){
				if (logger.isFinestEnabled()) {
    				String infoForLogging="Error: Exception " + e.getMessage() + " while updating a record ";
			logger.finest("loadBeforeImage(CTCDataSourceBObj bObj) " + infoForLogging);
				}
	            DWLExceptionUtils.throwDWLBaseException(e, 
            									new DWLUpdateException(e.getMessage()), 
            									bObj.getStatus(), DWLStatus.FATAL,
							                    CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ, 
                                                TCRMErrorCode.UPDATE_RECORD_ERROR,
							                    CTCAdditionsErrorReasonCode.CTCDATASOURCE_BEFORE_IMAGE_NOT_POPULATED, 
							                    bObj.getControl(), errHandler);
    		}
    		
    		if( beforeImage == null ){
				if (logger.isFinestEnabled()) {
    		    	String infoForLogging="Error: Before image for updating a record is null ";
			logger.finest("loadBeforeImage(CTCDataSourceBObj bObj) " + infoForLogging);
				}
	            DWLExceptionUtils.throwDWLBaseException( new DWLUpdateException(), 
            									bObj.getStatus(), DWLStatus.FATAL,
							                    CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ, 
                                                TCRMErrorCode.UPDATE_RECORD_ERROR,
							                    CTCAdditionsErrorReasonCode.CTCDATASOURCE_BEFORE_IMAGE_NOT_POPULATED, 
							                    bObj.getControl(), errHandler);
    		}
    		
    		bObj.setBeforeImage(beforeImage);
    		
    	}
		logger.finest("RETURN loadBeforeImage(CTCDataSourceBObj bObj)");
    }
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Populates the parent CTCDataSourceBObj with all contained BObjs and Type
     * Codes.
     * @generated
    **/
    public void postRetrieveCTCDataSourceBObj(CTCDataSourceBObj theBObj, String inquiryLevel, String filter, DWLControl control) throws Exception {
		logger.finest("ENTER postRetrieveCTCDataSourceBObj(CTCDataSourceBObj theBObj, String inquiryLevel, String filter, DWLControl control)");
        // processing code types  
        String langId = (String) theBObj.getControl().get(TCRMControlKeys.LANG_ID);
        CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
	
        String AdminSystemTp = theBObj.getAdminSystemType();
        if( AdminSystemTp != null && !AdminSystemTp.equals("")){
            CodeTypeBObj AdminSystemBObj = codeTypeCompHelper
                                        .getCodeTypeByCode("cdadminsystp", langId, AdminSystemTp,
                                                    theBObj.getControl());

            if (AdminSystemBObj != null) {
                theBObj.setAdminSystemValue(AdminSystemBObj.getvalue());
            }

		}
		logger.finest("RETURN postRetrieveCTCDataSourceBObj(CTCDataSourceBObj theBObj, String inquiryLevel, String filter, DWLControl control)");
	}


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the BObjQuery factory class from the configuration.
     * @generated
    **/
    private CTCAdditionsModuleBObjQueryFactory getBObjQueryFactory() throws BObjQueryException{
        logger.finest("ENTER getBObjQueryFactory()");

        if (bObjQueryFactory == null) {
            synchronized (CTCDataSourceComponent.class) {
                if (bObjQueryFactory == null) {
                    try {
                        String className = PropertyManager.getProperty(DWLConstantDef.TCRM_APP_NAME,CTCAdditionsModuleBObjQueryFactory.BOBJ_QUERY_FACTORY);
                        Class bObjQueryFactoryClass = null;
                        bObjQueryFactoryClass = Class.forName(className);
                        bObjQueryFactory = (CTCAdditionsModuleBObjQueryFactory) bObjQueryFactoryClass.newInstance();
                        if (logger.isFinestEnabled()) {
                            String infoForLogging="Created an instance of BObj query factory " + bObjQueryFactoryClass;
            logger.finest("getBObjQueryFactory() " + infoForLogging);

                        }
                    } catch (Exception e) {
                        //DWLExceptionUtils.log(e);                
                        if (logger.isFinestEnabled()) {
                            String infoForLogging="Error: Exception " + e.getMessage() + " creating a BObj query factory.";
            logger.finest("getBObjQueryFactory() " + infoForLogging);

                        }
                        throw new BObjQueryException(e);
                    }
                }
            }
        }
        logger.finest("RETURN getBObjQueryFactory()");

        return bObjQueryFactory;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the BObjPersistenceFactory class from the configuration.
     * @generated
    **/
    private CTCAdditionsModuleBObjPersistenceFactory getBObjPersistenceFactory() throws BObjQueryException {
        if (bObjPersistenceFactory == null) {
            synchronized (CTCDataSourceComponent.class) {
                if (bObjPersistenceFactory == null) {
                    try {
                        String className = PropertyManager.getProperty(DWLConstantDef.TCRM_APP_NAME,CTCAdditionsModuleBObjPersistenceFactory.BOBJ_PERSISTENCE_FACTORY);
                        Class bObjPersistenceFactoryClass = null;
                        bObjPersistenceFactoryClass = Class.forName(className);
                        bObjPersistenceFactory = (CTCAdditionsModuleBObjPersistenceFactory) bObjPersistenceFactoryClass.newInstance();
                    } catch (Exception e) {
                        //DWLExceptionUtils.log(e);                
                        if (logger.isFinestEnabled()) {
                            String infoForLogging="Error: Exception " + e.getMessage() + " accessing the Persistence factory.";
            logger.finest("Unknown method " + infoForLogging);

                        }
                        throw new BObjQueryException(e);
                    }
                }
            }
        }
        return bObjPersistenceFactory;
    }
    
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Builds duplicated key throwable message. There are only two elements in
     * the vector, one is the primary key, and the other is the class name.
     * @generated
     **/
    @SuppressWarnings("unused")
    private String buildDupThrowableMessage(String[] errParams) {
		return ResourceBundleHelper.resolve(
				ResourceBundleNames.COMMON_SERVICES_STRINGS,
				EXCEPTION_DUPLICATE_KEY, errParams);

    }
  

}


