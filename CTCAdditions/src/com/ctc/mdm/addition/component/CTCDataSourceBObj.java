
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[2d9f8fcdada55f3e71eced604342cfbe]
 */

package com.ctc.mdm.addition.component;

import java.util.Vector;

import com.dwl.tcrm.common.TCRMCommon;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.base.DWLResponse;



import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;

import com.ctc.mdm.addition.entityObject.EObjCTCDataSource;

import com.ctc.mdm.addition.interfaces.CTCDataSource;

import com.dwl.base.DWLControl;

import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.constant.DWLUtilErrorReasonCode;

import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;

import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.exception.DWLUpdateException;

import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLExceptionUtils;
import com.dwl.base.util.DWLFunctionUtils;

import com.dwl.management.config.client.Configuration;

import com.dwl.tcrm.common.ITCRMValidation;

import com.dwl.tcrm.utilities.DateFormatter;
import com.dwl.tcrm.utilities.DateValidator;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;

import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;

import com.ibm.mdm.common.codetype.obj.CodeTypeBObj;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This class provides the implementation of the business object
 * <code>CTCDataSourceBObj</code>.
 * 
 * @see com.dwl.tcrm.common.TCRMCommon
 * @generated
 */
 

@SuppressWarnings("serial")
public class CTCDataSourceBObj extends TCRMCommon  {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    protected EObjCTCDataSource eObjCTCDataSource;
	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCDataSourceBObj.class);
		
 
	protected boolean isValidStartDate = true;
	
	protected boolean isValidEndDate = true;
	
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    protected String adminSystemValue;


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */     
    public CTCDataSourceBObj() {
        super();
        init();
        eObjCTCDataSource = new EObjCTCDataSource();
        setComponentID(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ);
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Initializes the fields required to populate the metaDataMap. Each key is
     * an element-level field of the business object.
     *
     * @generated
     */
    private void init() {
        metaDataMap.put("DataSourceID", null);
        metaDataMap.put("EntityName", null);
        metaDataMap.put("InstancePK", null);
        metaDataMap.put("AttributeName", null);
        metaDataMap.put("StartDate", null);
        metaDataMap.put("EndDate", null);
        metaDataMap.put("AdminSystemType", null);
        metaDataMap.put("AdminSystemValue", null);
        metaDataMap.put("CTCDataSourceHistActionCode", null);
        metaDataMap.put("CTCDataSourceHistCreateDate", null);
        metaDataMap.put("CTCDataSourceHistCreatedBy", null);
        metaDataMap.put("CTCDataSourceHistEndDate", null);
        metaDataMap.put("CTCDataSourceHistoryIdPK", null);
        metaDataMap.put("CTCDataSourceLastUpdateDate", null);
        metaDataMap.put("CTCDataSourceLastUpdateTxId", null);
        metaDataMap.put("CTCDataSourceLastUpdateUser", null);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Refreshes all the attributes this business object supports.
     *
     * @see com.dwl.base.DWLCommon#refreshMap()
     * @generated
     */
    public void refreshMap() {

        if (bRequireMapRefresh) {
            super.refreshMap();
            metaDataMap.put("DataSourceID", getDataSourceID());
            metaDataMap.put("EntityName", getEntityName());
            metaDataMap.put("InstancePK", getInstancePK());
            metaDataMap.put("AttributeName", getAttributeName());
            metaDataMap.put("StartDate", getStartDate());
            metaDataMap.put("EndDate", getEndDate());
            metaDataMap.put("AdminSystemType", getAdminSystemType());
            metaDataMap.put("AdminSystemValue", getAdminSystemValue());
            metaDataMap.put("CTCDataSourceHistActionCode", getCTCDataSourceHistActionCode());
            metaDataMap.put("CTCDataSourceHistCreateDate", getCTCDataSourceHistCreateDate());
            metaDataMap.put("CTCDataSourceHistCreatedBy", getCTCDataSourceHistCreatedBy());
            metaDataMap.put("CTCDataSourceHistEndDate", getCTCDataSourceHistEndDate());
            metaDataMap.put("CTCDataSourceHistoryIdPK", getCTCDataSourceHistoryIdPK());
            metaDataMap.put("CTCDataSourceLastUpdateDate", getCTCDataSourceLastUpdateDate());
            metaDataMap.put("CTCDataSourceLastUpdateTxId", getCTCDataSourceLastUpdateTxId());
            metaDataMap.put("CTCDataSourceLastUpdateUser", getCTCDataSourceLastUpdateUser());
            bRequireMapRefresh = false;
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the control object on this business object.
     *
     * @see com.dwl.base.DWLCommon#setControl(DWLControl)
     * @generated
     */
    public void setControl(DWLControl newDWLControl) {
        super.setControl(newDWLControl);

        if (eObjCTCDataSource != null) {
            eObjCTCDataSource.setControl(newDWLControl);
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the entity object associated with this business object.
     *
     * @generated
     */
    public EObjCTCDataSource getEObjCTCDataSource() {
        bRequireMapRefresh = true;
        return eObjCTCDataSource;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the entity object associated with this business object.
     *
     * @param eObjCTCDataSource
     *            The eObjCTCDataSource to set.
     * @generated
     */
    public void setEObjCTCDataSource(EObjCTCDataSource eObjCTCDataSource) {
        bRequireMapRefresh = true;
        this.eObjCTCDataSource = eObjCTCDataSource;
        if (this.eObjCTCDataSource != null && this.eObjCTCDataSource.getControl() == null) {
            DWLControl control = this.getControl();
            if (control != null) {
                this.eObjCTCDataSource.setControl(control);
            }
        }
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the dataSourceID attribute.
     * 
     * @generated
     */
    public String getDataSourceID (){
   
        return DWLFunctionUtils.getStringFromLong(eObjCTCDataSource.getDataSourceID());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the dataSourceID attribute.
     * 
     * @param newDataSourceID
     *     The new value of dataSourceID.
     * @generated
     */
    public void setDataSourceID( String newDataSourceID ) throws Exception {
        metaDataMap.put("DataSourceID", newDataSourceID);

        if (newDataSourceID == null || newDataSourceID.equals("")) {
            newDataSourceID = null;


        }
        eObjCTCDataSource.setDataSourceID( DWLFunctionUtils.getLongFromString(newDataSourceID) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the entityName attribute.
     * 
     * @generated
     */
    public String getEntityName (){
   
        return eObjCTCDataSource.getEntityName();
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the entityName attribute.
     * 
     * @param newEntityName
     *     The new value of entityName.
     * @generated
     */
    public void setEntityName( String newEntityName ) throws Exception {
        metaDataMap.put("EntityName", newEntityName);

        if (newEntityName == null || newEntityName.equals("")) {
            newEntityName = null;


        }
        eObjCTCDataSource.setEntityName( newEntityName );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the instancePK attribute.
     * 
     * @generated
     */
    public String getInstancePK (){
   
        return DWLFunctionUtils.getStringFromLong(eObjCTCDataSource.getInstancePK());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the instancePK attribute.
     * 
     * @param newInstancePK
     *     The new value of instancePK.
     * @generated
     */
    public void setInstancePK( String newInstancePK ) throws Exception {
        metaDataMap.put("InstancePK", newInstancePK);

        if (newInstancePK == null || newInstancePK.equals("")) {
            newInstancePK = null;


        }
        eObjCTCDataSource.setInstancePK( DWLFunctionUtils.getLongFromString(newInstancePK) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the attributeName attribute.
     * 
     * @generated
     */
    public String getAttributeName (){
   
        return eObjCTCDataSource.getAttributeName();
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the attributeName attribute.
     * 
     * @param newAttributeName
     *     The new value of attributeName.
     * @generated
     */
    public void setAttributeName( String newAttributeName ) throws Exception {
        metaDataMap.put("AttributeName", newAttributeName);

        if (newAttributeName == null || newAttributeName.equals("")) {
            newAttributeName = null;


        }
        eObjCTCDataSource.setAttributeName( newAttributeName );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the startDate attribute.
     * 
     * @generated
     */
    public String getStartDate (){
   
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCDataSource.getStartDate());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the startDate attribute.
     * 
     * @param newStartDate
     *     The new value of startDate.
     * @generated
     */
    public void setStartDate( String newStartDate ) throws Exception {
        metaDataMap.put("StartDate", newStartDate);
       	isValidStartDate = true;

        if (newStartDate == null || newStartDate.equals("")) {
            newStartDate = null;
            eObjCTCDataSource.setStartDate(null);


        }
		else {
        	if (DateValidator.validates(newStartDate)) {
           		eObjCTCDataSource.setStartDate(DateFormatter.getStartDateTimestamp(newStartDate));
            	metaDataMap.put("StartDate", getStartDate());
        	} else {
            	if (Configuration.getConfiguration().getConfigItem(
							"/IBM/DWLCommonServices/InternalValidation/enabled").getBooleanValue()) {
                	if (metaDataMap.get("StartDate") != null) {
                    	metaDataMap.put("StartDate", "");
                	}
                	isValidStartDate = false;
                	eObjCTCDataSource.setStartDate(null);
            	}
        	}
        }
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the endDate attribute.
     * 
     * @generated
     */
    public String getEndDate (){
   
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCDataSource.getEndDate());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the endDate attribute.
     * 
     * @param newEndDate
     *     The new value of endDate.
     * @generated
     */
    public void setEndDate( String newEndDate ) throws Exception {
        metaDataMap.put("EndDate", newEndDate);
       	isValidEndDate = true;

        if (newEndDate == null || newEndDate.equals("")) {
            newEndDate = null;
            eObjCTCDataSource.setEndDate(null);


        }
		else {
        	if (DateValidator.validates(newEndDate)) {
           		eObjCTCDataSource.setEndDate(DateFormatter.getStartDateTimestamp(newEndDate));
            	metaDataMap.put("EndDate", getEndDate());
        	} else {
            	if (Configuration.getConfiguration().getConfigItem(
							"/IBM/DWLCommonServices/InternalValidation/enabled").getBooleanValue()) {
                	if (metaDataMap.get("EndDate") != null) {
                    	metaDataMap.put("EndDate", "");
                	}
                	isValidEndDate = false;
                	eObjCTCDataSource.setEndDate(null);
            	}
        	}
        }
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the adminSystemType attribute.
     * 
     * @generated
     */
    public String getAdminSystemType (){
   
        return DWLFunctionUtils.getStringFromLong(eObjCTCDataSource.getAdminSystem());
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the adminSystemType attribute.
     * 
     * @param newAdminSystemType
     *     The new value of adminSystemType.
     * @generated
     */
    public void setAdminSystemType( String newAdminSystemType ) throws Exception {
        metaDataMap.put("AdminSystemType", newAdminSystemType);

        if (newAdminSystemType == null || newAdminSystemType.equals("")) {
            newAdminSystemType = null;


        }
        eObjCTCDataSource.setAdminSystem( DWLFunctionUtils.getLongFromString(newAdminSystemType) );
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the adminSystemValue attribute.
     * 
     * @generated
     */
    public String getAdminSystemValue (){
      return adminSystemValue;
    }
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the adminSystemValue attribute.
     * 
     * @param newAdminSystemValue
     *     The new value of adminSystemValue.
     * @generated
     */
    public void setAdminSystemValue( String newAdminSystemValue ) throws Exception {
        metaDataMap.put("AdminSystemValue", newAdminSystemValue);

        if (newAdminSystemValue == null || newAdminSystemValue.equals("")) {
            newAdminSystemValue = null;


        }
        adminSystemValue = newAdminSystemValue;
     }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateTxId attribute.
     *
     * @generated
     */
    public String getCTCDataSourceLastUpdateTxId() {
        return DWLFunctionUtils.getStringFromLong(eObjCTCDataSource.getLastUpdateTxId());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateUser attribute.
     *
     * @generated
     */
    public String getCTCDataSourceLastUpdateUser() {
        return eObjCTCDataSource.getLastUpdateUser();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the LastUpdateDt attribute.
     *
     * @generated
     */
    public String getCTCDataSourceLastUpdateDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCDataSource.getLastUpdateDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateTxId attribute.
     *
     * @param newLastUpdateTxId
     *     The new value of LastUpdateTxId.
     * @generated
     */
    public void setCTCDataSourceLastUpdateTxId(String newLastUpdateTxId) {
        metaDataMap.put("CTCDataSourceLastUpdateTxId", newLastUpdateTxId);

        if ((newLastUpdateTxId == null) || newLastUpdateTxId.equals("")) {
            newLastUpdateTxId = null;
        }
        eObjCTCDataSource.setLastUpdateTxId(DWLFunctionUtils.getLongFromString(newLastUpdateTxId));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateUser attribute.
     *
     * @param newLastUpdateUser
     *     The new value of LastUpdateUser.
     * @generated
     */
    public void setCTCDataSourceLastUpdateUser(String newLastUpdateUser) {
        metaDataMap.put("CTCDataSourceLastUpdateUser", newLastUpdateUser);

        if ((newLastUpdateUser == null) || newLastUpdateUser.equals("")) {
            newLastUpdateUser = null;
        }
        eObjCTCDataSource.setLastUpdateUser(newLastUpdateUser);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the LastUpdateDt attribute.
     *
     * @param newLastUpdateDt
     *     The new value of LastUpdateDt.
     * @throws Exception
     * @generated
     */
    public void setCTCDataSourceLastUpdateDate(String newLastUpdateDt) throws Exception {
        metaDataMap.put("CTCDataSourceLastUpdateDate", newLastUpdateDt);

        if ((newLastUpdateDt == null) || newLastUpdateDt.equals("")) {
            newLastUpdateDt = null;
        }

        eObjCTCDataSource.setLastUpdateDt(DWLFunctionUtils.getTimestampFromTimestampString(newLastUpdateDt));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCDataSourceHistActionCode history attribute.
     *
     * @generated
     */
    public String getCTCDataSourceHistActionCode() {
        return eObjCTCDataSource.getHistActionCode();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCDataSourceHistActionCode history attribute.
     *
     * @param aCTCDataSourceHistActionCode
     *     The new value of CTCDataSourceHistActionCode.
     * @generated
     */
    public void setCTCDataSourceHistActionCode(String aCTCDataSourceHistActionCode) {
        metaDataMap.put("CTCDataSourceHistActionCode", aCTCDataSourceHistActionCode);

        if ((aCTCDataSourceHistActionCode == null) || aCTCDataSourceHistActionCode.equals("")) {
            aCTCDataSourceHistActionCode = null;
        }
        eObjCTCDataSource.setHistActionCode(aCTCDataSourceHistActionCode);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCDataSourceHistCreateDate history attribute.
     *
     * @generated
     */
    public String getCTCDataSourceHistCreateDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCDataSource.getHistCreateDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCDataSourceHistCreateDate history attribute.
     *
     * @param aCTCDataSourceHistCreateDate
     *     The new value of CTCDataSourceHistCreateDate.
     * @generated
     */
    public void setCTCDataSourceHistCreateDate(String aCTCDataSourceHistCreateDate) throws Exception{
        metaDataMap.put("CTCDataSourceHistCreateDate", aCTCDataSourceHistCreateDate);

        if ((aCTCDataSourceHistCreateDate == null) || aCTCDataSourceHistCreateDate.equals("")) {
            aCTCDataSourceHistCreateDate = null;
        }

        eObjCTCDataSource.setHistCreateDt(DWLFunctionUtils.getTimestampFromTimestampString(aCTCDataSourceHistCreateDate));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCDataSourceHistCreatedBy history attribute.
     *
     * @generated
     */
    public String getCTCDataSourceHistCreatedBy() {
        return eObjCTCDataSource.getHistCreatedBy();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCDataSourceHistCreatedBy history attribute.
     *
     * @param aCTCDataSourceHistCreatedBy
     *     The new value of CTCDataSourceHistCreatedBy.
     * @generated
     */
    public void setCTCDataSourceHistCreatedBy(String aCTCDataSourceHistCreatedBy) {
        metaDataMap.put("CTCDataSourceHistCreatedBy", aCTCDataSourceHistCreatedBy);

        if ((aCTCDataSourceHistCreatedBy == null) || aCTCDataSourceHistCreatedBy.equals("")) {
            aCTCDataSourceHistCreatedBy = null;
        }

        eObjCTCDataSource.setHistCreatedBy(aCTCDataSourceHistCreatedBy);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCDataSourceHistEndDate history attribute.
     *
     * @generated
     */
    public String getCTCDataSourceHistEndDate() {
        return DWLFunctionUtils.getStringFromTimestamp(eObjCTCDataSource.getHistEndDt());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCDataSourceHistEndDate history attribute.
     *
     * @param aCTCDataSourceHistEndDate
     *     The new value of CTCDataSourceHistEndDate.
     * @generated
     */
    public void setCTCDataSourceHistEndDate(String aCTCDataSourceHistEndDate) throws Exception{
        metaDataMap.put("CTCDataSourceHistEndDate", aCTCDataSourceHistEndDate);

        if ((aCTCDataSourceHistEndDate == null) || aCTCDataSourceHistEndDate.equals("")) {
            aCTCDataSourceHistEndDate = null;
        }
        eObjCTCDataSource.setHistEndDt(DWLFunctionUtils.getTimestampFromTimestampString(aCTCDataSourceHistEndDate));
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the CTCDataSourceHistoryIdPK history attribute.
     *
     * @generated
     */
    public String getCTCDataSourceHistoryIdPK() {
        return DWLFunctionUtils.getStringFromLong(eObjCTCDataSource.getHistoryIdPK());
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the CTCDataSourceHistoryIdPK history attribute.
     *
     * @param aCTCDataSourceHistoryIdPK
     *     The new value of CTCDataSourceHistoryIdPK.
     * @generated
     */
    public void setCTCDataSourceHistoryIdPK(String aCTCDataSourceHistoryIdPK) {
        metaDataMap.put("CTCDataSourceHistoryIdPK", aCTCDataSourceHistoryIdPK);

        if ((aCTCDataSourceHistoryIdPK == null) || aCTCDataSourceHistoryIdPK.equals("")) {
            aCTCDataSourceHistoryIdPK = null;
        }
        eObjCTCDataSource.setHistoryIdPK(DWLFunctionUtils.getLongFromString(aCTCDataSourceHistoryIdPK));
    }
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an add transaction.
     *
     * @generated NOT
     */
    public DWLStatus validateAdd(int level, DWLStatus status) throws Exception {

        status = super.validateAdd(level, status);
        String entityName = this.getEntityName();
        String instancePK = this.getInstancePK();
        
        /**
         *  BELOW CODE ARE COMMENTED AS THE LOGIC WILL NOT BE PERFORMED AT CONTROLLER LEVEL
         *   @generated NOT
         */
//        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
//
//        }

        status = getValidationStatus(level, status);
        if(status != null && status.getDwlErrorGroup() != null && status.getDwlErrorGroup().size() > 0){
        	return status;
        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
        	CTCDataSource comp = null;
            
            //get the CTCDataSource component
            comp = (CTCDataSource)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
            DWLResponse response = comp.getAllCTCDataSourcesByEntity(entityName, instancePK, TCRMRecordFilter.ACTIVE, this.getControl());
			// get the response from getAllCTCDataSourcesByEntity
            Vector<CTCDataSourceBObj> vecExistingDataSources = (Vector<CTCDataSourceBObj>)response.getData();
			
            if(vecExistingDataSources != null && vecExistingDataSources.size() > 0){
				for (CTCDataSourceBObj singleDataSource : vecExistingDataSources){
					
					//if any match found between input CTCDataSourceBObj and any of the existing CTCDataSourceBObjs, generate error
					if ((StringUtils.compareIgnoreCaseWithTrim(singleDataSource.getAdminSystemType(), this.getAdminSystemType())) && 
							(StringUtils.compareIgnoreCaseWithTrim(singleDataSource.getAttributeName(), this.getAttributeName()))){
							
						//create duplicated record error
						 	DWLError err = new DWLError();
			                err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
			                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.DUPLICATE_RECORD).longValue());
			                err.setErrorType(DWLErrorCode.DUPLICATE_RECORD_ERROR);
			                status.addError(err);
					}
					
				}
				
			}
        }
        return status;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation during an update transaction.
     *
     * @generated NOT
     */
    public DWLStatus validateUpdate(int level, DWLStatus status) throws Exception {
		logger.finest("ENTER validateUpdate(int level, DWLStatus status)");

        status = super.validateUpdate(level, status);
        
/**
 *  BELOW CODE ARE COMMENTED AS THE LOGIC WILL NOT BE PERFORMED AT CONTROLLER LEVEL
 *   @generated NOT
 */

//        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
//            if (eObjCTCDataSource.getDataSourceID() == null) {
//                DWLError err = new DWLError();
//                err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
//                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCDATASOURCE_DATASOURCEID_NULL).longValue());
//                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
//                if (logger.isFinestEnabled()) {
//                    String infoForLogging="Error: Validation error occured for update. No primary key for entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
//            logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);
//
//                }
//                status.addError(err);
//            }
//            if (eObjCTCDataSource.getLastUpdateDt() == null) {
//                DWLError err = new DWLError();
//                err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
//                err.setReasonCode(new Long(DWLUtilErrorReasonCode.LAST_UPDATED_DATE_NULL).longValue());
//                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
//                if (logger.isFinestEnabled()) {
//                    String infoForLogging="Error: Validation error occured for update. No last update date for entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
//            logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);
//
//                }
//                status.addError(err);
//            }
//        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
        	if(eObjCTCDataSource.getDataSourceID() == null ){
        		DWLError err = new DWLError();
        		err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCDATASOURCE_DATASOURCEID_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured for update. No primary key for entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);

                }
                status.addError(err);
        	}  
        	if (eObjCTCDataSource.getLastUpdateDt() == null){
        		DWLError err = new DWLError();
        		err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
                err.setReasonCode(new Long(DWLUtilErrorReasonCode.LAST_UPDATED_DATE_NULL).longValue());
                err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                if (logger.isFinestEnabled()) {
                    String infoForLogging="Error: Validation error occured for update. No last update date for entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
            logger.finest("validateUpdate(int level, DWLStatus status) " + infoForLogging);
                }
                status.addError(err);       		
        	}
            assignBeforeImageValues(metaDataMap);
        }
        status = getValidationStatus(level, status);
        if (logger.isFinestEnabled()) {
            String returnValue = status.toString();
            logger.finest("RETURN validateUpdate(int level, DWLStatus status) " + returnValue);
        }
        return status;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Populate the before image of this business object.
     *
     * @see com.dwl.base.DWLCommon#populateBeforeImage()
     * @generated NOT
     */
    public void populateBeforeImage() throws DWLBaseException {
        logger.finest("ENTER populateBeforeImage()");
        CTCDataSource comp = null;
        try {
        
            comp = (CTCDataSource)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
            
        } catch (Exception e) {
            if (logger.isFinestEnabled()) {
                String infoForLogging="Error: Fatal error while updating record " + e.getMessage();
            logger.finest("populateBeforeImage() " + infoForLogging);

            }
            DWLExceptionUtils.throwDWLBaseException(e, 
                                                new DWLUpdateException(e.getMessage()), 
                                                this.getStatus(), DWLStatus.FATAL,
                                                CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ, 
                                                TCRMErrorCode.UPDATE_RECORD_ERROR,
                                                CTCAdditionsErrorReasonCode.CTCDATASOURCE_BEFORE_IMAGE_NOT_POPULATED, 
                                                this.getControl());
        }
        comp.loadBeforeImage(this);
        logger.finest("RETURN populateBeforeImage()");

    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Perform validation common to both add and update transactions.
     *
     * @generated NOT
     */
     
    private DWLStatus getValidationStatus(int level, DWLStatus status) throws Exception {
        logger.finest("ENTER getValidationStatus(int level, DWLStatus status)");
/**
 * BELOW CODE ARE COMMENTED AS THE LOGIC WILL NOT BE PERFORMED AT CONTROLLER LEVEL
 *  @generated NOT
 */
        
//
//        if (level == ITCRMValidation.CONTROLLER_LEVEL_VALIDATION) {
//            // MDM_TODO: Add any common controller-level custom validation logic
//            // to be executed for this object during either "add" or "update"
//            // transactions
//
//            boolean isDataSourceIDNull = (eObjCTCDataSource.getDataSourceID() == null);
//            boolean isInstancePKNull = (eObjCTCDataSource.getInstancePK() == null);
//            boolean isStartDateNull = (eObjCTCDataSource.getStartDate() == null);
//            if (!isValidStartDate) {
//                DWLError err = new DWLError();
//                err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
//                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.INVALID_CTCDATASOURCE_STARTDATE).longValue());
//                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
//                String infoForLogging="Error: Validation error. Invalid time specified on property StartDate in entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
//                status.addError(err);
//            } 
//            boolean isEndDateNull = (eObjCTCDataSource.getEndDate() == null);
//            if (!isValidEndDate) {
//                DWLError err = new DWLError();
//                err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
//                err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.INVALID_CTCDATASOURCE_ENDDATE).longValue());
//                err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
//                String infoForLogging="Error: Validation error. Invalid time specified on property EndDate in entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
//                status.addError(err);
//            } 
//            //persistent code type
//            boolean isAdminSystemNull = false;
//            if ((eObjCTCDataSource.getAdminSystem() == null)
//                 && ((getAdminSystemValue() == null) || getAdminSystemValue()
//                     .trim().equals(""))) {
//                isAdminSystemNull = true;
//            }
//            if( !isAdminSystemNull ){
//                if( checkForInvalidCtcdatasourceAdminsystem()){
//                    DWLError err = new DWLError();
//                    err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
//                    err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.INVALID_CTCDATASOURCE_ADMINSYSTEM).longValue());
//                    err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
//                    if (logger.isFinestEnabled()) {
//                        String infoForLogging="Error: Custom validation error occured for entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
//            logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);
//
//                    }
//                    status.addError(err);
//                 }
//            }
//        }

        if (level == ITCRMValidation.COMPONENT_LEVEL_VALIDATION){
        	
             boolean isInstancePKNull = (eObjCTCDataSource.getInstancePK() == null);
             if (isInstancePKNull) {
                 DWLError err = new DWLError();
                 err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
                 err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCDATASOURCE_INSTANCEPK_NULL).longValue());
                 err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                 String infoForLogging="Error: Validation error. NULL value specified on property InstancePK in entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                 status.addError(err);
             } 
             
             boolean isStartDateNull = (eObjCTCDataSource.getStartDate() == null);
             if (isStartDateNull) {
                 DWLError err = new DWLError();
                 err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
                 err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.CTCDATASOURCE_STARTDATE_NULL).longValue());
                 err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                 String infoForLogging="Error: Validation error. NULL value specified on property StartDate in entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                 status.addError(err);
             } 
             
             if (!isValidStartDate) {
                 DWLError err = new DWLError();
                 err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
                 err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.INVALID_CTCDATASOURCE_STARTDATE).longValue());
                 err.setErrorType(DWLErrorCode.DATA_INVALID_ERROR);
                 String infoForLogging="Error: Validation error. Invalid time specified on property StartDate in entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode();
                 status.addError(err);
             } 
             
             //persistent code type
             boolean isAdminSystemNull = false;
             if ((eObjCTCDataSource.getAdminSystem() == null)
                  && ((getAdminSystemValue() == null) || getAdminSystemValue()
                      .trim().equals(""))) {
                 isAdminSystemNull = true;
             }
             if( !isAdminSystemNull ){
                 if( checkForInvalidCtcdatasourceAdminsystem()){
                     DWLError err = new DWLError();
                     err.setComponentType(new Long(CTCAdditionsComponentID.CTCDATA_SOURCE_BOBJ).longValue());
                     err.setReasonCode(new Long(CTCAdditionsErrorReasonCode.INVALID_CTCDATASOURCE_ADMINSYSTEM).longValue());
                     err.setErrorType(DWLErrorCode.FIELD_VALIDATION_ERROR);
                     if (logger.isFinestEnabled()) {
                         String infoForLogging="Error: Custom validation error occured for entity CTCDataSource, component type " +err.getComponentType() + " ReasonCode " +err.getReasonCode() + "  ";
             logger.finest("getValidationStatus(int level, DWLStatus status) " + infoForLogging);

                     }
                     status.addError(err);
                  }
             }
        }
        
        if (logger.isFinestEnabled()) {
			String returnValue = status.toString();
			logger.finest("RETURN getValidationStatus(int level, DWLStatus status) " + returnValue);
		}
		
        return status;
    }
    
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Check the value of the field AdminSystem and return true if the error
     * reason INVALID_CTCDATASOURCE_ADMINSYSTEM should be returned.
     *
     * @generated NOT
    **/
    private boolean checkForInvalidCtcdatasourceAdminsystem() throws Exception {
        logger.finest("ENTER checkForInvalidCtcdatasourceAdminsystem()");

        boolean notValid = false;
        String langId = (String) this.getControl().get(DWLControlKeys.LANG_ID);
        CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
        Long codeType = DWLFunctionUtils.getLongFromString( getAdminSystemType() );
        String codeValue = getAdminSystemValue();
        if( codeValue != null && codeValue.trim().equals("")){
            codeValue = null;
        }
         
        if ( codeType != null && codeValue == null ){

             if( codeTypeCompHelper.isCodeValid("cdadminsystp", langId, getAdminSystemType(),
                 CodeTypeComponentHelper.ACTIVE, getControl())) {
                 CodeTypeBObj ctBObj = codeTypeCompHelper
                                    .getCodeTypeByCode("cdadminsystp", langId, getAdminSystemType(),
                                                        getControl());
                if (ctBObj != null) {
                    setAdminSystemValue( ctBObj.getvalue() );
                }
             }
             else{
             	if (logger.isFinestEnabled()) {
              	 	String infoForLogging="NotValid 1";
			logger.finest("checkForInvalidCtcdatasourceAdminsystem() " + infoForLogging);
				}
                 notValid = true;
	         }
        }
        else if (codeType == null && codeValue != null ){
					
             CodeTypeBObj ctBObj = codeTypeCompHelper
                      .getCodeTypeByValue("cdadminsystp", langId, codeValue,
                                 getControl());

             if (ctBObj != null) {
                 setAdminSystemType(ctBObj.gettp_cd());
             } 
             else {
             	if (logger.isFinestEnabled()) {
              	  	String infoForLogging="NotValid 2";
			logger.finest("checkForInvalidCtcdatasourceAdminsystem() " + infoForLogging);
				}
                  notValid = true;
             }
        }
        else if ( codeType != null && codeValue != null
             && !codeTypeCompHelper.isCodeValuePairValid("cdadminsystp", langId, getAdminSystemType(), 
                     new String[] { codeValue }, CodeTypeComponentHelper.ACTIVE, getControl())) {   
             if (logger.isFinestEnabled()) {
                String infoForLogging="NotValid 3";
            logger.finest("checkForInvalidCtcdatasourceAdminsystem() " + infoForLogging);

             }
             notValid = true;
        }
        if (logger.isFinestEnabled()) {
			String returnValue ="" +notValid;
			logger.finest("RETURN checkForInvalidCtcdatasourceAdminsystem() " + returnValue);
		}
		return notValid;
     } 
				 



}


