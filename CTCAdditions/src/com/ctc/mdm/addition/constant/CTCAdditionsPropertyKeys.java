/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[3a3a13045ed9c16c75dd5fff5ea8dc93]
 */

package com.ctc.mdm.addition.constant;


/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Defines the property keys for accessing component classes and query factory
 * classes in the module.
 *
 * @generated
 */
public class CTCAdditionsPropertyKeys {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Property key for CTCDataSource.
     *
     * @generated
     */
    public static final String CTCDATA_SOURCE_COMPONENT = "ctcdatasource_component";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Property key for CTCPartySegment.
     *
     * @generated
     */
    public static final String CTCPARTY_SEGMENT_COMPONENT = "ctcpartysegment_component";
  //Added as part of POC By Shanu Kushwah
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Property key for CTCContractRolePrivPref.
     *
     * @generated
     */
    public static final String CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT = "ctccontractroleprivpref_component";


}


