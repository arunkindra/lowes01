/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[9910191b412d5751efee8664569f01da]
 */
package com.ctc.mdm.addition.constant;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Defines the error message codes used in this module.
 *
 * @generated
 */
public class CTCAdditionsErrorReasonCode {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: DataSourceID
     *
     * @generated
     */
    public final static String CTCDATASOURCE_DATASOURCEID_NULL = "1010004";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: InstancePK
     *
     * @generated
     */
    public final static String CTCDATASOURCE_INSTANCEPK_NULL = "1010007";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * StartDate is not correct
     *
     * @generated
     */
    public final static String INVALID_CTCDATASOURCE_STARTDATE = "1010010";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: StartDate
     *
     * @generated
     */
    public final static String CTCDATASOURCE_STARTDATE_NULL = "1010011";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * EndDate is not correct
     *
     * @generated
     */
    public final static String INVALID_CTCDATASOURCE_ENDDATE = "1010013";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * AdminSystem is not correct
     *
     * @generated
     */
    public final static String INVALID_CTCDATASOURCE_ADMINSYSTEM = "1010015";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: AdminSystem
     *
     * @generated
     */
    public final static String CTCDATASOURCE_ADMINSYSTEM_NULL = "1010016";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The before image of CTCDataSource is empty.
     *
     * @generated
     */
    public final static String CTCDATASOURCE_BEFORE_IMAGE_NOT_POPULATED = "1010017";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The transaction getAllCTCDataSourcesByEntity failed.
     *
     * @generated
     */
    public final static String GETALLCTCDATASOURCESBYENTITY_FAILED = "1010019";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * An attempt to read the CTCDataSource failed.
     *
     * @generated
     */
    public final static String GETCTCDATASOURCE_FAILED = "1010021";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The id is null.
     *
     * @generated
     */
    public final static String GETCTCDATASOURCE_ID_NULL = "1010022";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The format of inquireAsOfDate is not correct.
     *
     * @generated
     */
    public final static String GETCTCDATASOURCE_INVALID_INQUIRE_AS_OF_DATE_FORMAT = "1010023";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCDataSource insert failed.
     *
     * @generated
     */
    public final static String ADDCTCDATASOURCE_FAILED = "1010025";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Duplicate primary key already exists.
     *
     * @generated
     */
    public final static String DUPLICATE_PRIMARY_KEY_CTCDATASOURCE = "1010026";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCDataSource update failed.
     *
     * @generated
     */
    public final static String UPDATECTCDATASOURCE_FAILED = "1010028";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: PartySegmentId
     *
     * @generated
     */
    public final static String CTCPARTYSEGMENT_PARTYSEGMENTID_NULL = "2010004";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: ContId
     *
     * @generated
     */
    public final static String CTCPARTYSEGMENT_CONTID_NULL = "2010006";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: SegmentProvider
     *
     * @generated
     */
    public final static String CTCPARTYSEGMENT_SEGMENTPROVIDER_NULL = "2010008";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: BusinessGroupName
     *
     * @generated
     */
    public final static String CTCPARTYSEGMENT_BUSINESSGROUPNAME_NULL = "2010010";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: SegmentTypeName
     *
     * @generated
     */
    public final static String CTCPARTYSEGMENT_SEGMENTTYPENAME_NULL = "2010012";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: EffectiveStartDt
     *
     * @generated
     */
    public final static String CTCPARTYSEGMENT_EFFECTIVESTARTDT_NULL = "2010016";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * EffectiveStartDt is not correct
     *
     * @generated
     */
    public final static String INVALID_CTCPARTYSEGMENT_EFFECTIVESTARTDT = "2010017";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * EffectiveEndDt is not correct
     *
     * @generated
     */
    public final static String INVALID_CTCPARTYSEGMENT_EFFECTIVEENDDT = "2010019";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The before image of CTCPartySegment is empty.
     *
     * @generated
     */
    public final static String CTCPARTYSEGMENT_BEFORE_IMAGE_NOT_POPULATED = "2010020";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * An attempt to read the CTCPartySegment failed.
     *
     * @generated
     */
    public final static String GETCTCPARTYSEGMENT_FAILED = "2010022";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The id is null.
     *
     * @generated
     */
    public final static String GETCTCPARTYSEGMENT_ID_NULL = "2010023";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The format of inquireAsOfDate is not correct.
     *
     * @generated
     */
    public final static String GETCTCPARTYSEGMENT_INVALID_INQUIRE_AS_OF_DATE_FORMAT = "2010024";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * An attempt to read the CTCPartySegment failed.
     *
     * @generated
     */
    public final static String GETALLCTCPARTYSEGMENT_FAILED = "2010026";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The id is null.
     *
     * @generated
     */
    public final static String GETALLCTCPARTYSEGMENT_ID_NULL = "2010027";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The format of inquireAsOfDate is not correct.
     *
     * @generated
     */
    public final static String GETALLCTCPARTYSEGMENT_INVALID_INQUIRE_AS_OF_DATE_FORMAT = "2010028";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartySegment insert failed.
     *
     * @generated
     */
    public final static String ADDCTCPARTYSEGMENT_FAILED = "2010030";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Duplicate primary key already exists.
     *
     * @generated
     */
    public final static String DUPLICATE_PRIMARY_KEY_CTCPARTYSEGMENT = "2010031";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCPartySegment update failed.
     *
     * @generated
     */
    public final static String UPDATECTCPARTYSEGMENT_FAILED = "2010033";
	
	 /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTC Party Segment business key validation
     *
     * @generated NOT
     */
    public final static String CTCPARTYSEGMENT_BUSINESS_KEY_VALIDATION = "2010139";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Party is not exist or in-active
     *
     * @generated NOT
     */
    public final static String CTC_PARTY_NOT_EXIST_OR_INACTIVE ="2010138";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCDataSource update failed.
     *
     * @generated NOT
     */
    public final static String DUPLICATE_RECORD = "1010029";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCDataSource update failed.
     *
     * @generated NOT
     */
    public final static String CTCDATASOURCE_ENDDATE_NULL = "1010030";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The following is required: CTCContractRolePrivPrefpkId
     *
     * @generated
     */
    public final static String CTCCONTRACTROLEPRIVPREF_CTCCONTRACTROLEPRIVPREFPKID_NULL = "1010032";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The before image of CTCContractRolePrivPref is empty.
     *
     * @generated
     */
    public final static String CTCCONTRACTROLEPRIVPREF_BEFORE_IMAGE_NOT_POPULATED = "1010033";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The transaction getCTCContractRolePrivacyPreference failed.
     *
     * @generated
     */
    public final static String GETCTCCONTRACTROLEPRIVACYPREFERENCE_FAILED = "1010035";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The transaction getCTCContractRolePrivacyPreference failed.
     *
     * @generated
     */
    public final static String CTCCONTRACTROLEPRIVACYPREFERENCE_RECORD_NOT_FOUND = "1010051";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The transaction getAllCTCContractRolePrivacyPreferences failed.
     *
     * @generated
     */
    public final static String GETALLCTCCONTRACTROLEPRIVACYPREFERENCES_FAILED = "1010037";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * An attempt to read the CTCContractRolePrivPref failed.
     *
     * @generated
     */
    public final static String GETCTCCONTRACTROLEPRIVACYPREFERENCEBYIDPK_FAILED = "1010039";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The id is null.
     *
     * @generated
     */
    public final static String GETCTCCONTRACTROLEPRIVACYPREFERENCEBYIDPK_ID_NULL = "1010040";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * The format of inquireAsOfDate is not correct.
     *
     * @generated
     */
    public final static String GETCTCCONTRACTROLEPRIVACYPREFERENCEBYIDPK_INVALID_INQUIRE_AS_OF_DATE_FORMAT = "1010041";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCContractRolePrivPref insert failed.
     *
     * @generated
     */
    public final static String ADDCTCCONTRACTROLEPRIVACYPREFERENCE_FAILED = "1010043";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Duplicate primary key already exists.
     *
     * @generated
     */
    public final static String DUPLICATE_PRIMARY_KEY_CTCCONTRACTROLEPRIVPREF = "1010044";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCContractRolePrivPref update failed.
     *
     * @generated
     */
    public final static String UPDATECTCCONTRACTROLEPRIVACYPREFERENCE_FAILED = "1010046";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCContractRolePrivPref ContractRoleId Missing.
     *
     * @generated
     */
    public final static String MISSING_CONTRACTROLEID_CTCCONTRACTROLEPRIVACYPREFERENCE = "1010047";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCContractRolePrivPref ContractPartyRole Not Found.
     *
     * @generated
     */
    public final static String CONTRACTPARTYROLE_NOT_FOUND_CTCCONTRACTROLEPRIVACYPREFERENCE = "1010048";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCContractRolePrivPref update ContractRoleId failed.
     *
     * @generated
     */
    public final static String UPDATE_CONTRACTROLEID_CTCCONTRACTROLEPRIVACYPREFERENCE_FAILED = "1010049";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * CTCContractRolePrivPref before image is empty for CTCContractRolePrivPref.
     *
     * @generated
     */
    
    public final static String BEFORE_IMAGE_EMPTY_CTCCONTRACTROLEPRIVACYPREFERENCE = "1010050";

}


