/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[9ef02bd94cc8353692eb3f1c4e779034]
 */

package com.ctc.mdm.addition.constant;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Defines the component IDs used in this module.
 *
 * @generated
 */
public class CTCAdditionsComponentID {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCDataSourceComponent.
     *
     * @generated
     */
    public final static String CTCDATA_SOURCE_COMPONENT = "1010041";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCDataSourceController.
     *
     * @generated
     */
    public final static String CTCDATA_SOURCE_CONTROLLER = "1010039";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCDataSourceBObj.
     *
     * @generated
     */
    public final static String CTCDATA_SOURCE_BOBJ = "1010029";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCPartySegmentBObj.
     *
     * @generated
     */
    public final static String CTCPARTY_SEGMENT_BOBJ = "2010034";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCPartySegmentComponent.
     *
     * @generated
     */
    public final static String CTCPARTY_SEGMENT_COMPONENT = "2010047";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCPartySegmentController.
     *
     * @generated
     */
    public final static String CTCPARTY_SEGMENT_CONTROLLER = "2010045";
    //Added as part of POC for ContactPrivPref By Shanu Kushwah
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCContractRolePrivPrefComponent.
     *
     * @generated
     */
    public final static String CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT = "1010076";
  /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCContractRolePrivPrefBObj.
     *
     * @generated
     */
    public final static String CTCCONTRACT_ROLE_PRIV_PREF_BOBJ = "1010070";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Component ID for CTCContractRolePrivPrefController.
     *
     * @generated
     */
    public final static String CTCCONTRACT_ROLE_PRIV_PREF_CONTROLLER = "1010074";
}


