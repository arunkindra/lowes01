	
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[b687a51d7d255c08db96e607b34590ec]
 */

package com.ctc.mdm.addition.controller;

import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionPersistent;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMControlKeys;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLBaseException;
import com.ibm.mdm.annotations.Controller;
import com.ibm.mdm.annotations.TxMetadata;


import com.ctc.mdm.addition.component.CTCDataSourceBObj;

import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;

import com.ctc.mdm.addition.interfaces.CTCDataSource;
import com.ctc.mdm.addition.interfaces.CTCDataSourceTxn;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Controller implementation for all persistent CTCDataSource services.
 * @generated
 */
@Controller(errorComponentID = CTCAdditionsComponentID.CTCDATA_SOURCE_CONTROLLER)
public class CTCDataSourceTxnBean  extends TCRMCommonComponent implements CTCDataSourceTxn {

	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCDataSourceTxnBean.class);
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction handleAddCTCDataSource.
     *
     * @return DWLResponse containing the added 
     * @throws DWLBaseException
     * @see #handleaddCTCDataSource
     *
     * @ejb.interface-method
     *    view-type="both"
     *
     * @generated
     */
     @TxMetadata(actionCategory=TCRMControlKeys.ADD_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.INSERT_RECORD_ERROR, 
       txErrorReasonCode = CTCAdditionsErrorReasonCode.ADDCTCDATASOURCE_FAILED)
    public DWLResponse addCTCDataSource(CTCDataSourceBObj theBObj) throws DWLBaseException {
		logger.finest("ENTER addCTCDataSource(CTCDataSourceBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("addCTCDataSource", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before transaction addCTCDataSource execution";
			logger.finest("addCTCDataSource(CTCDataSourceBObj theBObj) " + infoForLogging);
		}
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After transaction addCTCDataSource execution";
			logger.finest("addCTCDataSource(CTCDataSourceBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN addCTCDataSource(CTCDataSourceBObj theBObj) " + returnValue);
		}
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction addCTCDataSource.
     * 
     * @return DWLResponse containing the added 
     * @throws Exception
     * @see #addCTCDataSource
     *
     * @generated
     */
    public DWLResponse handleAddCTCDataSource(CTCDataSourceBObj theBObj) throws Exception {
		logger.finest("ENTER handleAddCTCDataSource(CTCDataSourceBObj theBObj)");
        DWLResponse response = new DWLResponse();
        CTCDataSource aCTCDataSourceComponent = (CTCDataSource) TCRMClassFactory
			.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
        response = aCTCDataSourceComponent.addCTCDataSource(theBObj);
		logger.finest("RETURN handleAddCTCDataSource(CTCDataSourceBObj theBObj)");
        return response;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction handleUpdateCTCDataSource.
     *
     * @return DWLResponse containing the added 
     * @throws DWLBaseException
     * @see #handleupdateCTCDataSource
     *
     * @ejb.interface-method
     *    view-type="both"
     *
     * @generated
     */
     @TxMetadata(actionCategory=TCRMControlKeys.UPDATE_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.UPDATE_RECORD_ERROR, 
       txErrorReasonCode = CTCAdditionsErrorReasonCode.UPDATECTCDATASOURCE_FAILED)
    public DWLResponse updateCTCDataSource(CTCDataSourceBObj theBObj) throws DWLBaseException {
		logger.finest("ENTER updateCTCDataSource(CTCDataSourceBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("updateCTCDataSource", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before transaction updateCTCDataSource execution";
			logger.finest("updateCTCDataSource(CTCDataSourceBObj theBObj) " + infoForLogging);
		}
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After transaction updateCTCDataSource execution";
			logger.finest("updateCTCDataSource(CTCDataSourceBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN updateCTCDataSource(CTCDataSourceBObj theBObj) " + returnValue);
		}
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction updateCTCDataSource.
     * 
     * @return DWLResponse containing the added 
     * @throws Exception
     * @see #updateCTCDataSource
     *
     * @generated
     */
    public DWLResponse handleUpdateCTCDataSource(CTCDataSourceBObj theBObj) throws Exception {
		logger.finest("ENTER handleUpdateCTCDataSource(CTCDataSourceBObj theBObj)");
        DWLResponse response = new DWLResponse();
        CTCDataSource aCTCDataSourceComponent = (CTCDataSource) TCRMClassFactory
			.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
        response = aCTCDataSourceComponent.updateCTCDataSource(theBObj);
		logger.finest("RETURN handleUpdateCTCDataSource(CTCDataSourceBObj theBObj)");
        return response;
    }
}



