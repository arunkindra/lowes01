/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

package com.ctc.mdm.addition.controller;


import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.annotations.Controller;
import com.ibm.mdm.annotations.TxMetadata;


import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;

import com.ctc.mdm.addition.interfaces.CTCContractRolePrivPref;
import com.ctc.mdm.addition.interfaces.CTCContractRolePrivPrefFinder;

import com.dwl.base.error.DWLStatus;

import com.dwl.base.util.DWLExceptionUtils;

import java.util.Vector; 

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Controller class to handle inquiry requests.
 * @generated NOT
 */
 @Controller(errorComponentID = CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_CONTROLLER)
public class CTCContractRolePrivPrefFinderImpl extends TCRMCommonComponent implements CTCContractRolePrivPrefFinder {

    private IDWLErrorMessage errHandler;
    /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      * @generated 
      */
     private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCContractRolePrivPrefFinderImpl.class);


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     * @generated
     */
    public CTCContractRolePrivPrefFinderImpl() {
        super();
        errHandler = TCRMClassFactory.getErrorHandler();
    }

    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCContractRolePrivacyPreference.
     *
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetCTCContractRolePrivacyPreference
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETCTCCONTRACTROLEPRIVACYPREFERENCE_FAILED)
     public DWLResponse getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) throws DWLBaseException {
        logger.finest("ENTER getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control)");

        Vector params = new Vector();
        params.add(privPrefType);
        params.add(contractRoleId);
        DWLTransaction txObj = new DWLTransactionInquiry("getCTCContractRolePrivacyPreference", params, control);
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before finder transaction execution for getCTCContractRolePrivacyPreference";
            logger.finest("getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After finder transaction execution for getCTCContractRolePrivacyPreference";
            logger.finest("getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN getCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) " + returnValue);

        }
        return retObj;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction
     * getCTCContractRolePrivacyPreference.
     * 
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated NOT
     */
    public DWLResponse  handleGetCTCContractRolePrivacyPreference(String privPrefType, String contractRoleId, DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

        CTCContractRolePrivPref comp = 
            (CTCContractRolePrivPref)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT);
            
        response = comp.getCTCContractRolePrivacyPreference(privPrefType, contractRoleId, control);
        Vector vecObj = (Vector)response.getData(); 
        if (response.getData() == null || (response.getData() instanceof Vector && vecObj.isEmpty())) {
            String[] params = new String[] {  };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
                    CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_CONTROLLER,
                    TCRMErrorCode.READ_RECORD_ERROR,
                    CTCAdditionsErrorReasonCode.CTCCONTRACTROLEPRIVACYPREFERENCE_RECORD_NOT_FOUND,
                    control, params, errHandler);                   
        }
        return response;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllCTCContractRolePrivacyPreferences.
     *
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetAllCTCContractRolePrivacyPreferences
     * @generated NOT
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETALLCTCCONTRACTROLEPRIVACYPREFERENCES_FAILED)
     public DWLResponse getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) throws DWLBaseException {
        logger.finest("ENTER getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control)");

        Vector params = new Vector();
        params.add(contractRoleId);
        params.add(inquiryLevel);
        params.add(filter);
        DWLTransaction txObj = new DWLTransactionInquiry("getAllCTCContractRolePrivacyPreferences", params, control);
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before finder transaction execution for getAllCTCContractRolePrivacyPreferences";
            logger.finest("getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After finder transaction execution for getAllCTCContractRolePrivacyPreferences";
            logger.finest("getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("getAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) " + returnValue);

        }
        return retObj;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction
     * getAllCTCContractRolePrivacyPreferences.
     * 
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated NOT
     */
    public DWLResponse  handleGetAllCTCContractRolePrivacyPreferences(String contractRoleId, String inquiryLevel, String filter, DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

        CTCContractRolePrivPref comp = 
            (CTCContractRolePrivPref)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT);
            
        response = comp.getAllCTCContractRolePrivacyPreferences(contractRoleId, inquiryLevel, filter, control);
        Vector vecObj = (Vector)response.getData(); 
        if (response.getData() == null || (response.getData() instanceof Vector && vecObj.isEmpty())) {
            String[] params = new String[] {  };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
                    CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_CONTROLLER,
                    TCRMErrorCode.READ_RECORD_ERROR,
                    CTCAdditionsErrorReasonCode.CTCCONTRACTROLEPRIVACYPREFERENCE_RECORD_NOT_FOUND,
                    control, params, errHandler);                   
        }
        return response;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCContractRolePrivacyPreferenceByIdPK.
     *
     * @param theCTCContractRolePrivPrefpkId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetCTCContractRolePrivacyPreferenceByIdPK
     * @generated NOT
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETCTCCONTRACTROLEPRIVACYPREFERENCEBYIDPK_FAILED)
     public DWLResponse getCTCContractRolePrivacyPreferenceByIdPK(String idPK,  DWLControl control) throws DWLBaseException {
        logger.finest("ENTER getCTCContractRolePrivacyPreferenceByIdPK(String theCTCContractRolePrivPrefpkId,  DWLControl control)");

        Vector params = new Vector();
        params.add(idPK);
        DWLTransaction txObj = new DWLTransactionInquiry("getCTCContractRolePrivacyPreferenceByIdPK", params, control);
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before finder transaction execution for getCTCContractRolePrivacyPreferenceByIdPK";
            logger.finest("getCTCContractRolePrivacyPreferenceByIdPK(String theCTCContractRolePrivPrefpkId,  DWLControl control) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After finder transaction execution for getCTCContractRolePrivacyPreferenceByIdPK";
            logger.finest("getCTCContractRolePrivacyPreferenceByIdPK(String theCTCContractRolePrivPrefpkId,  DWLControl control) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN getCTCContractRolePrivacyPreferenceByIdPK(String theCTCContractRolePrivPrefpkId,  DWLControl control) " + returnValue);

        }
        return retObj;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction
     * getCTCContractRolePrivacyPreferenceByIdPK.
     * 
     * @param theCTCContractRolePrivPrefpkId
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated NOT
     */
    public DWLResponse  handleGetCTCContractRolePrivacyPreferenceByIdPK(String idPK,  DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

        CTCContractRolePrivPref comp = 
            (CTCContractRolePrivPref)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCCONTRACT_ROLE_PRIV_PREF_COMPONENT);
            
        response = comp.getCTCContractRolePrivacyPreferenceByIdPK(idPK,  control);
        if (response.getData() == null) {
            String[] params = new String[] { idPK };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
                    CTCAdditionsComponentID.CTCCONTRACT_ROLE_PRIV_PREF_CONTROLLER,
                    TCRMErrorCode.READ_RECORD_ERROR,
                    CTCAdditionsErrorReasonCode.GETCTCCONTRACTROLEPRIVACYPREFERENCEBYIDPK_FAILED,
                    control, params, errHandler);                   
        }
        return response;
    }
    

}

