	
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[6de61c39aa499ef1d01b269bde347e5f]
 */

package com.ctc.mdm.addition.controller;

import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionPersistent;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMControlKeys;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLBaseException;
import com.ibm.mdm.annotations.Controller;
import com.ibm.mdm.annotations.TxMetadata;


import com.ctc.mdm.addition.component.CTCPartySegmentBObj;

import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;

import com.ctc.mdm.addition.interfaces.CTCPartySegment;
import com.ctc.mdm.addition.interfaces.CTCPartySegmentTxn;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Controller implementation for all persistent CTCPartySegment services.
 * @generated
 */
@Controller(errorComponentID = CTCAdditionsComponentID.CTCPARTY_SEGMENT_CONTROLLER)
public class CTCPartySegmentTxnBean  extends TCRMCommonComponent implements CTCPartySegmentTxn {

	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCPartySegmentTxnBean.class);
    

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction handleAddCTCPartySegment.
     *
     * @return DWLResponse containing the added 
     * @throws DWLBaseException
     * @see #handleaddCTCPartySegment
     *
     * @ejb.interface-method
     *    view-type="both"
     *
     * @generated
     */
     @TxMetadata(actionCategory=TCRMControlKeys.ADD_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.INSERT_RECORD_ERROR, 
       txErrorReasonCode = CTCAdditionsErrorReasonCode.ADDCTCPARTYSEGMENT_FAILED)
    public DWLResponse addCTCPartySegment(CTCPartySegmentBObj theBObj) throws DWLBaseException {
		logger.finest("ENTER addCTCPartySegment(CTCPartySegmentBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("addCTCPartySegment", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before transaction addCTCPartySegment execution";
			logger.finest("addCTCPartySegment(CTCPartySegmentBObj theBObj) " + infoForLogging);
		}
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After transaction addCTCPartySegment execution";
			logger.finest("addCTCPartySegment(CTCPartySegmentBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN addCTCPartySegment(CTCPartySegmentBObj theBObj) " + returnValue);
		}
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction addCTCPartySegment.
     * 
     * @return DWLResponse containing the added 
     * @throws Exception
     * @see #addCTCPartySegment
     *
     * @generated
     */
    public DWLResponse handleAddCTCPartySegment(CTCPartySegmentBObj theBObj) throws Exception {
		logger.finest("ENTER handleAddCTCPartySegment(CTCPartySegmentBObj theBObj)");
        DWLResponse response = new DWLResponse();
        CTCPartySegment aCTCPartySegmentComponent = (CTCPartySegment) TCRMClassFactory
			.getTCRMComponent(CTCAdditionsPropertyKeys.CTCPARTY_SEGMENT_COMPONENT);
        response = aCTCPartySegmentComponent.addCTCPartySegment(theBObj);
		logger.finest("RETURN handleAddCTCPartySegment(CTCPartySegmentBObj theBObj)");
        return response;
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction handleUpdateCTCPartySegment.
     *
     * @return DWLResponse containing the added 
     * @throws DWLBaseException
     * @see #handleupdateCTCPartySegment
     *
     * @ejb.interface-method
     *    view-type="both"
     *
     * @generated
     */
     @TxMetadata(actionCategory=TCRMControlKeys.UPDATE_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.UPDATE_RECORD_ERROR, 
       txErrorReasonCode = CTCAdditionsErrorReasonCode.UPDATECTCPARTYSEGMENT_FAILED)
    public DWLResponse updateCTCPartySegment(CTCPartySegmentBObj theBObj) throws DWLBaseException {
		logger.finest("ENTER updateCTCPartySegment(CTCPartySegmentBObj theBObj)");
        DWLTransaction txObj = new DWLTransactionPersistent("updateCTCPartySegment", theBObj, theBObj.getControl());
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
        	String infoForLogging="Before transaction updateCTCPartySegment execution";
			logger.finest("updateCTCPartySegment(CTCPartySegmentBObj theBObj) " + infoForLogging);
		}
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
        	String infoForLogging="After transaction updateCTCPartySegment execution";
			logger.finest("updateCTCPartySegment(CTCPartySegmentBObj theBObj) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN updateCTCPartySegment(CTCPartySegmentBObj theBObj) " + returnValue);
		}
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction updateCTCPartySegment.
     * 
     * @return DWLResponse containing the added 
     * @throws Exception
     * @see #updateCTCPartySegment
     *
     * @generated
     */
    public DWLResponse handleUpdateCTCPartySegment(CTCPartySegmentBObj theBObj) throws Exception {
		logger.finest("ENTER handleUpdateCTCPartySegment(CTCPartySegmentBObj theBObj)");
        DWLResponse response = new DWLResponse();
        CTCPartySegment aCTCPartySegmentComponent = (CTCPartySegment) TCRMClassFactory
			.getTCRMComponent(CTCAdditionsPropertyKeys.CTCPARTY_SEGMENT_COMPONENT);
        response = aCTCPartySegmentComponent.updateCTCPartySegment(theBObj);
		logger.finest("RETURN handleUpdateCTCPartySegment(CTCPartySegmentBObj theBObj)");
        return response;
    }
}



