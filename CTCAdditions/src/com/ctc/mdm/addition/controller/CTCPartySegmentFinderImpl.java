/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[81611bc14bfafbf3efb755731df5699c]
 */

package com.ctc.mdm.addition.controller;


import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.annotations.Controller;
import com.ibm.mdm.annotations.TxMetadata;


import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;

import com.ctc.mdm.addition.interfaces.CTCPartySegment;
import com.ctc.mdm.addition.interfaces.CTCPartySegmentFinder;

import com.dwl.base.error.DWLStatus;

import com.dwl.base.util.DWLExceptionUtils;

import java.util.Vector;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Controller class to handle inquiry requests.
 * @generated
 */
 @Controller(errorComponentID = CTCAdditionsComponentID.CTCPARTY_SEGMENT_CONTROLLER)
public class CTCPartySegmentFinderImpl extends TCRMCommonComponent implements CTCPartySegmentFinder {

    private IDWLErrorMessage errHandler;
	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCPartySegmentFinderImpl.class);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     * @generated
     */
    public CTCPartySegmentFinderImpl() {
        super();
        errHandler = TCRMClassFactory.getErrorHandler();
    }

	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCPartySegment.
     *
     * @param PartySegmentId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetCTCPartySegment
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETCTCPARTYSEGMENT_FAILED)
     public DWLResponse getCTCPartySegment(String PartySegmentId,  DWLControl control) throws DWLBaseException {
		logger.finest("ENTER getCTCPartySegment(String PartySegmentId,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(PartySegmentId);
        DWLTransaction txObj = new DWLTransactionInquiry("getCTCPartySegment", params, control);
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
			String infoForLogging="Before finder transaction execution for getCTCPartySegment";
			logger.finest("getCTCPartySegment(String PartySegmentId,  DWLControl control) " + infoForLogging);
		}
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
       		String infoForLogging="After finder transaction execution for getCTCPartySegment";
			logger.finest("getCTCPartySegment(String PartySegmentId,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN getCTCPartySegment(String PartySegmentId,  DWLControl control) " + returnValue);
		}
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction getCTCPartySegment.
     * 
     * @param PartySegmentId
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated
     */
    public DWLResponse  handleGetCTCPartySegment(String PartySegmentId,  DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

		CTCPartySegment comp = 
			(CTCPartySegment)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCPARTY_SEGMENT_COMPONENT);
			
        response = comp.getCTCPartySegment(PartySegmentId,  control);
        if (response.getData() == null) {
            String[] params = new String[] { PartySegmentId };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
					CTCAdditionsComponentID.CTCPARTY_SEGMENT_CONTROLLER,
					TCRMErrorCode.READ_RECORD_ERROR,
					CTCAdditionsErrorReasonCode.GETCTCPARTYSEGMENT_FAILED,
					control, params, errHandler);					
        }
        return response;
    }
    
	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllCTCPartySegment.
     *
     * @param ContId
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetAllCTCPartySegment
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETALLCTCPARTYSEGMENT_FAILED)
     public DWLResponse getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) throws DWLBaseException {
		logger.finest("ENTER getAllCTCPartySegment(String ContId,  String filter,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(ContId);
        params.add(filter);
        DWLTransaction txObj = new DWLTransactionInquiry("getAllCTCPartySegment", params, control);
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
			String infoForLogging="Before finder transaction execution for getAllCTCPartySegment";
			logger.finest("getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) " + infoForLogging);
		}
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
       		String infoForLogging="After finder transaction execution for getAllCTCPartySegment";
			logger.finest("getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) " + returnValue);
		}
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction getAllCTCPartySegment.
     * 
     * @param ContId
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated
     */
    public DWLResponse  handleGetAllCTCPartySegment(String ContId,  String filter,  DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

		CTCPartySegment comp = 
			(CTCPartySegment)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCPARTY_SEGMENT_COMPONENT);
			
        response = comp.getAllCTCPartySegment(ContId,  filter,  control);
        if (response.getData() == null) {
            String[] params = new String[] { ContId,  filter };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
					CTCAdditionsComponentID.CTCPARTY_SEGMENT_CONTROLLER,
					TCRMErrorCode.READ_RECORD_ERROR,
					CTCAdditionsErrorReasonCode.GETALLCTCPARTYSEGMENT_FAILED,
					control, params, errHandler);					
        }
        return response;
    }
    

}


