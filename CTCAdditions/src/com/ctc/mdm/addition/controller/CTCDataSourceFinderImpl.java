/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[08bc1f488ea7e8af961b36f88917943b]
 */

package com.ctc.mdm.addition.controller;


import com.dwl.base.DWLControl;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.requestHandler.DWLTransaction;
import com.dwl.base.requestHandler.DWLTransactionInquiry;
import com.dwl.tcrm.common.TCRMCommonComponent;
import com.dwl.tcrm.common.TCRMErrorCode;
import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.annotations.Controller;
import com.ibm.mdm.annotations.TxMetadata;


import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;

import com.ctc.mdm.addition.interfaces.CTCDataSource;
import com.ctc.mdm.addition.interfaces.CTCDataSourceFinder;

import com.dwl.base.error.DWLStatus;

import com.dwl.base.util.DWLExceptionUtils;

import java.util.Vector;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * Controller class to handle inquiry requests.
 * @generated
 */
 @Controller(errorComponentID = CTCAdditionsComponentID.CTCDATA_SOURCE_CONTROLLER)
public class CTCDataSourceFinderImpl extends TCRMCommonComponent implements CTCDataSourceFinder {

    private IDWLErrorMessage errHandler;
	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCDataSourceFinderImpl.class);

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     * @generated
     */
    public CTCDataSourceFinderImpl() {
        super();
        errHandler = TCRMClassFactory.getErrorHandler();
    }

	
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllCTCDataSourcesByEntity.
     *
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetAllCTCDataSourcesByEntity
     * @generated NOT
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETALLCTCDATASOURCESBYENTITY_FAILED)
     public DWLResponse getAllCTCDataSourcesByEntity(String entityName,String instancePK,String filter, DWLControl control) throws DWLBaseException {
        logger.finest("ENTER getAllCTCDataSourcesByEntity( DWLControl control)");

        Vector params = new Vector();
        params.add(entityName);
        params.add(instancePK);
        params.add(filter);
        DWLTransaction txObj = new DWLTransactionInquiry("getAllCTCDataSourcesByEntity", params, control);
        DWLResponse retObj = null;
        if (logger.isFinestEnabled()) {
            String infoForLogging="Before finder transaction execution for getAllCTCDataSourcesByEntity";
            logger.finest("getAllCTCDataSourcesByEntity( DWLControl control) " + infoForLogging);

        }
        retObj = executeTx(txObj);
        if (logger.isFinestEnabled()) {
            String infoForLogging="After finder transaction execution for getAllCTCDataSourcesByEntity";
            logger.finest("getAllCTCDataSourcesByEntity( DWLControl control) " + infoForLogging);

            String returnValue=retObj.toString();
            logger.finest("RETURN getAllCTCDataSourcesByEntity( DWLControl control) " + returnValue);

        }
        return retObj;
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction getAllCTCDataSourcesByEntity.
     * 
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated NOT
     */
    public DWLResponse  handleGetAllCTCDataSourcesByEntity(String entityName,String instancePK, String filter, DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

        CTCDataSource comp = 
            (CTCDataSource)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
            
        response = comp.getAllCTCDataSourcesByEntity(entityName,instancePK,filter, control);
        if (response.getData() == null) {
            String[] params = new String[] {  };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
                    CTCAdditionsComponentID.CTCDATA_SOURCE_CONTROLLER,
                    TCRMErrorCode.READ_RECORD_ERROR,
                    CTCAdditionsErrorReasonCode.GETALLCTCDATASOURCESBYENTITY_FAILED,
                    control, params, errHandler);                   
        }
        return response;
    }
    
	
	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCDataSource.
     *
     * @param DataSourceID
     * @return com.dwl.base.DWLResponse
     * @exception DWLBaseException
     * @see #handleGetCTCDataSource
     * @generated
     */
     @TxMetadata(actionCategory=DWLControlKeys.VIEW_ACTION_CATEGORY,
       txErrorCode=TCRMErrorCode.READ_RECORD_ERROR,
       txErrorReasonCode = CTCAdditionsErrorReasonCode.GETCTCDATASOURCE_FAILED)
     public DWLResponse getCTCDataSource(String DataSourceID,  DWLControl control) throws DWLBaseException {
		logger.finest("ENTER getCTCDataSource(String DataSourceID,  DWLControl control)");
        Vector<String> params = new Vector<String>();
        params.add(DataSourceID);
        DWLTransaction txObj = new DWLTransactionInquiry("getCTCDataSource", params, control);
        DWLResponse retObj = null;
		if (logger.isFinestEnabled()) {
			String infoForLogging="Before finder transaction execution for getCTCDataSource";
			logger.finest("getCTCDataSource(String DataSourceID,  DWLControl control) " + infoForLogging);
		}
        retObj = executeTx(txObj);
		if (logger.isFinestEnabled()) {
       		String infoForLogging="After finder transaction execution for getCTCDataSource";
			logger.finest("getCTCDataSource(String DataSourceID,  DWLControl control) " + infoForLogging);
        	String returnValue=retObj.toString();
			logger.finest("RETURN getCTCDataSource(String DataSourceID,  DWLControl control) " + returnValue);
		}
        return retObj;
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Performs the business logic for transaction getCTCDataSource.
     * 
     * @param DataSourceID
     * @return com.dwl.base.DWLResponse
     * @exception Exception
     * @generated
     */
    public DWLResponse  handleGetCTCDataSource(String DataSourceID,  DWLControl control) throws Exception {

        DWLResponse response = new DWLResponse();

		CTCDataSource comp = 
			(CTCDataSource)TCRMClassFactory.getTCRMComponent(CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
			
        response = comp.getCTCDataSource(DataSourceID,  control);
        if (response.getData() == null) {
            String[] params = new String[] { DataSourceID };
            DWLExceptionUtils.addErrorToStatus(response.getStatus(), DWLStatus.FATAL,
					CTCAdditionsComponentID.CTCDATA_SOURCE_CONTROLLER,
					TCRMErrorCode.READ_RECORD_ERROR,
					CTCAdditionsErrorReasonCode.GETCTCDATASOURCE_FAILED,
					control, params, errHandler);					
        }
        return response;
    }
    

}


