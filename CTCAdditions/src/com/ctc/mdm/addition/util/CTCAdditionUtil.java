package com.ctc.mdm.addition.util;

import java.util.Collection;

import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.error.DWLStatus;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.utilities.TCRMClassFactory;

public class CTCAdditionUtil {

	/**
	 * Method to find party is exist and Active.
	 *  
	 * @param partyID String cont_id 
	 * @param dwlControl - control object
	 * @return - returns true If the party exist and active. else returns false.
	 * @throws Exception
	 */
	public static boolean isPartyExist(String partyID,DWLControl dwlControl) throws Exception {
		if(partyID!= null){
			IParty  partyComp = (IParty) TCRMClassFactory.getTCRMComponent(TCRMCorePropertyKeys.PARTY_COMPONENT);
			TCRMPartyBObj partyBobj = partyComp.getPartyBasic(partyID, dwlControl);
			if(partyBobj != null){
				return true;	
			}
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean isNotEmpty (Collection obj) {
		return obj != null && (!obj.isEmpty()) ;
	}
	
	/**
	 * returns true If the response is success else false
	 * 
	 * @param response DWLResponse object
	 * @return
	 */
	public static boolean isSuccessResponse(DWLResponse response) {
		if(response !=null && response.getStatus() != null && 
				response.getStatus().getStatus() == DWLStatus.SUCCESS) {
			return true;
		}
		return false;
	}
}
