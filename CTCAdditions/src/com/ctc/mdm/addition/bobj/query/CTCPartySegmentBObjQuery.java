
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[702f25fee60a1f3187b9e84ecf2f490b]
 */
package com.ctc.mdm.addition.bobj.query;




import com.dwl.base.DWLControl;
import com.dwl.bobj.query.BObjQueryException;
import com.dwl.base.DWLCommon;

import com.dwl.base.db.DataAccessFactory;


import com.ctc.mdm.addition.component.CTCPartySegmentBObj;
import com.ctc.mdm.addition.component.CTCPartySegmentResultSetProcessor;

import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;

import com.ctc.mdm.addition.entityObject.CTCPartySegmentInquiryData;
import com.ctc.mdm.addition.entityObject.EObjCTCPartySegmentData;

import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;

import com.dwl.base.exception.DWLDuplicateKeyException;

import com.dwl.base.interfaces.IGenericResultSetProcessor;

import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLExceptionUtils;


/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This class provides query information for the business object
 * <code>CTCPartySegmentBObj</code>.
 *
 * @generated
 */
public class CTCPartySegmentBObjQuery  extends com.dwl.bobj.query.GenericBObjQuery {

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCPARTY_SEGMENT_QUERY = "getCTCPartySegment(Object[])";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCPARTY_SEGMENT_HISTORY_QUERY = "getCTCPartySegmentHistory(Object[])";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String ALL_CTCPARTY_SEGMENT_QUERY = "getAllCTCPartySegment(Object[])";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated NOT
      */
     public static final String ACTIVE_CTCPARTY_SEGMENT_QUERY = "getAllActiveCTCPartySegment(Object[])";
     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated NOT
      */
     public static final String INACTIVE_CTCPARTY_SEGMENT_QUERY = "getAllInactiveCTCPartySegment(Object[])";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String ALL_CTCPARTY_SEGMENT_HISTORY_QUERY = "getAllCTCPartySegmentHistory(Object[])";

	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCPartySegmentBObjQuery.class);
     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCPARTY_SEGMENT_ADD = "CTCPARTY_SEGMENT_ADD";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCPARTY_SEGMENT_DELETE = "CTCPARTY_SEGMENT_DELETE";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCPARTY_SEGMENT_UPDATE = "CTCPARTY_SEGMENT_UPDATE";


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @param queryName
     * The name of the query.
     * @param control
     * The control object.
     *
     * @generated
     */
    public CTCPartySegmentBObjQuery(String queryName, DWLControl control) {
        super(queryName, control);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @param persistenceStrategyName
     * The persistence strategy name.  This parameter indicates the type of
     * database action to be taken such as addition, update or deletion of
     * records.
     * @param objectToPersist
     * The business object to be persisted.
     *
     * @generated
     */
    public CTCPartySegmentBObjQuery(String persistenceStrategyName, DWLCommon objectToPersist) {
        super(persistenceStrategyName, objectToPersist);
    }

	 
 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
	protected void persist() throws Exception{
		logger.finest("ENTER persist()");
		if (logger.isFinestEnabled()) {
	 		String infoForLogging="Persistence strategy is " + persistenceStrategyName;
			logger.finest("persist() " + infoForLogging);
        }
		if (persistenceStrategyName.equals(CTCPARTY_SEGMENT_ADD)) {
			addCTCPartySegment();
		}else if(persistenceStrategyName.equals(CTCPARTY_SEGMENT_UPDATE)) {
			updateCTCPartySegment();
		}else if(persistenceStrategyName.equals(CTCPARTY_SEGMENT_DELETE)) {
			deleteCTCPartySegment();
		}
		logger.finest("RETURN persist()");
	}
  
 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * Inserts ctcpartysegment data by calling
      * <code>EObjCTCPartySegmentData.createEObjCTCPartySegment</code>
     *
     * @throws Exception
     *
     * @generated
     */
	protected void addCTCPartySegment() throws Exception{
		logger.finest("ENTER addCTCPartySegment()");
		
		EObjCTCPartySegmentData theEObjCTCPartySegmentData = (EObjCTCPartySegmentData) DataAccessFactory
			.getQuery(EObjCTCPartySegmentData.class, connection);
		theEObjCTCPartySegmentData.createEObjCTCPartySegment(((CTCPartySegmentBObj) objectToPersist).getEObjCTCPartySegment());
		logger.finest("RETURN addCTCPartySegment()");
	}

 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * Updates ctcpartysegment data by calling
      * <code>EObjCTCPartySegmentData.updateEObjCTCPartySegment</code>
     *
     * @throws Exception
     *
     * @generated
     */
	protected void updateCTCPartySegment() throws Exception{
		logger.finest("ENTER updateCTCPartySegment()");
		EObjCTCPartySegmentData theEObjCTCPartySegmentData = (EObjCTCPartySegmentData) DataAccessFactory
			.getQuery(EObjCTCPartySegmentData.class, connection);
		theEObjCTCPartySegmentData.updateEObjCTCPartySegment(((CTCPartySegmentBObj) objectToPersist).getEObjCTCPartySegment());
		logger.finest("RETURN updateCTCPartySegment()");
	}

 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
      * Deletes {0} data by calling <{1}>{2}.{3}{4}</{1}>
	 *
     * @throws Exception
     *
     * @generated
     */
	protected void deleteCTCPartySegment() throws Exception{
		logger.finest("ENTER deleteCTCPartySegment()");
		     // MDM_TODO: CDKWB0018I Write customized business logic for the extension here.
		logger.finest("RETURN deleteCTCPartySegment()");
	} 
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * This method is overridden to construct
      * <code>DWLDuplicateKeyException</code> based on CTCPartySegment component
      * specific values.
     * 
     * @param errParams
     * The values to be substituted in the error message.
	 *
     * @throws Exception
     *
     * @generated
     */
    protected void throwDuplicateKeyException(String[] errParams) throws Exception {
		if (logger.isFinestEnabled()) {
	    	StringBuilder errParamsStringBuilder = new StringBuilder("Error: Duplicate key Exception parameters are ");
	    	for(int i=0;i<errParams.length;i++) {
	    		errParamsStringBuilder .append(errParams[i]);
	    		if (i!=errParams.length-1) {
	    			errParamsStringBuilder .append(" , ");
	    		}
	    	}
	        String infoForLogging="Error: Duplicate key Exception parameters are " + errParamsStringBuilder;
			logger.finest("Unknown method " + infoForLogging);
		}
    	DWLExceptionUtils.throwDWLDuplicateKeyException(
    		new DWLDuplicateKeyException(buildDupThrowableMessage(errParams)),
    		objectToPersist.getStatus(), 
    		DWLStatus.FATAL,
    		CTCAdditionsComponentID.CTCPARTY_SEGMENT_COMPONENT,
    		DWLErrorCode.DUPLICATE_KEY_ERROR, 
    		CTCAdditionsErrorReasonCode.DUPLICATE_PRIMARY_KEY_CTCPARTYSEGMENT,
    		objectToPersist.getControl(), 
    		DWLClassFactory.getErrorHandler()
    		);
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Provides the result set processor that is used to populate the business
     * object.
     *
     * @return
     * An instance of <code>CTCPartySegmentResultSetProcessor</code>.
     *
     * @see com.dwl.bobj.query.AbstractBObjQuery#provideResultSetProcessor()
     * @see com.ctc.mdm.addition.component.CTCPartySegmentResultSetProcessor
     *
     * @generated
     */
    protected IGenericResultSetProcessor provideResultSetProcessor()
            throws BObjQueryException {

        return new CTCPartySegmentResultSetProcessor();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    @Override
    protected Class<CTCPartySegmentInquiryData> provideQueryInterfaceClass() throws BObjQueryException {
        return CTCPartySegmentInquiryData.class;
    }

}


