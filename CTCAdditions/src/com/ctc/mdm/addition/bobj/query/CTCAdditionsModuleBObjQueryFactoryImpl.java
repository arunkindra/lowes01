/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[0e68334c97832f0695c088fcd8e58c95]
 */

package com.ctc.mdm.addition.bobj.query;


import com.ctc.mdm.addition.bobj.query.CTCDataSourceBObjQuery;
import com.ctc.mdm.addition.bobj.query.CTCPartySegmentBObjQuery;

import com.ctc.mdm.addition.constant.ResourceBundleNames;

import com.dwl.base.DWLCommon;
import com.dwl.base.DWLControl;

import com.dwl.bobj.query.BObjQuery;
import com.dwl.bobj.query.Persistence;

import com.dwl.common.globalization.util.ResourceBundleHelper;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This factory class provides methods to return the BObjQuery instances
 * relating to the relevant business objects.
 *
 * @generated
 */
public class CTCAdditionsModuleBObjQueryFactoryImpl  implements CTCAdditionsModuleBObjQueryFactory, CTCAdditionsModuleBObjPersistenceFactory {

	private final static String EXCEPTION_QUERYNAME_EMPTY = "Exception_AbstractBObjQuery_QueryNameCannotBeEmpty";
	/**
	  * <!-- begin-user-doc -->
	  * <!-- end-user-doc -->
	  * @generated 
	  */
	 private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCAdditionsModuleBObjQueryFactoryImpl.class);

    /**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 *
     * Default constructor.
     *
     * @generated
     */
    public CTCAdditionsModuleBObjQueryFactoryImpl() {
        super();
    }
    
     /** 
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * Provides the concrete BObjQuery instance corresponding to the
      * <code>CTCDataSourceBObj</code> business object.
      *
      * @return 
      * An instance of <code>CTCDataSourceBObjQuery</code>.
      *
      * @generated
      */
      public BObjQuery createCTCDataSourceBObjQuery(String queryName, DWLControl dwlControl) {
		logger.finest("ENTER createCTCDataSourceBObjQuery(String queryName, DWLControl dwlControl)");
        if ((queryName == null) || queryName.trim().equals("")) {
			throw new IllegalArgumentException(ResourceBundleHelper.resolve(
					ResourceBundleNames.COMMON_SERVICES_STRINGS,
					EXCEPTION_QUERYNAME_EMPTY));
        }
		logger.finest("RETURN createCTCDataSourceBObjQuery(String queryName, DWLControl dwlControl)");
        return new CTCDataSourceBObjQuery(queryName, dwlControl);
    }
    
     /** 
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * This method returns an object of type <code>Persistence</code>
      * corresponding to <code>CTCDataSourceBObj</code> business object.
      *
      * @param persistenceStrategyName
      * The persistence strategy name.  This parameter indicates the type of
      * database action to be taken such as addition, update or deletion of
      * records.
      * @param objectToPersist
      * The business object to be persisted.
      *      
      * @return 
      * An instance of <code>CTCDataSourceBObjQuery</code>.
      *
      * @generated
      */
      public Persistence createCTCDataSourceBObjPersistence(String persistenceStrategyName, DWLCommon objectToPersist) {

        return new CTCDataSourceBObjQuery(persistenceStrategyName, objectToPersist);
      }
      
     /** 
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * Provides the concrete BObjQuery instance corresponding to the
      * <code>CTCPartySegmentBObj</code> business object.
      *
      * @return 
      * An instance of <code>CTCPartySegmentBObjQuery</code>.
      *
      * @generated
      */
      public BObjQuery createCTCPartySegmentBObjQuery(String queryName, DWLControl dwlControl) {
		logger.finest("ENTER createCTCPartySegmentBObjQuery(String queryName, DWLControl dwlControl)");
        if ((queryName == null) || queryName.trim().equals("")) {
			throw new IllegalArgumentException(ResourceBundleHelper.resolve(
					ResourceBundleNames.COMMON_SERVICES_STRINGS,
					EXCEPTION_QUERYNAME_EMPTY));
        }
		logger.finest("RETURN createCTCPartySegmentBObjQuery(String queryName, DWLControl dwlControl)");
        return new CTCPartySegmentBObjQuery(queryName, dwlControl);
    }
    
     /** 
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * This method returns an object of type <code>Persistence</code>
      * corresponding to <code>CTCPartySegmentBObj</code> business object.
      *
      * @param persistenceStrategyName
      * The persistence strategy name.  This parameter indicates the type of
      * database action to be taken such as addition, update or deletion of
      * records.
      * @param objectToPersist
      * The business object to be persisted.
      *      
      * @return 
      * An instance of <code>CTCPartySegmentBObjQuery</code>.
      *
      * @generated
      */
      public Persistence createCTCPartySegmentBObjPersistence(String persistenceStrategyName, DWLCommon objectToPersist) {

        return new CTCPartySegmentBObjQuery(persistenceStrategyName, objectToPersist);
      }
      

}


