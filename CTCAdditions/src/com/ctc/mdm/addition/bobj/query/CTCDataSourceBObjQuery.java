
/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[0ce1e768bfaee0beedb2f3849a7abfb9]
 */
package com.ctc.mdm.addition.bobj.query;




import com.dwl.base.DWLControl;
import com.dwl.bobj.query.BObjQueryException;
import com.dwl.base.DWLCommon;

import com.dwl.base.db.DataAccessFactory;


import com.ctc.mdm.addition.component.CTCDataSourceBObj;
import com.ctc.mdm.addition.component.CTCDataSourceResultSetProcessor;

import com.ctc.mdm.addition.constant.CTCAdditionsComponentID;
import com.ctc.mdm.addition.constant.CTCAdditionsErrorReasonCode;

import com.ctc.mdm.addition.entityObject.CTCDataSourceInquiryData;
import com.ctc.mdm.addition.entityObject.EObjCTCDataSourceData;

import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;

import com.dwl.base.exception.DWLDuplicateKeyException;

import com.dwl.base.interfaces.IGenericResultSetProcessor;

import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLExceptionUtils;


/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * This class provides query information for the business object
 * <code>CTCDataSourceBObj</code>.
 *
 * @generated
 */
public class CTCDataSourceBObjQuery  extends com.dwl.bobj.query.GenericBObjQuery {

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCDATA_SOURCE_QUERY = "getCTCDataSource(Object[])";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCDATA_SOURCE_HISTORY_QUERY = "getCTCDataSourceHistory(Object[])";
     
     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated NOT
      */
     public static final String CTCDATA_SOURCE_BY_ENTITY_ALL_QUERY = "getAllCTCDataSourcesByEntity(Object[])";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated NOT
      */
     public static final String CTCDATA_SOURCE_BY_ENTITY_HISTORY_QUERY = "getAllCTCDataSourcesByEntityHistory(Object[])";
     
     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated NOT
      */
     public static final String CTCDATA_SOURCE_BY_ENTITY_ACTIVE_QUERY = "getAllActiveCTCDataSourcesByEntity(Object[])";
     
     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated NOT
      */
     public static final String CTCDATA_SOURCE_BY_ENTITY_INACTIVE_QUERY = "getAllInactiveCTCDataSourcesByEntity(Object[])";

    /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      * @generated 
      */
     private final static com.dwl.base.logging.IDWLLogger logger = com.dwl.base.logging.DWLLoggerManager.getLogger(CTCDataSourceBObjQuery.class);

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCDATA_SOURCE_ADD = "CTCDATA_SOURCE_ADD";

     /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCDATA_SOURCE_DELETE = "CTCDATA_SOURCE_DELETE";
	 
	  /**
      * <!-- begin-user-doc -->
      * <!-- end-user-doc -->
      *
      * @generated
      */
     public static final String CTCDATA_SOURCE_UPDATE = "CTCDATA_SOURCE_UPDATE";


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @param queryName
     * The name of the query.
     * @param control
     * The control object.
     *
     * @generated
     */
    public CTCDataSourceBObjQuery(String queryName, DWLControl control) {
        super(queryName, control);
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.
     *
     * @param persistenceStrategyName
     * The persistence strategy name.  This parameter indicates the type of
     * database action to be taken such as addition, update or deletion of
     * records.
     * @param objectToPersist
     * The business object to be persisted.
     *
     * @generated
     */
    public CTCDataSourceBObjQuery(String persistenceStrategyName, DWLCommon objectToPersist) {
        super(persistenceStrategyName, objectToPersist);
    }

	 
 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
	protected void persist() throws Exception{
		logger.finest("ENTER persist()");
		if (logger.isFinestEnabled()) {
	 		String infoForLogging="Persistence strategy is " + persistenceStrategyName;
			logger.finest("persist() " + infoForLogging);
        }
		if (persistenceStrategyName.equals(CTCDATA_SOURCE_ADD)) {
			addCTCDataSource();
		}else if(persistenceStrategyName.equals(CTCDATA_SOURCE_UPDATE)) {
			updateCTCDataSource();
		}else if(persistenceStrategyName.equals(CTCDATA_SOURCE_DELETE)) {
			deleteCTCDataSource();
		}
		logger.finest("RETURN persist()");
	}
  
 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * Inserts ctcdatasource data by calling
      * <code>EObjCTCDataSourceData.createEObjCTCDataSource</code>
     *
     * @throws Exception
     *
     * @generated
     */
	protected void addCTCDataSource() throws Exception{
		logger.finest("ENTER addCTCDataSource()");
		
		EObjCTCDataSourceData theEObjCTCDataSourceData = (EObjCTCDataSourceData) DataAccessFactory
			.getQuery(EObjCTCDataSourceData.class, connection);
		theEObjCTCDataSourceData.createEObjCTCDataSource(((CTCDataSourceBObj) objectToPersist).getEObjCTCDataSource());
		logger.finest("RETURN addCTCDataSource()");
	}

 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * Updates ctcdatasource data by calling
      * <code>EObjCTCDataSourceData.updateEObjCTCDataSource</code>
     *
     * @throws Exception
     *
     * @generated
     */
	protected void updateCTCDataSource() throws Exception{
		logger.finest("ENTER updateCTCDataSource()");
		EObjCTCDataSourceData theEObjCTCDataSourceData = (EObjCTCDataSourceData) DataAccessFactory
			.getQuery(EObjCTCDataSourceData.class, connection);
		theEObjCTCDataSourceData.updateEObjCTCDataSource(((CTCDataSourceBObj) objectToPersist).getEObjCTCDataSource());
		logger.finest("RETURN updateCTCDataSource()");
	}

 	/**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
      * Deletes {0} data by calling <{1}>{2}.{3}{4}</{1}>
	 *
     * @throws Exception
     *
     * @generated
     */
	protected void deleteCTCDataSource() throws Exception{
		logger.finest("ENTER deleteCTCDataSource()");
		     // MDM_TODO: CDKWB0018I Write customized business logic for the extension here.
		logger.finest("RETURN deleteCTCDataSource()");
	} 
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
      * This method is overridden to construct
      * <code>DWLDuplicateKeyException</code> based on CTCDataSource component
      * specific values.
     * 
     * @param errParams
     * The values to be substituted in the error message.
	 *
     * @throws Exception
     *
     * @generated
     */
    protected void throwDuplicateKeyException(String[] errParams) throws Exception {
		if (logger.isFinestEnabled()) {
	    	StringBuilder errParamsStringBuilder = new StringBuilder("Error: Duplicate key Exception parameters are ");
	    	for(int i=0;i<errParams.length;i++) {
	    		errParamsStringBuilder .append(errParams[i]);
	    		if (i!=errParams.length-1) {
	    			errParamsStringBuilder .append(" , ");
	    		}
	    	}
	        String infoForLogging="Error: Duplicate key Exception parameters are " + errParamsStringBuilder;
			logger.finest("Unknown method " + infoForLogging);
		}
    	DWLExceptionUtils.throwDWLDuplicateKeyException(
    		new DWLDuplicateKeyException(buildDupThrowableMessage(errParams)),
    		objectToPersist.getStatus(), 
    		DWLStatus.FATAL,
    		CTCAdditionsComponentID.CTCDATA_SOURCE_COMPONENT,
    		DWLErrorCode.DUPLICATE_KEY_ERROR, 
    		CTCAdditionsErrorReasonCode.DUPLICATE_PRIMARY_KEY_CTCDATASOURCE,
    		objectToPersist.getControl(), 
    		DWLClassFactory.getErrorHandler()
    		);
    }
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Provides the result set processor that is used to populate the business
     * object.
     *
     * @return
     * An instance of <code>CTCDataSourceResultSetProcessor</code>.
     *
     * @see com.dwl.bobj.query.AbstractBObjQuery#provideResultSetProcessor()
     * @see com.ctc.mdm.addition.component.CTCDataSourceResultSetProcessor
     *
     * @generated
     */
    protected IGenericResultSetProcessor provideResultSetProcessor()
            throws BObjQueryException {

        return new CTCDataSourceResultSetProcessor();
    }

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    @Override
    protected Class<CTCDataSourceInquiryData> provideQueryInterfaceClass() throws BObjQueryException {
        return CTCDataSourceInquiryData.class;
    }

}


