/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[d54d2a3f9737dbd2d40418518fb5f2fc]
 */

package com.ctc.mdm.addition.interfaces;


import com.dwl.base.DWLControl;
import com.dwl.tcrm.common.ITCRMComponent;
import com.dwl.base.DWLResponse;
import com.dwl.base.exception.DWLBaseException;


import com.ctc.mdm.addition.component.CTCPartySegmentBObj;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * A component level interface which defines all the CTCPartySegment services.
 * @generated
 **/
public interface CTCPartySegment extends ITCRMComponent {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getCTCPartySegment.
     *
     * @generated
     **/
    public DWLResponse getCTCPartySegment(String PartySegmentId,  DWLControl control) throws DWLBaseException;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction getAllCTCPartySegment.
     *
     * @generated
     **/
    public DWLResponse getAllCTCPartySegment(String ContId,  String filter,  DWLControl control) throws DWLBaseException;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction addCTCPartySegment.
     *
     * @generated
     **/
    public DWLResponse addCTCPartySegment(CTCPartySegmentBObj theBObj) throws DWLBaseException;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Executes the transaction updateCTCPartySegment.
     *
     * @generated
     **/
    public DWLResponse updateCTCPartySegment(CTCPartySegmentBObj theBObj) throws DWLBaseException;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     **/
    public void loadBeforeImage(CTCPartySegmentBObj bObj) throws DWLBaseException;

} 


