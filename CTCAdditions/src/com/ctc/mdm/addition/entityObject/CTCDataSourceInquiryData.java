/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[479d84b0f07eb204edc657363cd74fbf]
 */

package com.ctc.mdm.addition.entityObject;


import com.ibm.mdm.base.db.ResultQueue1;
import java.util.Iterator;
import com.ibm.mdm.base.db.EntityMapping;
import com.ibm.pdq.annotation.Select;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
public interface CTCDataSourceInquiryData {
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String tableAliasString = "tableAlias (" + 
                                            "CTCDATASOURCE => com.ctc.mdm.addition.entityObject.EObjCTCDataSource, " +
                                            "H_CTCDATASOURCE => com.ctc.mdm.addition.entityObject.EObjCTCDataSource" +
                                            ")";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getCTCDataSourceSql = "SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.data_source_id = ? ";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  static final String getAllActiveCTCDataSourcesByEntitySql = "SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND (r.end_dt is null or r.end_dt > ?) ";
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  static final String getAllInactiveCTCDataSourcesByEntitySql = "SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND r.end_dt <= ?) ";
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  static final String getAllCTCDataSourcesByEntitySql = "SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? ";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  public static final String getCTCDataSourceParameters =
	"EObjCTCDataSource.DataSourceID";
	
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  public static final String getAllCTCDataSourcesByEntityParameters =
	"EObjCTCDataSource.EntityName," +
	"EObjCTCDataSource.InstancePK";
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  public static final String getAllActiveCTCDataSourcesByEntityParameters =
	"EObjCTCDataSource.EntityName," +
	"EObjCTCDataSource.InstancePK," +
	"EObjCTCDataSource.EndDate";

  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  public static final String getAllInactiveCTCDataSourcesByEntityParameters =
	"EObjCTCDataSource.EntityName," +
	"EObjCTCDataSource.InstancePK," +
	"EObjCTCDataSource.EndDate";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getCTCDataSourceResults =
    "EObjCTCDataSource.DataSourceID," +
    "EObjCTCDataSource.EntityName," +
    "EObjCTCDataSource.InstancePK," +
    "EObjCTCDataSource.AttributeName," +
    "EObjCTCDataSource.StartDate," +
    "EObjCTCDataSource.EndDate," +
    "EObjCTCDataSource.AdminSystem," +
    "EObjCTCDataSource.lastUpdateDt," +
    "EObjCTCDataSource.lastUpdateUser," +
    "EObjCTCDataSource.lastUpdateTxId";
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  public static final String getAllCTCDataSourcesByEntityResults =
    "EObjCTCDataSource.DataSourceID," +
    "EObjCTCDataSource.EntityName," +
    "EObjCTCDataSource.InstancePK," +
    "EObjCTCDataSource.AttributeName," +
    "EObjCTCDataSource.StartDate," +
    "EObjCTCDataSource.EndDate," +
    "EObjCTCDataSource.AdminSystem," +
    "EObjCTCDataSource.lastUpdateDt," +
    "EObjCTCDataSource.lastUpdateUser," +
    "EObjCTCDataSource.lastUpdateTxId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getCTCDataSourceHistorySql = "SELECT r.H_data_source_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCDATASOURCE r WHERE r.H_data_source_id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))";
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  static final String getAllCTCDataSourcesByEntityHistorySql = "SELECT r.H_data_source_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))";
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getCTCDataSourceHistoryParameters =
    "EObjCTCDataSource.DataSourceID," +
    "EObjCTCDataSource.lastUpdateDt," +
    "EObjCTCDataSource.lastUpdateDt";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  public static final String getAllCTCDataSourcesByEntityHistoryParameters =
	"EObjCTCDataSource.EntityName," +
	"EObjCTCDataSource.InstancePK," +
    "EObjCTCDataSource.lastUpdateDt," +
    "EObjCTCDataSource.lastUpdateDt";
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getCTCDataSourceHistoryResults =
    "EObjCTCDataSource.historyIdPK," +
    "EObjCTCDataSource.histActionCode," +
    "EObjCTCDataSource.histCreatedBy," +
    "EObjCTCDataSource.histCreateDt," +
    "EObjCTCDataSource.histEndDt," +
    "EObjCTCDataSource.DataSourceID," +
    "EObjCTCDataSource.EntityName," +
    "EObjCTCDataSource.InstancePK," +
    "EObjCTCDataSource.AttributeName," +
    "EObjCTCDataSource.StartDate," +
    "EObjCTCDataSource.EndDate," +
    "EObjCTCDataSource.AdminSystem," +
    "EObjCTCDataSource.lastUpdateDt," +
    "EObjCTCDataSource.lastUpdateUser," +
    "EObjCTCDataSource.lastUpdateTxId";
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  public static final String getAllCTCDataSourcesByEntityHistoryResults =
    "EObjCTCDataSource.historyIdPK," +
    "EObjCTCDataSource.histActionCode," +
    "EObjCTCDataSource.histCreatedBy," +
    "EObjCTCDataSource.histCreateDt," +
    "EObjCTCDataSource.histEndDt," +
    "EObjCTCDataSource.DataSourceID," +
    "EObjCTCDataSource.EntityName," +
    "EObjCTCDataSource.InstancePK," +
    "EObjCTCDataSource.AttributeName," +
    "EObjCTCDataSource.StartDate," +
    "EObjCTCDataSource.EndDate," +
    "EObjCTCDataSource.AdminSystem," +
    "EObjCTCDataSource.lastUpdateDt," +
    "EObjCTCDataSource.lastUpdateUser," +
    "EObjCTCDataSource.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getCTCDataSourceSql, pattern=tableAliasString)
  @EntityMapping(parameters=getCTCDataSourceParameters, results=getCTCDataSourceResults)
  Iterator<ResultQueue1<EObjCTCDataSource>> getCTCDataSource(Object[] parameters);  


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getCTCDataSourceHistorySql, pattern=tableAliasString)
  @EntityMapping(parameters=getCTCDataSourceHistoryParameters, results=getCTCDataSourceHistoryResults)
  Iterator<ResultQueue1<EObjCTCDataSource>> getCTCDataSourceHistory(Object[] parameters);  

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  @Select(sql=getAllActiveCTCDataSourcesByEntitySql, pattern=tableAliasString)
  @EntityMapping(parameters=getAllActiveCTCDataSourcesByEntityParameters, results=getAllCTCDataSourcesByEntityResults)
  Iterator<ResultQueue1<EObjCTCDataSource>> getAllActiveCTCDataSourcesByEntity(Object[] parameters);

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  @Select(sql=getAllInactiveCTCDataSourcesByEntitySql, pattern=tableAliasString)
  @EntityMapping(parameters=getAllInactiveCTCDataSourcesByEntityParameters, results=getAllCTCDataSourcesByEntityResults)
  Iterator<ResultQueue1<EObjCTCDataSource>> getAllInactiveCTCDataSourcesByEntity(Object[] parameters);

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  @Select(sql=getAllCTCDataSourcesByEntitySql, pattern=tableAliasString)
  @EntityMapping(parameters=getAllCTCDataSourcesByEntityParameters, results=getAllCTCDataSourcesByEntityResults)
  Iterator<ResultQueue1<EObjCTCDataSource>> getAllCTCDataSourcesByEntity(Object[] parameters);


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  @Select(sql=getAllCTCDataSourcesByEntityHistorySql, pattern=tableAliasString)
  @EntityMapping(parameters=getAllCTCDataSourcesByEntityHistoryParameters, results=getAllCTCDataSourcesByEntityHistoryResults)
  Iterator<ResultQueue1<EObjCTCDataSource>> getAllCTCDataSourcesByEntityHistory(Object[] parameters);  

}


