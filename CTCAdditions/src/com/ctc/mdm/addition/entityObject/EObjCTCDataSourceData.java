/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[c9e51d8c43aba769481b5d7b8b957461]
 */


package com.ctc.mdm.addition.entityObject;

import java.util.Iterator;
import com.ibm.mdm.base.db.EntityMapping;
import com.ibm.pdq.annotation.Select;
import com.ibm.pdq.annotation.Update;

import com.ctc.mdm.addition.entityObject.EObjCTCDataSource;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
public interface EObjCTCDataSourceData {


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getEObjCTCDataSourceSql = "select data_source_id, entity_name, instance_pk, attribute_name, start_dt, end_dt, admin_sys_tp_cd, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from CTCDATASOURCE where data_source_id = ? ";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String createEObjCTCDataSourceSql = "insert into CTCDATASOURCE (data_source_id, entity_name, instance_pk, attribute_name, start_dt, end_dt, admin_sys_tp_cd, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values( :dataSourceID, :entityName, :instancePK, :attributeName, :startDate, :endDate, :adminSystem, :lastUpdateDt, :lastUpdateUser, :lastUpdateTxId)";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String updateEObjCTCDataSourceSql = "update CTCDATASOURCE set entity_name = :entityName, instance_pk = :instancePK, attribute_name = :attributeName, start_dt = :startDate, end_dt = :endDate, admin_sys_tp_cd = :adminSystem, LAST_UPDATE_DT = :lastUpdateDt, LAST_UPDATE_USER = :lastUpdateUser, LAST_UPDATE_TX_ID = :lastUpdateTxId where data_source_id = :dataSourceID and LAST_UPDATE_DT = :oldLastUpdateDt";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String deleteEObjCTCDataSourceSql = "delete from CTCDATASOURCE where data_source_id = ?";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjCTCDataSourceKeyField = "EObjCTCDataSource.dataSourceID";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjCTCDataSourceGetFields =
    "EObjCTCDataSource.dataSourceID," +
    "EObjCTCDataSource.entityName," +
    "EObjCTCDataSource.instancePK," +
    "EObjCTCDataSource.attributeName," +
    "EObjCTCDataSource.startDate," +
    "EObjCTCDataSource.endDate," +
    "EObjCTCDataSource.adminSystem," +
    "EObjCTCDataSource.lastUpdateDt," +
    "EObjCTCDataSource.lastUpdateUser," +
    "EObjCTCDataSource.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjCTCDataSourceAllFields =
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.dataSourceID," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.entityName," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.instancePK," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.attributeName," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.startDate," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.endDate," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.adminSystem," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.lastUpdateDt," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.lastUpdateUser," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjCTCDataSourceUpdateFields =
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.entityName," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.instancePK," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.attributeName," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.startDate," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.endDate," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.adminSystem," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.lastUpdateDt," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.lastUpdateUser," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.lastUpdateTxId," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.dataSourceID," +
    "com.ctc.mdm.addition.entityObject.EObjCTCDataSource.oldLastUpdateDt";   

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Select CTCDataSource by parameters.
   * @generated
   */
  @Select(sql=getEObjCTCDataSourceSql)
  @EntityMapping(parameters=EObjCTCDataSourceKeyField, results=EObjCTCDataSourceGetFields)
  Iterator<EObjCTCDataSource> getEObjCTCDataSource(Long dataSourceID);  
   
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Create CTCDataSource by EObjCTCDataSource Object.
   * @generated
   */
  @Update(sql=createEObjCTCDataSourceSql)
  @EntityMapping(parameters=EObjCTCDataSourceAllFields)
    int createEObjCTCDataSource(EObjCTCDataSource e); 

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Update one CTCDataSource by EObjCTCDataSource object.
   * @generated
   */
  @Update(sql=updateEObjCTCDataSourceSql)
  @EntityMapping(parameters=EObjCTCDataSourceUpdateFields)
    int updateEObjCTCDataSource(EObjCTCDataSource e); 

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Delete CTCDataSource by parameters.
   * @generated
   */
  @Update(sql=deleteEObjCTCDataSourceSql)
  @EntityMapping(parameters=EObjCTCDataSourceKeyField)
  int deleteEObjCTCDataSource(Long dataSourceID);

}

