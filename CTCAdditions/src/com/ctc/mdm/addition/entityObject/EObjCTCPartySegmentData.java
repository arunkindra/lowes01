/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[4519f872708815798744d713adfd3626]
 */


package com.ctc.mdm.addition.entityObject;

import java.util.Iterator;
import com.ibm.mdm.base.db.EntityMapping;
import com.ibm.pdq.annotation.Select;
import com.ibm.pdq.annotation.Update;

import com.ctc.mdm.addition.entityObject.EObjCTCPartySegment;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
public interface EObjCTCPartySegmentData {


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getEObjCTCPartySegmentSql = "select party_segment_id, cont_id, segment_provider, business_group_name, segment_type_name, segment_value, segment_category_type, effective_start_dt, effective_end_dt, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from CTCPARTYSEGMENT where party_segment_id = ? ";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String createEObjCTCPartySegmentSql = "insert into CTCPARTYSEGMENT (party_segment_id, cont_id, segment_provider, business_group_name, segment_type_name, segment_value, segment_category_type, effective_start_dt, effective_end_dt, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values( :partySegmentId, :contId, :segmentProvider, :businessGroupName, :segmentTypeName, :segmentValue, :segmentCategoryType, :effectiveStartDt, :effectiveEndDt, :lastUpdateDt, :lastUpdateUser, :lastUpdateTxId)";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String updateEObjCTCPartySegmentSql = "update CTCPARTYSEGMENT set cont_id = :contId, segment_provider = :segmentProvider, business_group_name = :businessGroupName, segment_type_name = :segmentTypeName, segment_value = :segmentValue, segment_category_type = :segmentCategoryType, effective_start_dt = :effectiveStartDt, effective_end_dt = :effectiveEndDt, LAST_UPDATE_DT = :lastUpdateDt, LAST_UPDATE_USER = :lastUpdateUser, LAST_UPDATE_TX_ID = :lastUpdateTxId where party_segment_id = :partySegmentId and LAST_UPDATE_DT = :oldLastUpdateDt";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String deleteEObjCTCPartySegmentSql = "delete from CTCPARTYSEGMENT where party_segment_id = ?";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjCTCPartySegmentKeyField = "EObjCTCPartySegment.partySegmentId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjCTCPartySegmentGetFields =
    "EObjCTCPartySegment.partySegmentId," +
    "EObjCTCPartySegment.contId," +
    "EObjCTCPartySegment.segmentProvider," +
    "EObjCTCPartySegment.businessGroupName," +
    "EObjCTCPartySegment.segmentTypeName," +
    "EObjCTCPartySegment.segmentValue," +
    "EObjCTCPartySegment.segmentCategoryType," +
    "EObjCTCPartySegment.effectiveStartDt," +
    "EObjCTCPartySegment.effectiveEndDt," +
    "EObjCTCPartySegment.lastUpdateDt," +
    "EObjCTCPartySegment.lastUpdateUser," +
    "EObjCTCPartySegment.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjCTCPartySegmentAllFields =
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.partySegmentId," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.contId," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.segmentProvider," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.businessGroupName," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.segmentTypeName," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.segmentValue," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.segmentCategoryType," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.effectiveStartDt," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.effectiveEndDt," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.lastUpdateDt," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.lastUpdateUser," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String EObjCTCPartySegmentUpdateFields =
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.contId," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.segmentProvider," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.businessGroupName," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.segmentTypeName," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.segmentValue," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.segmentCategoryType," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.effectiveStartDt," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.effectiveEndDt," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.lastUpdateDt," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.lastUpdateUser," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.lastUpdateTxId," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.partySegmentId," +
    "com.ctc.mdm.addition.entityObject.EObjCTCPartySegment.oldLastUpdateDt";   

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Select CTCPartySegment by parameters.
   * @generated
   */
  @Select(sql=getEObjCTCPartySegmentSql)
  @EntityMapping(parameters=EObjCTCPartySegmentKeyField, results=EObjCTCPartySegmentGetFields)
  Iterator<EObjCTCPartySegment> getEObjCTCPartySegment(Long partySegmentId);  
   
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Create CTCPartySegment by EObjCTCPartySegment Object.
   * @generated
   */
  @Update(sql=createEObjCTCPartySegmentSql)
  @EntityMapping(parameters=EObjCTCPartySegmentAllFields)
    int createEObjCTCPartySegment(EObjCTCPartySegment e); 

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Update one CTCPartySegment by EObjCTCPartySegment object.
   * @generated
   */
  @Update(sql=updateEObjCTCPartySegmentSql)
  @EntityMapping(parameters=EObjCTCPartySegmentUpdateFields)
    int updateEObjCTCPartySegment(EObjCTCPartySegment e); 

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
    * Delete CTCPartySegment by parameters.
   * @generated
   */
  @Update(sql=deleteEObjCTCPartySegmentSql)
  @EntityMapping(parameters=EObjCTCPartySegmentKeyField)
  int deleteEObjCTCPartySegment(Long partySegmentId);

}

