/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[a998b1bc7f8a33a94149662417e4c36a]
 */

package com.ctc.mdm.addition.entityObject;

import com.dwl.base.EObjCommon;
import com.ibm.mdm.base.db.DataType;
import com.ibm.pdq.annotation.Column;
import com.ibm.pdq.annotation.Table;


import com.ibm.pdq.annotation.Id;

import java.sql.Timestamp;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * The entity object corresponding to the CTCDataSource business object. This
 * entity object should include all the attributes as defined by the business
 * object.
 * 
 * @generated
 */
@SuppressWarnings("serial")
@Table(name=EObjCTCDataSource.tableName)
public class EObjCTCDataSource extends EObjCommon {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public static final String tableName = "CTCDATASOURCE";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String dataSourceIDColumn = "DATA_SOURCE_ID";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String dataSourceIDJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    dataSourceIDPrecision = 19;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String entityNameColumn = "ENTITY_NAME";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String entityNameJdbcType = "VARCHAR";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    entityNamePrecision = 255;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String instancePKColumn = "INSTANCE_PK";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String instancePKJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    instancePKPrecision = 19;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String attributeNameColumn = "ATTRIBUTE_NAME";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String attributeNameJdbcType = "VARCHAR";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    attributeNamePrecision = 250;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String startDateColumn = "START_DT";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String startDateJdbcType = "TIMESTAMP";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String endDateColumn = "END_DT";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String endDateJdbcType = "TIMESTAMP";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String adminSystemColumn = "ADMIN_SYS_TP_CD";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String adminSystemJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    adminSystemPrecision = 19;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long dataSourceID;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected String entityName;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long instancePK;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected String attributeName;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected  Timestamp startDate;
    //inside if 

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected  Timestamp endDate;
    //inside if 

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long adminSystem;



    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.     
     *
     * @generated
     */
    public EObjCTCDataSource() {
        super();
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the dataSourceID attribute. 
     *
     * @generated
     */
    @Id
    @Column(name=dataSourceIDColumn)
    @DataType(jdbcType=dataSourceIDJdbcType, precision=dataSourceIDPrecision)
    public Long getDataSourceID (){
        return dataSourceID;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the dataSourceID attribute. 
     *
     * @param dataSourceID
     *     The new value of DataSourceID. 
     * @generated
     */
    public void setDataSourceID( Long dataSourceID ){
        this.dataSourceID = dataSourceID;
		
        super.setIdPK(dataSourceID);
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the entityName attribute. 
     *
     * @generated
     */
    @Column(name=entityNameColumn)
    @DataType(jdbcType=entityNameJdbcType, precision=entityNamePrecision)
    public String getEntityName (){
        return entityName;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the entityName attribute. 
     *
     * @param entityName
     *     The new value of EntityName. 
     * @generated
     */
    public void setEntityName( String entityName ){
        this.entityName = entityName;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the instancePK attribute. 
     *
     * @generated
     */
    @Column(name=instancePKColumn)
    @DataType(jdbcType=instancePKJdbcType, precision=instancePKPrecision)
    public Long getInstancePK (){
        return instancePK;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the instancePK attribute. 
     *
     * @param instancePK
     *     The new value of InstancePK. 
     * @generated
     */
    public void setInstancePK( Long instancePK ){
        this.instancePK = instancePK;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the attributeName attribute. 
     *
     * @generated
     */
    @Column(name=attributeNameColumn)
    @DataType(jdbcType=attributeNameJdbcType, precision=attributeNamePrecision)
    public String getAttributeName (){
        return attributeName;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the attributeName attribute. 
     *
     * @param attributeName
     *     The new value of AttributeName. 
     * @generated
     */
    public void setAttributeName( String attributeName ){
        this.attributeName = attributeName;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the startDate attribute. 
     *
     * @generated
     */
    @Column(name=startDateColumn)
    @DataType(jdbcType=startDateJdbcType)
    public Timestamp getStartDate (){
        return startDate;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the startDate attribute. 
     *
     * @param startDate
     *     The new value of StartDate. 
     * @generated
     */
    public void setStartDate( Timestamp startDate ){
        this.startDate = startDate;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the endDate attribute. 
     *
     * @generated
     */
    @Column(name=endDateColumn)
    @DataType(jdbcType=endDateJdbcType)
    public Timestamp getEndDate (){
        return endDate;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the endDate attribute. 
     *
     * @param endDate
     *     The new value of EndDate. 
     * @generated
     */
    public void setEndDate( Timestamp endDate ){
        this.endDate = endDate;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the adminSystem attribute. 
     *
     * @generated
     */
    @Column(name=adminSystemColumn)
    @DataType(jdbcType=adminSystemJdbcType, precision=adminSystemPrecision)
    public Long getAdminSystem (){
        return adminSystem;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the adminSystem attribute. 
     *
     * @param adminSystem
     *     The new value of AdminSystem. 
     * @generated
     */
    public void setAdminSystem( Long adminSystem ){
        this.adminSystem = adminSystem;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the primary key. 
     *
     * @param aUniqueId
     *     The new value of the primary key. 
     * @generated
	 */
	public void setPrimaryKey(Object aUniqueId) {
		this.setDataSourceID((Long)aUniqueId);
	}

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the primary key.
     *
     * @generated
     */
	public Object getPrimaryKey() {
		return this.getDataSourceID();
	}
	 
}


