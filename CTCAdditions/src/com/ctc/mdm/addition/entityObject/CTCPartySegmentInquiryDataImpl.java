package com.ctc.mdm.addition.entityObject;

import java.sql.SQLException;
import com.ibm.pdq.runtime.generator.BaseData;
import com.ibm.pdq.annotation.Metadata;
import java.sql.Types;
import java.sql.PreparedStatement;
import com.ibm.pdq.runtime.generator.BaseRowHandler;
import com.ibm.pdq.runtime.statement.SqlStatementType;
import com.ibm.mdm.base.db.ResultQueue1;
import com.ibm.pdq.runtime.generator.BaseParameterHandler;
import java.util.Iterator;
import com.ibm.pdq.runtime.statement.StatementDescriptor;
import com.ctc.mdm.addition.entityObject.EObjCTCPartySegment;


@SuppressWarnings("unchecked")

/**
 * <!-- begin-user-doc -->
 * 
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class CTCPartySegmentInquiryDataImpl  extends BaseData implements CTCPartySegmentInquiryData
{

  /**
   * @generated
   */
  public static final String generatorVersion = "3.200.75";

  /**
   * @generated
   */
  public static final String identifier = "CTCPartySegmentInquiryData";

  /**
   * @generated
   */
  public static final long generationTime = 0x000001535f9cbc12L;

  /**
   * @generated
   */
  public static final String collection = "NULLID";

  /**
   * @generated
   */
  public static final String packageVersion = null;

  /**
   * @generated
   */
  public static final boolean forceSingleBindIsolation = false;

  /**
   * @generated
   */
  public CTCPartySegmentInquiryDataImpl()
  {
    super();
  } 

  /**
   * @generated
   */
  public String getGeneratorVersion()
  {
    return generatorVersion;
  }

  /**
   * @Select( sql="SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.party_segment_id = ? ", pattern="tableAlias (CTCPARTYSEGMENT => com.ctc.mdm.addition.entityObject.EObjCTCPartySegment, H_CTCPARTYSEGMENT => com.ctc.mdm.addition.entityObject.EObjCTCPartySegment)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCPartySegment>> getCTCPartySegment (Object[] parameters)
  {
    return queryIterator (getCTCPartySegmentStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getCTCPartySegmentStatementDescriptor = createStatementDescriptor (
    "getCTCPartySegment(Object[])",
    "SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.party_segment_id = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"party_segment_id", "cont_id", "segment_provider", "business_group_name", "segment_type_name", "segment_value", "segment_category_type", "effective_start_dt", "effective_end_dt", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetCTCPartySegmentParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    new GetCTCPartySegmentRowHandler (),
    new int[][]{ {Types.BIGINT, Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 19, 250, 250, 250, 250, 250, 0, 0, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    1);

  /**
   * @generated
   */
  public static class GetCTCPartySegmentParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetCTCPartySegmentRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCPartySegment>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCPartySegment> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCPartySegment> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCPartySegment> ();

      EObjCTCPartySegment returnObject1 = new EObjCTCPartySegment ();
      returnObject1.setPartySegmentId(getLongObject (rs, 1)); 
      returnObject1.setContId(getLongObject (rs, 2)); 
      returnObject1.setSegmentProvider(getString (rs, 3)); 
      returnObject1.setBusinessGroupName(getString (rs, 4)); 
      returnObject1.setSegmentTypeName(getString (rs, 5)); 
      returnObject1.setSegmentValue(getString (rs, 6)); 
      returnObject1.setSegmentCategoryType(getString (rs, 7)); 
      returnObject1.setEffectiveStartDt(getTimestamp (rs, 8)); 
      returnObject1.setEffectiveEndDt(getTimestamp (rs, 9)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 10)); 
      returnObject1.setLastUpdateUser(getString (rs, 11)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 12)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * @Select( sql="SELECT r.H_party_segment_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCPARTYSEGMENT r WHERE r.H_party_segment_id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))", pattern="tableAlias (CTCPARTYSEGMENT => com.ctc.mdm.addition.entityObject.EObjCTCPartySegment, H_CTCPARTYSEGMENT => com.ctc.mdm.addition.entityObject.EObjCTCPartySegment)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCPartySegment>> getCTCPartySegmentHistory (Object[] parameters)
  {
    return queryIterator (getCTCPartySegmentHistoryStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getCTCPartySegmentHistoryStatementDescriptor = createStatementDescriptor (
    "getCTCPartySegmentHistory(Object[])",
    "SELECT r.H_party_segment_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCPARTYSEGMENT r WHERE r.H_party_segment_id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"historyidpk", "h_action_code", "h_created_by", "h_create_dt", "h_end_dt", "party_segment_id", "cont_id", "segment_provider", "business_group_name", "segment_type_name", "segment_value", "segment_category_type", "effective_start_dt", "effective_end_dt", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetCTCPartySegmentHistoryParameterHandler (),
    new int[][]{{Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP}, {19, 0, 0}, {0, 0, 0}, {1, 1, 1}},
    null,
    new GetCTCPartySegmentHistoryRowHandler (),
    new int[][]{ {Types.BIGINT, Types.CHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 1, 20, 0, 0, 19, 19, 250, 250, 250, 250, 250, 0, 0, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    2);

  /**
   * @generated
   */
  public static class GetCTCPartySegmentHistoryParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
      setObject (stmt, 2, Types.TIMESTAMP, parameters[1], 0);
      setObject (stmt, 3, Types.TIMESTAMP, parameters[2], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetCTCPartySegmentHistoryRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCPartySegment>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCPartySegment> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCPartySegment> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCPartySegment> ();

      EObjCTCPartySegment returnObject1 = new EObjCTCPartySegment ();
      returnObject1.setHistoryIdPK(getLongObject (rs, 1)); 
      returnObject1.setHistActionCode(getString (rs, 2)); 
      returnObject1.setHistCreatedBy(getString (rs, 3)); 
      returnObject1.setHistCreateDt(getTimestamp (rs, 4)); 
      returnObject1.setHistEndDt(getTimestamp (rs, 5)); 
      returnObject1.setPartySegmentId(getLongObject (rs, 6)); 
      returnObject1.setContId(getLongObject (rs, 7)); 
      returnObject1.setSegmentProvider(getString (rs, 8)); 
      returnObject1.setBusinessGroupName(getString (rs, 9)); 
      returnObject1.setSegmentTypeName(getString (rs, 10)); 
      returnObject1.setSegmentValue(getString (rs, 11)); 
      returnObject1.setSegmentCategoryType(getString (rs, 12)); 
      returnObject1.setEffectiveStartDt(getTimestamp (rs, 13)); 
      returnObject1.setEffectiveEndDt(getTimestamp (rs, 14)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 15)); 
      returnObject1.setLastUpdateUser(getString (rs, 16)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 17)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * @Select( sql="SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.cont_id = ? ", pattern="tableAlias (CTCPARTYSEGMENT => com.ctc.mdm.addition.entityObject.EObjCTCPartySegment, H_CTCPARTYSEGMENT => com.ctc.mdm.addition.entityObject.EObjCTCPartySegment)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCPartySegment>> getAllCTCPartySegment (Object[] parameters)
  {
    return queryIterator (getAllCTCPartySegmentStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllCTCPartySegmentStatementDescriptor = createStatementDescriptor (
    "getAllCTCPartySegment(Object[])",
    "SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.cont_id = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"party_segment_id", "cont_id", "segment_provider", "business_group_name", "segment_type_name", "segment_value", "segment_category_type", "effective_start_dt", "effective_end_dt", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllCTCPartySegmentParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    new GetAllCTCPartySegmentRowHandler (),
    new int[][]{ {Types.BIGINT, Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 19, 250, 250, 250, 250, 250, 0, 0, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    3);

  /**
   * @generated
   */
  public static class GetAllCTCPartySegmentParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllCTCPartySegmentRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCPartySegment>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCPartySegment> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCPartySegment> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCPartySegment> ();

      EObjCTCPartySegment returnObject1 = new EObjCTCPartySegment ();
      returnObject1.setPartySegmentId(getLongObject (rs, 1)); 
      returnObject1.setContId(getLongObject (rs, 2)); 
      returnObject1.setSegmentProvider(getString (rs, 3)); 
      returnObject1.setBusinessGroupName(getString (rs, 4)); 
      returnObject1.setSegmentTypeName(getString (rs, 5)); 
      returnObject1.setSegmentValue(getString (rs, 6)); 
      returnObject1.setSegmentCategoryType(getString (rs, 7)); 
      returnObject1.setEffectiveStartDt(getTimestamp (rs, 8)); 
      returnObject1.setEffectiveEndDt(getTimestamp (rs, 9)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 10)); 
      returnObject1.setLastUpdateUser(getString (rs, 11)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 12)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  //added - start
  
  /**
   * SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_Provider segment_Provider, r.business_Group_Name business_Group_Name, r.segment_Type_Name segment_Type_Name, r.segment_Value segment_Value, r.segment_Category_Type segment_Category_Type, r.effective_Start_Dt effective_Start_Dt, r.effective_End_Dt effective_End_Dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CT_PARTYSEGMENT r WHERE r.cont_Id = ? 
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCPartySegment>> getAllActiveCTCPartySegment (Object[] parameters)
  {
    return queryIterator (getAllActiveCTCPartySegmentDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllActiveCTCPartySegmentDescriptor = createStatementDescriptor (
    "getAllActiveCTCPartySegment(Object[])",
    "SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.cont_id = ? AND (r.EFFECTIVE_END_DT is null or r.EFFECTIVE_END_DT > ?)",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY},
    SqlStatementType.QUERY,
    new String[]{"party_segment_id", "cont_id", "segment_provider", "business_group_name", "segment_type_name", "segment_value", "segment_category_type", "effective_start_dt", "effective_end_dt", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllActiveCTCPartySegmentParameterHandler (),
    new int[][]{{Types.VARCHAR}, {250}, {0}, {1}},
    null,
    new GetAllActiveCTCPartySegmentRowHandler (),
    new int[][]{ {Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 250, 250, 250, 250, 250, 250, 0, 0, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    3);

  /**
   * @generated
   */
  public static class GetAllActiveCTCPartySegmentParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.VARCHAR, parameters[0], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllActiveCTCPartySegmentRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCPartySegment>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCPartySegment> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCPartySegment> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCPartySegment> ();

      EObjCTCPartySegment returnObject1 = new EObjCTCPartySegment ();
      returnObject1.setPartySegmentId(getLongObject (rs, 1)); 
      returnObject1.setContId(getLongObject (rs, 2)); 
      returnObject1.setSegmentProvider(getString (rs, 3)); 
      returnObject1.setBusinessGroupName(getString (rs, 4)); 
      returnObject1.setSegmentTypeName(getString (rs, 5)); 
      returnObject1.setSegmentValue(getString (rs, 6)); 
      returnObject1.setSegmentCategoryType(getString (rs, 7)); 
      returnObject1.setEffectiveStartDt(getTimestamp (rs, 8)); 
      returnObject1.setEffectiveEndDt(getTimestamp (rs, 9)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 10)); 
      returnObject1.setLastUpdateUser(getString (rs, 11)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 12)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  
  //added - ends
  
  
//added - start
  
  /**
   * SELECT r.Party_Segmentpk_Id Party_Segmentpk_Id, r.cont_Id cont_Id, r.segment_Provider segment_Provider, r.business_Group_Name business_Group_Name, r.segment_Type_Name segment_Type_Name, r.segment_Value segment_Value, r.segment_Category_Type segment_Category_Type, r.effective_Start_Dt effective_Start_Dt, r.effective_End_Dt effective_End_Dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CT_PARTYSEGMENT r WHERE r.cont_Id = ? AND r.EFFECTIVE_END_DT < ? 
   * @generated NOT
   */
  public Iterator<ResultQueue1<EObjCTCPartySegment>> getAllInactiveCTCPartySegment (Object[] parameters)
  {
    return queryIterator (getAllInactiveCTCPartySegmentDescriptor, parameters);
  }

  /**
   * @generated NOT
   */
  @Metadata ()
  public static final StatementDescriptor getAllInactiveCTCPartySegmentDescriptor = createStatementDescriptor (
    "getAllInactiveCTCPartySegment(Object[])",
    "SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.cont_id = ? AND r.EFFECTIVE_END_DT < ?",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY},
    SqlStatementType.QUERY,
    new String[]{"party_segment_id", "cont_id", "segment_provider", "business_group_name", "segment_type_name", "segment_value", "segment_category_type", "effective_start_dt", "effective_end_dt", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new getAllInactiveCTCPartySegmentParameterHandler (),
    new int[][]{{Types.VARCHAR}, {250}, {0}, {1}},
    null,
    new getAllInactiveCTCPartySegmentRowHandler (),
    new int[][]{ {Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 250, 250, 250, 250, 250, 250, 0, 0, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    3);

  /**
   * @generated NOT
   */
  public static class getAllInactiveCTCPartySegmentParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated NOT
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.VARCHAR, parameters[0], 0);
    }
  }

  /**
   * @generated NOT
   */
  public static class getAllInactiveCTCPartySegmentRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCPartySegment>>
  {
    /**
     * @generated NOT
     */
    public ResultQueue1<EObjCTCPartySegment> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCPartySegment> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCPartySegment> ();

      EObjCTCPartySegment returnObject1 = new EObjCTCPartySegment ();
      returnObject1.setPartySegmentId(getLongObject (rs, 1)); 
      returnObject1.setContId(getLongObject (rs, 2));  
      returnObject1.setSegmentProvider(getString (rs, 3)); 
      returnObject1.setBusinessGroupName(getString (rs, 4)); 
      returnObject1.setSegmentTypeName(getString (rs, 5)); 
      returnObject1.setSegmentValue(getString (rs, 6)); 
      returnObject1.setSegmentCategoryType(getString (rs, 7)); 
      returnObject1.setEffectiveStartDt(getTimestamp (rs, 8)); 
      returnObject1.setEffectiveEndDt(getTimestamp (rs, 9)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 10)); 
      returnObject1.setLastUpdateUser(getString (rs, 11)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 12)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  
  //added - ends
  
  
  /**
   * SELECT r.H_Party_Segmentpk_Id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.Party_Segmentpk_Id Party_Segmentpk_Id, r.cont_Id cont_Id, r.segment_Provider segment_Provider, r.business_Group_Name business_Group_Name, r.segment_Type_Name segment_Type_Name, r.segment_Value segment_Value, r.segment_Category_Type segment_Category_Type, r.effective_Start_Dt effective_Start_Dt, r.effective_End_Dt effective_End_Dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CT_PARTYSEGMENT r WHERE r.cont_Id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCPartySegment>> getAllCTCPartySegmentHistory (Object[] parameters)
  {
    return queryIterator (getAllCTCPartySegmentHistoryStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllCTCPartySegmentHistoryStatementDescriptor = createStatementDescriptor (
    "getAllCTCPartySegmentHistory(Object[])",
    "SELECT r.H_party_segment_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCPARTYSEGMENT r WHERE r.cont_id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"historyidpk", "h_action_code", "h_created_by", "h_create_dt", "h_end_dt", "party_segment_id", "cont_id", "segment_provider", "business_group_name", "segment_type_name", "segment_value", "segment_category_type", "effective_start_dt", "effective_end_dt", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllCTCPartySegmentHistoryParameterHandler (),
    new int[][]{{Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP}, {19, 0, 0}, {0, 0, 0}, {1, 1, 1}},
    null,
    new GetAllCTCPartySegmentHistoryRowHandler (),
    new int[][]{ {Types.BIGINT, Types.CHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 1, 20, 0, 0, 19, 19, 250, 250, 250, 250, 250, 0, 0, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    4);

  /**
   * @generated
   */
  public static class GetAllCTCPartySegmentHistoryParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
      setObject (stmt, 2, Types.TIMESTAMP, parameters[1], 0);
      setObject (stmt, 3, Types.TIMESTAMP, parameters[2], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllCTCPartySegmentHistoryRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCPartySegment>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCPartySegment> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCPartySegment> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCPartySegment> ();

      EObjCTCPartySegment returnObject1 = new EObjCTCPartySegment ();
      returnObject1.setHistoryIdPK(getLongObject (rs, 1)); 
      returnObject1.setHistActionCode(getString (rs, 2)); 
      returnObject1.setHistCreatedBy(getString (rs, 3)); 
      returnObject1.setHistCreateDt(getTimestamp (rs, 4)); 
      returnObject1.setHistEndDt(getTimestamp (rs, 5)); 
      returnObject1.setPartySegmentId(getLongObject (rs, 6)); 
      returnObject1.setContId(getLongObject (rs, 7)); 
      returnObject1.setSegmentProvider(getString (rs, 8)); 
      returnObject1.setBusinessGroupName(getString (rs, 9)); 
      returnObject1.setSegmentTypeName(getString (rs, 10)); 
      returnObject1.setSegmentValue(getString (rs, 11)); 
      returnObject1.setSegmentCategoryType(getString (rs, 12)); 
      returnObject1.setEffectiveStartDt(getTimestamp (rs, 13)); 
      returnObject1.setEffectiveEndDt(getTimestamp (rs, 14)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 15)); 
      returnObject1.setLastUpdateUser(getString (rs, 16)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 17)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

}
