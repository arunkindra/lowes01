package com.ctc.mdm.addition.entityObject;

import java.sql.SQLException;
import com.ibm.pdq.runtime.generator.BaseData;
import com.ibm.pdq.annotation.Metadata;
import java.sql.Types;
import java.sql.PreparedStatement;
import com.ibm.pdq.runtime.generator.BaseRowHandler;
import com.ibm.pdq.runtime.statement.SqlStatementType;
import com.ctc.mdm.addition.entityObject.EObjCTCDataSource;
import com.ibm.pdq.runtime.generator.BaseParameterHandler;
import java.util.Iterator;
import com.ibm.pdq.runtime.statement.StatementDescriptor;


/**
 * <!-- begin-user-doc -->
 * 
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class EObjCTCDataSourceDataImpl  extends BaseData implements EObjCTCDataSourceData
{

  /**
   * @generated
   */
  public static final String generatorVersion = "3.200.75";

  /**
   * @generated
   */
  public static final String identifier = "EObjCTCDataSourceData";

  /**
   * @generated
   */
  public static final long generationTime = 0x000001535f9cbc7bL;

  /**
   * @generated
   */
  public static final String collection = "NULLID";

  /**
   * @generated
   */
  public static final String packageVersion = null;

  /**
   * @generated
   */
  public static final boolean forceSingleBindIsolation = false;

  /**
   * @generated
   */
  public EObjCTCDataSourceDataImpl()
  {
    super();
  } 

  /**
   * @generated
   */
  public String getGeneratorVersion()
  {
    return generatorVersion;
  }

  /**
   * @Select( sql="select data_source_id, entity_name, instance_pk, attribute_name, start_dt, end_dt, admin_sys_tp_cd, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from CTCDATASOURCE where data_source_id = ? " )
   * 
   * @generated
   */
  public Iterator<EObjCTCDataSource> getEObjCTCDataSource (Long dataSourceID)
  {
    return queryIterator (getEObjCTCDataSourceStatementDescriptor, dataSourceID);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getEObjCTCDataSourceStatementDescriptor = createStatementDescriptor (
    "getEObjCTCDataSource(Long)",
    "select data_source_id, entity_name, instance_pk, attribute_name, start_dt, end_dt, admin_sys_tp_cd, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from CTCDATASOURCE where data_source_id = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"data_source_id", "entity_name", "instance_pk", "attribute_name", "start_dt", "end_dt", "admin_sys_tp_cd", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetEObjCTCDataSourceParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    new GetEObjCTCDataSourceRowHandler (),
    new int[][]{ {Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 255, 19, 250, 0, 0, 19, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    1);

  /**
   * @generated
   */
  public static class GetEObjCTCDataSourceParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setLong (stmt, 1, Types.BIGINT, (Long)parameters[0]);
    }
  }

  /**
   * @generated
   */
  public static class GetEObjCTCDataSourceRowHandler extends BaseRowHandler<EObjCTCDataSource>
  {
    /**
     * @generated
     */
    public EObjCTCDataSource handle (java.sql.ResultSet rs, EObjCTCDataSource returnObject) throws java.sql.SQLException
    {
      returnObject = new EObjCTCDataSource ();
      returnObject.setDataSourceID(getLongObject (rs, 1)); 
      returnObject.setEntityName(getString (rs, 2)); 
      returnObject.setInstancePK(getLongObject (rs, 3)); 
      returnObject.setAttributeName(getString (rs, 4)); 
      returnObject.setStartDate(getTimestamp (rs, 5)); 
      returnObject.setEndDate(getTimestamp (rs, 6)); 
      returnObject.setAdminSystem(getLongObject (rs, 7)); 
      returnObject.setLastUpdateDt(getTimestamp (rs, 8)); 
      returnObject.setLastUpdateUser(getString (rs, 9)); 
      returnObject.setLastUpdateTxId(getLongObject (rs, 10)); 
    
      return returnObject;
    }
  }

  /**
   * @Update( sql="insert into CTCDATASOURCE (data_source_id, entity_name, instance_pk, attribute_name, start_dt, end_dt, admin_sys_tp_cd, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values( :dataSourceID, :entityName, :instancePK, :attributeName, :startDate, :endDate, :adminSystem, :lastUpdateDt, :lastUpdateUser, :lastUpdateTxId)" )
   * 
   * @generated
   */
  public int createEObjCTCDataSource (EObjCTCDataSource e)
  {
    return update (createEObjCTCDataSourceStatementDescriptor, e);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor createEObjCTCDataSourceStatementDescriptor = createStatementDescriptor (
    "createEObjCTCDataSource(com.ctc.mdm.addition.entityObject.EObjCTCDataSource)",
    "insert into CTCDATASOURCE (data_source_id, entity_name, instance_pk, attribute_name, start_dt, end_dt, admin_sys_tp_cd, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values(  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? )",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.INSERT,
    null,
    new CreateEObjCTCDataSourceParameterHandler (),
    new int[][]{{Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 255, 19, 250, 0, 0, 19, 0, 0, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    2);

  /**
   * @generated
   */
  public static class CreateEObjCTCDataSourceParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      EObjCTCDataSource bean0 = (EObjCTCDataSource) parameters[0];
      setLong (stmt, 1, Types.BIGINT, (Long)bean0.getDataSourceID());
      setString (stmt, 2, Types.VARCHAR, (String)bean0.getEntityName());
      setLong (stmt, 3, Types.BIGINT, (Long)bean0.getInstancePK());
      setString (stmt, 4, Types.VARCHAR, (String)bean0.getAttributeName());
      setTimestamp (stmt, 5, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getStartDate());
      setTimestamp (stmt, 6, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getEndDate());
      setLong (stmt, 7, Types.BIGINT, (Long)bean0.getAdminSystem());
      setTimestamp (stmt, 8, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getLastUpdateDt());
      setString (stmt, 9, Types.VARCHAR, (String)bean0.getLastUpdateUser());
      setLong (stmt, 10, Types.BIGINT, (Long)bean0.getLastUpdateTxId());
    }
  }

  /**
   * @Update( sql="update CTCDATASOURCE set entity_name = :entityName, instance_pk = :instancePK, attribute_name = :attributeName, start_dt = :startDate, end_dt = :endDate, admin_sys_tp_cd = :adminSystem, LAST_UPDATE_DT = :lastUpdateDt, LAST_UPDATE_USER = :lastUpdateUser, LAST_UPDATE_TX_ID = :lastUpdateTxId where data_source_id = :dataSourceID and LAST_UPDATE_DT = :oldLastUpdateDt" )
   * 
   * @generated
   */
  public int updateEObjCTCDataSource (EObjCTCDataSource e)
  {
    return update (updateEObjCTCDataSourceStatementDescriptor, e);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor updateEObjCTCDataSourceStatementDescriptor = createStatementDescriptor (
    "updateEObjCTCDataSource(com.ctc.mdm.addition.entityObject.EObjCTCDataSource)",
    "update CTCDATASOURCE set entity_name =  ? , instance_pk =  ? , attribute_name =  ? , start_dt =  ? , end_dt =  ? , admin_sys_tp_cd =  ? , LAST_UPDATE_DT =  ? , LAST_UPDATE_USER =  ? , LAST_UPDATE_TX_ID =  ?  where data_source_id =  ?  and LAST_UPDATE_DT =  ? ",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.UPDATE,
    null,
    new UpdateEObjCTCDataSourceParameterHandler (),
    new int[][]{{Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP}, {255, 19, 250, 0, 0, 19, 0, 0, 19, 19, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    3);

  /**
   * @generated
   */
  public static class UpdateEObjCTCDataSourceParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      EObjCTCDataSource bean0 = (EObjCTCDataSource) parameters[0];
      setString (stmt, 1, Types.VARCHAR, (String)bean0.getEntityName());
      setLong (stmt, 2, Types.BIGINT, (Long)bean0.getInstancePK());
      setString (stmt, 3, Types.VARCHAR, (String)bean0.getAttributeName());
      setTimestamp (stmt, 4, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getStartDate());
      setTimestamp (stmt, 5, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getEndDate());
      setLong (stmt, 6, Types.BIGINT, (Long)bean0.getAdminSystem());
      setTimestamp (stmt, 7, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getLastUpdateDt());
      setString (stmt, 8, Types.VARCHAR, (String)bean0.getLastUpdateUser());
      setLong (stmt, 9, Types.BIGINT, (Long)bean0.getLastUpdateTxId());
      setLong (stmt, 10, Types.BIGINT, (Long)bean0.getDataSourceID());
      setTimestamp (stmt, 11, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getOldLastUpdateDt());
    }
  }

  /**
   * @Update( sql="delete from CTCDATASOURCE where data_source_id = ?" )
   * 
   * @generated
   */
  public int deleteEObjCTCDataSource (Long dataSourceID)
  {
    return update (deleteEObjCTCDataSourceStatementDescriptor, dataSourceID);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor deleteEObjCTCDataSourceStatementDescriptor = createStatementDescriptor (
    "deleteEObjCTCDataSource(Long)",
    "delete from CTCDATASOURCE where data_source_id = ?",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.DELETE,
    null,
    new DeleteEObjCTCDataSourceParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    4);

  /**
   * @generated
   */
  public static class DeleteEObjCTCDataSourceParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setLong (stmt, 1, Types.BIGINT, (Long)parameters[0]);
    }
  }

}
