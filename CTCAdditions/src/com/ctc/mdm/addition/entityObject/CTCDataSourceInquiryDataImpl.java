package com.ctc.mdm.addition.entityObject;

import java.sql.SQLException;
import com.ibm.pdq.runtime.generator.BaseData;
import com.ibm.pdq.annotation.Metadata;
import java.sql.Types;
import java.sql.PreparedStatement;
import com.ibm.pdq.runtime.generator.BaseRowHandler;
import com.ibm.pdq.runtime.statement.SqlStatementType;
import com.ibm.mdm.base.db.ResultQueue1;
import com.ctc.mdm.addition.entityObject.EObjCTCDataSource;
import com.ibm.pdq.runtime.generator.BaseParameterHandler;
import java.util.Iterator;
import com.ibm.pdq.runtime.statement.StatementDescriptor;


@SuppressWarnings("unchecked")

/**
 * <!-- begin-user-doc -->
 * 
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class CTCDataSourceInquiryDataImpl  extends BaseData implements CTCDataSourceInquiryData
{

  /**
   * @generated
   */
  public static final String generatorVersion = "3.200.75";

  /**
   * @generated
   */
  public static final String identifier = "CTCDataSourceInquiryData";

  /**
   * @generated
   */
  public static final long generationTime = 0x000001535f9cbaddL;

  /**
   * @generated
   */
  public static final String collection = "NULLID";

  /**
   * @generated
   */
  public static final String packageVersion = null;

  /**
   * @generated
   */
  public static final boolean forceSingleBindIsolation = false;

  /**
   * @generated
   */
  public CTCDataSourceInquiryDataImpl()
  {
    super();
  } 

  /**
   * @generated
   */
  public String getGeneratorVersion()
  {
    return generatorVersion;
  }

  /**
   * @Select( sql="SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.data_source_id = ? ", pattern="tableAlias (CTCDATASOURCE => com.ctc.mdm.addition.entityObject.EObjCTCDataSource, H_CTCDATASOURCE => com.ctc.mdm.addition.entityObject.EObjCTCDataSource)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCDataSource>> getCTCDataSource (Object[] parameters)
  {
    return queryIterator (getCTCDataSourceStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getCTCDataSourceStatementDescriptor = createStatementDescriptor (
    "getCTCDataSource(Object[])",
    "SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.data_source_id = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"data_source_id", "entity_name", "instance_pk", "attribute_name", "start_dt", "end_dt", "admin_sys_tp_cd", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetCTCDataSourceParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    new GetCTCDataSourceRowHandler (),
    new int[][]{ {Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 255, 19, 250, 0, 0, 19, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    1);

  /**
   * @generated
   */
  public static class GetCTCDataSourceParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetCTCDataSourceRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCDataSource>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCDataSource> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCDataSource> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCDataSource> ();

      EObjCTCDataSource returnObject1 = new EObjCTCDataSource ();
      returnObject1.setDataSourceID(getLongObject (rs, 1)); 
      returnObject1.setEntityName(getString (rs, 2)); 
      returnObject1.setInstancePK(getLongObject (rs, 3)); 
      returnObject1.setAttributeName(getString (rs, 4)); 
      returnObject1.setStartDate(getTimestamp (rs, 5)); 
      returnObject1.setEndDate(getTimestamp (rs, 6)); 
      returnObject1.setAdminSystem(getLongObject (rs, 7)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 8)); 
      returnObject1.setLastUpdateUser(getString (rs, 9)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 10)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * @Select( sql="SELECT r.H_data_source_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCDATASOURCE r WHERE r.H_data_source_id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))", pattern="tableAlias (CTCDATASOURCE => com.ctc.mdm.addition.entityObject.EObjCTCDataSource, H_CTCDATASOURCE => com.ctc.mdm.addition.entityObject.EObjCTCDataSource)" )
   * 
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCDataSource>> getCTCDataSourceHistory (Object[] parameters)
  {
    return queryIterator (getCTCDataSourceHistoryStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getCTCDataSourceHistoryStatementDescriptor = createStatementDescriptor (
    "getCTCDataSourceHistory(Object[])",
    "SELECT r.H_data_source_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCDATASOURCE r WHERE r.H_data_source_id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"historyidpk", "h_action_code", "h_created_by", "h_create_dt", "h_end_dt", "data_source_id", "entity_name", "instance_pk", "attribute_name", "start_dt", "end_dt", "admin_sys_tp_cd", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetCTCDataSourceHistoryParameterHandler (),
    new int[][]{{Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP}, {19, 0, 0}, {0, 0, 0}, {1, 1, 1}},
    null,
    new GetCTCDataSourceHistoryRowHandler (),
    new int[][]{ {Types.BIGINT, Types.CHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 1, 20, 0, 0, 19, 255, 19, 250, 0, 0, 19, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    2);

  /**
   * @generated
   */
  public static class GetCTCDataSourceHistoryParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.BIGINT, parameters[0], 0);
      setObject (stmt, 2, Types.TIMESTAMP, parameters[1], 0);
      setObject (stmt, 3, Types.TIMESTAMP, parameters[2], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetCTCDataSourceHistoryRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCDataSource>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCDataSource> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCDataSource> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCDataSource> ();

      EObjCTCDataSource returnObject1 = new EObjCTCDataSource ();
      returnObject1.setHistoryIdPK(getLongObject (rs, 1)); 
      returnObject1.setHistActionCode(getString (rs, 2)); 
      returnObject1.setHistCreatedBy(getString (rs, 3)); 
      returnObject1.setHistCreateDt(getTimestamp (rs, 4)); 
      returnObject1.setHistEndDt(getTimestamp (rs, 5)); 
      returnObject1.setDataSourceID(getLongObject (rs, 6)); 
      returnObject1.setEntityName(getString (rs, 7)); 
      returnObject1.setInstancePK(getLongObject (rs, 8)); 
      returnObject1.setAttributeName(getString (rs, 9)); 
      returnObject1.setStartDate(getTimestamp (rs, 10)); 
      returnObject1.setEndDate(getTimestamp (rs, 11)); 
      returnObject1.setAdminSystem(getLongObject (rs, 12)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 13)); 
      returnObject1.setLastUpdateUser(getString (rs, 14)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 15)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND (r.end_dt is null or r.end_dt > ?) 
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCDataSource>> getAllActiveCTCDataSourcesByEntity (Object[] parameters)
  {
    return queryIterator (getAllActiveCTCDataSourcesByEntityStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllActiveCTCDataSourcesByEntityStatementDescriptor = createStatementDescriptor (
    "getAllActiveCTCDataSourcesByEntity(Object[])",
    "SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND (r.end_dt is null or r.end_dt > ?) ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY},
    SqlStatementType.QUERY,
    new String[]{"data_source_id", "entity_name", "instance_pk", "attribute_name", "start_dt", "end_dt", "admin_sys_tp_cd", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllActiveCTCDataSourcesByEntityParameterHandler (),
    new int[][]{{Types.VARCHAR, Types.BIGINT}, {250, 19}, {0, 0}, {1, 1}},
    null,
    new GetAllActiveCTCDataSourcesByEntityRowHandler (),
    new int[][]{ {Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 250, 19, 250, 0, 0, 19, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    3);

  /**
   * @generated
   */
  public static class GetAllActiveCTCDataSourcesByEntityParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.VARCHAR, parameters[0], 0);
      setObject (stmt, 2, Types.BIGINT, parameters[1], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllActiveCTCDataSourcesByEntityRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCDataSource>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCDataSource> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCDataSource> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCDataSource> ();

      EObjCTCDataSource returnObject1 = new EObjCTCDataSource ();
      returnObject1.setDataSourceID(getLongObject (rs, 1)); 
      returnObject1.setEntityName(getString (rs, 2)); 
      returnObject1.setInstancePK(getLongObject (rs, 3)); 
      returnObject1.setAttributeName(getString (rs, 4)); 
      returnObject1.setStartDate(getTimestamp (rs, 5)); 
      returnObject1.setEndDate(getTimestamp (rs, 6)); 
      returnObject1.setAdminSystem(getLongObject (rs, 7)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 8)); 
      returnObject1.setLastUpdateUser(getString (rs, 9)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 10)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND r.end_dt < ?
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCDataSource>> getAllInactiveCTCDataSourcesByEntity (Object[] parameters)
  {
    return queryIterator (getAllInactiveCTCDataSourcesByEntityStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllInactiveCTCDataSourcesByEntityStatementDescriptor = createStatementDescriptor (
    "getAllInactiveCTCDataSourcesByEntity(Object[])",
    "SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND r.end_dt < ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY},
    SqlStatementType.QUERY,
    new String[]{"data_source_id", "entity_name", "instance_pk", "attribute_name", "start_dt", "end_dt", "admin_sys_tp_cd", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllInactiveCTCDataSourcesByEntityParameterHandler (),
    new int[][]{{Types.VARCHAR, Types.BIGINT}, {250, 19}, {0, 0}, {1, 1}},
    null,
    new GetAllInactiveCTCDataSourcesByEntityRowHandler (),
    new int[][]{ {Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 250, 19, 250, 0, 0, 19, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    4);

  /**
   * @generated
   */
  public static class GetAllInactiveCTCDataSourcesByEntityParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.VARCHAR, parameters[0], 0);
      setObject (stmt, 2, Types.BIGINT, parameters[1], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllInactiveCTCDataSourcesByEntityRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCDataSource>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCDataSource> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCDataSource> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCDataSource> ();

      EObjCTCDataSource returnObject1 = new EObjCTCDataSource ();
      returnObject1.setDataSourceID(getLongObject (rs, 1)); 
      returnObject1.setEntityName(getString (rs, 2)); 
      returnObject1.setInstancePK(getLongObject (rs, 3)); 
      returnObject1.setAttributeName(getString (rs, 4)); 
      returnObject1.setStartDate(getTimestamp (rs, 5)); 
      returnObject1.setEndDate(getTimestamp (rs, 6)); 
      returnObject1.setAdminSystem(getLongObject (rs, 7)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 8)); 
      returnObject1.setLastUpdateUser(getString (rs, 9)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 10)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? 
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCDataSource>> getAllCTCDataSourcesByEntity (Object[] parameters)
  {
    return queryIterator (getAllCTCDataSourcesByEntityStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllCTCDataSourcesByEntityStatementDescriptor = createStatementDescriptor (
    "getAllCTCDataSourcesByEntity(Object[])",
    "SELECT r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY},
    SqlStatementType.QUERY,
    new String[]{"data_source_id", "entity_name", "instance_pk", "attribute_name", "start_dt", "end_dt", "admin_sys_tp_cd", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllCTCDataSourcesByEntityParameterHandler (),
    new int[][]{{Types.VARCHAR, Types.BIGINT}, {250, 19}, {0, 0}, {1, 1}},
    null,
    new GetAllCTCDataSourcesByEntityRowHandler (),
    new int[][]{ {Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 250, 19, 250, 0, 0, 19, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    5);

  /**
   * @generated
   */
  public static class GetAllCTCDataSourcesByEntityParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.VARCHAR, parameters[0], 0);
      setObject (stmt, 2, Types.BIGINT, parameters[1], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllCTCDataSourcesByEntityRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCDataSource>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCDataSource> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCDataSource> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCDataSource> ();

      EObjCTCDataSource returnObject1 = new EObjCTCDataSource ();
      returnObject1.setDataSourceID(getLongObject (rs, 1)); 
      returnObject1.setEntityName(getString (rs, 2)); 
      returnObject1.setInstancePK(getLongObject (rs, 3)); 
      returnObject1.setAttributeName(getString (rs, 4)); 
      returnObject1.setStartDate(getTimestamp (rs, 5)); 
      returnObject1.setEndDate(getTimestamp (rs, 6)); 
      returnObject1.setAdminSystem(getLongObject (rs, 7)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 8)); 
      returnObject1.setLastUpdateUser(getString (rs, 9)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 10)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

  /**
   * SELECT r.H_data_source_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))
   * @generated
   */
  public Iterator<ResultQueue1<EObjCTCDataSource>> getAllCTCDataSourcesByEntityHistory (Object[] parameters)
  {
    return queryIterator (getAllCTCDataSourcesByEntityHistoryStatementDescriptor, parameters);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getAllCTCDataSourcesByEntityHistoryStatementDescriptor = createStatementDescriptor (
    "getAllCTCDataSourcesByEntityHistory(Object[])",
    "SELECT r.H_data_source_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.data_source_id data_source_id, r.entity_name entity_name, r.instance_pk instance_pk, r.attribute_name attribute_name, r.start_dt start_dt, r.end_dt end_dt, r.admin_sys_tp_cd admin_sys_tp_cd, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCDATASOURCE r WHERE r.entity_name = ? AND r.instance_pk = ? AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY},
    SqlStatementType.QUERY,
    new String[]{"historyidpk", "h_action_code", "h_created_by", "h_create_dt", "h_end_dt", "data_source_id", "entity_name", "instance_pk", "attribute_name", "start_dt", "end_dt", "admin_sys_tp_cd", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetAllCTCDataSourcesByEntityHistoryParameterHandler (),
    new int[][]{{Types.VARCHAR, Types.BIGINT, Types.TIMESTAMP, Types.TIMESTAMP}, {250, 19, 0, 0}, {0, 0, 0, 0}, {1, 1, 1, 1}},
    null,
    new GetAllCTCDataSourcesByEntityHistoryRowHandler (),
    new int[][]{ {Types.BIGINT, Types.CHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.VARCHAR, Types.BIGINT, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.BIGINT, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 1, 20, 0, 0, 19, 250, 19, 250, 0, 0, 19, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    6);

  /**
   * @generated
   */
  public static class GetAllCTCDataSourcesByEntityHistoryParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setObject (stmt, 1, Types.VARCHAR, parameters[0], 0);
      setObject (stmt, 2, Types.BIGINT, parameters[1], 0);
      setObject (stmt, 3, Types.TIMESTAMP, parameters[2], 0);
      setObject (stmt, 4, Types.TIMESTAMP, parameters[3], 0);
    }
  }

  /**
   * @generated
   */
  public static class GetAllCTCDataSourcesByEntityHistoryRowHandler extends BaseRowHandler<ResultQueue1<EObjCTCDataSource>>
  {
    /**
     * @generated
     */
    public ResultQueue1<EObjCTCDataSource> handle (java.sql.ResultSet rs, ResultQueue1<EObjCTCDataSource> returnObject) throws java.sql.SQLException
    {
      returnObject = new ResultQueue1<EObjCTCDataSource> ();

      EObjCTCDataSource returnObject1 = new EObjCTCDataSource ();
      returnObject1.setHistoryIdPK(getLongObject (rs, 1)); 
      returnObject1.setHistActionCode(getString (rs, 2)); 
      returnObject1.setHistCreatedBy(getString (rs, 3)); 
      returnObject1.setHistCreateDt(getTimestamp (rs, 4)); 
      returnObject1.setHistEndDt(getTimestamp (rs, 5)); 
      returnObject1.setDataSourceID(getLongObject (rs, 6)); 
      returnObject1.setEntityName(getString (rs, 7)); 
      returnObject1.setInstancePK(getLongObject (rs, 8)); 
      returnObject1.setAttributeName(getString (rs, 9)); 
      returnObject1.setStartDate(getTimestamp (rs, 10)); 
      returnObject1.setEndDate(getTimestamp (rs, 11)); 
      returnObject1.setAdminSystem(getLongObject (rs, 12)); 
      returnObject1.setLastUpdateDt(getTimestamp (rs, 13)); 
      returnObject1.setLastUpdateUser(getString (rs, 14)); 
      returnObject1.setLastUpdateTxId(getLongObject (rs, 15)); 
      returnObject.add (returnObject1);

    
      return returnObject;
    }
  }

}
