/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[fa1248e2f395deb8fc22338d856caf17]
 */

package com.ctc.mdm.addition.entityObject;

import com.dwl.base.EObjCommon;
import com.ibm.mdm.base.db.DataType;
import com.ibm.pdq.annotation.Column;
import com.ibm.pdq.annotation.Table;


import com.ibm.pdq.annotation.Id;

import java.sql.Timestamp;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * The entity object corresponding to the CTCPartySegment business object. This
 * entity object should include all the attributes as defined by the business
 * object.
 * 
 * @generated
 */
@SuppressWarnings("serial")
@Table(name=EObjCTCPartySegment.tableName)
public class EObjCTCPartySegment extends EObjCommon {

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    public static final String tableName = "CTCPARTYSEGMENT";
    
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String partySegmentIdColumn = "PARTY_SEGMENT_ID";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String partySegmentIdJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    partySegmentIdPrecision = 19;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String contIdColumn = "CONT_ID";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String contIdJdbcType = "BIGINT";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    contIdPrecision = 19;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String segmentProviderColumn = "SEGMENT_PROVIDER";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String segmentProviderJdbcType = "VARCHAR";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    segmentProviderPrecision = 250;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String businessGroupNameColumn = "BUSINESS_GROUP_NAME";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String businessGroupNameJdbcType = "VARCHAR";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    businessGroupNamePrecision = 250;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String segmentTypeNameColumn = "SEGMENT_TYPE_NAME";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String segmentTypeNameJdbcType = "VARCHAR";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    segmentTypeNamePrecision = 250;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String segmentValueColumn = "SEGMENT_VALUE";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String segmentValueJdbcType = "VARCHAR";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    segmentValuePrecision = 250;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String segmentCategoryTypeColumn = "SEGMENT_CATEGORY_TYPE";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String segmentCategoryTypeJdbcType = "VARCHAR";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final int    segmentCategoryTypePrecision = 250;
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String effectiveStartDtColumn = "EFFECTIVE_START_DT";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String effectiveStartDtJdbcType = "TIMESTAMP";
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String effectiveEndDtColumn = "EFFECTIVE_END_DT";
  
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * @generated
     */
    private static final String effectiveEndDtJdbcType = "TIMESTAMP";

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long partySegmentId;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected Long contId;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected String segmentProvider;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected String businessGroupName;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected String segmentTypeName;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected String segmentValue;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected String segmentCategoryType;

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected  Timestamp effectiveStartDt;
    //inside if 

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    protected  Timestamp effectiveEndDt;
    //inside if 



    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Default constructor.     
     *
     * @generated
     */
    public EObjCTCPartySegment() {
        super();
    }


    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the partySegmentId attribute. 
     *
     * @generated
     */
    @Id
    @Column(name=partySegmentIdColumn)
    @DataType(jdbcType=partySegmentIdJdbcType, precision=partySegmentIdPrecision)
    public Long getPartySegmentId (){
        return partySegmentId;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the partySegmentId attribute. 
     *
     * @param partySegmentId
     *     The new value of PartySegmentId. 
     * @generated
     */
    public void setPartySegmentId( Long partySegmentId ){
        this.partySegmentId = partySegmentId;
		
        super.setIdPK(partySegmentId);
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the contId attribute. 
     *
     * @generated
     */
    @Column(name=contIdColumn)
    @DataType(jdbcType=contIdJdbcType, precision=contIdPrecision)
    public Long getContId (){
        return contId;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the contId attribute. 
     *
     * @param contId
     *     The new value of ContId. 
     * @generated
     */
    public void setContId( Long contId ){
        this.contId = contId;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the segmentProvider attribute. 
     *
     * @generated
     */
    @Column(name=segmentProviderColumn)
    @DataType(jdbcType=segmentProviderJdbcType, precision=segmentProviderPrecision)
    public String getSegmentProvider (){
        return segmentProvider;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the segmentProvider attribute. 
     *
     * @param segmentProvider
     *     The new value of SegmentProvider. 
     * @generated
     */
    public void setSegmentProvider( String segmentProvider ){
        this.segmentProvider = segmentProvider;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the businessGroupName attribute. 
     *
     * @generated
     */
    @Column(name=businessGroupNameColumn)
    @DataType(jdbcType=businessGroupNameJdbcType, precision=businessGroupNamePrecision)
    public String getBusinessGroupName (){
        return businessGroupName;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the businessGroupName attribute. 
     *
     * @param businessGroupName
     *     The new value of BusinessGroupName. 
     * @generated
     */
    public void setBusinessGroupName( String businessGroupName ){
        this.businessGroupName = businessGroupName;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the segmentTypeName attribute. 
     *
     * @generated
     */
    @Column(name=segmentTypeNameColumn)
    @DataType(jdbcType=segmentTypeNameJdbcType, precision=segmentTypeNamePrecision)
    public String getSegmentTypeName (){
        return segmentTypeName;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the segmentTypeName attribute. 
     *
     * @param segmentTypeName
     *     The new value of SegmentTypeName. 
     * @generated
     */
    public void setSegmentTypeName( String segmentTypeName ){
        this.segmentTypeName = segmentTypeName;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the segmentValue attribute. 
     *
     * @generated
     */
    @Column(name=segmentValueColumn)
    @DataType(jdbcType=segmentValueJdbcType, precision=segmentValuePrecision)
    public String getSegmentValue (){
        return segmentValue;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the segmentValue attribute. 
     *
     * @param segmentValue
     *     The new value of SegmentValue. 
     * @generated
     */
    public void setSegmentValue( String segmentValue ){
        this.segmentValue = segmentValue;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the segmentCategoryType attribute. 
     *
     * @generated
     */
    @Column(name=segmentCategoryTypeColumn)
    @DataType(jdbcType=segmentCategoryTypeJdbcType, precision=segmentCategoryTypePrecision)
    public String getSegmentCategoryType (){
        return segmentCategoryType;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the segmentCategoryType attribute. 
     *
     * @param segmentCategoryType
     *     The new value of SegmentCategoryType. 
     * @generated
     */
    public void setSegmentCategoryType( String segmentCategoryType ){
        this.segmentCategoryType = segmentCategoryType;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the effectiveStartDt attribute. 
     *
     * @generated
     */
    @Column(name=effectiveStartDtColumn)
    @DataType(jdbcType=effectiveStartDtJdbcType)
    public Timestamp getEffectiveStartDt (){
        return effectiveStartDt;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the effectiveStartDt attribute. 
     *
     * @param effectiveStartDt
     *     The new value of EffectiveStartDt. 
     * @generated
     */
    public void setEffectiveStartDt( Timestamp effectiveStartDt ){
        this.effectiveStartDt = effectiveStartDt;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the effectiveEndDt attribute. 
     *
     * @generated
     */
    @Column(name=effectiveEndDtColumn)
    @DataType(jdbcType=effectiveEndDtJdbcType)
    public Timestamp getEffectiveEndDt (){
        return effectiveEndDt;
    }
     
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the effectiveEndDt attribute. 
     *
     * @param effectiveEndDt
     *     The new value of EffectiveEndDt. 
     * @generated
     */
    public void setEffectiveEndDt( Timestamp effectiveEndDt ){
        this.effectiveEndDt = effectiveEndDt;
		
	}
	 
    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Sets the primary key. 
     *
     * @param aUniqueId
     *     The new value of the primary key. 
     * @generated
	 */
	public void setPrimaryKey(Object aUniqueId) {
		this.setPartySegmentId((Long)aUniqueId);
	}

    /**
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     *
     * Gets the primary key.
     *
     * @generated
     */
	public Object getPrimaryKey() {
		return this.getPartySegmentId();
	}
	 
}


