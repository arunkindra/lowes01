package com.ctc.mdm.addition.entityObject;

import java.sql.SQLException;
import com.ibm.pdq.runtime.generator.BaseData;
import com.ibm.pdq.annotation.Metadata;
import java.sql.Types;
import java.sql.PreparedStatement;
import com.ibm.pdq.runtime.generator.BaseRowHandler;
import com.ibm.pdq.runtime.statement.SqlStatementType;
import com.ibm.pdq.runtime.generator.BaseParameterHandler;
import java.util.Iterator;
import com.ibm.pdq.runtime.statement.StatementDescriptor;
import com.ctc.mdm.addition.entityObject.EObjCTCPartySegment;


/**
 * <!-- begin-user-doc -->
 * 
 * <!-- end-user-doc -->
 * 
 * @generated
 */
public class EObjCTCPartySegmentDataImpl  extends BaseData implements EObjCTCPartySegmentData
{

  /**
   * @generated
   */
  public static final String generatorVersion = "3.200.75";

  /**
   * @generated
   */
  public static final String identifier = "EObjCTCPartySegmentData";

  /**
   * @generated
   */
  public static final long generationTime = 0x000001535f9cbcd7L;

  /**
   * @generated
   */
  public static final String collection = "NULLID";

  /**
   * @generated
   */
  public static final String packageVersion = null;

  /**
   * @generated
   */
  public static final boolean forceSingleBindIsolation = false;

  /**
   * @generated
   */
  public EObjCTCPartySegmentDataImpl()
  {
    super();
  } 

  /**
   * @generated
   */
  public String getGeneratorVersion()
  {
    return generatorVersion;
  }

  /**
   * @Select( sql="select party_segment_id, cont_id, segment_provider, business_group_name, segment_type_name, segment_value, segment_category_type, effective_start_dt, effective_end_dt, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from CTCPARTYSEGMENT where party_segment_id = ? " )
   * 
   * @generated
   */
  public Iterator<EObjCTCPartySegment> getEObjCTCPartySegment (Long partySegmentId)
  {
    return queryIterator (getEObjCTCPartySegmentStatementDescriptor, partySegmentId);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor getEObjCTCPartySegmentStatementDescriptor = createStatementDescriptor (
    "getEObjCTCPartySegment(Long)",
    "select party_segment_id, cont_id, segment_provider, business_group_name, segment_type_name, segment_value, segment_category_type, effective_start_dt, effective_end_dt, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID from CTCPARTYSEGMENT where party_segment_id = ? ",
    new int[] {SINGLE_ROW_PARAMETERS, MULTI_ROW_RESULT, java.sql.ResultSet.CONCUR_READ_ONLY, java.sql.ResultSet.CLOSE_CURSORS_AT_COMMIT, java.sql.ResultSet.TYPE_FORWARD_ONLY, DISALLOW_STATIC_ROWSET_CURSORS},
    SqlStatementType.QUERY,
    new String[]{"party_segment_id", "cont_id", "segment_provider", "business_group_name", "segment_type_name", "segment_value", "segment_category_type", "effective_start_dt", "effective_end_dt", "last_update_dt", "last_update_user", "last_update_tx_id"},
    new GetEObjCTCPartySegmentParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    new GetEObjCTCPartySegmentRowHandler (),
    new int[][]{ {Types.BIGINT, Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 19, 250, 250, 250, 250, 250, 0, 0, 0, 20, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    1);

  /**
   * @generated
   */
  public static class GetEObjCTCPartySegmentParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setLong (stmt, 1, Types.BIGINT, (Long)parameters[0]);
    }
  }

  /**
   * @generated
   */
  public static class GetEObjCTCPartySegmentRowHandler extends BaseRowHandler<EObjCTCPartySegment>
  {
    /**
     * @generated
     */
    public EObjCTCPartySegment handle (java.sql.ResultSet rs, EObjCTCPartySegment returnObject) throws java.sql.SQLException
    {
      returnObject = new EObjCTCPartySegment ();
      returnObject.setPartySegmentId(getLongObject (rs, 1)); 
      returnObject.setContId(getLongObject (rs, 2)); 
      returnObject.setSegmentProvider(getString (rs, 3)); 
      returnObject.setBusinessGroupName(getString (rs, 4)); 
      returnObject.setSegmentTypeName(getString (rs, 5)); 
      returnObject.setSegmentValue(getString (rs, 6)); 
      returnObject.setSegmentCategoryType(getString (rs, 7)); 
      returnObject.setEffectiveStartDt(getTimestamp (rs, 8)); 
      returnObject.setEffectiveEndDt(getTimestamp (rs, 9)); 
      returnObject.setLastUpdateDt(getTimestamp (rs, 10)); 
      returnObject.setLastUpdateUser(getString (rs, 11)); 
      returnObject.setLastUpdateTxId(getLongObject (rs, 12)); 
    
      return returnObject;
    }
  }

  /**
   * @Update( sql="insert into CTCPARTYSEGMENT (party_segment_id, cont_id, segment_provider, business_group_name, segment_type_name, segment_value, segment_category_type, effective_start_dt, effective_end_dt, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values( :partySegmentId, :contId, :segmentProvider, :businessGroupName, :segmentTypeName, :segmentValue, :segmentCategoryType, :effectiveStartDt, :effectiveEndDt, :lastUpdateDt, :lastUpdateUser, :lastUpdateTxId)" )
   * 
   * @generated
   */
  public int createEObjCTCPartySegment (EObjCTCPartySegment e)
  {
    return update (createEObjCTCPartySegmentStatementDescriptor, e);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor createEObjCTCPartySegmentStatementDescriptor = createStatementDescriptor (
    "createEObjCTCPartySegment(com.ctc.mdm.addition.entityObject.EObjCTCPartySegment)",
    "insert into CTCPARTYSEGMENT (party_segment_id, cont_id, segment_provider, business_group_name, segment_type_name, segment_value, segment_category_type, effective_start_dt, effective_end_dt, LAST_UPDATE_DT, LAST_UPDATE_USER, LAST_UPDATE_TX_ID) values(  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? ,  ? )",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.INSERT,
    null,
    new CreateEObjCTCPartySegmentParameterHandler (),
    new int[][]{{Types.BIGINT, Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT}, {19, 19, 250, 250, 250, 250, 250, 0, 0, 0, 0, 19}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    2);

  /**
   * @generated
   */
  public static class CreateEObjCTCPartySegmentParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      EObjCTCPartySegment bean0 = (EObjCTCPartySegment) parameters[0];
      setLong (stmt, 1, Types.BIGINT, (Long)bean0.getPartySegmentId());
      setLong (stmt, 2, Types.BIGINT, (Long)bean0.getContId());
      setString (stmt, 3, Types.VARCHAR, (String)bean0.getSegmentProvider());
      setString (stmt, 4, Types.VARCHAR, (String)bean0.getBusinessGroupName());
      setString (stmt, 5, Types.VARCHAR, (String)bean0.getSegmentTypeName());
      setString (stmt, 6, Types.VARCHAR, (String)bean0.getSegmentValue());
      setString (stmt, 7, Types.VARCHAR, (String)bean0.getSegmentCategoryType());
      setTimestamp (stmt, 8, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getEffectiveStartDt());
      setTimestamp (stmt, 9, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getEffectiveEndDt());
      setTimestamp (stmt, 10, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getLastUpdateDt());
      setString (stmt, 11, Types.VARCHAR, (String)bean0.getLastUpdateUser());
      setLong (stmt, 12, Types.BIGINT, (Long)bean0.getLastUpdateTxId());
    }
  }

  /**
   * @Update( sql="update CTCPARTYSEGMENT set cont_id = :contId, segment_provider = :segmentProvider, business_group_name = :businessGroupName, segment_type_name = :segmentTypeName, segment_value = :segmentValue, segment_category_type = :segmentCategoryType, effective_start_dt = :effectiveStartDt, effective_end_dt = :effectiveEndDt, LAST_UPDATE_DT = :lastUpdateDt, LAST_UPDATE_USER = :lastUpdateUser, LAST_UPDATE_TX_ID = :lastUpdateTxId where party_segment_id = :partySegmentId and LAST_UPDATE_DT = :oldLastUpdateDt" )
   * 
   * @generated
   */
  public int updateEObjCTCPartySegment (EObjCTCPartySegment e)
  {
    return update (updateEObjCTCPartySegmentStatementDescriptor, e);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor updateEObjCTCPartySegmentStatementDescriptor = createStatementDescriptor (
    "updateEObjCTCPartySegment(com.ctc.mdm.addition.entityObject.EObjCTCPartySegment)",
    "update CTCPARTYSEGMENT set cont_id =  ? , segment_provider =  ? , business_group_name =  ? , segment_type_name =  ? , segment_value =  ? , segment_category_type =  ? , effective_start_dt =  ? , effective_end_dt =  ? , LAST_UPDATE_DT =  ? , LAST_UPDATE_USER =  ? , LAST_UPDATE_TX_ID =  ?  where party_segment_id =  ?  and LAST_UPDATE_DT =  ? ",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.UPDATE,
    null,
    new UpdateEObjCTCPartySegmentParameterHandler (),
    new int[][]{{Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.TIMESTAMP, Types.TIMESTAMP, Types.TIMESTAMP, Types.VARCHAR, Types.BIGINT, Types.BIGINT, Types.TIMESTAMP}, {19, 250, 250, 250, 250, 250, 0, 0, 0, 0, 19, 19, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    3);

  /**
   * @generated
   */
  public static class UpdateEObjCTCPartySegmentParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      EObjCTCPartySegment bean0 = (EObjCTCPartySegment) parameters[0];
      setLong (stmt, 1, Types.BIGINT, (Long)bean0.getContId());
      setString (stmt, 2, Types.VARCHAR, (String)bean0.getSegmentProvider());
      setString (stmt, 3, Types.VARCHAR, (String)bean0.getBusinessGroupName());
      setString (stmt, 4, Types.VARCHAR, (String)bean0.getSegmentTypeName());
      setString (stmt, 5, Types.VARCHAR, (String)bean0.getSegmentValue());
      setString (stmt, 6, Types.VARCHAR, (String)bean0.getSegmentCategoryType());
      setTimestamp (stmt, 7, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getEffectiveStartDt());
      setTimestamp (stmt, 8, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getEffectiveEndDt());
      setTimestamp (stmt, 9, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getLastUpdateDt());
      setString (stmt, 10, Types.VARCHAR, (String)bean0.getLastUpdateUser());
      setLong (stmt, 11, Types.BIGINT, (Long)bean0.getLastUpdateTxId());
      setLong (stmt, 12, Types.BIGINT, (Long)bean0.getPartySegmentId());
      setTimestamp (stmt, 13, Types.TIMESTAMP, (java.sql.Timestamp)bean0.getOldLastUpdateDt());
    }
  }

  /**
   * @Update( sql="delete from CTCPARTYSEGMENT where party_segment_id = ?" )
   * 
   * @generated
   */
  public int deleteEObjCTCPartySegment (Long partySegmentId)
  {
    return update (deleteEObjCTCPartySegmentStatementDescriptor, partySegmentId);
  }

  /**
   * @generated
   */
  @Metadata ()
  public static final StatementDescriptor deleteEObjCTCPartySegmentStatementDescriptor = createStatementDescriptor (
    "deleteEObjCTCPartySegment(Long)",
    "delete from CTCPARTYSEGMENT where party_segment_id = ?",
    new int[] {SINGLE_ROW_PARAMETERS},
    SqlStatementType.DELETE,
    null,
    new DeleteEObjCTCPartySegmentParameterHandler (),
    new int[][]{{Types.BIGINT}, {19}, {0}, {1}},
    null,
    null,
    null,
    null,
    identifier,
    generationTime,
    collection,
    forceSingleBindIsolation,
    null,
    4);

  /**
   * @generated
   */
  public static class DeleteEObjCTCPartySegmentParameterHandler extends BaseParameterHandler 
  {
    /**
     * @generated
     */
    public void handleParameters (PreparedStatement stmt, Object... parameters) throws SQLException
    {
      setLong (stmt, 1, Types.BIGINT, (Long)parameters[0]);
    }
  }

}
