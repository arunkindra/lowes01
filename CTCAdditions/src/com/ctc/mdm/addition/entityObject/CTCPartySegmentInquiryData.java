/*
 * The following source code ("Code") may only be used in accordance with the terms
 * and conditions of the license agreement you have with IBM Corporation. The Code 
 * is provided to you on an "AS IS" basis, without warranty of any kind.  
 * SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
 * WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
 * IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
 * CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
 * LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
 * ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
 * NOT APPLY TO YOU.
 */

/*
 * IBM-MDMWB-1.0-[ff3420e28912bd61862a928f0289695e]
 */

package com.ctc.mdm.addition.entityObject;


import com.ibm.mdm.base.db.ResultQueue1;
import java.util.Iterator;
import com.ibm.mdm.base.db.EntityMapping;
import com.ibm.pdq.annotation.Select;

/**
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 *
 * @generated
 */
public interface CTCPartySegmentInquiryData {
  
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String tableAliasString = "tableAlias (" + 
                                            "CTCPARTYSEGMENT => com.ctc.mdm.addition.entityObject.EObjCTCPartySegment, " +
                                            "H_CTCPARTYSEGMENT => com.ctc.mdm.addition.entityObject.EObjCTCPartySegment" +
                                            ")";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getCTCPartySegmentSql = "SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.party_segment_id = ? ";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getCTCPartySegmentParameters =
    "EObjCTCPartySegment.PartySegmentId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getCTCPartySegmentResults =
    "EObjCTCPartySegment.PartySegmentId," +
    "EObjCTCPartySegment.ContId," +
    "EObjCTCPartySegment.SegmentProvider," +
    "EObjCTCPartySegment.BusinessGroupName," +
    "EObjCTCPartySegment.SegmentTypeName," +
    "EObjCTCPartySegment.SegmentValue," +
    "EObjCTCPartySegment.SegmentCategoryType," +
    "EObjCTCPartySegment.EffectiveStartDt," +
    "EObjCTCPartySegment.EffectiveEndDt," +
    "EObjCTCPartySegment.lastUpdateDt," +
    "EObjCTCPartySegment.lastUpdateUser," +
    "EObjCTCPartySegment.lastUpdateTxId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated NOT
   */
  
  static final String getAllActiveCTCPartySegmentSql = "SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.cont_id = ? AND (r.EFFECTIVE_END_DT is null or r.EFFECTIVE_END_DT > ?) ";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getAllInactiveCTCPartySegmentSql = "SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.cont_id = ? AND r.EFFECTIVE_END_DT < ? ";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getCTCPartySegmentHistorySql = "SELECT r.H_party_segment_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCPARTYSEGMENT r WHERE r.H_party_segment_id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getCTCPartySegmentHistoryParameters =
    "EObjCTCPartySegment.PartySegmentId," +
    "EObjCTCPartySegment.lastUpdateDt," +
    "EObjCTCPartySegment.lastUpdateDt";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getCTCPartySegmentHistoryResults =
    "EObjCTCPartySegment.historyIdPK," +
    "EObjCTCPartySegment.histActionCode," +
    "EObjCTCPartySegment.histCreatedBy," +
    "EObjCTCPartySegment.histCreateDt," +
    "EObjCTCPartySegment.histEndDt," +
    "EObjCTCPartySegment.PartySegmentId," +
    "EObjCTCPartySegment.ContId," +
    "EObjCTCPartySegment.SegmentProvider," +
    "EObjCTCPartySegment.BusinessGroupName," +
    "EObjCTCPartySegment.SegmentTypeName," +
    "EObjCTCPartySegment.SegmentValue," +
    "EObjCTCPartySegment.SegmentCategoryType," +
    "EObjCTCPartySegment.EffectiveStartDt," +
    "EObjCTCPartySegment.EffectiveEndDt," +
    "EObjCTCPartySegment.lastUpdateDt," +
    "EObjCTCPartySegment.lastUpdateUser," +
    "EObjCTCPartySegment.lastUpdateTxId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getAllCTCPartySegmentSql = "SELECT r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM CTCPARTYSEGMENT r WHERE r.cont_id = ? ";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getAllCTCPartySegmentParameters =
    "EObjCTCPartySegment.ContId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getAllCTCPartySegmentResults =
    "EObjCTCPartySegment.PartySegmentId," +
    "EObjCTCPartySegment.ContId," +
    "EObjCTCPartySegment.SegmentProvider," +
    "EObjCTCPartySegment.BusinessGroupName," +
    "EObjCTCPartySegment.SegmentTypeName," +
    "EObjCTCPartySegment.SegmentValue," +
    "EObjCTCPartySegment.SegmentCategoryType," +
    "EObjCTCPartySegment.EffectiveStartDt," +
    "EObjCTCPartySegment.EffectiveEndDt," +
    "EObjCTCPartySegment.lastUpdateDt," +
    "EObjCTCPartySegment.lastUpdateUser," +
    "EObjCTCPartySegment.lastUpdateTxId";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  static final String getAllCTCPartySegmentHistorySql = "SELECT r.H_party_segment_id hist_id_pk, r.H_ACTION_CODE h_action_code, r.H_CREATED_BY h_created_by, r.H_CREATE_DT h_create_dt, r.H_END_DT h_end_dt, r.party_segment_id party_segment_id, r.cont_id cont_id, r.segment_provider segment_provider, r.business_group_name business_group_name, r.segment_type_name segment_type_name, r.segment_value segment_value, r.segment_category_type segment_category_type, r.effective_start_dt effective_start_dt, r.effective_end_dt effective_end_dt, r.LAST_UPDATE_DT LAST_UPDATE_DT, r.LAST_UPDATE_USER LAST_UPDATE_USER, r.LAST_UPDATE_TX_ID LAST_UPDATE_TX_ID FROM H_CTCPARTYSEGMENT r WHERE r.cont_id = ?  AND (( ? BETWEEN r.H_CREATE_DT AND r.H_END_DT ) OR ( ? >= r.H_CREATE_DT AND r.H_END_DT IS NULL ))";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getAllCTCPartySegmentHistoryParameters =
    "EObjCTCPartySegment.ContId," +
    "EObjCTCPartySegment.lastUpdateDt," +
    "EObjCTCPartySegment.lastUpdateDt";
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  public static final String getAllCTCPartySegmentHistoryResults =
    "EObjCTCPartySegment.historyIdPK," +
    "EObjCTCPartySegment.histActionCode," +
    "EObjCTCPartySegment.histCreatedBy," +
    "EObjCTCPartySegment.histCreateDt," +
    "EObjCTCPartySegment.histEndDt," +
    "EObjCTCPartySegment.PartySegmentId," +
    "EObjCTCPartySegment.ContId," +
    "EObjCTCPartySegment.SegmentProvider," +
    "EObjCTCPartySegment.BusinessGroupName," +
    "EObjCTCPartySegment.SegmentTypeName," +
    "EObjCTCPartySegment.SegmentValue," +
    "EObjCTCPartySegment.SegmentCategoryType," +
    "EObjCTCPartySegment.EffectiveStartDt," +
    "EObjCTCPartySegment.EffectiveEndDt," +
    "EObjCTCPartySegment.lastUpdateDt," +
    "EObjCTCPartySegment.lastUpdateUser," +
    "EObjCTCPartySegment.lastUpdateTxId";

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getCTCPartySegmentSql, pattern=tableAliasString)
  @EntityMapping(parameters=getCTCPartySegmentParameters, results=getCTCPartySegmentResults)
  Iterator<ResultQueue1<EObjCTCPartySegment>> getCTCPartySegment(Object[] parameters);  

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getAllActiveCTCPartySegmentSql, pattern=tableAliasString)
  @EntityMapping(parameters=getCTCPartySegmentParameters, results=getCTCPartySegmentResults)
  Iterator<ResultQueue1<EObjCTCPartySegment>> getAllActiveCTCPartySegment(Object[] parameters);  

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getAllInactiveCTCPartySegmentSql, pattern=tableAliasString)
  @EntityMapping(parameters=getCTCPartySegmentParameters, results=getCTCPartySegmentResults)
  Iterator<ResultQueue1<EObjCTCPartySegment>> getAllInactiveCTCPartySegment(Object[] parameters);  

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getCTCPartySegmentHistorySql, pattern=tableAliasString)
  @EntityMapping(parameters=getCTCPartySegmentHistoryParameters, results=getCTCPartySegmentHistoryResults)
  Iterator<ResultQueue1<EObjCTCPartySegment>> getCTCPartySegmentHistory(Object[] parameters);  


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getAllCTCPartySegmentSql, pattern=tableAliasString)
  @EntityMapping(parameters=getAllCTCPartySegmentParameters, results=getAllCTCPartySegmentResults)
  Iterator<ResultQueue1<EObjCTCPartySegment>> getAllCTCPartySegment(Object[] parameters);  


  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   *
   * @generated
   */
  @Select(sql=getAllCTCPartySegmentHistorySql, pattern=tableAliasString)
  @EntityMapping(parameters=getAllCTCPartySegmentHistoryParameters, results=getAllCTCPartySegmentHistoryResults)
  Iterator<ResultQueue1<EObjCTCPartySegment>> getAllCTCPartySegmentHistory(Object[] parameters);  


}


