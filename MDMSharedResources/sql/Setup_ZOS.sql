
-- @SqlSnippetPriority 100
-- @SqlModuleOrdering 2

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- MDM_TODO2: CDKWB0055I Replace <DBNAME> with the database name.
-- MDM_TODO2: CDKWB0058I Replace <TABLESPACENAME> with the table space name.
-- MDM_TODO2: CDKWB0056I Replace <H_DBNAME> with the history database name.
-- MDM_TODO2: CDKWB0057I Replace <H_TABLESPACENAME> with the history table space name.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_ZOS.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_ZOS.sql
--#SET TERMINATOR ;


CREATE TABLE CTCMDM.CDCTCLOBSOURCETP (  
	lang_tp_cd DECIMAL(19)  NOT NULL  , 
	Lob_Source_Tp_Cd DECIMAL(19)  NOT NULL  , 
	Name VARCHAR(120)  NOT NULL  , 
	Admin_Sys_Tp_Cd DECIMAL(19)  NOT NULL  , 
	Lob_Tp_Cd DECIMAL(19)  NOT NULL  , 
	description VARCHAR(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  NOT NULL   DEFAULT, 
	last_update_user VARCHAR(20)   , 
	CONSTRAINT P_CDCTCLOBSOURCETP PRIMARY KEY (
	 lang_tp_cd, 
	 Lob_Source_Tp_Cd
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_CDCTCLOBSOURCETP
  ON CTCMDM.CDCTCLOBSOURCETP (
	 lang_tp_cd, 
	 Lob_Source_Tp_Cd
  );


CREATE TABLE CTCMDM.CDCTCADMINSYSPRIORITYTP (  
	lang_tp_cd DECIMAL(19)  NOT NULL  , 
	Admin_Sys_Tp_Cd DECIMAL(19)  NOT NULL  , 
	name VARCHAR(120)  NOT NULL  , 
	Priority_Tp_Cd DECIMAL(19)   , 
	description VARCHAR(250)   , 
	expiry_dt TIMESTAMP   , 
	last_update_dt TIMESTAMP  NOT NULL   DEFAULT, 
	last_update_user VARCHAR(20)   , 
	CONSTRAINT P_CDCTCADMINSYSPRIORITYTP PRIMARY KEY (
	 lang_tp_cd, 
	 Admin_Sys_Tp_Cd
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_CDCTCADMINSYSPRIORITYTP
  ON CTCMDM.CDCTCADMINSYSPRIORITYTP (
	 lang_tp_cd, 
	 Admin_Sys_Tp_Cd
  );


CREATE TABLE CTCMDM.H_CDCTCLOBSOURCETP (
	h_lang_tp_cd DECIMAL(19)  NOT NULL  ,
	h_Lob_Source_Tp_Cd DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	lang_tp_cd DECIMAL(19)  NOT NULL   ,
	Lob_Source_Tp_Cd DECIMAL(19)  NOT NULL   ,
	Name VARCHAR(120)  NOT NULL   ,
	Admin_Sys_Tp_Cd DECIMAL(19)  NOT NULL   ,
	Lob_Tp_Cd DECIMAL(19)  NOT NULL   ,
	description VARCHAR(250)    ,
	expiry_dt TIMESTAMP    ,
	last_update_dt TIMESTAMP  NOT NULL   DEFAULT ,
	last_update_user VARCHAR(20)    ,
	CONSTRAINT P_H_CDCTCLOBSOURCETP PRIMARY KEY (
	 h_lang_tp_cd,
	 h_Lob_Source_Tp_Cd,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_CDCTCLOBSOURCETP
  ON CTCMDM.H_CDCTCLOBSOURCETP
  (
	 h_lang_tp_cd,
	 h_Lob_Source_Tp_Cd,
	 h_create_dt
  );

CREATE TABLE CTCMDM.H_CDCTCADMINSYSPRIORITYTP (
	h_lang_tp_cd DECIMAL(19)  NOT NULL  ,
	h_Admin_Sys_Tp_Cd DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	lang_tp_cd DECIMAL(19)  NOT NULL   ,
	Admin_Sys_Tp_Cd DECIMAL(19)  NOT NULL   ,
	name VARCHAR(120)  NOT NULL   ,
	Priority_Tp_Cd DECIMAL(19)    ,
	description VARCHAR(250)    ,
	expiry_dt TIMESTAMP    ,
	last_update_dt TIMESTAMP  NOT NULL   DEFAULT ,
	last_update_user VARCHAR(20)    ,
	CONSTRAINT P_H_CDCTCADMINSYSPRIORITYTP PRIMARY KEY (
	 h_lang_tp_cd,
	 h_Admin_Sys_Tp_Cd,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_CDCTCADMINSYSPRIORITYTP
  ON CTCMDM.H_CDCTCADMINSYSPRIORITYTP
  (
	 h_lang_tp_cd,
	 h_Admin_Sys_Tp_Cd,
	 h_create_dt
  );



-- @SqlSnippetPriority 100
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- MDM_TODO2: CDKWB0055I Replace <DBNAME> with the database name.
-- MDM_TODO2: CDKWB0058I Replace <TABLESPACENAME> with the table space name.
-- MDM_TODO2: CDKWB0056I Replace <H_DBNAME> with the history database name.
-- MDM_TODO2: CDKWB0057I Replace <H_TABLESPACENAME> with the history table space name.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCAdditions_SETUP_ZOS.sql
-- 			db2 -vf CTCAdditions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCAdditions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCAdditions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCAdditions_CODETABLES_ZOS.sql
--#SET TERMINATOR ;


CREATE TABLE CTCMDM.CTCDATASOURCE (  
	data_source_id DECIMAL(19)  NOT NULL  , 
	entity_name VARCHAR(255)   , 
	instance_pk DECIMAL(19)  NOT NULL  , 
	attribute_name VARCHAR(250)   , 
	start_dt TIMESTAMP  NOT NULL  , 
	end_dt TIMESTAMP   , 
	admin_sys_tp_cd DECIMAL(19)  NOT NULL  , 
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT, 
	LAST_UPDATE_TX_ID DECIMAL(19)   , 
	LAST_UPDATE_USER VARCHAR(20)   , 
	CONSTRAINT P_CTCDATASOURCE PRIMARY KEY (
	 data_source_id
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_CTCDATASOURCE
  ON CTCMDM.CTCDATASOURCE (
	 data_source_id
  );


CREATE TABLE CTCMDM.CTCPARTYSEGMENT (  
	party_segment_id DECIMAL(19)  NOT NULL  , 
	cont_id DECIMAL(19)  NOT NULL  , 
	segment_provider VARCHAR(250)  NOT NULL  , 
	business_group_name VARCHAR(250)  NOT NULL  , 
	segment_type_name VARCHAR(250)  NOT NULL  , 
	segment_value VARCHAR(250)   , 
	segment_category_type VARCHAR(250)   , 
	effective_start_dt TIMESTAMP  NOT NULL  , 
	effective_end_dt TIMESTAMP   , 
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT, 
	LAST_UPDATE_TX_ID DECIMAL(19)   , 
	LAST_UPDATE_USER VARCHAR(20)   , 
	CONSTRAINT P_CTCPARTYSEGMENT PRIMARY KEY (
	 party_segment_id
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_CTCPARTYSEGMENT
  ON CTCMDM.CTCPARTYSEGMENT (
	 party_segment_id
  );


CREATE TABLE CTCMDM.H_CTCDATASOURCE (
	h_data_source_id DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	data_source_id DECIMAL(19)  NOT NULL   ,
	entity_name VARCHAR(255)    ,
	instance_pk DECIMAL(19)  NOT NULL   ,
	attribute_name VARCHAR(250)    ,
	start_dt TIMESTAMP  NOT NULL   ,
	end_dt TIMESTAMP    ,
	admin_sys_tp_cd DECIMAL(19)  NOT NULL   ,
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT ,
	LAST_UPDATE_TX_ID DECIMAL(19)    ,
	LAST_UPDATE_USER VARCHAR(20)    ,
	CONSTRAINT P_H_CTCDATASOURCE PRIMARY KEY (
	 h_data_source_id,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_CTCDATASOURCE
  ON CTCMDM.H_CTCDATASOURCE
  (
	 h_data_source_id,
	 h_create_dt
  );

CREATE TABLE CTCMDM.H_CTCPARTYSEGMENT (
	h_party_segment_id DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	party_segment_id DECIMAL(19)  NOT NULL   ,
	cont_id DECIMAL(19)  NOT NULL   ,
	segment_provider VARCHAR(250)  NOT NULL   ,
	business_group_name VARCHAR(250)  NOT NULL   ,
	segment_type_name VARCHAR(250)  NOT NULL   ,
	segment_value VARCHAR(250)    ,
	segment_category_type VARCHAR(250)    ,
	effective_start_dt TIMESTAMP  NOT NULL   ,
	effective_end_dt TIMESTAMP    ,
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT ,
	LAST_UPDATE_TX_ID DECIMAL(19)    ,
	LAST_UPDATE_USER VARCHAR(20)    ,
	CONSTRAINT P_H_CTCPARTYSEGMENT PRIMARY KEY (
	 h_party_segment_id,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_CTCPARTYSEGMENT
  ON CTCMDM.H_CTCPARTYSEGMENT
  (
	 h_party_segment_id,
	 h_create_dt
  );



-- @SqlSnippetPriority 200
-- @SqlModuleOrdering 2

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_ZOS.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_ZOS.sql--#SET TERMINATOR @



-- @SqlSnippetPriority 200
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCAdditions_SETUP_ZOS.sql
-- 			db2 -vf CTCAdditions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCAdditions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCAdditions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCAdditions_CODETABLES_ZOS.sql--#SET TERMINATOR @



-- @SqlSnippetPriority 300
-- @SqlModuleOrdering 2

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_ZOS.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



-- @SqlSnippetPriority 300
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCAdditions_SETUP_ZOS.sql
-- 			db2 -vf CTCAdditions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCAdditions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCAdditions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCAdditions_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCommon_SETUP_ZOS.sql
-- 			db2 -vf CTCCommon_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCommon_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCommon_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCommon_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCommon_CODETABLES_ZOS.sql	
--#SET TERMINATOR ;


-- For locale: 100 / default

-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCComposites_SETUP_ZOS.sql
-- 			db2 -vf CTCComposites_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCComposites_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCComposites_ERRORS_100_DB2.sql
-- 			db2 -vf CTCComposites_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCComposites_CODETABLES_ZOS.sql	
--#SET TERMINATOR ;


-- For locale: 100 / default

-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCExtensions_SETUP_ZOS.sql
-- 			db2 -vf CTCExtensions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCExtensions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCExtensions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCExtensions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCExtensions_CODETABLES_ZOS.sql	
--#SET TERMINATOR ;


-- For locale: 100 / default

-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCExternalRules_SETUP_ZOS.sql
-- 			db2 -vf CTCExternalRules_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCExternalRules_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCExternalRules_ERRORS_100_DB2.sql
-- 			db2 -vf CTCExternalRules_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCExternalRules_CODETABLES_ZOS.sql	
--#SET TERMINATOR ;


-- For locale: 100 / default

-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 2

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_ZOS.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_ZOS.sql	
--#SET TERMINATOR ;


-- For locale: 100 / default
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020050, 'The following is required: LobSourceTpCd', 'The following is required: LobSourceTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020058, 'The following is required: Name', 'The following is required: Name', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020065, 'The following is not correct: AdminSystemTpCd', 'AdminSystemTpCd is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020069, 'The following is required: AdminSystemTpCd', 'The following is required: AdminSystemTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020078, 'The following is not correct: LobTpCd', 'LobTpCd is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020082, 'The following is required: LobTpCd', 'The following is required: LobTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020129, 'The following is required: AdminSysTpCd', 'The following is required: AdminSysTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020137, 'The following is required: name', 'The following is required: name', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020149, 'The following is required: PriorityTpCd', 'The following is required: PriorityTpCd', CURRENT_TIMESTAMP);

-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCAdditions_SETUP_ZOS.sql
-- 			db2 -vf CTCAdditions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCAdditions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCAdditions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCAdditions_CODETABLES_ZOS.sql	
--#SET TERMINATOR ;


-- For locale: 100 / default
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010039, 'The following is required: DataSourceID', 'The following is required: DataSourceID', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010043, 'The before image for the following is empty: CTCDataSource.', 'The before image of CTCDataSource is empty.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010060, 'Read of the following record failed: CTCDataSource.', 'An attempt to read the CTCDataSource failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010064, 'The following is incorrect: id is null.', 'The id is null.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010068, 'The format of the following is not correct: inquireAsOfDate.', 'The format of inquireAsOfDate is not correct.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010083, 'Insert of the following failed: CTCDataSource.', 'CTCDataSource insert failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010087, 'Duplicate primary key already exists.', 'Duplicate primary key already exists.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010100, 'Update of the following failed: CTCDataSource.', 'CTCDataSource update failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010114, 'The following is required: InstancePK', 'The following is required: InstancePK', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010136, 'The following is not correct: StartDate', 'StartDate is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010148, 'The following is not correct: EndDate', 'EndDate is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010190, 'The following is required: PartySegmentId', 'The following is required: PartySegmentId', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010194, 'The before image for the following is empty: CTCPartySegment.', 'The before image of CTCPartySegment is empty.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010211, 'Read of the following record failed: CTCPartySegment.', 'An attempt to read the CTCPartySegment failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010215, 'The following is incorrect: id is null.', 'The id is null.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010219, 'The format of the following is not correct: inquireAsOfDate.', 'The format of inquireAsOfDate is not correct.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010234, 'Insert of the following failed: CTCPartySegment.', 'CTCPartySegment insert failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010238, 'Duplicate primary key already exists.', 'Duplicate primary key already exists.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010251, 'Update of the following failed: CTCPartySegment.', 'CTCPartySegment update failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010257, 'The following is required: ContId', 'The following is required: ContId', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010265, 'The following is required: SegmentProvider', 'The following is required: SegmentProvider', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010273, 'The following is required: BusinessGroupName', 'The following is required: BusinessGroupName', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010281, 'The following is required: SegmentTypeName', 'The following is required: SegmentTypeName', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010305, 'The following is required: EffectiveStartDt', 'The following is required: EffectiveStartDt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010311, 'The following is not correct: EffectiveStartDt', 'EffectiveStartDt is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010323, 'The following is not correct: EffectiveEndDt', 'EffectiveEndDt is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010330, 'The following is required: StartDate', 'The following is required: StartDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010337, 'The following is not correct: AdminSystem', 'AdminSystem is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010341, 'The following is required: AdminSystem', 'The following is required: AdminSystem', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010357, 'The following transaction failed: getAllCTCDataSourcesByEntity.', 'The transaction getAllCTCDataSourcesByEntity failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010376, 'Read of the following record failed: CTCPartySegment.', 'An attempt to read the CTCPartySegment failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010380, 'The following is incorrect: id is null.', 'The id is null.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010384, 'The format of the following is not correct: inquireAsOfDate.', 'The format of inquireAsOfDate is not correct.', CURRENT_TIMESTAMP);

-- @SqlSnippetPriority 400
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- MDM_TODO: CDKWB0044I Review CDDWLCOLUMNTP LOCALE_SENSITIVE settings
-- MDM_TODO: CDKWB0043I You may need to add entries to the INTERNALTXNKEY table for any new transactions


-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCommon_SETUP_ZOS.sql
-- 			db2 -vf CTCCommon_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCommon_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCommon_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCommon_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCommon_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



----------------------------------------------
-- Component type
----------------------------------------------


----------------------------------------------
-- Error messages
----------------------------------------------


---------------------------------------------
-- Metadata setup
---------------------------------------------


----------------------------------------------
-- V_GROUP
----------------------------------------------


CREATE SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ AS BIGINT START WITH 1060000 INCREMENT BY 1 MINVALUE 1060000 MAXVALUE 1069999 CACHE 10;


DROP SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ RESTRICT;

----------------------------------------------
-- V_ELEMENTATTRIBUTE
----------------------------------------------


CREATE SEQUENCE DB2admin.V_ELATTR_ID_SEQ AS BIGINT START WITH 1060000 INCREMENT BY 1 MINVALUE 1060000 MAXVALUE 1069999 CACHE 10;

DROP SEQUENCE DB2admin.V_ELATTR_ID_SEQ RESTRICT;

----------------------------------------------
-- Transactions
----------------------------------------------


----------------------------------------------
-- CDBUSINESSTXTP
----------------------------------------------


----------------------------------------------
-- CDINTERNALTXNTP
----------------------------------------------




----------------------------------------------
-- BUSINTERNALTXN
----------------------------------------------



----------------------------------------------
-- BUSINESSTXREQRESP
----------------------------------------------



----------------------------------------------
-- INTERNALTXREQRESP
----------------------------------------------


----------------------------------------------
-- GROUPTXMAP
----------------------------------------------



----------------------------------------------
-- TAIL setup
----------------------------------------------




-- @SqlSnippetPriority 400
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- MDM_TODO: CDKWB0044I Review CDDWLCOLUMNTP LOCALE_SENSITIVE settings
-- MDM_TODO: CDKWB0043I You may need to add entries to the INTERNALTXNKEY table for any new transactions


-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCComposites_SETUP_ZOS.sql
-- 			db2 -vf CTCComposites_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCComposites_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCComposites_ERRORS_100_DB2.sql
-- 			db2 -vf CTCComposites_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCComposites_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



----------------------------------------------
-- Component type
----------------------------------------------


----------------------------------------------
-- Error messages
----------------------------------------------


---------------------------------------------
-- Metadata setup
---------------------------------------------


----------------------------------------------
-- V_GROUP
----------------------------------------------


CREATE SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ AS BIGINT START WITH 1080000 INCREMENT BY 1 MINVALUE 1080000 MAXVALUE 1089999 CACHE 10;


DROP SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ RESTRICT;

----------------------------------------------
-- V_ELEMENTATTRIBUTE
----------------------------------------------


CREATE SEQUENCE DB2admin.V_ELATTR_ID_SEQ AS BIGINT START WITH 1080000 INCREMENT BY 1 MINVALUE 1080000 MAXVALUE 1089999 CACHE 10;

DROP SEQUENCE DB2admin.V_ELATTR_ID_SEQ RESTRICT;

----------------------------------------------
-- Transactions
----------------------------------------------


----------------------------------------------
-- CDBUSINESSTXTP
----------------------------------------------


----------------------------------------------
-- CDINTERNALTXNTP
----------------------------------------------




----------------------------------------------
-- BUSINTERNALTXN
----------------------------------------------



----------------------------------------------
-- BUSINESSTXREQRESP
----------------------------------------------



----------------------------------------------
-- INTERNALTXREQRESP
----------------------------------------------


----------------------------------------------
-- GROUPTXMAP
----------------------------------------------



----------------------------------------------
-- TAIL setup
----------------------------------------------




-- @SqlSnippetPriority 400
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- MDM_TODO: CDKWB0044I Review CDDWLCOLUMNTP LOCALE_SENSITIVE settings
-- MDM_TODO: CDKWB0043I You may need to add entries to the INTERNALTXNKEY table for any new transactions


-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCExtensions_SETUP_ZOS.sql
-- 			db2 -vf CTCExtensions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCExtensions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCExtensions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCExtensions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCExtensions_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



----------------------------------------------
-- Component type
----------------------------------------------


----------------------------------------------
-- Error messages
----------------------------------------------


---------------------------------------------
-- Metadata setup
---------------------------------------------


----------------------------------------------
-- V_GROUP
----------------------------------------------


CREATE SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ AS BIGINT START WITH 1000000 INCREMENT BY 1 MINVALUE 1000000 MAXVALUE 1009999 CACHE 10;


DROP SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ RESTRICT;

----------------------------------------------
-- V_ELEMENTATTRIBUTE
----------------------------------------------


CREATE SEQUENCE DB2admin.V_ELATTR_ID_SEQ AS BIGINT START WITH 1000000 INCREMENT BY 1 MINVALUE 1000000 MAXVALUE 1009999 CACHE 10;

DROP SEQUENCE DB2admin.V_ELATTR_ID_SEQ RESTRICT;

----------------------------------------------
-- Transactions
----------------------------------------------


----------------------------------------------
-- CDBUSINESSTXTP
----------------------------------------------


----------------------------------------------
-- CDINTERNALTXNTP
----------------------------------------------




----------------------------------------------
-- BUSINTERNALTXN
----------------------------------------------



----------------------------------------------
-- BUSINESSTXREQRESP
----------------------------------------------



----------------------------------------------
-- INTERNALTXREQRESP
----------------------------------------------


----------------------------------------------
-- GROUPTXMAP
----------------------------------------------



----------------------------------------------
-- TAIL setup
----------------------------------------------




-- @SqlSnippetPriority 400
-- @SqlModuleOrdering 0

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- MDM_TODO: CDKWB0044I Review CDDWLCOLUMNTP LOCALE_SENSITIVE settings
-- MDM_TODO: CDKWB0043I You may need to add entries to the INTERNALTXNKEY table for any new transactions


-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCExternalRules_SETUP_ZOS.sql
-- 			db2 -vf CTCExternalRules_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCExternalRules_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCExternalRules_ERRORS_100_DB2.sql
-- 			db2 -vf CTCExternalRules_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCExternalRules_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



----------------------------------------------
-- Component type
----------------------------------------------


----------------------------------------------
-- Error messages
----------------------------------------------


---------------------------------------------
-- Metadata setup
---------------------------------------------


----------------------------------------------
-- V_GROUP
----------------------------------------------


CREATE SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ AS BIGINT START WITH 1070000 INCREMENT BY 1 MINVALUE 1070000 MAXVALUE 1079999 CACHE 10;


DROP SEQUENCE DB2admin.GROUPDWLTABLE_ID_SEQ RESTRICT;

----------------------------------------------
-- V_ELEMENTATTRIBUTE
----------------------------------------------


CREATE SEQUENCE DB2admin.V_ELATTR_ID_SEQ AS BIGINT START WITH 1070000 INCREMENT BY 1 MINVALUE 1070000 MAXVALUE 1079999 CACHE 10;

DROP SEQUENCE DB2admin.V_ELATTR_ID_SEQ RESTRICT;

----------------------------------------------
-- Transactions
----------------------------------------------


----------------------------------------------
-- CDBUSINESSTXTP
----------------------------------------------


----------------------------------------------
-- CDINTERNALTXNTP
----------------------------------------------




----------------------------------------------
-- BUSINTERNALTXN
----------------------------------------------



----------------------------------------------
-- BUSINESSTXREQRESP
----------------------------------------------



----------------------------------------------
-- INTERNALTXREQRESP
----------------------------------------------


----------------------------------------------
-- GROUPTXMAP
----------------------------------------------



----------------------------------------------
-- TAIL setup
----------------------------------------------




-- @SqlSnippetPriority 400
-- @SqlModuleOrdering 2

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- MDM_TODO: CDKWB0044I Review CDDWLCOLUMNTP LOCALE_SENSITIVE settings
-- MDM_TODO: CDKWB0043I You may need to add entries to the INTERNALTXNKEY table for any new transactions


-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_ZOS.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



----------------------------------------------
-- Component type
----------------------------------------------

INSERT INTO CTCMDM.COMPONENTTYPE (COMPONENT_TYPE_ID, DWL_PROD_TP_CD, COMPON_TYPE_VALUE, COMPON_LONG_DESC, LAST_UPDATE_DT, COMPONENT_CLASS ) 
   VALUES ( 1020010, 1, 'CTCLobSourceTypeBObj', null, CURRENT_TIMESTAMP, null);
INSERT INTO CTCMDM.COMPONENTTYPE (COMPONENT_TYPE_ID, DWL_PROD_TP_CD, COMPON_TYPE_VALUE, COMPON_LONG_DESC, LAST_UPDATE_DT, COMPONENT_CLASS ) 
   VALUES ( 1020089, 1, 'CTCAdminSysPriorityTypeBObj', null, CURRENT_TIMESTAMP, null);

----------------------------------------------
-- Error messages
----------------------------------------------

   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020051, 1020010, 'FVERR', 1020050, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020059, 1020010, 'FVERR', 1020058, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020066, 1020010, 'FVERR', 1020065, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020070, 1020010, 'FVERR', 1020069, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020079, 1020010, 'FVERR', 1020078, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020083, 1020010, 'FVERR', 1020082, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020130, 1020089, 'FVERR', 1020129, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020138, 1020089, 'FVERR', 1020137, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1020150, 1020089, 'FVERR', 1020149, CURRENT_TIMESTAMP);

---------------------------------------------
-- Metadata setup
---------------------------------------------


----------------------------------------------
-- Add record for the database table for CTCLobSource
----------------------------------------------

INSERT INTO CTCMDM.CDDWLTABLETP (DWLTABLE_TP_CD, TABLE_NAME, DESCRIPTION, LAST_UPDATE_DT, EXPIRY_DT, CODE_TYPE_IND, DWL_PROD_TP_CD)
VALUES (1020021, 'CDCTCLOBSOURCETP', null, CURRENT_TIMESTAMP, null, 'Y', 1);

INSERT INTO CTCMDM.CDDWLTABLETP (DWLTABLE_TP_CD, TABLE_NAME, DESCRIPTION, LAST_UPDATE_DT, EXPIRY_DT, CODE_TYPE_IND, DWL_PROD_TP_CD)
VALUES (1020022, 'H_CDCTCLOBSOURCETP', null, CURRENT_TIMESTAMP, null, 'Y', 1);

----------------------------------------------
-- Add records for the table columns for CTCLobSource
----------------------------------------------


INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020043, 1020021, 'lang_tp_cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020048, 1020021, 'Lob_Source_Tp_Cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020056, 1020021, 'Name', null, CURRENT_TIMESTAMP, null, 'Y');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020073, 1020021, 'Admin_Sys_Tp_Cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020086, 1020021, 'Lob_Tp_Cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020038, 1020021, 'description', null, CURRENT_TIMESTAMP, null, 'Y');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020035, 1020021, 'expiry_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020023, 1020021, 'last_update_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020025, 1020021, 'last_update_user', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020042, 1020022, 'h_lang_tp_cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020049, 1020022, 'Lob_Source_Tp_Cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020057, 1020022, 'Name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020074, 1020022, 'Admin_Sys_Tp_Cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020087, 1020022, 'Lob_Tp_Cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020037, 1020022, 'description', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020041, 1020022, 'expiry_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020027, 1020022, 'last_update_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020029, 1020022, 'last_update_user', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020031, 1020022, 'h_action_code', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020026, 1020022, 'h_create_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020024, 1020022, 'h_created_by', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020030, 1020022, 'h_end_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020034, 1020022, 'H_Lob_Source_Tp_Cd', null, CURRENT_TIMESTAMP, null, 'N');

----------------------------------------------
-- Add record for the database table for CTCAdminSysPriority
----------------------------------------------

INSERT INTO CTCMDM.CDDWLTABLETP (DWLTABLE_TP_CD, TABLE_NAME, DESCRIPTION, LAST_UPDATE_DT, EXPIRY_DT, CODE_TYPE_IND, DWL_PROD_TP_CD)
VALUES (1020100, 'CDCTCADMINSYSPRIORITYTP', null, CURRENT_TIMESTAMP, null, 'Y', 1);

INSERT INTO CTCMDM.CDDWLTABLETP (DWLTABLE_TP_CD, TABLE_NAME, DESCRIPTION, LAST_UPDATE_DT, EXPIRY_DT, CODE_TYPE_IND, DWL_PROD_TP_CD)
VALUES (1020101, 'H_CDCTCADMINSYSPRIORITYTP', null, CURRENT_TIMESTAMP, null, 'Y', 1);

----------------------------------------------
-- Add records for the table columns for CTCAdminSysPriority
----------------------------------------------


INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020122, 1020100, 'lang_tp_cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020127, 1020100, 'Admin_Sys_Tp_Cd', null, CURRENT_TIMESTAMP, 'Identifies the type code for the specific source system', 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020135, 1020100, 'name', null, CURRENT_TIMESTAMP, 'A short name for the AdminSystem', 'Y');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020147, 1020100, 'Priority_Tp_Cd', null, CURRENT_TIMESTAMP, 'Identifies the priority of the AdminSystem', 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020117, 1020100, 'description', null, CURRENT_TIMESTAMP, null, 'Y');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020114, 1020100, 'expiry_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020102, 1020100, 'last_update_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020104, 1020100, 'last_update_user', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020121, 1020101, 'h_lang_tp_cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020128, 1020101, 'Admin_Sys_Tp_Cd', null, CURRENT_TIMESTAMP, 'Identifies the type code for the specific source system', 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020136, 1020101, 'name', null, CURRENT_TIMESTAMP, 'A short name for the AdminSystem', 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020148, 1020101, 'Priority_Tp_Cd', null, CURRENT_TIMESTAMP, 'Identifies the priority of the AdminSystem', 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020116, 1020101, 'description', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020120, 1020101, 'expiry_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020106, 1020101, 'last_update_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020108, 1020101, 'last_update_user', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020110, 1020101, 'h_action_code', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020105, 1020101, 'h_create_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020103, 1020101, 'h_created_by', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020109, 1020101, 'h_end_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1020113, 1020101, 'H_Admin_Sys_Tp_Cd', null, CURRENT_TIMESTAMP, null, 'N');

----------------------------------------------
-- V_GROUP
----------------------------------------------

INSERT INTO CTCMDM.V_GROUP (GROUP_NAME, APPLICATION, OBJECT_NAME, LAST_UPDATE_DT, SORTBY, ALIAS_NAME, PARENT_GRP_NAME ) 
  VALUES ('CTCLobSourceTypeBObj', 'TCRM', 'com.ctc.mdm.codetable.obj.CTCLobSourceTypeBObj', CURRENT_TIMESTAMP, 'LAST_UPDATE_DT', 'CDCTCLOBSOURCETP', null);
INSERT INTO CTCMDM.V_GROUP (GROUP_NAME, APPLICATION, OBJECT_NAME, LAST_UPDATE_DT, SORTBY, ALIAS_NAME) 
  VALUES ('CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'com.ctc.mdm.codetable.obj.CTCLobSourceTypeBObj', CURRENT_TIMESTAMP, 'LAST_UPDATE_DT', 'CDCTCLOBSOURCETP');
INSERT INTO CTCMDM.V_GROUP (GROUP_NAME, APPLICATION, OBJECT_NAME, LAST_UPDATE_DT, SORTBY, ALIAS_NAME, PARENT_GRP_NAME ) 
  VALUES ('CTCAdminSysPriorityTypeBObj', 'TCRM', 'com.ctc.mdm.codetable.obj.CTCAdminSysPriorityTypeBObj', CURRENT_TIMESTAMP, 'LAST_UPDATE_DT', 'CDCTCADMINSYSPRIORITYTP', null);
INSERT INTO CTCMDM.V_GROUP (GROUP_NAME, APPLICATION, OBJECT_NAME, LAST_UPDATE_DT, SORTBY, ALIAS_NAME) 
  VALUES ('CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'com.ctc.mdm.codetable.obj.CTCAdminSysPriorityTypeBObj', CURRENT_TIMESTAMP, 'LAST_UPDATE_DT', 'CDCTCADMINSYSPRIORITYTP');

CREATE SEQUENCE CTCMDM.GROUPDWLTABLE_ID_SEQ AS BIGINT START WITH 1020000 INCREMENT BY 1 MINVALUE 1020000 MAXVALUE 1029999 CACHE 10;

----------------------------------------------
-- Connect V_GROUP record with CDDWLTABLETP
----------------------------------------------

INSERT INTO CTCMDM.GROUPDWLTABLE (GROUP_DWLTABLE_ID, APPLICATION, GROUP_NAME, DWLTABLE_TP_CD, LAST_UPDATE_USER, LAST_UPDATE_DT)
VALUES (NEXTVAL FOR CTCMDM.GROUPDWLTABLE_ID_SEQ, 'TCRM', 'CTCLobSourceTypeBObj', 1020021, 'cusadmin', CURRENT_TIMESTAMP);

----------------------------------------------
-- Add attributes for entity type CTCLobSource to V_ELEMENT table
----------------------------------------------

INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lang_tp_cd', 'CTCLobSourceTypeBObj', 'TCRM', 'lang_tp_cd', CURRENT_TIMESTAMP, 10, 'TCRM',  null, 1020043, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lang_tp_value', 'CTCLobSourceTypeBObj', 'TCRM', 'lang_tp_value', CURRENT_TIMESTAMP, 20, 'TCRM',  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('LobSourceTpCd', 'CTCLobSourceTypeBObj', 'TCRM', 'LobSourceTpCd', CURRENT_TIMESTAMP, 30, 'TCRM',  null, 1020048, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('Name', 'CTCLobSourceTypeBObj', 'TCRM', 'Name', CURRENT_TIMESTAMP, 40, 'TCRM',  null, 1020056, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('adminSystemTpCdType', 'CTCLobSourceTypeBObj', 'TCRM', 'adminSystemTpCdType', CURRENT_TIMESTAMP, 50, 'TCRM',  null, 1020073, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('adminSystemTpCdValue', 'CTCLobSourceTypeBObj', 'TCRM', 'adminSystemTpCdValue', CURRENT_TIMESTAMP, 60, 'TCRM',  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lobTpCdType', 'CTCLobSourceTypeBObj', 'TCRM', 'lobTpCdType', CURRENT_TIMESTAMP, 70, 'TCRM',  null, 1020086, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lobTpCdValue', 'CTCLobSourceTypeBObj', 'TCRM', 'lobTpCdValue', CURRENT_TIMESTAMP, 80, 'TCRM',  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('description', 'CTCLobSourceTypeBObj', 'TCRM', 'description', CURRENT_TIMESTAMP, 90, 'TCRM',  null, 1020038, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('expiry_dt', 'CTCLobSourceTypeBObj', 'TCRM', 'expiry_dt', CURRENT_TIMESTAMP, 100, 'TCRM',  null, 1020035, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('last_update_dt', 'CTCLobSourceTypeBObj', 'TCRM', 'last_update_dt', CURRENT_TIMESTAMP, 110, 'TCRM',  null, 1020023, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('last_update_user', 'CTCLobSourceTypeBObj', 'TCRM', 'last_update_user', CURRENT_TIMESTAMP, 120, 'TCRM',  null, 1020025, 1);
----------------------------------------------
-- Connect V_GROUP record with CDDWLTABLETP
----------------------------------------------

INSERT INTO CTCMDM.GROUPDWLTABLE (GROUP_DWLTABLE_ID, APPLICATION, GROUP_NAME, DWLTABLE_TP_CD, LAST_UPDATE_USER, LAST_UPDATE_DT)
VALUES (NEXTVAL FOR CTCMDM.GROUPDWLTABLE_ID_SEQ, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 1020021, 'cusadmin', CURRENT_TIMESTAMP);


----------------------------------------------
-- DWLADMINSERVICE: Add attributes for entity type CTCLobSource to V_ELEMENT table
----------------------------------------------

INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lang_tp_cd', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'lang_tp_cd', CURRENT_TIMESTAMP, 10, 'DWLADMINSERVICE',  null, 1020043, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lang_tp_value', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'lang_tp_value', CURRENT_TIMESTAMP, 20, 'DWLADMINSERVICE',  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('LobSourceTpCd', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'LobSourceTpCd', CURRENT_TIMESTAMP, 30, 'DWLADMINSERVICE',  null, 1020048, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('Name', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'Name', CURRENT_TIMESTAMP, 40, 'DWLADMINSERVICE',  null, 1020056, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('adminSystemTpCdType', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'adminSystemTpCdType', CURRENT_TIMESTAMP, 50, 'DWLADMINSERVICE',  null, 1020073, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('adminSystemTpCdValue', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'adminSystemTpCdValue', CURRENT_TIMESTAMP, 60, 'DWLADMINSERVICE',  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lobTpCdType', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'lobTpCdType', CURRENT_TIMESTAMP, 70, 'DWLADMINSERVICE',  null, 1020086, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lobTpCdValue', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'lobTpCdValue', CURRENT_TIMESTAMP, 80, 'DWLADMINSERVICE',  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('description', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'description', CURRENT_TIMESTAMP, 90, 'DWLADMINSERVICE',  null, 1020038, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('expiry_dt', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'expiry_dt', CURRENT_TIMESTAMP, 100, 'DWLADMINSERVICE',  null, 1020035, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('last_update_dt', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'last_update_dt', CURRENT_TIMESTAMP, 110, 'DWLADMINSERVICE',  null, 1020023, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('last_update_user', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'last_update_user', CURRENT_TIMESTAMP, 120, 'DWLADMINSERVICE',  null, 1020025, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistActionCode', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'HistActionCode', CURRENT_TIMESTAMP, 130, 'DWLADMINSERVICE',  null, 1020031, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistCreateDate', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'HistCreateDate', CURRENT_TIMESTAMP, 140, 'DWLADMINSERVICE',  null, 1020026, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistCreatedBy', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'HistCreatedBy', CURRENT_TIMESTAMP, 150, 'DWLADMINSERVICE',  null, 1020024, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistEndDate', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'HistEndDate', CURRENT_TIMESTAMP, 160, 'DWLADMINSERVICE',  null, 1020030, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistTypeCode', 'CTCLobSourceTypeBObj', 'DWLADMINSERVICE', 'HistTypeCode', CURRENT_TIMESTAMP, 170, 'DWLADMINSERVICE',  null, 1020034, 1);
----------------------------------------------
-- Connect V_GROUP record with CDDWLTABLETP
----------------------------------------------

INSERT INTO CTCMDM.GROUPDWLTABLE (GROUP_DWLTABLE_ID, APPLICATION, GROUP_NAME, DWLTABLE_TP_CD, LAST_UPDATE_USER, LAST_UPDATE_DT)
VALUES (NEXTVAL FOR CTCMDM.GROUPDWLTABLE_ID_SEQ, 'TCRM', 'CTCAdminSysPriorityTypeBObj', 1020100, 'cusadmin', CURRENT_TIMESTAMP);

----------------------------------------------
-- Add attributes for entity type CTCAdminSysPriority to V_ELEMENT table
----------------------------------------------

INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lang_tp_cd', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'lang_tp_cd', CURRENT_TIMESTAMP, 10, 'TCRM',  null, 1020122, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lang_tp_value', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'lang_tp_value', CURRENT_TIMESTAMP, 20, 'TCRM',  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('AdminSysTpCd', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'AdminSysTpCd', CURRENT_TIMESTAMP, 30, 'TCRM',  null, 1020127, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('name', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'name', CURRENT_TIMESTAMP, 40, 'TCRM',  null, 1020135, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('PriorityTpCd', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'PriorityTpCd', CURRENT_TIMESTAMP, 50, 'TCRM',  null, 1020147, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('description', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'description', CURRENT_TIMESTAMP, 60, 'TCRM',  null, 1020117, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('expiry_dt', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'expiry_dt', CURRENT_TIMESTAMP, 70, 'TCRM',  null, 1020114, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('last_update_dt', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'last_update_dt', CURRENT_TIMESTAMP, 80, 'TCRM',  null, 1020102, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('last_update_user', 'CTCAdminSysPriorityTypeBObj', 'TCRM', 'last_update_user', CURRENT_TIMESTAMP, 90, 'TCRM',  null, 1020104, 1);
----------------------------------------------
-- Connect V_GROUP record with CDDWLTABLETP
----------------------------------------------

INSERT INTO CTCMDM.GROUPDWLTABLE (GROUP_DWLTABLE_ID, APPLICATION, GROUP_NAME, DWLTABLE_TP_CD, LAST_UPDATE_USER, LAST_UPDATE_DT)
VALUES (NEXTVAL FOR CTCMDM.GROUPDWLTABLE_ID_SEQ, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 1020100, 'cusadmin', CURRENT_TIMESTAMP);


----------------------------------------------
-- DWLADMINSERVICE: Add attributes for entity type CTCAdminSysPriority to V_ELEMENT table
----------------------------------------------

INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lang_tp_cd', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'lang_tp_cd', CURRENT_TIMESTAMP, 10, 'DWLADMINSERVICE',  null, 1020122, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('lang_tp_value', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'lang_tp_value', CURRENT_TIMESTAMP, 20, 'DWLADMINSERVICE',  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('AdminSysTpCd', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'AdminSysTpCd', CURRENT_TIMESTAMP, 30, 'DWLADMINSERVICE',  null, 1020127, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('name', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'name', CURRENT_TIMESTAMP, 40, 'DWLADMINSERVICE',  null, 1020135, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('PriorityTpCd', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'PriorityTpCd', CURRENT_TIMESTAMP, 50, 'DWLADMINSERVICE',  null, 1020147, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('description', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'description', CURRENT_TIMESTAMP, 60, 'DWLADMINSERVICE',  null, 1020117, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('expiry_dt', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'expiry_dt', CURRENT_TIMESTAMP, 70, 'DWLADMINSERVICE',  null, 1020114, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('last_update_dt', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'last_update_dt', CURRENT_TIMESTAMP, 80, 'DWLADMINSERVICE',  null, 1020102, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('last_update_user', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'last_update_user', CURRENT_TIMESTAMP, 90, 'DWLADMINSERVICE',  null, 1020104, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistActionCode', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'HistActionCode', CURRENT_TIMESTAMP, 100, 'DWLADMINSERVICE',  null, 1020110, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistCreateDate', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'HistCreateDate', CURRENT_TIMESTAMP, 110, 'DWLADMINSERVICE',  null, 1020105, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistCreatedBy', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'HistCreatedBy', CURRENT_TIMESTAMP, 120, 'DWLADMINSERVICE',  null, 1020103, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistEndDate', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'HistEndDate', CURRENT_TIMESTAMP, 130, 'DWLADMINSERVICE',  null, 1020109, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('HistTypeCode', 'CTCAdminSysPriorityTypeBObj', 'DWLADMINSERVICE', 'HistTypeCode', CURRENT_TIMESTAMP, 140, 'DWLADMINSERVICE',  null, 1020113, 1);

DROP SEQUENCE CTCMDM.GROUPDWLTABLE_ID_SEQ RESTRICT;

----------------------------------------------
-- V_ELEMENTATTRIBUTE
----------------------------------------------


CREATE SEQUENCE CTCMDM.V_ELATTR_ID_SEQ AS BIGINT START WITH 1020000 INCREMENT BY 1 MINVALUE 1020000 MAXVALUE 1029999 CACHE 10;

INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 'last_update_dt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCLobSourceTypeBObj', 'last_update_dt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 'HistCreateDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 'HistEndDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 'expiry_dt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCLobSourceTypeBObj', 'expiry_dt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 4, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 'lang_tp_cd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 1, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 'lang_tp_cd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 4, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 'LobSourceTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 1, 'DWLADMINSERVICE', 'CTCLobSourceTypeBObj', 'Name', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 5, 'TCRM', 'CTCLobSourceTypeBObj', 'adminSystemTpCdType', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 5, 'TCRM', 'CTCLobSourceTypeBObj', 'lobTpCdType', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 'last_update_dt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCAdminSysPriorityTypeBObj', 'last_update_dt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 'HistCreateDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 'HistEndDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 'expiry_dt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCAdminSysPriorityTypeBObj', 'expiry_dt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 4, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 'lang_tp_cd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 1, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 'lang_tp_cd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 4, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 'AdminSysTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 1, 'DWLADMINSERVICE', 'CTCAdminSysPriorityTypeBObj', 'name', CURRENT_TIMESTAMP);
DROP SEQUENCE CTCMDM.V_ELATTR_ID_SEQ RESTRICT;

----------------------------------------------
-- Transactions
----------------------------------------------


----------------------------------------------
-- CDBUSINESSTXTP
----------------------------------------------


----------------------------------------------
-- CDINTERNALTXNTP
----------------------------------------------




----------------------------------------------
-- BUSINTERNALTXN
----------------------------------------------



----------------------------------------------
-- BUSINESSTXREQRESP
----------------------------------------------



----------------------------------------------
-- INTERNALTXREQRESP
----------------------------------------------


----------------------------------------------
-- GROUPTXMAP
----------------------------------------------



----------------------------------------------
-- TAIL setup
----------------------------------------------




-- @SqlSnippetPriority 400
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- MDM_TODO: CDKWB0044I Review CDDWLCOLUMNTP LOCALE_SENSITIVE settings
-- MDM_TODO: CDKWB0043I You may need to add entries to the INTERNALTXNKEY table for any new transactions


-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCAdditions_SETUP_ZOS.sql
-- 			db2 -vf CTCAdditions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCAdditions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCAdditions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCAdditions_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



----------------------------------------------
-- Component type
----------------------------------------------

INSERT INTO CTCMDM.COMPONENTTYPE (COMPONENT_TYPE_ID, DWL_PROD_TP_CD, COMPON_TYPE_VALUE, COMPON_LONG_DESC, LAST_UPDATE_DT, COMPONENT_CLASS ) 
   VALUES ( 1010007, 1, 'CTCDataSourceComponent', null, CURRENT_TIMESTAMP, 'com.ctc.mdm.addition.component.CTCDataSourceComponent');
INSERT INTO CTCMDM.COMPONENTTYPE (COMPONENT_TYPE_ID, DWL_PROD_TP_CD, COMPON_TYPE_VALUE, COMPON_LONG_DESC, LAST_UPDATE_DT, COMPONENT_CLASS ) 
   VALUES ( 1010008, 1, 'CTCDataSourceController', null, CURRENT_TIMESTAMP, null);
INSERT INTO CTCMDM.COMPONENTTYPE (COMPONENT_TYPE_ID, DWL_PROD_TP_CD, COMPON_TYPE_VALUE, COMPON_LONG_DESC, LAST_UPDATE_DT, COMPONENT_CLASS ) 
   VALUES ( 1010010, 1, 'CTCDataSourceBObj', null, CURRENT_TIMESTAMP, null);
INSERT INTO CTCMDM.COMPONENTTYPE (COMPONENT_TYPE_ID, DWL_PROD_TP_CD, COMPON_TYPE_VALUE, COMPON_LONG_DESC, LAST_UPDATE_DT, COMPONENT_CLASS ) 
   VALUES ( 1010161, 1, 'CTCPartySegmentBObj', null, CURRENT_TIMESTAMP, null);
INSERT INTO CTCMDM.COMPONENTTYPE (COMPONENT_TYPE_ID, DWL_PROD_TP_CD, COMPON_TYPE_VALUE, COMPON_LONG_DESC, LAST_UPDATE_DT, COMPONENT_CLASS ) 
   VALUES ( 1010328, 1, 'CTCPartySegmentComponent', null, CURRENT_TIMESTAMP, 'com.ctc.mdm.addition.component.CTCPartySegmentComponent');
INSERT INTO CTCMDM.COMPONENTTYPE (COMPONENT_TYPE_ID, DWL_PROD_TP_CD, COMPON_TYPE_VALUE, COMPON_LONG_DESC, LAST_UPDATE_DT, COMPONENT_CLASS ) 
   VALUES ( 1010329, 1, 'CTCPartySegmentController', null, CURRENT_TIMESTAMP, null);

----------------------------------------------
-- Error messages
----------------------------------------------

   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010063, 1010007, 'READERR', 1010060, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010067, 1010007, 'READERR', 1010064, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010071, 1010007, 'READERR', 1010068, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010086, 1010007, 'INSERR', 1010083, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010090, 1010007, 'DKERR', 1010087, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010103, 1010007, 'UPDERR', 1010100, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010360, 1010007, 'READERR', 1010357, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010061, 1010008, 'READERR', 1010060, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010084, 1010008, 'INSERR', 1010083, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010101, 1010008, 'UPDERR', 1010100, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010358, 1010008, 'READERR', 1010357, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010012, 1010010, 'FVERR', 20, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010040, 1010010, 'FVERR', 1010039, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010044, 1010010, 'DIERR', 1010043, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010115, 1010010, 'FVERR', 1010114, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010137, 1010010, 'DIERR', 1010136, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010149, 1010010, 'DIERR', 1010148, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010331, 1010010, 'FVERR', 1010330, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010338, 1010010, 'FVERR', 1010337, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010342, 1010010, 'FVERR', 1010341, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010163, 1010161, 'FVERR', 20, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010191, 1010161, 'FVERR', 1010190, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010195, 1010161, 'DIERR', 1010194, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010258, 1010161, 'FVERR', 1010257, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010266, 1010161, 'FVERR', 1010265, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010274, 1010161, 'FVERR', 1010273, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010282, 1010161, 'FVERR', 1010281, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010306, 1010161, 'FVERR', 1010305, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010312, 1010161, 'DIERR', 1010311, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010324, 1010161, 'DIERR', 1010323, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010214, 1010328, 'READERR', 1010211, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010218, 1010328, 'READERR', 1010215, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010222, 1010328, 'READERR', 1010219, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010237, 1010328, 'INSERR', 1010234, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010241, 1010328, 'DKERR', 1010238, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010254, 1010328, 'UPDERR', 1010251, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010379, 1010328, 'READERR', 1010376, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010383, 1010328, 'READERR', 1010380, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010387, 1010328, 'READERR', 1010384, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010212, 1010329, 'READERR', 1010211, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010235, 1010329, 'INSERR', 1010234, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010252, 1010329, 'UPDERR', 1010251, CURRENT_TIMESTAMP);
   
INSERT INTO CTCMDM.ERRREASON (ERR_REASON_TP_CD, COMPONENT_TYPE_ID, ERR_TYPE_CD, ERR_MESSAGE_TP_CD, LAST_UPDATE_DT) 
   VALUES (1010377, 1010329, 'READERR', 1010376, CURRENT_TIMESTAMP);

---------------------------------------------
-- Metadata setup
---------------------------------------------


----------------------------------------------
-- Add record for the database table for CTCDataSource
----------------------------------------------

INSERT INTO CTCMDM.CDDWLTABLETP (DWLTABLE_TP_CD, TABLE_NAME, DESCRIPTION, LAST_UPDATE_DT, EXPIRY_DT, CODE_TYPE_IND, DWL_PROD_TP_CD)
VALUES (1010021, 'CTCDATASOURCE', null, CURRENT_TIMESTAMP, null, 'Y', 1);

INSERT INTO CTCMDM.CDDWLTABLETP (DWLTABLE_TP_CD, TABLE_NAME, DESCRIPTION, LAST_UPDATE_DT, EXPIRY_DT, CODE_TYPE_IND, DWL_PROD_TP_CD)
VALUES (1010022, 'H_CTCDATASOURCE', null, CURRENT_TIMESTAMP, null, 'Y', 1);

----------------------------------------------
-- Add records for the table columns for CTCDataSource
----------------------------------------------


INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010037, 1010021, 'data_source_id', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010110, 1010021, 'entity_name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010118, 1010021, 'instance_pk', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010126, 1010021, 'attribute_name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010134, 1010021, 'start_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010146, 1010021, 'end_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010345, 1010021, 'admin_sys_tp_cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010023, 1010021, 'LAST_UPDATE_DT', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010033, 1010021, 'LAST_UPDATE_TX_ID', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010025, 1010021, 'LAST_UPDATE_USER', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010038, 1010022, 'data_source_id', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010111, 1010022, 'entity_name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010119, 1010022, 'instance_pk', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010127, 1010022, 'attribute_name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010135, 1010022, 'start_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010147, 1010022, 'end_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010346, 1010022, 'admin_sys_tp_cd', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010031, 1010022, 'h_action_code', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010026, 1010022, 'h_create_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010024, 1010022, 'h_created_by', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010030, 1010022, 'h_end_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010028, 1010022, 'H_data_source_id', null, CURRENT_TIMESTAMP, null, 'N');

----------------------------------------------
-- Add record for the database table for CTCPartySegment
----------------------------------------------

INSERT INTO CTCMDM.CDDWLTABLETP (DWLTABLE_TP_CD, TABLE_NAME, DESCRIPTION, LAST_UPDATE_DT, EXPIRY_DT, CODE_TYPE_IND, DWL_PROD_TP_CD)
VALUES (1010172, 'CTCPARTYSEGMENT', null, CURRENT_TIMESTAMP, null, 'Y', 1);

INSERT INTO CTCMDM.CDDWLTABLETP (DWLTABLE_TP_CD, TABLE_NAME, DESCRIPTION, LAST_UPDATE_DT, EXPIRY_DT, CODE_TYPE_IND, DWL_PROD_TP_CD)
VALUES (1010173, 'H_CTCPARTYSEGMENT', null, CURRENT_TIMESTAMP, null, 'Y', 1);

----------------------------------------------
-- Add records for the table columns for CTCPartySegment
----------------------------------------------


INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010188, 1010172, 'party_segment_id', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010261, 1010172, 'cont_id', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010269, 1010172, 'segment_provider', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010277, 1010172, 'business_group_name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010285, 1010172, 'segment_type_name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010293, 1010172, 'segment_value', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010301, 1010172, 'segment_category_type', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010309, 1010172, 'effective_start_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010361, 1010172, 'effective_end_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010174, 1010172, 'LAST_UPDATE_DT', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010184, 1010172, 'LAST_UPDATE_TX_ID', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010176, 1010172, 'LAST_UPDATE_USER', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010189, 1010173, 'party_segment_id', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010262, 1010173, 'cont_id', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010270, 1010173, 'segment_provider', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010278, 1010173, 'business_group_name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010286, 1010173, 'segment_type_name', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010294, 1010173, 'segment_value', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010302, 1010173, 'segment_category_type', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010310, 1010173, 'effective_start_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010362, 1010173, 'effective_end_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010182, 1010173, 'h_action_code', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010177, 1010173, 'h_create_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010175, 1010173, 'h_created_by', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010181, 1010173, 'h_end_dt', null, CURRENT_TIMESTAMP, null, 'N');

INSERT INTO CTCMDM.CDDWLCOLUMNTP (DWLCOLUMN_TP_CD, DWLTABLE_TP_CD, COLUMN_NAME, EXPIRY_DT, LAST_UPDATE_DT, DESCRIPTION, LOCALE_SENSITIVE)
VALUES (1010179, 1010173, 'H_party_segment_id', null, CURRENT_TIMESTAMP, null, 'N');

----------------------------------------------
-- V_GROUP
----------------------------------------------

INSERT INTO CTCMDM.V_GROUP (GROUP_NAME, APPLICATION, OBJECT_NAME, LAST_UPDATE_DT, SORTBY, ALIAS_NAME, PARENT_GRP_NAME ) 
  VALUES ('CTCDataSourceBObj', 'TCRM', 'com.ctc.mdm.addition.component.CTCDataSourceBObj', CURRENT_TIMESTAMP, 'LAST_UPDATE_DT', 'CTCDATASOURCE', null);
INSERT INTO CTCMDM.V_GROUP (GROUP_NAME, APPLICATION, OBJECT_NAME, LAST_UPDATE_DT, SORTBY, ALIAS_NAME, PARENT_GRP_NAME ) 
  VALUES ('CTCPartySegmentBObj', 'TCRM', 'com.ctc.mdm.addition.component.CTCPartySegmentBObj', CURRENT_TIMESTAMP, 'LAST_UPDATE_DT', 'CTCPARTYSEGMENT', null);

CREATE SEQUENCE CTCMDM.GROUPDWLTABLE_ID_SEQ AS BIGINT START WITH 1010000 INCREMENT BY 1 MINVALUE 1010000 MAXVALUE 1019999 CACHE 10;

----------------------------------------------
-- Connect V_GROUP record with CDDWLTABLETP
----------------------------------------------

INSERT INTO CTCMDM.GROUPDWLTABLE (GROUP_DWLTABLE_ID, APPLICATION, GROUP_NAME, DWLTABLE_TP_CD, LAST_UPDATE_USER, LAST_UPDATE_DT)
VALUES (NEXTVAL FOR CTCMDM.GROUPDWLTABLE_ID_SEQ, 'TCRM', 'CTCDataSourceBObj', 1010021, 'cusadmin', CURRENT_TIMESTAMP);

----------------------------------------------
-- Add attributes for entity type CTCDataSource to V_ELEMENT table
----------------------------------------------

INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('DataSourceID', 'CTCDataSourceBObj', 'TCRM', 'DataSourceID', CURRENT_TIMESTAMP, 10, null,  null, 1010037, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('EntityName', 'CTCDataSourceBObj', 'TCRM', 'EntityName', CURRENT_TIMESTAMP, 20, null,  null, 1010110, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('InstancePK', 'CTCDataSourceBObj', 'TCRM', 'InstancePK', CURRENT_TIMESTAMP, 30, null,  null, 1010118, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('AttributeName', 'CTCDataSourceBObj', 'TCRM', 'AttributeName', CURRENT_TIMESTAMP, 40, null,  null, 1010126, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('StartDate', 'CTCDataSourceBObj', 'TCRM', 'StartDate', CURRENT_TIMESTAMP, 50, null,  null, 1010134, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('EndDate', 'CTCDataSourceBObj', 'TCRM', 'EndDate', CURRENT_TIMESTAMP, 60, null,  null, 1010146, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('AdminSystemType', 'CTCDataSourceBObj', 'TCRM', 'AdminSystemType', CURRENT_TIMESTAMP, 70, 'TCRM',  'ClientPotentialTypeTypeBObj', 1010345, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('AdminSystemValue', 'CTCDataSourceBObj', 'TCRM', 'AdminSystemValue', CURRENT_TIMESTAMP, 80, null,  null, null, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCDataSourceLastUpdateDate', 'CTCDataSourceBObj', 'TCRM', 'CTCDataSourceLastUpdateDate', CURRENT_TIMESTAMP, 90, null,  null, 1010023, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCDataSourceLastUpdateTxId', 'CTCDataSourceBObj', 'TCRM', 'CTCDataSourceLastUpdateTxId', CURRENT_TIMESTAMP, 100, null,  null, 1010033, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCDataSourceLastUpdateUser', 'CTCDataSourceBObj', 'TCRM', 'CTCDataSourceLastUpdateUser', CURRENT_TIMESTAMP, 110, null,  null, 1010025, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCDataSourceHistActionCode', 'CTCDataSourceBObj', 'TCRM', 'CTCDataSourceHistActionCode', CURRENT_TIMESTAMP, 120, null,  null, 1010031, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCDataSourceHistCreateDate', 'CTCDataSourceBObj', 'TCRM', 'CTCDataSourceHistCreateDate', CURRENT_TIMESTAMP, 130, null,  null, 1010026, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCDataSourceHistCreatedBy', 'CTCDataSourceBObj', 'TCRM', 'CTCDataSourceHistCreatedBy', CURRENT_TIMESTAMP, 140, null,  null, 1010024, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCDataSourceHistEndDate', 'CTCDataSourceBObj', 'TCRM', 'CTCDataSourceHistEndDate', CURRENT_TIMESTAMP, 150, null,  null, 1010030, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCDataSourceHistoryIdPK', 'CTCDataSourceBObj', 'TCRM', 'CTCDataSourceHistoryIdPK', CURRENT_TIMESTAMP, 160, null,  null, 1010028, 1);
----------------------------------------------
-- Connect V_GROUP record with CDDWLTABLETP
----------------------------------------------

INSERT INTO CTCMDM.GROUPDWLTABLE (GROUP_DWLTABLE_ID, APPLICATION, GROUP_NAME, DWLTABLE_TP_CD, LAST_UPDATE_USER, LAST_UPDATE_DT)
VALUES (NEXTVAL FOR CTCMDM.GROUPDWLTABLE_ID_SEQ, 'TCRM', 'CTCPartySegmentBObj', 1010172, 'cusadmin', CURRENT_TIMESTAMP);

----------------------------------------------
-- Add attributes for entity type CTCPartySegment to V_ELEMENT table
----------------------------------------------

INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('PartySegmentId', 'CTCPartySegmentBObj', 'TCRM', 'PartySegmentId', CURRENT_TIMESTAMP, 10, null,  null, 1010188, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('ContId', 'CTCPartySegmentBObj', 'TCRM', 'ContId', CURRENT_TIMESTAMP, 20, null,  null, 1010261, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('SegmentProvider', 'CTCPartySegmentBObj', 'TCRM', 'SegmentProvider', CURRENT_TIMESTAMP, 30, null,  null, 1010269, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('BusinessGroupName', 'CTCPartySegmentBObj', 'TCRM', 'BusinessGroupName', CURRENT_TIMESTAMP, 40, null,  null, 1010277, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('SegmentTypeName', 'CTCPartySegmentBObj', 'TCRM', 'SegmentTypeName', CURRENT_TIMESTAMP, 50, null,  null, 1010285, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('SegmentValue', 'CTCPartySegmentBObj', 'TCRM', 'SegmentValue', CURRENT_TIMESTAMP, 60, null,  null, 1010293, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('SegmentCategoryType', 'CTCPartySegmentBObj', 'TCRM', 'SegmentCategoryType', CURRENT_TIMESTAMP, 70, null,  null, 1010301, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('EffectiveStartDt', 'CTCPartySegmentBObj', 'TCRM', 'EffectiveStartDt', CURRENT_TIMESTAMP, 80, null,  null, 1010309, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('EffectiveEndDt', 'CTCPartySegmentBObj', 'TCRM', 'EffectiveEndDt', CURRENT_TIMESTAMP, 90, null,  null, 1010361, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCPartySegmentLastUpdateDate', 'CTCPartySegmentBObj', 'TCRM', 'CTCPartySegmentLastUpdateDate', CURRENT_TIMESTAMP, 100, null,  null, 1010174, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCPartySegmentLastUpdateTxId', 'CTCPartySegmentBObj', 'TCRM', 'CTCPartySegmentLastUpdateTxId', CURRENT_TIMESTAMP, 110, null,  null, 1010184, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCPartySegmentLastUpdateUser', 'CTCPartySegmentBObj', 'TCRM', 'CTCPartySegmentLastUpdateUser', CURRENT_TIMESTAMP, 120, null,  null, 1010176, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCPartySegmentHistActionCode', 'CTCPartySegmentBObj', 'TCRM', 'CTCPartySegmentHistActionCode', CURRENT_TIMESTAMP, 130, null,  null, 1010182, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCPartySegmentHistCreateDate', 'CTCPartySegmentBObj', 'TCRM', 'CTCPartySegmentHistCreateDate', CURRENT_TIMESTAMP, 140, null,  null, 1010177, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCPartySegmentHistCreatedBy', 'CTCPartySegmentBObj', 'TCRM', 'CTCPartySegmentHistCreatedBy', CURRENT_TIMESTAMP, 150, null,  null, 1010175, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCPartySegmentHistEndDate', 'CTCPartySegmentBObj', 'TCRM', 'CTCPartySegmentHistEndDate', CURRENT_TIMESTAMP, 160, null,  null, 1010181, 1);
INSERT INTO CTCMDM.V_ELEMENT (ELEMENT_NAME, GROUP_NAME, APPLICATION, ATTRIBUTE_NAME, LAST_UPDATE_DT, RESPONSE_ORDER, ELEMENTAPPNAME, ELEMENTGROUPNAME, DWLCOLUMN_TP_CD, CARDINALITY_TP_CD) 
   VALUES ('CTCPartySegmentHistoryIdPK', 'CTCPartySegmentBObj', 'TCRM', 'CTCPartySegmentHistoryIdPK', CURRENT_TIMESTAMP, 170, null,  null, 1010179, 1);

DROP SEQUENCE CTCMDM.GROUPDWLTABLE_ID_SEQ RESTRICT;

----------------------------------------------
-- V_ELEMENTATTRIBUTE
----------------------------------------------


CREATE SEQUENCE CTCMDM.V_ELATTR_ID_SEQ AS BIGINT START WITH 1010000 INCREMENT BY 1 MINVALUE 1010000 MAXVALUE 1019999 CACHE 10;

INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCDataSourceBObj', 'CTCDataSourceLastUpdateDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCDataSourceBObj', 'CTCDataSourceHistCreateDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCDataSourceBObj', 'CTCDataSourceHistEndDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 4, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 100, 'TCRM', 'CTCDataSourceBObj', 'StartDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 100, 'TCRM', 'CTCDataSourceBObj', 'EndDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 5, 'TCRM', 'CTCDataSourceBObj', 'AdminSystemType', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCPartySegmentBObj', 'CTCPartySegmentLastUpdateDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCPartySegmentBObj', 'CTCPartySegmentHistCreateDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 101, 'TCRM', 'CTCPartySegmentBObj', 'CTCPartySegmentHistEndDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 4, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 100, 'TCRM', 'CTCPartySegmentBObj', 'EffectiveStartDt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.V_ELEMENTATTRIBUTE (V_ELEMENT_ATTRB_ID, ATTRIBUTE_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_DT) 
   VALUES (NEXTVAL FOR CTCMDM.V_ELATTR_ID_SEQ, 100, 'TCRM', 'CTCPartySegmentBObj', 'EffectiveEndDt', CURRENT_TIMESTAMP);
DROP SEQUENCE CTCMDM.V_ELATTR_ID_SEQ RESTRICT;

----------------------------------------------
-- Transactions
----------------------------------------------


----------------------------------------------
-- CDBUSINESSTXTP
----------------------------------------------


INSERT INTO CTCMDM.CDBUSINESSTXTP (BUSINESS_TX_TP_CD, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, TX_LOG_IND, TX_OBJECT_TP, DEPRECATED_SINCE, DWL_PROD_TP_CD, PARENT_BUSINESS_TX_TP_CD) 
   VALUES (1010047, 'getCTCDataSource', null, null, CURRENT_TIMESTAMP, 'N', 'I', null, 1, null);

INSERT INTO CTCMDM.EXTERNALTXNKEY (EXTERN_TX_KEY_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010048, 1010047, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDBUSINESSTXTP (BUSINESS_TX_TP_CD, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, TX_LOG_IND, TX_OBJECT_TP, DEPRECATED_SINCE, DWL_PROD_TP_CD, PARENT_BUSINESS_TX_TP_CD) 
   VALUES (1010074, 'addCTCDataSource', null, null, CURRENT_TIMESTAMP, 'N', 'P', null, 1, null);

INSERT INTO CTCMDM.EXTERNALTXNKEY (EXTERN_TX_KEY_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010075, 1010074, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDBUSINESSTXTP (BUSINESS_TX_TP_CD, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, TX_LOG_IND, TX_OBJECT_TP, DEPRECATED_SINCE, DWL_PROD_TP_CD, PARENT_BUSINESS_TX_TP_CD) 
   VALUES (1010091, 'updateCTCDataSource', null, null, CURRENT_TIMESTAMP, 'N', 'P', null, 1, null);

INSERT INTO CTCMDM.EXTERNALTXNKEY (EXTERN_TX_KEY_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010092, 1010091, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDBUSINESSTXTP (BUSINESS_TX_TP_CD, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, TX_LOG_IND, TX_OBJECT_TP, DEPRECATED_SINCE, DWL_PROD_TP_CD, PARENT_BUSINESS_TX_TP_CD) 
   VALUES (1010198, 'getCTCPartySegment', null, null, CURRENT_TIMESTAMP, 'N', 'I', null, 1, null);

INSERT INTO CTCMDM.EXTERNALTXNKEY (EXTERN_TX_KEY_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010199, 1010198, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDBUSINESSTXTP (BUSINESS_TX_TP_CD, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, TX_LOG_IND, TX_OBJECT_TP, DEPRECATED_SINCE, DWL_PROD_TP_CD, PARENT_BUSINESS_TX_TP_CD) 
   VALUES (1010225, 'addCTCPartySegment', null, null, CURRENT_TIMESTAMP, 'N', 'P', null, 1, null);

INSERT INTO CTCMDM.EXTERNALTXNKEY (EXTERN_TX_KEY_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010226, 1010225, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDBUSINESSTXTP (BUSINESS_TX_TP_CD, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, TX_LOG_IND, TX_OBJECT_TP, DEPRECATED_SINCE, DWL_PROD_TP_CD, PARENT_BUSINESS_TX_TP_CD) 
   VALUES (1010242, 'updateCTCPartySegment', null, null, CURRENT_TIMESTAMP, 'N', 'P', null, 1, null);

INSERT INTO CTCMDM.EXTERNALTXNKEY (EXTERN_TX_KEY_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010243, 1010242, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDBUSINESSTXTP (BUSINESS_TX_TP_CD, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, TX_LOG_IND, TX_OBJECT_TP, DEPRECATED_SINCE, DWL_PROD_TP_CD, PARENT_BUSINESS_TX_TP_CD) 
   VALUES (1010347, 'getAllCTCDataSourcesByEntity', null, null, CURRENT_TIMESTAMP, 'N', 'I', null, 1, null);

INSERT INTO CTCMDM.EXTERNALTXNKEY (EXTERN_TX_KEY_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010348, 1010347, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDBUSINESSTXTP (BUSINESS_TX_TP_CD, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, TX_LOG_IND, TX_OBJECT_TP, DEPRECATED_SINCE, DWL_PROD_TP_CD, PARENT_BUSINESS_TX_TP_CD) 
   VALUES (1010363, 'getAllCTCPartySegment', null, null, CURRENT_TIMESTAMP, 'N', 'I', null, 1, null);

INSERT INTO CTCMDM.EXTERNALTXNKEY (EXTERN_TX_KEY_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010364, 1010363, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', null, CURRENT_TIMESTAMP);


----------------------------------------------
-- CDINTERNALTXNTP
----------------------------------------------


INSERT INTO CTCMDM.CDINTERNALTXNTP (INTERNAL_BUS_TX_TP, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, COMPONENT_TYPE_ID, PARENT_INTERNAL_BUS_TX_TP) 
   VALUES (1010050, 'getCTCDataSource', null, null, CURRENT_TIMESTAMP, 1010007, null);
 
INSERT INTO CTCMDM.INTERNALTXNKEY (INTERN_TX_KEY_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010049, 1010050, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDINTERNALTXNTP (INTERNAL_BUS_TX_TP, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, COMPONENT_TYPE_ID, PARENT_INTERNAL_BUS_TX_TP) 
   VALUES (1010077, 'addCTCDataSource', null, null, CURRENT_TIMESTAMP, 1010007, null);
 
INSERT INTO CTCMDM.INTERNALTXNKEY (INTERN_TX_KEY_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010076, 1010077, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDINTERNALTXNTP (INTERNAL_BUS_TX_TP, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, COMPONENT_TYPE_ID, PARENT_INTERNAL_BUS_TX_TP) 
   VALUES (1010094, 'updateCTCDataSource', null, null, CURRENT_TIMESTAMP, 1010007, null);
 
INSERT INTO CTCMDM.INTERNALTXNKEY (INTERN_TX_KEY_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010093, 1010094, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDINTERNALTXNTP (INTERNAL_BUS_TX_TP, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, COMPONENT_TYPE_ID, PARENT_INTERNAL_BUS_TX_TP) 
   VALUES (1010201, 'getCTCPartySegment', null, null, CURRENT_TIMESTAMP, 1010328, null);
 
INSERT INTO CTCMDM.INTERNALTXNKEY (INTERN_TX_KEY_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010200, 1010201, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDINTERNALTXNTP (INTERNAL_BUS_TX_TP, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, COMPONENT_TYPE_ID, PARENT_INTERNAL_BUS_TX_TP) 
   VALUES (1010228, 'addCTCPartySegment', null, null, CURRENT_TIMESTAMP, 1010328, null);
 
INSERT INTO CTCMDM.INTERNALTXNKEY (INTERN_TX_KEY_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010227, 1010228, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDINTERNALTXNTP (INTERNAL_BUS_TX_TP, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, COMPONENT_TYPE_ID, PARENT_INTERNAL_BUS_TX_TP) 
   VALUES (1010245, 'updateCTCPartySegment', null, null, CURRENT_TIMESTAMP, 1010328, null);
 
INSERT INTO CTCMDM.INTERNALTXNKEY (INTERN_TX_KEY_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010244, 1010245, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDINTERNALTXNTP (INTERNAL_BUS_TX_TP, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, COMPONENT_TYPE_ID, PARENT_INTERNAL_BUS_TX_TP) 
   VALUES (1010350, 'getAllCTCDataSourcesByEntity', null, null, CURRENT_TIMESTAMP, 1010007, null);
 
INSERT INTO CTCMDM.INTERNALTXNKEY (INTERN_TX_KEY_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010349, 1010350, 'TCRM', 'CTCDataSourceBObj', 'DataSourceID', null, CURRENT_TIMESTAMP);


INSERT INTO CTCMDM.CDINTERNALTXNTP (INTERNAL_BUS_TX_TP, NAME, DESCRIPTION, EXPIRY_DT, LAST_UPDATE_DT, COMPONENT_TYPE_ID, PARENT_INTERNAL_BUS_TX_TP) 
   VALUES (1010366, 'getAllCTCPartySegment', null, null, CURRENT_TIMESTAMP, 1010328, null);
 
INSERT INTO CTCMDM.INTERNALTXNKEY (INTERN_TX_KEY_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, ELEMENT_NAME, LAST_UPDATE_USER, LAST_UPDATE_DT)
   VALUES (1010365, 1010366, 'TCRM', 'CTCPartySegmentBObj', 'PartySegmentId', null, CURRENT_TIMESTAMP);




----------------------------------------------
-- BUSINTERNALTXN
----------------------------------------------
INSERT INTO CTCMDM.BUSINTERNALTXN (BUS_INTERN_TXN_ID, BUSINESS_TX_TP_CD, INTERNAL_BUS_TX_TP, INT_TX_LOG_IND, LAST_UPDATE_DT) 
   VALUES (1010051, 1010047, 1010050, 'N', CURRENT_TIMESTAMP);
 
INSERT INTO CTCMDM.BUSINTERNALTXN (BUS_INTERN_TXN_ID, BUSINESS_TX_TP_CD, INTERNAL_BUS_TX_TP, INT_TX_LOG_IND, LAST_UPDATE_DT) 
   VALUES (1010078, 1010074, 1010077, 'N', CURRENT_TIMESTAMP);
 
INSERT INTO CTCMDM.BUSINTERNALTXN (BUS_INTERN_TXN_ID, BUSINESS_TX_TP_CD, INTERNAL_BUS_TX_TP, INT_TX_LOG_IND, LAST_UPDATE_DT) 
   VALUES (1010095, 1010091, 1010094, 'N', CURRENT_TIMESTAMP);
 
INSERT INTO CTCMDM.BUSINTERNALTXN (BUS_INTERN_TXN_ID, BUSINESS_TX_TP_CD, INTERNAL_BUS_TX_TP, INT_TX_LOG_IND, LAST_UPDATE_DT) 
   VALUES (1010202, 1010198, 1010201, 'N', CURRENT_TIMESTAMP);
 
INSERT INTO CTCMDM.BUSINTERNALTXN (BUS_INTERN_TXN_ID, BUSINESS_TX_TP_CD, INTERNAL_BUS_TX_TP, INT_TX_LOG_IND, LAST_UPDATE_DT) 
   VALUES (1010229, 1010225, 1010228, 'N', CURRENT_TIMESTAMP);
 
INSERT INTO CTCMDM.BUSINTERNALTXN (BUS_INTERN_TXN_ID, BUSINESS_TX_TP_CD, INTERNAL_BUS_TX_TP, INT_TX_LOG_IND, LAST_UPDATE_DT) 
   VALUES (1010246, 1010242, 1010245, 'N', CURRENT_TIMESTAMP);
 
INSERT INTO CTCMDM.BUSINTERNALTXN (BUS_INTERN_TXN_ID, BUSINESS_TX_TP_CD, INTERNAL_BUS_TX_TP, INT_TX_LOG_IND, LAST_UPDATE_DT) 
   VALUES (1010351, 1010347, 1010350, 'N', CURRENT_TIMESTAMP);
 
INSERT INTO CTCMDM.BUSINTERNALTXN (BUS_INTERN_TXN_ID, BUSINESS_TX_TP_CD, INTERNAL_BUS_TX_TP, INT_TX_LOG_IND, LAST_UPDATE_DT) 
   VALUES (1010367, 1010363, 1010366, 'N', CURRENT_TIMESTAMP);
 



----------------------------------------------
-- BUSINESSTXREQRESP
----------------------------------------------

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010072, 1010047, null, null, 'I', 1, 'DataSourceID', 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010052, 1010047, null, null, 'I', 5, 'aDWLControl', 2, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010053, 1010047, 'TCRM', 'CTCDataSourceBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');


  
INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010079, 1010074, 'TCRM', 'CTCDataSourceBObj', 'I', null, null, 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010080, 1010074, 'TCRM', 'CTCDataSourceBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');
	    

  
INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010096, 1010091, 'TCRM', 'CTCDataSourceBObj', 'I', null, null, 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010097, 1010091, 'TCRM', 'CTCDataSourceBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');
	    
INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010223, 1010198, null, null, 'I', 1, 'PartySegmentId', 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010203, 1010198, null, null, 'I', 5, 'aDWLControl', 2, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010204, 1010198, 'TCRM', 'CTCPartySegmentBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');


  
INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010230, 1010225, 'TCRM', 'CTCPartySegmentBObj', 'I', null, null, 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010231, 1010225, 'TCRM', 'CTCPartySegmentBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');
	    

  
INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010247, 1010242, 'TCRM', 'CTCPartySegmentBObj', 'I', null, null, 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010248, 1010242, 'TCRM', 'CTCPartySegmentBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');
	    
INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010352, 1010347, null, null, 'I', 5, 'aDWLControl', 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010353, 1010347, 'TCRM', 'CTCDataSourceBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'Y');

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010388, 1010363, null, null, 'I', 6, 'ContId', 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010373, 1010363, null, null, 'I', 6, 'filter', 2, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010368, 1010363, null, null, 'I', 5, 'aDWLControl', 3, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.BUSINESSTXREQRESP (BUSTX_REQRESP_ID, BUSINESS_TX_TP_CD, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010369, 1010363, 'TCRM', 'CTCPartySegmentBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'Y');



----------------------------------------------
-- INTERNALTXREQRESP
----------------------------------------------

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010073, 1010050, null, null, 'I', 1, 'DataSourceID', 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010054, 1010050, null, null, 'I', 5, 'aDWLControl', 2, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010055, 1010050, 'TCRM', 'CTCDataSourceBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');

  
INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010081, 1010077, 'TCRM', 'CTCDataSourceBObj', 'I', null, null, 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010082, 1010077, 'TCRM', 'CTCDataSourceBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');
	    
  
INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010098, 1010094, 'TCRM', 'CTCDataSourceBObj', 'I', null, null, 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010099, 1010094, 'TCRM', 'CTCDataSourceBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');
	    
INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010224, 1010201, null, null, 'I', 1, 'PartySegmentId', 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010205, 1010201, null, null, 'I', 5, 'aDWLControl', 2, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010206, 1010201, 'TCRM', 'CTCPartySegmentBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');

  
INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010232, 1010228, 'TCRM', 'CTCPartySegmentBObj', 'I', null, null, 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010233, 1010228, 'TCRM', 'CTCPartySegmentBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');
	    
  
INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010249, 1010245, 'TCRM', 'CTCPartySegmentBObj', 'I', null, null, 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010250, 1010245, 'TCRM', 'CTCPartySegmentBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'N');
	    
INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010354, 1010350, null, null, 'I', 5, 'aDWLControl', 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010355, 1010350, 'TCRM', 'CTCDataSourceBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'Y');

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010389, 1010366, null, null, 'I', 6, 'ContId', 1, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010375, 1010366, null, null, 'I', 6, 'filter', 2, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010370, 1010366, null, null, 'I', 5, 'aDWLControl', 3, 'cusadmin', CURRENT_TIMESTAMP, null);

INSERT INTO CTCMDM.INTERNALTXREQRESP (INTERNTX_REQRESP_ID, INTERNAL_BUS_TX_TP, APPLICATION, GROUP_NAME, REQ_RESP_IND, TX_PARAM_TP_CD, PARAM_NAME, PARAM_ORDER, LAST_UPDATE_USER, LAST_UPDATE_DT, COLLECTION_IND)
values (1010371, 1010366, 'TCRM', 'CTCPartySegmentBObj', 'O', null, null, null, 'cusadmin', CURRENT_TIMESTAMP, 'Y');


----------------------------------------------
-- GROUPTXMAP
----------------------------------------------

INSERT INTO CTCMDM.GROUPTXMAP (ENTITY_TX_MAP_ID, GROUP_NAME, APPLICATION, BUSINESS_TX_TP_CD, LAST_UPDATE_DT ) 
   VALUES (1010034, 'CTCDataSourceBObj', 'TCRM', 1010047, CURRENT_TIMESTAMP);
 
INSERT INTO CTCMDM.GROUPTXMAP (ENTITY_TX_MAP_ID, GROUP_NAME, APPLICATION, BUSINESS_TX_TP_CD, LAST_UPDATE_DT ) 
   VALUES (1010185, 'CTCPartySegmentBObj', 'TCRM', 1010198, CURRENT_TIMESTAMP);
 


----------------------------------------------
-- TAIL setup
----------------------------------------------



INSERT INTO CTCMDM.ASIDEFINITION (ASI_DEFINITION_ID, ASI_NAME, TRANSFORM_TP_CD, MAPPING_DEFN, ADAPTER_NAME, DESCRIPTION, START_DT, LAST_UPDATE_DT)
	VALUES (1010004, 'http://addition.mdm.ctc.com/CTCAdditions/port', 1, 'com.ibm.mdm.asi.transformation.MDMDefaultWSReqTransformation', 'com.ibm.mdm.asi.MDMOperationalServicesRequestAdapter', 'WS request transform for CTCAdditions', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
	
INSERT INTO CTCMDM.ASIDEFINITION (ASI_DEFINITION_ID, ASI_NAME, TRANSFORM_TP_CD, MAPPING_DEFN, ADAPTER_NAME, DESCRIPTION, START_DT, LAST_UPDATE_DT)
	VALUES (1010005, 'http://addition.mdm.ctc.com/CTCAdditions/port', 2, 'com.ibm.mdm.asi.transformation.MDMDefaultWSRespTransformation', 'com.ibm.mdm.asi.MDMDefaultResponseAdapter', 'WS response transform for CTCAdditions', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
	

-- @SqlSnippetPriority 600

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.




-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCAdditions_SETUP_ZOS.sql
-- 			db2 -vf CTCAdditions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCAdditions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCAdditions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCAdditions_CODETABLES_ZOS.sql
--#SET TERMINATOR ;




-- @SqlSnippetPriority 600

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.




-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_ZOS.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



INSERT INTO CTCMDM.CDCTCLOBSOURCETP ( lang_tp_cd, Lob_Source_Tp_Cd, Name, Admin_Sys_Tp_Cd, Lob_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 1, 'name1', 1, 1 , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCLOBSOURCETP ( lang_tp_cd, Lob_Source_Tp_Cd, Name, Admin_Sys_Tp_Cd, Lob_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 2, 'name2', 1, 1 , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCLOBSOURCETP ( lang_tp_cd, Lob_Source_Tp_Cd, Name, Admin_Sys_Tp_Cd, Lob_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 3, 'name3', 1, 1 , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCLOBSOURCETP ( lang_tp_cd, Lob_Source_Tp_Cd, Name, Admin_Sys_Tp_Cd, Lob_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 4, 'name4', 1, 1 , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCADMINSYSPRIORITYTP ( lang_tp_cd, Admin_Sys_Tp_Cd, name, Priority_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 1, 'name1', NULL , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCADMINSYSPRIORITYTP ( lang_tp_cd, Admin_Sys_Tp_Cd, name, Priority_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 2, 'name2', NULL , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCADMINSYSPRIORITYTP ( lang_tp_cd, Admin_Sys_Tp_Cd, name, Priority_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 3, 'name3', NULL , NULL, CURRENT_TIMESTAMP, 'cusadmin' );

INSERT INTO CTCMDM.CDCTCADMINSYSPRIORITYTP ( lang_tp_cd, Admin_Sys_Tp_Cd, name, Priority_Tp_Cd , description, last_update_dt, last_update_user ) 
   VALUES ( 100, 4, 'name4', NULL , NULL, CURRENT_TIMESTAMP, 'cusadmin' );


-- @SqlSnippetPriority 600

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.




-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCommon_SETUP_ZOS.sql
-- 			db2 -vf CTCCommon_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCCommon_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCCommon_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCommon_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCCommon_CODETABLES_ZOS.sql
--#SET TERMINATOR ;




-- @SqlSnippetPriority 600

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.




-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCComposites_SETUP_ZOS.sql
-- 			db2 -vf CTCComposites_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCComposites_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCComposites_ERRORS_100_DB2.sql
-- 			db2 -vf CTCComposites_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCComposites_CODETABLES_ZOS.sql
--#SET TERMINATOR ;




-- @SqlSnippetPriority 600

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.




-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCExtensions_SETUP_ZOS.sql
-- 			db2 -vf CTCExtensions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCExtensions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCExtensions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCExtensions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCExtensions_CODETABLES_ZOS.sql
--#SET TERMINATOR ;




-- @SqlSnippetPriority 600

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.




-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCExternalRules_SETUP_ZOS.sql
-- 			db2 -vf CTCExternalRules_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCExternalRules_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCExternalRules_ERRORS_100_DB2.sql
-- 			db2 -vf CTCExternalRules_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCExternalRules_CODETABLES_ZOS.sql
--#SET TERMINATOR ;



