
-- @SqlSnippetPriority 100
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			sqlplus userid/password@host @CTCAdditions_SETUP_ORACLE.sql
-- 			sqlplus userid/password@host @CTCAdditions_TRIGGERS_ORACLE.sql
-- 			sqlplus userid/password@host @CTCAdditions_CONSTRAINTS_ORACLE.sql
--			sqlplus userid/password@host @CTCAdditions_ERRORS_100_ORACLE.sql
-- 			sqlplus userid/password@host @CTCAdditions_MetaData_ORACLE.sql
-- 			sqlplus userid/password@host CONFIG_XMLSERVICES_RESPONSE_ORACLE.sql
-- 			sqlplus userid/password@host @CTCAdditions_CODETABLES_ORACLE.sql
  
CREATE TABLE CTCMDM.CTCDATASOURCE (
	data_source_id NUMBER(19, 0)   NOT NULL  , 
	entity_name VARCHAR2(255)    , 
	instance_pk NUMBER(19, 0)   NOT NULL  , 
	attribute_name VARCHAR2(250)    , 
	start_dt TIMESTAMP   NOT NULL  , 
	end_dt TIMESTAMP    , 
	admin_sys_tp_cd NUMBER(19, 0)   NOT NULL  , 
	LAST_UPDATE_DT TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL  , 
	LAST_UPDATE_TX_ID NUMBER(19, 0)    , 
	LAST_UPDATE_USER VARCHAR2(20)    
  );

ALTER TABLE CTCMDM.CTCDATASOURCE
  ADD PRIMARY KEY (
	data_source_id
  );

  
CREATE TABLE CTCMDM.CTCPARTYSEGMENT (
	party_segment_id NUMBER(19, 0)   NOT NULL  , 
	cont_id NUMBER(19, 0)   NOT NULL  , 
	segment_provider VARCHAR2(250)   NOT NULL  , 
	business_group_name VARCHAR2(250)   NOT NULL  , 
	segment_type_name VARCHAR2(250)   NOT NULL  , 
	segment_value VARCHAR2(250)    , 
	segment_category_type VARCHAR2(250)    , 
	effective_start_dt TIMESTAMP   NOT NULL  , 
	effective_end_dt TIMESTAMP    , 
	LAST_UPDATE_DT TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL  , 
	LAST_UPDATE_TX_ID NUMBER(19, 0)    , 
	LAST_UPDATE_USER VARCHAR2(20)    
  );

ALTER TABLE CTCMDM.CTCPARTYSEGMENT
  ADD PRIMARY KEY (
	party_segment_id
  );

  
CREATE TABLE CTCMDM.H_CTCDATASOURCE (
	h_data_source_id NUMBER(19, 0)   NOT NULL,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR2(20)    NOT NULL,
	h_create_dt                                      TIMESTAMP 	     DEFAULT LOCALTIMESTAMP NOT NULL,
	h_end_dt                                         TIMESTAMP,
	data_source_id NUMBER(19, 0)   NOT NULL , 
	entity_name VARCHAR2(255)   , 
	instance_pk NUMBER(19, 0)   NOT NULL , 
	attribute_name VARCHAR2(250)   , 
	start_dt TIMESTAMP   NOT NULL , 
	end_dt TIMESTAMP   , 
	admin_sys_tp_cd NUMBER(19, 0)   NOT NULL , 
	LAST_UPDATE_DT TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL , 
	LAST_UPDATE_TX_ID NUMBER(19, 0)   , 
	LAST_UPDATE_USER VARCHAR2(20)   
  );

ALTER TABLE CTCMDM.H_CTCDATASOURCE
  ADD PRIMARY KEY (
	h_data_source_id,
   	h_create_dt    
 );

  
CREATE TABLE CTCMDM.H_CTCPARTYSEGMENT (
	h_party_segment_id NUMBER(19, 0)   NOT NULL,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR2(20)    NOT NULL,
	h_create_dt                                      TIMESTAMP 	     DEFAULT LOCALTIMESTAMP NOT NULL,
	h_end_dt                                         TIMESTAMP,
	party_segment_id NUMBER(19, 0)   NOT NULL , 
	cont_id NUMBER(19, 0)   NOT NULL , 
	segment_provider VARCHAR2(250)   NOT NULL , 
	business_group_name VARCHAR2(250)   NOT NULL , 
	segment_type_name VARCHAR2(250)   NOT NULL , 
	segment_value VARCHAR2(250)   , 
	segment_category_type VARCHAR2(250)   , 
	effective_start_dt TIMESTAMP   NOT NULL , 
	effective_end_dt TIMESTAMP   , 
	LAST_UPDATE_DT TIMESTAMP  DEFAULT LOCALTIMESTAMP  NOT NULL , 
	LAST_UPDATE_TX_ID NUMBER(19, 0)   , 
	LAST_UPDATE_USER VARCHAR2(20)   
  );

ALTER TABLE CTCMDM.H_CTCPARTYSEGMENT
  ADD PRIMARY KEY (
	h_party_segment_id,
   	h_create_dt    
 );

