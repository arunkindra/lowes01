
-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			sqlplus userid/password@host @CTCAdditions_SETUP_ORACLE.sql
-- 			sqlplus userid/password@host @CTCAdditions_TRIGGERS_ORACLE.sql
-- 			sqlplus userid/password@host @CTCAdditions_CONSTRAINTS_ORACLE.sql
--			sqlplus userid/password@host @CTCAdditions_ERRORS_100_ORACLE.sql
-- 			sqlplus userid/password@host @CTCAdditions_MetaData_ORACLE.sql
-- 			sqlplus userid/password@host CONFIG_XMLSERVICES_RESPONSE_ORACLE.sql
-- 			sqlplus userid/password@host @CTCAdditions_CODETABLES_ORACLE.sql

-- For locale: 100 / default
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010039, 'The following is required: DataSourceID', 'The following is required: DataSourceID', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010043, 'The before image for the following is empty: CTCDataSource.', 'The before image of CTCDataSource is empty.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010060, 'Read of the following record failed: CTCDataSource.', 'An attempt to read the CTCDataSource failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010064, 'The following is incorrect: id is null.', 'The id is null.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010068, 'The format of the following is not correct: inquireAsOfDate.', 'The format of inquireAsOfDate is not correct.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010083, 'Insert of the following failed: CTCDataSource.', 'CTCDataSource insert failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010087, 'Duplicate primary key already exists.', 'Duplicate primary key already exists.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010100, 'Update of the following failed: CTCDataSource.', 'CTCDataSource update failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010114, 'The following is required: InstancePK', 'The following is required: InstancePK', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010136, 'The following is not correct: StartDate', 'StartDate is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010148, 'The following is not correct: EndDate', 'EndDate is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010190, 'The following is required: PartySegmentId', 'The following is required: PartySegmentId', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010194, 'The before image for the following is empty: CTCPartySegment.', 'The before image of CTCPartySegment is empty.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010211, 'Read of the following record failed: CTCPartySegment.', 'An attempt to read the CTCPartySegment failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010215, 'The following is incorrect: id is null.', 'The id is null.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010219, 'The format of the following is not correct: inquireAsOfDate.', 'The format of inquireAsOfDate is not correct.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010234, 'Insert of the following failed: CTCPartySegment.', 'CTCPartySegment insert failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010238, 'Duplicate primary key already exists.', 'Duplicate primary key already exists.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010251, 'Update of the following failed: CTCPartySegment.', 'CTCPartySegment update failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010257, 'The following is required: ContId', 'The following is required: ContId', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010265, 'The following is required: SegmentProvider', 'The following is required: SegmentProvider', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010273, 'The following is required: BusinessGroupName', 'The following is required: BusinessGroupName', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010281, 'The following is required: SegmentTypeName', 'The following is required: SegmentTypeName', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010305, 'The following is required: EffectiveStartDt', 'The following is required: EffectiveStartDt', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010311, 'The following is not correct: EffectiveStartDt', 'EffectiveStartDt is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010323, 'The following is not correct: EffectiveEndDt', 'EffectiveEndDt is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010330, 'The following is required: StartDate', 'The following is required: StartDate', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010337, 'The following is not correct: AdminSystem', 'AdminSystem is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010341, 'The following is required: AdminSystem', 'The following is required: AdminSystem', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010357, 'The following transaction failed: getAllCTCDataSourcesByEntity.', 'The transaction getAllCTCDataSourcesByEntity failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010376, 'Read of the following record failed: CTCPartySegment.', 'An attempt to read the CTCPartySegment failed.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010380, 'The following is incorrect: id is null.', 'The id is null.', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1010384, 'The format of the following is not correct: inquireAsOfDate.', 'The format of inquireAsOfDate is not correct.', CURRENT_TIMESTAMP);
