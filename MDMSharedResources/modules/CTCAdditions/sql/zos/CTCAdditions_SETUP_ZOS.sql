
-- @SqlSnippetPriority 100
-- @SqlModuleOrdering 3

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.

-- MDM_TODO2: CDKWB0055I Replace <DBNAME> with the database name.
-- MDM_TODO2: CDKWB0058I Replace <TABLESPACENAME> with the table space name.
-- MDM_TODO2: CDKWB0056I Replace <H_DBNAME> with the history database name.
-- MDM_TODO2: CDKWB0057I Replace <H_TABLESPACENAME> with the history table space name.

-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCAdditions_SETUP_ZOS.sql
-- 			db2 -vf CTCAdditions_TRIGGERS_ZOS.sql
-- 			db2 -vf CTCAdditions_CONSTRAINTS_ZOS.sql
--			db2 -vf CTCAdditions_ERRORS_100_DB2.sql
-- 			db2 -vf CTCAdditions_MetaData_ZOS.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_ZOS.sql
-- 			db2 -vf CTCAdditions_CODETABLES_ZOS.sql
--#SET TERMINATOR ;


CREATE TABLE CTCMDM.CTCDATASOURCE (  
	data_source_id DECIMAL(19)  NOT NULL  , 
	entity_name VARCHAR(255)   , 
	instance_pk DECIMAL(19)  NOT NULL  , 
	attribute_name VARCHAR(250)   , 
	start_dt TIMESTAMP  NOT NULL  , 
	end_dt TIMESTAMP   , 
	admin_sys_tp_cd DECIMAL(19)  NOT NULL  , 
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT, 
	LAST_UPDATE_TX_ID DECIMAL(19)   , 
	LAST_UPDATE_USER VARCHAR(20)   , 
	CONSTRAINT P_CTCDATASOURCE PRIMARY KEY (
	 data_source_id
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_CTCDATASOURCE
  ON CTCMDM.CTCDATASOURCE (
	 data_source_id
  );


CREATE TABLE CTCMDM.CTCPARTYSEGMENT (  
	party_segment_id DECIMAL(19)  NOT NULL  , 
	cont_id DECIMAL(19)  NOT NULL  , 
	segment_provider VARCHAR(250)  NOT NULL  , 
	business_group_name VARCHAR(250)  NOT NULL  , 
	segment_type_name VARCHAR(250)  NOT NULL  , 
	segment_value VARCHAR(250)   , 
	segment_category_type VARCHAR(250)   , 
	effective_start_dt TIMESTAMP  NOT NULL  , 
	effective_end_dt TIMESTAMP   , 
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT, 
	LAST_UPDATE_TX_ID DECIMAL(19)   , 
	LAST_UPDATE_USER VARCHAR(20)   , 
	CONSTRAINT P_CTCPARTYSEGMENT PRIMARY KEY (
	 party_segment_id
	 )
  ) IN <DBNAME>.<TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_CTCPARTYSEGMENT
  ON CTCMDM.CTCPARTYSEGMENT (
	 party_segment_id
  );


CREATE TABLE CTCMDM.H_CTCDATASOURCE (
	h_data_source_id DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	data_source_id DECIMAL(19)  NOT NULL   ,
	entity_name VARCHAR(255)    ,
	instance_pk DECIMAL(19)  NOT NULL   ,
	attribute_name VARCHAR(250)    ,
	start_dt TIMESTAMP  NOT NULL   ,
	end_dt TIMESTAMP    ,
	admin_sys_tp_cd DECIMAL(19)  NOT NULL   ,
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT ,
	LAST_UPDATE_TX_ID DECIMAL(19)    ,
	LAST_UPDATE_USER VARCHAR(20)    ,
	CONSTRAINT P_H_CTCDATASOURCE PRIMARY KEY (
	 h_data_source_id,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_CTCDATASOURCE
  ON CTCMDM.H_CTCDATASOURCE
  (
	 h_data_source_id,
	 h_create_dt
  );

CREATE TABLE CTCMDM.H_CTCPARTYSEGMENT (
	h_party_segment_id DECIMAL(19)  NOT NULL  ,
	h_action_code                                    CHAR(1)         NOT NULL,
	h_created_by                                     VARCHAR(20)     NOT NULL,
	h_create_dt                                      TIMESTAMP       NOT NULL   DEFAULT,
	h_end_dt                                         TIMESTAMP,
	party_segment_id DECIMAL(19)  NOT NULL   ,
	cont_id DECIMAL(19)  NOT NULL   ,
	segment_provider VARCHAR(250)  NOT NULL   ,
	business_group_name VARCHAR(250)  NOT NULL   ,
	segment_type_name VARCHAR(250)  NOT NULL   ,
	segment_value VARCHAR(250)    ,
	segment_category_type VARCHAR(250)    ,
	effective_start_dt TIMESTAMP  NOT NULL   ,
	effective_end_dt TIMESTAMP    ,
	LAST_UPDATE_DT TIMESTAMP  NOT NULL   DEFAULT ,
	LAST_UPDATE_TX_ID DECIMAL(19)    ,
	LAST_UPDATE_USER VARCHAR(20)    ,
	CONSTRAINT P_H_CTCPARTYSEGMENT PRIMARY KEY (
	 h_party_segment_id,
	 h_create_dt
	 )
  ) IN <H_DBNAME>.<H_TABLESPACENAME>;

CREATE UNIQUE INDEX  U1_H_CTCPARTYSEGMENT
  ON CTCMDM.H_CTCPARTYSEGMENT
  (
	 h_party_segment_id,
	 h_create_dt
  );


