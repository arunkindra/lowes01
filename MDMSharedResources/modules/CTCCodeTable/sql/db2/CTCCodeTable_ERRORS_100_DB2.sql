
-- @SqlSnippetPriority 350
-- @SqlModuleOrdering 2

-- The following source code ("Code") may only be used in accordance with the terms
-- and conditions of the license agreement you have with IBM Corporation. The Code 
-- is provided to you on an "AS IS" basis, without warranty of any kind.  
-- SUBJECT TO ANY STATUTORY WARRANTIES WHICH CAN NOT BE EXCLUDED, IBM MAKES NO 
-- WARRANTIES OR CONDITIONS EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
-- TO, THE IMPLIED WARRANTIES OR CONDITIONS OF MERCHANTABILITY, FITNESS FOR A 
-- PARTICULAR PURPOSE, AND NON-INFRINGEMENT, REGARDING THE CODE. IN NO EVENT WILL 
-- IBM BE LIABLE TO YOU OR ANY PARTY FOR ANY DIRECT, INDIRECT, SPECIAL OR OTHER 
-- CONSEQUENTIAL DAMAGES FOR ANY USE OF THE CODE, INCLUDING, WITHOUT LIMITATION, 
-- LOSS OF, OR DAMAGE TO, DATA, OR LOST PROFITS, BUSINESS, REVENUE, GOODWILL, OR 
-- ANTICIPATED SAVINGS, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH 
-- DAMAGES. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF 
-- INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY 
-- NOT APPLY TO YOU.



-- Notes
-- MDM_TODO: CDKWB0046I Statements are placed in the generated SQL file when user changes are required.
-- 1. Edit the following SQL files following any associated instructions.
-- 2. Connect to the database.
-- 3. Run each SQL file as shown below and in the same order.
-- 			db2 -vf CTCCodeTable_SETUP_DB2.sql
-- 			db2 -vf CTCCodeTable_TRIGGERS_DB2.sql
-- 			db2 -vf CTCCodeTable_CONSTRAINTS_DB2.sql
--			db2 -vf CTCCodeTable_ERRORS_100_DB2.sql
-- 			db2 -vf CTCCodeTable_MetaData_DB2.sql
-- 			db2 -vf CONFIG_XMLSERVICES_RESPONSE_DB2.sql
-- 			db2 -vf CTCCodeTable_CODETABLES_DB2.sql
	
--#SET TERMINATOR ;


-- For locale: 100 / default
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020050, 'The following is required: LobSourceTpCd', 'The following is required: LobSourceTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020058, 'The following is required: Name', 'The following is required: Name', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020065, 'The following is not correct: AdminSystemTpCd', 'AdminSystemTpCd is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020069, 'The following is required: AdminSystemTpCd', 'The following is required: AdminSystemTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020078, 'The following is not correct: LobTpCd', 'LobTpCd is not correct', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020082, 'The following is required: LobTpCd', 'The following is required: LobTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020129, 'The following is required: AdminSysTpCd', 'The following is required: AdminSysTpCd', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020137, 'The following is required: name', 'The following is required: name', CURRENT_TIMESTAMP);
INSERT INTO CTCMDM.CDERRMESSAGETP (LANG_TP_CD, ERR_MESSAGE_TP_CD, ERR_MESSAGE, COMMENTS, LAST_UPDATE_DT) 
   VALUES (100, 1020149, 'The following is required: PriorityTpCd', 'The following is required: PriorityTpCd', CURRENT_TIMESTAMP);
