package com.ctc.mdm.search.component;

import com.dwl.base.DWLHistoryInquiryCommon;
import com.dwl.tcrm.common.GenericResultSetProcessor;
import com.dwl.tcrm.financial.component.TCRMContractBObj;
import com.dwl.tcrm.financial.entityObject.EObjContract;
import java.sql.ResultSet;
import java.util.*;

 public class CTCContractSearchResultProcessor extends GenericResultSetProcessor
{
    private static final String CONTRACTID = "CONTRACTID30";
	private static final String ENDDT = "ENDDT30";
    private static final String TERMINATIONDT = "TERMINATIONDT30";
   
   
    /**
     * Constructor
     */
    public CTCContractSearchResultProcessor()
    {
    }

    /* (non-Javadoc)
     * @see com.dwl.tcrm.common.GenericResultSetProcessor#getObjectFromResultSet(java.sql.ResultSet)
     */
    public Vector getObjectFromResultSet(ResultSet rs)
        throws Exception
    {
        TCRMContractBObj contract = null;
        Vector vector = new Vector();
        HashMap columnInfo = getColumnInfo(rs);
        for(; rs.next(); vector.addElement(contract))
        {
            EObjContract eobjcontract = new EObjContract();
            if(columnInfo.containsKey(CONTRACTID))
            {
                long contractIdPk = rs.getLong(CONTRACTID);
                if(rs.wasNull())
                {
                    eobjcontract.setContractIdPK(null);
                } else
                {
                    eobjcontract.setContractIdPK(new Long(contractIdPk));
                }
            }
            
            if(columnInfo.containsKey(ENDDT))
            {
                eobjcontract.setEndDate(rs.getTimestamp(ENDDT));
            }
            
            if(columnInfo.containsKey(TERMINATIONDT))
            {
                eobjcontract.setTerminationDate(rs.getTimestamp(TERMINATIONDT));
            }

            eobjcontract = (EObjContract)DWLHistoryInquiryCommon.getHistoryData(eobjcontract, rs);
            contract = (TCRMContractBObj)super.createBObj(TCRMContractBObj.class);
            contract.setEObjContract(eobjcontract);
        }

        return vector;
    }

    
    /* (non-Javadoc)
     * @see com.dwl.base.interfaces.IGenericResultSetProcessor#createObject(java.lang.Object)
     */
    public Object createObject(Object eObj)
        throws Exception
    {
        TCRMContractBObj contract = (TCRMContractBObj)super.createBObj(TCRMContractBObj.class);
        contract.setEObjContract((EObjContract)eObj);
        return contract;
    }
}
