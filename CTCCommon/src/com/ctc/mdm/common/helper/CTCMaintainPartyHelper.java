package com.ctc.mdm.common.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.ctc.mdm.addition.component.CTCDataSourceBObj;
import com.ctc.mdm.addition.constant.CTCAdditionsPropertyKeys;
import com.ctc.mdm.addition.interfaces.CTCDataSource;
import com.ctc.mdm.common.constants.CTCConstants;
import com.ctc.mdm.common.constants.CTCErrorConstants;
import com.ctc.mdm.common.util.CTCCommonUtil;
import com.ctc.mdm.extension.component.CTCPartyAddressBObjExt;
import com.ctc.mdm.extension.component.CTCPartyContactMethodBObjExt;
import com.ctc.mdm.extension.component.CTCPersonNameBObjExt;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.IDWLErrorMessage;
import com.dwl.base.error.DWLError;
import com.dwl.base.error.DWLErrorCode;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.base.util.DWLDateTimeUtilities;
import com.dwl.tcrm.common.TCRMRecordFilter;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyLobRelationshipBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonBObj;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.component.TCRMPhoneNumberBObj;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;


public class CTCMaintainPartyHelper {
	DWLControl objDWLControl=null;
	protected final IDWLLogger logger = DWLLoggerManager.getLogger(CTCMaintainPartyHelper.class);
		
	/**
	 * This handleAddCustomer Method used to Construct Request Party Objects
	 * 
	 * @param reqTCRMPartyBObj
	 * @throws Exception
	 */
	public TCRMPartyBObj handleAddCustomer(TCRMPartyBObj reqTCRMPartyBObj)throws Exception {
		long startTime = System.currentTimeMillis();
		logger.info("CTCMaintainPartyHelper:handleAddCustomer - " + CTCConstants.LOG_ENTRY_OF_METHOD);
		if(reqTCRMPartyBObj != null) {
			reqTCRMPartyBObj.setMandatorySearchDone(CTCConstants.MANDATORY_SEARCH_DONE);
			constructTCRMPersonName(reqTCRMPartyBObj);
			constructTCRMPartyAddress(reqTCRMPartyBObj);
			constructTCRMPartyContactMethod(reqTCRMPartyBObj);
			constructPartyLOBRelationship(reqTCRMPartyBObj,reqTCRMPartyBObj.getControl());
		}
		logger.info("CTCMaintainPartyHelper:handleAddCustomer - " + CTCConstants.LOG_EXIT_OF_METHOD);
		long endTime = System.currentTimeMillis();
		logger.info(CTCConstants.TIME_TAKEN + "CTCMaintainPartyHelper:handleAddCustomer - " + (endTime-startTime));
		return reqTCRMPartyBObj;
	}
	/**
	 * This Method used to Construct PersonName Objects.
	 * In the scenario if NameUsageType or NameUsageValue is Null in the request then 
	 * 		if index[0] set Primary for object else set Alternate for all other index Objects.
	 * 
	 * @param reqTCRMPartyBObj request Party object
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void constructTCRMPersonName(TCRMPartyBObj reqTCRMPartyBObj)throws Exception {
		logger.info("CTCMaintainPartyHelper:constructTCRMPersonName - " + CTCConstants.LOG_ENTRY_OF_METHOD);
		try {
			TCRMPersonBObj reqTCRMPersonBObj = (TCRMPersonBObj)reqTCRMPartyBObj;
			if(reqTCRMPersonBObj != null) {
				TCRMPersonNameBObj reqTCRMPersonNameBObj;
				//New vector for the extension objects.
				Vector vecCTCPersonNameBObjExt = new Vector();
				Vector reqVecTCRMPersonNameBObj = reqTCRMPersonBObj.getItemsTCRMPersonNameBObj();
				if(CTCCommonUtil.isNotEmpty(reqVecTCRMPersonNameBObj)) {
					int lenPersonName = reqVecTCRMPersonNameBObj.size();
					for(int reqPersonNameIndex = 0;reqPersonNameIndex < lenPersonName;reqPersonNameIndex++) {
						reqTCRMPersonNameBObj = (TCRMPersonNameBObj)reqVecTCRMPersonNameBObj.get(reqPersonNameIndex);
						if(reqTCRMPersonNameBObj != null && StringUtils.isBlank(reqTCRMPersonNameBObj.getEndDate())&& 
								StringUtils.isBlank(reqTCRMPersonNameBObj.getNameUsageType())&& 
								StringUtils.isBlank(reqTCRMPersonNameBObj.getNameUsageValue())) {
							if(reqPersonNameIndex == 0) {
								//Setting name usage type as Primary (1). 
								reqTCRMPersonNameBObj.setNameUsageType(CTCCommonUtil.getProperty(
										CTCConstants.PERSONNAME_PRIMARY_USAGE_TYPE));
							}else {
								//Setting name usage type as Alternate (2).
								reqTCRMPersonNameBObj.setNameUsageType(CTCCommonUtil.getProperty(
										CTCConstants.PERSONNAME_ALTERNATE_USAGE_TYPE));
							}	
						}
						// Get the cloned extension object.
						CTCPersonNameBObjExt objCTCPersonNameBObjExt = getCTCPersonNameBObjExt(reqTCRMPersonNameBObj);
						//Request system name form the request party object.
						String strRequestName = getClientSystemName(reqTCRMPartyBObj);
						//New CTC Data source based on request system name.
						objCTCPersonNameBObjExt.setCTCDataSourceBObj(
								getCTCDataSourceBObj(strRequestName, reqTCRMPartyBObj.getControl()));
						vecCTCPersonNameBObjExt.add(objCTCPersonNameBObjExt);
					}
					//Clear and add all the extension objects to the request party.
					if(CTCCommonUtil.isNotEmpty(vecCTCPersonNameBObjExt)){
						reqTCRMPersonBObj.getItemsTCRMPersonNameBObj().clear();
						reqTCRMPersonBObj.getItemsTCRMPersonNameBObj().addAll(vecCTCPersonNameBObjExt);
					}
				}
			}
		}
		catch(Exception ex){
			logger.error("CTCMaintainPartyHelper:constructTCRMPersonName - " + ex.getMessage());
			throw ex;
		}
		logger.info("CTCMaintainPartyHelper:constructTCRMPersonName - " + CTCConstants.LOG_EXIT_OF_METHOD);
  }

	/**
	 * This method is used to clone the TCRMPersonNameBObj and create the CTCPersonNameBObjExt object.
	 * 
	 * @param reqTCRMPersonNameBObj TCRMPersonNameBObj
	 * @return CTCPersonNameBObjExt
	 * @throws Exception
	 */
	private CTCPersonNameBObjExt getCTCPersonNameBObjExt(TCRMPersonNameBObj reqTCRMPersonNameBObj) throws Exception {
		CTCPersonNameBObjExt objCTCPersonNameBObjExt = (CTCPersonNameBObjExt)
				 TCRMClassFactory.createBObj(CTCPersonNameBObjExt.class);
		//Shallow clone the request person name object.
		objCTCPersonNameBObjExt.shallowClone(reqTCRMPersonNameBObj, true, true,true);
		objCTCPersonNameBObjExt.setControl(reqTCRMPersonNameBObj.getControl());
		return objCTCPersonNameBObjExt;
	}
	
  /**
   * This Method used to Construct PartyAddress Objects.
   * In the scenario if AddressUsageType or AddressUsageValue is Null in the request then 
   *	if index[0] set as Primary else set as Alternate for all other index Objects.
   *
   * @param reqTCRMPartyBObj request Party object
   * @throws Exception
   */
	@SuppressWarnings("unchecked")
	private void constructTCRMPartyAddress(TCRMPartyBObj reqTCRMPartyBObj)throws Exception{
	  logger.info("CTCMaintainPartyHelper:constructTCRMPersonAddress - " + CTCConstants.LOG_ENTRY_OF_METHOD);
	  try{
		if(reqTCRMPartyBObj != null){
			Vector reqVecTCRMPartyAddressBObj = reqTCRMPartyBObj.getItemsTCRMPartyAddressBObj();
			if(CTCCommonUtil.isNotEmpty(reqVecTCRMPartyAddressBObj)) {
				//New vector for the extension objects.
				Vector vecCTCPartyAddressBObjExt = new Vector();
				int lenPersonAddress = reqVecTCRMPartyAddressBObj.size();
				TCRMPartyAddressBObj reqTCRMPartyAddressBObj;
				for(int reqPartyAddressIndex = 0;reqPartyAddressIndex < lenPersonAddress; reqPartyAddressIndex++) {  
					reqTCRMPartyAddressBObj = (TCRMPartyAddressBObj)reqVecTCRMPartyAddressBObj.get(reqPartyAddressIndex);
					if(reqTCRMPartyAddressBObj != null && StringUtils.isBlank(reqTCRMPartyAddressBObj.getEndDate()) &&
							StringUtils.isBlank(reqTCRMPartyAddressBObj.getAddressUsageType())&&
							StringUtils.isBlank(reqTCRMPartyAddressBObj.getAddressUsageValue())) {
						if(reqPartyAddressIndex == 0) {
							//Setting Address usage type as Primary (1).
							reqTCRMPartyAddressBObj.setAddressUsageType(CTCCommonUtil.getProperty(
									CTCConstants.ADDRESS_PRIMARY_USAGE_TYPE));
						}else {
							//Setting Address usage type as Alternate (2).
							reqTCRMPartyAddressBObj.setAddressUsageType(CTCCommonUtil.getProperty(
									CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE));
						}
					}
					//Get the cloned extension object.
					CTCPartyAddressBObjExt objCTCPartyAddressBObjExt =  getCTCPartyAddressBObjExt(reqTCRMPartyAddressBObj);
					//Request system name form the request party object.
					String strRequestName = getClientSystemName(reqTCRMPartyBObj);
					//New CTC Data source based on request system name.
					objCTCPartyAddressBObjExt.setCTCDataSourceBObj(
							getCTCDataSourceBObj(strRequestName, reqTCRMPartyBObj.getControl()));
					vecCTCPartyAddressBObjExt.add(objCTCPartyAddressBObjExt);
				}
				//Clear and add all the extension objects to the request party.
				if(CTCCommonUtil.isNotEmpty(vecCTCPartyAddressBObjExt)){
					reqTCRMPartyBObj.getItemsTCRMPartyAddressBObj().clear();
					reqTCRMPartyBObj.getItemsTCRMPartyAddressBObj().addAll(vecCTCPartyAddressBObjExt);
				}
			}
		}
	}
	catch(Exception ex) {
		logger.error("CTCMaintainPartyHelper:constructTCRMPersonAddress - " + ex.getMessage());
		throw ex;
	}
	logger.info("CTCMaintainPartyHelper:constructTCRMPersonAddress - " + CTCConstants.LOG_EXIT_OF_METHOD);
  }

	/**
	 * This method is used to clone the TCRMPartyAddressBObj and create the CTCPartyAddressBObjExt object.
	 * 
	 * @param reqTCRMPartyAddressBObj TCRMPartyAddressBObj
	 * @return CTCPartyAddressBObjExt
	 * @throws Exception
	 */
	private CTCPartyAddressBObjExt getCTCPartyAddressBObjExt(TCRMPartyAddressBObj reqTCRMPartyAddressBObj) throws Exception {
		CTCPartyAddressBObjExt objCTCPartyAddressBObjExt = (CTCPartyAddressBObjExt) TCRMClassFactory.createBObj(
				CTCPartyAddressBObjExt.class);
		//Shallow clone the request Party Address object.
		objCTCPartyAddressBObjExt.shallowClone(reqTCRMPartyAddressBObj, true, true,true);
		objCTCPartyAddressBObjExt.setTCRMAddressBObj(reqTCRMPartyAddressBObj.getTCRMAddressBObj());
		objCTCPartyAddressBObjExt.setControl(reqTCRMPartyAddressBObj.getControl());
		return objCTCPartyAddressBObjExt;
	}
	
	/**
	 * This Method used to Construct PartyContactMethod Objects.
	 *  
	 * @param reqTCRMPartyBObj
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void constructTCRMPartyContactMethod(TCRMPartyBObj reqTCRMPartyBObj) throws Exception{
	  logger.info("CTCMaintainPartyHelper:constructTCRMPartyContactMethod - " + CTCConstants.LOG_ENTRY_OF_METHOD);
	  Vector reqVecTCRMPartyContactMethodBObj = reqTCRMPartyBObj.getItemsTCRMPartyContactMethodBObj();
	  if(CTCCommonUtil.isNotEmpty(reqVecTCRMPartyContactMethodBObj)) {
		  Vector objVecTCRMPartyContactMethodBObj = new Vector();
		  int lenContactMethod = reqVecTCRMPartyContactMethodBObj.size();
		  for(int reqContactMethodIndex =0; reqContactMethodIndex < lenContactMethod; reqContactMethodIndex++) {
			TCRMPartyContactMethodBObj reqTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)
					reqVecTCRMPartyContactMethodBObj.get(reqContactMethodIndex);
			TCRMContactMethodBObj reqTCRMContactMethodBObj= reqTCRMPartyContactMethodBObj.getTCRMContactMethodBObj();
			if (reqTCRMPartyContactMethodBObj!= null && CTCConstants.CONT_METH_TP_EMAIL.equals(
					reqTCRMContactMethodBObj.getContactMethodType())){
				//Get the cloned extension object.
				CTCPartyContactMethodBObjExt objCTCPartyContactMethodBObjExt = getCTCPartyContactMethodBObjExt(
						reqTCRMPartyContactMethodBObj);
				//Request system name form the request party object.
				String strRequestSystem = getClientSystemName(reqTCRMPartyBObj);
				//New CTC Data source based on request system name.
				objCTCPartyContactMethodBObjExt.setCTCDataSourceBObj(
						getCTCDataSourceBObj(strRequestSystem,
								reqTCRMPartyBObj.getControl()));
				objVecTCRMPartyContactMethodBObj.add(objCTCPartyContactMethodBObjExt);
			}else{
				//Added the Party Contact Method if not e-mail
				objVecTCRMPartyContactMethodBObj.add(reqTCRMPartyContactMethodBObj);
			}
		  }
		  //Clear and add all the extension objects to the request party.
		  if(CTCCommonUtil.isNotEmpty(objVecTCRMPartyContactMethodBObj)){
			  reqTCRMPartyBObj.getItemsTCRMPartyContactMethodBObj().clear();
			  reqTCRMPartyBObj.getItemsTCRMPartyContactMethodBObj().addAll(objVecTCRMPartyContactMethodBObj);
		  }
	  }
	  logger.info("CTCMaintainPartyHelper:constructTCRMPartyContactMethod -" + CTCConstants.LOG_EXIT_OF_METHOD);
	}
	
	/**
	 * This method is used to clone the TCRMPartyAddressBObj and create the CTCPartyAddressBObjExt object.
	 * 
	 * @param reqTCRMPartyAddressBObj TCRMPartyAddressBObj
	 * @return CTCPartyAddressBObjExt
	 * @throws Exception
	 */
	private CTCPartyContactMethodBObjExt getCTCPartyContactMethodBObjExt(
			TCRMPartyContactMethodBObj reqTCRMPartyContactMethodBObj) throws Exception {
		CTCPartyContactMethodBObjExt objCTCPartyContactMethodBObjExt = (CTCPartyContactMethodBObjExt) TCRMClassFactory.createBObj(
				CTCPartyContactMethodBObjExt.class);
		//Shallow clone the request Party Contact Method object
		objCTCPartyContactMethodBObjExt.shallowClone(reqTCRMPartyContactMethodBObj, true, true,true);
		objCTCPartyContactMethodBObjExt.setTCRMContactMethodBObj(reqTCRMPartyContactMethodBObj.getTCRMContactMethodBObj());
		objCTCPartyContactMethodBObjExt.setControl(reqTCRMPartyContactMethodBObj.getControl());
		return objCTCPartyContactMethodBObjExt;
	}
	
	/**
	 * This method is used to create the CTCDataSourceBObj with the admin sys type & value and the start date.
	 * 
	 * @param adminSysValue Request System 
	 * @param dwlControl DWLControl
	 * @return CTCDataSourceBObj
	 * @throws Exception
	 */
	private CTCDataSourceBObj getCTCDataSourceBObj(String adminSysValue, DWLControl dwlControl) throws Exception{
		CTCDataSourceBObj sourceBObj = (CTCDataSourceBObj)TCRMClassFactory.createBObj(CTCDataSourceBObj.class);
		sourceBObj.setAdminSystemValue(adminSysValue);
		sourceBObj.setAdminSystemType(CTCCommonUtil.getCodeType(adminSysValue, CTCConstants.CD_ADMIN_SYS_TP, dwlControl));
		sourceBObj.setStartDate(DWLDateTimeUtilities.getCurrentSystemTime());
		sourceBObj.setControl(dwlControl);
		return sourceBObj;
	}

	/**
	 * This Method used to Construct update party Object.
	 *  if isUpdateParty is not passed then defaulted to false.
	 * 
	 * @param reqTCRMPartyBObj request party object
	 * @param dbTCRMPartyBObj DB  party object
	 * @return
	 * @throws Exception
	 */
	public TCRMPartyBObj handleUpdateCustomer(TCRMPartyBObj reqTCRMPartyBObj,TCRMPartyBObj dbTCRMPartyBObj)throws Exception{
		return handleUpdateCustomer(reqTCRMPartyBObj, dbTCRMPartyBObj, false);
	}
	
  /**
   * This Method used to Construct update party Object.
   *   if isUpdateParty is true construct methods will be invoked.
   *   
   *   construct methods are invoked to construct the DSI objects based on the input system and 
   *   attached to the corresponding extension objects.
   *   
   *   handle methods are invoked to set the ID PKs ,last update date and business logic for 
   *   corresponding objects.
   * 
   * @param reqTCRMPartyBObj - request party object
   * @param dbTCRMPartyBObj - DB party object
   * @throws Exception
   */
	public TCRMPartyBObj handleUpdateCustomer(TCRMPartyBObj reqTCRMPartyBObj,
			TCRMPartyBObj dbTCRMPartyBObj, boolean isUpdateParty)throws Exception
	{
	  logger.info("CTCMaintainPartyHelper:handleUpdateCustomer-" + CTCConstants.LOG_ENTRY_OF_METHOD);
	  try {
		  if(reqTCRMPartyBObj!=null && dbTCRMPartyBObj!=null)
		  {
			  //Setting customer's MDM party Id and last updated date. 
			  reqTCRMPartyBObj.setPartyId(dbTCRMPartyBObj.getPartyId());
			  reqTCRMPartyBObj.setPartyLastUpdateDate(dbTCRMPartyBObj.getPartyLastUpdateDate());

			  if(reqTCRMPartyBObj instanceof TCRMPersonBObj) {//if request is person 
				  TCRMPersonBObj reqTCRMPersonBObj=(TCRMPersonBObj)reqTCRMPartyBObj;
				  reqTCRMPersonBObj.setPersonPartyId(((TCRMPersonBObj)dbTCRMPartyBObj).getPersonPartyId());
				  reqTCRMPersonBObj.setPersonLastUpdateDate(((TCRMPersonBObj)dbTCRMPartyBObj).getPersonLastUpdateDate());
				  if(isUpdateParty){ // If update party, constructing extension object.
					  constructTCRMPersonName(reqTCRMPartyBObj);
				  }
				  handleTCRMPersonName(reqTCRMPartyBObj,dbTCRMPartyBObj);
			  }
			  if(isUpdateParty) { // If update party, constructing extension objects.
				  constructTCRMPartyAddress(reqTCRMPartyBObj);
				  constructTCRMPartyContactMethod(reqTCRMPartyBObj);
				  constructPartyLOBRelationship(reqTCRMPartyBObj,reqTCRMPartyBObj.getControl());
			  }
			  handleTCRMPartyAddress(reqTCRMPartyBObj,dbTCRMPartyBObj);
			  handleContactMethod(reqTCRMPartyBObj, dbTCRMPartyBObj);
			  handlePartyLOBRelationship(reqTCRMPartyBObj.getItemsTCRMPartyLobRelationshipBObj(),
					  dbTCRMPartyBObj.getItemsTCRMPartyLobRelationshipBObj());
		  } 
	  	}catch(Exception ex){
	  		logger.error("CTCMaintainPartyHelper:handleUpdateCustomer-"+ex);
	  		throw ex;
	  	}
	  	return reqTCRMPartyBObj;
  }
  
  /**
   * This method is used to handle the TCRM Party Address objects.
   *    1. Setting ID PKs and last updated date.
   *    2. Creating the CTC Data source vector for all the DB Party address objects
   *    3. Standardize the request Address.
   *    4. Full address(AddressLineOne, AddressLineTwo, City, ProvinceState , ZipPostalCode and Country)
   *          exactly matching the incoming Address�s standardized full address.
   *    5. If end-date passed, inactive the corresponding request system DSI.
   *    6. If the matched Address is already actively associated, clear the request DSI object.
   *    7. If the found Party Address record is an Address Usage Type of �Alternate� and 
   *			the incoming Address is the billing address in TSYS or 
   *		if Party does not have a relationship with the Credit Card LOB
   *		  then 
   *			find the DB Primary Address Object and update to Alternate. 
   *			update the request object to Primary.
   *		  else
   *			No address usage type change. Perform redundant update.
   *	8. Process non-match list.
   *		If end-date passed, inactive the corresponding request system DSI.
   *		If the incoming Address is the billing address in TSYS or 
   *			if Party does not have a relationship with the Credit Card LOB
   *		and if the request address is primary
   *		then 
   *			find the DB Primary Address Object and update to Alternate. 
   *			update the request object to Primary.
   *		else
   *			Add a new Address record with Address Usage Type of Alternate
   * 
   * @param reqTCRMPartyBObj request party 
   * @param dbTCRMPartyBObj DB party
   * @throws Exception
   */
	@SuppressWarnings("unchecked")
	private void handleTCRMPartyAddress(TCRMPartyBObj reqTCRMPartyBObj,TCRMPartyBObj dbTCRMPartyBObj) throws Exception{
	  logger.info("CTCMaintainPartyHelper:handleTCRMPartyAddress-"+CTCConstants.LOG_ENTRY_OF_METHOD);
	  try {
		 Vector reqVecTCRMPartyAddressBObj=reqTCRMPartyBObj.getItemsTCRMPartyAddressBObj();
		 if (CTCCommonUtil.isNotEmpty(reqVecTCRMPartyAddressBObj)) {
			 //New Vector for adding the modified DB objects. 
			 Vector vecModifiedDBPartyAddressBObj = new Vector();
			 Vector dbVecTCRMPartyAddressBObj = dbTCRMPartyBObj.getItemsTCRMPartyAddressBObj();
			 //Map to store the modified objects and re-used for any duplicate update
			 HashMap mapCTCPartyAddressBObjExt = new HashMap();
			 //Getting all the CTC Data source vector for all the DB Party address objects.
			 HashMap dbMapCTCDataSourceBObj = getAddressCTCDataSourceBObjs(dbVecTCRMPartyAddressBObj);
			 //Getting the request system name
			 String strClientSystemName = getClientSystemName(reqTCRMPartyBObj);
			 Iterator itrReqVecTCRMPartyAddressBObj = reqVecTCRMPartyAddressBObj.iterator();
			 
			 Vector matchedAddressVec = new Vector();
			 
			 while(itrReqVecTCRMPartyAddressBObj.hasNext()) {
				 TCRMPartyAddressBObj reqTCRMPartyAddressBObj=(TCRMPartyAddressBObj)itrReqVecTCRMPartyAddressBObj.next();
				 CTCPartyAddressBObjExt reqPartyAddressBObjExt = null;
				 if(reqTCRMPartyAddressBObj instanceof CTCPartyAddressBObjExt){
					 reqPartyAddressBObjExt = (CTCPartyAddressBObjExt)reqTCRMPartyAddressBObj;
				 }
				 if(reqTCRMPartyAddressBObj != null) {
					 TCRMAddressBObj reqTCRMAddressBObj=(TCRMAddressBObj)reqTCRMPartyAddressBObj.getTCRMAddressBObj();
					 //Standardize the request Address
					 TCRMAddressBObj reqStdTCRMAddressBObj = CTCCommonUtil.standardizeAddress(reqTCRMAddressBObj);
					 CTCCommonUtil.setAddressFieldsValueToType(reqStdTCRMAddressBObj);
					 if (CTCCommonUtil.isNotEmpty(dbVecTCRMPartyAddressBObj)) {
						Iterator itrDBVecTCRMPartyAddressBObj = dbVecTCRMPartyAddressBObj.iterator();
						while(itrDBVecTCRMPartyAddressBObj.hasNext()) {
							 TCRMPartyAddressBObj dbTCRMPartyAddressBObj=(TCRMPartyAddressBObj)itrDBVecTCRMPartyAddressBObj.next();
							 if(dbTCRMPartyAddressBObj!= null) {
								 TCRMAddressBObj dbTCRMAddressBObj = dbTCRMPartyAddressBObj.getTCRMAddressBObj();
								 //Standardized full address exactly matching the incoming Address�s standardized full address
								 if(CTCCommonUtil.isAddressMatch(reqStdTCRMAddressBObj,dbTCRMAddressBObj) 
										 && (!matchedAddressVec.contains(dbTCRMPartyAddressBObj))) {
									 
									 /*
									  * Add to the matchedAddressVec list.This should not be used for comparing for the susbsequent PartyAddresses.
									  * Or else same PartyAddress will run into multiple updates with updateParty transaction, resulting in 
									  * LastUpdateDate non-match error
									  */
									 matchedAddressVec.add(dbTCRMPartyAddressBObj);
									 
									 //setting ID PKs and Last updated date.
								 	reqTCRMPartyAddressBObj.setPartyAddressIdPK(dbTCRMPartyAddressBObj.getPartyAddressIdPK());
								 	reqTCRMPartyAddressBObj.setPartyId(dbTCRMPartyAddressBObj.getPartyId());
								 	reqTCRMPartyAddressBObj.setAddressId(dbTCRMPartyAddressBObj.getAddressId());
								 	reqTCRMPartyAddressBObj.setAddressGroupLastUpdateDate(
								 			dbTCRMPartyAddressBObj.getAddressGroupLastUpdateDate());
								 	reqTCRMPartyAddressBObj.setLocationGroupLastUpdateDate(
								 			dbTCRMPartyAddressBObj.getLocationGroupLastUpdateDate());
									 reqStdTCRMAddressBObj.setAddressIdPK(dbTCRMAddressBObj.getAddressIdPK());
									 reqStdTCRMAddressBObj.setAddressLastUpdateDate(
											 dbTCRMAddressBObj.getAddressLastUpdateDate());
									 //Putting in the map to re-use if any update on the same object.
									 mapCTCPartyAddressBObjExt.put(dbTCRMPartyAddressBObj.getPartyAddressIdPK(), reqTCRMPartyAddressBObj);
									 //Getting corresponding DSI Vector from the map for the matched Address
									 Vector<CTCDataSourceBObj> dbVecCTCDataSourceBObj = (Vector<CTCDataSourceBObj>)dbMapCTCDataSourceBObj.get(
											  dbTCRMPartyAddressBObj.getPartyAddressIdPK());
									 CTCDataSourceBObj dbCTCDataSourceBObj = null;
									 if (CTCCommonUtil.isNotEmpty(dbVecCTCDataSourceBObj)) {
										 //Getting the request system DSI  
											dbCTCDataSourceBObj = getRequestSystemCTCDataSourceBObj(dbVecCTCDataSourceBObj, strClientSystemName);
									 }
									 if (reqTCRMPartyAddressBObj.getEndDate() != null){
										 //If end-date passed, inactive the corresponding request system DSI. 
										 reqTCRMPartyAddressBObj.setEndDate(null);
										 reqTCRMPartyAddressBObj.setAddressUsageValue(null);
										 reqTCRMPartyAddressBObj.setAddressUsageType(dbTCRMPartyAddressBObj.getAddressUsageType());
										 
											if(dbCTCDataSourceBObj != null && reqPartyAddressBObjExt != null){
												reqPartyAddressBObjExt.getItemsCTCDataSourceBObj().clear();
												dbCTCDataSourceBObj.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
												reqPartyAddressBObjExt.getItemsCTCDataSourceBObj().add(dbCTCDataSourceBObj);
											}else if(reqPartyAddressBObjExt != null){
												reqPartyAddressBObjExt.getItemsCTCDataSourceBObj().clear();
											}
									 }else {//If the matched Address is already actively associated DSI, clear the request DSI object.
										if(reqPartyAddressBObjExt!= null && dbCTCDataSourceBObj != null) {
											reqPartyAddressBObjExt.getItemsCTCDataSourceBObj().clear();
											reqPartyAddressBObjExt.getItemsCTCDataSourceBObj().add(dbCTCDataSourceBObj);
										}
										//If the found Party Address record has an Address Usage Type of �Alternate� and 
										//the incoming Address is the billing address in TSYS or 
										//if Party does not have a relationship with the Credit Card LOB
										//   then 
										//		find the DB Primary Address Object and update to Alternate. 
										//      update the request object to Primary.
										//else
										//		No address usage type change. 
										 if(CTCCommonUtil.getProperty(CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE).equals(
												 dbTCRMPartyAddressBObj.getAddressUsageType())
												 && (CTCCommonUtil.getProperty(CTCConstants.TSYS_CLIENT_SYSTEM_NAME).equalsIgnoreCase(strClientSystemName)
														 ||(!hasCreditCardLOBRelationship(dbTCRMPartyBObj.getItemsTCRMPartyLobRelationshipBObj())))
												 && CTCCommonUtil.getProperty(CTCConstants.ADDRESS_PRIMARY_USAGE_TYPE).equals(
														 reqTCRMPartyAddressBObj.getAddressUsageType())){
											 //Getting the DB primary object.
											 TCRMPartyAddressBObj dbPrimaryPartyAddress =(TCRMPartyAddressBObj)getPrimaryAddressObject(
													 dbVecTCRMPartyAddressBObj);
											 if(dbPrimaryPartyAddress != null){
												 //Get the existing reference if it exist in the map
												 TCRMPartyAddressBObj objPrimaryPartyAddressBObj=(TCRMPartyAddressBObj)mapCTCPartyAddressBObjExt.get(
													 dbPrimaryPartyAddress.getPartyAddressIdPK());
												 if(objPrimaryPartyAddressBObj!=null) {
													 objPrimaryPartyAddressBObj.setAddressUsageValue(null);
													 objPrimaryPartyAddressBObj.setAddressUsageType(CTCCommonUtil.getProperty(
															 CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE));
												 }else {
													 dbPrimaryPartyAddress.setAddressUsageValue(null);
													 dbPrimaryPartyAddress.setAddressUsageType(CTCCommonUtil.getProperty(
															 CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE));
													 //Putting in the map to re-use if any update on the same object.
													 mapCTCPartyAddressBObjExt.put(dbPrimaryPartyAddress.getPartyAddressIdPK(), dbPrimaryPartyAddress);
													 vecModifiedDBPartyAddressBObj.add(dbPrimaryPartyAddress);
												 }
											 }
										 }else { // No address usage type change.
											 reqTCRMPartyAddressBObj.setAddressUsageValue(null);
											 reqTCRMPartyAddressBObj.setAddressUsageType(dbTCRMPartyAddressBObj.getAddressUsageType());
										 }
									 }
									 //breaking the DB loop once any address object matched.
									 break;
								 } 
							 }
						}
					 }
				 }
			 }
			 //Processing non match address.
			 Iterator itrReqVecNonMatchTCRMPartyAddressBObj=reqVecTCRMPartyAddressBObj.iterator();
			 while(itrReqVecNonMatchTCRMPartyAddressBObj.hasNext())
			 {
				 TCRMPartyAddressBObj reqTCRMPartyAddressBObj=(TCRMPartyAddressBObj)itrReqVecNonMatchTCRMPartyAddressBObj.next();
					 if(StringUtils.isBlank(reqTCRMPartyAddressBObj.getAddressGroupLastUpdateDate())) {
						 if(reqTCRMPartyAddressBObj.getEndDate() != null) {
							//If end-date passed, inactive the corresponding request system DSI.
							 reqTCRMPartyAddressBObj.setEndDate(null);
							 reqTCRMPartyAddressBObj.setAddressUsageType(CTCCommonUtil.getProperty(CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE));
							 if(reqTCRMPartyAddressBObj instanceof CTCPartyAddressBObjExt) {
								 CTCPartyAddressBObjExt reqPartyAddressBObjExt = (CTCPartyAddressBObjExt)reqTCRMPartyAddressBObj;
								 Vector reqVecCTCDataSource =reqPartyAddressBObjExt.getItemsCTCDataSourceBObj();
								 if(CTCCommonUtil.isNotEmpty(reqVecCTCDataSource)) {
									 CTCDataSourceBObj reqCTCDataSource = (CTCDataSourceBObj)reqVecCTCDataSource.firstElement();
									 reqCTCDataSource.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
								 }
							}
					 }
					else { //If end-date not passed, Add a new Address with corresponding request system DSI.
							//If the incoming Address is the billing address in TSYS or 
							//     if Party does not have a relationship with the Credit Card LOB
							// and if the request address is primary
							// then 
							//		find the DB Primary Address Object and update to Alternate. 
							//      update the request object to Primary.
							//else
							//		Add a new Address record with Address Usage Type of �Alternate
							 if((CTCCommonUtil.getProperty(CTCConstants.TSYS_CLIENT_SYSTEM_NAME).equalsIgnoreCase(strClientSystemName)
									 	|| !hasCreditCardLOBRelationship(dbTCRMPartyBObj.getItemsTCRMPartyLobRelationshipBObj()))
									 && CTCCommonUtil.getProperty(CTCConstants.ADDRESS_PRIMARY_USAGE_TYPE).equals(
											 reqTCRMPartyAddressBObj.getAddressUsageType())){
								 //Getting the DB primary object.
								 TCRMPartyAddressBObj dbPrimaryPartyAddress=(TCRMPartyAddressBObj)
								 			getPrimaryAddressObject(dbVecTCRMPartyAddressBObj);
								 if(dbPrimaryPartyAddress != null){
									 //Get the existing reference if it exist in the map.
									 TCRMPartyAddressBObj dbObjPrimaryPartyAddress =(TCRMPartyAddressBObj)
									 			mapCTCPartyAddressBObjExt.get(dbPrimaryPartyAddress.getPartyAddressIdPK());
									 if(dbObjPrimaryPartyAddress != null){
										 dbObjPrimaryPartyAddress.setAddressUsageValue(null);
										 dbObjPrimaryPartyAddress.setAddressUsageType(CTCCommonUtil.getProperty(
												 CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE));
									 }else {
										 dbPrimaryPartyAddress.setAddressUsageValue(null);
										 dbPrimaryPartyAddress.setAddressUsageType(CTCCommonUtil.getProperty(
												 CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE));
										 //Putting in the map to re-use if any update on the same object.
										 mapCTCPartyAddressBObjExt.put(dbPrimaryPartyAddress.getPartyAddressIdPK(), dbPrimaryPartyAddress);
										 vecModifiedDBPartyAddressBObj.add(dbPrimaryPartyAddress);
									 }
								 }
							 }else { // Add a new Address record with Address Usage Type of �Alternate
								 reqTCRMPartyAddressBObj.setAddressUsageValue(null);
								 reqTCRMPartyAddressBObj.setAddressUsageType(CTCCommonUtil.getProperty(
										 CTCConstants.ADDRESS_ALTERNATE_USAGE_TYPE));
							 }
						 }
					 }
			 }
			 //Add all modified objects to the request party. 
			 if(CTCCommonUtil.isNotEmpty(vecModifiedDBPartyAddressBObj)){
				 
				 /*
				  * Remove the Duplicate Party Address from the Party since it can lead to multiple updates of the same Party Address
				  * failing due to LastUpdateDate non-match issue.
				  */
				 Iterator modifiedDBPartyAddressItr = vecModifiedDBPartyAddressBObj.iterator();
				 while (modifiedDBPartyAddressItr.hasNext()) {
					TCRMPartyAddressBObj modifiedDBPartyAddress = (TCRMPartyAddressBObj) modifiedDBPartyAddressItr.next();
					String dbPartyAddressIdPK = modifiedDBPartyAddress.getPartyAddressIdPK();
					
					 Vector tempRequestPartyAddressVec = reqTCRMPartyBObj.getItemsTCRMPartyAddressBObj();
					 
					 Iterator tempItr = tempRequestPartyAddressVec.iterator();
					 while (tempItr.hasNext()) {
						TCRMPartyAddressBObj requestPartyAddress = (TCRMPartyAddressBObj) tempItr.next();
						
						String requestPartyAddressIdPK = requestPartyAddress.getPartyAddressIdPK();
						
						if(StringUtils.isNonBlank(dbPartyAddressIdPK) && StringUtils.isNonBlank(requestPartyAddressIdPK)
								&& StringUtils.compareIgnoreCaseWithTrim(requestPartyAddressIdPK, dbPartyAddressIdPK)){
							tempItr.remove();
						}
					}
				}
				 
				 reqTCRMPartyBObj.getItemsTCRMPartyAddressBObj().addAll(vecModifiedDBPartyAddressBObj);
			 }
		 }	 		  
	  }catch(Exception ex){
		  logger.info("CTCMaintainPartyHelper:handleTCRMPartyAddress-"+ex.getMessage());
		  throw ex;
   }  
  }
	 
  /**
   * This method is used to handle the TCRM Person Name objects.
   *    1. Setting ID PKs and last updated date.
   *    2. Creating the CTC Data source vector for all the DB Person Name objects
   *    3. If end-date passed, inactive the corresponding request system DSI.
   *    4. If the matched Name is already actively associated, clear the request DSI object.
   *    5. Process the match list
   *    	Party�s active Name record with non-standardized First Name, Last Name, and Generation 
   *    		exactly matching (case-insensitive) the incoming non-standardized Name�s 
   *    		First Name, Last Name, and Generation, respectively.
   *    	if matched and update for fields then
   *    		update Prefix, Middle Initial/Name(s), or both.
   *    	else
   *    		Perform a redundant update on the Name record, No name usage type also changed.
   *    6.  Process non-match list
   *    	If end-date passed, inactive the corresponding request system DSI.
   *    	If the incoming Name is from TSYS or 
   *			if Party does not have a relationship with the Credit Card LOB
   *		then 
   *			find the DB Primary Name Object and update to Alternate. 
   *			update the request Name object to Primary.
   *		else
   *			Add a new Name record with Name Usage Type of Alternate
   *    	
   *    
   * @param reqTCRMPartyBObj request party object
   * @param dbTCRMPartyBObj DB party object
   * @throws Exception
   */
	@SuppressWarnings("unchecked")
	private void handleTCRMPersonName(TCRMPartyBObj reqTCRMPartyBObj, TCRMPartyBObj dbTCRMPartyBObj) throws Exception{
	  try{
		  TCRMPersonBObj reqTCRMPersonBObj=(TCRMPersonBObj)reqTCRMPartyBObj;
		  TCRMPersonBObj dbTCRMPersonBobj=(TCRMPersonBObj)dbTCRMPartyBObj;		
			
		  Vector reqVecTCRMPersonNameBObj=reqTCRMPersonBObj.getItemsTCRMPersonNameBObj();
		  if (CTCCommonUtil.isNotEmpty(reqVecTCRMPersonNameBObj)) {
			  Vector dbVecTCRMPersonNameBObj=dbTCRMPersonBobj.getItemsTCRMPersonNameBObj();
			  //Map to store the modified objects and re-used for any duplicate update
			  HashMap mapCTCPersonNameBObjExt = new HashMap();
			  //Getting all the CTC Data source vector for all the DB Person Name objects.
			  HashMap dbMapCTCDataSourceBObj = getPersonNameCTCDataSourceBObjs(dbVecTCRMPersonNameBObj);
			  //Getting the request system name
			  String strClientSysteName = getClientSystemName(reqTCRMPartyBObj);
			  Iterator itrReqVecPersonNameBObj=reqVecTCRMPersonNameBObj.iterator();
			  while(itrReqVecPersonNameBObj.hasNext()) {
				  TCRMPersonNameBObj reqTCRMPersonNameBObj = (TCRMPersonNameBObj)itrReqVecPersonNameBObj.next();
				  if(reqTCRMPersonNameBObj!= null) {
					  CTCPersonNameBObjExt reqPersonNameBObjExt = null;
					  if(reqTCRMPersonNameBObj instanceof CTCPersonNameBObjExt){
						  reqPersonNameBObjExt = (CTCPersonNameBObjExt)reqTCRMPersonNameBObj;
					  }
					  if (CTCCommonUtil.isNotEmpty(dbVecTCRMPersonNameBObj)) {
						  Iterator itrDBVecPersonNameBObj = dbVecTCRMPersonNameBObj.iterator();
						  while(itrDBVecPersonNameBObj.hasNext()){
							  TCRMPersonNameBObj dbTCRMPersonNameBObj = (TCRMPersonNameBObj)itrDBVecPersonNameBObj.next();
							  //Getting corresponding DSI from the map
							  Vector<CTCDataSourceBObj> dbVecCTCDataSourceBObj = (Vector<CTCDataSourceBObj>)
					  				dbMapCTCDataSourceBObj.get(dbTCRMPersonNameBObj.getPersonNameIdPK());
							  CTCDataSourceBObj dbCTCDataSourceBObj = null;
							  if (CTCCommonUtil.isNotEmpty(dbVecCTCDataSourceBObj)){
								  //Getting the request system DSI
								  dbCTCDataSourceBObj = getRequestSystemCTCDataSourceBObj(dbVecCTCDataSourceBObj, strClientSysteName);
							  }
							  //Party�s active Name record with non-standardized First Name, Last Name, and Generation 
							  //   		exactly matching (case-insensitive) the incoming non-standardized Name�s 
							  //   		First Name, Last Name, and Generation, respectively.
							  //   	if matched and update for fields then
							  //   		update Prefix, Middle Initial/Name(s), or both.
							  //   	else
							  //    		Perform a redundant update on the Name record, No name usage type also changed.
							  
							  if(CTCCommonUtil.matchStringField(reqTCRMPersonNameBObj.getLastName(),
									  		 dbTCRMPersonNameBObj.getLastName()) &&
									 CTCCommonUtil.matchStringField(reqTCRMPersonNameBObj.getGivenNameOne(), 
											 dbTCRMPersonNameBObj.getGivenNameOne()) &&
									 CTCCommonUtil.matchStringField(reqTCRMPersonNameBObj.getGenerationType(),
											 dbTCRMPersonNameBObj.getGenerationType())) {
								  //Setting ID PKs and last updated date.
								 reqTCRMPersonNameBObj.setPersonPartyId(dbTCRMPersonNameBObj.getPersonPartyId());
								 reqTCRMPersonNameBObj.setPersonNameIdPK(dbTCRMPersonNameBObj.getPersonNameIdPK());
								 reqTCRMPersonNameBObj.setPersonNameLastUpdateDate(
										 dbTCRMPersonNameBObj.getPersonNameLastUpdateDate());
								  //Setting usage type for redundant update 
								 reqTCRMPersonNameBObj.setNameUsageType(dbTCRMPersonNameBObj.getNameUsageType());
								 reqTCRMPersonNameBObj.setNameUsageValue(dbTCRMPersonNameBObj.getNameUsageValue());
								//If end-date passed, inactive the corresponding request system DSI. 
								if (reqTCRMPersonNameBObj.getEndDate() != null){
									reqTCRMPersonNameBObj.setEndDate(null);
									if(dbCTCDataSourceBObj != null){
										reqPersonNameBObjExt.getItemsCTCDataSourceBObj().clear();
										dbCTCDataSourceBObj.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
										reqPersonNameBObjExt.getItemsCTCDataSourceBObj().add(dbCTCDataSourceBObj);
									}else{
										reqPersonNameBObjExt.getItemsCTCDataSourceBObj().clear();
									}
								}
								else{ //If the matched name is already actively associated DSI, clear the request DSI object.
									if(dbCTCDataSourceBObj != null ){
										reqPersonNameBObjExt.getItemsCTCDataSourceBObj().clear();
										reqPersonNameBObjExt.getItemsCTCDataSourceBObj().add(dbCTCDataSourceBObj);
									}
								}
								//Putting in the map to re-use if any update on the same object.
								mapCTCPersonNameBObjExt.put(reqTCRMPersonNameBObj.getPersonNameIdPK(), 
										reqTCRMPersonNameBObj);
								//breaking the DB loop once any Name object matched.
								break;
							  }
						  }
					  }
				  }
			  }
			  // Process non-match list
			  Iterator itrReqVecNonMatchPersonNameBObj = reqVecTCRMPersonNameBObj.iterator();
			  //New Vector for adding the modified DB objects. 
			  Vector vecModifiedDBTCRMPersonNameBObj = new Vector();
			  boolean blnRequestPrimary = false;
		  		while(itrReqVecNonMatchPersonNameBObj.hasNext())
		  		{
		  			TCRMPersonNameBObj reqTCRMPersonNameBObj = (TCRMPersonNameBObj)itrReqVecNonMatchPersonNameBObj.next();
		  			if(StringUtils.isBlank(reqTCRMPersonNameBObj.getPersonNameLastUpdateDate())){
		  				if(reqTCRMPersonNameBObj.getEndDate() != null) {
		  				//If end-date passed, inactive the corresponding request system DSI.
		  					reqTCRMPersonNameBObj.setEndDate(null);
		  					reqTCRMPersonNameBObj.setNameUsageType(CTCCommonUtil.getProperty(
		  						CTCConstants.PERSONNAME_ALTERNATE_USAGE_TYPE));
							 if(reqTCRMPersonNameBObj instanceof CTCPersonNameBObjExt) {
								 CTCPersonNameBObjExt reqPersonNameBObjExt = (CTCPersonNameBObjExt)reqTCRMPersonNameBObj;
								 Vector reqVecCTCDataSource = reqPersonNameBObjExt.getItemsCTCDataSourceBObj();
								 if(CTCCommonUtil.isNotEmpty(reqVecCTCDataSource)) {
									 CTCDataSourceBObj reqCTCDataSource = (CTCDataSourceBObj)reqVecCTCDataSource.firstElement();
									 reqCTCDataSource.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
								 }
							}
						 }
			  			else { // If end-date not passed, Add a new name with corresponding request system DSI.
			  				reqTCRMPersonNameBObj.setNameUsageType(CTCCommonUtil.getProperty(
			  						CTCConstants.PERSONNAME_ALTERNATE_USAGE_TYPE));
			  				reqTCRMPersonNameBObj.setNameUsageValue(null);
			  				//If the incoming Name is from TSYS or 
							//     if Party does not have a relationship with the Credit Card LOB
							// then 
							//		find the DB Primary Name Object and update to Alternate. 
							//      update the request Name object to Primary.
							//else
							//		Add a new Name record with Name Usage Type of Alternate
			  				if(CTCCommonUtil.getProperty(CTCConstants.TSYS_CLIENT_SYSTEM_NAME).equalsIgnoreCase(strClientSysteName) ||
			  						!hasCreditCardLOBRelationship(dbTCRMPartyBObj.getItemsTCRMPartyLobRelationshipBObj())){
			  					//Getting the DB primary object
			  					TCRMPersonNameBObj dbPrimaryPersonNameBObj = getPrimaryPersonName(dbVecTCRMPersonNameBObj);
			  					if(dbPrimaryPersonNameBObj != null){
			  						if(!blnRequestPrimary){
			  							reqTCRMPersonNameBObj.setNameUsageType(CTCCommonUtil.getProperty(
			  									CTCConstants.PERSONNAME_PRIMARY_USAGE_TYPE));
			  							reqTCRMPersonNameBObj.setNameUsageValue(null);
			  							blnRequestPrimary = true;
			  						}
			  						//Get the existing reference if it exist in the map
			  						TCRMPersonNameBObj dbObjTCRMPersonNameBObj  = (TCRMPersonNameBObj)mapCTCPersonNameBObjExt.get(
			  								dbPrimaryPersonNameBObj.getPersonNameIdPK());
			  						if(dbObjTCRMPersonNameBObj != null){
			  							dbObjTCRMPersonNameBObj.setNameUsageType(CTCCommonUtil.getProperty(
			  									CTCConstants.PERSONNAME_ALTERNATE_USAGE_TYPE));
			  							dbObjTCRMPersonNameBObj.setNameUsageValue(null);
			  						}else {
			  							dbPrimaryPersonNameBObj.setNameUsageType(CTCCommonUtil.getProperty(
			  									CTCConstants.PERSONNAME_ALTERNATE_USAGE_TYPE));
			  							dbPrimaryPersonNameBObj.setNameUsageValue(null);
			  							//Putting in the map to re-use if any update on the same object.
			  							mapCTCPersonNameBObjExt.put(dbPrimaryPersonNameBObj.getPersonNameIdPK(), 
			  									dbPrimaryPersonNameBObj);
			  							//Added in the vector for update
			  							vecModifiedDBTCRMPersonNameBObj.add(dbPrimaryPersonNameBObj);
			  						}
			  					}
			  				}
			  			}
		  			}
		  		}
		  		//Add all modified objects to the request party.
		  		if(CTCCommonUtil.isNotEmpty(vecModifiedDBTCRMPersonNameBObj)){
		  			reqTCRMPersonBObj.getItemsTCRMPersonNameBObj().addAll(vecModifiedDBTCRMPersonNameBObj);
		  		}
		  	}
	  	}catch(Exception ex){
	  		logger.info("CTCMaintainPartyHelper:handleTCRMPersonName-"+ex.getMessage());
	  		throw ex;
	  	}
  	}
  	/**
  	 * This method is used to handle the TCRM Email contact method objects.
  	 *    1. Setting ID PKs and last updated date.
  	 *    2. Creating the CTC Data source vector for all the DB Email contact method objects
  	 *    3. If end-date passed, inactive the corresponding request system DSI.
  	 *    4. If the matched Name is already actively associated, clear the request DSI object.
  	 *    5. Process the match list
  	 *    		if DB Party�s active Email record with Email Address exactly matching 
  	 *    				(case-insensitive) the incoming Email Address
  	 *    			and If the found Email record is an ContactMethod Usage Type of Alternate
  	 *    		then
  	 *    			find the DB Primary Email Object and update to Alternate. 
  	 *				update the request Email object to Primary.
  	 *		  	else
  	 *				No ContactMethod usage type change. Perform redundant update.
  	 *	  6. Process the non-match list
  	 *			If email is not matched with any of the DB record then
  	 *				find the DB Primary Email Object and update to Alternate. 
  	 *				update the request Email object to Primary.
  	 *    
  	 * @param reqTCRMPartyBObj - request party object
  	 * @param dbTCRMPartyBObj - DB party object
  	 * @throws Exception
  	 */
	@SuppressWarnings("unchecked")
	private void handleEmailContactMethod(TCRMPartyBObj reqTCRMPartyBObj,TCRMPartyBObj dbTCRMPartyBObj) throws Exception {
	  logger.info("CTCMaintainPartyHelper:handleEmailContactMethod - "+CTCConstants.LOG_ENTRY_OF_METHOD);
	  try {
		  Vector reqVecPartyContactMethodBObj = reqTCRMPartyBObj.getItemsTCRMPartyContactMethodBObj();
		  if (CTCCommonUtil.isNotEmpty(reqVecPartyContactMethodBObj)) {
			  //New Vector for adding the modified DB objects. 
			  Vector vecModifiedDBEmailContactMethodBObj = new Vector();
			  //Map to store the modified objects and re-used for any duplicate update
			  Vector dbVecPartyContactMethodBObj = dbTCRMPartyBObj.getItemsTCRMPartyContactMethodBObj();
			  HashMap mapCTCPartyContactMethodBObjExt = new HashMap();
			  //Getting all the CTC Data source vector for all the DB Email objects
			  HashMap dbMapCTCDataSourceBObj = getEmailCTCDataSourceBObjs(dbVecPartyContactMethodBObj);
			  //Getting the request system name
			  String strClientSysteName = getClientSystemName(reqTCRMPartyBObj);
			  Iterator itrReqPartyContactMethodBObj=reqVecPartyContactMethodBObj.iterator();
			  while(itrReqPartyContactMethodBObj.hasNext()) {
				  TCRMPartyContactMethodBObj reqTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)itrReqPartyContactMethodBObj.next();
				  TCRMContactMethodBObj reqTCRMContactMethodBObj = reqTCRMPartyContactMethodBObj.getTCRMContactMethodBObj();
				  if(reqTCRMContactMethodBObj!= null && 
						  CTCConstants.CONT_METH_TP_EMAIL.equals(reqTCRMContactMethodBObj.getContactMethodType())) {
					  CTCPartyContactMethodBObjExt reqPartyContactMethodBObjExt = null;
					  if(reqTCRMPartyContactMethodBObj instanceof CTCPartyContactMethodBObjExt){
						  reqPartyContactMethodBObjExt = (CTCPartyContactMethodBObjExt)reqTCRMPartyContactMethodBObj;
					  }
					  if (CTCCommonUtil.isNotEmpty(dbVecPartyContactMethodBObj)) {
						  Iterator itrDBPartyContactMethodBObj = dbVecPartyContactMethodBObj.iterator();
						  while(itrDBPartyContactMethodBObj.hasNext()){
							  TCRMPartyContactMethodBObj dbTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)itrDBPartyContactMethodBObj.next();
							  TCRMContactMethodBObj dbTCRMContactMethodBObj = dbTCRMPartyContactMethodBObj.getTCRMContactMethodBObj();
							  if(dbTCRMContactMethodBObj!= null && 
									  CTCConstants.CONT_METH_TP_EMAIL.equals(dbTCRMContactMethodBObj.getContactMethodType())) {
								  //Getting corresponding DSI from the map
								  Vector<CTCDataSourceBObj> dbVecCTCDataSourceBObj = (Vector<CTCDataSourceBObj>)dbMapCTCDataSourceBObj.get(
											dbTCRMPartyContactMethodBObj.getPartyContactMethodIdPK());
									CTCDataSourceBObj dbCTCDataSourceBObj = null;
									if (CTCCommonUtil.isNotEmpty(dbVecCTCDataSourceBObj)){
										//Getting the request system DSI
										dbCTCDataSourceBObj = getRequestSystemCTCDataSourceBObj(dbVecCTCDataSourceBObj, strClientSysteName);
									}
									if(CTCCommonUtil.matchStringField(reqTCRMContactMethodBObj.getReferenceNumber(),
											dbTCRMContactMethodBObj.getReferenceNumber())) {
										//Setting ID PKs and last updated date.
										setContactMethodIDPKs(reqTCRMPartyContactMethodBObj, dbTCRMPartyContactMethodBObj);
										//Putting in the map to re-use if any update on the same object.
										mapCTCPartyContactMethodBObjExt.put(dbTCRMPartyContactMethodBObj.getPartyContactMethodIdPK(), 
												reqTCRMPartyContactMethodBObj);
										//If end-date passed, inactive the corresponding request system DSI. 
										if (reqTCRMPartyContactMethodBObj.getEndDate() != null){
											reqTCRMPartyContactMethodBObj.setEndDate(null);
											reqTCRMPartyContactMethodBObj.setContactMethodUsageType(
													dbTCRMPartyContactMethodBObj.getContactMethodUsageType());
											reqTCRMPartyContactMethodBObj.setContactMethodUsageValue(null);
											if(dbCTCDataSourceBObj != null && reqPartyContactMethodBObjExt != null){
												reqPartyContactMethodBObjExt.getItemsCTCDataSourceBObj().clear();
												dbCTCDataSourceBObj.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
												reqPartyContactMethodBObjExt.getItemsCTCDataSourceBObj().add(dbCTCDataSourceBObj);
											}else if(reqPartyContactMethodBObjExt != null){
												reqPartyContactMethodBObjExt.getItemsCTCDataSourceBObj().clear();
											}
										}
										else{
											if(dbCTCDataSourceBObj != null && reqPartyContactMethodBObjExt != null){
												reqPartyContactMethodBObjExt.getItemsCTCDataSourceBObj().clear();
												reqPartyContactMethodBObjExt.getItemsCTCDataSourceBObj().add(dbCTCDataSourceBObj);
											}
											//if DB Party�s active Email record with Email Address exactly matching 
										  	//    	(case-insensitive) the incoming Email Address
										  	//    and If the found Email record is an ContactMethod Usage Type of Alternate
										  	// then
										  	// 		find the DB Primary Email Object and update to Alternate. 
										  	//		update the request Email object to Primary.
										  	//	else
										  	//		No ContactMethod usage type change. Perform redundant update.
											if(CTCCommonUtil.getProperty(CTCConstants.EMAIL_ALTERNATE_USAGE_TYPE).equals(
													dbTCRMPartyContactMethodBObj.getContactMethodUsageType())){
												TCRMPartyContactMethodBObj dbPrimaryPartyContactMethodBObj = getPrimaryEmail(dbVecPartyContactMethodBObj);
												if(dbPrimaryPartyContactMethodBObj != null){
													TCRMPartyContactMethodBObj dbPrimaryCTCDataSourceBObj = 
														(TCRMPartyContactMethodBObj)mapCTCPartyContactMethodBObjExt.get(
															dbPrimaryPartyContactMethodBObj.getPartyContactMethodIdPK());
													if(dbPrimaryCTCDataSourceBObj != null){
														dbPrimaryCTCDataSourceBObj.setContactMethodUsageType(CTCCommonUtil.getProperty(
																CTCConstants.EMAIL_ALTERNATE_USAGE_TYPE));
														dbPrimaryCTCDataSourceBObj.setContactMethodUsageValue(null);
													}else {
														dbPrimaryPartyContactMethodBObj.setContactMethodUsageType(CTCCommonUtil.getProperty(
																CTCConstants.EMAIL_ALTERNATE_USAGE_TYPE));
														dbPrimaryPartyContactMethodBObj.setContactMethodUsageValue(null);
														//Putting in the map to re-use if any update on the same object.
														mapCTCPartyContactMethodBObjExt.put(dbPrimaryPartyContactMethodBObj.getPartyContactMethodIdPK(), 
																dbPrimaryPartyContactMethodBObj);
														//Added in the vector for update
														vecModifiedDBEmailContactMethodBObj.add(dbPrimaryPartyContactMethodBObj);
													}
												}
												reqTCRMPartyContactMethodBObj.setContactMethodUsageType(CTCCommonUtil.getProperty(
														CTCConstants.EMAIL_PRIMARY_USAGE_TYPE));
												reqTCRMPartyContactMethodBObj.setContactMethodUsageValue(null);
											}
										}
										//Breaking the DB loop if any one object matched.
										break;
									}
							  }
						  }
					  }
				  	}
			  	}
			  //process non-match list
			  Iterator itrReqVecNonMatchPartyContactMethodBObj = reqVecPartyContactMethodBObj.iterator();
			  boolean blnRequestPrimary = false;
			  	while(itrReqVecNonMatchPartyContactMethodBObj.hasNext())
			  	{
			  		TCRMPartyContactMethodBObj reqTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)itrReqVecNonMatchPartyContactMethodBObj.next();
					  if(CTCConstants.CONT_METH_TP_EMAIL.equals(reqTCRMPartyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType()))
					  {
						  if(StringUtils.isBlank(reqTCRMPartyContactMethodBObj.getContactMethodGroupLastUpdateDate()))
						  {
							  if(reqTCRMPartyContactMethodBObj.getEndDate() != null) {
								//If end-date passed, inactive the corresponding request system DSI.
								  reqTCRMPartyContactMethodBObj.setEndDate(null);
								  reqTCRMPartyContactMethodBObj.setContactMethodUsageType
								  	(CTCCommonUtil.getProperty(CTCConstants.EMAIL_ALTERNATE_USAGE_TYPE));
									 if(reqTCRMPartyContactMethodBObj instanceof CTCPartyContactMethodBObjExt) {
										 CTCPartyContactMethodBObjExt reqPartyContactMethodBObjExt = (CTCPartyContactMethodBObjExt)reqTCRMPartyContactMethodBObj;
										 Vector reqVecCTCDataSource = reqPartyContactMethodBObjExt.getItemsCTCDataSourceBObj();
										 if(CTCCommonUtil.isNotEmpty(reqVecCTCDataSource)) {
											 CTCDataSourceBObj reqCTCDataSource = (CTCDataSourceBObj)reqVecCTCDataSource.firstElement();
											 reqCTCDataSource.setEndDate(DWLDateTimeUtilities.getCurrentSystemTime());
										 }
									}
								 }
							  //If end-date not passed, Add a new Email with corresponding request system DSI.
							  //If email is not matched with any of the DB record then
							  //	find the DB Primary Email Object and update to Alternate. 
							  //	update the request Email object to Primary.
							 else {
								  if(!blnRequestPrimary){
									  //update the request Email object to Primary.
									  reqTCRMPartyContactMethodBObj.setContactMethodUsageType(CTCCommonUtil.getProperty(
											  CTCConstants.EMAIL_PRIMARY_USAGE_TYPE));
								  }else{
									  reqTCRMPartyContactMethodBObj.setContactMethodUsageType(CTCCommonUtil.getProperty(
											  CTCConstants.EMAIL_ALTERNATE_USAGE_TYPE));  
								  }
								  //find the DB Primary Email Object and update to Alternate.
								  TCRMPartyContactMethodBObj dbPrimaryPartyContactMethodBObj = getPrimaryEmail(dbVecPartyContactMethodBObj);
								  if(dbPrimaryPartyContactMethodBObj != null){
									  TCRMPartyContactMethodBObj dbObjTCRMPartyContactMethodBObj  = 
										  (TCRMPartyContactMethodBObj)mapCTCPartyContactMethodBObjExt.get(
											  dbPrimaryPartyContactMethodBObj.getPartyContactMethodIdPK());
									  if(dbObjTCRMPartyContactMethodBObj != null){
										  dbObjTCRMPartyContactMethodBObj.setContactMethodUsageType(CTCCommonUtil.getProperty(
												  CTCConstants.EMAIL_ALTERNATE_USAGE_TYPE));
										  dbObjTCRMPartyContactMethodBObj.setContactMethodUsageValue(null);
									  }
									  else{
										  dbPrimaryPartyContactMethodBObj.setContactMethodUsageType(CTCCommonUtil.getProperty(
												  CTCConstants.EMAIL_ALTERNATE_USAGE_TYPE));
										  dbPrimaryPartyContactMethodBObj.setContactMethodUsageValue(null);
										  //Putting in the map to re-use if any update on the same object.
										  mapCTCPartyContactMethodBObjExt.put(dbPrimaryPartyContactMethodBObj.getPartyContactMethodIdPK() , 
												  dbPrimaryPartyContactMethodBObj);
										  //Added in the vector for update
										  vecModifiedDBEmailContactMethodBObj.add(dbPrimaryPartyContactMethodBObj);
									  }
								  }
							 }
						  }
					  }
			  	}
			  	//Add all modified objects to the request party.
			  	if(CTCCommonUtil.isNotEmpty(vecModifiedDBEmailContactMethodBObj)){
			  		reqTCRMPartyBObj.getItemsTCRMPartyContactMethodBObj().addAll(vecModifiedDBEmailContactMethodBObj);
			  	}
			  	
		  	}
	  }catch(Exception ex)
	  {
		  logger.info("CTCMaintainPartyHelper: handleEmailContactMethod - "+ex.getMessage());
		  throw ex;
	  }
  }
	
	
	/**
	 * This method is used to handle the TCRM Phone number & FAX contact method objects.
  	 *    1. Setting ID PKs and last updated date.
  	 *    2. Standardize phone number Contact Method object
  	 *    3. Setting ID PKs and last updated date for TCRM Phone number objects. 
  	 *    4. Process end-date phone number & FAX objects from the request,
  	 *         If end-date is populated in the request and contact method type and usage type matches with DB then 
  	 *         		a. Set the ID PKs and last update date form the DB objects.
  	 *         		b. Remove the corresponding object from the DB vector.
  	 *         		c. This record will be end-dated in the DB.
  	 *         		
  	 *    5. Process new phone number & FAX objects from the request,
  	 *         If contact method type and usage type matches with DB object then
  	 *         		Set the ID PKs and last update date form the DB objects.
  	 *         else
  	 *         		The new record will be added in the DB
	 * 
	 * @param reqTCRMPartyBObj request party object
	 * @param dbTCRMPartyBObj DB party object
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void handleContactMethod(TCRMPartyBObj reqTCRMPartyBObj,TCRMPartyBObj dbTCRMPartyBObj) throws Exception {
		  logger.info("CTCMaintainPartyHelper:handleTCRMPartyContactMethod - "+CTCConstants.LOG_ENTRY_OF_METHOD);
		  try {
			  //Request vector
			  Vector reqVecPartyContactMethodBObj = reqTCRMPartyBObj.getItemsTCRMPartyContactMethodBObj();
			  if (CTCCommonUtil.isNotEmpty(reqVecPartyContactMethodBObj)) {
				  //DB Vector
				  Vector dbVecPartyContactMethodBObj = dbTCRMPartyBObj.getItemsTCRMPartyContactMethodBObj();
				  Iterator itrReqVecPartyContactMethodBObj = reqVecPartyContactMethodBObj.iterator();
				  //Processing end-date phone number & FAX objects from the request
				  while(itrReqVecPartyContactMethodBObj.hasNext()) {
					  TCRMPartyContactMethodBObj reqTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)itrReqVecPartyContactMethodBObj.next();
					  TCRMContactMethodBObj reqTCRMContactMethodBObj = reqTCRMPartyContactMethodBObj.getTCRMContactMethodBObj();
					  //if Request is not Email and end-date is populated.
					  if(reqTCRMContactMethodBObj!= null && 
							  reqTCRMPartyContactMethodBObj.getEndDate() != null &&
							  !CTCConstants.CONT_METH_TP_EMAIL.equals(reqTCRMContactMethodBObj.getContactMethodType())) {
						  if(CTCConstants.CONT_METH_TP_PHONE.equals(reqTCRMContactMethodBObj.getContactMethodType())){
							  //Standardize phone number Contact Method object
							  reqTCRMContactMethodBObj = CTCCommonUtil.standardizeContactMethod(reqTCRMContactMethodBObj);
						  }
						  if (CTCCommonUtil.isNotEmpty(dbVecPartyContactMethodBObj)) {
							  Iterator itrDBVecPartyContactMethodBObj = dbVecPartyContactMethodBObj.iterator();
							  while(itrDBVecPartyContactMethodBObj.hasNext()){
								  TCRMPartyContactMethodBObj dbTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)
										  itrDBVecPartyContactMethodBObj.next();
								  TCRMContactMethodBObj dbTCRMContactMethodBObj = dbTCRMPartyContactMethodBObj.getTCRMContactMethodBObj();
								  //If DB record is not Email and contact method type and usage type matches with DB then 
								  //	a. Set the ID PKs and last update date form the DB objects.
								  //	b. Remove the corresponding object from the DB vector.
								  //	c. This record will be end-dated in the DB.
								  if(dbTCRMContactMethodBObj!= null && 
										  !CTCConstants.CONT_METH_TP_EMAIL.equals(dbTCRMContactMethodBObj.getContactMethodType())&&
										  dbTCRMPartyContactMethodBObj.getContactMethodUsageType().equals(
												  reqTCRMPartyContactMethodBObj.getContactMethodUsageType())&&
										  dbTCRMContactMethodBObj.getContactMethodType().equals(
												  reqTCRMContactMethodBObj.getContactMethodType())) {
									  //Setting ID PKs for Contact method object.
									  setContactMethodIDPKs(reqTCRMPartyContactMethodBObj, dbTCRMPartyContactMethodBObj);
									  TCRMPhoneNumberBObj reqTCRMPhoneNumberBObj = reqTCRMContactMethodBObj.getTCRMPhoneNumberBObj();
									  //Set ID PKs for TCRM phone number objects.
									  if(reqTCRMPhoneNumberBObj != null){
										  TCRMPhoneNumberBObj dbTCRMPhoneNumberBObj = dbTCRMContactMethodBObj.getTCRMPhoneNumberBObj();
										  if(dbTCRMPhoneNumberBObj != null){
											  reqTCRMPhoneNumberBObj.setPhoneNumberId(
													  dbTCRMPhoneNumberBObj.getPhoneNumberId());
											  reqTCRMPhoneNumberBObj.setPhoneLastUpdateDate(
													  dbTCRMPhoneNumberBObj.getPhoneLastUpdateDate());
										  }
									  }
									  //Remove the corresponding object from the DB vector.
									  dbVecPartyContactMethodBObj.remove(dbTCRMPartyContactMethodBObj);
									  //Breaking the DB loop if any one object matched.
									  break;
								  }
							  }
						  }
					  }
				  }
				  //Processing new phone number & FAX objects from the request
				  itrReqVecPartyContactMethodBObj = reqVecPartyContactMethodBObj.iterator();
				  while(itrReqVecPartyContactMethodBObj.hasNext()) {
					  TCRMPartyContactMethodBObj reqTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)itrReqVecPartyContactMethodBObj.next();
					  TCRMContactMethodBObj reqTCRMContactMethodBObj = reqTCRMPartyContactMethodBObj.getTCRMContactMethodBObj();
					  //if Request is not Email and end-date is populated.
					  if(reqTCRMContactMethodBObj!= null && 
							  reqTCRMPartyContactMethodBObj.getEndDate() == null &&
							  !CTCConstants.CONT_METH_TP_EMAIL.equals(reqTCRMContactMethodBObj.getContactMethodType())) {
						  if(CTCConstants.CONT_METH_TP_PHONE.equals(reqTCRMContactMethodBObj.getContactMethodType())){
							  //Standardize phone number Contact Method object
							  reqTCRMContactMethodBObj = CTCCommonUtil.standardizeContactMethod(reqTCRMContactMethodBObj);
						  }
						  if (CTCCommonUtil.isNotEmpty(dbVecPartyContactMethodBObj)) {
							  Iterator itrDBVecPartyContactMethodBObj = dbVecPartyContactMethodBObj.iterator();
							  while(itrDBVecPartyContactMethodBObj.hasNext()){
								  TCRMPartyContactMethodBObj dbTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)
										  itrDBVecPartyContactMethodBObj.next();
								  TCRMContactMethodBObj dbTCRMContactMethodBObj = dbTCRMPartyContactMethodBObj.getTCRMContactMethodBObj();
								  //If DB record is not Email and contact method type and usage type matches with DB then 
								  //	Set the ID PKs and last update date form the DB objects.
								  //else
								  //	The new record will be added in the DB
								  if(dbTCRMContactMethodBObj!= null && 
										  !CTCConstants.CONT_METH_TP_EMAIL.equals(dbTCRMContactMethodBObj.getContactMethodType())&&
										  dbTCRMPartyContactMethodBObj.getContactMethodUsageType().equals(
												  reqTCRMPartyContactMethodBObj.getContactMethodUsageType())&&
										  dbTCRMContactMethodBObj.getContactMethodType().equals(
												  reqTCRMContactMethodBObj.getContactMethodType())) {
									  //Setting ID PKs for Contact method object.
									  setContactMethodIDPKs(reqTCRMPartyContactMethodBObj, dbTCRMPartyContactMethodBObj);
									  TCRMPhoneNumberBObj reqTCRMPhoneNumberBObj = reqTCRMContactMethodBObj.getTCRMPhoneNumberBObj();
									  //Set ID PKs for TCRM phone number objects.
									  if(reqTCRMPhoneNumberBObj != null){
										  TCRMPhoneNumberBObj dbTCRMPhoneNumberBObj = dbTCRMContactMethodBObj.getTCRMPhoneNumberBObj();
										  if(dbTCRMPhoneNumberBObj != null){
											  reqTCRMPhoneNumberBObj.setPhoneNumberId(
													  dbTCRMPhoneNumberBObj.getPhoneNumberId());
											  reqTCRMPhoneNumberBObj.setPhoneLastUpdateDate(
													  dbTCRMPhoneNumberBObj.getPhoneLastUpdateDate());
										  }
									  }
									  //Breaking the DB loop if any one object matched.
									  break;
								  }
							  }
						  }
					  }
				  }
				  //invoking to handle the email objects.
				  handleEmailContactMethod(reqTCRMPartyBObj, dbTCRMPartyBObj);
			  }
		  }catch(Exception ex){
			  logger.info("CTCMaintainPartyHelper:handleTCRMPartyContactMethod - "+ex.getMessage());
			  throw ex;
		  }
	}

	/**
	 * Setting Party Contact method and contact method objects ID PKs and last update date.
	 * 
	 * @param reqTCRMPartyContactMethodBObj - request party contact method object
	 * @param dbTCRMPartyContactMethodBObj - DB party contact method object
	 * @throws Exception
	 */
	private void setContactMethodIDPKs(TCRMPartyContactMethodBObj reqTCRMPartyContactMethodBObj, 
			TCRMPartyContactMethodBObj dbTCRMPartyContactMethodBObj) throws Exception{
		reqTCRMPartyContactMethodBObj.setPartyContactMethodIdPK(
				  dbTCRMPartyContactMethodBObj.getPartyContactMethodIdPK());
		  reqTCRMPartyContactMethodBObj.setContactMethodGroupLastUpdateDate(
				dbTCRMPartyContactMethodBObj.getContactMethodGroupLastUpdateDate());
		  reqTCRMPartyContactMethodBObj.setLocationGroupLastUpdateDate(
				dbTCRMPartyContactMethodBObj.getLocationGroupLastUpdateDate());
		  reqTCRMPartyContactMethodBObj.getTCRMContactMethodBObj().setContactMethodIdPK(
				  dbTCRMPartyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodIdPK());
		  reqTCRMPartyContactMethodBObj.getTCRMContactMethodBObj().setContactMethodLastUpdateDate(
				  dbTCRMPartyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodLastUpdateDate());
		
	}
	
  /**
   * Creating the HasMap for the ACTIVE CTCDataSource�s for the CONTACTMETHODGROUP entity based on the 
   *    Party Contact Method ID PK.
   * 
   * @param dbVecTCRMPartyContactMethodBObj - DB Contact method Vector
   * @return
   * @throws Exception
   */
	@SuppressWarnings("unchecked")
	private HashMap getEmailCTCDataSourceBObjs(Vector dbVecTCRMPartyContactMethodBObj) throws Exception{
	  HashMap dbMapCTCDataSourceBObj = new HashMap();
	  if (CTCCommonUtil.isNotEmpty(dbVecTCRMPartyContactMethodBObj)) {
		  Iterator itrDBVecPartyContactMethodBObj = dbVecTCRMPartyContactMethodBObj.iterator();
		  while(itrDBVecPartyContactMethodBObj.hasNext()){
			  Vector<CTCDataSourceBObj> dbVecCTCDataSourceBObj = null;
			  Object obj = itrDBVecPartyContactMethodBObj.next();
			  TCRMPartyContactMethodBObj dbTCRMPartyContactMethodBObj = (TCRMPartyContactMethodBObj)obj;
			  TCRMContactMethodBObj dbTCRMContactMethodBObj = dbTCRMPartyContactMethodBObj.getTCRMContactMethodBObj();
			  if(dbTCRMContactMethodBObj!= null && 
					  CTCConstants.CONT_METH_TP_EMAIL.equals(dbTCRMContactMethodBObj.getContactMethodType())) {
				  if ( obj instanceof CTCPartyContactMethodBObjExt){
					  CTCPartyContactMethodBObjExt dbCTCPartyContactMethodBObjExt = (CTCPartyContactMethodBObjExt)obj;
					  dbVecCTCDataSourceBObj = dbCTCPartyContactMethodBObjExt.getItemsCTCDataSourceBObj();
				  }
				  if(CTCCommonUtil.isEmpty(dbVecCTCDataSourceBObj)) {
					  CTCDataSource dataSourceComponent = (CTCDataSource) TCRMClassFactory.getTCRMComponent(
							  CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
					  DWLResponse response = dataSourceComponent.getAllCTCDataSourcesByEntity( "CONTACTMETHODGROUP", 
							  dbTCRMPartyContactMethodBObj.getPartyContactMethodIdPK(), TCRMRecordFilter.ACTIVE, 
							  dbTCRMPartyContactMethodBObj.getControl());
					  dbVecCTCDataSourceBObj = (Vector<CTCDataSourceBObj>)response.getData();
				  }
				  dbMapCTCDataSourceBObj.put(dbTCRMPartyContactMethodBObj.getPartyContactMethodIdPK(), dbVecCTCDataSourceBObj);
			  }
			  
		  }
	  }
	  return dbMapCTCDataSourceBObj;
  }
  
  /**
   *  Creating the HasMap for the ACTIVE CTCDataSource�s for the PERSONNAME entity based on the 
   *    Person Name ID PK. 
   * 
   * @param dbVecTCRMPersonNameBObj - DB Person name vector.
   * @return
   * @throws Exception
   */
	@SuppressWarnings("unchecked")
	private HashMap getPersonNameCTCDataSourceBObjs(Vector dbVecTCRMPersonNameBObj) throws Exception{
	  HashMap dbMapCTCDataSourceBObj = new HashMap();
	  if (CTCCommonUtil.isNotEmpty(dbVecTCRMPersonNameBObj)) {
		  Iterator itrDBVecPersonNameBObj = dbVecTCRMPersonNameBObj.iterator();
		  while(itrDBVecPersonNameBObj.hasNext()){
			  Vector<CTCDataSourceBObj> dbVecCTCDataSourceBObj = null;
			  TCRMPersonNameBObj dbTCRMPersonNameBObj = (TCRMPersonNameBObj)itrDBVecPersonNameBObj.next();
			  if(dbTCRMPersonNameBObj!= null) {
				  if ( dbTCRMPersonNameBObj instanceof CTCPersonNameBObjExt){
					  CTCPersonNameBObjExt dbCTCPersonNameBObjExt = (CTCPersonNameBObjExt)dbTCRMPersonNameBObj;
					  dbVecCTCDataSourceBObj = dbCTCPersonNameBObjExt.getItemsCTCDataSourceBObj();
				  }
				  if(CTCCommonUtil.isEmpty(dbVecCTCDataSourceBObj)) {
					  CTCDataSource dataSourceComponent = (CTCDataSource) TCRMClassFactory.getTCRMComponent(
							  CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
					  DWLResponse response = dataSourceComponent.getAllCTCDataSourcesByEntity( "PERSONNAME", 
							  dbTCRMPersonNameBObj.getPersonNameIdPK(), TCRMRecordFilter.ACTIVE, 
							  dbTCRMPersonNameBObj.getControl());
					  dbVecCTCDataSourceBObj = (Vector<CTCDataSourceBObj>)response.getData();
				  }
				  dbMapCTCDataSourceBObj.put(dbTCRMPersonNameBObj.getPersonNameIdPK(), dbVecCTCDataSourceBObj);
			  }
		  }
	  }
	  return dbMapCTCDataSourceBObj;
  }
  
  /**
   * Getting the request system DSI
   * 
   * @param dbVecCTCDataSourceBObj DB CTC Data Source Vector
   * @param requestSystemName - Request system name
   * @return
   */
	@SuppressWarnings("unchecked")
	private CTCDataSourceBObj getRequestSystemCTCDataSourceBObj(Vector<CTCDataSourceBObj> dbVecCTCDataSourceBObj, 
		  String requestSystemName){
	  if (StringUtils.isNonBlank(requestSystemName) && CTCCommonUtil.isNotEmpty(dbVecCTCDataSourceBObj)) {
		  Iterator itrDBVecCTCDataSourceBObj = dbVecCTCDataSourceBObj.iterator();
		  while(itrDBVecCTCDataSourceBObj.hasNext()){
			  CTCDataSourceBObj objCTCDataSourceBObj = (CTCDataSourceBObj)itrDBVecCTCDataSourceBObj.next();
			  if (requestSystemName.equalsIgnoreCase(objCTCDataSourceBObj.getAdminSystemValue())){
				  return objCTCDataSourceBObj;
			  }
		  }
	  }
			  
	  return null;
  }
  
  /**
   * Getting the request system Name.
   *    If TCRMAdminContEquivBObj is present in the request then 
   *    	Get the admin system value
   *    else 
   *    	Get the value from the DWLControl.ClientSystemName.
   *    
   *      
   * 
   * @param reqTCRMPartyBObj request party object
   * @return 
   */
	@SuppressWarnings("unchecked")
	private String getClientSystemName(TCRMPartyBObj reqTCRMPartyBObj) throws Exception{
	  Vector vecTCRMAdminContEquivBObj =  reqTCRMPartyBObj.getItemsTCRMAdminContEquivBObj();
	  if(CTCCommonUtil.isNotEmpty(vecTCRMAdminContEquivBObj)){
		  TCRMAdminContEquivBObj  objTCRMAdminContEquivBObj = (TCRMAdminContEquivBObj)vecTCRMAdminContEquivBObj.firstElement();
		  if (objTCRMAdminContEquivBObj != null){
			  if(!StringUtils.isBlank(objTCRMAdminContEquivBObj.getAdminSystemType())){
				  objTCRMAdminContEquivBObj.setAdminSystemValue(CTCCommonUtil.getCodeValue(
						  objTCRMAdminContEquivBObj.getAdminSystemType(), CTCConstants.CD_ADMIN_SYS_TP, 
						  objTCRMAdminContEquivBObj.getControl()));
			  }
				  
			  return objTCRMAdminContEquivBObj.getAdminSystemValue();
		  }
	  }else{
		  DWLControl dwlControl = reqTCRMPartyBObj.getControl();
		  return dwlControl.getClientSystemName();
	  }
	  return null;
	}
	/**
	 * Get the Primary Email object from the given vector.
	 *    if ContactMethodUsageType is equal to 5(Primary Email) then return the object.
	 * 
	 * @param vecTCRMPartyContactMethodBObj Party contact method vector
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private TCRMPartyContactMethodBObj getPrimaryEmail(Vector vecTCRMPartyContactMethodBObj) throws Exception{
		if(CTCCommonUtil.isNotEmpty(vecTCRMPartyContactMethodBObj)){
			Iterator itrTCRMPartyContactMethodBObj = vecTCRMPartyContactMethodBObj.iterator();
			while(itrTCRMPartyContactMethodBObj.hasNext())
			{
				TCRMPartyContactMethodBObj objTCRMPartyContactMethodBObj=(TCRMPartyContactMethodBObj)itrTCRMPartyContactMethodBObj.next();
				if(CTCConstants.CONT_METH_TP_EMAIL.equals(objTCRMPartyContactMethodBObj.getTCRMContactMethodBObj().getContactMethodType())&&
						CTCCommonUtil.getProperty(CTCConstants.EMAIL_PRIMARY_USAGE_TYPE).equals(objTCRMPartyContactMethodBObj.getContactMethodUsageType())){
					return objTCRMPartyContactMethodBObj;
				}
			}
		}
			return null;
	}
	/**
	 * Get the Primary Person Name object from the given vector.
	 *    if NameUsageType is equal to 1 (Primary) then return the object.
	 *    
	 * @param vecTCRMPersonNameBObj person name vector
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private TCRMPersonNameBObj getPrimaryPersonName(Vector vecTCRMPersonNameBObj) throws Exception{
		if(CTCCommonUtil.isNotEmpty(vecTCRMPersonNameBObj)){
			Iterator itrTCRMPersonNameBObj = vecTCRMPersonNameBObj.iterator();
			while(itrTCRMPersonNameBObj.hasNext())
			{
				TCRMPersonNameBObj objTCRMPersonNameBObj=(TCRMPersonNameBObj)itrTCRMPersonNameBObj.next();
				if(CTCCommonUtil.getProperty(CTCConstants.PERSONNAME_PRIMARY_USAGE_TYPE).equals(objTCRMPersonNameBObj.getNameUsageType())){
					return objTCRMPersonNameBObj;
				}
			}
		}
			return null;
	}
	/**
	 * Creating the HasMap for the ACTIVE CTCDataSource�s for the ADDRESSGROUP entity based on the 
	 *    Party Address ID PK. 
	 * 
	 * @param dbVecTCRMPartyAddressBObj DB party Address vector
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private HashMap getAddressCTCDataSourceBObjs(Vector dbVecTCRMPartyAddressBObj) throws Exception{
		  HashMap dbMapCTCDataSourceBObj = new HashMap();
		  if (CTCCommonUtil.isNotEmpty(dbVecTCRMPartyAddressBObj)) {
			  Iterator itrDBVecPartyAddressBObj = dbVecTCRMPartyAddressBObj.iterator();
			  while(itrDBVecPartyAddressBObj.hasNext()){
				  Vector<CTCDataSourceBObj> dbVecCTCDataSourceBObj = null;
				  Object obj = itrDBVecPartyAddressBObj.next();
				  TCRMPartyAddressBObj dbTCRMPartyAddressBObj = (TCRMPartyAddressBObj)obj;
				
				  if(obj!= null) {
					  if ( obj instanceof CTCPartyAddressBObjExt){
						  CTCPartyAddressBObjExt dbCTCPartyAddressBObjExt = (CTCPartyAddressBObjExt)obj;
						  dbVecCTCDataSourceBObj = dbCTCPartyAddressBObjExt.getItemsCTCDataSourceBObj();
					  }
					  
					  if(CTCCommonUtil.isEmpty(dbVecCTCDataSourceBObj)) {
						  CTCDataSource dataSourceComponent = (CTCDataSource) TCRMClassFactory.getTCRMComponent(
								  CTCAdditionsPropertyKeys.CTCDATA_SOURCE_COMPONENT);
						  DWLResponse response = dataSourceComponent.getAllCTCDataSourcesByEntity( "ADDRESSGROUP", 
								  dbTCRMPartyAddressBObj.getPartyAddressIdPK(), TCRMRecordFilter.ACTIVE, 
								  dbTCRMPartyAddressBObj.getControl());
						  dbVecCTCDataSourceBObj = (Vector<CTCDataSourceBObj>)response.getData();
					  }
					  dbMapCTCDataSourceBObj.put(dbTCRMPartyAddressBObj.getPartyAddressIdPK(), dbVecCTCDataSourceBObj);
				  }
				  
			  }
		  }
		  return dbMapCTCDataSourceBObj;
	  }
	/**
	 * Get the Primary Address object from the given vector.
	 *    if AddressUsageType is equal to 1 (Primary Address) then return the object.
	 * 
	 * @param vecTCRMPartyAddress Party Address vector
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private Object getPrimaryAddressObject(Vector vecTCRMPartyAddress) throws Exception {
		Iterator itrTCRMPartyAddress = vecTCRMPartyAddress.iterator();
		while(itrTCRMPartyAddress.hasNext()){
			TCRMPartyAddressBObj objTCRMPartyAddress=(TCRMPartyAddressBObj)itrTCRMPartyAddress.next();
			if(CTCCommonUtil.getProperty(CTCConstants.ADDRESS_PRIMARY_USAGE_TYPE).equals(
					objTCRMPartyAddress.getAddressUsageType())) {
				return objTCRMPartyAddress;
			}
		}
		return null;
	}
	/**
	 * This is method is used to find the party has Credit Card LOB relationship.
	 *    if RelatedLobType is equal to 1 (Credit card LOB) then return true.
	 * 
	 * 
	 * @param vecTCRMPartyLobRelationshipBObj
	 * @return
	 * @throws Exception
	 */
	private boolean hasCreditCardLOBRelationship(Vector<TCRMPartyLobRelationshipBObj> vecTCRMPartyLobRelationshipBObj) throws Exception{
		if(CTCCommonUtil.isNotEmpty(vecTCRMPartyLobRelationshipBObj)){
			Iterator<TCRMPartyLobRelationshipBObj> itrTCRMPartyLobRelationshipBObj = vecTCRMPartyLobRelationshipBObj.iterator();
			String strCreditCardLobType = CTCCommonUtil.getProperty(CTCConstants.CREDIT_CARD_LOB_TYPE);
			while(itrTCRMPartyLobRelationshipBObj.hasNext()){
				TCRMPartyLobRelationshipBObj objTCRMPartyLobRelationshipBObj = itrTCRMPartyLobRelationshipBObj.next();
				if(strCreditCardLobType.equals(objTCRMPartyLobRelationshipBObj.getRelatedLobType())){
					return true;
				}
			}
		}
		return false;
	  }
	
	/**
	 * This method is used to handle the TCRM Party LOB Relationship objects.
	 *    1. Setting ID PKs and last updated date.
	 *    2. If request start date is earlier than the DB start date then
	 *    		update the DB LOB start date
	 *    	else 
	 *    		ignore the LOB relationship update 
	 *    		 
	 *    
	 * @param vecReqPartyLOBRelationship
	 * @param vecResPartyLOBRelationship
	 * @throws Exception
	 */
	private void  handlePartyLOBRelationship(Vector<TCRMPartyLobRelationshipBObj> vecReqPartyLOBRelationship
		, Vector<TCRMPartyLobRelationshipBObj> vecResPartyLOBRelationship) throws Exception
		{
		boolean isFound=false;
	
		logger.info("Entering HandlePartyLOBRelationship");
		Vector<TCRMPartyLobRelationshipBObj> vecPartyLobReltoPersist = new Vector<TCRMPartyLobRelationshipBObj>();
	
		if(CTCCommonUtil.isEmpty(vecReqPartyLOBRelationship) ||CTCCommonUtil.isEmpty(vecResPartyLOBRelationship))
		{
			logger.error("HandlePartyLOBRelationship : Input Vector is Empty");
			return;
		}
		for (TCRMPartyLobRelationshipBObj reqPartyLOBRelationship : vecReqPartyLOBRelationship)
		{
			for (TCRMPartyLobRelationshipBObj resPartyLOBRelationship : vecResPartyLOBRelationship)
			{
			if((resPartyLOBRelationship.getLobRelationshipType()
					.equalsIgnoreCase(reqPartyLOBRelationship.getLobRelationshipType())) 
					&& (resPartyLOBRelationship.getRelatedLobType().equalsIgnoreCase(
							reqPartyLOBRelationship.getRelatedLobType())) ) {
				isFound=true;
				String strReqStartDate = reqPartyLOBRelationship.getStartDate();
				String strResStartDate = resPartyLOBRelationship.getStartDate();
				//If request start date is earlier than the DB start date
				if (strReqStartDate.compareTo(strResStartDate) < 0) {
					//update the DB LOB start date
					reqPartyLOBRelationship.setPartyLobRelationshipLastUpdateDate
					(resPartyLOBRelationship.getPartyLobRelationshipLastUpdateDate());
					reqPartyLOBRelationship.setPartyId(resPartyLOBRelationship.getPartyId());
					reqPartyLOBRelationship.setPartyLobRelationshipIdPK(resPartyLOBRelationship.getPartyLobRelationshipIdPK());
					vecPartyLobReltoPersist.add(reqPartyLOBRelationship);
				}				
				break;
			}
		}

		if(!isFound) {
			vecPartyLobReltoPersist.add(reqPartyLOBRelationship);
		}
		
	}
	
		// Refresh input vector with relationship objects to be processed
		vecReqPartyLOBRelationship.clear();
		vecReqPartyLOBRelationship.addAll(vecPartyLobReltoPersist);
	
		logger.info("Exiting from handlePartyLOBRelationship ");
	} 
	
	/**
	 * Constructs TCRMPartyLOBRelationship Object by querying code table - CDCTCLOBSOURCETP based on DWLControl.ClientSystemName
	 * 
	 * @param tcrmPartyBObj
	 * @param theControl
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private void constructPartyLOBRelationship(TCRMPartyBObj tcrmPartyBObj, DWLControl theControl) throws Exception
	{
		logger.info("Entering constructPartyLOBRelationship");
		String strRelatedLobType=null;
		String strAdminSystemType=null;
		
		Vector<TCRMPartyLobRelationshipBObj> vecLOBRel = tcrmPartyBObj.getItemsTCRMPartyLobRelationshipBObj();
		Vector<TCRMAdminContEquivBObj> vecAdminCont = tcrmPartyBObj.getItemsTCRMAdminContEquivBObj();
		 
			TCRMAdminContEquivBObj AdminSystemType = vecAdminCont.get(0);
			strAdminSystemType =AdminSystemType.getAdminSystemType();
			strRelatedLobType = CTCCommonUtil.getLobTypebySource(tcrmPartyBObj.getControl(),strAdminSystemType);
				
		if(StringUtils.isBlank(strRelatedLobType)) {
			IDWLErrorMessage errHandler = DWLClassFactory.getErrorHandler();
			DWLError error = errHandler.getErrorMessage(CTCConstants.CTCMAINTAINPARTYBP_COMP, DWLErrorCode.READ_RECORD_ERROR, CTCErrorConstants.ERR_MSG_TP_LOB_NOT_FOUND, theControl);
		
			TCRMException tcrmException = new TCRMException();
			DWLStatus dwlStatus = new DWLStatus();
			dwlStatus.setStatus(DWLStatus.FATAL);
			dwlStatus.addError(error);
			tcrmException.setStatus(dwlStatus);

			throw tcrmException;

		}
		
		try {
			// Construct new object if input does not have PartyLOBRelationship Objects
					
			String currentDateTime = DWLDateTimeUtilities.getCurrentSystemTime();
			
			if(CTCCommonUtil.isEmpty(vecLOBRel)) {
				TCRMPartyLobRelationshipBObj tcrmPartyLobRelationshipBObj = new TCRMPartyLobRelationshipBObj();				
				tcrmPartyLobRelationshipBObj.setStartDate(currentDateTime);
				tcrmPartyLobRelationshipBObj.setRelatedLobType(strRelatedLobType);
				tcrmPartyLobRelationshipBObj.setLobRelationshipType(CTCConstants.LOB_RELATIONSHIP_TP);
				tcrmPartyLobRelationshipBObj.setControl(theControl);
				tcrmPartyBObj.setTCRMPartyLobRelationshipBObj(tcrmPartyLobRelationshipBObj);
				}
			else {
				TCRMPartyLobRelationshipBObj tcrmPartyLobRelationshipBObj = (TCRMPartyLobRelationshipBObj)vecLOBRel.get(0);
				
				if(tcrmPartyLobRelationshipBObj.getStartDate() == null) {
					
					tcrmPartyLobRelationshipBObj.setStartDate(currentDateTime);
				}
				tcrmPartyLobRelationshipBObj.setRelatedLobType(strRelatedLobType);
				tcrmPartyLobRelationshipBObj.setLobRelationshipType(CTCConstants.LOB_RELATIONSHIP_TP);
			}
		}
		catch(Exception e) {
			logger.error("Error in constructPartyLOBRelationship() :" + e.getMessage());
			throw e;
		}
		logger.info("Exiting constructPartyLOBRelationship");

	}
}
