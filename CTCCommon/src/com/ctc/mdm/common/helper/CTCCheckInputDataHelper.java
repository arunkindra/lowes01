package com.ctc.mdm.common.helper;
 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ctc.mdm.common.util.CTCCommonUtil;
import com.dwl.base.DWLControl;
import com.dwl.base.db.DataManager;
import com.dwl.base.db.QueryConnection;
import com.dwl.base.exception.ServiceLocatorException;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;

/**
 * Check if the input address is in the invalid address list or not.
 * 
 * @author Schubert Zhu
 *
 */
public class CTCCheckInputDataHelper {
     private final static String retrieveAllInvalidAddress = "SELECT NAME, CATEGORY, DESCRIPTION, expiry_dt from CDCTCInvalidItemsTP";
     private final static String ADDRESS = "ADR";
     private final static String COMMAN_NAME = "CMN";
     private final static String PERSON_NAME = "PNM";
     private final static String POSTAL_CODE = "ZIP";
     private final static String PHONE = "PHN";
     private static long startedTime;
     private static long currentTime;
     private static long wakeUpTime = 60;
     
     private static CTCCheckInputDataHelper checkInputDataHelper;
     private Map htInvalidAddresses;
     private Map htCommonNames;
     private Map htPostCodes;
     private Map htPersonNames;
     private Map htPhones;
     private final static String REFRESH_INTERVAL = "/IBM/EventManager/MessagesInQueue/max";
 	private final static IDWLLogger logger = DWLLoggerManager.getLogger(CTCCheckInputDataHelper.class);
 	
     @SuppressWarnings("deprecation")
	public static CTCCheckInputDataHelper getInstance(DWLControl dwlControl) {
    	 long difference = System.currentTimeMillis() - startedTime;
    	 
    		 // wakeUpTime = Configuration.getConfiguration().getConfigItem(REFRESH_INTERVAL, dwlControl.retrieveConfigContext()).getLongValue();
    		 try {
				String strRefreshInterval = CTCCommonUtil.getProperty("RefreshBadData_Interval_Minutes");
				
				if (strRefreshInterval != null) {
					wakeUpTime = Long.parseLong(strRefreshInterval);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
    	 
    	 if (checkInputDataHelper == null) {
    		 synchronized (CTCCheckInputDataHelper.class) {
    			 if (checkInputDataHelper == null) {
    				 checkInputDataHelper = new CTCCheckInputDataHelper();
    			 }
    		 }
    	 } else if (difference > (wakeUpTime * 1000 * 60)) {
    		 checkInputDataHelper = null;
    		 Timestamp currentTimestamp = new Timestamp(startedTime);
    		 String lastStartedTime = (currentTimestamp.getYear() + 1900) + "-" + (currentTimestamp.getMonth()+1) + "-" + currentTimestamp.getDate() + " " + currentTimestamp.getHours() + ":" + currentTimestamp.getMinutes();
    		 
    		 // logger.fine("--- Waketup time interval (minutes): " + wakeUpTime + ". Last Started Time: " + lastStartedTime);
    		 CTCCommonUtil.printResults("CTCCheckInputDataHelper", "-- Waketup time interval (minutes): " + wakeUpTime + "; Last Started Time: " + lastStartedTime, startedTime, startedTime, dwlControl);
    		 
			 synchronized (CTCCheckInputDataHelper.class) {
    			 if (checkInputDataHelper == null) {
    				 checkInputDataHelper = new CTCCheckInputDataHelper();
    			 }
    		 }
		 }
    	 
    	 return checkInputDataHelper;
     }
     
     public CTCCheckInputDataHelper() {
    	 init();
     }
     
     /**
      * Initial load for the table
      */
     private void init() {
    	 htInvalidAddresses = new HashMap(1000);
         htCommonNames = new HashMap(1000);
         htPostCodes = new HashMap(1000);
         htPersonNames = new HashMap(1000);
         htPhones = new HashMap(1000);
         
    	startedTime = System.currentTimeMillis();
    	
		ResultSet rs = null;
		QueryConnection connection = null;
		
		try {
			connection = DataManager.getInstance().getQueryConnection();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
		List<String> sqlParam = new ArrayList<String>(2);
		
		/*if (connection == null) {
			throw new Exception(CTCErrorConstants.ERROR_GETTING_DATABASE_CONNECTION);
		}*/

		rs = connection.queryResults(retrieveAllInvalidAddress, sqlParam.toArray());
		String name = null;
		String description = null;
		Timestamp expirydt = null;
		java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(System.currentTimeMillis());
		String category = null;
		
		try {
			while (rs.next()) {
				name = (String)rs.getString("name");
				category = (String)rs.getString("CATEGORY");
				description = (String)rs.getString("description");
				expirydt = (Timestamp)rs.getTimestamp("expiry_dt");
				
				if (expirydt == null || expirydt != null && expirydt.after(currentTimestamp)) {
					if (name != null) {
						if (category.equalsIgnoreCase(ADDRESS)) {
					        htInvalidAddresses.put(name.trim().toUpperCase(), name.trim().toUpperCase());
						} else if (category.equalsIgnoreCase(COMMAN_NAME)) {
							htCommonNames.put(name.trim().toUpperCase(), name.trim().toUpperCase());
						} else if (category.equalsIgnoreCase(POSTAL_CODE)) {
							htPostCodes.put(name.trim().toUpperCase(), name.trim().toUpperCase());
						} else if (category.equalsIgnoreCase(PERSON_NAME)) {
							htPersonNames.put(name.trim().toUpperCase(), name.trim().toUpperCase());
						} else  if (category.equalsIgnoreCase(PHONE)) {
							htPhones.put(name.trim().toUpperCase(), name.trim().toUpperCase());
						}
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
     }
     
     /**
      * Check if the input address is in the invalid address list or not.
      * 
      * @param addressLineOne
      * @return
      */
     public boolean isValidAddress(String addressLineOne) {
    	 boolean isValid = true;
    	 
    	 if (addressLineOne != null && addressLineOne.trim().length() > 0) {
    		 Object returnedValue = htInvalidAddresses.get(addressLineOne.trim().toUpperCase());
    		 
    		 if (returnedValue != null) {
    			 isValid = false;
    		 }
    	 } else {
    		 isValid = false;
    	 }
    	 
    	 return isValid;
     }
     
     /*
      * Use the same contents as LastName
      */
     public boolean isValidFistName(String firstName) {
    	 boolean isValid = true;
    	 
    	 if (firstName != null && firstName.trim().length() > 0) {
    		 Object returnedValue = htPersonNames.get(firstName.trim().toUpperCase());
    		 
    		 if (returnedValue != null) {
    			 isValid = false;
    		 }
    	 } else {
    		 isValid = false;
    	 }
    	 
    	 return isValid;
     }
     
     
     /*
      * Use the same contents as FirstName
      */
     public boolean isValidName(String personName) {
    	 boolean isValid = true;
    	 
    	 if (personName != null && personName.trim().length() > 0) {
    		 Object returnedValue = htPersonNames.get(personName.trim().toUpperCase());
    		 
    		 if (returnedValue != null) {
    			 isValid = false;
    		 }
    	 } else {
    		 isValid = false;
    	 }
    	 
    	 return isValid;
     }
     
     /*
      * 
      */
     public boolean isValidPostCode(String postalcode) {
    	 boolean isValid = true;
    	 
    	 if (postalcode != null && postalcode.trim().length() > 0) {
    		 Object returnedValue = htPostCodes.get(postalcode.trim().toUpperCase());
    		 
    		 if (returnedValue != null) {
    			 isValid = false;
    		 }
    	 } else {
    		 isValid = false;
    	 }
    	 
    	 return isValid;
     }
     
     public boolean isValidPhoneNumber(String phoneNumber) {
    	 boolean isValid = true;
    	 
    	 if (phoneNumber != null && phoneNumber.trim().length() > 0) {
    		 Object returnedValue = htPhones.get(phoneNumber.trim().toUpperCase());
    		 
    		 if (returnedValue != null) {
    			 isValid = false;
    		 }
    	 } else {
    		 isValid = false;
    	 }
    	 
    	 return isValid;
     }
     
     public boolean isCommonName(String inputName) {
    	 boolean isFound = false;
    	 
    	 if (inputName != null && inputName.trim().length() > 0) {
    		 Object returnedValue = htCommonNames.get(inputName.trim().toUpperCase());
    		 
    		 if (returnedValue != null) {
    			 isFound = true;
    		 }
    	 }
    	 
    	 return isFound;
     }
     
}
