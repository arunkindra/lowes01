package com.ctc.mdm.common.constants;


public class CTCConstants {
	public static final String LOG_ENTRY_OF_METHOD = "Entering";
    public static final String LOG_EXIT_OF_METHOD = "Exiting";
    public static final String TIME_TAKEN="Time Taken to  Complete ";
    
    public static final String TXN_ADD_PARTY = "addParty";
    public static final String TXN_UPDATE_PARTY = "updateParty";
    public static final String TXN_GET_PARTY = "getParty";
    public static final String TXN_GET_PARTY_WITH_CONTRACTS = "getPartyWithContracts";
    public static final String TXN_GET_CONTRACT_PARTY_ROLE = "getContractPartyRole";
    public static final String TXN_GET_ALL_CONTRACT_ROLE_LOCATION = "getAllContractRoleLocations";
    public static final String MANDATORY_SEARCH_DONE="Y";
    public static final String INQUIRY_LEVEL_1000 = "1000";
    
    public static final String PERSONNAME_PRIMARY_USAGE_TYPE="PERSONNAME_PRIMARY_USAGE_TYPE";
    public static final String PERSONNAME_ALTERNATE_USAGE_TYPE="PERSONNAME_ALTERNATE_USAGE_TYPE";
    
    public static final String ADDRESS_PRIMARY_USAGE_TYPE="ADDRESS_PRIMARY_USAGE_TYPE";
    public static final String ADDRESS_ALTERNATE_USAGE_TYPE="ADDRESS_ALTERNATE_USAGE_TYPE";

    public static final String EMAIL_PRIMARY_USAGE_TYPE="EMAIL_PRIMARY_USAGE_TYPE";
    public static final String EMAIL_ALTERNATE_USAGE_TYPE="EMAIL_ALTERNATE_USAGE_TYPE";
    
    public static final String TSYS_CLIENT_SYSTEM_NAME = "TSYS_CLIENT_SYSTEM_NAME";
    public static final String EPSILON_CLIENT_SYSTEM_NAME = "EPSILON_CLIENT_SYSTEM_NAME";
    
    public static final String CREDIT_CARD_LOB_TYPE = "CREDIT_CARD_LOB_TYPE";
    
    public static final String CONT_METH_TP_PHONE = "1";
    public static final String CONT_METH_TP_EMAIL="2";
    public static final String CONT_METH_TP_FAX="3";
    
    public static final String CD_ADMIN_SYS_TP = "cdadminsystp";
    public static final String CD_COUNTRY_TP = "cdcountrytp";
    public static final String CD_PROV_STATE_TP = "cdprovstatetp";
    
    
    public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT_YYYY_MM_DD = "yyyy-MM-dd hh:mm:ss";
    public static final String CTCMAINTAINPARTY_COMPONENTID="900201";
    public static final String CTCGETFSPARTY_COMPONENTID="900202";
    public static final String SEARCH_COMPLETED_MSSG="SEARCH COMPLETED";
    public static final String CONT_ID="CONT_ID";
    
    public static final  String LOB_RELATIONSHIP_TP = "1";
	public static final String DWLCONTROL_COMP = "4029";
	public static final String CTCMAINTAINPARTYBP_COMP = "100001";
	public static final String ADMIN_SYSTEM_KEY_COMPONENT = "25";
	public static final String CODETABLE_ADMINSYSTEM = "CDADMINSYSTP";
	public static final String LANGUAGE_EN = "100";
	public static final String CODETABLE_CTCLOBSOURCE = "CDCTCLOBSOURCETP";
	public static final String CODETABLE_CTCADMINSYSPRIORITY = "CDCTCADMINSYSPRIORITYTP";
	public static final String FILTER_ALL = "ALL";
	public static final String PARTY_LOB_ACTIVE_FILTER = "ACTIVE";
	public static final String FILTER_ACTIVE = "ALL";
		
	public static final String PARTY_TYPE_ORG = "O";
	
	public static final String PARTY_TYPE_PERSON = "P";
		
	public static final String SUSPECT_TYPE_L = "L";
	
	public static final String SUSPECT_TYPE_A1 = "A1";

	public static final String SUSPECT_TYPE_C = "C";
	
	public static final String ACTIVE_PARTY = "Y";
	
	public static final String INACTIVE_PARTY = "N";
	
	public static final String SOURCE_IDENTIFIER_MDM = "4";
	
	public static final String CTCMAINTAINCONTRACT_COMPONENTID = "900203";

	public static final String PRIMARY_EMAIL_USAGE_TYPE = "5";
	
	public static final String ALTERNATE_EMAIL_USAGE_TYPE = "6";
	
	public static final String ALTERNATE_ADDRESS_USAGE_TYPE = "2";
	
	public static final String COSTAR_CLIENT_SYSTEM_NAME = "COSTAR_CLIENT_SYSTEM_NAME";
	
	public static final String COSTAR = "CoStar";
	
	public static final String TSYS = "TSYS";
	
	public static final String EPSILON = "Epsilon";
	
	public static final String ADMIN_FIELD_NAME_TYPE_COSTAR = "2";
	
	public static final String GET_CONTRACT = "getContract";
	
	public static final String CONTRACT_INQUIRY_LEVEL_2 = "2";
	
	public static final String PARTY_INQUIRY_LEVEL_1 = "1";
	
	public static final String ADMIN_FIELD_NAME_TYPE_CODE_TABLE = "CDADMINFLDNMTP";
	
	public static final String ADMIN_FIELD_NAME_TYPE_CUSTOMER_ID = "4";
	
	public static final String CONTRACT_STATUS_TYPE = "1";
	
	public static final String ADMIN_CONTRACT_ID_TYPE_CUSTOMER_ID = "ADMIN_CONTRACT_ID_TYPE_CUSTOMER_ID";

	public static final String CONTRACT_STATUS_TYPE_ACTIVE = "2";
}
