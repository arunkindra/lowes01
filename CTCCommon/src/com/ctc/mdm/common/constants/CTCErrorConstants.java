package com.ctc.mdm.common.constants;

public class CTCErrorConstants {

	public static final String CTCMAINTAINPARTY_NOT_OF_TYPE_TCRMPARTYBOBJ="2000001";
    public static final String ADMINPARTYID_MISSING_ERROR_CODE="2000002";
    public static final String ADMINSYSTEMTYPE_AND_ADMINSYSTEMVALUE_MISSING_ERROR_CODE="2000003";
    public static final String ADMINCONTEQUIVOBJ_MISSING_ERROR_CODE="2000004";
    public static final String MULTIPLE_ACTIVE_PARTIES_FOUND_ERROR_CODE="2000005";
    public static final String ADMINSYSTEMTYPE_AND_ADMINSYSTEMVALUE_IS_NOT_VALID_ERROR_CODE="2000006";
    public static final String REQUIRED_PARAMETERS_NOT_SUPLIED="2000007";
	public static final String ERR_MSG_ADMIN_SYSTEM_NOT_FOUND = "3725";
	public static final String ERR_MSG_TP_LOB_NOT_FOUND = "1000002";
	
	public static final String COLLAPSE_NOT_ALLOWED_DUE_TO_BIRTHDATE_MISMATCH = "2000008";
	
	public static final String CTCMAINTAINCONTRACT_NOT_OF_TYPE_TCRMCONTRACTBOBJ_ERR_CODE="2000009";
	public static final String ADMINCONTRACTID_MISSING_ERR_CODE="2000010";
	public static final String ADMINFIELDNAMETYPE_OR_ADMINFIELDNAMEVALUE_MISSING_ERR_CODE="2000011";
	public static final String ADMINFEILDNAMETYPE_OR_ADMINFIELDNAMEVALUE_IS_NOT_CORRECT = "2000012";
	public static final String ADMINNATIVEKEY_IS_REQUIRED_ERR_CODE = "2000013";
	public static final String CLLIENTSYSTEMNAME_NOT_FOUND_ERR_CODE = "2000014";
	public static final String NO_ADMINNATIVEKEY_FOR_COSTAR_ERR_CODE = "2000015";
	public static final String MULTIPLE_ACTIVE_CONTRACTS_FOUND_ERR_CODE = "2000016";
	public static final String CONTRACT_COMPONENT_NOT_FOUND_ERR_CODE = "2000017";
	public static final String CONTRACT_PARTY_ROLE_NOT_FOUND_ERR_CODE = "2000018";
	public static final String PARTY_NOT_FOUND_ERR_CODE = "2000019";
	public static final String ERROR_IN_GETTING_CODE_TYPE = "2000020";

	public static final String ERROR_GETTING_DATABASE_CONNECTION = "Error Getting Database Connection";
	
	//Loyalty Error Constants
	
	//ContractStatusType must be provided in the input ContractComponent.
	public static final String CONTRACT_STATUS_TYPE_MISSING = "2000021";
	
	//ContractPartyRole must be provided in the Active ContractComponent.
	public static final String CONTRACT_PARTY_ROLE_MISSING_IN_THE_ACTIVE_CONTRCOMP = "2000022";
	
	//Only one ContractPartyRole must be provided in the input ContractComponent.
	public static final String ONLY_ONE_CONTRACT_PARTY_ROLE_REQUIRED_IN_CONTRCOMP = "2000023";
	
	//Only one ContractPartyRole must be provided in the input Contract.
	public static final String ONLY_ONE_CONTRACT_PARTY_ROLE_REQUIRED_IN_CONTRACT = "2000024";
	
	//Only one Active ContractComponent must be provided in the input Contract.
	public static final String ONLY_ONE_ACTIVE_CONTRACT_COMPONENT_REQUIRED_IN_CONTRACT = "2000025";
	
}
