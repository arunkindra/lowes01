package com.ctc.mdm.common.util;

import com.dwl.base.DWLCommon;
import com.dwl.base.DWLControl;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.utilities.TCRMClassFactory;

public class CTCClassFactory {

	/**
	 * 
	 * @param className
	 * @param control
	 * @return
	 * @throws TCRMException
	 */
	@SuppressWarnings("unchecked")
	public static DWLCommon createBObj(Class className, DWLControl control)throws TCRMException{
		DWLCommon commonBObj = TCRMClassFactory.createBObj(className);
		commonBObj.setControl(control);
		return commonBObj;
	}
}
