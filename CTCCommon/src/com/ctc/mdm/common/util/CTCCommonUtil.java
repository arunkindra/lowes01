package com.ctc.mdm.common.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.ctc.mdm.codetable.obj.CTCLobSourceTypeBObj;
import com.ctc.mdm.common.constants.CTCConstants;
import com.dwl.base.DWLControl;
import com.dwl.base.DWLResponse;
import com.dwl.base.constant.DWLConstantDef;
import com.dwl.base.constant.DWLControlKeys;
import com.dwl.base.error.DWLStatus;
import com.dwl.base.exception.DWLBaseException;
import com.dwl.base.logging.DWLLoggerManager;
import com.dwl.base.logging.IDWLLogger;
import com.dwl.base.util.DWLClassFactory;
import com.dwl.tcrm.coreParty.component.TCRMAddressBObj;
import com.dwl.tcrm.coreParty.component.TCRMAddressStandardizerManager;
import com.dwl.tcrm.coreParty.component.TCRMAdminContEquivBObj;
import com.dwl.tcrm.coreParty.component.TCRMContactMethodBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyLobRelationshipBObj;
import com.dwl.tcrm.coreParty.component.TCRMPartyStandardizerManager;
import com.dwl.tcrm.coreParty.component.TCRMPersonNameBObj;
import com.dwl.tcrm.coreParty.constant.TCRMCorePropertyKeys;
import com.dwl.tcrm.coreParty.interfaces.IAddressStandardizer;
import com.dwl.tcrm.coreParty.interfaces.IAddressStandardizerManager;
import com.dwl.tcrm.coreParty.interfaces.IParty;
import com.dwl.tcrm.coreParty.interfaces.IPartyBusinessServices;
import com.dwl.tcrm.coreParty.interfaces.IPartyStandardizer;
import com.dwl.tcrm.coreParty.interfaces.IPartyStandardizerManager;
import com.dwl.tcrm.exception.TCRMException;
import com.dwl.tcrm.utilities.StringUtils;
import com.dwl.tcrm.utilities.TCRMClassFactory;
import com.ibm.mdm.common.codetype.component.CodeTypeComponentHelperImpl;
import com.ibm.mdm.common.codetype.interfaces.CodeTypeComponentHelper;
import com.ibm.mdm.common.codetype.obj.CodeTypeBObj;
import com.ibm.mdm.common.util.PropertyManager;


public class CTCCommonUtil {
	
	private final static IDWLLogger logger = DWLLoggerManager.getLogger(CTCCommonUtil.class);
	@SuppressWarnings("unchecked")
	public static boolean isNotEmpty (Collection obj) {
		return obj != null && (!obj.isEmpty()) ;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean isEmpty(Collection obj) {
		return obj == null || obj.isEmpty();
	}
	/**
	 * Getting LOB type by Source.
	 * 
	 * @param tcrmPartyBObj
	 * @param strSourceSystem
	 * @return
	 * @throws DWLBaseException
	 */
	public static String getLobTypebySource(DWLControl dwlControl, String  strSourceType) throws DWLBaseException{
	
		logger.info("Entering getLobTpbySource");
		String strLobType = null;
		List<CodeTypeBObj> listCdCTCLobSourceTp = null;
		
		CodeTypeComponentHelper objCodeTypeComponent= new CodeTypeComponentHelperImpl();
		
		try 
		{
		listCdCTCLobSourceTp = objCodeTypeComponent.getAllCodeTypes(CTCConstants.CODETABLE_CTCLOBSOURCE,CTCConstants.LANGUAGE_EN,CTCConstants.FILTER_ALL,dwlControl);
		} 
		catch (Exception objException) 
		{
			logger.error("Error fetching code table values - CDCTCLOBSOURCETP");
			throw new DWLBaseException(objException.getMessage());
		}
		
		if(CTCCommonUtil.isNotEmpty(listCdCTCLobSourceTp))  
		{
			boolean isLobTpFound = false;
			
			for (CodeTypeBObj lobSourceObj : listCdCTCLobSourceTp)
			{
				CTCLobSourceTypeBObj objCTCSourceType = (CTCLobSourceTypeBObj) lobSourceObj;
				
				if(StringUtils.compareWithTrim(strSourceType, objCTCSourceType.getadminsystemtpcd()))
				{
					strLobType = objCTCSourceType.getlobtpcd();
					isLobTpFound = true;
					break;
				}
			}
			
			if(isLobTpFound){
				return strLobType;
			} else {
				return null;
			}
		
		}
		logger.info("Exiting from getLobTpbySource");
			
	return strLobType;
	}
	

	/**
	 * Standardize Address
	 * 
	 * @param objAddress
	 * @return
	 * @throws TCRMException
	 */
	public  static TCRMAddressBObj standardizeAddress(TCRMAddressBObj objAddress) throws TCRMException{

		boolean flagOverride = false;
		if ((objAddress.getStandardFormatingOverride() != null)
				&& (objAddress.getStandardFormatingOverride().trim()
						.equalsIgnoreCase("Y"))) {
			flagOverride = true;
		}
		if ((objAddress.getStandardFormatingIndicator() != null)
				&& (objAddress.getStandardFormatingIndicator().trim()
						.equalsIgnoreCase("Y"))) {
			flagOverride = true;
		}
		if ((!flagOverride) && (!objAddress.isSkipAddressStandardization())) {
			IAddressStandardizerManager theAddressStandardizerManager = new TCRMAddressStandardizerManager();
			IAddressStandardizer theAddressStandardizer;
		
				theAddressStandardizer = theAddressStandardizerManager
						.getAddressStandardizer();
				objAddress = theAddressStandardizer
						.standardizeAddress(objAddress);
			
		
		} else if ((objAddress.getStandardFormatingIndicator() == null)
				|| (objAddress.getStandardFormatingIndicator().trim()
						.equalsIgnoreCase(""))) {
			objAddress.setStandardFormatingIndicator("N");
		}

		return objAddress;

	}
	
	/**
	 * Standardize Contact Method
	 * 
	 * @param objContactMethod
	 * @return
	 * @throws TCRMException
	 */
	public  static TCRMContactMethodBObj standardizeContactMethod(TCRMContactMethodBObj objContactMethod) throws TCRMException{

		boolean flagOverride = false;

		if ((objContactMethod.getStandardFormatingIndicator() != null)
				&& (objContactMethod.getStandardFormatingIndicator().trim()
						.equalsIgnoreCase("Y"))) {
			flagOverride = true;
		}
		if ((!flagOverride) && (!objContactMethod.isSkipContactMethodStandardization())) {
			IAddressStandardizerManager theAddressStandardizerManager = new TCRMAddressStandardizerManager();
			IAddressStandardizer theAddressStandardizer;
		
				theAddressStandardizer = theAddressStandardizerManager
						.getAddressStandardizer();
				objContactMethod = theAddressStandardizer
						.standardizeContactMethod(objContactMethod);
		
		} else if ((objContactMethod.getStandardFormatingIndicator() == null)
				|| (objContactMethod.getStandardFormatingIndicator().trim()
						.equalsIgnoreCase(""))) {
			objContactMethod.setStandardFormatingIndicator("N");
		}

		return objContactMethod;

	}
	
	
	/**
	 *  Standardize Person Name
	 *  
	 * @param objPersonName
	 * @return
	 * @throws TCRMException
	 */
	public  static TCRMPersonNameBObj standardizePersonName(TCRMPersonNameBObj objPersonName) throws TCRMException{

		boolean flagOverride = false;

		if ((objPersonName.getStandardFormattingIndicator() != null)
				&& (objPersonName.getStandardFormattingIndicator().trim()
						.equalsIgnoreCase("Y"))) {
			flagOverride = true;
		}
		if ((!flagOverride) && (!objPersonName.isNameStandardizedFlag())) {
		
			IPartyStandardizerManager thePartyStandardizerManager = new TCRMPartyStandardizerManager();
			IPartyStandardizer thePartyStandardizer = thePartyStandardizerManager
					.getPartyStandardizer();
			objPersonName = thePartyStandardizer.standardizePersonName(objPersonName);
			
		} else if ((objPersonName.getStandardFormattingIndicator() == null)
				|| (objPersonName.getStandardFormattingIndicator().trim()
						.equalsIgnoreCase(""))) {
			objPersonName.setStandardFormattingIndicator("N");
		}

		return objPersonName;

	}

	/**
	 * Match the two string fields, this method will true if the second argument is also null.
	 * 
	 * @param reqField
	 * @param dbField
	 * @return
	 */
	public static boolean matchStringField(String reqField, String dbField)
	{
//		if((StringUtils.isBlank(reqField) && StringUtils.isBlank(dbField))
//			||StringUtils.isNonBlank(reqField) && (dbField==null || reqField.equalsIgnoreCase(dbField)))
//		{
//			return true;
//		}
//		return false;
		return StringUtils.compareIgnoreCaseWithTrim(reqField, dbField);
	
	}
	/**
	 * Code Type will be returned for the code value from the table name.
	 *  
	 * @param codeValue - code value to get the code type
	 * @param tableName - Table name which perform the lookup
	 * @param dwlControl - DWL control
	 * @return Code type will be returned.
	 * @throws Exception
	 */
	public static String getCodeType(String codeValue, String tableName, DWLControl dwlControl) throws Exception {
        CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
        String langId = (String) dwlControl.get(DWLControlKeys.LANG_ID);
        if( StringUtils.isNonBlank(codeValue)){
             CodeTypeBObj ctBObj = codeTypeCompHelper
                      .getCodeTypeByValue(tableName, langId, codeValue,
                    		  dwlControl);
             if (ctBObj != null) {
                 return ctBObj.gettp_cd();
             } 
        }
        return null;
     }
	/**
	 *	Code Value will be returned for the code type from the table name.  
	 * 
	 * @param codeType - Type to get the code Value
	 * @param tableName - Table name which perform the lookup
	 * @param dwlControl - DWL control
	 * @return Code value will be returned.
	 * @throws Exception
	 */
	public static String getCodeValue(String codeType, String tableName, DWLControl dwlControl) throws Exception {
	        CodeTypeComponentHelper codeTypeCompHelper = DWLClassFactory.getCodeTypeComponentHelper();
	        String langId = (String) dwlControl.get(DWLControlKeys.LANG_ID);
	        if( StringUtils.isNonBlank(codeType)){
	             CodeTypeBObj ctBObj = codeTypeCompHelper
	                      .getCodeTypeByCode(tableName, langId, codeType,
	                    		  dwlControl);
	             if (ctBObj != null) {
	                 return ctBObj.getvalue();
	             } 
	        }
	        return null;
	     }
	/**
	 * This method is used to get the property value from the properties file.
	 * 
	 * @param key String key
	 * @return Value will be returned based on the key
	 * @throws Exception
	 */
	public static String getProperty(String key) throws Exception{
		 return PropertyManager.getProperty(DWLConstantDef.TCRM_APP_NAME, key);
	 }

	/**
	 * 
	 * @param reqTCRMAddressBObj
	 * @throws Exception
	 */
	public static void setAddressFieldsValueToType(TCRMAddressBObj reqTCRMAddressBObj)throws Exception {
		if(StringUtils.isBlank(reqTCRMAddressBObj.getProvinceStateType())) {
			 reqTCRMAddressBObj.setProvinceStateType(getCodeType(
					 reqTCRMAddressBObj.getProvinceStateValue(), CTCConstants.CD_PROV_STATE_TP, 
					 reqTCRMAddressBObj.getControl()));
		 }
		 if(StringUtils.isBlank(reqTCRMAddressBObj.getCountryType())) {
			 reqTCRMAddressBObj.setCountryType(getCodeType(
					 reqTCRMAddressBObj.getCountryValue(), CTCConstants.CD_COUNTRY_TP, 
					 reqTCRMAddressBObj.getControl()));
		 }
	}
	/**
	 * 
	 * To get the matched DB admin contEquiv object for redundant update and populate in the response .
	 * 
	 * @param strPartyID String Party ID
	 * @param objAdminContEquiv - Input contequiv object
	 * @param dwlControl - control object
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static TCRMAdminContEquivBObj getMatchedDBAdminContEquiv(String strPartyID, 
			TCRMAdminContEquivBObj objAdminContEquiv,DWLControl dwlControl) throws Exception{
		try {
			IParty partyComponent = (IParty) TCRMClassFactory.getTCRMComponent(
					TCRMCorePropertyKeys.PARTY_COMPONENT);
			//get the admin sys keys based on the party ID.
			Vector vecAdminContEquiv = partyComponent.getAllPartyAdminSysKeys(strPartyID, dwlControl);
			//compare and return the matching admin contequiv object.
			if(isNotEmpty(vecAdminContEquiv)){
				Iterator itrAdminContEquiv = vecAdminContEquiv.iterator();
				while(itrAdminContEquiv.hasNext()) {
					TCRMAdminContEquivBObj foundAdminContEquivBObj = (TCRMAdminContEquivBObj)itrAdminContEquiv.next();
					if(objAdminContEquiv.getAdminPartyId().equalsIgnoreCase(foundAdminContEquivBObj.getAdminPartyId()) 
							&& objAdminContEquiv.getAdminSystemType().equals(foundAdminContEquivBObj.getAdminSystemType())) {
						return foundAdminContEquivBObj;
					}
				}
			}
			}catch(Exception ex) {
				logger.error("AdminContEqualiantID is not Matching");
				throw ex;
			}
		return null;
	}
	/**
	 * 
	 * To get the matched DB PartyLobRelationship object for populate in the response .
	 * 
	 * @param strPartyID
	 * @param strAdminSysType 
	 * @param dwlControl
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static TCRMPartyLobRelationshipBObj getMatchedDBPartyLobRelationship(String strPartyID, 
			String strAdminSysType,DWLControl dwlControl) throws Exception{
		try {
			IPartyBusinessServices partyComponent = (IPartyBusinessServices) TCRMClassFactory.getTCRMComponent(
					TCRMCorePropertyKeys.PARTY_BUSINESS_SERVICES_COMPONENT);
			//get the Active PartyLobRelationship based on the party ID.
			Vector<TCRMPartyLobRelationshipBObj> vecPartyLobRelationship = partyComponent.getAllPartyLobRelationships(strPartyID, 
					CTCConstants.PARTY_LOB_ACTIVE_FILTER, dwlControl);
			//compare and return the matching PartyLobRelationship object.
			if(isNotEmpty(vecPartyLobRelationship)){
				Iterator itrVecPartyLobRelationship = vecPartyLobRelationship.iterator();
				while(itrVecPartyLobRelationship.hasNext()) {
					TCRMPartyLobRelationshipBObj foundPartyLobRelationshipBObj = (TCRMPartyLobRelationshipBObj)itrVecPartyLobRelationship.next();
					if(foundPartyLobRelationshipBObj.getRelatedLobType().equals(strAdminSysType)) {
						return foundPartyLobRelationshipBObj;
					}
				}
			}
			}catch(Exception ex) {
				logger.error("Error occured in Retrive data from DB");
				throw ex;
			}
		return null;
	}
	/**
	  * Full address(AddressLineOne, AddressLineTwo, City, ProvinceState , ZipPostalCode and Country)
	  *          exactly matching the incoming Address�s standardized full address.
	  *          
	  * @param reqTCRMAddressBObj - Request Address
	  * @param dbTCRMAddressBObj - DB Address
	  *	@return true if all the fields matched.
	  */
	public static boolean isAddressMatch(TCRMAddressBObj reqTCRMAddressBObj, TCRMAddressBObj dbTCRMAddressBObj) {
		 if(StringUtils.compareIgnoreCaseWithTrim(reqTCRMAddressBObj.getAddressLineOne(),dbTCRMAddressBObj.getAddressLineOne())
				 && StringUtils.compareIgnoreCaseWithTrim(reqTCRMAddressBObj.getAddressLineTwo(),dbTCRMAddressBObj.getAddressLineTwo())
				 && StringUtils.compareIgnoreCaseWithTrim(reqTCRMAddressBObj.getCity(),dbTCRMAddressBObj.getCity())
				 && StringUtils.compareIgnoreCaseWithTrim(reqTCRMAddressBObj.getProvinceStateType(),dbTCRMAddressBObj.getProvinceStateType())
				 && StringUtils.compareIgnoreCaseWithTrim(reqTCRMAddressBObj.getZipPostalCode(),dbTCRMAddressBObj.getZipPostalCode())
				 && StringUtils.compareIgnoreCaseWithTrim(reqTCRMAddressBObj.getCountryType(),dbTCRMAddressBObj.getCountryType())){
			 return true;
		 }
		 return false;
	 }
	/**
	 * 
	 * This method used to check the Response weather success or not.
	 * @param response
	 * @return
	 */
	public static boolean isNotFatalResponse(DWLResponse response) {
		if(response.getStatus().getStatus() != 0 || response.getData() == null) {
			return false;
		}
		return true;
	}
	 
	
	/**
	 * returns true If the response is success else false
	 * 
	 * @param response DWLResponse object
	 * @return
	 */
	public static boolean isSuccessResponse(DWLResponse response) {
		if(response !=null && response.getStatus() != null && 
				response.getStatus().getStatus() == DWLStatus.SUCCESS) {
			return true;
		}
		return false;
	}

	public static void printResults(String component, String callingMethod, long startTime, long endTime, DWLControl control) { 
		String strAdminPartyId = "";
		String requestID = "";
		
		if (control != null) {
		    strAdminPartyId = (String) control.get("AdminPartyId");
		    
		    if (control.getRequestID() != null) {
		        requestID = control.getRequestID().toString();
		    }
		}
		
		StringBuffer tempOutput = new StringBuffer(200);
		tempOutput.append(">********=================  Start  ===========================********<\nComponent: ").append(component).append(", AdminPartyId:").append(strAdminPartyId).append(" requestID = ").append(requestID).append(",\nCalling Method: ").append(callingMethod).append("\nDuration: ").append(endTime - startTime).append("\n<********=================  End  ===========================********>");
		logger.info(tempOutput.toString());
		// System.out.println(tempOutput.toString());
	}
}